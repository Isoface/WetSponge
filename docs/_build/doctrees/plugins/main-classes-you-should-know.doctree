��9      �docutils.nodes��document���)��}�(�refnames�}��samples git�]�h �	reference���)��}�(�parent�h �	paragraph���)��}�(�line�Khh �section���)��}�(hKhh�children�]�(h �title���)��}�(hKhhh]�h �Text����Main classes you should know�����}�(�	rawsource�� �hh�source�NhhhNubahhh$�Main classes you should know�h&�RD:\Programador\Java\Degoos\WetSponge\docs\plugins\main-classes-you-should-know.rst��tagname�h�
attributes�}�(�dupnames�]��backrefs�]��names�]��ids�]��classes�]�uubhh �compound���)��}�(hNhhh]��sphinx.addnodes��toctree���)��}�(hKhh8h]�h$h%h&h(h)h<h*}�(�maxdepth�Kh�$plugins/main-classes-you-should-know�h2]��includefiles�]��includehidden���glob���entries�]��
titlesonly��h4]��caption��	Contents:��hidden��h,]�h.]��numbered�K h0]��
rawcaption�hNuubahhh$h%h&h(h)h6h*}�(h,]�h.]�h0]�h2]�h4]��toctree-wrapper�auubh)��}�(hKhhh]�(h)��}�(hKhh\h]�h�WSText�����}�(h$h%hh_h&NhhhNubahhh$�WSText�h&h(h)hh*}�(h,]�h.]�h0]�h2]�h4]�uubh)��}�(hKhh\h]�(h�eThis class represents a text, with its colors, styles and actions.
Take a look at the whole class on �����}�(h$h%hhmh&NhhhNubh
)��}�(hhmh]�h�JavaDocs�����}�(h$h%hhtubah$�v`JavaDocs <https://degoos.com/javadocs/WetSponge/index.html?com/degoos/wetsponge/entity/living/player/WSPlayer.html>`_�h)h	h*}�(�name��JavaDocs�h,]�h.]�h0]�h4]��refuri��hhttps://degoos.com/javadocs/WetSponge/index.html?com/degoos/wetsponge/entity/living/player/WSPlayer.html�h2]�uubh �target���)��}�(hhmh]��
referenced�Kh$�k <https://degoos.com/javadocs/WetSponge/index.html?com/degoos/wetsponge/entity/living/player/WSPlayer.html>�h)h�h*}�(h,]�h.]�h0]��javadocs�ah4]��refuri�h�h2]��javadocs�auubehhh$��This class represents a text, with its colors, styles and actions.
Take a look at the whole class on `JavaDocs <https://degoos.com/javadocs/WetSponge/index.html?com/degoos/wetsponge/entity/living/player/WSPlayer.html>`_�h&h(h)hh*}�(h,]�h.]�h0]�h2]�h4]�uubh)��}�(hKhh\h]�h�,To create a WSText from a String you can use�����}�(h$h%hh�h&NhhhNubahhh$�,To create a WSText from a String you can use�h&h(h)hh*}�(h,]�h.]�h0]�h2]�h4]�uubh �literal_block���)��}�(hKhh\h]�h�ZWSText customText = WSText.of("Put here the String that you want to convert to a WSText");�����}�(h$h%hh�ubahhh$�ZWSText customText = WSText.of("Put here the String that you want to convert to a WSText");�h&h(h)h�h*}�(�highlight_args�}�h,]��	xml:space��preserve�h.]��linenos���language��java�h0]�h4]�h2]�uubh)��}�(hKhh\h]�(h)��}�(hKhh�h]�h�Builder�����}�(h$h%hh�h&NhhhNubahhh$�Builder�h&h(h)hh*}�(h,]�h.]�h0]�h2]�h4]�uubh)��}�(hKhh�h]�(h�\You can create a more complex WSText taking advantage of the
WSText.builder. Take a look on
�����}�(h$h%hh�h&NhhhNubh
)��}�(hh�h]�h�JavaDocs�����}�(h$h%hh�ubah$�m`JavaDocs <https://degoos.com/javadocs/WetSponge/index.html?com/degoos/wetsponge/text/WSText.Builder.html>`__�h)h	h*}�(�name��JavaDocs�h,]�h.]�h0]�h4]�h��^https://degoos.com/javadocs/WetSponge/index.html?com/degoos/wetsponge/text/WSText.Builder.html�h2]�uubh�^.
Remember that, as every other WetSponge builder, you have to end your
code with the methods �����}�(h$h%hh�h&NhhhNubh �emphasis���)��}�(hh�h]�h�.build()�����}�(h$h%hh�ubah$�
*.build()*�h)h�h*}�(h,]�h.]�h0]�h2]�h4]�uubh� to instanciate the new object.�����}�(h$h%hh�h&NhhhNubehhh$XP  You can create a more complex WSText taking advantage of the
WSText.builder. Take a look on
`JavaDocs <https://degoos.com/javadocs/WetSponge/index.html?com/degoos/wetsponge/text/WSText.Builder.html>`__.
Remember that, as every other WetSponge builder, you have to end your
code with the methods *.build()* to instanciate the new object.�h&h(h)hh*}�(h,]�h.]�h0]�h2]�h4]�uubh)��}�(hK hh�h]�h�-An example of using a WSText.builder would be�����}�(h$h%hj
  h&NhhhNubahhh$�-An example of using a WSText.builder would be�h&h(h)hh*}�(h,]�h.]�h0]�h2]�h4]�uubh�)��}�(hK"hh�h]�hXf  WSText complexText = WSText.builder("Main text of the builder:")
                        .color(EnumTextColor.YELLOW)
                        .append(WSText.builder("Hello people!").color(EnumTextColor.AQUA).style(EnumTextStyle.BOLD).build())
                        .build();

// Equivalent text on Minecraft: "&eMain text of the builder: &b&lHello people!"�����}�(h$h%hj  ubahhh$Xf  WSText complexText = WSText.builder("Main text of the builder:")
                        .color(EnumTextColor.YELLOW)
                        .append(WSText.builder("Hello people!").color(EnumTextColor.AQUA).style(EnumTextStyle.BOLD).build())
                        .build();

// Equivalent text on Minecraft: "&eMain text of the builder: &b&lHello people!"�h&h(h)h�h*}�(h�}�h,]�h�h�h.]�h��h��java�h0]�h4]�h2]�uubh)��}�(hK+hh�h]�h�)��}�(hj(  h]�h�RWe have to extend the snippet to include WSClickActiond and
WSHoverAction examples�����}�(h$h%hj+  ubah$�T*We have to extend the snippet to include WSClickActiond and
WSHoverAction examples*�h)h�h*}�(h,]�h.]�h0]�h2]�h4]�uubahhh$j2  h&h(h)hh*}�(h,]�h.]�h0]�h2]�h4]�uubehhh$h%h&h(h)hh*}�(h,]�h.]�h0]��builder�ah2]��builder�ah4]�uubehhh$h%h&h(h)hh*}�(h,]�h.]�h0]��wstext�ah2]��wstext�ah4]�uubh)��}�(hK/hhh]�(h)��}�(hK/hjO  h]�h�
WSLocation�����}�(h$h%hjR  h&NhhhNubahhh$�
WSLocation�h&h(h)hh*}�(h,]�h.]�h0]�h2]�h4]�uubh)��}�(hK1hjO  h]�(h�2It’s the bridge class between Spigot and Sponge �����}�(h$h%hj`  h&NhhhNubh�)��}�(hj`  h]�h�Location�����}�(h$h%hjg  ubah$�
*Location*�h)h�h*}�(h,]�h.]�h0]�h2]�h4]�uubh�K class but
with more methods to simplify how it works, you can see them on
�����}�(h$h%hj`  h&NhhhNubh
)��}�(hj`  h]�h�JavaDocs�����}�(h$h%hjy  ubah$�j`JavaDocs <https://degoos.com/javadocs/WetSponge/index.html?com/degoos/wetsponge/world/WSLocation.html>`__�h)h	h*}�(�name��JavaDocs�h,]�h.]�h0]�h4]�h��[https://degoos.com/javadocs/WetSponge/index.html?com/degoos/wetsponge/world/WSLocation.html�h2]�uubh�.�����}�(h$h%hj`  h&NhhhNubehhh$��It’s the bridge class between Spigot and Sponge *Location* class but
with more methods to simplify how it works, you can see them on
`JavaDocs <https://degoos.com/javadocs/WetSponge/index.html?com/degoos/wetsponge/world/WSLocation.html>`__.�h&h(h)hh*}�(h,]�h.]�h0]�h2]�h4]�uubh�)��}�(hK5hjO  h]�h��// Get the distance to center from the coord 200x54x200 on "customWorld"
double distanceToCenter = WSLocation.of("customWorld", 200.0, 54.0, 200.0).distance(0, 0, 0);�����}�(h$h%hj�  ubahhh$��// Get the distance to center from the coord 200x54x200 on "customWorld"
double distanceToCenter = WSLocation.of("customWorld", 200.0, 54.0, 200.0).distance(0, 0, 0);�h&h(h)h�h*}�(h�}�h,]�h�h�h.]�h��h��java�h0]�h4]�h2]�uubehhh$h%h&h(h)hh*}�(h,]�h.]�h0]��
wslocation�ah2]��
wslocation�ah4]�uubh)��}�(hK;hhh]�(h)��}�(hK;hj�  h]�h�WSPlayer�����}�(h$h%hj�  h&NhhhNubahhh$�WSPlayer�h&h(h)hh*}�(h,]�h.]�h0]�h2]�h4]�uubh)��}�(hK=hj�  h]�(h�_This class will represent a handled player from the server.
You can check the class methods on
�����}�(h$h%hj�  h&NhhhNubh
)��}�(hj�  h]�h�JavaDocs�����}�(h$h%hj�  ubah$�w`JavaDocs <https://degoos.com/javadocs/WetSponge/index.html?com/degoos/wetsponge/entity/living/player/WSPlayer.html>`__�h)h	h*}�(�name��JavaDocs�h,]�h.]�h0]�h4]�h��hhttps://degoos.com/javadocs/WetSponge/index.html?com/degoos/wetsponge/entity/living/player/WSPlayer.html�h2]�uubh�.�����}�(h$h%hj�  h&NhhhNubehhh$��This class will represent a handled player from the server.
You can check the class methods on
`JavaDocs <https://degoos.com/javadocs/WetSponge/index.html?com/degoos/wetsponge/entity/living/player/WSPlayer.html>`__.�h&h(h)hh*}�(h,]�h.]�h0]�h2]�h4]�uubh�)��}�(hKAhj�  h]�hX0  try {
    // Get the player named IhToN and change its GameMode to Creative or throw a PlayerNotFound exception.
    WetSponge.getServer().getPlayer("IhToN").orElseThrow(WSPlayerNotFoundException::new).setGameMode(EnumGameMode.CREATIVE);
} catch (WSPlayerNotFoundException e) {
    e.printStackTrace();
}�����}�(h$h%hj�  ubahhh$X0  try {
    // Get the player named IhToN and change its GameMode to Creative or throw a PlayerNotFound exception.
    WetSponge.getServer().getPlayer("IhToN").orElseThrow(WSPlayerNotFoundException::new).setGameMode(EnumGameMode.CREATIVE);
} catch (WSPlayerNotFoundException e) {
    e.printStackTrace();
}�h&h(h)h�h*}�(h�}�h,]�h�h�h.]�h��h��java�h0]�h4]�h2]�uubehhh$h%h&h(h)hh*}�(h,]�h.]�h0]��wsplayer�ah2]��wsplayer�ah4]�uubh)��}�(hKKhhh]�(h)��}�(hKKhj�  h]�h�WSWorld�����}�(h$h%hj�  h&NhhhNubahhh$�WSWorld�h&h(h)hh*}�(h,]�h.]�h0]�h2]�h4]�uubh)��}�(hKMhj�  h]�(h�mThe WSWorld class represents a loaded world of Minecraft.
You can take a further look to the class methos on �����}�(h$h%hj
  h&NhhhNubh
)��}�(hj
  h]�h�JavaDocs�����}�(h$h%hj  ubah$�g`JavaDocs <https://degoos.com/javadocs/WetSponge/index.html?com/degoos/wetsponge/world/WSWorld.html>`__�h)h	h*}�(�name��JavaDocs�h,]�h.]�h0]�h4]�h��Xhttps://degoos.com/javadocs/WetSponge/index.html?com/degoos/wetsponge/world/WSWorld.html�h2]�uubh�.�����}�(h$h%hj
  h&NhhhNubehhh$��The WSWorld class represents a loaded world of Minecraft.
You can take a further look to the class methos on `JavaDocs <https://degoos.com/javadocs/WetSponge/index.html?com/degoos/wetsponge/world/WSWorld.html>`__.�h&h(h)hh*}�(h,]�h.]�h0]�h2]�h4]�uubh�)��}�(hKPhj�  h]�h��// Spawn a Wither on the coordinates 50x0x50 in "customWorld"
WSWorld customWorld = WetSponge.getServer().getWorld("customWorld").get();
customWorld.spawnEntity(EnumEntityType.WITHER, Vector3d.from(50, 0, 50));�����}�(h$h%hj-  ubahhh$��// Spawn a Wither on the coordinates 50x0x50 in "customWorld"
WSWorld customWorld = WetSponge.getServer().getWorld("customWorld").get();
customWorld.spawnEntity(EnumEntityType.WITHER, Vector3d.from(50, 0, 50));�h&h(h)h�h*}�(h�}�h,]�h�h�h.]�h��h��java�h0]�h4]�h2]�uubehhh$h%h&h(h)hh*}�(h,]�h.]�h0]��wsworld�ah2]��wsworld�ah4]�uubh)��}�(hKWhhh]�(h)��}�(hKWhjE  h]�h�WSWorldProperties�����}�(h$h%hjH  h&NhhhNubahhh$�WSWorldProperties�h&h(h)hh*}�(h,]�h.]�h0]�h2]�h4]�uubh)��}�(hKYhjE  h]�(h�oCustom properties of a WSWorld, you can get and set almost any world property.
You can see them all of them on �����}�(h$h%hjV  h&NhhhNubh
)��}�(hjV  h]�h�JavaDocs�����}�(h$h%hj]  ubah$�q`JavaDocs <https://degoos.com/javadocs/WetSponge/index.html?com/degoos/wetsponge/world/WSWorldProperties.html>`__�h)h	h*}�(�name��JavaDocs�h,]�h.]�h0]�h4]�h��bhttps://degoos.com/javadocs/WetSponge/index.html?com/degoos/wetsponge/world/WSWorldProperties.html�h2]�uubh�.�����}�(h$h%hjV  h&NhhhNubehhh$��Custom properties of a WSWorld, you can get and set almost any world property.
You can see them all of them on `JavaDocs <https://degoos.com/javadocs/WetSponge/index.html?com/degoos/wetsponge/world/WSWorldProperties.html>`__.�h&h(h)hh*}�(h,]�h.]�h0]�h2]�h4]�uubh�)��}�(hK\hjE  h]�h��// Get the WSWorld "customWorld" and set the world PVP enabled and also make it rain
WSWorld customWorld = WetSponge.getServer().getWorld("customWorld").get();
customWorld.getProperties().setPVPEnabled(true);
customWorld.getProperties().setRaining(true);�����}�(h$h%hjy  ubahhh$��// Get the WSWorld "customWorld" and set the world PVP enabled and also make it rain
WSWorld customWorld = WetSponge.getServer().getWorld("customWorld").get();
customWorld.getProperties().setPVPEnabled(true);
customWorld.getProperties().setRaining(true);�h&h(h)h�h*}�(h�}�h,]�h�h�h.]�h��h��java�h0]�h4]�h2]�uubh�)��}�(hKchjE  h]�h�Khhh$�H.. _Samples Git: https://gitlab.com/degoos/WS-Sample-Plugins/tree/master�h&h(h)h�h*}�(h,]�h.]�h0]��samples git�ah4]�h��7https://gitlab.com/degoos/WS-Sample-Plugins/tree/master�h2]��samples-git�auubehhh$h%h&h(h)hh*}�(h,]�h.]�h0]��wsworldproperties�ah2]��wsworldproperties�ah4]�uubehhh$h%h&h(h)hh*}�(h,]�h.]�h0]��main classes you should know�ah2]��main-classes-you-should-know�ah4]�uubh]�(h�*You can see some WetSponge plugins on our �����}�(h$h%hhh&NhhhNubhh�M but you should take a look to these classes if you don’t want to get lost.�����}�(h$h%hhh&NhhhNubehhh$��You can see some WetSponge plugins on our `Samples Git`_ but you should take a look to these classes if you don't want to get lost.�h&h(h)hh*}�(h,]�h.]�h0]�h2]�h4]�uubh]�h�Samples Git�����}�(h$h%hhuba�resolved�Kh$�`Samples Git`_�h)h	h*}�(�name��Samples Git�h,]�h.]�h0]�h4]�h�j�  h2]�uubas�settings��docutils.frontend��Values���)��}�(�language_code��en��dump_transforms�N�smart_quotes���error_encoding_error_handler��backslashreplace��error_encoding��cp850��record_dependencies�N�pep_base_url�� https://www.python.org/dev/peps/��exit_status_level�K�rfc_references�N�toc_backlinks��entry��strip_classes�N�pep_references�N�_config_files�]��
source_url�N�gettext_compact���pep_file_url_template��pep-%04d��strict_visitor�N�cloak_email_addresses���sectsubtitle_xform���input_encoding_error_handler��strict��config�N�doctitle_xform���	traceback���trim_footnote_reference_space���_source�h(�source_link�N�expose_internals�N�embed_stylesheet���env�N�_disable_config�N�	datestamp�N�report_level�K�warning_stream�N�output_encoding_error_handler�j�  �rfc_base_url��https://tools.ietf.org/html/��output_encoding��utf-8��syntax_highlight��long��sectnum_xform�K�
halt_level�K�footnote_backlinks�K�file_insertion_enabled��hN�strip_comments�N�dump_settings�N�dump_internals�N�character_level_inline_markup���debug�N�dump_pseudo_xml�N�	tab_width�K�raw_enabled�K�	generator�N�input_encoding��	utf-8-sig��_destination�N�docinfo_xform�K�auto_id_prefix��id��strip_elements_with_classes�N�	id_prefix�h%ub�autofootnote_start�K�	nametypes�}�(j�  NjA  Nh��j�  NjC  Nj�  �j�  NjK  Nj�  Nuh$h%�substitution_defs�}�h)h�symbol_footnotes�]��parse_messages�]��indirect_targets�]��	footnotes�]��current_source�N�transform_messages�]��
decoration�N�reporter�Nh]�ha�ids�}�(j�  jE  jC  j�  h�h�j�  j�  jE  h�j�  j�  j�  hjM  h\j�  jO  u�citation_refs�}��symbol_footnote_refs�]��transformer�N�footnote_refs�}��autofootnotes�]��refids�}��current_line�Nh*}�(h,]�h.]��source�h(h0]�h4]�h2]�u�id_start�K�symbol_footnote_start�K �	citations�]�hh�autofootnote_refs�]��nameids�}�(j�  j�  jA  jC  h�h�j�  j�  jC  jE  j�  j�  j�  j�  jK  jM  j�  j�  u�substitution_names�}�ub.