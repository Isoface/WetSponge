.. WetSponge documentation master file, created by
   sphinx-quickstart on Fri Jul  7 23:48:44 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Starting with WetSponge
=======================

| **What is WetSponge?**
| It’s a whole new and powerful API for developers to let them make their plugins compatible *with Spigot 1.8.8, 1.12.2 and 1.13* and the latest *Sponge Version*.
|
| It also modifies apply a performance and utility layer over Spigot, optimizing and adapting to Java8 some of its methods.
| And at last but not least, there are custom APIs such as *NBTTags*, *Text Generation*, *Config Accessors*... which will make your dev proccess easier than ever!
|

.. toctree::
    :maxdepth: 4
    :caption: Contents:


Server Owners
-------------

Installing WetSponge
````````````````````

| It's as easy as including it as a normal plugin.
|
| If you are using Spigot, just drop your WetSponge jar file to your */plugins* folder.
| If you are using Sponge, do the same but into your */mods* folder.
|

Plugins folder
``````````````

| Once you launch your server for the first time using WetSponge, a new plugins directory named *WetSpongePlugins* will be created on your server root folder.
|
| Any WetSponge plugin must go in that folder. They won’t work if you put them inside the original server folder as they aren’t coded as a *“normal”* plugin.
|

Developers
----------

WetSponge as a Dependency
`````````````````````````

| If you know how to add a dependency to your plugin you know how to add WetSponge. But we encourage you to use our Maven Repository to include WetSponge.
|
| As WetSponge can be found in `MVNRepository <https://mvnrepository.com/artifact/com.degoos/WetSponge>`__ you can include WetSponge as a dependency in your *pom.xml* file.
|

.. code-block:: html

          <dependencies>
              <!-- WetSponge -->
              <dependency>
                  <groupId>com.degoos</groupId>
                  <artifactId>WetSponge</artifactId>
                  <version>LATEST</version>
                  <scope>provided</scope>
              </dependency>
          </dependencies>