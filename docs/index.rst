.. WetSponge documentation master file, created by
   sphinx-quickstart on Fri Jul  7 23:48:44 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to WetSponge's documentation!
=====================================

If you don’t know how to start using WetSponge this is your place.

You’ll find code examples and a proccess to get your first plugin
working. Also you should take a look to `WetSponge JavaDocs`_

.. _WetSponge JavaDocs: https://degoos.com/javadocs/WetSponge/

.. toctree::
    :maxdepth: 4
    :caption: Getting Started:

    getting-started

.. toctree::
    :maxdepth: 4
    :caption: Plugin Development:

    plugins/main-classes-you-should-know
    plugins/creating-custom-plugin
    plugins/error-logging
    plugins/creating-commands
    plugins/creating-using-tasks
    plugins/working-with-blocks