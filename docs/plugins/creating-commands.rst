Creating custom Commands
========================

Commands are an important component in any plugins. WetSponge API allows you to create them in a simple and spigot-like way.
You can also create ramified commands using our prebuilt command classes!

.. toctree::
    :maxdepth: 4
    :caption: Contents:


Command manager
---------------

Using the command manager you can create and delete commands. Remember that in WetSponge commands are not linked to a plugin!
Remember that WetSponge commands can replace Spigot and Sponge commands.

.. code-block:: java

    WSCommandManager manager = WetSponge.getCommandManager();
    manager.addCommand(WSCommand command);
    manager.removeCommand(String name);
    Set<WSCommand> commands = manager.getCommands();

If you want to improve your timings result you can link a command to a plugin using this method:

.. code-block:: java

    WetSponge.getCommandManager().addCommand(WSPlugin plugin, WSCommand command);

Commands
--------

Creating a command is very simple. Create a class extending WSCommand and implement its methods.

.. code-block:: java

    public class MyCommand extends WSCommand {

        public MyCommand() {
			super(String name, String description, String[] aliases);
		}

        @Override
        public void executeCommand(WSCommandSource commandSource, String command, String[] arguments) {

        }

        @Override
        public List<String> sendTab(WSCommandSource commandSource, String command, String[] arguments) {
            return new ArrayList<>();
        }

    }

Write the command's code in the method 'executeCommand' and the tabulator list's code in the method 'sendTab'.

| WSCommandSource can be a player (WSPlayer),the console (WSConsoleSource), a rcon (WSRconSource), etc.
| The String 'command' represents the executed command without the slash.
| The String array 'arguments' represents the given arguments along with the command by the source.


You can also add aliases to an already created command.

.. code-block:: java

    WSCommand command = ...;
    command.addAlias(String alias);

Example
~~~~~~~

.. code-block:: java

    public class KillCommand extends WSCommand {

		public KillCommand() {
			super("kill", "Kills a player", "k");
		}

		@Override
		public void executeCommand(WSCommandSource commandSource, String command, String[] arguments) {
			if (arguments.length == 0) {
				if (commandSource instanceof WSPlayer)
					((WSPlayer) commandSource).kill();
				else commandSource.sendMessage("You must be a player to execute this command without target!");
				return;
			}

			Optional<WSPlayer> optional = WetSponge.getServer().getPlayer(arguments[0]);
			if (optional.isPresent()) optional.get().kill();
			else commandSource.sendMessage("The given player doesn't exist!");
		}

		@Override
		public List<String> sendTab(WSCommandSource commandSource, String command, String[] arguments) {
			if (arguments.length != 1) return new ArrayList<>();
			return WetSponge.getServer().getOnlinePlayers().stream().map(WSPlayer::getName)
					.filter(name -> name.toLowerCase().startsWith(arguments[0].toLowerCase())).collect(Collectors.toList());
		}
	}


Ramified Commands
-----------------

Ramified commands can be created using prebuilt classes made by us. The main class is WSRamifiedCommand

.. code-block:: java

    public class MyRamifiedCommand extends WSRamifiedCommand {

		public MyRamifiedCommand() {
			super(String name, String description, WSSubcommand notFoundSubcommand, WSSubcommand[] subcommands, String[] aliases);
		}
	}

| The class is similar to WSCommand, but it has all abstract classes already filled.
| The WSSubcommand 'notFoundSubcommand' will be executed when a requested subcommand by the sender cannot be found. It can be null.
| The WSSubcommand array 'subcommands' are the subcommands of the ramified command. You can add more using the method 'addSubcommand(WSSubcommand subcommand)'

.. code-block:: java

    WSRamifiedCommand command = ...;
    WSSubcommand subcommand = ...;
    command.addSubcommand(subcommand);

Remember that there are several constructors for ramified commands!

Subcommands
~~~~~~~~~~~

Subcommands are similar to normal commands, but instead of requiring a description and aliases it requires the root ramified command.

.. code-block:: java

    public class MySubcommand extends WSSubcommand {


		public MySubcommand() {
			super(String name, WSRamifiedCommand command);
		}

		@Override
		public void executeCommand(WSCommandSource commandSource, String command, String[] arguments, String[] remainingArguments) {

		}

		@Override
		public List<String> sendTab(WSCommandSource commandSource, String command, String[] arguments, String[] remainingArguments) {
			return new ArrayList<>();
		}
	}

There is a ramified subcommand with two constructors too!

.. code-block:: java

    public class MyRamifiedSubcommand extends WSRamifiedSubcommand {

		public MyRamifiedSubcommand() {
			super(String name, WSSubcommand notFoundSubcommand, WSRamifiedCommand command, WSSubcommand... subcommands);
		}

		public MyRamifiedSubcommand() {
			super(String name, WSSubcommand notFoundSubcommand, WSRamifiedCommand command, Set<WSSubcommand> subcommands);
		}
	}



