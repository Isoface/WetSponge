Main classes you should know
============================

You can see some WetSponge plugins on our `Samples Git`_ but you should take a look to these classes if you don't want to get lost.

.. toctree::
    :maxdepth: 4
    :caption: Contents:


WSText
------

This class represents a text, with its colors, styles and actions.
Take a look at the whole class on `JavaDocs <https://degoos.com/javadocs/WetSponge/index.html?com/degoos/wetsponge/entity/living/player/WSPlayer.html>`_

To create a WSText from a String you can use

.. code-block:: java

        WSText customText = WSText.of("Put here the String that you want to convert to a WSText");

Builder
~~~~~~~

You can create a more complex WSText taking advantage of the
WSText.builder. Take a look on
`JavaDocs <https://degoos.com/javadocs/WetSponge/index.html?com/degoos/wetsponge/text/WSText.Builder.html>`__.
Remember that, as every other WetSponge builder, you have to end your
code with the methods *.build()* to instanciate the new object.

An example of using a WSText.builder would be

.. code-block:: java

        WSText complexText = WSText.builder("Main text of the builder:")
                                .color(EnumTextColor.YELLOW)
                                .append(WSText.builder("Hello people!").color(EnumTextColor.AQUA).style(EnumTextStyle.BOLD).build())
                                .build();

        // Equivalent text on Minecraft: "&eMain text of the builder: &b&lHello people!"

*We have to extend the snippet to include WSClickActiond and
WSHoverAction examples*

WSLocation
----------

It’s the bridge class between Spigot and Sponge *Location* class but
with more methods to simplify how it works, you can see them on
`JavaDocs <https://degoos.com/javadocs/WetSponge/index.html?com/degoos/wetsponge/world/WSLocation.html>`__.

.. code-block:: java

    // Get the distance to center from the coord 200x54x200 on "customWorld"
    double distanceToCenter = WSLocation.of("customWorld", 200.0, 54.0, 200.0).distance(0, 0, 0);

WSPlayer
--------

This class will represent a handled player from the server.
You can check the class methods on
`JavaDocs <https://degoos.com/javadocs/WetSponge/index.html?com/degoos/wetsponge/entity/living/player/WSPlayer.html>`__.

.. code-block:: java

    try {
        // Get the player named IhToN and change its GameMode to Creative or throw a PlayerNotFound exception.
        WetSponge.getServer().getPlayer("IhToN").orElseThrow(WSPlayerNotFoundException::new).setGameMode(EnumGameMode.CREATIVE);
    } catch (WSPlayerNotFoundException e) {
        e.printStackTrace();
    }

WSWorld
-------

The WSWorld class represents a loaded world of Minecraft.
You can take a further look to the class methos on `JavaDocs <https://degoos.com/javadocs/WetSponge/index.html?com/degoos/wetsponge/world/WSWorld.html>`__.

.. code-block:: java

    // Spawn a Wither on the coordinates 50x0x50 in "customWorld"
    WSWorld customWorld = WetSponge.getServer().getWorld("customWorld").get();
    customWorld.spawnEntity(EnumEntityType.WITHER, Vector3d.from(50, 0, 50));

WSWorldProperties
-----------------

Custom properties of a WSWorld, you can get and set almost any world property.
You can see them all of them on `JavaDocs <https://degoos.com/javadocs/WetSponge/index.html?com/degoos/wetsponge/world/WSWorldProperties.html>`__.

.. code-block:: java

    // Get the WSWorld "customWorld" and set the world PVP enabled and also make it rain
    WSWorld customWorld = WetSponge.getServer().getWorld("customWorld").get();
    customWorld.getProperties().setPVPEnabled(true);
    customWorld.getProperties().setRaining(true);

.. _Samples Git: https://gitlab.com/degoos/WS-Sample-Plugins/tree/master
