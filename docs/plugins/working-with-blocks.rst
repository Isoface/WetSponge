Working with blocks
===================

Blocks are the most important part of Minecraft. These cubes shape the worlds of Minecraft (Unless it is empty!).
That's why we create a powerful API to edit them!

The block - WSBlock
-------------------

This class represents an unique block. With this class you can get
an immutable copy of the material, the location inside the world or the tile entity.

You can access this class using a WSLocation.

.. code-block:: java

    WSLocation location = ...;
    WSBlock block = location.getBlock();

Modifying blocks - WSBlockState
-------------------------------

Block states allows you to modify a block. **They are not tile entities**.
Spigot uses the name 'BlockState' to refer to tile entities, but that is incorrect.

You can create a block state using a block.

.. code-block:: java

    WSBlock block = ...;
    WSBlockState state = block.createState();

Then you can modify the state.

.. code-block:: java

    WSBlockState state = ...;

    //You can found all block types in the class WSBlockTypes.
    state.setBlockType(WSBlockType blockType);

You can also modify the material. Unlike in WSBlock, it is mutable here.

.. code-block:: java

    //Changing the color of a colored block.
    WSBlockState state = ...;
    WSBlockType blockType = state.getBlockType();
    if(blockType instanceof WSBlockTypeDyeColor)
        ((WSBlockTypeDyeColor)blockType).setDyeColor(EnumDyeColor.BROWN);

The block won't update until you push the changes using the method 'update'.

.. code-block:: java

    WSBlockState state = ...;

    //Modifier code...

    state.update();

If you want to reset the state you can use the method 'refresh'.

.. code-block:: java

    WSBlockState state = ...;

    //Modifier code...

    state.refresh();

Tile entities
-------------

Tile entities are used by blocks that must save several data. Unlike Spigot, WetSponge tile entities are update
immediately after using a method.

To get a tile entity you need a WSBlock object.

.. code-block:: java

    WSBlock block = ...;
    WSTileEntity tileEntity = block.getTileEntity();

Example
~~~~~~~

In this example we will open a sign editor to a player.

.. code-block:: java

    public static void openSignForPlayer (WSBlock block, WSPlayer player) {
		WSTileEntity entity = block.getTileEntity();
		if(entity instanceof WSTileEntitySign) {
			((WSTileEntitySign) entity).editSign(player);
		}
	}

It's also possible to edit the text using your plugin!

.. code-block:: java

    public static void editSign(WSBlock block) {
		WSTileEntity entity = block.getTileEntity();
		if (entity instanceof WSTileEntitySign) {
			((WSTileEntitySign) entity).setLine(0, WSText.of("Hi!"));
			((WSTileEntitySign) entity).setLine(1, WSText.of("I'm"));
			((WSTileEntitySign) entity).setLine(2, WSText.of("using"));
			((WSTileEntitySign) entity).setLine(3, WSText.of("WetSponge!"));
		}
	}