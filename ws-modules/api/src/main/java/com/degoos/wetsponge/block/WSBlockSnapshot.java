package com.degoos.wetsponge.block;

import com.degoos.wetsponge.material.block.WSBlockType;

public class WSBlockSnapshot {

	protected WSBlockType original, material;
	private boolean hasChanged;

	/**
	 * Creates a new {@link WSBlockSnapshot snapshot} by
	 * the original {@link WSBlockType material}.
	 *
	 * @param original the original {@link WSBlockType material}.
	 */
	public WSBlockSnapshot(WSBlockType original) {
		this.original = this.material = original;
		this.hasChanged = false;
	}

	/**
	 * Returns a new copy of the original {@link WSBlockType material}
	 * if this {@link WSBlockSnapshot snapshot}.
	 *
	 * @return a new copy of the original {@link WSBlockType material}.
	 */
	public WSBlockType getOriginal() {
		return original.clone();
	}

	/**
	 * @return the {@link WSBlockType material} of this {@link WSBlockSnapshot snapshot}.
	 */
	public WSBlockType getMaterial() {
		return material;
	}

	/**
	 * Sets the {@link WSBlockType material} of this {@link WSBlockSnapshot snapshot}.
	 *
	 * @param material the new {@link WSBlockType material}.
	 */
	public void setMaterial(WSBlockType material) {
		this.material = material;
		this.hasChanged = true;
	}

	/**
	 * @return whether the material has changed.
	 */
	public boolean hasChanged() {
		return hasChanged && !original.equals(material);
	}

	/**
	 * Sets this {@link WSBlockSnapshot snapshot} to a {@link WSBlock block}.
	 *
	 * @param block the target {@link WSBlock block}.
	 */
	public void setToBlock(WSBlock block) {
		block.createState().setBlockType(material).update();
	}
}
