package com.degoos.wetsponge.block;


import com.degoos.wetsponge.material.block.WSBlockType;
import com.degoos.wetsponge.world.WSLocatable;

public interface WSBlockState extends WSLocatable {

	/**
	 * @return the block of this {@link WSBlockState}.
	 */
	WSBlock getBlock();

	/**
	 * This method returns the {@link WSBlockType} of this {@link WSBlockState}.
	 * A {@link WSBlockType} represents the material of the block.
	 * With a {@link WSBlockType} you can the data of the material of this block.
	 *
	 * @return the {@link WSBlockType}
	 */
	WSBlockType getBlockType();

	/**
	 * Sets the {@link WSBlockType} of this {@link WSBlockState}.
	 * A {@link WSBlockType} represents the material of the block.
	 * With a {@link WSBlockType} you can the data of the material of this block.
	 * You can found all {@link WSBlockType}s in {@link com.degoos.wetsponge.material.WSBlockTypes}.
	 *
	 * @param blockType the {@link WSBlockType} to set
	 * @return this {@link WSBlockState}
	 */
	WSBlockState setBlockType(WSBlockType blockType);

	/**
	 * Updates the {@link WSBlock}, applying all changes of this {@link WSBlockState}.
	 * Physics will be applied.
	 */
	void update();

	/**
	 * Updates the {@link WSBlock}, applying all changes of this {@link WSBlockState}.
	 * Physics will be applied if the boolean is true.
	 *
	 * @param applyPhysics the applyPhysics boolean.
	 */
	void update(boolean applyPhysics);

	/**
	 * Refresh this {@link WSBlockState}, losing all changes you've made.
	 */
	void refresh();
}
