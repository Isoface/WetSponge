package com.degoos.wetsponge.block.tileentity;


import com.degoos.wetsponge.block.WSBlock;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;

public interface WSTileEntity {

	WSBlock getBlock();

	WSNBTTagCompound writeToNBTTagCompound(WSNBTTagCompound nbtTagCompound);

	WSNBTTagCompound readFromNBTTagCompound(WSNBTTagCompound nbtTagCompound);

	Object getHandled();
}
