package com.degoos.wetsponge.block.tileentity;

import com.degoos.wetsponge.entity.projectile.WSProjectileSource;

public interface WSTileEntityDispenser extends WSTileEntityNameableInventory, WSProjectileSource {

}
