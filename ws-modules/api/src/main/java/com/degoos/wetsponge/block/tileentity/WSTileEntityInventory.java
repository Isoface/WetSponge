package com.degoos.wetsponge.block.tileentity;


import com.degoos.wetsponge.inventory.WSInventory;

public interface WSTileEntityInventory extends WSTileEntity {

	WSInventory getInventory ();

}
