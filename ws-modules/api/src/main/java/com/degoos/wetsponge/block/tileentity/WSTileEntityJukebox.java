package com.degoos.wetsponge.block.tileentity;


import com.degoos.wetsponge.item.WSItemStack;

public interface WSTileEntityJukebox extends WSTileEntity {

    /**
     * Attempts to play the currently stored {@link WSItemStack} of this {@link WSTileEntityJukebox}.
     * It won't play if the stored {@link WSItemStack} is not a record.
     */
    void playRecord();

    /**
     * Stops the currently playing record, if any.
     */
    void stopRecord();

    /**
     * Ejects the record item in this Jukebox into the world.
     */
    void ejectRecord();

    /**
     * Ejects the current record in this Jukebox and inserts the given one.
     *
     * @param record The record to insert
     */
    void insertRecord(WSItemStack record);

}
