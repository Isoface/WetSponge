package com.degoos.wetsponge.block.tileentity;

public interface WSTileEntityNameableInventory extends WSTileEntityInventory, WSTileEntityNameable {

}
