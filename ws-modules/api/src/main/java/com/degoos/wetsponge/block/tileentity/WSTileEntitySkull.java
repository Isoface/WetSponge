package com.degoos.wetsponge.block.tileentity;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.enums.block.EnumBlockTypeSkullType;
import com.degoos.wetsponge.user.WSGameProfile;

import java.net.URL;

public interface WSTileEntitySkull extends WSTileEntity {

	WSGameProfile getGameProfile();

	void setGameProfile(WSGameProfile gameProfile);

	EnumBlockFace getOrientation();

	void setOrientation(EnumBlockFace orientation);

	EnumBlockTypeSkullType getSkullType();

	void setSkullType(EnumBlockTypeSkullType skullType);

	void setTexture(String texture);

	void setTexture(URL texture);

	void setTextureByPlayerName(String name);

	void findFormatAndSetTexture(String texture);

}
