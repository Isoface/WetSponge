package com.degoos.wetsponge.bridge.item;

import com.degoos.wetsponge.item.WSItemStack;
import com.degoos.wetsponge.material.WSMaterial;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;

public class BridgeItemStack {

	public static WSItemStack of(WSMaterial material) {
		/*switch (WetSponge.getServerType()) {
			case SPONGE:
				return SpongeItemStack.of(material);
			case SPIGOT:
			case PAPER_SPIGOT:
				return SpigotItemStack.of(material);
			default:
				return null;
		}*/
		return null;
	}

	public static WSItemStack ofSerializedNBTTag(String nbt) throws Exception {
		/*switch (WetSponge.getServerType()) {
			case SPONGE:
				return new SpongeItemStack(nbt);
			case SPIGOT:
			case PAPER_SPIGOT:
				return new SpigotItemStack(nbt);
			default:
				return null;
		}*/
		return null;
	}

	public static WSItemStack ofNBTTagCompound(WSNBTTagCompound nbtTagCompound) throws Exception {
		/*switch (WetSponge.getServerType()) {
			case SPONGE:
				return new SpongeItemStack(nbtTagCompound);
			case SPIGOT:
			case PAPER_SPIGOT:
				return new SpigotItemStack(nbtTagCompound);
			default:
				return null;
		}*/
		return null;
	}

	public static WSItemStack createSkull(String format) {
		/*switch (WetSponge.getServerType()) {
			case SPONGE:
				return new SpongeItemStack(SpongeSkullBuilder.createItemStackByUnknownFormat(format));
			case SPIGOT:
			case PAPER_SPIGOT:
				return new SpigotItemStack(SpigotSkullBuilder.createItemStackByUnknownFormat(format));
			default:
				return null;
		}*/
		return null;
	}

}
