package com.degoos.wetsponge.bridge.nbt;

import com.degoos.wetsponge.exception.nbt.NBTParseException;
import com.degoos.wetsponge.nbt.*;

public class BridgeNBT {

	public static WSNBTTagByte ofByte(byte b) {
		/*switch (WetSponge.getServerType()) {
			case SPONGE:
				return new SpongeNBTTagByte(b);
			case SPIGOT:
			case PAPER_SPIGOT:
				try {
					return new SpigotNBTTagByte(b);
				} catch (Exception e) {
					InternalLogger.printException(e, "An error has occurred while WetSponge was creating a new NBTTag!");
					return null;
				}
			default:
				return null;
		}*/
		return null;
	}

	public static WSNBTTagByteArray ofByteArray(byte[] bytes) {
		/*switch (WetSponge.getServerType()) {
			case SPONGE:
				return new SpongeNBTTagByteArray(bytes);
			case SPIGOT:
			case PAPER_SPIGOT:
				try {
					return new SpigotNBTTagByteArray(bytes);
				} catch (Exception e) {
					InternalLogger.printException(e, "An error has occurred while WetSponge was creating a new NBTTag!");
					return null;
				}
			default:
				return null;
		}*/
		return null;
	}

	public static WSNBTTagCompound newCompound() {
		/*switch (WetSponge.getServerType()) {
			case SPONGE:
				return new SpongeNBTTagCompound();
			case SPIGOT:
			case PAPER_SPIGOT:
				try {
					return new SpigotNBTTagCompound();
				} catch (Exception e) {
					InternalLogger.printException(e, "An error has occurred while WetSponge was creating a new NBTTag!");
					return null;
				}
			default:
				return null;
		}*/
		return null;
	}

	public static WSNBTTagCompound ofCompound(String string) throws NBTParseException {
		/*switch (WetSponge.getServerType()) {
			case SPONGE:
				return new SpongeNBTTagCompound(string);
			case SPIGOT:
			case PAPER_SPIGOT:
				try {
					return new SpigotNBTTagCompound(string);
				} catch (Exception e) {
					InternalLogger.printException(e, "An error has occurred while WetSponge was creating a new NBTTag!");
					return null;
				}
			default:
				return null;
		}*/
		return null;
	}

	public static WSNBTTagDouble ofDouble(double d) {
		/*switch (WetSponge.getServerType()) {
			case SPONGE:
				return new SpongeNBTTagDouble(d);
			case SPIGOT:
			case PAPER_SPIGOT:
				try {
					return new SpigotNBTTagDouble(d);
				} catch (Exception e) {
					InternalLogger.printException(e, "An error has occurred while WetSponge was creating a new NBTTag!");
					return null;
				}
			default:
				return null;
		}*/
		return null;
	}

	public static WSNBTTagEnd newEnd() {
		/*switch (WetSponge.getServerType()) {
			case SPONGE:
				return new SpongeNBTTagEnd();
			case SPIGOT:
			case PAPER_SPIGOT:
				return new SpigotNBTTagEnd();
			default:
				return null;
		}*/
		return null;
	}

	public static WSNBTTagFloat ofFloat(float f) {
		/*switch (WetSponge.getServerType()) {
			case SPONGE:
				return new SpongeNBTTagFloat(f);
			case SPIGOT:
			case PAPER_SPIGOT:
				try {
					return new SpigotNBTTagFloat(f);
				} catch (Exception e) {
					InternalLogger.printException(e, "An error has occurred while WetSponge was creating a new NBTTag!");
					return null;
				}
			default:
				return null;
		}*/
		return null;
	}

	public static WSNBTTagInt ofInt(int i) {
		/*switch (WetSponge.getServerType()) {
			case SPONGE:
				return new SpongeNBTTagInt(i);
			case SPIGOT:
			case PAPER_SPIGOT:
				try {
					return new SpigotNBTTagInt(i);
				} catch (Exception e) {
					InternalLogger.printException(e, "An error has occurred while WetSponge was creating a new NBTTag!");
					return null;
				}
			default:
				return null;
		}*/
		return null;
	}

	public static WSNBTTagIntArray ofIntArray(int[] ints) {
		/*switch (WetSponge.getServerType()) {
			case SPONGE:
				return new SpongeNBTTagIntArray(ints);
			case SPIGOT:
			case PAPER_SPIGOT:
				try {
					return new SpigotNBTTagIntArray(ints);
				} catch (Exception e) {
					InternalLogger.printException(e, "An error has occurred while WetSponge was creating a new NBTTag!");
					return null;
				}
			default:
				return null;
		}*/
		return null;
	}

	public static WSNBTTagList newList() {
		/*switch (WetSponge.getServerType()) {
			case SPONGE:
				return new SpongeNBTTagList();
			case SPIGOT:
			case PAPER_SPIGOT:
				try {
					return new SpigotNBTTagList();
				} catch (Exception e) {
					InternalLogger.printException(e, "An error has occurred while WetSponge was creating a new NBTTag!");
					return null;
				}
			default:
				return null;
		}*/
		return null;
	}

	public static WSNBTTagLong ofLong(long l) {
		/*switch (WetSponge.getServerType()) {
			case SPONGE:
				return new SpongeNBTTagLong(l);
			case SPIGOT:
			case PAPER_SPIGOT:
				try {
					return new SpigotNBTTagLong(l);
				} catch (Exception e) {
					InternalLogger.printException(e, "An error has occurred while WetSponge was creating a new NBTTag!");
					return null;
				}
			default:
				return null;
		}*/
		return null;
	}

	public static WSNBTTagLongArray ofLongArray(long[] longs) {
		/*switch (WetSponge.getServerType()) {
			case SPONGE:
				return new SpongeNBTTagLongArray(longs);
			case SPIGOT:
			case PAPER_SPIGOT:
				try {
					return new SpigotNBTTagLongArray(longs);
				} catch (Exception e) {
					InternalLogger.printException(e, "An error has occurred while WetSponge was creating a new NBTTag!");
					return null;
				}
			default:
				return null;
		}*/
		return null;
	}

	public static WSNBTTagShort ofShort(short s) {
		/*switch (WetSponge.getServerType()) {
			case SPONGE:
				return new SpongeNBTTagShort(s);
			case SPIGOT:
			case PAPER_SPIGOT:
				try {
					return new SpigotNBTTagShort(s);
				} catch (Exception e) {
					InternalLogger.printException(e, "An error has occurred while WetSponge was creating a new NBTTag!");
					return null;
				}
			default:
				return null;
		}*/
		return null;
	}

	public static WSNBTTagString ofString(String string) {
		/*switch (WetSponge.getServerType()) {
			case SPONGE:
				return new SpongeNBTTagString(string);
			case SPIGOT:
			case PAPER_SPIGOT:
				try {
					return new SpigotNBTTagString(string);
				} catch (Exception e) {
					InternalLogger.printException(e, "An error has occurred while WetSponge was creating a new NBTTag!");
					return null;
				}
			default:
				return null;
		}*/
		return null;
	}
}
