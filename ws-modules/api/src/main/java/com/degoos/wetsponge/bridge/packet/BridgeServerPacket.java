package com.degoos.wetsponge.bridge.packet;

import com.degoos.wetsponge.entity.WSEntity;
import com.degoos.wetsponge.entity.living.WSLivingEntity;
import com.degoos.wetsponge.entity.living.player.WSHuman;
import com.degoos.wetsponge.enums.EnumEquipType;
import com.degoos.wetsponge.enums.EnumPlayerListItemAction;
import com.degoos.wetsponge.item.WSItemStack;
import com.degoos.wetsponge.map.WSMapView;
import com.degoos.wetsponge.material.block.WSBlockType;
import com.degoos.wetsponge.packet.play.server.*;
import com.degoos.wetsponge.packet.play.server.extra.WSPlayerListItemData;
import com.degoos.wetsponge.server.response.WSServerStatusResponse;
import com.degoos.wetsponge.text.WSText;
import com.flowpowered.math.vector.Vector2d;
import com.flowpowered.math.vector.Vector2i;
import com.flowpowered.math.vector.Vector3d;
import com.flowpowered.math.vector.Vector3i;

import java.util.Collection;
import java.util.List;
import java.util.Map;

public class BridgeServerPacket {

	public static WSSPacketAnimation newWSSPacketAnimation(WSEntity entity, int animationType) {
		/*switch (WetSponge.getServerType()) {
			case SPIGOT:  		case PAPER_SPIGOT:
				try {
					return new SpigotSPacketAnimation(entity, animationType);
				} catch (Throwable e) {
					e.printStackTrace();
				}
			case SPONGE:
				return new SpongeSPacketAnimation(entity, animationType);
			default:
				return null;
		}*/
		return null;
	}

	public static WSSPacketAnimation newWSSPacketAnimation(int entityId, int animationType) {
		/*switch (WetSponge.getServerType()) {
			case SPIGOT:  		case PAPER_SPIGOT:
				try {
					return new SpigotSPacketAnimation(entityId, animationType);
				} catch (Throwable e) {
					e.printStackTrace();
				}
			case SPONGE:
				return new SpongeSPacketAnimation(entityId, animationType);
			default:
				return null;
		}*/
		return null;
	}

	public static WSSPacketBlockChange newWSSPacketBlockChange(WSBlockType material, Vector3i position) {
		/*switch (WetSponge.getServerType()) {
			case SPIGOT:  		case PAPER_SPIGOT:
				try {
					return new SpigotSPacketBlockChange(material, position);
				} catch (Throwable e) {
					e.printStackTrace();
				}
			case SPONGE:
				return new SpongeSPacketBlockChange(material, position);
			default:
				return null;
		}*/
		return null;
	}

	public static WSSPacketCloseWindows newWSSPacketCloseWindows(int windowsId) {
		/*switch (WetSponge.getServerType()) {
			case SPIGOT:  		case PAPER_SPIGOT:
				try {
					return new SpigotSPacketCloseWindows(windowsId);
				} catch (Throwable e) {
					e.printStackTrace();
				}
			case SPONGE:
				return new SpongeSPacketCloseWindows(windowsId);
			default:
				return null;
		}*/
		return null;
	}

	public static WSSPacketDestroyEntities newWSSPacketDestroyEntities(int... entityIds) {
		/*switch (WetSponge.getServerType()) {
			case SPIGOT:  		case PAPER_SPIGOT:
				try {
					return new SpigotSPacketDestroyEntities(entityIds);
				} catch (Throwable e) {
					e.printStackTrace();
				}
			case SPONGE:
				return new SpongeSPacketDestroyEntities(entityIds);
			default:
				return null;
		}*/
		return null;
	}

	public static WSSPacketEntityEquipment newWSSPacketEntityEquipment(int entityId, EnumEquipType equipType, WSItemStack itemStack) {
		/*switch (WetSponge.getServerType()) {
			case SPONGE:
				return new SpongeSPacketEntityHeadLook(entityId, yaw);
			case SPIGOT:
			case PAPER_SPIGOT:
				try {
					return new SpigotSPacketEntityHeadLook(entityId, yaw);
				} catch (Exception e) {
					InternalLogger.printException(e, "An error has occurred while WetSponge was creating the packet WSSPacketEntityHeadLook!");
				}
			default:
				return null;
		}*/
		return null;
	}

	public static WSSPacketEntityHeadLook newWSSPacketEntityHeadLook(int entityId, int yaw) {
		/*switch (WetSponge.getServerType()) {
			case SPONGE:
				return new SpongeSPacketEntityHeadLook(entityId, yaw);
			case SPIGOT:
			case PAPER_SPIGOT:
				try {
					return new SpigotSPacketEntityHeadLook(entityId, yaw);
				} catch (Exception e) {
					InternalLogger.printException(e, "An error has occurred while WetSponge was creating the packet WSSPacketEntityHeadLook!");
				}
			default:
				return null;
		}*/
		return null;
	}

	public static WSSPacketEntityHeadLook newWSSPacketEntityHeadLook(WSLivingEntity entity) {
		/*switch (WetSponge.getServerType()) {
			case SPONGE:
				return new SpongeSPacketEntityHeadLook(entity);
			case SPIGOT:
			case PAPER_SPIGOT:
				try {
					return new SpigotSPacketEntityHeadLook(entity);
				} catch (Exception e) {
					InternalLogger.printException(e, "An error has occurred while WetSponge was creating the packet WSSPacketEntityHeadLook!");
				}
			default:
				return null;
		}*/
		return null;
	}

	public static WSSPacketEntityLook newWSSPacketEntityLook(WSEntity entity, Vector2i rotation, boolean onGround) {
		/*switch (WetSponge.getServerType()) {
			case SPIGOT:  		case PAPER_SPIGOT:
				return new SpigotSPacketEntityLook(entity, rotation, onGround);
			case SPONGE:
				return new SpongeSPacketEntityLook(entity, rotation, onGround);
			default:
				return null;
		}*/
		return null;
	}

	public static WSSPacketEntityLook newWSSPacketEntityLook(int entity, Vector2i rotation, boolean onGround) {
		/*switch (WetSponge.getServerType()) {
			case SPIGOT:  		case PAPER_SPIGOT:
				return new SpigotSPacketEntityLook(entity, rotation, onGround);
			case SPONGE:
				return new SpongeSPacketEntityLook(entity, rotation, onGround);
			default:
				return null;
		}*/
		return null;
	}

	public static WSSPacketEntityLookMove newWSSPacketEntityLookMove(WSEntity entity, Vector3i position, Vector2i rotation, boolean onGround) {
		/*switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				return new SpigotSPacketEntityLookMove(entity, position, rotation, onGround);
			case SPONGE:
				return new SpongeSPacketEntityLookMove(entity, position, rotation, onGround);
			default:
				return null;
		}*/
		return null;
	}

	public static WSSPacketEntityLookMove newWSSPacketEntityLookMove(int entity, Vector3i position, Vector2i rotation, boolean onGround) {
		/*switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				return new SpigotSPacketEntityLookMove(entity, position, rotation, onGround);
			case SPONGE:
				return new SpongeSPacketEntityLookMove(entity, position, rotation, onGround);
			default:
				return null;
		}*/
		return null;
	}

	public static WSSPacketEntityMetadata newWSSPacketEntityMetadata(WSEntity entity) {
		/*switch (WetSponge.getServerType()) {
			case PAPER_SPIGOT:
			case SPIGOT:
				try {
					return new SpigotSPacketEntityMetadata(entity);
				} catch (Exception e) {
					e.printStackTrace();
				}
			case SPONGE:
				return new SpongeSPacketEntityMetadata(entity);
			default:
				return null;
		}*/
		return null;
	}

	public static WSSPacketEntityProperties newWSSPacketEntityProperties(WSLivingEntity entity) {
		/*switch (WetSponge.getServerType()) {
			case PAPER_SPIGOT:
			case SPIGOT:
				try {
					return new SpigotSPacketEntityProperties(entity);
				} catch (Exception e) {
					e.printStackTrace();
				}
			case SPONGE:
				return new SpongeSPacketEntityProperties(entity);
			default:
				return null;
		}*/
		return null;
	}

	public static WSSPacketEntityRelMove newWSSPacketEntityRelMove(WSEntity entity, Vector3i position, boolean onGround) {
		/*switch (WetSponge.getServerType()) {
			case SPIGOT:  		case PAPER_SPIGOT:
				return new SpigotSPacketEntityRelMove(entity, position, onGround);
			case SPONGE:
				return new SpongeSPacketEntityRelMove(entity, position, onGround);
			default:
				return null;
		}*/
		return null;
	}

	public static WSSPacketEntityRelMove newWSSPacketEntityRelMove(int entity, Vector3i position, boolean onGround) {
		/*switch (WetSponge.getServerType()) {
			case SPIGOT:  		case PAPER_SPIGOT:
				return new SpigotSPacketEntityRelMove(entity, position, onGround);
			case SPONGE:
				return new SpongeSPacketEntityRelMove(entity, position, onGround);
			default:
				return null;
		}*/
		return null;
	}

	public static WSSPacketEntityTeleport newWSSPacketEntityTeleport(WSEntity entity) {
		/*switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				try {
					return new SpigotSPacketEntityTeleport(entity);
				} catch (Exception e) {
					e.printStackTrace();
				}
			case SPONGE:
				return new SpongeSPacketEntityTeleport(entity);
			default:
				return null;
		}*/
		return null;
	}

	public static WSSPacketEntityTeleport newWSSPacketEntityTeleport(int entityIds, Vector3d position, Vector2i rotation, boolean onGround) {
		/*switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				try {
					return new SpigotSPacketEntityTeleport(entityIds, position, rotation, onGround);
				} catch (Exception e) {
					e.printStackTrace();
				}
			case SPONGE:
				return new SpongeSPacketEntityTeleport(entityIds, position, rotation, onGround);
			default:
				return null;
		}*/
		return null;
	}

	public static WSSPacketHeldItemChange newWSSPacketHeldItemChange(int slot) {
		/*switch (WetSponge.getServerType()) {
			case SPIGOT:  		case PAPER_SPIGOT:
				try {
					return new SpigotSPacketHeldItemChange(slot);
				} catch (Throwable e) {
					e.printStackTrace();
				}
			case SPONGE:
				return new SpongeSPacketHeldItemChange(slot);
			default:
				return null;
		}*/
		return null;
	}

	public static WSSPacketMaps newWSSPacketMaps(int mapId, Vector2i origin, Vector2i size, WSMapView mapView) {
		/*switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				try {
					return new SpigotSPacketMaps(mapId, origin, size, mapView);
				} catch (Throwable e) {
					e.printStackTrace();
				}
			case SPONGE:
				return new SpongeSPacketMaps(mapId, origin, size, mapView);
			default:
				return null;
		}*/
		return null;
	}

	public static WSSPacketMultiBlockChange newWSSPacketMultiBlockChange(Vector2i chunkPos, Map<Vector3i, WSBlockType> materials) {
		/*switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				try {
					return new SpigotSPacketMultiBlockChange(chunkPos, materials);
				} catch (Throwable e) {
					e.printStackTrace();
				}
			case SPONGE:
				return new SpongeSPacketMultiBlockChange(chunkPos, materials);
			default:
				return null;
		}*/
		return null;
	}

	public static WSSPacketOpenWindow newWSSPacketOpenWindow(int windowsId, String inventoryType, WSText windowTitle, int slotCount, int entityId) {
		/*switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				try {
					return new SpigotSPacketOpenWindow(windowsId, inventoryType, windowTitle, slotCount, entityId);
				} catch (Throwable e) {
					e.printStackTrace();
				}
			case SPONGE:
				return new SpongeSPacketOpenWindow(windowsId, inventoryType, windowTitle, slotCount, entityId);
			default:
				return null;
		}*/
		return null;
	}

	public static WSSPacketPlayerListItem newWSSPacketPlayerListItem(EnumPlayerListItemAction action, Collection<WSPlayerListItemData> collection) {
		/*switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				try {
					return new SpigotSPacketOpenWindow(windowsId, inventoryType, windowTitle, slotCount, entityId);
				} catch (Throwable e) {
					e.printStackTrace();
				}
			case SPONGE:
				return new SpongeSPacketOpenWindow(windowsId, inventoryType, windowTitle, slotCount, entityId);
			default:
				return null;
		}*/
		return null;
	}

	public static WSSPacketPlayerListItem newWSSPacketPlayerListItem(EnumPlayerListItemAction action, WSPlayerListItemData... itemData) {
		/*switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				try {
					return new SpigotSPacketOpenWindow(windowsId, inventoryType, windowTitle, slotCount, entityId);
				} catch (Throwable e) {
					e.printStackTrace();
				}
			case SPONGE:
				return new SpongeSPacketOpenWindow(windowsId, inventoryType, windowTitle, slotCount, entityId);
			default:
				return null;
		}*/
		return null;
	}

	public static WSSPacketServerInfo newWSSPacketServerInfo(WSServerStatusResponse response) {
		/*switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				try {
					return new SpigotSPacketServerInfo(response);
				} catch (Throwable e) {
					e.printStackTrace();
				}
			case SPONGE:
				return new SpongeSPacketServerInfo(response);
			default:
				return null;
		}*/
		return null;
	}

	public static WSSPacketSetSlot newWSSPacketSetSlot(int windowsId, int slot, WSItemStack itemStack) {
		/*switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				try {
					return new SpigotSPacketSetSlot(windowsId, slot, itemStack);
				} catch (Throwable e) {
					e.printStackTrace();
				}
			case SPONGE:
				return new SpongeSPacketSetSlot(windowsId, slot, itemStack);
			default:
				return null;
		}*/
		return null;
	}

	public static WSSPacketSignEditorOpen newWSSPacketSignEditorOpen(Vector3d position) {
		/*switch (WetSponge.getServerType()) {
			case SPIGOT:  		case PAPER_SPIGOT:
				try {
					return new SpigotSPacketSignEditorOpen(position);
				} catch (Throwable e) {
					e.printStackTrace();
				}
			case SPONGE:
				return new SpongeSPacketSignEditorOpen(position);
			default:
				return null;
		}*/
		return null;
	}

	public static WSSPacketSpawnExprerienceOrb newWSSPacketSpawnExprerienceOrb(Vector3d position, int entityId, int type) {
		/*switch (WetSponge.getServerType()) {
			case SPIGOT:  		case PAPER_SPIGOT:
				try {
					return new SpigotSPacketSpawnExperienceOrb(position, entityId, item);
				} catch (Throwable e) {
					e.printStackTrace();
				}
			case SPONGE:
				return new SpongeSPacketSpawnExperienceOrb(position, entityId, item);
			default:
				return null;
		}*/
		return null;
	}

	public static WSSPacketSpawnGlobalEntity newWSSPacketSpawnGlobalEntity(Vector3d position, int entityId, int type) {
		/*switch (WetSponge.getServerType()) {
			case SPIGOT:  		case PAPER_SPIGOT:
				try {
					return new SpigotSPacketSpawnGlobalEntity(position, entityId, item);
				} catch (Throwable e) {
					e.printStackTrace();
				}
			case SPONGE:
				return new SpongeSPacketSpawnGlobalEntity(position, entityId, item);
			default:
				return null;
		}*/
		return null;
	}


	public static WSSPacketSpawnMob newWSSPacketSpawnMob(WSLivingEntity entity) {
		/*switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				try {
					return new SpigotSPacketSpawnMob(entity);
				} catch (Throwable e) {
					e.printStackTrace();
				}
			case SPONGE:
				return new SpongeSPacketSpawnMob(entity);
			default:
				return null;
		}*/
		return null;
	}

	public static WSSPacketSpawnMob newWSSPacketSpawnMob(WSLivingEntity entity, Vector3d position, Vector3d velocity, Vector2d rotation, float headPitch) {
		/*switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				try {
					return new SpigotSPacketSpawnMob(entity, position, velocity, rotation, headPitch);
				} catch (Throwable e) {
					e.printStackTrace();
				}
			case SPONGE:
				return new SpongeSPacketSpawnMob(entity, position, velocity, rotation, headPitch);
			default:
				return null;
		}*/
		return null;
	}

	public static WSSPacketSpawnObject newWSSPacketSpawnObject(WSEntity entity, int type, int data) {
		/*switch (WetSponge.getServerType()) {
			case SPIGOT:  		case PAPER_SPIGOT:
				try {
					return new SpigotSPacketSpawnObject(entity, item, data);
				} catch (Throwable e) {
					e.printStackTrace();
				}
			case SPONGE:
				return new SpongeSPacketSpawnObject(entity, item, data);
			default:
				return null;
		}*/
		return null;
	}

	public static WSSPacketSpawnObject newWSSPacketSpawnObject(WSEntity entity, Vector3d position, Vector3d velocity, Vector2d rotation, int type, int data) {
		/*switch (WetSponge.getServerType()) {
			case SPIGOT:  		case PAPER_SPIGOT:
				try {
					return new SpigotSPacketSpawnObject(entity, position, velocity, rotation, item, data);
				} catch (Throwable e) {
					e.printStackTrace();
				}
			case SPONGE:
				return new SpongeSPacketSpawnObject(entity, position, velocity, rotation, item, data);
			default:
				return null;
		}*/
		return null;
	}

	public static WSSPacketSpawnPlayer newWSSPacketSpawnPlayer(WSHuman entity) {
		/*switch (WetSponge.getServerType()) {
			case SPIGOT:  		case PAPER_SPIGOT:
				try {
					return new SpigotSPacketSpawnPlayer(entity);
				} catch (Throwable e) {
					e.printStackTrace();
				}
			case SPONGE:
				return new SpongeSPacketSpawnPlayer(entity);
			default:
				return null;
		}*/
		return null;
	}

	public static WSSPacketSpawnPlayer newWSSPacketSpawnPlayer(WSHuman entity, Vector3d position, Vector2d rotation) {
		/*switch (WetSponge.getServerType()) {
			case SPIGOT:  		case PAPER_SPIGOT:
				try {
					return new SpigotSPacketSpawnPlayer(entity, position, rotation);
				} catch (Throwable e) {
					e.printStackTrace();
				}
			case SPONGE:
				return new SpongeSPacketSpawnPlayer(entity, position, rotation);
			default:
				return null;
		}*/
		return null;
	}

	public static WSSPacketWindowItems newWSSPacketWindowItems(int windowsId, List<WSItemStack> itemStacks) {
		/*switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				try {
					return new SpigotSPacketWindowItems(windowsId, itemStacks);
				} catch (Throwable e) {
					e.printStackTrace();
				}
			case SPONGE:
				return new SpongeSPacketWindowItems(windowsId, itemStacks);
			default:
				return null;
		}*/
		return null;
	}

	public static WSSPacketUseBed newWSSPacketUseBed(int entityId, Vector3i position) {
		/*switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				try {
					return new SpigotSPacketWindowItems(windowsId, itemStacks);
				} catch (Throwable e) {
					e.printStackTrace();
				}
			case SPONGE:
				return new SpongeSPacketWindowItems(windowsId, itemStacks);
			default:
				return null;
		}*/
		return null;
	}

	public static WSSPacketUseBed newWSSPacketUseBed(WSHuman human, Vector3i position) {
		/*switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				try {
					return new SpigotSPacketWindowItems(windowsId, itemStacks);
				} catch (Throwable e) {
					e.printStackTrace();
				}
			case SPONGE:
				return new SpongeSPacketWindowItems(windowsId, itemStacks);
			default:
				return null;
		}*/
		return null;
	}
}
