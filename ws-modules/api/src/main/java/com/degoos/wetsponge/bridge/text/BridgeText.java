package com.degoos.wetsponge.bridge.text;

import com.degoos.wetsponge.text.WSText;
import com.degoos.wetsponge.text.WSTranslatableText;
import com.degoos.wetsponge.text.translation.WSTranslation;

public class BridgeText {

	public static WSText.Builder builder() {
		/*switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				return new SpigotText.Builder();
			case SPONGE:
				return new SpongeText.Builder();
			default:
				return null;
		}*/
		return null;
	}

	public static WSText.Builder builder(String string) {
		/*switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				return new SpigotText.Builder(string);
			case SPONGE:
				return new SpongeText.Builder(string);
			default:
				return null;
		}*/
		return null;
	}


	public static WSText.Builder builder(WSText text) {
		/*switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				return new SpigotText.Builder(text);
			case SPONGE:
				return new SpongeText.Builder(text);
			default:
				return null;
		}*/
		return null;
	}


	public static WSText of(String string) {
		/*switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				return new SpigotText(string);
			case SPONGE:
				return new SpongeText(string);
			default:
				return null;
		}*/
		return null;
	}

	public static WSTranslatableText of(WSTranslation translation, Object... objects) {
		/*switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				return new SpigotTranslatableText(translation, objects);
			case SPONGE:
				return new SpongeTranslatableText(translation, objects);
			default:
				return null;
		}*/
		return null;
	}

	public static WSText getByFormattingText(String text) {
		/*switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				return SpigotText.getByFormattingText(text);
			case SPONGE:
				return SpongeText.getByFormattingText(text);
			default:
				return null;
		}*/
		return null;
	}

}
