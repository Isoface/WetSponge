package com.degoos.wetsponge.bridge.text.action;

import com.degoos.wetsponge.text.WSText;
import com.degoos.wetsponge.text.action.click.WSChangePageAction;
import com.degoos.wetsponge.text.action.click.WSOpenURLAction;
import com.degoos.wetsponge.text.action.click.WSRunCommandAction;
import com.degoos.wetsponge.text.action.click.WSSuggestCommandAction;
import com.degoos.wetsponge.text.action.hover.WSShowTextAction;

import java.net.URL;

public class BridgeTextAction {

	public static WSChangePageAction newWSChangePageAction(int page) {
		/*switch (WetSponge.getServerType()) {
			case SPIGOT:  		case PAPER_SPIGOT:
				return new SpigotChangePageAction(page);
			case SPONGE:
				return new SpongeChangePageAction(page);
			default:
				return null;
		}*/
		return null;
	}

	public static WSOpenURLAction newWSOpenURLAction(URL url) {
		/*switch (WetSponge.getServerType()) {
			case SPIGOT:  		case PAPER_SPIGOT:
				return new SpigotOpenURLAction(url);
			case SPONGE:
				return new SpongeOpenURLAction(url);
			default:
				return null;
		}*/
		return null;
	}

	public static WSRunCommandAction newWSRunCommandAction(String command) {
		/*switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				return new SpigotRunCommandAction(command);
			case SPONGE:
				return new SpongeRunCommandAction(command);
			default:
				return null;
		}*/
		return null;
	}

	public static WSSuggestCommandAction newWSSuggestCommandAction(String command) {
		/*switch (WetSponge.getServerType()) {
			case SPIGOT:  		case PAPER_SPIGOT:
				return new SpigotSuggestCommandAction(command);
			case SPONGE:
				return new SpongeSuggestCommandAction(command);
			default:
				return null;
		}*/
		return null;
	}

	public static WSShowTextAction newWSShowTextAction(WSText text) {
		/*switch (WetSponge.getServerType()) {
			case SPIGOT:  		case PAPER_SPIGOT:
				return new SpigotShowTextAction(text);
			case SPONGE:
				return new SpongeShowTextAction(text);
			default:
				return null;
		}*/
		return null;
	}
}
