package com.degoos.wetsponge.color;


import com.degoos.wetsponge.enums.EnumDyeColor;
import com.degoos.wetsponge.enums.EnumTextColor;
import com.degoos.wetsponge.util.Validate;

public class WSColor {

	private int red;
	private int green;
	private int blue;


	public WSColor (int red, int green, int blue) {
		this.red = red;
		this.green = green;
		this.blue = blue;
	}


	public WSColor (byte red, byte green, byte blue) {
		this.red = red & 0xFF;
		this.green = green & 0xFF;
		this.blue = blue & 0xFF;
	}


	/**
	 * Returns the color from a RGB {@link Integer}.
	 *
	 * @param rgb
	 * 		the RGB color.
	 *
	 * @return the {@link WSColor} instance.
	 */
	public static WSColor ofRGB (int rgb) throws IllegalArgumentException {
		try {
			Validate.isTrue(rgb >> 24 == 0, "Extrenuous data in: ", (long) rgb);
			return new WSColor(rgb >> 16 & 0xFF, rgb >> 8 & 0xFF, rgb & 0xFF);
		} catch (Throwable ex) {
			ex.printStackTrace();
			return new WSColor(0, 0, 0);
		}
	}


	/**
	 * Returns the color from a HEX color
	 *
	 * @param hexColor
	 * 		e.g. "#FFFFFF"
	 *
	 * @return the {@link WSColor} instance.
	 */
	public static WSColor ofHEX (String hexColor) {
		return new WSColor(Integer.valueOf(hexColor.substring(1, 3), 16), Integer.valueOf(hexColor.substring(3, 5), 16), Integer.valueOf(hexColor.substring(5, 7), 16));
	}


	/**
	 * Returns the color from a {@link EnumTextColor}
	 *
	 * @param textColor
	 * 		EnumTextColor to create the color from
	 *
	 * @return the {@link WSColor} instance.
	 */
	public static WSColor ofTextColor (EnumTextColor textColor) {
		switch (textColor) {
			case BLACK:
				return WSColor.ofHEX("#000000");
			case DARK_BLUE:
				return WSColor.ofHEX("#0000AA");
			case DARK_GREEN:
				return WSColor.ofHEX("#00AA00");
			case DARK_AQUA:
				return WSColor.ofHEX("#00AAAA");
			case DARK_RED:
				return WSColor.ofHEX("#AA0000");
			case DARK_PURPLE:
				return WSColor.ofHEX("#AA00AA");
			case GOLD:
				return WSColor.ofHEX("#FFAA00");
			case GRAY:
				return WSColor.ofHEX("#AAAAAA");
			case DARK_GRAY:
				return WSColor.ofHEX("#555555");
			case BLUE:
				return WSColor.ofHEX("#5555FF");
			case GREEN:
				return WSColor.ofHEX("#55FF55");
			case AQUA:
				return WSColor.ofHEX("#55FFFF");
			case RED:
				return WSColor.ofHEX("#FF5555");
			case LIGHT_PURPLE:
				return WSColor.ofHEX("#FF55FF");
			case YELLOW:
				return WSColor.ofHEX("#FFFF55");
			case WHITE:
			default:
				return WSColor.ofHEX("#FFFFFF");
		}
	}


	public int getRed () {
		return red;
	}


	public WSColor setRed (int red) {
		this.red = red;
		return this;
	}


	public WSColor setRed (byte red) {
		this.red = red & 0xFF;
		return this;
	}


	public int getGreen () {
		return green;
	}


	public WSColor setGreen (int green) {
		this.green = green;
		return this;
	}


	public WSColor setGreen (byte green) {
		this.green = green & 0xFF;
		return this;
	}


	public int getBlue () {
		return blue;
	}


	public WSColor setBlue (int blue) {
		this.blue = blue;
		return this;
	}


	public WSColor setBlue (byte blue) {
		this.blue = blue & 0xFF;
		return this;
	}


	public int toRGB () {
		return this.getRed() << 16 | this.getGreen() << 8 | this.getBlue();
	}


	public String toHEX () {
		return "#" + String.format("%02X", this.getRed()) + "" + String.format("%02X", this.getGreen()) + "" + String.format("%02X", this.getBlue());
	}


	public WSColor mixColors (WSColor... colors) {
		Validate.noNullElements(colors, "Colors cannot be null");
		int totalRed    = this.getRed();
		int totalGreen  = this.getGreen();
		int totalBlue   = this.getBlue();
		int totalMax    = Math.max(Math.max(totalRed, totalGreen), totalBlue);
		int averageBlue = colors.length;

		for (WSColor averageRed : colors) {
			totalRed += averageRed.getRed();
			totalGreen += averageRed.getGreen();
			totalBlue += averageRed.getBlue();
			totalMax += Math.max(Math.max(averageRed.getRed(), averageRed.getGreen()), averageRed.getBlue());
		}

		float var15             = (float) (totalRed / (colors.length + 1));
		float var14             = (float) (totalGreen / (colors.length + 1));
		float var13             = (float) (totalBlue / (colors.length + 1));
		float var12             = (float) (totalMax / (colors.length + 1));
		float maximumOfAverages = Math.max(Math.max(var15, var14), var13);
		float gainFactor        = var12 / maximumOfAverages;
		return new WSColor((int) (var15 * gainFactor), (int) (var14 * gainFactor), (int) (var13 * gainFactor));
	}


	private double getDistance (WSColor c1, WSColor c2) {
		double rmean   = (c1.getRed() + c2.getRed()) / 2.0D;
		double r       = c1.getRed() - c2.getRed();
		double g       = c1.getGreen() - c2.getGreen();
		int    b       = c1.getBlue() - c2.getBlue();
		double weightR = 2.0D + rmean / 256.0D;
		double weightG = 4.0D;
		double weightB = 2.0D + (255.0D - rmean) / 256.0D;
		return weightR * r * r + weightG * g * g + weightB * b * b;
	}


	private boolean areIdentical (WSColor c1, WSColor c2) {
		return (Math.abs(c1.getRed() - c2.getRed()) <= 5) && (Math.abs(c1.getGreen() - c2.getGreen()) <= 5) && (Math.abs(c1.getBlue() - c2.getBlue()) <= 5);
	}


	public EnumTextColor toTextColor () {
		int    index = 0;
		double best  = -1.0D;
		for (int i = 0 ; i < EnumTextColor.values().length ; i++) {
			if (areIdentical(WSColor.ofTextColor(EnumTextColor.values()[i]), this)) {
				return EnumTextColor.values()[i];
			}
		}
		for (int i = 0 ; i < EnumTextColor.values().length ; i++) {
			double distance = getDistance(this, WSColor.ofTextColor(EnumTextColor.values()[i]));
			if ((distance < best) || (best == -1.0D)) {
				best = distance;
				index = i;
			}
		}
		return EnumTextColor.values()[index];
	}


	public EnumDyeColor toDyeColor () {
		return toTextColor().toDyeColor();
	}


	@Override
	public boolean equals (Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		WSColor color = (WSColor) o;

		if (red != color.red) return false;
		if (green != color.green) return false;
		return blue == color.blue;
	}


	@Override
	public int hashCode () {
		int result = red;
		result = 31 * result + green;
		result = 31 * result + blue;
		return result;
	}


	public String toString () {
		return "WSColor: " + red + " " + green + " " + blue;
	}

}
