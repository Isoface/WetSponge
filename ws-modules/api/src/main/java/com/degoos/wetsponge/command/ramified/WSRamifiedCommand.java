package com.degoos.wetsponge.command.ramified;

import com.degoos.wetsponge.command.WSCommand;
import com.degoos.wetsponge.command.WSCommandSource;
import com.degoos.wetsponge.util.ListUtils;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public class WSRamifiedCommand extends WSCommand {

	private Set<WSSubcommand> subcommands;
	private WSSubcommand notFoundSubcommand;

	public WSRamifiedCommand(String name, String description, WSSubcommand notFoundSubcommand, WSSubcommand[] subcommands, String... aliases) {
		super(name, description, aliases);
		this.notFoundSubcommand = notFoundSubcommand;
		this.subcommands = ListUtils.toSet(subcommands);
	}

	public WSRamifiedCommand(String name, String description, WSSubcommand notFoundSubcommand, Set<WSSubcommand> subcommands, String... aliases) {
		super(name, description, aliases);
		this.notFoundSubcommand = notFoundSubcommand;
		this.subcommands = new HashSet<>(subcommands);
	}

	public WSRamifiedCommand(String name, String description, WSSubcommand notFoundSubcommand, String[] aliases, WSSubcommand... subcommands) {
		super(name, description, aliases);
		this.notFoundSubcommand = notFoundSubcommand;
		this.subcommands = ListUtils.toSet(subcommands);
	}

	public WSRamifiedCommand(String name, String description, WSSubcommand notFoundSubcommand, WSSubcommand... subcommands) {
		super(name, description);
		this.notFoundSubcommand = notFoundSubcommand;
		this.subcommands = ListUtils.toSet(subcommands);
	}

	public WSRamifiedCommand(String name, String description, WSSubcommand notFoundSubcommand, Set<WSSubcommand> subcommands) {
		super(name, description);
		this.notFoundSubcommand = notFoundSubcommand;
		this.subcommands = new HashSet<>(subcommands);
	}

	public Set<WSSubcommand> getSubcommands() {
		return subcommands;
	}

	public void setSubcommands(Set<WSSubcommand> subcommands) {
		this.subcommands = subcommands;
	}

	public boolean addSubcommand(WSSubcommand subcommand) {
		return subcommands.add(subcommand);
	}

	public boolean removeSubcommand(WSSubcommand subcommand) {
		return subcommands.remove(subcommand);
	}

	public void clearSubcommands() {
		subcommands.clear();
	}

	public WSSubcommand getNotFoundSubcommand() {
		return notFoundSubcommand;
	}

	public void setNotFoundSubcommand(WSSubcommand notFoundSubcommand) {
		this.notFoundSubcommand = notFoundSubcommand;
	}

	@Override
	public void executeCommand(WSCommandSource commandSource, String command, String[] arguments) {
		if (!beforeExecute(commandSource, command, arguments)) return;
		if (arguments.length == 0) {
			if (notFoundSubcommand != null) notFoundSubcommand.executeCommand(commandSource, command, arguments, arguments);
			return;
		}
		Optional<WSSubcommand> optional = subcommands.stream().filter(subcommand -> subcommand.getName().equalsIgnoreCase(arguments[0])).findAny();
		if (!optional.isPresent()) {
			if (notFoundSubcommand != null) notFoundSubcommand.executeCommand(commandSource, command, arguments, Arrays.copyOfRange(arguments, 1, arguments.length));
			return;
		}
		optional.get().executeCommand(commandSource, command, arguments, Arrays.copyOfRange(arguments, 1, arguments.length));
	}

	@Override
	public List<String> sendTab(WSCommandSource commandSource, String command, String[] arguments) {
		if (arguments.length == 0) return new ArrayList<>();
		return arguments.length == 1 ? subcommands.stream().map(WSSubcommand::getName).filter(name -> name.toLowerCase().startsWith(arguments[0].toLowerCase()))
			.collect(Collectors.toList()) : subcommands.stream().filter(subcommand -> subcommand.getName().toLowerCase().equals(arguments[0].toLowerCase())).findAny()
			       .map(subcommand -> subcommand.sendTab(commandSource, command, arguments, Arrays.copyOfRange(arguments, 1, arguments.length)))
			       .orElse(new ArrayList<>());
	}

	public boolean beforeExecute(WSCommandSource commandSource, String command, String[] arguments) {
		return true;
	}
}
