package com.degoos.wetsponge.command.wetspongecommand;


import com.degoos.wetsponge.command.WSCommandSource;
import com.degoos.wetsponge.command.ramified.WSRamifiedCommand;
import com.degoos.wetsponge.command.wetspongecommand.plugin.WetSpongeSubcommandPlugin;
import com.degoos.wetsponge.command.wetspongecommand.timings.WetSpongeSubcommandTimings;
import com.degoos.wetsponge.config.WetSpongeMessages;
import com.degoos.wetsponge.text.WSText;

public class WetSpongeCommand extends WSRamifiedCommand {

	public WetSpongeCommand() {
		super("wetSponge", "WetSponge main command.", null);
		setNotFoundSubcommand(new WetSpongeSubcommandHelp(this));
		addSubcommand(getNotFoundSubcommand());
		addSubcommand(new WetSpongeSubcommandPlugin(getNotFoundSubcommand(), this));
		addSubcommand(new WetSpongeSubcommandReloadMessages(this));
		addSubcommand(new WetSpongeSubcommandTimings(getNotFoundSubcommand(), this));
		addSubcommand(new WetSpongeSubcommandErrors(this));
	}

	@Override
	public boolean beforeExecute(WSCommandSource commandSource, String command, String[] arguments) {
		if (!commandSource.hasPermission("wetsponge.admin")) {
			commandSource.sendMessage(WetSpongeMessages.getMessage("command.noPermission").orElse(WSText.empty()));
			return false;
		}
		return true;
	}
}
