package com.degoos.wetsponge.command.wetspongecommand;

import com.degoos.wetsponge.command.WSCommandSource;
import com.degoos.wetsponge.command.ramified.WSRamifiedCommand;
import com.degoos.wetsponge.command.ramified.WSSubcommand;
import com.degoos.wetsponge.config.WetSpongeMessages;
import com.degoos.wetsponge.enums.EnumTextColor;
import com.degoos.wetsponge.enums.EnumTextStyle;
import com.degoos.wetsponge.text.WSText;
import com.degoos.wetsponge.text.action.click.WSOpenURLAction;
import com.degoos.wetsponge.util.InternalLogger;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

public class WetSpongeSubcommandErrors extends WSSubcommand {


	public WetSpongeSubcommandErrors(WSRamifiedCommand command) {
		super("errors", command);
	}

	@Override
	public void executeCommand(WSCommandSource commandSource, String command, String[] arguments, String[] remainingArguments) {
		sendErrors(commandSource);
	}

	@Override
	public List<String> sendTab(WSCommandSource commandSource, String command, String[] arguments, String[] remainingArguments) {
		return null;
	}

	public static void sendErrors(WSCommandSource source) {
		if (InternalLogger.getLastStackTrace() == null) {
			source.sendMessage(WetSpongeMessages.getMessage("command.errors.noErrors").orElse(WSText.empty()));
			return;
		}

		WSText warning = WSText.of("----------------------------------------------", EnumTextColor.DARK_RED, EnumTextStyle.OBFUSCATED);

		WSText text = WetSpongeMessages
			.getMessage("command.errors.errors", "<ERRORS>", InternalLogger.getStackTraces().size(), "<LINK>", InternalLogger.getLastStackTrace().getUrl())
			.orElse(WSText.of(InternalLogger.getLastStackTrace().getUrl()));
		try {
			text = text.toBuilder().clickAction(WSOpenURLAction.of(new URL(InternalLogger.getLastStackTrace().getUrl()))).build();
		} catch (MalformedURLException e) {
			InternalLogger.printException(e, "An error has occurred while WetSponge was pasting the errors!");
		}

		source.sendMessage(warning);
		source.sendMessage(text);
		source.sendMessage(warning);
	}
}
