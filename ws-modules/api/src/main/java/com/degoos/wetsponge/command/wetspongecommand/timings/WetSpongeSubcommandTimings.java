package com.degoos.wetsponge.command.wetspongecommand.timings;

import com.degoos.wetsponge.command.ramified.WSRamifiedCommand;
import com.degoos.wetsponge.command.ramified.WSRamifiedSubcommand;
import com.degoos.wetsponge.command.ramified.WSSubcommand;

public class WetSpongeSubcommandTimings extends WSRamifiedSubcommand {

	public WetSpongeSubcommandTimings(WSSubcommand notFoundSubcommand, WSRamifiedCommand command) {
		super("timings", notFoundSubcommand, command);
		//addSubcommand(new WetSpongeSubcommandTimingsCost(command));
		addSubcommand(new WetSpongeSubcommandTimingsDisable(command));
		addSubcommand(new WetSpongeSubcommandTimingsEnable(command));
		addSubcommand(new WetSpongeSubcommandTimingsPaste(command));
		addSubcommand(new WetSpongeSubcommandTimingsReset(command));
		//addSubcommand(new WetSpongeSubcommandTimingsVerbOff(command));
		//addSubcommand(new WetSpongeSubcommandTimingsVerbOn(command));
	}
}
