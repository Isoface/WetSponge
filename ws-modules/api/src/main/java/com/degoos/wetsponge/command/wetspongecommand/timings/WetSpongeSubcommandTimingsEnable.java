package com.degoos.wetsponge.command.wetspongecommand.timings;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.command.WSCommandSource;
import com.degoos.wetsponge.command.ramified.WSRamifiedCommand;
import com.degoos.wetsponge.command.ramified.WSSubcommand;
import com.degoos.wetsponge.config.WetSpongeMessages;
import com.degoos.wetsponge.text.WSText;

import java.util.ArrayList;
import java.util.List;

public class WetSpongeSubcommandTimingsEnable extends WSSubcommand {


	public WetSpongeSubcommandTimingsEnable(WSRamifiedCommand command) {
		super("on", command);
	}

	@Override
	public void executeCommand(WSCommandSource source, String command, String[] arguments, String[] remainingArguments) {
		WetSponge.getTimings().setEnabled(true);
		source.sendMessage(WetSpongeMessages.getMessage("command.timings.enable").orElse(WSText.empty()));
	}

	@Override
	public List<String> sendTab(WSCommandSource source, String command, String[] arguments, String[] remainingArguments) {
		return new ArrayList<>();
	}
}
