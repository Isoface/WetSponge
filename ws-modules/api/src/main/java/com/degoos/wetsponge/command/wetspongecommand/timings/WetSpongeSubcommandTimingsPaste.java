package com.degoos.wetsponge.command.wetspongecommand.timings;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.command.WSCommandSource;
import com.degoos.wetsponge.command.ramified.WSRamifiedCommand;
import com.degoos.wetsponge.command.ramified.WSSubcommand;

import java.util.ArrayList;
import java.util.List;

public class WetSpongeSubcommandTimingsPaste extends WSSubcommand {


	public WetSpongeSubcommandTimingsPaste(WSRamifiedCommand command) {
		super("paste", command);
	}

	@Override
	public void executeCommand(WSCommandSource source, String command, String[] arguments, String[] remainingArguments) {
		if (WetSponge.getTimings().isEnabled()) {
			WetSponge.getTimings().report(source);
		}
	}

	@Override
	public List<String> sendTab(WSCommandSource source, String command, String[] arguments, String[] remainingArguments) {
		return new ArrayList<>();
	}
}
