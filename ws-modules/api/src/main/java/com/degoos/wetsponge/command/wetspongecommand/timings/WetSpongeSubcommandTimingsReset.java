package com.degoos.wetsponge.command.wetspongecommand.timings;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.command.WSCommandSource;
import com.degoos.wetsponge.command.ramified.WSRamifiedCommand;
import com.degoos.wetsponge.command.ramified.WSSubcommand;
import com.degoos.wetsponge.config.WetSpongeMessages;
import com.degoos.wetsponge.text.WSText;

import java.util.ArrayList;
import java.util.List;

public class WetSpongeSubcommandTimingsReset extends WSSubcommand {


	public WetSpongeSubcommandTimingsReset(WSRamifiedCommand command) {
		super("reset", command);
	}

	@Override
	public void executeCommand(WSCommandSource source, String command, String[] arguments, String[] remainingArguments) {
		if (!WetSponge.getTimings().isEnabled()) {
			source.sendMessage(WetSpongeMessages.getMessage("command.timings.notEnabled").orElse(WSText.empty()));
			return;
		}
		WetSponge.getTimings().reset();
		source.sendMessage(WetSpongeMessages.getMessage("command.timings.reset").orElse(WSText.empty()));
	}

	@Override
	public List<String> sendTab(WSCommandSource source, String command, String[] arguments, String[] remainingArguments) {
		return new ArrayList<>();
	}
}
