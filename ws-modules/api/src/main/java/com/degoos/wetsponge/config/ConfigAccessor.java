package com.degoos.wetsponge.config;


import com.degoos.wetsponge.enums.EnumConfigType;
import com.degoos.wetsponge.util.Validate;
import com.google.common.base.Charsets;
import org.yaml.snakeyaml.DumperOptions;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.nodes.Tag;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

public class ConfigAccessor {

	private Yaml yaml;
	private File file;
	private InputStream inputStream;
	private OutputStream outputStream;
	private Reader reader;
	private Writer writer;
	private Map<String, Object> data;
	private EnumConfigType configType;


	public ConfigAccessor(File file) {
		Validate.notNull(file, "File cannot be null!");
		this.file = file;
		configType = EnumConfigType.FILE;
		loadFile(file);
		if (data == null) this.data = new HashMap<>();
	}


	public ConfigAccessor(InputStream inputStream) {
		this(inputStream, null);
	}


	public ConfigAccessor(InputStream inputStream, OutputStream outputStream) {
		Validate.notNull(inputStream, "InputStream cannot be null!");
		this.inputStream = inputStream;
		this.outputStream = outputStream;
		configType = EnumConfigType.INPUT_STREAM;
		loadInputStream(inputStream);
		if (data == null) this.data = new HashMap<>();
	}


	public ConfigAccessor(Reader reader) {
		this(reader, null);
	}


	public ConfigAccessor(Reader reader, Writer writer) {
		Validate.notNull(reader, "Reader cannot be null!");
		this.reader = reader;
		this.writer = writer;
		configType = EnumConfigType.READER;
		loadReader(reader);
		if (data == null) this.data = new HashMap<>();
	}


	private void loadFile(File file) {
		try {
			if (!file.exists()) file.createNewFile();
			if (file.isDirectory()) {
				file.delete();
				file.createNewFile();
			}
		} catch (Throwable ex) {
			ex.printStackTrace();
			return;
		}
		yaml = new Yaml();
		try {
			data = (Map<String, Object>) yaml.load(new FileInputStream(file));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}


	private void loadInputStream(InputStream inputStream) {
		yaml = new Yaml();
		data = (Map<String, Object>) yaml.load(inputStream);
		try {
			inputStream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	private void loadReader(Reader reader) {
		yaml = new Yaml();
		data = (Map<String, Object>) yaml.load(reader);
	}


	public Yaml getYaml() {
		return yaml;
	}


	public EnumConfigType getConfigType() {
		return configType;
	}


	public File getFile() {
		return file;
	}


	public InputStream getInputStream() {
		return null;
	}


	public OutputStream getOutputStream() {
		return outputStream;
	}


	public Reader getReader() {
		return reader;
	}


	public Writer getWriter() {
		return writer;
	}


	public void save() {
		switch (configType) {
			case FILE:
				save(file);
				break;
			case INPUT_STREAM:
				save(outputStream);
				break;
			case READER:
				save(writer);
				break;
		}
	}


	public void save(File file) {
		Writer writer = null;
		try {
			writer = new OutputStreamWriter(new FileOutputStream(file), Charsets.UTF_8);
			String string = yaml.dumpAs(data, Tag.YAML, DumperOptions.FlowStyle.BLOCK);
			writer.write(string.replaceFirst("!!yaml\n", ""));
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (writer != null) try {
				writer.close();
			} catch (IOException ignore) {
			}
		}
	}


	public void save(OutputStream stream) {
		if (outputStream == null) return;
		Writer writer = null;
		try {
			writer = new OutputStreamWriter(outputStream, Charsets.UTF_8);
			String string = yaml.dumpAs(data, Tag.YAML, DumperOptions.FlowStyle.BLOCK);
			writer.write(string.replaceFirst("!!yaml\n", ""));
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (writer != null) try {
				writer.close();
			} catch (IOException ignore) {
			}
		}
	}


	public void save(Writer writer) {
		if (writer == null) return;
		try {
			String string = yaml.dumpAs(data, Tag.YAML, DumperOptions.FlowStyle.BLOCK);
			writer.write(string.replaceFirst("!!yaml\n", ""));
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				writer.close();
			} catch (IOException ignore) {
			}
		}
	}


	public void reload() {
		switch (configType) {
			case FILE:
				loadFile(file);
				break;
			case INPUT_STREAM:
				try {
					inputStream.reset();
					loadInputStream(inputStream);
				} catch (IOException e) {
					e.printStackTrace();
				}
				break;
			case READER:
				loadReader(reader);
				break;
			default:
				break;
		}
	}


	public void set(String node, Object value) {
		String[] array = node.replace(".", ";").split(";");
		Map<String, Object> section = data;
		for (int i = 0; i < array.length - 1; i++) {
			if (!section.containsKey(array[i]) || !(section.get(array[i]) instanceof Map)) {
				if (value == null) return;
				Map<String, Object> newMap = new HashMap<>();
				section.put(array[i], newMap);
				section = newMap;
			} else section = (Map<String, Object>) section.get(array[i]);
		}
		if (value == null) {
			section.remove(array[array.length - 1]);
			StringBuilder builder = new StringBuilder();
			for (int n = 0; n < array.length - 1; n++)
				builder.append(".").append(array[n]);
			String mapNode = builder.toString().replaceFirst(".", "");

			Map<String, Object> map = (Map<String, Object>) get(mapNode);
			if (map != null && map.isEmpty()) set(mapNode, null);
		} else section.put(array[array.length - 1], value);
	}


	public Object get(String node) {
		String[] array = node.replace(".", ";").split(";");
		Map<String, Object> section = data;
		for (int i = 0; i < array.length - 1; i++) {
			if (!section.containsKey(array[i])) return null;
			Object object = section.get(array[i]);
			if (object instanceof Map) section = (Map<String, Object>) object;
			else return null;
		}
		return section.getOrDefault(array[array.length - 1], null);

	}


	public String getString(String node) {
		Object object = get(node);
		return object != null && object instanceof String ? (String) object : "";
	}


	public String getString(String node, String def) {
		Object object = get(node);
		return object != null && object instanceof String ? (String) object : def;
	}


	public byte getByte(String node) {
		Object object = get(node);
		return object != null && object instanceof Byte ? (byte) object : 0;
	}


	public byte getByte(String node, byte def) {
		Object object = get(node);
		return object != null && object instanceof Byte ? (byte) object : def;
	}


	public short getShort(String node) {
		Object object = get(node);
		return object != null && object instanceof Short ? (short) object : 0;
	}


	public short getShort(String node, short def) {
		Object object = get(node);
		return object != null && object instanceof Short ? (short) object : def;
	}


	public int getInt(String node) {
		Object object = get(node);
		return object != null && object instanceof Integer ? (int) object : 0;
	}


	public int getInt(String node, int def) {
		Object object = get(node);
		return object != null && object instanceof Integer ? (int) object : def;
	}


	public long getLong(String node) {
		Object object = get(node);
		return object != null && object instanceof Long ? (long) object : 0;
	}


	public long getLong(String node, long def) {
		Object object = get(node);
		return object != null && object instanceof Long ? (long) object : def;
	}


	public float getFloat(String node) {
		Object object = get(node);
		return object != null && object instanceof Float ? (float) object : 0;
	}


	public float getFloat(String node, float def) {
		Object object = get(node);
		return object != null && object instanceof Float ? (float) object : def;
	}


	public double getDouble(String node) {
		Object object = get(node);
		return object != null && object instanceof Double ? (double) object : 0;
	}


	public double getDouble(String node, double def) {
		Object object = get(node);
		return object != null && object instanceof Double ? (double) object : def;
	}


	public boolean getBoolean(String node) {
		Object object = get(node);
		return object != null && object instanceof Boolean ? (Boolean) object : false;
	}


	public boolean getBoolean(String node, boolean def) {
		Object object = get(node);
		return object != null && object instanceof Boolean ? (Boolean) object : def;
	}


	public List<?> getList(String node) {
		Object val = get(node);
		return val != null && val instanceof List ? (List) val : null;
	}


	public List<?> getList(String node, List<?> def) {
		Object val = get(node);
		return (List) (val instanceof List ? val : def);
	}


	public List<String> getStringList(String path) {
		List list = this.getList(path);
		if (list == null) {
			return new ArrayList<>(0);
		} else {
			List<String> result = new ArrayList<>();
			Iterator var4 = list.iterator();

			while (true) {
				Object object;
				do {
					if (!var4.hasNext()) {
						return result;
					}

					object = var4.next();
				} while (!(object instanceof String) && !this.isPrimitiveWrapper(object));

				result.add(String.valueOf(object));
			}
		}
	}


	public List<String> getStringList(String node, List<String> def) {
		List<String> list = getStringList(node);
		return list == null || list.isEmpty() ? def : list;
	}


	private boolean isPrimitiveWrapper(Object input) {
		return input instanceof Integer ||
			input instanceof Boolean ||
			input instanceof Character ||
			input instanceof Byte ||
			input instanceof Short ||
			input instanceof Double ||
			input instanceof Long ||
			input instanceof Float;
	}


	public Set<String> getKeys() {
		return getKeys(data);
	}


	public Set<String> getKeys(boolean deep) {
		if (deep) return getKeys();
		return getKeys(data)
			.stream()
			.map(string -> string.replace(".", "/").split("/")[0])
			.collect(Collectors.toSet());
	}


	private Set<String> getKeys(Map<String, Object> map) {
		Set<String> strings = new HashSet<>();
		map.forEach((key, value) -> {
			if (value instanceof Map) strings.addAll(getKeys((Map<String, Object>) value).stream().map(string -> key + "." + string).collect(Collectors.toList()));
			else strings.add(key);
		});
		return strings;
	}

	public void checkNodes(ConfigAccessor master, boolean save) {
		master.getKeys(true).stream().filter(node -> !contains(node)).forEach(node -> set(node, master.get(node)));
		getKeys(true).stream().filter(node -> !master.contains(node)).forEach(node -> set(node, null));
		if (save) save();
	}


	public boolean contains(String node) {
		return get(node) != null;
	}

	public ConfigSection getSection(String node) {
		return new ConfigSection(this, node);
	}
}
