package com.degoos.wetsponge.config;


import com.degoos.wetsponge.WetSponge;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

public class WetSpongeConfig {

	private static ConfigAccessor config;


	public static void load() {
		ConfigAccessor jarConfig = new ConfigAccessor(getResource("wetSpongeConfig.yml"));
		config = new ConfigAccessor(new File("wetSpongeConfig.yml"));
		config.checkNodes(jarConfig, true);
		config.save();
	}


	static InputStream getResource(String filename) {
		if (filename == null) {
			throw new IllegalArgumentException("Filename cannot be null");
		} else {
			try {
				URL url = WetSponge.getServer().getClass().getClassLoader().getResource(filename);
				if (url == null) {
					return null;
				} else {
					URLConnection connection = url.openConnection();
					connection.setUseCaches(false);
					return connection.getInputStream();
				}
			} catch (IOException var4) {
				return null;
			}
		}
	}


	public static ConfigAccessor getConfig() {
		return config;
	}
}
