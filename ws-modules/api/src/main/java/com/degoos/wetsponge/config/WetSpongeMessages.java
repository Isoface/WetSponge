package com.degoos.wetsponge.config;


import com.degoos.wetsponge.text.WSText;

import java.io.File;
import java.util.Optional;

public class WetSpongeMessages {

	private static ConfigAccessor config;


	public static void load() {
		ConfigAccessor jarConfig = new ConfigAccessor(WetSpongeConfig.getResource("wetSpongeMessages.yml"));
		config = new ConfigAccessor(new File("wetSpongeMessages.yml"));
		jarConfig.getKeys(true).stream().filter(node -> !config.contains(node)).forEach(node -> config.set(node, jarConfig.getString(node)));
		config.getKeys(true).stream().filter(node -> !jarConfig.contains(node)).forEach(node -> config.set(node, null));
		config.save();
	}


	public static Optional<WSText> getMessage(String node, Object... replaces) {
		return Optional.ofNullable(config.getString(node)).map(value -> {
			for (int i = 0; i + 1 < replaces.length; i += 2)
				value = value
						.replace(replaces[i].toString(), replaces[i + 1] instanceof WSText ? ((WSText) replaces[i + 1]).toFormattingText() : replaces[i + 1].toString());
			return value;
		}).map(value -> value.replace('&', '\u00A7')).map(WSText::getByFormattingText);
	}

}
