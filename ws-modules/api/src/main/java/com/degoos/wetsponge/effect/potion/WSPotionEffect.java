package com.degoos.wetsponge.effect.potion;

import com.degoos.wetsponge.bridge.effect.potion.BridgePotionEffect;
import com.degoos.wetsponge.enums.EnumPotionEffectType;

public interface WSPotionEffect {

	public static Builder builder() {
		return BridgePotionEffect.builder();
	}

	EnumPotionEffectType getType();

	int getDuration();

	int getAmplifier();

	boolean isAmbient();

	boolean getShowParticles();

	Object getHandled();

	interface Builder {


		EnumPotionEffectType type();

		Builder type(EnumPotionEffectType type);

		int duration();

		Builder duration(int duration);

		int amplifier();

		Builder amplifier(int amplifier);

		boolean ambient();

		Builder ambient(boolean ambient);

		boolean showParticles();

		Builder showParticles(boolean showParticles);

		WSPotionEffect build();

	}

}
