package com.degoos.wetsponge.entity.explosive;

import com.degoos.wetsponge.entity.WSEntity;

public interface WSExplosive extends WSEntity {

	int setExplosionRadius();

	void setExplosionRadius(int explosionRadius);

	void detonate();
}
