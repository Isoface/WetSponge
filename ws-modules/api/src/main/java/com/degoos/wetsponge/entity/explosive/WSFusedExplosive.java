package com.degoos.wetsponge.entity.explosive;

public interface WSFusedExplosive extends WSExplosive {

	int getFuseDuration();

	void setFuseDuration(int fuseDuration);

	int getTicksRemaining();

	void setTicksRemaining(int ticksRemaining);

	boolean isPrimed();

	void prime();

	void defuse();

}
