package com.degoos.wetsponge.entity.living;


public interface WSAgeable extends WSCreature {

	/**
	 * @return the age of this entity. If age is greater than 0, the entity is an adult.
	 */
	int getAge ();

	/**
	 * Sets the age of the entity.
	 *
	 * @param age
	 * 		the age.
	 */
	void setAge (int age);

	/**
	 * @return true if this entity is a baby.
	 */
	boolean isBaby ();

	/**
	 * Transform the entity into a baby, if the boolean is true.
	 *
	 * @param baby
	 * 		true if the entity is a baby. False if the entity is an adult.
	 */
	void setBaby (boolean baby);

	/**
	 * @return true if this entity is an adult.
	 */
	boolean isAdult ();


	/**
	 * Transform the entity into an adult, if the boolean is true.
	 *
	 * @param adult
	 * 		true if the entity is an adult. False if the entity is a baby.
	 */
	void setAdult (boolean adult);

}
