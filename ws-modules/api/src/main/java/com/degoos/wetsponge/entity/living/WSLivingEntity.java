package com.degoos.wetsponge.entity.living;


import com.degoos.wetsponge.entity.WSEntity;
import com.degoos.wetsponge.entity.WSPotionEffectApplicable;
import com.degoos.wetsponge.entity.projectile.WSProjectileSource;
import com.flowpowered.math.vector.Vector3d;

import java.util.Optional;

public interface WSLivingEntity extends WSEntity, WSPotionEffectApplicable, WSProjectileSource {

	/**
	 * @return true if the entity is alive.
	 */
	boolean isAlive();

	/**
	 * Kill the entity, if alive.
	 */
	void kill();

	/**
	 * @return the entity health.
	 */
	double getHealth();

	/**
	 * Sets the entity health. If health if true, this method works as the same way as {@link WSLivingEntity#kill()}.
	 *
	 * @param health the entity health.
	 */
	void setHealth(double health);

	/**
	 * @return the max health the entity can have.
	 */
	double getMaxHealth();

	/**
	 * Sets the maximum amount of health the entity can have.
	 *
	 * @param maxHealth the maximum amount of health.
	 */
	void setMaxHealth(double maxHealth);

	/**
	 * The format of the rotation is represented by: x = pitch, y = yaw, z = roll
	 *
	 * @return the rotation of the body of this entity.
	 */
	Vector3d getRotation();

	/**
	 * Sets the body rotation of this entity. The format of the rotation is represented by: x = pitch, y = yaw, z = roll
	 *
	 * @param rotation the head rotation.
	 */
	void setRotation(Vector3d rotation);

	/**
	 * The format of the rotation is represented by: x = pitch, y = yaw, z = roll
	 *
	 * @return the rotation of the head of this entity. Due to Spigot doesn't have implemented this method, it will return the body rotation on this server item.
	 */
	Vector3d getHeadRotation();

	/**
	 * Returns the eye height of the {@link WSLivingEntity living entity}.
	 *
	 * @return the eye height.
	 */
	double getEyeHeight();

	/**
	 * Sets the head rotation of this entity. Due to Spigot doesn't have implemented this method, it will set the body rotation instead on this server item. The
	 * format of
	 * the rotation is represented by: x = pitch, y = yaw, z = roll
	 *
	 * @param rotation the head rotation.
	 */
	void setHeadRotation(Vector3d rotation);

	/**
	 * Makes the entity look at the specified target position.
	 *
	 * @param targetPosition Position to target
	 */
	void lookAt(Vector3d targetPosition);

	/**
	 * Returns the disguise of the {@link WSLivingEntity entity}, if present. Any modification to the disguise won't be fully updated. To do it use the method {@link
	 * #setDisguise(WSLivingEntity)} again.
	 *
	 * @return the disguise, if present.
	 */
	Optional<WSLivingEntity> getDisguise();

	/**
	 * Sets the disguise of the {@link WSLivingEntity entity}, of clears it if null.
	 *
	 * @param disguise the disguise.
	 */
	void setDisguise(WSLivingEntity disguise);

	/**
	 * Returns whether the {@link WSLivingEntity entity} has a disguise.
	 *
	 * @return whether the {@link WSLivingEntity entity} has a disguise.
	 */
	boolean hasDisguise();

	/**
	 * Clears the disguise of the {@link WSLivingEntity entity}, if present.
	 */
	void clearDisguise();

	/**
	 * Returns the leash holder for the {@link WSLivingEntity living entity}, if present.
	 *
	 * @return the leash holder.
	 */
	Optional<WSEntity> getLeashHolder();

	/**
	 * Sets the leash holder for the {@link WSLivingEntity living entity}, or null to remove.
	 *
	 * @param entity the leash holder or null.
	 */
	void setLeashHolder(WSEntity entity);
}
