package com.degoos.wetsponge.entity.living.aerial;

import com.degoos.wetsponge.entity.living.WSAgent;

/**
 * Represents an Ghast. A Ghast is an entity that can shoot fireballs. It appears in the Nether.
 */
public interface WSGhast extends WSAerial, WSAgent {}
