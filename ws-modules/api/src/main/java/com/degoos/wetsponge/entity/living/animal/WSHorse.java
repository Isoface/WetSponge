package com.degoos.wetsponge.entity.living.animal;

import com.degoos.wetsponge.enums.EnumHorseArmorType;
import com.degoos.wetsponge.enums.EnumHorseColor;
import com.degoos.wetsponge.enums.EnumHorseStyle;
import com.degoos.wetsponge.inventory.WSInventory;

/**
 * Represents a Horse.
 */
public interface WSHorse extends WSAbstractHorse {

	/**
	 * Returns the {@link EnumHorseColor horse color} of the {@link WSHorse horse}.
	 *
	 * @return the {@link EnumHorseColor horse color} of the {@link WSHorse horse}.
	 */
	EnumHorseColor getHorseColor();

	/**
	 * Sets the {@link EnumHorseColor horse color} of the {@link WSHorse horse}.
	 *
	 * @param horseColor the {@link EnumHorseColor horse color} of the {@link WSHorse horse}.
	 */
	void setHorseColor(EnumHorseColor horseColor);

	/**
	 * Returns the {@link EnumHorseStyle horse style} of the {@link WSHorse horse}.
	 *
	 * @return the {@link EnumHorseStyle horse style} of the {@link WSHorse horse}.
	 */
	EnumHorseStyle getHorseStyle();

	/**
	 * Sets the {@link EnumHorseStyle horse style} of the {@link WSHorse horse}.
	 *
	 * @param horseStyle the {@link EnumHorseStyle horse style} of the {@link WSHorse horse}.
	 */
	void setHorseStyle(EnumHorseStyle horseStyle);

	/**
	 * Returns the {@link EnumHorseArmorType armor item} of the {@link WSHorse horse}.
	 *
	 * @return the {@link EnumHorseArmorType armor item} of the {@link WSHorse horse}.
	 */
	EnumHorseArmorType getArmor();

	/**
	 * Sets the {@link EnumHorseArmorType armor item} of the {@link WSHorse horse}.
	 *
	 * @param armor the {@link EnumHorseArmorType armor item} of the {@link WSHorse horse}.
	 */
	void setArmor(EnumHorseArmorType armor);

	/**
	 * Returns the {@link WSInventory inventory} of the {@link WSHorse horse.}
	 *
	 * @return the {@link WSInventory inventory} of the {@link WSHorse horse.}
	 */
	WSInventory getInventory();

}
