package com.degoos.wetsponge.entity.living.animal;


import com.degoos.wetsponge.enums.EnumLlamaType;
import com.degoos.wetsponge.inventory.WSInventory;

/**
 * Represents a Llama.
 */
public interface WSLlama extends WSChestedHorse {

	/**
	 * Returns the {@link EnumLlamaType llama item} of the {@link WSLlama llama}.
	 *
	 * @return the {@link EnumLlamaType llama item}.
	 */
	EnumLlamaType getLlamaType();

	/**
	 * Sets the {@link EnumLlamaType llama item} of the {@link WSLlama llama}.
	 *
	 * @param type the {@link EnumLlamaType llama item}.
	 */
	void setLlamaType(EnumLlamaType type);

	/**
	 * Returns the {@link WSInventory inventory} of the {@link WSLlama llama}.
	 *
	 * @return the {@link WSInventory inventory}.
	 */
	WSInventory getInventory();
}
