package com.degoos.wetsponge.entity.living.animal;

import com.degoos.wetsponge.enums.EnumParrotVariant;

public interface WSParrot extends WSAnimal, WSTameable, WSSittable {

	EnumParrotVariant getVariant();

	void setVariant(EnumParrotVariant variant);

}
