package com.degoos.wetsponge.entity.living.animal;

/**
 * Represents a Polar Bear.
 */
public interface WSPolarBear extends WSAnimal {}
