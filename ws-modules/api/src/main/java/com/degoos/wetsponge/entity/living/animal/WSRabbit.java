package com.degoos.wetsponge.entity.living.animal;


import com.degoos.wetsponge.enums.EnumRabbitType;

import java.util.Optional;

/**
 * Represents a Rabbit.
 */
public interface WSRabbit extends WSAnimal {

	/**
	 * @return the rabbit item of this rabbit.
	 */
	Optional<EnumRabbitType> getRabbitType();

	/**
	 * Sets the rabbit item of this rabbit.
	 * @param rabbitType the rabbit item.
	 */
	void setRabbitType(EnumRabbitType rabbitType);

}
