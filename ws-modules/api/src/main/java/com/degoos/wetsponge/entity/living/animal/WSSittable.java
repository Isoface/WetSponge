package com.degoos.wetsponge.entity.living.animal;

/**
 * Represents an {@link com.degoos.wetsponge.entity.WSEntity entity} that can sit.
 */
public interface WSSittable {

	/**
	 * Returns whether the {@link WSSittable sittable entity} is sitting.
	 *
	 * @return whether the {@link WSSittable sittable entity} is sitting.
	 */
	boolean isSitting();

	/**
	 * Sets whether the {@link WSSittable sittable entity} is sitting.
	 *
	 * @param sitting whether the {@link WSSittable sittable entity} is sitting.
	 */
	void setSitting(boolean sitting);

}
