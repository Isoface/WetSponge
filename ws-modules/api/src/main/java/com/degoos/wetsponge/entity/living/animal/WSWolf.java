package com.degoos.wetsponge.entity.living.animal;

import com.degoos.wetsponge.enums.EnumDyeColor;

/**
 * Represents a Wolf.
 */
public interface WSWolf extends WSAnimal, WSTameable, WSSittable {

	/**
	 * Returns the collar color of the {@link WSWolf wolf}.
	 *
	 * @return the collar color of the {@link WSWolf wolf}.
	 */
	EnumDyeColor getCollarColor();

	/**
	 * Sets the collar color of the {@link WSWolf wolf}.
	 *
	 * @param collarColor the collar color of the {@link WSWolf wolf}.
	 */
	void setCollarColor(EnumDyeColor collarColor);

	/**
	 * Returns whether the {@link WSWolf wolf} is angry.
	 *
	 * @return whether the {@link WSWolf wolf} is angry.
	 */
	boolean isAngry();

	/**
	 * Sets whether the {@link WSWolf wolf} is angry.
	 *
	 * @param angry whether the {@link WSWolf wolf} is angry.
	 */
	void setAngry(boolean angry);
}
