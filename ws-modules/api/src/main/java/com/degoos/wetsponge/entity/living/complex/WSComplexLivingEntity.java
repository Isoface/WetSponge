package com.degoos.wetsponge.entity.living.complex;

import com.degoos.wetsponge.entity.living.WSLivingEntity;

import java.util.Set;

public interface WSComplexLivingEntity extends WSLivingEntity {

	Set<? extends WSComplexLivingEntityPart> getParts();

}
