package com.degoos.wetsponge.entity.living.golem;

import com.degoos.wetsponge.enums.EnumDyeColor;

public interface WSShulker extends WSGolem {

	EnumDyeColor getDyeColor();

	void setDyeColor(EnumDyeColor color);

}
