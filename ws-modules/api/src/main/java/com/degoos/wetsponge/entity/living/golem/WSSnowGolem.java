package com.degoos.wetsponge.entity.living.golem;

/**
 * Represents a Snow Golem.
 */
public interface WSSnowGolem extends WSGolem {

	/**
	 * Returns whether the {@link WSSnowGolem snow golem} has a pumpkin. In {@link com.degoos.wetsponge.enums.EnumServerVersion#MINECRAFT_OLD MINECRAFT OLD} this method
	 * always return true.
	 *
	 * @return whether the {@link WSSnowGolem snow golem} has a pumpkin.
	 */
	boolean hasPumpkinEquipped();

	/**
	 * Sets whether the {@link WSSnowGolem snow golem} has a pumpkin. In {@link com.degoos.wetsponge.enums.EnumServerVersion#MINECRAFT_OLD MINECRAFT OLD} this method has
	 * no effect.
	 *
	 * @param pumpkinEquipped whether the {@link WSSnowGolem snow golem} has a pumpkin.
	 */
	void setPumpkinEquipped(boolean pumpkinEquipped);

}
