package com.degoos.wetsponge.entity.living.monster;

public interface WSSlime extends WSMonster {

	/**
	 * @return the slime size.
	 */
	int getSize();

	/**
	 * Sets the slime size.
	 *
	 * @param size the size.
	 */
	void setSize(int size);

}
