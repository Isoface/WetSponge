package com.degoos.wetsponge.entity.living.monster;


public interface WSSpider extends WSMonster {


	/**
	 * Returns true if the spider is climbing.
	 *
	 * @return true if the blaze is climbing.
	 */
	boolean isClimbing();


	/**
	 * Sets whether the spider is climbing.
	 * @param climbing whether the spider is climbing.
	 */
	void setClimbing (boolean climbing);

}
