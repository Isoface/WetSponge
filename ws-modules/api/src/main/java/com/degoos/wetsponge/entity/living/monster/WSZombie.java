package com.degoos.wetsponge.entity.living.monster;

import com.degoos.wetsponge.entity.living.WSAgeable;
import com.degoos.wetsponge.entity.living.WSEquipable;

public interface WSZombie extends WSMonster, WSEquipable, WSAgeable {

}
