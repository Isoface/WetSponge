package com.degoos.wetsponge.entity.living.player;

import com.degoos.wetsponge.entity.living.WSEquipable;
import com.degoos.wetsponge.entity.living.WSLivingEntity;
import com.degoos.wetsponge.enums.EnumGameMode;
import com.degoos.wetsponge.inventory.WSInventory;
import com.degoos.wetsponge.item.WSItemStack;
import com.degoos.wetsponge.permission.WSPermisible;
import com.degoos.wetsponge.user.WSGameProfile;

import java.util.Optional;

public interface WSHuman extends WSLivingEntity, WSEquipable, WSPermisible {

	/**
	 * @return the food level of the player.
	 **/
	int getFoodLevel();

	/**
	 * Sets the food level of the player.
	 *
	 * @param foodLevel the food level.
	 */
	void setFoodLevel(int foodLevel);

	/**
	 * @return the player inventory.
	 */
	WSInventory getInventory();

	/**
	 * Returns the {@link WSHuman human}'s enderchest.
	 *
	 * @return the {@link WSInventory inventory} of the {@link WSHuman human}'s enderchest.
	 */
	WSInventory getEnderChestInventory();

	/**
	 * Returns the {@link WSInventory inventory} the {@link WSHuman human} has opened.
	 *
	 * @return the {@link WSInventory inventory}.
	 */
	Optional<WSInventory> getOpenInventory();

	/**
	 * Opens an inventory to the {@link WSHuman player}. If null, this method will close the current opened inventory.
	 *
	 * @param inventory the inventory, or null.
	 */
	void openInventory(WSInventory inventory);

	/**
	 * Closes the opened inventory, if present.
	 */
	void closeInventory();

	/**
	 * Returns the {@link WSItemStack item} the {@link WSHuman human} has in them cursor, if present.
	 *
	 * @return the {@link WSItemStack item} the {@link WSHuman human} has in them cursor, if present.
	 */
	Optional<WSItemStack> getItemOnCursor();

	/**
	 * Sets the {@link WSItemStack item} the {@link WSHuman human} has in them cursor. It can be null.
	 *
	 * @param itemOnCursor the {@link WSItemStack item}.
	 */
	void setItemOnCursor(WSItemStack itemOnCursor);

	/**
	 * @return the {@link EnumGameMode} of this {@link WSHuman}.
	 */
	EnumGameMode getGameMode();

	/**
	 * Sets the {@link EnumGameMode} of this {@link WSHuman}.
	 *
	 * @param gameMode the {@link EnumGameMode}.
	 */
	void setGameMode(EnumGameMode gameMode);

	/**
	 * Returns the {@link WSHuman}'s {@link WSGameProfile}.
	 *
	 * @return the {@link WSHuman}'s {@link WSGameProfile}.
	 */
	WSGameProfile getProfile();
}
