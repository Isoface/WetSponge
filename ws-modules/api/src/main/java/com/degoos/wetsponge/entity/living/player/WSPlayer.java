package com.degoos.wetsponge.entity.living.player;


import com.degoos.wetsponge.bridge.sound.BridgeSound;
import com.degoos.wetsponge.command.WSCommandSource;
import com.degoos.wetsponge.enums.EnumGameMode;
import com.degoos.wetsponge.enums.EnumSoundCategory;
import com.degoos.wetsponge.item.WSItemStack;
import com.degoos.wetsponge.material.block.WSBlockType;
import com.degoos.wetsponge.packet.WSPacket;
import com.degoos.wetsponge.particle.WSParticle;
import com.degoos.wetsponge.scoreboard.WSScoreboard;
import com.degoos.wetsponge.sound.WSSound;
import com.degoos.wetsponge.text.WSText;
import com.degoos.wetsponge.text.WSTitle;
import com.degoos.wetsponge.user.WSGameProfile;
import com.degoos.wetsponge.user.WSUser;
import com.degoos.wetsponge.world.WSLocation;
import com.degoos.wetsponge.world.WSWorld;
import com.flowpowered.math.vector.Vector3d;
import com.flowpowered.math.vector.Vector3f;
import com.flowpowered.math.vector.Vector3i;

import java.net.URI;
import java.util.Map;
import java.util.Optional;

public interface WSPlayer extends WSHuman, WSCommandSource, WSUser {

	/**
	 * @return the displayed name of this player.
	 */
	WSText getDisplayedName();

	/**
	 * Sets the displayed name of this player.
	 *
	 * @param displayedName the displayed name.
	 */
	void setDisplayedName(WSText displayedName);



	/**
	 * Spawns a particle to the player. Radius is (1, 1, 1) by default.
	 *
	 * @param location the center of the radius where particles are spawned.
	 * @param speed    the speed of the particle.
	 * @param amount   the amount of particles spawned.
	 * @param particle the particle item.
	 */
	void spawnParticle(WSLocation location, float speed, int amount, WSParticle particle);

	/**
	 * Spawns a particle to the player.
	 *
	 * @param location the center of the radius where particles are spawned.
	 * @param speed    the speed of the particle.
	 * @param amount   the amount of particles spawned.
	 * @param radius   the area where particles are spawned.
	 * @param particle the particle item.
	 */
	void spawnParticle(WSLocation location, float speed, int amount, Vector3f radius, WSParticle particle);

	/**
	 * Teleports this {@link WSPlayer} to the target {@link WSWorld}.
	 *
	 * @param world the target {@link WSWorld}.
	 */
	default void moveToWorld(WSWorld world) {
		Vector3i vector3i = world.getProperties().getSpawnPosition();
		if (vector3i == null) vector3i = new Vector3i(0, 1, 0);
		setLocation(WSLocation.of(world, vector3i));
	}

	/**
	 * Kicks this {@link WSPlayer}.
	 */
	void kick();

	/**
	 * Kicks this {@link WSPlayer}.
	 *
	 * @param message the kick message.
	 */
	void kick(WSText message);

	/**
	 * Simulates a message from this {@link WSPlayer}.
	 *
	 * @param message the message.
	 */
	void simulateMessage(WSText message);


	/**
	 * @return the {@link WSPlayer}'s {@link WSScoreboard}.
	 */
	WSScoreboard getScoreboard();

	/**
	 * Sets the {@link WSPlayer}'s {@link WSScoreboard}.
	 *
	 * @param scoreboard the new {@link WSScoreboard}.
	 */
	void setScoreboard(WSScoreboard scoreboard);

	/**
	 * Plays a sound to the {@link WSPlayer player}.
	 *
	 * @param sound  the sound.
	 * @param volume the volume.
	 */
	default void playSound(String sound, float volume) {
		playSound(sound, volume, 1);
	}

	/**
	 * Plays a sound to the {@link WSPlayer player}.
	 *
	 * @param sound  the sound.
	 * @param volume the volume.
	 * @param pitch  the pitch. Min 0.5, max 2.
	 */
	default void playSound(String sound, float volume, float pitch) {
		playSound(sound, volume, pitch, EnumSoundCategory.VOICE);
	}

	/**
	 * Plays a sound to the {@link WSPlayer player}.
	 *
	 * @param sound    the sound.
	 * @param volume   the volume.
	 * @param pitch    the pitch. Min 0.5, max 2.
	 * @param category the {@link EnumSoundCategory sound category}.
	 */
	default void playSound(String sound, float volume, float pitch, EnumSoundCategory category) {
		playSound(sound, volume, pitch, category, getLocation().toVector3d());
	}

	/**
	 * Plays a sound to the {@link WSPlayer player}.
	 *
	 * @param sound    the sound.
	 * @param volume   the volume.
	 * @param pitch    the pitch. Min 0.5, max 2.
	 * @param position the {@link Vector3d position} where the {@link WSSound sound} will be played.
	 */
	default void playSound(String sound, float volume, float pitch, Vector3d position) {
		playSound(sound, volume, pitch, EnumSoundCategory.VOICE, position);
	}

	/**
	 * Plays a sound to the {@link WSPlayer player}.
	 *
	 * @param sound    the sound.
	 * @param volume   the volume.
	 * @param pitch    the pitch. Min 0.5, max 2.
	 * @param category the {@link EnumSoundCategory sound category}.
	 * @param position the {@link Vector3d position} where the {@link WSSound sound} will be played.
	 */
	default void playSound(String sound, float volume, float pitch, EnumSoundCategory category, Vector3d position) {
		BridgeSound.playSound(sound, volume, pitch, category, position, this);
	}

	/**
	 * Plays a {@link WSSound sound} to the {@link WSPlayer player}.
	 *
	 * @param sound  the {@link WSSound sound}.
	 * @param volume the volume.
	 */
	default void playSound(WSSound sound, float volume) {
		sound.playSound(this, volume);
	}

	/**
	 * Plays a {@link WSSound sound} to the {@link WSPlayer player}.
	 *
	 * @param sound  the {@link WSSound sound}.
	 * @param volume the volume.
	 * @param pitch  the pitch. Min 0.5, max 2.
	 */
	default void playSound(WSSound sound, float volume, float pitch) {
		sound.playSound(this, volume, pitch);
	}

	/**
	 * Plays a {@link WSSound sound} to the {@link WSPlayer player}.
	 *
	 * @param sound    the {@link WSSound sound}.
	 * @param volume   the volume.
	 * @param pitch    the pitch. Min 0.5, max 2.
	 * @param category the {@link EnumSoundCategory sound category}.
	 */
	default void playSound(WSSound sound, float volume, float pitch, EnumSoundCategory category) {
		sound.playSound(this, volume, pitch, category);
	}

	/**
	 * Plays a {@link WSSound sound} to the {@link WSPlayer player}.
	 *
	 * @param sound    the {@link WSSound sound}.
	 * @param volume   the volume.
	 * @param pitch    the pitch. Min 0.5, max 2.
	 * @param position the {@link Vector3d position} where the {@link WSSound sound} will be played.
	 */
	default void playSound(WSSound sound, float volume, float pitch, Vector3d position) {
		sound.playSound(this, volume, pitch, position);
	}

	/**
	 * Plays a {@link WSSound sound} to the {@link WSPlayer player}.
	 *
	 * @param sound    the {@link WSSound sound}.
	 * @param volume   the volume.
	 * @param pitch    the pitch. Min 0.5, max 2.
	 * @param category the {@link EnumSoundCategory sound category}.
	 * @param position the {@link Vector3d position} where the {@link WSSound sound} will be played.
	 */
	default void playSound(WSSound sound, float volume, float pitch, EnumSoundCategory category, Vector3d position) {
		sound.playSound(this, volume, pitch, category, position);
	}

	/**
	 * Sends a {@link WSTitle title} to the {@link WSPlayer player}.
	 *
	 * @param title the {@link WSTitle title}.
	 */
	void sendTitle(WSTitle title);

	/**
	 * Sends a resource pack to the {@link WSPlayer player}.
	 *
	 * @param uri the {@link URI uri} of the resource pack.
	 */
	void setResourcePack(URI uri);

	/**
	 * Returns the language code of the {@link WSPlayer player}. The language code is the id of the language the {@link WSPlayer player} has selected in its minecraft
	 * client.
	 *
	 * @return the language code.
	 */
	String getLanguageCode();

	/**
	 * Returns whether the {@link WSPlayer player} is online. If the player is not online, all other methods won't work.
	 *
	 * @return whether the {@link WSPlayer player} is online.
	 */
	boolean isOnline();

	/**
	 * Returns the amount of experience the {@link WSPlayer player} has. The min amount is 0 and the max is 1.
	 *
	 * @return the amount of experience.
	 */
	float getExperience();

	/**
	 * Sets the amount of experience the {@link WSPlayer player} has. The min amount is 0 and the max is 1.
	 *
	 * @param experience the amount of experience.
	 */
	void setExperience(float experience);

	/**
	 * Returns the {@link WSPlayer player}'s experience level.
	 *
	 * @return the experience level.
	 */
	int getExperienceLevel();

	/**
	 * Sets the {@link WSPlayer player}'s experience level.
	 *
	 * @param experienceLevel the level.
	 */
	void setExperienceLevel(int experienceLevel);

	/**
	 * Returns the speed value of the {@link WSPlayer} while walking.
	 *
	 * @return the speed value.
	 */
	double getWalkingSpeed();

	/**
	 * Sets the speed value of the {@link WSPlayer} while walking.
	 *
	 * @param walkingSpeed the speed value.
	 */
	void setWalkingSpeed(double walkingSpeed);

	/**
	 * Returns the speed value of the {@link WSPlayer} while aerial.
	 *
	 * @return the speed value.
	 */
	double getFlyingSpeed();

	/**
	 * Sets the speed value of the {@link WSPlayer} while aerial.
	 *
	 * @param flyingSpeed the speed value.
	 */
	void setFlyingSpeed(double flyingSpeed);

	/**
	 * Returns whether the {@link WSPlayer player} is allowed to fly.
	 *
	 * @return whether the {@link WSPlayer player} can fly.
	 */
	boolean canFly();

	/**
	 * Sets whether the {@link WSPlayer player} if allowed to fly
	 *
	 * @param canFly whether the {@link WSPlayer player} can fly.
	 */
	void setCanFly(boolean canFly);

	/**
	 * Returns whether the {@link WSPlayer player} is aerial.
	 *
	 * @return whether the {@link WSPlayer player} is aerial.
	 */
	boolean isFlying();

	/**
	 * Sets whether the {@link WSPlayer player} is aerial.
	 *
	 * @param flying whether the {@link WSPlayer player} is aerial.
	 */
	void setFlying(boolean flying);

	/**
	 * Sends a fake block to the {@link WSPlayer player}.
	 *
	 * @param location the block location.
	 * @param material the {@link WSBlockType material} of the block.
	 */
	void addFakeBlock(WSLocation location, WSBlockType material);

	/**
	 * Returns the fake block by its {@link WSLocation location}, if present.
	 *
	 * @param location the {@link WSLocation location}.
	 * @return the fake block, if present.
	 */
	Optional<WSBlockType> getFakeBlock(WSLocation location);

	/**
	 * Returns all fake blocks the {@link WSPlayer player} has set. The fake blocks will be removed when the {@link WSPlayer player} disconnects.
	 *
	 * @return a immutable {@link Map map} with all fake blocks.
	 */
	Map<WSLocation, WSBlockType> getFakeBlocks();

	/**
	 * Returns whether the {@link WSPlayer player} has a fake block in the target {@link WSLocation location}.
	 *
	 * @param location the {@link WSLocation locaiton}.
	 * @return whether the {@link WSPlayer player} has a fake block in the target {@link WSLocation location}.
	 */
	boolean containsFakeBlock(WSLocation location);

	/**
	 * Re-sends a fake block to the {@link WSPlayer player}, if present.
	 *
	 * @param location the location.
	 */
	void refreshFakeBlock(WSLocation location);

	/**
	 * Removes the fake block of the {@link WSPlayer player}, if present.
	 *
	 * @param location the location.
	 */
	void removeFakeBlock(WSLocation location);

	/**
	 * Removes all fake blocks of the {@link WSPlayer player}.
	 */
	void clearFakeBlocks();

	/**
	 * Sends a {@link WSPacket packet} to the {@link WSPlayer player}.
	 *
	 * @param packet the {@link WSPacket packet}.
	 */
	void sendPacket(WSPacket packet);

	/**
	 * Returns the selected hotbar slot of the {@link WSPlayer player}.
	 *
	 * @return the selected hotbar slot of the {@link WSPlayer player}.
	 */
	int getSelectedHotbarSlot();

	/**
	 * Sets the selected hotbar slot of the {@link WSPlayer player}.
	 *
	 * @param slot the slot.
	 */
	void setSelectedHotbarSlot(int slot);

	/**
	 * Sends a sign change to the {@link WSPlayer player}. The sign can be a fake block.
	 *
	 * @param sign  the sign {@link WSLocation location}.
	 * @param lines the new lines of the sign.
	 */
	void sendSignChange(WSLocation sign, WSText[] lines);

	/**
	 * Gets the ping from the {@link WSPlayer player}.
	 *
	 * @return ping as an integer number
	 */
	int getPing();
}
