package com.degoos.wetsponge.entity.projectile;

import com.degoos.wetsponge.enums.EnumPickupStatus;

public interface WSArrow extends WSProjectile {

	double getDamage();

	void setDamage(double damage);

	int getKnockbackStrength();

	void setKnockbackStrength(int knockbackStrength);

	EnumPickupStatus getPickupStatus();

	void setPickupStatus(EnumPickupStatus status);

}
