package com.degoos.wetsponge.enums;

import java.util.Arrays;
import java.util.Optional;

public enum EnumArtType {

    ALBAN(2),
    AZTEC(1),
    AZTEC_2(3),
    BOMB(4),
    BURNING_SKULL(23),
    BUST(15),
    COURBET(8),
    CREEBET(11),
    DONKEY_KONG(25),
    FIGHTERS(20),
    GRAHAM(13),
    KEBAB(0),
    MATCH(14),
    PIGSCENE(22),
    PLANT(5),
    POINTER(21),
    POOL(7),
    SEA(9),
    SKELETON(24),
    SKULL_AND_ROSES(18),
    STAGE(16),
    SUNSET(10),
    VOID(17),
    WANDERER(12),
    WASTELAND(6),
    WITHER(19);

    private int id;

    EnumArtType(int id) {
        this.id = id;
    }

    public static Optional<EnumArtType> getByName(String name) {
        return Arrays.stream(values()).filter(target -> target.name().equalsIgnoreCase(name)).findAny();
    }

    public static Optional<EnumArtType> getById(int id) {
        return Arrays.stream(values()).filter(target -> target.getId() == id).findAny();
    }

    public int getId() {
        return id;
    }

}
