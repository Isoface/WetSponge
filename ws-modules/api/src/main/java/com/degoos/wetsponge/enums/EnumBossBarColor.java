package com.degoos.wetsponge.enums;

import java.util.Arrays;
import java.util.Optional;

public enum EnumBossBarColor {

    BLUE, GREEN, PINK, PURPLE, RED, WHITE, YELLOW;

    public static Optional<EnumBossBarColor> getByName(String name) {
        return Arrays.stream(values()).filter(target -> target.name().equalsIgnoreCase(name)).findAny();
    }

}
