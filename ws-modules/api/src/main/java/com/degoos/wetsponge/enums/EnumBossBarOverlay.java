package com.degoos.wetsponge.enums;

import java.util.Arrays;
import java.util.Optional;

public enum EnumBossBarOverlay {

    NOTCHED_6("SEGMENTED_6"),
    NOTCHED_10("SEGMENTED_10"),
    NOTCHED_12("SEGMENTED_12"),
    NOTCHED_20("SEGMENTED_20"),
    PROGRESS("SOLID");


    private String spigotName;

    EnumBossBarOverlay(String spigotName) {
        this.spigotName = spigotName;
    }

    public static Optional<EnumBossBarOverlay> getBySpongeName(String name) {
        return Arrays.stream(values()).filter(target -> target.getSpongeName().equalsIgnoreCase(name)).findAny();
    }

    public static Optional<EnumBossBarOverlay> getBySpigotName(String name) {
        return Arrays.stream(values()).filter(target -> target.getSpigotName().equalsIgnoreCase(name)).findAny();
    }

    public String getSpongeName() {
        return name();
    }

    public String getSpigotName() {
        return spigotName;
    }
}
