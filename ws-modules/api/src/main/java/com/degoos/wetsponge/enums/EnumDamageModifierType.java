package com.degoos.wetsponge.enums;

import java.util.Arrays;
import java.util.Optional;

public enum EnumDamageModifierType {

    ABSORPTION("ABSORPTION"),
    ARMOR("ARMOR"),
    ARMOR_ENCHANTMENT("ARMOR"),
    ATTACK_COOLDOWN("ARMOR"),
    BLOCKING("BLOCKING"),
    CRITICAL_HIT("MAGIC"),
    DEFENSIVE_POTION_EFFECT("MAGIC"),
    DIFFICULTY("BASE"),
    HARD_HAT("HARD_HAT"),
    MAGIC("MAGIC"),
    NEGATIVE_POTION_EFFECT("MAGIC"),
    OFFENSIVE_POTION_EFFECT("MAGIC"),
    SHIELD("BLOCKING"),
    SWEAPING("HARD_HAT"),
    WEAPON_ENCHANTMENT("MAGIC");

    private String spigotName;

    EnumDamageModifierType(String spigotName) {
        this.spigotName = spigotName;
    }

    public String getSpigotName() {
        return spigotName;
    }

    public static Optional<EnumDamageModifierType> getBySpongeName(String name) {
        return Arrays.stream(values()).filter(target -> target.name().equalsIgnoreCase(name)).findAny();
    }

    public static Optional<EnumDamageModifierType> getBySpigotName(String name) {
        return Arrays.stream(values()).filter(target -> target.getSpigotName().equalsIgnoreCase(name)).findAny();
    }
}
