package com.degoos.wetsponge.enums;

import java.util.Arrays;
import java.util.Optional;

public enum EnumDamageType {

	ATTACK("ENTITY_ATTACK", "LIGHTNING", "FALLING_BLOCK", "DRAGON_BREATH"),
	CONTACT("CONTACT", "FLY_INTO_WALL", "HOT_FLOOR"),
	CUSTOM("CUSTOM"),
	DROWN("DROWNING"),
	EXPLOSIVE("BLOCK_EXPLOSION", "ENTITY_EXPLOSION"),
	FALL("FALL"),
	FIRE("FIRE", "FIRE_TICK"),
	GENERIC("SUICIDE", "CRAMMING"),
	HUNGER("STARVATION"),
	MAGIC("POISON", "MAGIC", "WITHER", "THORNS"),
	MAGMA("LAVA", "MELTING"),
	PROJECTILE("PROJECTILE"),
	SUFFOCATE("SUFFOCATION"),
	SWEEPING_ATTACK("ENTITY_SWEEP_ATTACK"),
	VOID("VOID");

	private String mainSpigotName;
	private String[] spigotNames;

	EnumDamageType(String... spigotNames) {
		mainSpigotName = spigotNames[0];
		this.spigotNames = spigotNames;
	}

	public String getMainSpigotName() {
		return mainSpigotName;
	}

	public String[] getSpigotNames() {
		return spigotNames.clone();
	}

	public static Optional<EnumDamageType> getBySpongeName(String name) {
		return Arrays.stream(values()).filter(target -> target.name().equalsIgnoreCase(name)).findAny();
	}

	public static Optional<EnumDamageType> getBySpigotName(String name) {
		return Arrays.stream(values()).filter(target -> Arrays.stream(target.spigotNames).anyMatch(spigotName -> spigotName.equalsIgnoreCase(name))).findAny();
	}

}
