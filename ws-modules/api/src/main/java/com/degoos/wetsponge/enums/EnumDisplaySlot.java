package com.degoos.wetsponge.enums;

import java.util.Arrays;
import java.util.Optional;

public enum EnumDisplaySlot {

    BELOW_NAME("BELOW_NAME", "BELOW_NAME"),
    LIST("PLAYER_LIST", "LIST"),
    SIDEBAR("SIDEBAR", "SIDEBAR");

    private String spigotName, spongeName;

    EnumDisplaySlot(String spigotName, String spongeName) {
        this.spigotName = spigotName;
        this.spongeName = spongeName;
    }

    public static Optional<EnumDisplaySlot> getBySpigotName(String name) {
        if (name == null) return Optional.empty();
        return Arrays.stream(values()).filter(target -> name.equalsIgnoreCase(target.getSpigotName())).findAny();
    }

    public static Optional<EnumDisplaySlot> getBySpongeName(String name) {
        if (name == null) return Optional.empty();
        return Arrays.stream(values()).filter(target -> name.equalsIgnoreCase(target.getSpongeName())).findAny();
    }

    public String getSpigotName() {
        return spigotName;
    }

    public String getSpongeName() {
        return spongeName;
    }
}
