package com.degoos.wetsponge.enums;


import java.util.Arrays;
import java.util.Optional;

public enum EnumEquipType {

	HELMET("HEAD", 5), CHESTPLATE("CHEST", 4), LEGGINGS("LEGS", 3), BOOTS("FEET", 2), MAIN_HAND("MAINHAND", 0), OFF_HAND("OFFHAND", 1);

	private String minecraftName;
	private int value;

	EnumEquipType(String minecraftName, int value) {
		this.minecraftName = minecraftName;
		this.value = value;
	}

	public String getMinecraftName() {
		return minecraftName;
	}

	public int getValue() {
		return value;
	}

	public static Optional<EnumEquipType> getByMinecraftName(String minecraftName) {
		return Arrays.stream(values()).filter(target -> target.getMinecraftName().equals(minecraftName)).findAny();
	}

	public static Optional<EnumEquipType> getByValue(int value) {
		return Arrays.stream(values()).filter(target -> target.getValue() == value).findAny();
	}
}
