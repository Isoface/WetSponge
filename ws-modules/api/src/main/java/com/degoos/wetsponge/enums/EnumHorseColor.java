package com.degoos.wetsponge.enums;

import java.util.Arrays;
import java.util.Optional;

public enum EnumHorseColor {

	BLACK, BROWN, CHESTNUT, CREAMY, DARK_BROWN, GRAY, WHITE;

	public static Optional<EnumHorseColor> getByName(String name) {
		return Arrays.stream(values()).filter(target -> target.name().equalsIgnoreCase(name)).findAny();
	}

}
