package com.degoos.wetsponge.enums;

import java.util.Arrays;
import java.util.Optional;

public enum EnumHorseStyle {

	BLACK_DOTS, NONE, WHITE, WHITEFIELD, WHITE_DOTS;

	public static Optional<EnumHorseStyle> getByName(String name) {
		return Arrays.stream(values()).filter(target -> target.name().equalsIgnoreCase(name)).findAny();
	}

}
