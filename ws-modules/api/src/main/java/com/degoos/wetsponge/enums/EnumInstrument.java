package com.degoos.wetsponge.enums;

import java.util.Arrays;
import java.util.Optional;

public enum EnumInstrument {

	PIANO(0),
	BASS_DRUM(1),
	SNARE_DRUM(2),
	STICKS(3),
	BASS_GUITAR(4),
	FLUTE(5),
	BELL(6),
	GUITAR(7),
	CHIME(8),
	XYLOPHONE(9);

	private int type;

	EnumInstrument(int type) {
		this.type = type;
	}

	public int getType() {
		return type;
	}

	public static Optional<EnumInstrument> getByType(int type) {
		return Arrays.stream(values()).filter(target -> target.type == type).findAny();
	}
}
