package com.degoos.wetsponge.enums;

import java.util.Arrays;
import java.util.Optional;

public enum EnumOcelotType {

    WILD_OCELOT, BLACK_CAT, RED_CAT, SIAMESE_CAT;

    public static Optional<EnumOcelotType> getByName(String name) {
        return Arrays.stream(values()).filter(target -> target.name().equalsIgnoreCase(name)).findAny();
    }

}
