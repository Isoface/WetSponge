package com.degoos.wetsponge.enums;

public enum EnumPlayerListItemAction {

	ADD_PLAYER,
	UPDATE_GAME_MODE,
	UPDATE_LATENCY,
	UPDATE_DISPLAY_NAME,
	REMOVE_PLAYER

}
