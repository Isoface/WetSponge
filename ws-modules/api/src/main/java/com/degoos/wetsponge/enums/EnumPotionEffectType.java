package com.degoos.wetsponge.enums;

import java.util.Arrays;
import java.util.Optional;

public enum EnumPotionEffectType {

    ABSORPTION(22, "minecraft:absorption"),
    BLINDNESS(15, "minecraft:blindness"),
    FIRE_RESISTANCE(12, "minecraft:fire_resistance"),
    GLOWING(24, "minecraft:glowing"),
    HASTE(3, "minecraft:haste"),
    HEALTH_BOOST(21, "minecraft:health_boost"),
    HUNGER(17, "minecraft:hunger"),
    INSTANT_DAMAGE(7, "minecraft:instant_damage"),
    INSTANT_HEALTH(6, "minecraft:instant_health"),
    INVISIBILITY(14, "minecraft:invisibility"),
    JUMP_BOOST(8, "minecraft:jump_boost"),
    LEVITATION(25, "minecraft:levitation"),
    LUCK(26, "minecraft:luck"),
    MINING_FATIGUE(4, "minecraft:mining_fatigue"),
    NAUSEA(9, "minecraft:nausea"),
    NIGHT_VISION(16, "minecraft:night_vision"),
    POISON(19, "minecraft:poison"),
    REGENERATION(10, "minecraft:regeneration"),
    RESISTANCE(11, "minecraft:resistance"),
    SATURATION(23, "minecraft:saturation"),
    SLOWNESS(2, "minecraft:slowness"),
    SPEED(1, "minecraft:speed"),
    STRENGTH(5, "minecraft:strength"),
    UNLUCK(27, "minecraft:unluck"),
    WATER_BREATHING(13, "minecraft:water_breathing"),
    WEAKNESS(18, "minecraft:weakness"),
    WITHER(20, "minecraft:wither");

    private int value;
    private String id;

    EnumPotionEffectType(int value, String id) {
        this.value = value;
        this.id = id;
    }


    public int getValue() {
        return value;
    }

    public String getId() {
        return id;
    }

    public static Optional<EnumPotionEffectType> getByValue(int value) {
        return Arrays.stream(values()).filter(target -> target.getValue() == value).findAny();
    }

    public static Optional<EnumPotionEffectType> getById(String id) {
        return Arrays.stream(values()).filter(target -> target.getId().equalsIgnoreCase(id)).findAny();
    }

}
