package com.degoos.wetsponge.enums;

import java.util.Arrays;
import java.util.Optional;

public enum EnumUseEntityType {

	INTERACT(0), ATTACK(1), INTERACT_AT(2);

	private int id;

	EnumUseEntityType(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public static Optional<EnumUseEntityType> getById(int id) {
		return Arrays.stream(values()).filter(target -> target.getId() == id).findAny();
	}
}
