package com.degoos.wetsponge.enums;

import java.util.Arrays;
import java.util.Optional;

public enum EnumVillagerProfession {

    ARMORER("BLACKSMITH"),
    BUTCHER("FARMER"),
    CARTOGRAPHER("LIBRARIAN"),
    CLERIC("PRIEST"),
    FARMER("FARMER"),
    FISHERMAN("FARMER"),
    FLETCHER("FARMER"),
    LEATHERWORKER("FARMER"),
    LIBRARIAN("LIBRARIAN"),
    SHEPHERD("FARMER"),
    TOOL_SMITH("BLACKSMITH"),
    WEAPON_SMITH("BLACKSMITH");

    private String spigotName;

    EnumVillagerProfession(String spigotName) {
        this.spigotName = spigotName;
    }

    public String getSpongeName() {
        return name();
    }

    public String getSpigotName() {
        return spigotName;
    }

    public static Optional<EnumVillagerProfession> getBySpongeName(String name) {
        return Arrays.stream(values()).filter(target -> target.name().equalsIgnoreCase(name)).findAny();
    }

    public static Optional<EnumVillagerProfession> getBySpigotName(String name) {
        return Arrays.stream(values()).filter(target -> target.getSpigotName().equalsIgnoreCase(name)).findAny();
    }
}
