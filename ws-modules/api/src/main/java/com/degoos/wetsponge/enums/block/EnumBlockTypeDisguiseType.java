package com.degoos.wetsponge.enums.block;


import java.util.Arrays;
import java.util.Optional;

public enum EnumBlockTypeDisguiseType {

    STONE(0), COBBLESTONE(1), STONE_BRICK(2), MOSSY_STONE_BRICK(3), CRACKED_STONE_BRICK(4), CHISELED_STONE_BRICK(5);

    private int value;

    EnumBlockTypeDisguiseType(int value) {
        this.value = value;
    }


    public static Optional<EnumBlockTypeDisguiseType> getByValue(int value) {
        return Arrays.stream(values()).filter(target -> target.getValue() == value).findAny();
    }


    public static Optional<EnumBlockTypeDisguiseType> getByName(String name) {
        return Arrays.stream(values()).filter(target -> target.name().equalsIgnoreCase(name)).findAny();
    }


    public int getValue() {
        return value;
    }

}
