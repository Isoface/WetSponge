package com.degoos.wetsponge.enums.block;


import java.util.Arrays;
import java.util.Optional;

public enum EnumBlockTypeDoublePlantType {

	SUNFLOWER(0, "SUNFLOWER"), LILAC(1, "SYRINGA"), DOUBLE_TALLGRASS(2, "GRASS"), LARGE_FERN(3, "FERN"), ROSE_BUSH(4, "ROSE"), PEONY(5, "PAEONIA");

	private int value;
	private String spongeName;

	EnumBlockTypeDoublePlantType(int value, String spongeName) {
		this.value = value;
		this.spongeName = spongeName;
	}


	public static Optional<EnumBlockTypeDoublePlantType> getByValue(int value) {
		return Arrays.stream(values()).filter(dirtType -> dirtType.getValue() == value).findAny();
	}

	public static Optional<EnumBlockTypeDoublePlantType> getByName(String name) {
		return Arrays.stream(values()).filter(dirtType -> dirtType.name().equalsIgnoreCase(name)).findAny();
	}

	public static Optional<EnumBlockTypeDoublePlantType> getBySpongeName(String name) {
		return Arrays.stream(values()).filter(dirtType -> dirtType.spongeName.equalsIgnoreCase(name)).findAny();
	}


	public int getValue() {
		return value;
	}

	public String getSpongeName() {
		return spongeName;
	}
}
