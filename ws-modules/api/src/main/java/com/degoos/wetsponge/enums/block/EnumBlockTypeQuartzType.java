package com.degoos.wetsponge.enums.block;


import java.util.Arrays;
import java.util.Optional;

public enum EnumBlockTypeQuartzType {

	NORMAL(0), CHISELED(1), PILLAR(2);

	private int value;


	EnumBlockTypeQuartzType(int value) {
		this.value = value;
	}


	public static Optional<EnumBlockTypeQuartzType> getByValue(int value) {
		return Arrays.stream(values()).filter(dirtType -> dirtType.getValue() == value).findAny();
	}

	public static Optional<EnumBlockTypeQuartzType> getByName(String name) {
		return Arrays.stream(values()).filter(dirtType -> dirtType.name().equalsIgnoreCase(name)).findAny();
	}


	public int getValue() {
		return value;
	}
}
