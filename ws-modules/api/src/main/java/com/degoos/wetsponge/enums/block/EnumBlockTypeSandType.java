package com.degoos.wetsponge.enums.block;


import java.util.Arrays;
import java.util.Optional;

public enum EnumBlockTypeSandType {

	NORMAL(0), RED(1);

	private int value;


	EnumBlockTypeSandType(int value) {
		this.value = value;
	}


	public static Optional<EnumBlockTypeSandType> getByValue (int value) {
		return Arrays.stream(values()).filter(sandType -> sandType.getValue() == value).findAny();
	}

	public static Optional<EnumBlockTypeSandType> getByName (String name) {
		return Arrays.stream(values()).filter(sandType -> sandType.name().equalsIgnoreCase(name)).findAny();
	}


	public int getValue () {
		return value;
	}

}
