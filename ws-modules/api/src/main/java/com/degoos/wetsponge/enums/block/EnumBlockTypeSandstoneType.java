package com.degoos.wetsponge.enums.block;


import java.util.Arrays;
import java.util.Optional;

public enum EnumBlockTypeSandstoneType {

	DEFAULT(0), CHISELED(1), SMOOTH(2);

	private int value;


	EnumBlockTypeSandstoneType(int value) {
		this.value = value;
	}


	public static Optional<EnumBlockTypeSandstoneType> getByValue (int value) {
		return Arrays.stream(values()).filter(sandstoneType -> sandstoneType.getValue() == value).findAny();
	}


	public static Optional<EnumBlockTypeSandstoneType> getByName (String name) {
		return Arrays.stream(values()).filter(sandstoneType -> sandstoneType.name().equalsIgnoreCase(name)).findAny();
	}


	public int getValue () {
		return value;
	}

}
