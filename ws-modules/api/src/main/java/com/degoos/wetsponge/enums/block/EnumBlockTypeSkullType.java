package com.degoos.wetsponge.enums.block;

import java.util.Arrays;
import java.util.Optional;

public enum EnumBlockTypeSkullType {

	CREEPER("CREEPER", "CREEPER", 4, "creeper"),
	ENDER_DRAGON("ENDER_DRAGON", "DRAGON", 5, "dragon"),
	PLAYER("PLAYER", "PLAYER", 3, "player"),
	SKELETON("SKELETON", "SKELETON", 0, "skeleton"),
	WITHER_SKELETON("WITHER_SKELETON", "WITHER", 1, "wither_skeleton"),
	ZOMBIE("ZOMBIE", "ZOMBIE", 2, "zombie");

	private String spigotName, spongeName, minecraftName;
	private int value;

	EnumBlockTypeSkullType(String spigotName, String spongeName, int value, String minecraftName) {
		this.spigotName = spigotName;
		this.spongeName = spongeName;
		this.value = value;
		this.minecraftName = minecraftName;
	}

	public static Optional<EnumBlockTypeSkullType> getBySpigotName(String name) {
		if (name == null) return Optional.empty();
		return Arrays.stream(values()).filter(target -> name.equalsIgnoreCase(target.getSpigotName())).findAny();
	}

	public static Optional<EnumBlockTypeSkullType> getBySpongeName(String name) {
		if (name == null) return Optional.empty();
		return Arrays.stream(values()).filter(target -> name.equalsIgnoreCase(target.getSpongeName())).findAny();
	}

	public static Optional<EnumBlockTypeSkullType> getByMinecraftName(String name) {
		if (name == null) return Optional.empty();
		return Arrays.stream(values()).filter(target -> name.equalsIgnoreCase(target.getMinecraftName())).findAny();
	}


	public static Optional<EnumBlockTypeSkullType> getByValue(int value) {
		return Arrays.stream(values()).filter(target -> value == target.getValue()).findAny();
	}

	public String getSpigotName() {
		return spigotName;
	}

	public String getSpongeName() {
		return spongeName;
	}

	public String getMinecraftName() {
		return minecraftName;
	}

	public int getValue() {
		return value;
	}
}
