package com.degoos.wetsponge.enums.block;

import java.util.Arrays;
import java.util.Optional;

public enum EnumBlockTypeSlabType {

	STONE,
	SANDSTONE,
	PETRIFIED_OAK,
	COBBLESTONE,
	BRICK,
	STONE_BRICK,
	NETHER_BRICK,
	QUARTZ,
	RED_SANDSTONE,
	PURPUR,
	OAK,
	SPRUCE,
	BIRCH,
	JUNGLE,
	ACACIA,
	DARK_OAK,
	PRISMARINE,
	PRISMARINE_BRICK,
	DARK_PRISMARINE;

	public static Optional<EnumBlockTypeSlabType> getByName(String name) {
		return Arrays.stream(values()).filter(target -> target.name().equalsIgnoreCase(name)).findAny();
	}

}
