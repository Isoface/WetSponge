package com.degoos.wetsponge.enums.block;


import java.util.Arrays;
import java.util.Optional;

public enum EnumBlockTypeStoneBrickType {

    DEFAULT(0), MOSSY(1), CRACKED(2), CHISELED(3);

    private int value;

    EnumBlockTypeStoneBrickType(int value) {
        this.value = value;
    }


    public static Optional<EnumBlockTypeStoneBrickType> getByValue(int value) {
        return Arrays.stream(values()).filter(target -> target.getValue() == value).findAny();
    }


    public static Optional<EnumBlockTypeStoneBrickType> getByName(String name) {
        return Arrays.stream(values()).filter(target -> target.name().equalsIgnoreCase(name)).findAny();
    }


    public int getValue() {
        return value;
    }

}
