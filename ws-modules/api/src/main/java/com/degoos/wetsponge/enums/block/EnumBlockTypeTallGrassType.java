package com.degoos.wetsponge.enums.block;


import java.util.Arrays;
import java.util.Optional;

public enum EnumBlockTypeTallGrassType {

	DEAD_BUSH(0), TALL_GRASS(1), FERN(2);

	private int value;


	EnumBlockTypeTallGrassType(int value) {
		this.value = value;
	}


	public static Optional<EnumBlockTypeTallGrassType> getByValue (int value) {
		return Arrays.stream(values()).filter(tallGrassType -> tallGrassType.getValue() == value).findAny();
	}


	public static Optional<EnumBlockTypeTallGrassType> getByName (String name) {
		return Arrays.stream(values()).filter(tallGrassType -> tallGrassType.name().equalsIgnoreCase(name)).findAny();
	}


	public int getValue () {
		return value;
	}
}
