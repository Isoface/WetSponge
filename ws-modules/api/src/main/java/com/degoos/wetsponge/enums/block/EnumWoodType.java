package com.degoos.wetsponge.enums.block;


import java.util.Arrays;
import java.util.Optional;

public enum EnumWoodType {

	OAK(0), SPRUCE(1), BIRCH(2), JUNGLE(3), ACACIA(4), DARK_OAK(5);

	private int value;


	EnumWoodType (int value) {
		this.value = value;
	}


	public static Optional<EnumWoodType> getByValue (int value) {
		return Arrays.stream(values()).filter(woodType -> woodType.getValue() == value).findAny();
	}


	public static Optional<EnumWoodType> getByName (String name) {
		return Arrays.stream(values()).filter(woodType -> woodType.name().equalsIgnoreCase(name)).findAny();
	}


	public int getValue () {
		return value;
	}
}
