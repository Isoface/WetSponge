package com.degoos.wetsponge.enums.item;

import java.util.Arrays;
import java.util.Optional;

public enum EnumItemTypeGoldenAppleType {

    GOLDEN_APPLE(0), ENCHANTED_GOLDEN_APPLE(1);

    private int value;

    EnumItemTypeGoldenAppleType(int value) {
        this.value = value;
    }

    public static Optional<EnumItemTypeGoldenAppleType> getByValue(int value) {
        return Arrays.stream(values()).filter(target -> target.getValue() == value).findAny();
    }


    public static Optional<EnumItemTypeGoldenAppleType> getByName(String name) {
        return Arrays.stream(values()).filter(target -> target.name().equalsIgnoreCase(name)).findAny();
    }


    public int getValue() {
        return value;
    }
}
