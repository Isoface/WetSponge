package com.degoos.wetsponge.event.command;


import com.degoos.wetsponge.event.WSCancellable;
import com.degoos.wetsponge.command.WSCommandSource;
import com.degoos.wetsponge.event.WSEvent;

import java.util.List;

public class WSTabCompleteChatEvent extends WSEvent implements WSCancellable {

	private WSCommandSource source;
	private String          message;
	private List<String>    completions;
	private boolean         cancelled;


	public WSTabCompleteChatEvent (WSCommandSource source, String message, List<String> completions) {
		this.source = source;
		this.message = message;
		this.completions = completions;
		this.cancelled = false;
	}


	public WSCommandSource getSource () {
		return source;
	}


	public String getMessage () {
		return message;
	}


	public List<String> getCompletions () {
		return completions;
	}


	public void setCompletions (List<String> completions) {
		this.completions = completions;
	}


	@Override
	public boolean isCancelled () {
		return cancelled;
	}


	@Override
	public void setCancelled (boolean cancelled) {
		this.cancelled = cancelled;
	}
}
