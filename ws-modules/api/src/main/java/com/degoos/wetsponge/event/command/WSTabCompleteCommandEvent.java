package com.degoos.wetsponge.event.command;


import com.degoos.wetsponge.event.WSCancellable;
import com.degoos.wetsponge.event.WSEvent;
import com.degoos.wetsponge.command.WSCommandSource;

import java.util.List;

public class WSTabCompleteCommandEvent extends WSEvent implements WSCancellable {

	private WSCommandSource source;
	private String          command;
	private String[]        arguments;
	private List<String>    completions;
	private boolean         cancelled;


	public WSTabCompleteCommandEvent (WSCommandSource source, String command, String[] arguments, List<String> completions) {
		this.source = source;
		this.command = command;
		this.arguments = arguments;
		this.completions = completions;
		this.cancelled = false;
	}


	public WSCommandSource getSource () {
		return source;
	}


	public void setSource (WSCommandSource source) {
		this.source = source;
	}


	public String getCommand () {
		return command;
	}


	public String[] getArguments () {
		return arguments;
	}


	public List<String> getCompletions () {
		return completions;
	}


	public void setCompletions (List<String> completions) {
		this.completions = completions;
	}


	@Override
	public boolean isCancelled () {
		return cancelled;
	}


	@Override
	public void setCancelled (boolean cancelled) {
		this.cancelled = cancelled;
	}
}
