package com.degoos.wetsponge.event.entity;

import com.degoos.wetsponge.block.WSBlock;
import com.degoos.wetsponge.entity.WSEntity;
import com.degoos.wetsponge.entity.modifier.WSDamageModifier;
import com.degoos.wetsponge.enums.EnumDamageType;
import java.util.Set;

public class WSEntityDamageByBlockEvent extends WSEntityDamageEvent {

	private WSBlock damager;

	public WSEntityDamageByBlockEvent(WSEntity entity, double baseDamage, Set<WSDamageModifier> damageModifiers, EnumDamageType damageType, WSBlock damager,
		double firstFinalDamage) {
		super(entity, baseDamage, damageModifiers, damageType, firstFinalDamage);
		this.damager = damager;
	}

	public WSBlock getDamager() {
		return damager;
	}
}
