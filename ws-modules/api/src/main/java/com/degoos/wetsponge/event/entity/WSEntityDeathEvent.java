package com.degoos.wetsponge.event.entity;

import com.degoos.wetsponge.entity.WSEntity;
import com.degoos.wetsponge.event.WSCancellable;
import com.degoos.wetsponge.text.WSText;

public class WSEntityDeathEvent extends WSEntityDestructEvent {

	public WSEntityDeathEvent(WSEntity player) {
		super(player);
	}

	public static class Pre extends WSEntityDeathEvent implements WSCancellable {

		private boolean cancelled;

		public Pre(WSEntity player) {
			super(player);
		}

		@Override
		public boolean isCancelled() {
			return cancelled;
		}

		@Override
		public void setCancelled(boolean cancelled) {
			this.cancelled = cancelled;
		}
	}

	public static class Post extends WSEntityDeathEvent {

		private boolean keepInventory;
		private WSText deathMessage;

		public Post(WSEntity player, boolean keepInventory, WSText deathMessage) {
			super(player);
			this.keepInventory = keepInventory;
			this.deathMessage = deathMessage;
		}

		public boolean keepInventory() {
			return keepInventory;
		}

		public void setKeepInventory(boolean keepInventory) {
			this.keepInventory = keepInventory;
		}

		public WSText getDeathMessage() {
			return deathMessage;
		}

		public void setDeathMessage(WSText deathMessage) {
			this.deathMessage = deathMessage;
		}
	}
}
