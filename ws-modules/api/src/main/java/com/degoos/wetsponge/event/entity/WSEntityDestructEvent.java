package com.degoos.wetsponge.event.entity;

import com.degoos.wetsponge.entity.WSEntity;

public class WSEntityDestructEvent extends WSEntityEvent {

    public WSEntityDestructEvent(WSEntity entity) {
        super(entity);
    }

}
