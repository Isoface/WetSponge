package com.degoos.wetsponge.event.entity;

import com.degoos.wetsponge.entity.WSEntity;

public class WSEntityMountEvent extends WSEntityRideEvent {

	public WSEntityMountEvent(WSEntity entity, WSEntity vehicle) {
		super(entity, vehicle);
	}
}
