package com.degoos.wetsponge.event.entity;

import com.degoos.wetsponge.entity.WSEntity;
import com.degoos.wetsponge.event.WSCancellable;

public class WSEntitySpawnEvent extends WSEntityEvent implements WSCancellable {

    private boolean cancelled;

    public WSEntitySpawnEvent(WSEntity entity) {
        super(entity);
        cancelled = false;
    }

    @Override
    public boolean isCancelled() {
        return cancelled;
    }

    @Override
    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }
}
