package com.degoos.wetsponge.event.entity.player;

import com.degoos.wetsponge.event.entity.WSEntityEvent;
import com.degoos.wetsponge.entity.living.player.WSPlayer;

public class WSPlayerEvent extends WSEntityEvent {

    private WSPlayer player;

    public WSPlayerEvent(WSPlayer player) {
        super(player);
        this.player = player;
    }

    public WSPlayer getPlayer() {
        return player;
    }
}
