package com.degoos.wetsponge.event.entity.player;

import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.entity.other.WSItem;
import com.degoos.wetsponge.event.WSCancellable;

public class WSPlayerPickupItemEvent extends WSPlayerEvent implements WSCancellable {

    private WSItem item;
    private boolean cancelled;

    public WSPlayerPickupItemEvent(WSPlayer player, WSItem item) {
        super(player);
        this.item = item;
        this.cancelled = false;
    }

    public WSItem getItem() {
        return item;
    }

    public void setItem(WSItem item) {
        this.item = item;
    }

    @Override
    public boolean isCancelled() {
        return cancelled;
    }

    @Override
    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }
}
