package com.degoos.wetsponge.event.entity.player;

import com.degoos.wetsponge.data.WSTransaction;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.event.WSCancellable;
import com.degoos.wetsponge.item.WSItemStack;

public class WSPlayerSwapHandItemsEvent extends WSPlayerEvent implements WSCancellable {

	private WSTransaction<WSItemStack> mainHand, offHand;
	private boolean cancelled;

	public WSPlayerSwapHandItemsEvent(WSPlayer player, WSTransaction<WSItemStack> mainHand, WSTransaction<WSItemStack> offHand) {
		super(player);
		this.mainHand = mainHand;
		this.offHand = offHand;
		this.cancelled = false;
	}

	public WSTransaction<WSItemStack> getMainHand() {
		return mainHand;
	}

	public WSTransaction<WSItemStack> getOffHand() {
		return offHand;
	}

	@Override
	public boolean isCancelled() {
		return cancelled;
	}

	@Override
	public void setCancelled(boolean cancelled) {
		this.cancelled = cancelled;
	}
}
