package com.degoos.wetsponge.event.entity.player.bed;

import com.degoos.wetsponge.block.WSBlock;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.event.entity.player.WSPlayerEvent;

public class WSPlayerBedEvent extends WSPlayerEvent {

    private WSBlock bedBlock;

    public WSPlayerBedEvent(WSPlayer player, WSBlock bedBlock) {
        super(player);
        this.bedBlock = bedBlock;
    }

    public WSBlock getBedBlock() {
        return bedBlock;
    }


}
