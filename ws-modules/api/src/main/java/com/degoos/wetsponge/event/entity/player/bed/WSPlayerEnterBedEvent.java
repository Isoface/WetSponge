package com.degoos.wetsponge.event.entity.player.bed;

import com.degoos.wetsponge.block.WSBlock;
import com.degoos.wetsponge.event.WSCancellable;
import com.degoos.wetsponge.entity.living.player.WSPlayer;

public class WSPlayerEnterBedEvent extends WSPlayerBedEvent implements WSCancellable {
    private boolean cancelled;


    public WSPlayerEnterBedEvent(WSPlayer player, WSBlock bedBlock) {
        super(player, bedBlock);
        cancelled = false;
    }


    @Override
    public boolean isCancelled() {
        return cancelled;
    }

    @Override
    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }
}
