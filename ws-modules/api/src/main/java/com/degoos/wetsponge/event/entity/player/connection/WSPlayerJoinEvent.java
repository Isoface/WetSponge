package com.degoos.wetsponge.event.entity.player.connection;

import com.degoos.wetsponge.text.WSText;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.event.entity.player.WSPlayerEvent;

public class WSPlayerJoinEvent extends WSPlayerEvent {

    private WSText message, originalMessage;

    public WSPlayerJoinEvent(WSPlayer player, WSText message, WSText originalMessage) {
        super(player);
        this.message = message;
        this.originalMessage = originalMessage;
    }

    public WSText getMessage() {
        return message;
    }

    public void setMessage(WSText message) {
        this.message = message;
    }

    public WSText getOriginalMessage() {
        return originalMessage;
    }
}
