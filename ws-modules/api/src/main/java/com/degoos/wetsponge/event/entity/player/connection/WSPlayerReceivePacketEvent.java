package com.degoos.wetsponge.event.entity.player.connection;

import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.event.WSCancellable;
import com.degoos.wetsponge.event.entity.player.WSPlayerEvent;
import com.degoos.wetsponge.packet.WSPacket;

public class WSPlayerReceivePacketEvent extends WSPlayerEvent implements WSCancellable {

    private WSPacket packet;
    private boolean cancelled;

    public WSPlayerReceivePacketEvent(WSPlayer player, WSPacket packet) {
        super(player);
        this.packet = packet;
        cancelled = false;
    }

    public WSPacket getPacket() {
        return packet;
    }

    public void setPacket(WSPacket packet) {
        this.packet = packet;
    }

    @Override
    public boolean isCancelled() {
        return cancelled;
    }

    @Override
    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }
}
