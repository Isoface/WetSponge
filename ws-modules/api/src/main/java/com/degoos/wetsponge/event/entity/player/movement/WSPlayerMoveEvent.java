package com.degoos.wetsponge.event.entity.player.movement;

import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.event.WSCancellable;
import com.degoos.wetsponge.event.WSEvent;
import com.degoos.wetsponge.event.entity.movement.WSEntityMoveEvent;
import com.degoos.wetsponge.world.WSLocation;

/**
 * This event will be called after the {@link WSEntityMoveEvent} event if the server is a Sponge server. If not, {@link WSEntityMoveEvent} won't be called.
 */
public class WSPlayerMoveEvent extends WSEvent implements WSCancellable {

    private WSPlayer   player;
    private WSLocation from, to;
    private boolean cancelled;

    public WSPlayerMoveEvent(WSPlayer player, WSLocation from, WSLocation to) {
        this.player = player;
        this.from = from;
        this.to = to;
        this.cancelled = false;
    }

    public WSPlayer getPlayer() {
        return player;
    }


    public WSLocation getFrom() {
        return from != null ? from : from.clone();
    }

    public WSLocation getTo() {
        return to;
    }

    public void setTo(WSLocation to) {
        this.to = to;
    }

    @Override
    public boolean isCancelled() {
        return cancelled;
    }

    @Override
    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }
}
