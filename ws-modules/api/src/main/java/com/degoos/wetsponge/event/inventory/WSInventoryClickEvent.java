package com.degoos.wetsponge.event.inventory;


import com.degoos.wetsponge.data.WSTransaction;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.event.WSCancellable;
import com.degoos.wetsponge.event.WSEvent;
import com.degoos.wetsponge.inventory.WSInventory;
import com.degoos.wetsponge.inventory.WSSlotPos;
import com.degoos.wetsponge.item.WSItemStack;

import java.util.Optional;

public class WSInventoryClickEvent extends WSEvent implements WSCancellable {

    private WSSlotPos clickedSlot;
    private WSInventory clickedInventory;
    private WSPlayer player;
    private WSTransaction<Optional<WSItemStack>> transaction;
    private boolean cancelled;


    public WSInventoryClickEvent(WSSlotPos clickedSlot, WSInventory clickedInventory, WSPlayer player, WSTransaction<Optional<WSItemStack>> transaction) {
        this.clickedSlot = clickedSlot;
        this.clickedInventory = clickedInventory;
        this.player = player;
        this.transaction = transaction;
        this.cancelled = false;
    }


    public WSSlotPos getClickedSlot() {
        return clickedSlot;
    }


    public WSInventory getClickedInventory() {
        return clickedInventory;
    }


    public WSPlayer getPlayer() {
        return player;
    }

    public WSTransaction<Optional<WSItemStack>> getTransaction() {
        return transaction;
    }

    @Override
    public boolean isCancelled() {
        return cancelled;
    }


    @Override
    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }
}
