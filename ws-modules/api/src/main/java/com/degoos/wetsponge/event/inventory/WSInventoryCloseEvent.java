package com.degoos.wetsponge.event.inventory;


import com.degoos.wetsponge.event.WSCancellable;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.event.WSEvent;
import com.degoos.wetsponge.inventory.WSInventory;

public class WSInventoryCloseEvent extends WSEvent implements WSCancellable {

	private WSInventory inventory;
	private WSPlayer    player;
	private boolean     cancelled;


	public WSInventoryCloseEvent (WSInventory inventory, WSPlayer player) {
		this.inventory = inventory;
		this.player = player;
		this.cancelled = false;
	}


	public WSInventory getInventory () {
		return inventory;
	}


	public WSPlayer getPlayer () {
		return player;
	}


	@Override
	public boolean isCancelled () {
		return cancelled;
	}


	@Override
	public void setCancelled (boolean cancelled) {
		this.cancelled = cancelled;
	}
}
