package com.degoos.wetsponge.event.plugin;


import com.degoos.wetsponge.plugin.WSPlugin;

public class WSPluginDisableEvent extends WSPluginEvent {

	public WSPluginDisableEvent(WSPlugin plugin) {
		super(plugin);
	}
}
