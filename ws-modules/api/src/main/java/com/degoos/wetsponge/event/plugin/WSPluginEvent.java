package com.degoos.wetsponge.event.plugin;

import com.degoos.wetsponge.event.WSEvent;
import com.degoos.wetsponge.plugin.WSPlugin;

public class WSPluginEvent extends WSEvent {

	private WSPlugin plugin;

	public WSPluginEvent(WSPlugin plugin) {
		this.plugin = plugin;
	}

	public WSPlugin getPlugin() {
		return plugin;
	}

}
