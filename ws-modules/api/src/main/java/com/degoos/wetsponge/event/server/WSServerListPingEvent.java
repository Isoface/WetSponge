package com.degoos.wetsponge.event.server;

import com.degoos.wetsponge.event.WSEvent;
import com.degoos.wetsponge.server.WSFavicon;
import com.degoos.wetsponge.text.WSText;
import java.net.InetAddress;
import java.util.Optional;

public class WSServerListPingEvent extends WSEvent {

	private WSText description;
	private Optional<WSFavicon> favicon;
	private int maxPlayers;
	private InetAddress address;

	public WSServerListPingEvent(WSText description, Optional<WSFavicon> favicon, int maxPlayers, InetAddress address) {
		this.description = description;
		this.favicon = favicon;
		this.maxPlayers = maxPlayers;
		this.address = address;
	}

	public WSText getDescription() {
		return description;
	}

	public void setDescription(WSText description) {
		this.description = description;
	}

	public Optional<WSFavicon> getFavicon() {
		return favicon;
	}

	public void setFavicon(WSFavicon favicon) {
		this.favicon = Optional.ofNullable(favicon);
	}

	public int getMaxPlayers() {
		return maxPlayers;
	}

	public void setMaxPlayers(int maxPlayers) {
		this.maxPlayers = maxPlayers;
	}

	public InetAddress getAddress() {
		return address;
	}
}
