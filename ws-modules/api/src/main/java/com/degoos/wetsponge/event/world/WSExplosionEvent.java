package com.degoos.wetsponge.event.world;

import com.degoos.wetsponge.event.WSCancellable;
import com.degoos.wetsponge.world.WSExplosion;
import com.degoos.wetsponge.world.WSLocation;
import com.degoos.wetsponge.world.WSWorld;
import java.util.List;

/**
 * Represents a {@link WSExplosion explosion} event. These events are very bugged in {@link com.degoos.wetsponge.enums.EnumServerType#SPIGOT Spigot}, because it has a bad
 * Explosion API.
 */
public class WSExplosionEvent extends WSWorldEvent {

	protected WSExplosion explosion;

	public WSExplosionEvent(WSWorld world, WSExplosion explosion) {
		super(world);
		this.explosion = explosion;
	}

	public WSExplosion getExplosion() {
		return explosion;
	}

	public static class Detonate extends WSExplosionEvent {

		private List<WSLocation> affectedLocations;

		public Detonate(WSWorld world, WSExplosion explosion, List<WSLocation> affectedBlocks) {
			super(world, explosion);
			this.affectedLocations = affectedBlocks;
		}

		public List<WSLocation> getAffectedLocations() {
			return affectedLocations;
		}
	}

	public static class Pre extends WSExplosionEvent implements WSCancellable {

		private boolean cancelled;

		public Pre(WSWorld world, WSExplosion explosion) {
			super(world, explosion);
			this.cancelled = false;
		}

		void setExplosion(WSExplosion explosion) {
			this.explosion = explosion;
		}

		@Override
		public boolean isCancelled() {
			return cancelled;
		}

		@Override
		public void setCancelled(boolean cancelled) {
			this.cancelled = cancelled;
		}
	}
}
