package com.degoos.wetsponge.event.world;

import com.degoos.wetsponge.event.WSEvent;
import com.degoos.wetsponge.world.WSWorld;

public class WSWorldEvent extends WSEvent{

	private WSWorld world;

	public WSWorldEvent(WSWorld world) {
		this.world = world;
	}

	public WSWorld getWorld() {
		return world;
	}
}
