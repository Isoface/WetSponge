package com.degoos.wetsponge.exception.event;



public class WSEventException extends Exception {

	public WSEventException (final Throwable cause) {
		super(cause);
	}


	public WSEventException () {
	}


	public WSEventException (final Throwable cause, final String message) {
		super(message, cause);
	}


	public WSEventException (final String message) {
		super(message);
	}
}
