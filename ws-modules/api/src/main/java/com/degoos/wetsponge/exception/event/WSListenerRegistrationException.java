package com.degoos.wetsponge.exception.event;


public class WSListenerRegistrationException extends Exception {

	public WSListenerRegistrationException (final Throwable cause) {
		super(cause);
	}


	public WSListenerRegistrationException () {
	}


	public WSListenerRegistrationException (final Throwable cause, final String message) {
		super(message, cause);
	}


	public WSListenerRegistrationException (final String message) {
		super(message);
	}
}
