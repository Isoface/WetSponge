package com.degoos.wetsponge.exception.nbt;

public class NBTParseException extends Exception {

	public NBTParseException() {
		super();
	}

	public NBTParseException(String message) {
		super(message);
	}

	public NBTParseException(String message, Throwable cause) {
		super(message, cause);
	}

	public NBTParseException(Throwable cause) {
		super(cause);
	}
}
