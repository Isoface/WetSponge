package com.degoos.wetsponge.exception.particle;


public class WSInvalidParticleException extends NullPointerException {


    public WSInvalidParticleException() {
    }

    public WSInvalidParticleException(final String message) {
        super(message);
    }

}
