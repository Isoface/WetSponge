package com.degoos.wetsponge.exception.player;


public class WSPlayerNotFoundException extends Exception {

	public WSPlayerNotFoundException (final Throwable cause) {
		super(cause);
	}


	public WSPlayerNotFoundException () {
	}


	public WSPlayerNotFoundException (final Throwable cause, final String message) {
		super(message, cause);
	}


	public WSPlayerNotFoundException (final String message) {
		super(message);
	}

}
