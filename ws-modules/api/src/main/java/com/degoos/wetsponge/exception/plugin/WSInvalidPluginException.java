package com.degoos.wetsponge.exception.plugin;


public class WSInvalidPluginException extends Exception {

	public WSInvalidPluginException (final Throwable cause) {
		super(cause);
	}


	public WSInvalidPluginException () {
	}


	public WSInvalidPluginException (final Throwable cause, final String message) {
		super(message, cause);
	}


	public WSInvalidPluginException (final String message) {
		super(message);
	}

}
