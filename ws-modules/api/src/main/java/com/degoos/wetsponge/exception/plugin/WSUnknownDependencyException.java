package com.degoos.wetsponge.exception.plugin;


public class WSUnknownDependencyException extends Exception {

	public WSUnknownDependencyException (final Throwable cause) {
		super(cause);
	}


	public WSUnknownDependencyException () {
	}


	public WSUnknownDependencyException (final Throwable cause, final String message) {
		super(message, cause);
	}


	public WSUnknownDependencyException (final String message) {
		super(message);
	}

}
