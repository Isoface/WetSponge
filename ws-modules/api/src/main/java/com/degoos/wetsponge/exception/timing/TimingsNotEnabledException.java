package com.degoos.wetsponge.exception.timing;

public class TimingsNotEnabledException extends NullPointerException {

	public TimingsNotEnabledException() {
	}

	public TimingsNotEnabledException(String message) {
		super(message);
	}
}
