package com.degoos.wetsponge.hook;

import com.degoos.wetsponge.enums.EnumServerType;
import java.util.Arrays;
import java.util.Objects;

public abstract class WSHook {

	private String pluginId;
	private EnumServerType[] serverTypes;

	public WSHook(String pluginId, EnumServerType... serverTypes) {
		this.pluginId = pluginId;
		this.serverTypes = serverTypes;
	}

	public String getPluginId() {
		return pluginId;
	}

	public EnumServerType[] getServerTypes() {
		return serverTypes;
	}

	public boolean isSupported(EnumServerType type) {
		for (EnumServerType target : serverTypes)
			if (target == type) return true;
		return false;
	}

	public abstract void onUnload();


	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		WSHook wsHook = (WSHook) o;
		return Objects.equals(pluginId, wsHook.pluginId) && Arrays.equals(serverTypes, wsHook.serverTypes);
	}

	@Override
	public int hashCode() {

		int result = Objects.hash(pluginId);
		result = 31 * result + Arrays.hashCode(serverTypes);
		return result;
	}
}
