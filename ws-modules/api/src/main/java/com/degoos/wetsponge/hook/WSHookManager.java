package com.degoos.wetsponge.hook;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.enums.EnumTextColor;
import com.degoos.wetsponge.text.WSText;
import com.degoos.wetsponge.util.InternalLogger;

import java.util.*;

public class WSHookManager {

	private static WSHookManager instance = new WSHookManager();

	private Set<WSHook> hooks;
	private Map<String, Class<? extends WSHook>> hookClasses;

	public static WSHookManager getInstance() {
		return instance;
	}

	private WSHookManager() {
		hooks = new HashSet<>();
		hookClasses = new HashMap<>();
		try {
			switch (WetSponge.getServerType()) {
				case PAPER_SPIGOT:
				case SPIGOT:
					addHook("Vault", (Class<? extends WSHook>) Class.forName("com.degoos.wetsponge.hook.vault.SpigotVault"));
					addHook("PlaceholderAPI", (Class<? extends WSHook>) Class.forName("com.degoos.wetsponge.hook.placeholderapi.SpigotPlaceholderAPI"));
					break;
				case SPONGE:
					break;
			}
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An errror has occurred while WetSponge was loading a hook!");
		}
	}

	public void addHook(String plugin, Class<? extends WSHook> hook) {
		hookClasses.put(plugin, hook);
		if (WetSponge.getPluginManager().getBasePlugin(plugin).isPresent())
			enableHook(plugin);
	}

	public Optional<Class<? extends WSHook>> getHookClass(String plugin) {
		return Optional.ofNullable(hookClasses.get(plugin));
	}

	private Set<WSHook> getEnabledHooks() {
		return new HashSet<>(hooks);
	}

	public Optional<WSHook> getHook(String plugin) {
		return hooks.stream().filter(hook -> hook.getPluginId().equals(plugin)).findAny();
	}

	public <T extends WSHook> Optional<T> getHook(Class<T> hookInstance) {
		return hooks.stream().filter(hookInstance::isInstance).map(hook -> (T) hook).findAny();
	}

	public boolean isHookEnabled(String plugin) {
		return getHook(plugin).isPresent();
	}

	public <T extends WSHook> boolean isHookEnabled(Class<T> hookInstance) {
		return getHook(hookInstance).isPresent();
	}

	public void enableHook(String plugin) {
		if (getHook(plugin).isPresent()) return;
		getHookClass(plugin).ifPresent(hookClass -> {
			try {
				InternalLogger.sendInfo(WSText.of("Loading hook: ", WSText.of(plugin, EnumTextColor.YELLOW)));
				WSHook hook = hookClass.newInstance();
				hooks.add(hook);
			} catch (Exception ex) {
				InternalLogger.printException(ex, "An error has occurred while WetSponge was trying to load a hook!");
			}
		});
	}

	public void disableHook(String plugin) {
		hooks.stream().filter(hook -> hook.getPluginId().equals(plugin)).forEach(WSHook::onUnload);
		hooks.removeIf(hook -> hook.getPluginId().equals(plugin));
	}

}
