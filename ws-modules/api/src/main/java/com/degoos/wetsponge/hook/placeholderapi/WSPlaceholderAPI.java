package com.degoos.wetsponge.hook.placeholderapi;

import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.enums.EnumServerType;
import com.degoos.wetsponge.hook.WSHook;
import com.degoos.wetsponge.text.WSText;
import java.util.Collection;
import java.util.List;

public abstract class WSPlaceholderAPI extends WSHook {

	public WSPlaceholderAPI(EnumServerType... serverTypes) {
		super("PlaceholderAPI", serverTypes);
	}

	public abstract WSText parse(WSPlayer player, WSText text);

	public abstract List<WSText> parse(WSPlayer player, List<WSText> text);

	public abstract String parse(WSPlayer player, String string);

	public abstract void registerExpansion(WSPlaceholderAPIExpansion expansion);

	public abstract void unregisterExpansion(String identifier);

	public abstract Collection<WSPlaceholderAPIHook> getHooks();

	public abstract void unregisterAllExpansions();
}
