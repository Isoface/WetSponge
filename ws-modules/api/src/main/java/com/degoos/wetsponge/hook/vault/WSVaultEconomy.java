package com.degoos.wetsponge.hook.vault;

import com.degoos.wetsponge.user.WSUser;
import java.util.List;

public interface WSVaultEconomy {

	boolean isEnabled();

	String getName();

	boolean hasBankSupport();

	int getFractionalDigits();

	String getCurrencyNamePlural();

	String getCurrencyNameSingular();

	String formatValue(double value);

	boolean hasAccount(WSUser user);

	boolean hasAccount(WSUser user, String account);

	double getBalance(WSUser user);

	double getBalance(WSUser user, String account);

	boolean hasMoney(WSUser user, double amount);

	boolean hasMoney(WSUser user, double amount, String account);

	WSVaultEconomyResponse withdrawPlayer(WSUser user, double amount);

	WSVaultEconomyResponse withdrawPlayer(WSUser user, double amount, String account);

	WSVaultEconomyResponse depositPlayer(WSUser user, double amount);

	WSVaultEconomyResponse depositPlayer(WSUser user, double amount, String account);

	WSVaultEconomyResponse createBank(String bank, WSUser user);

	WSVaultEconomyResponse deleteBank(String bank);

	WSVaultEconomyResponse bankBalance(String bank);

	WSVaultEconomyResponse bankHasMoney(String bank, double amount);

	WSVaultEconomyResponse bankWithdraw(String bank, double amount);

	WSVaultEconomyResponse bankDeposit(String bank, double amount);

	WSVaultEconomyResponse isBankOwner(String bank, WSUser user);

	WSVaultEconomyResponse isBankMember(String bank, WSUser user);

	List<String> getBanks();

	boolean createPlayerAccount(WSUser user);

	boolean createPlayerAccount(WSUser user, String account);
}
