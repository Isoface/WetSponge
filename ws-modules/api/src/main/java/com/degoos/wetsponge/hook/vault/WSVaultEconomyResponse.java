package com.degoos.wetsponge.hook.vault;

import java.util.Arrays;
import java.util.Optional;

public interface WSVaultEconomyResponse {

	double getAmount();

	double getBalance();

	WSVaultResponseType getResponseType();

	Optional<String> getErrorMessage();


	enum WSVaultResponseType {
		SUCCESS(1), FAILURE(2), NOT_IMPLEMENTED(3);

		private int id;

		WSVaultResponseType(int id) {
			this.id = id;
		}

		public int getId() {
			return this.id;
		}

		public static Optional<WSVaultResponseType> getById(int id) {
			return Arrays.stream(values()).filter(target -> target.getId() == id).findAny();
		}

		public static Optional<WSVaultResponseType> getByName(String name) {
			return Arrays.stream(values()).filter(target -> target.name().equals(name)).findAny();
		}
	}
}
