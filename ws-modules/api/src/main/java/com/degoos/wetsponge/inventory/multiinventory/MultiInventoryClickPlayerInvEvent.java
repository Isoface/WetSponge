package com.degoos.wetsponge.inventory.multiinventory;


import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.event.WSCancellable;
import com.degoos.wetsponge.event.entity.player.WSPlayerEvent;
import com.degoos.wetsponge.event.inventory.WSInventoryClickEvent;
import com.degoos.wetsponge.inventory.WSInventory;
import com.degoos.wetsponge.inventory.WSSlotPos;

public class MultiInventoryClickPlayerInvEvent extends WSPlayerEvent implements WSCancellable {

    private WSSlotPos clickedSlot;
    private WSInventory inventory;
    private MultiInventory multiInventory;
    private WSInventoryClickEvent wetSpongeEvent;
    private boolean cancelled;


    public MultiInventoryClickPlayerInvEvent(WSSlotPos clickedSlot, WSInventory inventory, MultiInventory multiInventory,
                                             WSPlayer player, WSInventoryClickEvent wetSpongeEvent) {
        super(player);
        this.clickedSlot = clickedSlot;
        this.inventory = inventory;
        this.multiInventory = multiInventory;
        this.wetSpongeEvent = wetSpongeEvent;
        cancelled = false;
    }


    public WSInventoryClickEvent getWetSpongeEvent() {
        return wetSpongeEvent;
    }

    public WSInventory getInventory() {
        return inventory;
    }


    public MultiInventory getMultiInventory() {
        return multiInventory;
    }

    public WSSlotPos getInvSlot() {
        return clickedSlot;
    }


    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }


    public boolean isCancelled() {
        return cancelled;
    }

}
