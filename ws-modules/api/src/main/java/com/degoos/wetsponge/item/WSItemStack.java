package com.degoos.wetsponge.item;


import com.degoos.wetsponge.bridge.item.BridgeItemStack;
import com.degoos.wetsponge.effect.potion.WSPotionEffect;
import com.degoos.wetsponge.item.enchantment.WSEnchantment;
import com.degoos.wetsponge.material.WSBlockTypes;
import com.degoos.wetsponge.material.WSItemTypes;
import com.degoos.wetsponge.material.WSMaterial;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;
import com.degoos.wetsponge.text.WSText;
import com.degoos.wetsponge.text.translation.WSTranslatable;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * This class represents an ItemStack. To push the changes to the handled ItemStack use {@link #update()}. To reset all changes to the handled values use {@link
 * #refresh()}. To clone the handled ItemStack use {@link #clone()}.
 */
public interface WSItemStack extends WSTranslatable {

	/**
	 * Creates a new {@link WSItemStack item stack} from a {@link WSMaterial material}.
	 *
	 * @param material the {@link WSMaterial material}.
	 * @return a new {@link WSItemStack item stack}.
	 */
	public static WSItemStack of(WSMaterial material) {
		return BridgeItemStack.of(material);
	}

	/**
	 * Creates a new {@link WSItemStack item stack} from a {@link WSItemTypes material}.
	 *
	 * @param material the {@link WSItemTypes material}.
	 * @return a new {@link WSItemStack item stack}.
	 */
	public static WSItemStack of(WSItemTypes material) {
		return of(material.getDefaultState());
	}

	/**
	 * Creates a new {@link WSItemStack item stack} from a {@link WSBlockTypes material}.
	 *
	 * @param material the {@link WSBlockTypes material}.
	 * @return a new {@link WSItemStack item stack}.
	 */
	public static WSItemStack of(WSBlockTypes material) {
		return of(material.getDefaultState());
	}

	/**
	 * Creates a new {@link WSItemStack item stack} from a serialized NBTTagCompound. <p>NBTTagCompounds are used by Minecraft for serialization.</p>
	 *
	 * @param nbt the serialized NBTTag.
	 * @return a new {@link WSItemStack item stack}.
	 * @throws Exception if the NBTTag serialization is wrong.
	 */
	public static WSItemStack ofSerializedNBTTag(String nbt) throws Exception {
		return BridgeItemStack.ofSerializedNBTTag(nbt);
	}

	/**
	 * Creates a new {@link WSItemStack item stack} from a {@link WSNBTTagCompound}. <p>NBTTagCompounds are used by Minecraft for serialization.</p>
	 *
	 * @param nbtTagCompound the {@link WSNBTTagCompound}.
	 * @return a new {@link WSItemStack item stack}.
	 * @throws Exception if the {@link WSNBTTagCompound}'s ID value is wrong.
	 */
	public static WSItemStack ofNBTTagCompound(WSNBTTagCompound nbtTagCompound) throws Exception {
		return BridgeItemStack.ofNBTTagCompound(nbtTagCompound);
	}

	/**
	 * Creates a skull by a format. A format can be a URL, a B64 code or a player name.
	 *
	 * @param format the format.
	 * @return the new skull.
	 */
	public static WSItemStack createSkull(String format) {
		return BridgeItemStack.createSkull(format);
	}

	/**
	 * @return the display name of the {@link WSItemStack}.
	 */
	WSText getDisplayName();

	/**
	 * This method sets the display name of the {@link WSItemStack}.
	 *
	 * @param displayName the displayed name.
	 * @return this {@link WSItemStack}.
	 */
	WSItemStack setDisplayName(WSText displayName);

	/**
	 * This method returns the lore of the {@link WSItemStack}. If there are not lore, this method will return a empty {@link java.util.ArrayList}.
	 * The given {@link java.util.ArrayList} is mutable.
	 *
	 * @return the item lore.
	 */
	List<WSText> getLore();

	/**
	 * This method sets the lore of the {@link WSItemStack}. If the param lore is null, the lore will be cleared. After use this method, every change you made in the
	 * list
	 * will not be applied into the lore.
	 *
	 * @param lore Set the {@link WSText} as the new lore
	 * @return this {@link WSItemStack}.
	 */
	WSItemStack setLore(List<WSText> lore);

	/**
	 * Adds a line to the lore of the {@link WSItemStack item stack}.
	 *
	 * @param line the line.
	 * @return the {@link WSItemStack item stack}.
	 */
	WSItemStack addLoreLine(WSText line);

	/**
	 * Clears the lore of the {@link WSItemStack item stack}.
	 *
	 * @return the {@link WSItemStack item stack}.
	 */
	WSItemStack clearLore();

	/**
	 * This method returns a list with all enchantments of the {@link WSItemStack} and their levels.
	 *
	 * @return a list with all enchantments of the {@link WSItemStack} and their levels.
	 */
	Map<WSEnchantment, Integer> getEnchantments();

	/**
	 * This method returns the target {@link WSEnchantment} level for the {@link WSItemStack}, if present.
	 *
	 * @param enchantment the target {@link WSEnchantment}.
	 * @return the target {@link WSEnchantment} level for the {@link WSItemStack}, if present.
	 */
	Optional<Integer> getEnchantmentLevel(WSEnchantment enchantment);

	/**
	 * This method returns true if the target {@link WSEnchantment} is present on the {@link WSItemStack}.
	 *
	 * @param enchantment the target {@link WSEnchantment}.
	 * @return true if the target {@link WSEnchantment} is present on the {@link WSItemStack}.
	 */
	boolean containsEnchantment(WSEnchantment enchantment);

	/**
	 * This method adds a {@link WSEnchantment} to the {@link WSItemStack}. If the {@link WSEnchantment} is already added, it won't do nothing.
	 *
	 * @param enchantment the {@link WSEnchantment}.
	 * @param level       the {@link WSEnchantment} level.
	 * @return the {@link WSItemStack item stack}.
	 */
	WSItemStack addEnchantment(WSEnchantment enchantment, int level);

	/**
	 * This method removes a {@link WSEnchantment} from the {@link WSItemStack}. If the {@link WSItemStack} has not the {@link WSEnchantment}, it won't do nothing.
	 *
	 * @param enchantment the {@link WSEnchantment} to remove.
	 * @return the {@link WSItemStack item stack}.
	 */
	WSItemStack removeEnchantment(WSEnchantment enchantment);

	/**
	 * This method clears all {@link WSEnchantment} of this {@link WSItemStack}.
	 *
	 * @return the {@link WSItemStack item stack}.
	 */
	WSItemStack clearEnchantments();

	/**
	 * This method returns the {@link WSMaterial} of this {@link WSItemStack}. It can be a {@link com.degoos.wetsponge.material.block.WSBlockType}
	 * or a {@link com.degoos.wetsponge.material.item.WSItemType}. To find all materials, you can
	 * search in the enums {@link WSBlockTypes} and {@link WSItemTypes}.
	 *
	 * @return the material.
	 */
	WSMaterial getMaterial();

	/**
	 * This method sets the the {@link WSMaterial} of this {@link WSItemStack}. It can be a {@link com.degoos.wetsponge.material.block.WSBlockType}
	 * or a {@link com.degoos.wetsponge.material.item.WSItemType}. To find all materials, you can
	 * * search in the enums {@link WSBlockTypes} and {@link WSItemTypes}.
	 *
	 * @param material the {@link WSMaterial material}.
	 * @return this {@link WSItemStack}.
	 */
	WSItemStack setMaterial(WSMaterial material);

	/**
	 * @return the quantity of items of this {@link WSItemStack}.
	 */
	int getQuantity();

	/**
	 * This method sets the quantity of items of this {@link WSItemStack}.
	 *
	 * @param quantity the quantity.
	 * @return this {@link WSItemStack}.
	 */
	WSItemStack setQuantity(int quantity);

	/**
	 * This method returns true if the {@link WSItemStack} is unbreakable.
	 *
	 * @return true if the {@link WSItemStack} is unbreakable.
	 */
	boolean isUnbreakable();

	/**
	 * This method sets if the {@link WSItemStack} is unbreakable.
	 *
	 * @param unbreakable the boolean.
	 * @return the {@link WSItemStack item stack}.
	 */
	WSItemStack setUnbreakable(boolean unbreakable);

	/**
	 * This method returns true if the {@link WSItemStack} has its enchantments hidden.
	 *
	 * @return true if the {@link WSItemStack} has its enchantments hidden.
	 */
	boolean isHidingEnchantments();

	/**
	 * This method sets if the {@link WSItemStack} will hide its enchantments.
	 *
	 * @param hideEnchantments the boolean.
	 * @return the {@link WSItemStack item stack}.
	 */
	WSItemStack hideEnchantments(boolean hideEnchantments);

	/**
	 * This method returns true if the {@link WSItemStack} has its attributes hidden.
	 *
	 * @return true if the {@link WSItemStack} has its attributes hidden.
	 */
	boolean isHidingAttributes();

	/**
	 * This method sets if the {@link WSItemStack} will hide its attributes.
	 *
	 * @param hideAttributes the boolean.
	 * @return the {@link WSItemStack item stack}.
	 */
	WSItemStack hideAttributes(boolean hideAttributes);

	/**
	 * This method returns true if the {@link WSItemStack} has its unbreakable value hidden.
	 *
	 * @return true if the {@link WSItemStack} has its unbreakable value hidden.
	 */
	boolean isHidingUnbreakable();

	/**
	 * This method sets if the {@link WSItemStack} will hide its unbreakable value.
	 *
	 * @param hideUnbreakable the boolean.
	 * @return the {@link WSItemStack item stack}.
	 */
	WSItemStack hideUnbreakable(boolean hideUnbreakable);

	/**
	 * This method returns true if the {@link WSItemStack} has its can destroy values hidden.
	 *
	 * @return true if the {@link WSItemStack} has its can destroy values hidden.
	 */
	boolean isHidingCanDestroy();

	/**
	 * This method sets if the {@link WSItemStack} will hide its can destroy values.
	 *
	 * @param hideCanDestroy the boolean.
	 * @return the {@link WSItemStack item stack}.
	 */
	WSItemStack hideCanDestroy(boolean hideCanDestroy);

	/**
	 * This method returns true if the {@link WSItemStack} has its can be placed on values hidden.
	 *
	 * @return true if the {@link WSItemStack} has its can be placed on values hidden.
	 */
	boolean isHidingCanBePlacedOn();

	/**
	 * This method sets if the {@link WSItemStack} will hide its can be placed on values.
	 *
	 * @param hideCanBePlacedOn the boolean.
	 * @return the {@link WSItemStack item stack}.
	 */
	WSItemStack hideCanBePlacedOn(boolean hideCanBePlacedOn);

	/**
	 * This method returns true if the {@link WSItemStack} has its {@link WSPotionEffect potion effects} hidden.
	 *
	 * @return true if the {@link WSItemStack} has its {@link WSPotionEffect potion effects} hidden.
	 */
	boolean isHidingPotionEffects();

	/**
	 * This method sets if the {@link WSItemStack} will hide if its {@link WSPotionEffect potion effects}.
	 *
	 * @param hidePotionEffects the boolean.
	 * @return the {@link WSItemStack item stack}.
	 */
	WSItemStack hidePotionEffects(boolean hidePotionEffects);

	/**
	 * Returns the NBTTag version of the {@link WSItemStack}.
	 *
	 * @return the NBTTag.
	 */
	WSNBTTagCompound toNBTTag();

	/**
	 * Returns a serialized NBTTag of the {@link WSItemStack}. To deserialize it, use {@link WSItemStack#ofSerializedNBTTag(String)}
	 *
	 * @return the serialized NBTTag.
	 */
	String toSerializedNBTTag();

	/**
	 * Updates the handled ItemStack, applying all changes of this {@link WSItemStack}.
	 *
	 * @return this {@link WSItemStack}.
	 */
	WSItemStack update();

	/**
	 * Refresh this {@link WSItemStack}, losing all changes you've made.
	 *
	 * @return this {@link WSItemStack}.
	 */
	WSItemStack refresh();

	/**
	 * @return a new copy of this {@link WSItemStack}, with a new handled ItemStack.
	 */
	WSItemStack clone();

	/**
	 * Works the same way as <code>equals()</code>, but it doesn't count the quantity.
	 *
	 * @param o the other {@link WSItemStack ItemStack}.
	 * @return if both {@link WSItemStack ItemStack}s are similar.
	 */
	boolean isSimilar(Object o);

	/**
	 * @return the handled ItemStack.
	 */
	Object getHandled();
}
