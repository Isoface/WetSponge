package com.degoos.wetsponge.item.enchantment;

import com.degoos.wetsponge.item.WSItemStack;
import com.degoos.wetsponge.text.translation.WSTranslatable;

public interface WSEnchantment extends WSTranslatable {

    String getName();

    int getMaximumLevel();

    int getMinimumLevel();

    boolean canBeAppliedToStack(WSItemStack itemStack);

    boolean isCompatibleWith(WSEnchantment enchantment);

    Object getHandled ();
}
