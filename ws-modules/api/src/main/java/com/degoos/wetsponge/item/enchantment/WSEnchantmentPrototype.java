package com.degoos.wetsponge.item.enchantment;

import com.degoos.wetsponge.item.WSItemStack;

public interface WSEnchantmentPrototype {

    boolean canBeAppliedToStack(WSItemStack itemStack);

    boolean isCompatibleWith(WSEnchantment enchantment);

}
