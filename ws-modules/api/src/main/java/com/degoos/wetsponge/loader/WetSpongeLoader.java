package com.degoos.wetsponge.loader;

/**
 * The WetSpongeLoader class represents the class which loads WetSponge.
 * It's the first class to loads, and setups the {@link com.degoos.wetsponge.WetSponge} class.
 */
public interface WetSpongeLoader {

}
