package com.degoos.wetsponge.map;

public class WSMapCharacterSprite {

	private final int width;
	private final int height;
	private final boolean[] data;

	public WSMapCharacterSprite(int width, int height, boolean[] data) {
		this.width = width;
		this.height = height;
		this.data = data;
		if (data.length != width * height) {
			throw new IllegalArgumentException("size of data does not match dimensions");
		}
	}

	public boolean get(int row, int col) {
		return (row >= 0 && col >= 0 && row < this.height && col < this.width) && this.data[row * this.width + col];
	}

	public int getWidth() {
		return this.width;
	}

	public int getHeight() {
		return this.height;
	}

}
