package com.degoos.wetsponge.map;

import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.enums.EnumMapBaseColor;
import com.degoos.wetsponge.enums.EnumMapIllumination;
import com.degoos.wetsponge.enums.EnumMapScale;
import com.degoos.wetsponge.packet.WSPacket;
import com.degoos.wetsponge.packet.play.server.WSSPacketMaps;
import com.degoos.wetsponge.util.Validate;
import com.flowpowered.math.vector.Vector2i;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class WSMapView {

	private byte[] colors;
	private Set<WSMapDecoration> decorations;
	private boolean trackingPositions;
	private EnumMapScale mapScale;

	public WSMapView() {
		colors = new byte[16384];
		decorations = new HashSet<>();
		trackingPositions = false;
		mapScale = EnumMapScale.NORMAL;
	}

	public WSMapView(byte[] colors, Set<WSMapDecoration> decorations, boolean trackingPositions, EnumMapScale mapScale) {
		Validate.notNull(colors, "Colors cannot be null!");
		Validate.isTrue(colors.length == 16384, "Colors length must be 128^2 but it's " + colors.length + "!");
		this.colors = colors;
		this.decorations = decorations == null ? new HashSet<>() : decorations;
		this.trackingPositions = trackingPositions;
		this.mapScale = mapScale == null ? EnumMapScale.CLOSEST : mapScale;
	}

	public byte[] getColors() {
		return colors;
	}

	public void setColors(byte[] colors) {
		Validate.notNull(colors, "Colors cannot be null!");
		Validate.isTrue(colors.length == 16384, "Colors length must be 128^2 but it's " + colors.length + "!");
		this.colors = colors;
	}

	public boolean isTrackingPositions() {
		return trackingPositions;
	}

	public void setTrackingPositions(boolean trackingPositions) {
		this.trackingPositions = trackingPositions;
	}

	public EnumMapScale getMapScale() {
		return mapScale;
	}

	public void setMapScale(EnumMapScale mapScale) {
		this.mapScale = mapScale == null ? EnumMapScale.NORMAL : mapScale;
	}

	public Vector2i getSize() {
		return new Vector2i(128, 128);
	}


	public void setPixelExact(Vector2i position, WSMapColor color) {
		setPixelExact(position.getX(), position.getY(), color);
	}

	public void setPixelExact(int x, int y, WSMapColor color) {
		Validate.isTrue(x >= 0 && x < 128, "x >= 0 && x < 128");
		Validate.isTrue(y >= 0 && y < 128, "y >= 0 && y < 128");
		colors[y * 128 + x] = color.getMapColor();
	}

	public void setPixelExact(Vector2i position, byte color) {
		setPixelExact(position.getX(), position.getY(), color);
	}

	public void setPixelExact(int x, int y, byte color) {
		Validate.isTrue(x >= 0 && x < 128, "x >= 0 && x < 128");
		Validate.isTrue(y >= 0 && y < 128, "y >= 0 && y < 128");
		colors[y * 128 + x] = color;
	}

	public void setPixelSimilar(Vector2i position, Color color) {
		setPixelSimilar(position.getX(), position.getY(), color);
	}

	public void setPixelSimilar(int x, int y, Color color) {
		Validate.isTrue(x >= 0 && x < 128, "x >= 0 && x < 128");
		Validate.isTrue(y >= 0 && y < 128, "y >= 0 && y < 128");
		colors[y * 128 + x] = EnumMapBaseColor.getBestColor(color).getMapColor();
	}

	public WSMapColor getPixel(Vector2i position) {
		return getPixel(position.getX(), position.getY());
	}

	public WSMapColor getPixel(int x, int y) {
		Validate.isTrue(x >= 0 && x < 128, "x >= 0 && x < 128");
		Validate.isTrue(y >= 0 && y < 128, "y >= 0 && y < 128");
		int pixel = colors[y * 128 + x];

		int illumination = pixel % 4;

		return new WSMapColor(EnumMapBaseColor.getById((pixel - illumination) / 4).orElse(EnumMapBaseColor.AIR), EnumMapIllumination.getById(illumination)
			.orElse(EnumMapIllumination.NORMAL));
	}

	public BufferedImage toImage() {
		BufferedImage image = new BufferedImage(128, 128, BufferedImage.TYPE_INT_RGB);
		int[] data = ((DataBufferInt) image.getRaster().getDataBuffer()).getData();
		for (int i = 0; i < 128; i++) {
			for (int j = 0; j < 128; j++) {
				data[j + (i * 128)] = getPixel(j, i).getColor().getRGB();
			}
		}
		return image;
	}

	public void fill(WSMapColor mapColor) {
		Vector2i size = getSize();
		for (int x = 0; x < size.getX(); x++)
			for (int y = 0; y < size.getY(); y++)
				colors[y * 128 + x] = mapColor.getMapColor();
	}

	public void drawImage(BufferedImage image) {
		drawImage(0, 0, image);
	}

	public void drawImage(Vector2i position, BufferedImage image) {
		drawImage(position.getX(), position.getY(), image);
	}

	public void drawImage(int x, int y, BufferedImage image) {
		drawImage(x, y, -1, image);
	}

	public void drawImage(int threshold, BufferedImage image) {
		drawImage(0, 0, threshold, image);
	}

	public void drawImage(Vector2i position, int threshold, BufferedImage image) {
		drawImage(position.getX(), position.getY(), threshold, image);
	}

	public void drawImage(int x, int y, int threshold, BufferedImage image) {
		Validate.notNull(image, "image");
		Validate.isTrue(x >= 0 && x < 128, "x >= 0 && x < 128");
		Validate.isTrue(y >= 0 && y < 128, "y >= 0 && y < 128");
		int width = Math.min(image.getWidth(), 128);
		int height = Math.min(image.getHeight(), 128);

		int[] pixels = image.getRGB(0, 0, width, height, null, 0, image.getWidth());

		for (int i = 0; i < height; i++) {
			for (int j = 0; j < width; j++) {
				int argb = pixels[i * width + j];
				int alpha = (argb >> 24) & 0xFF;
				if (alpha > threshold) {
					if (alpha > 0) this.colors[i * 128 + j] = EnumMapBaseColor.getBestColor(new Color(argb, true)).getMapColor();
					else this.colors[i * 128 + j] = 0;
				}
			}
		}
	}

	public void drawText(Vector2i position, String text, WSMapFont font) {
		drawText(position.getX(), position.getY(), text, font);
	}

	public void drawText(int x, int y, String text, WSMapFont font) {
		int xStart = x;
		byte color = 44;
		if (!font.isValid(text)) {
			throw new IllegalArgumentException("text contains invalid characters");
		} else {
			for (int i = 0; i < text.length(); ++i) {
				char ch = text.charAt(i);
				if (ch == '\n') {
					x = xStart;
					y += font.getHeight() + 1;
				} else {
					if (ch == '\u00A7') {
						int j = text.indexOf(59, i);
						if (j >= 0) {
							try {
								color = Byte.parseByte(text.substring(i + 1, j));
								i = j;
								continue;
							} catch (NumberFormatException var12) {
							}
						}
					}

					WSMapCharacterSprite sprite = font.getChar(text.charAt(i));

					for (int r = 0; r < font.getHeight(); ++r) {
						for (int c = 0; c < sprite.getWidth(); ++c) {
							if (sprite.get(r, c)) {
								setPixelExact(x + c, y + r, color);
							}
						}
					}
					x += sprite.getWidth() + 1;
				}
			}
		}
	}

	public Collection<WSMapDecoration> getAllDecorations() {
		return decorations;
	}

	public void addDecoration(WSMapDecoration decoration) {
		Validate.notNull(decoration, "Decoration cannot be null!");
		decorations.add(decoration);
	}

	public void sendUpdate(WSPlayer player, int map, Vector2i origin, Vector2i size) {
		Validate.notNull(player, "Player cannot be null!");
		Validate.notNull(origin, "Origin cannot be null!");
		Validate.notNull(size, "Size cannot be null!");
		player.sendPacket(WSSPacketMaps.of(map, origin, size, this));
	}

	public void sendUpdate(WSPlayer player, int map) {
		Validate.notNull(player, "Player cannot be null!");
		player.sendPacket(WSSPacketMaps.of(map, new Vector2i(), getSize(), this));
	}

	public void sendUpdate(Iterable<WSPlayer> players, int map, Vector2i origin, Vector2i size) {
		WSPacket packet = WSSPacketMaps.of(map, origin, size, this);
		players.forEach(player -> player.sendPacket(packet));
	}

	public void sendUpdate(Iterable<WSPlayer> players, int map) {
		WSPacket packet = WSSPacketMaps.of(map, new Vector2i(), getSize(), this);
		players.forEach(player -> player.sendPacket(packet));
	}
}
