package com.degoos.wetsponge.material;

import com.degoos.wetsponge.enums.block.EnumAxis;
import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.enums.block.EnumBlockTypeRailShape;
import com.degoos.wetsponge.util.ListUtils;

import java.util.Set;

class WSBlockTypesUtils {

	static final Set<EnumAxis> ALL_AXES = ListUtils.toSet(EnumAxis.X, EnumAxis.Y, EnumAxis.Z);
	static final Set<EnumBlockFace> ALL_FACES = ListUtils.toSet(EnumBlockFace.UP, EnumBlockFace.DOWN, EnumBlockFace.NORTH, EnumBlockFace.SOUTH, EnumBlockFace.EAST, EnumBlockFace.WEST);
	static final Set<EnumBlockFace> SIDE_FACES = ListUtils.toSet(EnumBlockFace.NORTH, EnumBlockFace.SOUTH, EnumBlockFace.EAST, EnumBlockFace.WEST);
	static final Set<EnumBlockFace> TORCH_FACES = ListUtils.toSet(EnumBlockFace.NORTH, EnumBlockFace.SOUTH, EnumBlockFace.EAST, EnumBlockFace.WEST, EnumBlockFace.UP);
	static final Set<EnumBlockFace> HOPPER_FACES = ListUtils.toSet(EnumBlockFace.NORTH, EnumBlockFace.SOUTH, EnumBlockFace.EAST, EnumBlockFace.WEST, EnumBlockFace.DOWN);
	static final Set<EnumBlockTypeRailShape> ALL_RAIL_SHAPES = ListUtils.toSet(EnumBlockTypeRailShape.values());
	static final Set<EnumBlockTypeRailShape> REDSTONE_RAIL_SHAPES = ListUtils.toSet(EnumBlockTypeRailShape.NORTH_SOUTH, EnumBlockTypeRailShape.EAST_WEST, EnumBlockTypeRailShape.ASCENDING_NORTH,
			EnumBlockTypeRailShape.ASCENDING_EAST, EnumBlockTypeRailShape.ASCENDING_SOUTH, EnumBlockTypeRailShape.ASCENDING_WEST);

}
