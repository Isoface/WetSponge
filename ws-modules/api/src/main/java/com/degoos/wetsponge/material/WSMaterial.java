package com.degoos.wetsponge.material;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.material.block.WSBlockType;
import com.degoos.wetsponge.material.item.WSItemType;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;

import java.util.Optional;

public interface WSMaterial {

	WSMaterial clone();

	int getNumericalId();

	String getStringId();

	String getNewStringId();

	String getOldStringId();

	int getMaxStackSize();

	WSNBTTagCompound writeToData(WSNBTTagCompound compound);

	WSNBTTagCompound readFromData(WSNBTTagCompound compound);

	static Optional<? extends WSMaterial> getById(String string) {
		Optional<? extends WSMaterial> optional;
		if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13)) {
			optional = WSBlockTypes.getByOldId(string);
			if (!optional.isPresent()) optional = WSItemTypes.getByOldId(string);
			if (!optional.isPresent()) optional = WSBlockTypes.getByNewId(string);
			if (!optional.isPresent()) optional = WSItemTypes.getByNewId(string);
		} else {
			optional = WSBlockTypes.getByNewId(string);
			if (!optional.isPresent()) optional = WSItemTypes.getByNewId(string);
			if (!optional.isPresent()) optional = WSBlockTypes.getByOldId(string);
			if (!optional.isPresent()) optional = WSItemTypes.getByOldId(string);
		}
		return optional;
	}

	static Optional<? extends WSMaterial> getById(int id) {
		Optional<? extends WSMaterial> optional = WSBlockTypes.getById(id);
		if (!optional.isPresent()) optional = WSItemTypes.getById(id);
		return optional;
	}

	static Class<? extends WSMaterial> getMaterialClass(WSMaterial material) {

		String id = material.getStringId();

		if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13)) {
			Optional<WSBlockTypes> blockTypes = WSBlockTypes.getBlockTypesByNewId(id);
			if (blockTypes.isPresent()) return blockTypes.get().getMaterialClass();
			Optional<WSItemTypes> itemTypes = WSItemTypes.getItemTypesByNewId(id);
			if (itemTypes.isPresent()) return itemTypes.get().getMaterialClass();
			blockTypes = WSBlockTypes.getBlockTypesByOldId(id);
			if (blockTypes.isPresent()) return blockTypes.get().getMaterialClass();
			itemTypes = WSItemTypes.getItemTypesByOldId(id);
			if (itemTypes.isPresent()) return itemTypes.get().getMaterialClass();
		} else {
			Optional<WSBlockTypes> blockTypes = WSBlockTypes.getBlockTypesByOldId(id);
			if (blockTypes.isPresent()) return blockTypes.get().getMaterialClass();
			Optional<WSItemTypes> itemTypes = WSItemTypes.getItemTypesByOldId(id);
			if (itemTypes.isPresent()) return itemTypes.get().getMaterialClass();
			blockTypes = WSBlockTypes.getBlockTypesByNewId(id);
			if (blockTypes.isPresent()) return blockTypes.get().getMaterialClass();
			itemTypes = WSItemTypes.getItemTypesByNewId(id);
			if (itemTypes.isPresent()) return itemTypes.get().getMaterialClass();
		}

		if (material instanceof WSBlockType) return WSBlockType.class;
		if (material instanceof WSItemType) return WSItemType.class;
		return null;
	}
}
