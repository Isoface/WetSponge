package com.degoos.wetsponge.material.block;

import com.degoos.wetsponge.material.WSMaterial;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;

public interface WSBlockType extends WSMaterial {

	WSBlockType clone();

	@Override
	default WSNBTTagCompound writeToData(WSNBTTagCompound compound) {
		return compound;
	}

	@Override
	default WSNBTTagCompound readFromData(WSNBTTagCompound compound) {
		return compound;
	}

}
