package com.degoos.wetsponge.material.block;

import com.degoos.wetsponge.nbt.WSNBTTagCompound;

public interface WSBlockTypeAgeable extends WSBlockType {

	int getAge();

	void setAge(int age);

	int getMaximumAge();

	@Override
	WSBlockTypeAgeable clone();

	@Override
	default WSNBTTagCompound writeToData(WSNBTTagCompound compound) {
		compound.setInteger("age", getAge());
		return compound;
	}

	@Override
	default WSNBTTagCompound readFromData(WSNBTTagCompound compound) {
		setAge(compound.getInteger("age"));
		return compound;
	}
}
