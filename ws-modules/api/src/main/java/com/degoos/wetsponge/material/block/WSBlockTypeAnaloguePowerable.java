package com.degoos.wetsponge.material.block;

import com.degoos.wetsponge.nbt.WSNBTTagCompound;

public interface WSBlockTypeAnaloguePowerable extends WSBlockType {

	int getPower();

	void setPower(int power);

	int gerMaximumPower();

	@Override
	WSBlockTypeAnaloguePowerable clone();

	@Override
	default WSNBTTagCompound writeToData(WSNBTTagCompound compound) {
		compound.setInteger("power", getPower());
		return compound;
	}

	@Override
	default WSNBTTagCompound readFromData(WSNBTTagCompound compound) {
		setPower(compound.getInteger("power"));
		return compound;
	}
}
