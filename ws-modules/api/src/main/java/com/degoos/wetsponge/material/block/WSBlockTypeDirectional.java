package com.degoos.wetsponge.material.block;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;

import java.util.Set;

public interface WSBlockTypeDirectional extends WSBlockType {

	EnumBlockFace getFacing();

	void setFacing(EnumBlockFace blockFace);

	Set<EnumBlockFace> getFaces();

	@Override
	WSBlockTypeDirectional clone();

	@Override
	default WSNBTTagCompound writeToData(WSNBTTagCompound compound) {
		compound.setString("facing", getFacing().name());
		return compound;
	}

	@Override
	default WSNBTTagCompound readFromData(WSNBTTagCompound compound) {
		setFacing(EnumBlockFace.valueOf(compound.getString("facing")));
		return compound;
	}
}
