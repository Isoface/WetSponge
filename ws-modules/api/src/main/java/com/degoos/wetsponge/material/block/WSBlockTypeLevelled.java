package com.degoos.wetsponge.material.block;

import com.degoos.wetsponge.nbt.WSNBTTagCompound;

public interface WSBlockTypeLevelled extends WSBlockType {

	int getLevel();

	void setLevel(int level);

	int getMaximumLevel();

	@Override
	WSBlockTypeLevelled clone();

	@Override
	default WSNBTTagCompound writeToData(WSNBTTagCompound compound) {
		compound.setInteger("level", getLevel());
		return compound;
	}

	@Override
	default WSNBTTagCompound readFromData(WSNBTTagCompound compound) {
		setLevel(compound.getInteger("level"));
		return compound;
	}
}
