package com.degoos.wetsponge.material.block;

import com.degoos.wetsponge.nbt.WSNBTTagCompound;

public interface WSBlockTypeLightable extends WSBlockType {

	boolean isLit();

	void setLit(boolean lit);

	@Override
	WSBlockTypeLightable clone();

	@Override
	default WSNBTTagCompound writeToData(WSNBTTagCompound compound) {
		compound.setBoolean("lit", isLit());
		return compound;
	}

	@Override
	default WSNBTTagCompound readFromData(WSNBTTagCompound compound) {
		setLit(compound.getBoolean("lit"));
		return compound;
	}
}
