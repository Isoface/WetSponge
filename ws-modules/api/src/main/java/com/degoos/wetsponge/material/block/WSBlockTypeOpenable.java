package com.degoos.wetsponge.material.block;

import com.degoos.wetsponge.nbt.WSNBTTagCompound;

public interface WSBlockTypeOpenable extends WSBlockType {

	boolean isOpen();

	void setOpen(boolean open);

	@Override
	WSBlockTypeOpenable clone();

	@Override
	default WSNBTTagCompound writeToData(WSNBTTagCompound compound) {
		compound.setBoolean("open", isOpen());
		return compound;
	}

	@Override
	default WSNBTTagCompound readFromData(WSNBTTagCompound compound) {
		setOpen(compound.getBoolean("open"));
		return compound;
	}
}
