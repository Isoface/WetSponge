package com.degoos.wetsponge.material.block;

import com.degoos.wetsponge.enums.block.EnumAxis;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;

import java.util.Set;

public interface WSBlockTypeOrientable extends WSBlockType {

	EnumAxis getAxis();

	void setAxis(EnumAxis axis);

	Set<EnumAxis> getAxes();

	@Override
	WSBlockTypeOrientable clone();

	@Override
	default WSNBTTagCompound writeToData(WSNBTTagCompound compound) {
		compound.setString("axis", getAxis().name());
		return compound;
	}

	@Override
	default WSNBTTagCompound readFromData(WSNBTTagCompound compound) {
		setAxis(EnumAxis.valueOf(compound.getString("axis")));
		return compound;
	}
}
