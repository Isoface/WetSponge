package com.degoos.wetsponge.material.block;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;

public interface WSBlockTypeRotatable extends WSBlockType {

	EnumBlockFace getRotation();

	void setRotation(EnumBlockFace rotation);

	@Override
	WSBlockTypeRotatable clone();

	@Override
	default WSNBTTagCompound writeToData(WSNBTTagCompound compound) {
		compound.setString("direction", getRotation().name());
		return compound;
	}

	@Override
	default WSNBTTagCompound readFromData(WSNBTTagCompound compound) {
		setRotation(EnumBlockFace.valueOf(compound.getString("direction")));
		return compound;
	}
}
