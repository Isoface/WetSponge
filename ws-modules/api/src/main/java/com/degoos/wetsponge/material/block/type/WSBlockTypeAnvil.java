package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.enums.block.EnumBlockTypeAnvilDamage;
import com.degoos.wetsponge.material.block.WSBlockTypeDirectional;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;

public interface WSBlockTypeAnvil extends WSBlockTypeDirectional {

	EnumBlockTypeAnvilDamage getDamage();

	void setDamage(EnumBlockTypeAnvilDamage damage);

	@Override
	WSBlockTypeAnvil clone();

	@Override
	default WSNBTTagCompound writeToData(WSNBTTagCompound compound) {
		compound.setString("facing", getFacing().name());
		compound.setString("damage", getDamage().name());
		return compound;
	}

	@Override
	default WSNBTTagCompound readFromData(WSNBTTagCompound compound) {
		setFacing(EnumBlockFace.valueOf(compound.getString("facing")));
		setDamage(EnumBlockTypeAnvilDamage.valueOf(compound.getString("damage")));
		return compound;
	}
}
