package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.EnumDyeColor;
import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.enums.block.EnumBlockTypeBedPart;
import com.degoos.wetsponge.material.block.WSBlockTypeDirectional;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;

public interface WSBlockTypeBed extends WSBlockTypeDirectional, WSBlockTypeDyeColored {

	EnumBlockTypeBedPart getPart();

	void setPart(EnumBlockTypeBedPart part);

	boolean isOccupied();

	@Override
	WSBlockTypeBed clone();

	@Override
	default WSNBTTagCompound writeToData(WSNBTTagCompound compound) {
		compound.setString("facing", getFacing().name());
		compound.setString("color", getDyeColor().name());
		compound.setString("part", getPart().name());
		return compound;
	}

	@Override
	default WSNBTTagCompound readFromData(WSNBTTagCompound compound) {
		setFacing(EnumBlockFace.valueOf(compound.getString("facing")));
		setDyeColor(EnumDyeColor.valueOf(compound.getString("color")));
		setPart(EnumBlockTypeBedPart.valueOf(compound.getString("part")));
		return compound;
	}
}
