package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.material.block.WSBlockType;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;
import com.degoos.wetsponge.nbt.WSNBTTagInt;
import com.degoos.wetsponge.nbt.WSNBTTagList;

import java.util.Set;

public interface WSBlockTypeBrewingStand extends WSBlockType {

	boolean hasBottle(int index);

	void setBottle(int index, boolean bottle);

	Set<Integer> getBottles();

	int getMaximumBottles();

	@Override
	WSBlockTypeBrewingStand clone();

	@Override
	default WSNBTTagCompound writeToData(WSNBTTagCompound compound) {
		WSNBTTagList list = WSNBTTagList.of();

		getBottles().forEach(target -> list.appendTag(WSNBTTagInt.of(target)));
		compound.setTag("bottles", list);
		return compound;
	}

	@Override
	default WSNBTTagCompound readFromData(WSNBTTagCompound compound) {
		getBottles().forEach(target -> setBottle(target, false));
		WSNBTTagList list = compound.getTagList("bottles", 3);
		for (int i = 0; i < list.tagCount(); i++)
			setBottle(list.getIntAt(i), true);
		return compound;
	}
}
