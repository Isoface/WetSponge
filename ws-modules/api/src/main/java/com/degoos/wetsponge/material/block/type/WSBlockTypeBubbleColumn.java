package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.material.block.WSBlockType;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;

public interface WSBlockTypeBubbleColumn extends WSBlockType {

	boolean isDrag();

	void setDrag(boolean drag);

	@Override
	WSBlockTypeBubbleColumn clone();

	@Override
	default WSNBTTagCompound writeToData(WSNBTTagCompound compound) {
		compound.setBoolean("drag", isDrag());
		return compound;
	}

	@Override
	default WSNBTTagCompound readFromData(WSNBTTagCompound compound) {
		setDrag(compound.getBoolean("drag"));
		return compound;
	}
}
