package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;
import com.degoos.wetsponge.nbt.WSNBTTagList;
import com.degoos.wetsponge.nbt.WSNBTTagString;

public interface WSBlockTypeCobblestoneWall extends WSBlockTypeFence {

	boolean isMossy();

	void setMossy(boolean mossy);

	@Override
	WSBlockTypeCobblestoneWall clone();

	@Override
	default WSNBTTagCompound writeToData(WSNBTTagCompound compound) {
		WSNBTTagList list = WSNBTTagList.of();
		getFaces().forEach(target -> list.appendTag(WSNBTTagString.of(target.name())));
		compound.setTag("faces", list);
		compound.setBoolean("waterlogged", isWaterlogged());
		compound.setBoolean("mossy", isMossy());
		return compound;
	}

	@Override
	default WSNBTTagCompound readFromData(WSNBTTagCompound compound) {
		getFaces().forEach(target -> setFace(target, false));
		WSNBTTagList list = compound.getTagList("faces", 8);
		for (int i = 0; i < list.tagCount(); i++)
			setFace(EnumBlockFace.valueOf(list.getStringAt(i)), true);
		setWaterlogged(compound.getBoolean("waterlogged"));
		setMossy(compound.getBoolean("mossy"));
		return compound;
	}
}
