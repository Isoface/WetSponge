package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.material.block.WSBlockTypeAgeable;
import com.degoos.wetsponge.material.block.WSBlockTypeDirectional;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;

public interface WSBlockTypeCocoa extends WSBlockTypeAgeable, WSBlockTypeDirectional {

	@Override
	WSBlockTypeCocoa clone();

	@Override
	default WSNBTTagCompound writeToData(WSNBTTagCompound compound) {
		compound.setInteger("age", getAge());
		compound.setString("facing", getFacing().name());
		return compound;
	}

	@Override
	default WSNBTTagCompound readFromData(WSNBTTagCompound compound) {
		setAge(compound.getInteger("age"));
		setFacing(EnumBlockFace.valueOf(compound.getString("facing")));
		return compound;
	}
}
