package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.material.block.WSBlockTypeDirectional;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;

public interface WSBlockTypeCommandBlock extends WSBlockTypeDirectional {

	boolean isConditional();

	void setConditional(boolean conditional);

	@Override
	WSBlockTypeCommandBlock clone();

	@Override
	default WSNBTTagCompound writeToData(WSNBTTagCompound compound) {
		compound.setString("facing", getFacing().name());
		compound.setBoolean("conditional", isConditional());
		return compound;
	}

	@Override
	default WSNBTTagCompound readFromData(WSNBTTagCompound compound) {
		setFacing(EnumBlockFace.valueOf(compound.getString("facing")));
		setConditional(compound.getBoolean("conditional"));
		return compound;
	}
}
