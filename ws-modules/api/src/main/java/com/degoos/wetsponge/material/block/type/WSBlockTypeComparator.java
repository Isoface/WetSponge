package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.enums.block.EnumBlockTypeComparatorMode;
import com.degoos.wetsponge.material.block.WSBlockTypeDirectional;
import com.degoos.wetsponge.material.block.WSBlockTypePowerable;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;

public interface WSBlockTypeComparator extends WSBlockTypeDirectional, WSBlockTypePowerable {

	EnumBlockTypeComparatorMode getMode();

	void setMode(EnumBlockTypeComparatorMode mode);

	@Override
	WSBlockTypeComparator clone();

	@Override
	default WSNBTTagCompound writeToData(WSNBTTagCompound compound) {
		compound.setString("facing", getFacing().name());
		compound.setBoolean("powered", isPowered());
		compound.setString("mode", getMode().name());
		return compound;
	}

	@Override
	default WSNBTTagCompound readFromData(WSNBTTagCompound compound) {
		setFacing(EnumBlockFace.valueOf(compound.getString("facing")));
		setPowered(compound.getBoolean("powered"));
		setMode(EnumBlockTypeComparatorMode.valueOf(compound.getString("mode")));
		return compound;
	}
}
