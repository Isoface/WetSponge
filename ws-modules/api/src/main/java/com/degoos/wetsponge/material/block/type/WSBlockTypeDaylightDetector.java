package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.material.block.WSBlockTypeAnaloguePowerable;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;

public interface WSBlockTypeDaylightDetector extends WSBlockTypeAnaloguePowerable {

	boolean isInverted();

	void setInverted(boolean inverted);

	@Override
	WSBlockTypeDaylightDetector clone();

	@Override
	default WSNBTTagCompound writeToData(WSNBTTagCompound compound) {
		compound.setInteger("power", getPower());
		compound.setBoolean("inverted", isInverted());
		return compound;
	}

	@Override
	default WSNBTTagCompound readFromData(WSNBTTagCompound compound) {
		setPower(compound.getInteger("power"));
		setInverted(compound.getBoolean("inverted"));
		return compound;
	}
}
