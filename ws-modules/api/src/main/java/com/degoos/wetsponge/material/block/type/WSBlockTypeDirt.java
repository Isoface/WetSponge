package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockTypeDirtType;
import com.degoos.wetsponge.material.block.WSBlockType;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;

public interface WSBlockTypeDirt extends WSBlockType {

	EnumBlockTypeDirtType getDirtType();

	void setDirtType(EnumBlockTypeDirtType dirtType);

	@Override
	WSBlockTypeDirt clone();

	@Override
	default WSNBTTagCompound writeToData(WSNBTTagCompound compound) {
		compound.setString("dirtType", getDirtType().name());
		return compound;
	}

	@Override
	default WSNBTTagCompound readFromData(WSNBTTagCompound compound) {
		setDirtType(EnumBlockTypeDirtType.valueOf(compound.getString("dirtType")));
		return compound;
	}
}
