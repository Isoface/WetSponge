package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.enums.block.EnumBlockTypeBisectedHalf;
import com.degoos.wetsponge.enums.block.EnumBlockTypeDoorHinge;
import com.degoos.wetsponge.material.block.WSBlockTypeBisected;
import com.degoos.wetsponge.material.block.WSBlockTypeDirectional;
import com.degoos.wetsponge.material.block.WSBlockTypeOpenable;
import com.degoos.wetsponge.material.block.WSBlockTypePowerable;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;

public interface WSBlockTypeDoor extends WSBlockTypeBisected, WSBlockTypeDirectional, WSBlockTypeOpenable, WSBlockTypePowerable {

	EnumBlockTypeDoorHinge getHinge();

	void setHinge(EnumBlockTypeDoorHinge hinge);

	@Override
	WSBlockTypeDoor clone();

	@Override
	default WSNBTTagCompound writeToData(WSNBTTagCompound compound) {
		compound.setString("half", getHalf().name());
		compound.setString("facing", getFacing().name());
		compound.setBoolean("open", isOpen());
		compound.setBoolean("powered", isPowered());
		compound.setString("hinge", getHinge().name());
		return compound;
	}

	@Override
	default WSNBTTagCompound readFromData(WSNBTTagCompound compound) {
		setHalf(EnumBlockTypeBisectedHalf.valueOf(compound.getString("half")));
		setFacing(EnumBlockFace.valueOf(compound.getString("facing")));
		setOpen(compound.getBoolean("open"));
		setPowered(compound.getBoolean("powered"));
		setHinge(EnumBlockTypeDoorHinge.valueOf(compound.getString("hinge")));
		return compound;
	}
}
