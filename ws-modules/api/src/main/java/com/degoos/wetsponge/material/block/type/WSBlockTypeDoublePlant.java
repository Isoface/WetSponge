package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockTypeBisectedHalf;
import com.degoos.wetsponge.enums.block.EnumBlockTypeDoublePlantType;
import com.degoos.wetsponge.material.block.WSBlockTypeBisected;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;

public interface WSBlockTypeDoublePlant extends WSBlockTypeBisected {

	EnumBlockTypeDoublePlantType getDoublePlantType();

	void setDoublePlantType(EnumBlockTypeDoublePlantType doublePlantType);

	@Override
	WSBlockTypeDoublePlant clone();

	@Override
	default WSNBTTagCompound writeToData(WSNBTTagCompound compound) {
		compound.setString("half", getHalf().name());
		compound.setString("doublePlantType", getDoublePlantType().name());
		return compound;
	}

	@Override
	default WSNBTTagCompound readFromData(WSNBTTagCompound compound) {
		setHalf(EnumBlockTypeBisectedHalf.valueOf(compound.getString("half")));
		setDoublePlantType(EnumBlockTypeDoublePlantType.valueOf(compound.getString("doublePlantHinge")));
		return compound;
	}
}
