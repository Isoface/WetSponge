package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.EnumDyeColor;
import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.material.block.WSBlockType;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;

public interface WSBlockTypeDyeColored extends WSBlockType {

	EnumDyeColor getDyeColor();

	void setDyeColor(EnumDyeColor dyeColor);

	@Override
	WSBlockTypeDyeColored clone();

	@Override
	default WSNBTTagCompound writeToData(WSNBTTagCompound compound) {
		compound.setString("color", getDyeColor().name());
		return compound;
	}

	@Override
	default WSNBTTagCompound readFromData(WSNBTTagCompound compound) {
		setDyeColor(EnumDyeColor.valueOf(compound.getString("color")));
		return compound;
	}
}
