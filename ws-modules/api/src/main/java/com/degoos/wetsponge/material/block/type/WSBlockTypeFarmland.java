package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.material.block.WSBlockType;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;

public interface WSBlockTypeFarmland extends WSBlockType {

	int getMoisture();

	void setMoisture(int moisture);

	int getMaximumMoisture();

	@Override
	WSBlockTypeFarmland clone();

	@Override
	default WSNBTTagCompound writeToData(WSNBTTagCompound compound) {
		compound.setInteger("moisture", getMoisture());
		return compound;
	}

	@Override
	default WSNBTTagCompound readFromData(WSNBTTagCompound compound) {
		setMoisture(compound.getInteger("moisture"));
		return compound;
	}
}
