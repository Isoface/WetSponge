package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.enums.block.EnumBlockTypeFlowerType;
import com.degoos.wetsponge.material.block.WSBlockType;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;
import com.degoos.wetsponge.nbt.WSNBTTagList;
import com.degoos.wetsponge.nbt.WSNBTTagString;

public interface WSBlockTypeFlower extends WSBlockType {

	EnumBlockTypeFlowerType getFlowerType();

	void setFlowerType(EnumBlockTypeFlowerType flowerType);

	@Override
	WSBlockTypeFlower clone();

	@Override
	default WSNBTTagCompound writeToData(WSNBTTagCompound compound) {
		compound.setString("flowerType", getFlowerType().name());
		return compound;
	}

	@Override
	default WSNBTTagCompound readFromData(WSNBTTagCompound compound) {
		setFlowerType(EnumBlockTypeFlowerType.valueOf(compound.getString("flowerType")));
		return compound;
	}
}