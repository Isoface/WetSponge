package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockTypePottedPlant;
import com.degoos.wetsponge.material.block.WSBlockType;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;

public interface WSBlockTypeFlowerPot extends WSBlockType {

	EnumBlockTypePottedPlant getPottedPlant();

	void setPottedPlant(EnumBlockTypePottedPlant pottedPlant);

	@Override
	WSBlockTypeFlowerPot clone();

	@Override
	default WSNBTTagCompound writeToData(WSNBTTagCompound compound) {
		compound.setString("pottedPlant", getPottedPlant().name());
		return compound;
	}

	@Override
	default WSNBTTagCompound readFromData(WSNBTTagCompound compound) {
		setPottedPlant(EnumBlockTypePottedPlant.valueOf(compound.getString("pottedPlant")));
		return compound;
	}
}
