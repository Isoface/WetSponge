package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockTypeDisguiseType;
import com.degoos.wetsponge.material.block.WSBlockType;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;

public interface WSBlockTypeInfestedStone extends WSBlockType {

	EnumBlockTypeDisguiseType getDisguiseType();

	void setDisguiseType(EnumBlockTypeDisguiseType disguiseType);

	@Override
	WSBlockTypeInfestedStone clone();

	@Override
	default WSNBTTagCompound writeToData(WSNBTTagCompound compound) {
		compound.setString("disguiseType", getDisguiseType().name());
		return compound;
	}

	@Override
	default WSNBTTagCompound readFromData(WSNBTTagCompound compound) {
		setDisguiseType(EnumBlockTypeDisguiseType.valueOf(compound.getString("disguiseType")));
		return compound;
	}
}
