package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.material.block.WSBlockType;

public interface WSBlockTypeJukebox extends WSBlockType {

	boolean hasRecord();

	@Override
	WSBlockTypeJukebox clone();
}
