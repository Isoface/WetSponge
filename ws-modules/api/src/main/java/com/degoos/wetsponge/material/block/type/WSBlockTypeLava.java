package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.material.block.WSBlockTypeLevelled;

public interface WSBlockTypeLava extends WSBlockTypeLevelled {

	@Override
	WSBlockTypeLava clone();
}
