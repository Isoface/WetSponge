package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumWoodType;
import com.degoos.wetsponge.material.block.WSBlockType;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;

public interface WSBlockTypeLeaves extends WSBlockType {

	boolean isPersistent();

	void setPersistent(boolean persistent);

	int getDistance();

	void setDistance(int distance);

	EnumWoodType getWoodType();

	void setWoodType(EnumWoodType woodType);

	@Override
	WSBlockTypeLeaves clone();

	@Override
	default WSNBTTagCompound writeToData(WSNBTTagCompound compound) {
		compound.setBoolean("persistent", isPersistent());
		compound.setInteger("distance", getDistance());
		compound.setString("woodType", getWoodType().name());
		return compound;
	}

	@Override
	default WSNBTTagCompound readFromData(WSNBTTagCompound compound) {
		setPersistent(compound.getBoolean("persistent"));
		setDistance(compound.getInteger("distance"));
		setWoodType(EnumWoodType.valueOf(compound.getString("woodType")));
		return compound;
	}
}
