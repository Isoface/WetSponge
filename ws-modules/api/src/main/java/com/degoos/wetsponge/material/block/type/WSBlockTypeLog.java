package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumAxis;
import com.degoos.wetsponge.enums.block.EnumWoodType;
import com.degoos.wetsponge.material.block.WSBlockTypeOrientable;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;

public interface WSBlockTypeLog extends WSBlockTypeOrientable {

	EnumWoodType getWoodType();

	void setWoodType(EnumWoodType woodType);

	boolean isStripped();

	void setStripped(boolean stripped);

	boolean hasAllFacesCovered();

	void setAllFacesCovered(boolean allFacesCovered);

	@Override
	WSBlockTypeLog clone();

	@Override
	default WSNBTTagCompound writeToData(WSNBTTagCompound compound) {
		compound.setString("axis", getAxis().name());
		compound.setString("woodType", getWoodType().name());
		compound.setBoolean("stripped", isStripped());
		compound.setBoolean("allFacesCovered", hasAllFacesCovered());
		return compound;
	}

	@Override
	default WSNBTTagCompound readFromData(WSNBTTagCompound compound) {
		setAxis(EnumAxis.valueOf(compound.getString("axis")));
		setWoodType(EnumWoodType.valueOf(compound.getString("woodType")));
		setStripped(compound.getBoolean("stripped"));
		setAllFacesCovered(compound.getBoolean("allFacesCovered"));
		return compound;
	}
}
