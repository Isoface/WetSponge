package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.material.block.WSBlockTypeDirectional;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;

public interface WSBlockTypePiston extends WSBlockTypeDirectional {

	boolean isExtended();

	void setExtended(boolean extended);

	@Override
	WSBlockTypePiston clone();

	@Override
	default WSNBTTagCompound writeToData(WSNBTTagCompound compound) {
		compound.setString("facing", getFacing().name());
		compound.setBoolean("extended", isExtended());
		return compound;
	}

	@Override
	default WSNBTTagCompound readFromData(WSNBTTagCompound compound) {
		setFacing(EnumBlockFace.valueOf(compound.getString("facing")));
		setExtended(compound.getBoolean("extended"));
		return compound;
	}
}
