package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.enums.block.EnumBlockTypePistonType;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;

public interface WSBlockTypePistonHead extends WSBlockTypeTechnicalPiston {

	boolean isShort();

	void setShort(boolean s);

	@Override
	WSBlockTypePistonHead clone();

	@Override
	default WSNBTTagCompound writeToData(WSNBTTagCompound compound) {
		compound.setString("facing", getFacing().name());
		compound.setString("pistonType", getType().name());
		compound.setBoolean("short", isShort());
		return compound;
	}

	@Override
	default WSNBTTagCompound readFromData(WSNBTTagCompound compound) {
		setFacing(EnumBlockFace.valueOf(compound.getString("facing")));
		setType(EnumBlockTypePistonType.valueOf(compound.getString("pistonType")));
		setShort(compound.getBoolean("short"));
		return compound;
	}
}
