package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockTypePrismarineType;
import com.degoos.wetsponge.material.block.WSBlockType;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;

public interface WSBlockTypePrismarine extends WSBlockType {

	EnumBlockTypePrismarineType getPrismarineType();

	void setPrismarineType(EnumBlockTypePrismarineType prismarineType);

	@Override
	WSBlockTypePrismarine clone();

	@Override
	default WSNBTTagCompound writeToData(WSNBTTagCompound compound) {
		compound.setString("prismarineType", getPrismarineType().name());
		return compound;
	}

	@Override
	default WSNBTTagCompound readFromData(WSNBTTagCompound compound) {
		setPrismarineType(EnumBlockTypePrismarineType.valueOf(compound.getString("prismarineType")));
		return compound;
	}
}
