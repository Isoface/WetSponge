package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumAxis;
import com.degoos.wetsponge.enums.block.EnumBlockTypeQuartzType;
import com.degoos.wetsponge.material.block.WSBlockTypeOrientable;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;

public interface WSBlockTypeQuartz extends WSBlockTypeOrientable {

	EnumBlockTypeQuartzType getQuartzType();

	void setQuartzType(EnumBlockTypeQuartzType quartzType);

	@Override
	WSBlockTypeQuartz clone();

	@Override
	default WSNBTTagCompound writeToData(WSNBTTagCompound compound) {
		compound.setString("axis", getAxis().name());
		compound.setString("quartzType", getQuartzType().name());
		return compound;
	}

	@Override
	default WSNBTTagCompound readFromData(WSNBTTagCompound compound) {
		setAxis(EnumAxis.valueOf(compound.getString("axis")));
		setQuartzType(EnumBlockTypeQuartzType.valueOf(compound.getString("quartzType")));
		return compound;
	}
}
