package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.material.block.WSBlockTypeLightable;

public interface WSBlockTypeRedstoneOre extends WSBlockTypeLightable {

	@Override
	WSBlockTypeRedstoneOre clone();
}
