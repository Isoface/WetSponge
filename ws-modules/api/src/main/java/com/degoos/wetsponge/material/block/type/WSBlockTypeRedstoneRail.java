package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockTypeRailShape;
import com.degoos.wetsponge.material.block.WSBlockTypePowerable;
import com.degoos.wetsponge.material.block.WSBlockTypeRail;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;

public interface WSBlockTypeRedstoneRail extends WSBlockTypePowerable, WSBlockTypeRail {

	@Override
	WSBlockTypeRedstoneRail clone();

	@Override
	default WSNBTTagCompound writeToData(WSNBTTagCompound compound) {
		compound.setBoolean("powered", isPowered());
		compound.setString("shape", getShape().name());
		return compound;
	}

	@Override
	default WSNBTTagCompound readFromData(WSNBTTagCompound compound) {
		setPowered(compound.getBoolean("powered"));
		setShape(EnumBlockTypeRailShape.valueOf(compound.getString("shape")));
		return compound;
	}
}
