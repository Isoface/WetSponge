package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.enums.block.EnumBlockTypeRedstoneWireConnection;
import com.degoos.wetsponge.material.block.WSBlockTypeAnaloguePowerable;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;

import java.util.Set;

public interface WSBlockTypeRedstoneWire extends WSBlockTypeAnaloguePowerable {

	EnumBlockTypeRedstoneWireConnection getFaceConnection(EnumBlockFace face);

	void setFaceConnection(EnumBlockFace face, EnumBlockTypeRedstoneWireConnection connection);

	Set<EnumBlockFace> getAllowedFaces();

	@Override
	WSBlockTypeRedstoneWire clone();

	@Override
	default WSNBTTagCompound writeToData(WSNBTTagCompound compound) {
		compound.setInteger("power", getPower());

		WSNBTTagCompound list = WSNBTTagCompound.of();
		getAllowedFaces().forEach(target -> {
			EnumBlockTypeRedstoneWireConnection connection = getFaceConnection(target);
			list.setString(target.name(), connection.name());
		});
		compound.setTag("connections", list);
		return compound;
	}

	@Override
	default WSNBTTagCompound readFromData(WSNBTTagCompound compound) {
		setPower(compound.getInteger("power"));
		getAllowedFaces().forEach(target -> setFaceConnection(target, EnumBlockTypeRedstoneWireConnection.NONE));
		WSNBTTagCompound list = compound.getCompoundTag("connections");
		list.getKeySet().forEach(target -> setFaceConnection(EnumBlockFace.valueOf(target), EnumBlockTypeRedstoneWireConnection.valueOf(list.getString(target))));
		return compound;
	}
}
