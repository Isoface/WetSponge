package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.material.block.WSBlockTypeDirectional;
import com.degoos.wetsponge.material.block.WSBlockTypePowerable;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;

public interface WSBlockTypeRepeater extends WSBlockTypeDirectional, WSBlockTypePowerable {

	int getDelay();

	void setDelay(int delay);

	int getMinimumDelay();

	int getMaximumDelay();

	boolean isLocked();

	void setLocked(boolean locked);

	@Override
	WSBlockTypeRepeater clone();

	@Override
	default WSNBTTagCompound writeToData(WSNBTTagCompound compound) {
		compound.setString("facing", getFacing().name());
		compound.setBoolean("powered", isPowered());
		compound.setInteger("delay", getDelay());
		compound.setBoolean("locked", isLocked());
		return compound;
	}

	@Override
	default WSNBTTagCompound readFromData(WSNBTTagCompound compound) {
		setFacing(EnumBlockFace.valueOf(compound.getString("facing")));
		setPowered(compound.getBoolean("powered"));
		setDelay(compound.getInteger("delay"));
		setLocked(compound.getBoolean("locked"));
		return compound;
	}
}
