package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockTypeSandType;
import com.degoos.wetsponge.enums.block.EnumBlockTypeSandstoneType;
import com.degoos.wetsponge.material.block.WSBlockType;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;

public interface WSBlockTypeSandstone extends WSBlockType {

	EnumBlockTypeSandType getSandType();

	void setSandType(EnumBlockTypeSandType sandType);

	EnumBlockTypeSandstoneType getSandstoneType();

	void setSandstoneType(EnumBlockTypeSandstoneType sandstoneType);

	@Override
	WSBlockTypeSandstone clone();

	@Override
	default WSNBTTagCompound writeToData(WSNBTTagCompound compound) {
		compound.setString("sandType", getSandType().name());
		compound.setString("sandstoneType", getSandstoneType().name());
		return compound;
	}

	@Override
	default WSNBTTagCompound readFromData(WSNBTTagCompound compound) {
		setSandType(EnumBlockTypeSandType.valueOf(compound.getString("sandType")));
		setSandstoneType(EnumBlockTypeSandstoneType.valueOf(compound.getString("sandstoneType")));
		return compound;
	}
}
