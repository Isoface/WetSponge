package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.material.block.WSBlockTypeWaterlogged;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;

public interface WSBlockTypeSeaPickle extends WSBlockTypeWaterlogged {

	int getPickles();

	void setPickles(int pickles);

	int getMinimumPickles();

	int getMaximumPickles();

	@Override
	WSBlockTypeSeaPickle clone();

	@Override
	default WSNBTTagCompound writeToData(WSNBTTagCompound compound) {
		compound.setBoolean("waterlogged", isWaterlogged());
		compound.setInteger("pickles", getPickles());
		return compound;
	}

	@Override
	default WSNBTTagCompound readFromData(WSNBTTagCompound compound) {
		setWaterlogged(compound.getBoolean("waterlogged"));
		setPickles(compound.getInteger("pickles"));
		return compound;
	}
}
