package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.material.block.WSBlockTypeRotatable;
import com.degoos.wetsponge.material.block.WSBlockTypeWaterlogged;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;

public interface WSBlockTypeSign extends WSBlockTypeRotatable, WSBlockTypeWaterlogged {

	@Override
	WSBlockTypeSign clone();

	@Override
	default WSNBTTagCompound writeToData(WSNBTTagCompound compound) {
		compound.setString("direction", getRotation().name());
		compound.setBoolean("waterlogged", isWaterlogged());
		return compound;
	}

	@Override
	default WSNBTTagCompound readFromData(WSNBTTagCompound compound) {
		setRotation(EnumBlockFace.valueOf(compound.getString("direction")));
		setWaterlogged(compound.getBoolean("waterlogged"));
		return compound;
	}
}
