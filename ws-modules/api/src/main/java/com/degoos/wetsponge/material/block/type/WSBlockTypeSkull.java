package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.enums.block.EnumBlockTypeSkullType;
import com.degoos.wetsponge.material.block.WSBlockTypeDirectional;
import com.degoos.wetsponge.material.block.WSBlockTypeRotatable;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;

public interface WSBlockTypeSkull extends WSBlockTypeDirectional, WSBlockTypeRotatable {

	EnumBlockTypeSkullType getSkullType();

	void setSkullType(EnumBlockTypeSkullType skullType);

	@Override
	WSBlockTypeSkull clone();

	@Override
	default WSNBTTagCompound writeToData(WSNBTTagCompound compound) {
		compound.setString("facing", getFacing().name());
		compound.setString("direction", getRotation().name());
		compound.setString("skullType", getSkullType().name());
		return compound;
	}

	@Override
	default WSNBTTagCompound readFromData(WSNBTTagCompound compound) {
		setFacing(EnumBlockFace.valueOf(compound.getString("facing")));
		setRotation(EnumBlockFace.valueOf(compound.getString("direction")));
		setSkullType(EnumBlockTypeSkullType.valueOf(compound.getString("skullType")));
		return compound;
	}
}
