package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockTypeSlabPosition;
import com.degoos.wetsponge.enums.block.EnumBlockTypeSlabType;
import com.degoos.wetsponge.material.block.WSBlockTypeWaterlogged;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;

public interface WSBlockTypeSlab extends WSBlockTypeWaterlogged {

	EnumBlockTypeSlabType getType();

	void setType(EnumBlockTypeSlabType type);

	EnumBlockTypeSlabPosition getPosition();

	void setPosition(EnumBlockTypeSlabPosition position);

	@Override
	WSBlockTypeSlab clone();

	@Override
	default WSNBTTagCompound writeToData(WSNBTTagCompound compound) {
		compound.setBoolean("waterlogged", isWaterlogged());
		compound.setString("slabType", getType().name());
		compound.setString("slabPosition", getType().name());
		return compound;
	}

	@Override
	default WSNBTTagCompound readFromData(WSNBTTagCompound compound) {
		setWaterlogged(compound.getBoolean("waterlogged"));
		setType(EnumBlockTypeSlabType.valueOf(compound.getString("slabType")));
		setPosition(EnumBlockTypeSlabPosition.valueOf(compound.getString("slabPosition")));
		return compound;
	}
}
