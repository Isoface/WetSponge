package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.material.block.WSBlockType;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;

public interface WSBlockTypeSnow extends WSBlockType {

	int getLayers();

	void setLayers(int layers);

	int getMinimumLayers();

	int getMaximumLayers();

	@Override
	WSBlockTypeSnow clone();

	@Override
	default WSNBTTagCompound writeToData(WSNBTTagCompound compound) {
		compound.setInteger("layers", getLayers());
		return compound;
	}

	@Override
	default WSNBTTagCompound readFromData(WSNBTTagCompound compound) {
		setLayers(compound.getInteger("layers"));
		return compound;
	}
}
