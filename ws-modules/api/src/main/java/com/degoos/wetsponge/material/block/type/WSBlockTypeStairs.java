package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.enums.block.EnumBlockTypeBisectedHalf;
import com.degoos.wetsponge.enums.block.EnumBlockTypeStairShape;
import com.degoos.wetsponge.material.block.WSBlockTypeBisected;
import com.degoos.wetsponge.material.block.WSBlockTypeDirectional;
import com.degoos.wetsponge.material.block.WSBlockTypeWaterlogged;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;

public interface WSBlockTypeStairs extends WSBlockTypeBisected, WSBlockTypeDirectional, WSBlockTypeWaterlogged {

	EnumBlockTypeStairShape getShape();

	void setShape(EnumBlockTypeStairShape shape);

	@Override
	WSBlockTypeStairs clone();

	@Override
	default WSNBTTagCompound writeToData(WSNBTTagCompound compound) {
		compound.setString("half", getHalf().name());
		compound.setString("facing", getFacing().name());
		compound.setBoolean("waterlogged", isWaterlogged());
		compound.setString("shape", getShape().name());
		return compound;
	}

	@Override
	default WSNBTTagCompound readFromData(WSNBTTagCompound compound) {
		setHalf(EnumBlockTypeBisectedHalf.valueOf(compound.getString("half")));
		setFacing(EnumBlockFace.valueOf(compound.getString("facing")));
		setWaterlogged(compound.getBoolean("waterlogged"));
		setShape(EnumBlockTypeStairShape.valueOf(compound.getString("shape")));
		return compound;
	}
}
