package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockTypeStructureBlockMode;
import com.degoos.wetsponge.material.block.WSBlockType;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;

public interface WSBlockTypeStructureBlock extends WSBlockType {

	EnumBlockTypeStructureBlockMode getMode();

	void setMode(EnumBlockTypeStructureBlockMode mode);

	@Override
	WSBlockTypeStructureBlock clone();

	@Override
	default WSNBTTagCompound writeToData(WSNBTTagCompound compound) {
		compound.setString("mode", getMode().name());
		return compound;
	}

	@Override
	default WSNBTTagCompound readFromData(WSNBTTagCompound compound) {
		setMode(EnumBlockTypeStructureBlockMode.valueOf(compound.getString("mode")));
		return compound;
	}
}
