package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.enums.block.EnumBlockTypePistonType;
import com.degoos.wetsponge.material.block.WSBlockTypeDirectional;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;

public interface WSBlockTypeTechnicalPiston extends WSBlockTypeDirectional {

	EnumBlockTypePistonType getType();

	void setType(EnumBlockTypePistonType type);

	@Override
	WSBlockTypeTechnicalPiston clone();

	@Override
	default WSNBTTagCompound writeToData(WSNBTTagCompound compound) {
		compound.setString("facing", getFacing().name());
		compound.setString("pistonType", getType().name());
		return compound;
	}

	@Override
	default WSNBTTagCompound readFromData(WSNBTTagCompound compound) {
		setFacing(EnumBlockFace.valueOf(compound.getString("facing")));
		setType(EnumBlockTypePistonType.valueOf(compound.getString("pistonType")));
		return compound;
	}
}
