package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.material.block.WSBlockTypeDirectional;

public interface WSBlockTypeTorch extends WSBlockTypeDirectional {

	@Override
	WSBlockTypeTorch clone();
}
