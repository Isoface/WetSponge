package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.enums.block.EnumBlockTypeBisectedHalf;
import com.degoos.wetsponge.material.block.*;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;

public interface WSBlockTypeTrapDoor extends WSBlockTypeBisected, WSBlockTypeDirectional, WSBlockTypeOpenable, WSBlockTypePowerable, WSBlockTypeWaterlogged {

	@Override
	WSBlockTypeTrapDoor clone();

	@Override
	default WSNBTTagCompound writeToData(WSNBTTagCompound compound) {
		compound.setString("half", getHalf().name());
		compound.setString("facing", getFacing().name());
		compound.setBoolean("open", isOpen());
		compound.setBoolean("powered", isPowered());
		compound.setBoolean("waterlogged", isWaterlogged());
		return compound;
	}

	@Override
	default WSNBTTagCompound readFromData(WSNBTTagCompound compound) {
		setHalf(EnumBlockTypeBisectedHalf.valueOf(compound.getString("half")));
		setFacing(EnumBlockFace.valueOf(compound.getString("facing")));
		setOpen(compound.getBoolean("open"));
		setPowered(compound.getBoolean("powered"));
		setWaterlogged(compound.getBoolean("waterlogged"));
		return compound;
	}
}
