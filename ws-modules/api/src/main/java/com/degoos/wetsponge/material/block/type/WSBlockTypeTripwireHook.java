package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.material.block.WSBlockTypeAttachable;
import com.degoos.wetsponge.material.block.WSBlockTypeDirectional;
import com.degoos.wetsponge.material.block.WSBlockTypePowerable;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;

public interface WSBlockTypeTripwireHook extends WSBlockTypeAttachable, WSBlockTypeDirectional, WSBlockTypePowerable {

	@Override
	WSBlockTypeTripwireHook clone();

	@Override
	default WSNBTTagCompound writeToData(WSNBTTagCompound compound) {
		compound.setBoolean("attached", isAttached());
		compound.setString("facing", getFacing().name());
		compound.setBoolean("powered", isPowered());
		return compound;
	}

	@Override
	default WSNBTTagCompound readFromData(WSNBTTagCompound compound) {
		setAttached(compound.getBoolean("attached"));
		setFacing(EnumBlockFace.valueOf(compound.getString("facing")));
		setPowered(compound.getBoolean("powered"));
		return compound;
	}
}
