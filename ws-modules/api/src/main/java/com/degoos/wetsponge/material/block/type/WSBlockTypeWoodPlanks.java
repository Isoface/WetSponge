package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumWoodType;
import com.degoos.wetsponge.material.block.WSBlockType;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;

public interface WSBlockTypeWoodPlanks extends WSBlockType {

	EnumWoodType getWoodType();

	void setWoodType(EnumWoodType woodType);

	@Override
	WSBlockTypeWoodPlanks clone();

	@Override
	default WSNBTTagCompound writeToData(WSNBTTagCompound compound) {
		compound.setString("woodType", getWoodType().name());
		return compound;
	}

	@Override
	default WSNBTTagCompound readFromData(WSNBTTagCompound compound) {
		setWoodType(EnumWoodType.valueOf(compound.getString("woodType")));
		return compound;
	}
}
