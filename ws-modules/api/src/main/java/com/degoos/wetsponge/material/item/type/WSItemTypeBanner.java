package com.degoos.wetsponge.material.item.type;

import com.degoos.wetsponge.block.tileentity.extra.WSBannerPattern;
import com.degoos.wetsponge.enums.EnumDyeColor;
import com.degoos.wetsponge.enums.block.EnumBannerPatternShape;
import com.degoos.wetsponge.nbt.WSNBTBase;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;
import com.degoos.wetsponge.nbt.WSNBTTagList;

import java.util.ArrayList;
import java.util.List;

public interface WSItemTypeBanner extends WSItemTypeDyeColored {

	List<WSBannerPattern> getPatterns();

	void setPatterns(List<WSBannerPattern> patterns);

	@Override
	WSItemTypeBanner clone();

	@Override
	default WSNBTTagCompound writeToData(WSNBTTagCompound compound) {
		WSNBTTagList list = WSNBTTagList.of();
		getPatterns().forEach(target -> {
			WSNBTTagCompound pattern = WSNBTTagCompound.of();
			pattern.setString("color", target.getColor().name());
			pattern.setString("shape", target.getShape().getCode());
		});
		compound.setTag("patterns", list);
		return compound;
	}

	@Override
	default WSNBTTagCompound readFromData(WSNBTTagCompound compound) {
		List<WSBannerPattern> list = new ArrayList<>();
		if (!compound.hasKey("patterns")) return compound;
		WSNBTBase base = compound.getTag("patterns");
		if (!(base instanceof WSNBTTagList) || ((WSNBTTagList) base).getTagType() != 10) return compound;
		for (int i = 0; i < ((WSNBTTagList) base).tagCount(); i++) {
			WSNBTTagCompound pattern = (WSNBTTagCompound) ((WSNBTTagList) base).get(i);
			list.add(new WSBannerPattern(EnumBannerPatternShape.getByCode(pattern.getString("shape")).orElse(EnumBannerPatternShape.BASE),
					EnumDyeColor.valueOf(pattern.getString("color"))));
		}
		setPatterns(list);
		return compound;
	}
}
