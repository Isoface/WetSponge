package com.degoos.wetsponge.material.item.type;

import com.degoos.wetsponge.enums.item.EnumItemTypeCookedFishType;
import com.degoos.wetsponge.material.item.WSItemType;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;

public interface WSItemTypeCookedFish extends WSItemType {

	EnumItemTypeCookedFishType getCookedFishType();

	void setCookedFishType(EnumItemTypeCookedFishType cookedFishType);

	@Override
	WSItemTypeCookedFish clone();

	@Override
	default WSNBTTagCompound writeToData(WSNBTTagCompound compound) {
		compound.setString("cookedFishType", getCookedFishType().name());
		return compound;
	}

	@Override
	default WSNBTTagCompound readFromData(WSNBTTagCompound compound) {
		setCookedFishType(EnumItemTypeCookedFishType.valueOf(compound.getString("cookedFishType")));
		return compound;
	}
}
