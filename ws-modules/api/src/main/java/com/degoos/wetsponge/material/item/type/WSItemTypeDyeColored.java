package com.degoos.wetsponge.material.item.type;

import com.degoos.wetsponge.enums.EnumDyeColor;
import com.degoos.wetsponge.material.item.WSItemType;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;

public interface WSItemTypeDyeColored extends WSItemType {

	EnumDyeColor getDyeColor();

	void setDyeColor(EnumDyeColor dyeColor);

	@Override
	WSItemTypeDyeColored clone();

	@Override
	default WSNBTTagCompound writeToData(WSNBTTagCompound compound) {
		compound.setString("dyeColor", getDyeColor().name());
		return compound;
	}

	@Override
	default WSNBTTagCompound readFromData(WSNBTTagCompound compound) {
		setDyeColor(EnumDyeColor.valueOf(compound.getString("dyeColor")));
		return compound;
	}
}
