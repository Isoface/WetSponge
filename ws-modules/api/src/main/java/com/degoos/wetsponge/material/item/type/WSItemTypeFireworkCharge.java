package com.degoos.wetsponge.material.item.type;

import com.degoos.wetsponge.color.WSColor;
import com.degoos.wetsponge.firework.WSFireworkEffect;
import com.degoos.wetsponge.material.item.WSItemType;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;
import com.degoos.wetsponge.nbt.WSNBTTagInt;
import com.degoos.wetsponge.nbt.WSNBTTagList;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public interface WSItemTypeFireworkCharge extends WSItemType {

	Optional<WSFireworkEffect> getEffect();

	void setEffect(WSFireworkEffect effect);

	@Override
	WSItemTypeFireworkCharge clone();

	@Override
	default WSNBTTagCompound writeToData(WSNBTTagCompound compound) {
		WSFireworkEffect fireworkEffect = getEffect().orElse(null);
		if (fireworkEffect == null) return compound;
		WSNBTTagCompound fireworkCompound = WSNBTTagCompound.of();
		fireworkCompound.setString("shape", fireworkEffect.getShape().name());
		fireworkCompound.setBoolean("flickers", fireworkEffect.flickers());
		fireworkCompound.setBoolean("trail", fireworkEffect.hasTrail());
		WSNBTTagList colors = WSNBTTagList.of();
		fireworkEffect.getColors().forEach(target -> colors.appendTag(WSNBTTagInt.of(target.toRGB())));
		fireworkCompound.setTag("colors", colors);
		WSNBTTagList fadeColors = WSNBTTagList.of();
		fireworkEffect.getFadeColors().forEach(target -> fadeColors.appendTag(WSNBTTagInt.of(target.toRGB())));
		fireworkCompound.setTag("fadeColors", fadeColors);
		compound.setTag("effect", fireworkCompound);
		return compound;
	}

	@Override
	default WSNBTTagCompound readFromData(WSNBTTagCompound compound) {
		if (!compound.hasKey("effect")) {
			setEffect(null);
			return compound;
		}
		WSNBTTagCompound firework = (WSNBTTagCompound) compound.getTag("effect");
		boolean flickers = firework.getBoolean("flickers");
		boolean trail = firework.getBoolean("trail");
		List<WSColor> colors = new ArrayList<>();
		WSNBTTagList colorTag = (WSNBTTagList) firework.getTag("colors");
		for (int c = 0; c < colorTag.tagCount(); c++)
			colors.add(WSColor.ofRGB(colorTag.getIntAt(c)));

		List<WSColor> fadeColors = new ArrayList<>();
		WSNBTTagList fadeColorTag = (WSNBTTagList) firework.getTag("fadeColors");
		for (int c = 0; c < fadeColorTag.tagCount(); c++)
			fadeColors.add(WSColor.ofRGB(fadeColorTag.getIntAt(c)));
		setEffect(WSFireworkEffect.builder().flicker(flickers).trail(trail).colors(colors).fades(fadeColors).build());
		return compound;
	}
}
