package com.degoos.wetsponge.material.item.type;

import com.degoos.wetsponge.enums.item.EnumItemTypeFishType;
import com.degoos.wetsponge.material.item.WSItemType;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;

public interface WSItemTypeFish extends WSItemType {

	EnumItemTypeFishType getFishType();

	void setFishType(EnumItemTypeFishType fishType);

	@Override
	WSItemTypeFish clone();

	@Override
	default WSNBTTagCompound writeToData(WSNBTTagCompound compound) {
		compound.setString("fishType", getFishType().name());
		return compound;
	}

	@Override
	default WSNBTTagCompound readFromData(WSNBTTagCompound compound) {
		setFishType(EnumItemTypeFishType.valueOf(compound.getString("fishType")));
		return compound;
	}
}
