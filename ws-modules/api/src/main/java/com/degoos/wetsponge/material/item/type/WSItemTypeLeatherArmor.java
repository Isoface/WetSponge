package com.degoos.wetsponge.material.item.type;

import com.degoos.wetsponge.color.WSColor;
import com.degoos.wetsponge.material.item.WSItemType;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;

public interface WSItemTypeLeatherArmor extends WSItemTypeDamageable {

	WSColor getColor();

	void setColor(WSColor color);

	@Override
	WSItemTypeLeatherArmor clone();

	@Override
	default WSNBTTagCompound writeToData(WSNBTTagCompound compound) {
		compound.setInteger("damage", getDamage());
		WSColor color = getColor();
		if (color == null) return compound;
		compound.setInteger("color", color.toRGB());
		return compound;
	}

	@Override
	default WSNBTTagCompound readFromData(WSNBTTagCompound compound) {
		setDamage(compound.getInteger("damage"));
		if (!compound.hasKey("color")) {
			setColor(null);
			return compound;
		}
		setColor(WSColor.ofRGB(compound.getInteger("color")));
		return compound;
	}
}
