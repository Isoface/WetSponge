package com.degoos.wetsponge.merchant;

import com.degoos.wetsponge.bridge.merchant.BridgeTrade;
import com.degoos.wetsponge.item.WSItemStack;

import java.util.Optional;

public interface WSTrade {

	public static Builder builder() {
		return BridgeTrade.builder();
	}

	int getUses();

	int getMaxUses();

	WSItemStack getResult();

	WSItemStack getFirstItem();

	Optional<WSItemStack> getSecondItem();

	boolean doesGrantExperience();

	Object getHandled();

	public interface Builder {

		Builder uses(int uses);

		Builder maxUses(int maxUses);

		Builder result(WSItemStack result);

		Builder firstItem(WSItemStack firstItem);

		Builder secondItem(WSItemStack secondItem);

		Builder canGrantExperience(boolean grantExperience);

		WSTrade build();

	}


}
