package com.degoos.wetsponge.nbt;

import com.degoos.wetsponge.bridge.nbt.BridgeNBT;

public interface WSNBTTagByte extends WSNBTPrimitive {

	public static WSNBTTagByte of(byte b) {
		return BridgeNBT.ofByte(b);
	}

	@Override
	WSNBTTagByte copy();
}
