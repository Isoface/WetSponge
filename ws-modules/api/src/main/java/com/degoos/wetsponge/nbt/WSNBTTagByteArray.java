package com.degoos.wetsponge.nbt;

import com.degoos.wetsponge.bridge.nbt.BridgeNBT;

public interface WSNBTTagByteArray extends WSNBTBase {

	public static WSNBTTagByteArray of(byte[] bytes) {
		return BridgeNBT.ofByteArray(bytes);
	}

	byte[] getByteArray();

	@Override
	WSNBTTagByteArray copy();
}
