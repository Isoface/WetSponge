package com.degoos.wetsponge.nbt;

import com.degoos.wetsponge.bridge.nbt.BridgeNBT;
import com.degoos.wetsponge.exception.nbt.NBTParseException;

import java.util.Set;
import java.util.UUID;

public interface WSNBTTagCompound extends WSNBTBase {

	public static WSNBTTagCompound of() {
		return BridgeNBT.newCompound();
	}

	public static WSNBTTagCompound of(String string) throws NBTParseException {
		return BridgeNBT.ofCompound(string);
	}

	Set<String> getKeySet();

	int getSize();

	void setTag(String key, WSNBTBase wsnbt);

	void setByte(String key, byte b);

	void setShort(String key, short s);

	void setInteger(String key, int i);

	void setLong(String key, long l);

	void setUniqueId(String key, UUID uuid);

	void setFloat(String key, float f);

	void setDouble(String key, double d);

	void setString(String key, String string);

	void setByteArray(String key, byte[] bytes);

	void setIntArray(String key, int[] ints);

	void setBoolean(String key, boolean b);

	WSNBTBase getTag(String key);

	byte getTagId(String key);

	boolean hasKey(String key);

	boolean hasKeyOfType(String key, int id);

	byte getByte(String key);

	short getShort(String key);

	int getInteger(String key);

	long getLong(String key);

	UUID getUniqueId(String key);

	boolean hasUniqueId(String key);

	float getFloat(String key);

	double getDouble(String key);

	String getString(String key);

	byte[] getByteArray(String key);

	int[] getIntArray(String key);

	WSNBTTagCompound getCompoundTag(String key);

	WSNBTTagList getTagList(String key, int expectedTagId);

	boolean getBoolean(String key);

	void removeTag(String key);

	void merge(WSNBTTagCompound wsnbtTagCompound);


	@Override
	WSNBTTagCompound copy();
}
