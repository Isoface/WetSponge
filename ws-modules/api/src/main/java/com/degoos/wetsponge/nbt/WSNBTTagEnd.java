package com.degoos.wetsponge.nbt;

import com.degoos.wetsponge.bridge.nbt.BridgeNBT;

public interface WSNBTTagEnd extends WSNBTBase {

	public static WSNBTTagEnd of() {
		return BridgeNBT.newEnd();
	}

	@Override
	WSNBTTagEnd copy();
}
