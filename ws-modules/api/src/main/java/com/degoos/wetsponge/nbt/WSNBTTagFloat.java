package com.degoos.wetsponge.nbt;

import com.degoos.wetsponge.bridge.nbt.BridgeNBT;

public interface WSNBTTagFloat extends WSNBTPrimitive {

	public static WSNBTTagFloat of(float f) {
		return BridgeNBT.ofFloat(f);
	}

	@Override
	WSNBTTagFloat copy();
}
