package com.degoos.wetsponge.nbt;

import com.degoos.wetsponge.bridge.nbt.BridgeNBT;

public interface WSNBTTagIntArray extends WSNBTBase {

	public static WSNBTTagIntArray of(int[] ints) {
		return BridgeNBT.ofIntArray(ints);
	}

	int[] getIntArray();

	@Override
	WSNBTTagIntArray copy();
}
