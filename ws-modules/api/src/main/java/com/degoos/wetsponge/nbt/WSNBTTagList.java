package com.degoos.wetsponge.nbt;

import com.degoos.wetsponge.bridge.nbt.BridgeNBT;

public interface WSNBTTagList extends WSNBTBase {

	public static WSNBTTagList of() {
		return BridgeNBT.newList();
	}

	void appendTag(WSNBTBase wsnbt);

	void set(int index, WSNBTBase wsnbt);

	void removeTag(int index);

	WSNBTTagCompound getCompoundTagAt(int index);

	int getIntAt(int index);

	int[] getIntArrayAt(int index);

	double getDoubleAt(int index);

	float getFloatAt(int index);

	String getStringAt(int index);

	WSNBTBase get(int index);

	int tagCount();

	int getTagType();

	@Override
	WSNBTTagList copy();
}
