package com.degoos.wetsponge.nbt;

import com.degoos.wetsponge.bridge.nbt.BridgeNBT;

public interface WSNBTTagLong extends WSNBTPrimitive {

	public static WSNBTTagLong of(long l) {
		return BridgeNBT.ofLong(l);
	}

	@Override
	WSNBTTagLong copy();
}
