package com.degoos.wetsponge.nbt;

import com.degoos.wetsponge.bridge.nbt.BridgeNBT;

public interface WSNBTTagLongArray extends WSNBTBase {

	public static WSNBTTagLongArray of(long[] longs) {
		return BridgeNBT.ofLongArray(longs);
	}

	long[] getLongArray();

	@Override
	WSNBTTagLongArray copy();
}
