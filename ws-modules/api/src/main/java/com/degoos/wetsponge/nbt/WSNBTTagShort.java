package com.degoos.wetsponge.nbt;

import com.degoos.wetsponge.bridge.nbt.BridgeNBT;

public interface WSNBTTagShort extends WSNBTPrimitive {

	public static WSNBTTagShort of(short s) {
		return BridgeNBT.ofShort(s);
	}

	@Override
	WSNBTTagShort copy();
}
