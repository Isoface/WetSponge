package com.degoos.wetsponge.nbt;

import com.degoos.wetsponge.bridge.nbt.BridgeNBT;

public interface WSNBTTagString extends WSNBTBase {

	public static WSNBTTagString of(String string) {
		return BridgeNBT.ofString(string);
	}

	String getString();

	@Override
	WSNBTTagString copy();
}
