package com.degoos.wetsponge.packet.play.client;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.packet.WSPacket;
import com.flowpowered.math.vector.Vector3i;

public interface WSCPacketPlayerTryUseItemOnBlock extends WSPacket {

	Vector3i getPosition();

	void setPosition(Vector3i position);

	EnumBlockFace getPlacedBlockDirection();

	void setPlacedBlockDirection(EnumBlockFace direction);

	boolean isMainHand();

	void setMainHand(boolean mainHand);

	float getFacingX();

	void setFacingX(float facingX);

	float getFacingY();

	void setFacingY(float facingY);

	float getFacingZ();

	void setFacingZ(float facingZ);

}
