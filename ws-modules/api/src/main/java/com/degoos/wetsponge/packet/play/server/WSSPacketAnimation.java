package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.bridge.packet.BridgeServerPacket;
import com.degoos.wetsponge.entity.WSEntity;
import com.degoos.wetsponge.packet.WSPacket;

public interface WSSPacketAnimation extends WSPacket {

	public static WSSPacketAnimation of(WSEntity entity, int animationType) {
		return BridgeServerPacket.newWSSPacketAnimation(entity, animationType);
	}

	public static WSSPacketAnimation of(int entityId, int animationType) {
		return BridgeServerPacket.newWSSPacketAnimation(entityId, animationType);
	}

	public int getEntityId();

	public void setEntityId(int entityId);

	public int getAnimationType();

	public void setAnimationType(int animationType);
}
