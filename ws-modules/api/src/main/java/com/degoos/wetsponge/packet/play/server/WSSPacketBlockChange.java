package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.bridge.packet.BridgeServerPacket;
import com.degoos.wetsponge.material.block.WSBlockType;
import com.degoos.wetsponge.packet.WSPacket;
import com.flowpowered.math.vector.Vector3i;

public interface WSSPacketBlockChange extends WSPacket {

	public static WSSPacketBlockChange of(WSBlockType material, Vector3i position) {
		return BridgeServerPacket.newWSSPacketBlockChange(material, position);
	}

	public Vector3i getBlockPosition();

	public void setBlockPosition(Vector3i position);

	public WSBlockType getMaterial();

	public void setMaterial(WSBlockType material);

}
