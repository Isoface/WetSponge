package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.bridge.packet.BridgeServerPacket;
import com.degoos.wetsponge.packet.WSPacket;

import java.util.List;

public interface WSSPacketDestroyEntities extends WSPacket {

	public static WSSPacketDestroyEntities of(int... entityIds) {
		return BridgeServerPacket.newWSSPacketDestroyEntities(entityIds);
	}

	List<Integer> getEntityIds();

	void setEntityIds(List<Integer> entityIds);

	void addEntityId(int entityId);

	void removeEntityId(int entityId);
}
