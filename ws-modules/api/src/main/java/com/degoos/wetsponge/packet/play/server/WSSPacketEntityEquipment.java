package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.bridge.packet.BridgeServerPacket;
import com.degoos.wetsponge.enums.EnumEquipType;
import com.degoos.wetsponge.item.WSItemStack;
import com.degoos.wetsponge.packet.WSPacket;

public interface WSSPacketEntityEquipment extends WSPacket {

	public static WSSPacketEntityEquipment of(int entityId, EnumEquipType equipType, WSItemStack itemStack) {
		return BridgeServerPacket.newWSSPacketEntityEquipment(entityId, equipType, itemStack);
	}

	int getEntityId();

	void setEntityId(int entityId);

	EnumEquipType getEquipType();

	void setEquipType(EnumEquipType equipType);

	WSItemStack getItemStack();

	void setItemStack(WSItemStack itemStack);

}
