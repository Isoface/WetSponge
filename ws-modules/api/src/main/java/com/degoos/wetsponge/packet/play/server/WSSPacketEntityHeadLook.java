package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.bridge.packet.BridgeServerPacket;
import com.degoos.wetsponge.entity.living.WSLivingEntity;
import com.degoos.wetsponge.packet.WSPacket;

public interface WSSPacketEntityHeadLook extends WSPacket {

	public static WSSPacketEntityHeadLook of(int entityId, int yaw) {
		return BridgeServerPacket.newWSSPacketEntityHeadLook(entityId, yaw);
	}

	public static WSSPacketEntityHeadLook of(WSLivingEntity entity, int yaw) {
		return of(entity.getEntityId(), yaw);
	}

	public static WSSPacketEntityHeadLook of(WSLivingEntity entity) {
		return BridgeServerPacket.newWSSPacketEntityHeadLook(entity);
	}


	int getEntityId();

	void setEntityId(int entityId);

	int getYaw();

	void setYaw(int yaw);

}
