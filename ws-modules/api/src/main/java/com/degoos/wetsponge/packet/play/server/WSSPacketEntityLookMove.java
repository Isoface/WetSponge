package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.bridge.packet.BridgeServerPacket;
import com.degoos.wetsponge.entity.WSEntity;
import com.flowpowered.math.vector.Vector2i;
import com.flowpowered.math.vector.Vector3i;

public interface WSSPacketEntityLookMove extends WSSPacketEntity {

	public static WSSPacketEntityLookMove of(WSEntity entity, Vector3i position, Vector2i rotation, boolean onGround) {
		return BridgeServerPacket.newWSSPacketEntityLookMove(entity, position, rotation, onGround);
	}

	public static WSSPacketEntityLookMove of(int entity, Vector3i position, Vector2i rotation, boolean onGround) {
		return BridgeServerPacket.newWSSPacketEntityLookMove(entity, position, rotation, onGround);
	}

}
