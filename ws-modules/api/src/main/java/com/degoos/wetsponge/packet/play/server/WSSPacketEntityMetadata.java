package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.bridge.packet.BridgeServerPacket;
import com.degoos.wetsponge.entity.WSEntity;
import com.degoos.wetsponge.packet.WSPacket;
import com.degoos.wetsponge.text.WSText;

public interface WSSPacketEntityMetadata extends WSPacket {

	public static WSSPacketEntityMetadata of(WSEntity entity) {
		return BridgeServerPacket.newWSSPacketEntityMetadata(entity);
	}

	int getEntityId();

	void setEntityId(int entityId);

	void setMetadataOf(WSEntity entity);

	void setCustomName(WSText text);
}
