package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.bridge.packet.BridgeServerPacket;
import com.degoos.wetsponge.entity.living.WSLivingEntity;
import com.degoos.wetsponge.packet.WSPacket;

public interface WSSPacketEntityProperties extends WSPacket {

	public static WSSPacketEntityProperties of(WSLivingEntity entity) {
		return BridgeServerPacket.newWSSPacketEntityProperties(entity);
	}

	int getEntityId();

	void setEntityId(int entityId);

	void setPropertiesOf(WSLivingEntity entity);

}
