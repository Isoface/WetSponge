package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.bridge.packet.BridgeServerPacket;
import com.degoos.wetsponge.entity.WSEntity;
import com.flowpowered.math.vector.Vector3i;

public interface WSSPacketEntityRelMove extends WSSPacketEntity {

	public static WSSPacketEntityRelMove of(WSEntity entity, Vector3i position, boolean onGround) {
		return BridgeServerPacket.newWSSPacketEntityRelMove(entity, position, onGround);
	}

	public static WSSPacketEntityRelMove of(int entity, Vector3i position, boolean onGround) {
		return BridgeServerPacket.newWSSPacketEntityRelMove(entity, position, onGround);
	}

}
