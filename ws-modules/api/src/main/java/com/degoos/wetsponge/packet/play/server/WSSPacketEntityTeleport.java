package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.bridge.packet.BridgeServerPacket;
import com.degoos.wetsponge.entity.WSEntity;
import com.degoos.wetsponge.packet.WSPacket;
import com.flowpowered.math.vector.Vector2i;
import com.flowpowered.math.vector.Vector3d;

public interface WSSPacketEntityTeleport extends WSPacket {

	public static WSSPacketEntityTeleport of(WSEntity entity) {
		return BridgeServerPacket.newWSSPacketEntityTeleport(entity);
	}

	public static WSSPacketEntityTeleport of(int entityId, Vector3d position, Vector2i rotation, boolean onGround) {
		return BridgeServerPacket.newWSSPacketEntityTeleport(entityId, position, rotation, onGround);
	}

	int getEntityId();

	void setEntityId(int entityId);

	Vector3d getPosition();

	void setPosition(Vector3d position);

	Vector2i getRotation();

	void setRotation(Vector2i rotation);

	boolean isOnGround();

	void setOnGround(boolean onGround);

}
