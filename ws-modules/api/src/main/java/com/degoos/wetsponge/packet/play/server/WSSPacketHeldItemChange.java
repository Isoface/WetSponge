package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.bridge.packet.BridgeServerPacket;
import com.degoos.wetsponge.packet.WSPacket;

public interface WSSPacketHeldItemChange extends WSPacket {

	public static WSSPacketHeldItemChange of(int slot) {
		return BridgeServerPacket.newWSSPacketHeldItemChange(slot);
	}

	int getSlot();

	void setSlot(int slot);

}
