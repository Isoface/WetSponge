package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.bridge.packet.BridgeServerPacket;
import com.degoos.wetsponge.map.WSMapView;
import com.degoos.wetsponge.packet.WSPacket;
import com.flowpowered.math.vector.Vector2i;

public interface WSSPacketMaps extends WSPacket {

	public static WSSPacketMaps of(int mapId, Vector2i origin, Vector2i size, WSMapView mapView) {
		return BridgeServerPacket.newWSSPacketMaps(mapId, origin, size, mapView);
	}

	int getMapId();

	void setMapId(int id);

	Vector2i getOrigin();

	void setOrigin(Vector2i origin);

	Vector2i setSize();

	void setSize(Vector2i size);

	WSMapView getMapView();

	void setMapView(WSMapView mapView);
}
