package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.bridge.packet.BridgeServerPacket;
import com.degoos.wetsponge.material.block.WSBlockType;
import com.degoos.wetsponge.packet.WSPacket;
import com.flowpowered.math.vector.Vector2i;
import com.flowpowered.math.vector.Vector3i;

import java.util.Map;

public interface WSSPacketMultiBlockChange extends WSPacket {

	public static WSSPacketMultiBlockChange of(Vector2i chunkPos, Map<Vector3i, WSBlockType> materials) {
		return BridgeServerPacket.newWSSPacketMultiBlockChange(chunkPos, materials);
	}

	Vector2i getChunkPosition();

	void setChunkPosition(Vector2i chunkPosition);

	Map<Vector3i, WSBlockType> getMaterials();

	void setMaterials(Map<Vector3i, WSBlockType> materials);

	void removeMaterial(Vector3i position);

	void addMaterial(Vector3i position, WSBlockType type);
}
