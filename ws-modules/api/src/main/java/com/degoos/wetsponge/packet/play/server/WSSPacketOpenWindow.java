package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.bridge.packet.BridgeServerPacket;
import com.degoos.wetsponge.packet.WSPacket;
import com.degoos.wetsponge.text.WSText;

public interface WSSPacketOpenWindow extends WSPacket {

	public static WSSPacketOpenWindow of(int windowsId, String inventoryType, WSText windowTitle, int slotCount, int entityId) {
		return BridgeServerPacket.newWSSPacketOpenWindow(windowsId, inventoryType, windowTitle, slotCount, entityId);
	}

	int getWindowId();

	void setWindowId(int windowId);

	String getInventoryType();

	void setInventoryType(String inventoryType);

	WSText getWindowTitle();

	void setWindowTitle(WSText windowTitle);

	int getSlotCount();

	void setSlotCount(int slotCount);

	int getEntityId();

	void setEntityId(int entityId);

}
