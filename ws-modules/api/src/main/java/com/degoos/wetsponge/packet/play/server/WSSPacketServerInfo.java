package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.bridge.packet.BridgeServerPacket;
import com.degoos.wetsponge.packet.WSPacket;
import com.degoos.wetsponge.server.response.WSServerStatusResponse;

public interface WSSPacketServerInfo extends WSPacket {

	public static WSSPacketServerInfo of(WSServerStatusResponse response) {
		return BridgeServerPacket.newWSSPacketServerInfo(response);
	}

	WSServerStatusResponse getResponse();

	void setResponse(WSServerStatusResponse response);
}
