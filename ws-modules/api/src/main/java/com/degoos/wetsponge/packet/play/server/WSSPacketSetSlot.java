package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.bridge.packet.BridgeServerPacket;
import com.degoos.wetsponge.item.WSItemStack;
import com.degoos.wetsponge.packet.WSPacket;

public interface WSSPacketSetSlot extends WSPacket {

	public static WSSPacketSetSlot of(int windowsId, int slot, WSItemStack itemStack) {
		return BridgeServerPacket.newWSSPacketSetSlot(windowsId, slot, itemStack);
	}

	int getWindowId();

	void setWindowId(int windowsId);

	int getSlot();

	void setSlot(int slot);

	WSItemStack getItemStack();

	void setItemStack(WSItemStack itemStack);
}
