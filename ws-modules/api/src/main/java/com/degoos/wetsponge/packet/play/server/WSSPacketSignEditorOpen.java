package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.bridge.packet.BridgeServerPacket;
import com.degoos.wetsponge.packet.WSPacket;
import com.flowpowered.math.vector.Vector3d;

public interface WSSPacketSignEditorOpen extends WSPacket {

	public static WSSPacketSignEditorOpen of(Vector3d position) {
		return BridgeServerPacket.newWSSPacketSignEditorOpen(position);
	}

	Vector3d getPosition();

	void setPosition(Vector3d position);
}
