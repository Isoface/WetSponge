package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.bridge.packet.BridgeServerPacket;
import com.degoos.wetsponge.packet.WSPacket;
import com.flowpowered.math.vector.Vector3d;

public interface WSSPacketSpawnGlobalEntity extends WSPacket {

	public static WSSPacketSpawnGlobalEntity of(Vector3d position, int entityId, int type) {
		return BridgeServerPacket.newWSSPacketSpawnGlobalEntity(position, entityId, type);
	}

	Vector3d getPosition();

	void setPosition(Vector3d position);

	int getEntityId();

	void setEntityId(int entityId);

	int getType();

	void setType(int type);
}
