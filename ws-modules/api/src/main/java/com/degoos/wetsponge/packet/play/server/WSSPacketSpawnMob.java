package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.bridge.packet.BridgeServerPacket;
import com.degoos.wetsponge.entity.living.WSLivingEntity;
import com.degoos.wetsponge.packet.WSPacket;
import com.flowpowered.math.vector.Vector2d;
import com.flowpowered.math.vector.Vector3d;

import java.util.Optional;
import java.util.UUID;

public interface WSSPacketSpawnMob extends WSPacket {

	public static WSSPacketSpawnMob of(WSLivingEntity entity) {
		return BridgeServerPacket.newWSSPacketSpawnMob(entity);
	}

	public static WSSPacketSpawnMob of(WSLivingEntity entity, Vector3d position, Vector3d velocity, Vector2d rotation, float headPitch) {
		return BridgeServerPacket.newWSSPacketSpawnMob(entity, position, velocity, rotation, headPitch);
	}

	Optional<WSLivingEntity> getEntity();

	void setEntity(WSLivingEntity entity);

	Optional<WSLivingEntity> getDisguise();

	void setDisguise(WSLivingEntity disguise);

	int getEntityId();

	void setEntityId(int entityId);

	UUID getUniqueId();

	void setUniqueId(UUID uniqueId);

	int getType();

	void setType(int type);

	Vector3d getPosition();

	void setPosition(Vector3d position);

	Vector3d getVelocity();

	void setVelocity(Vector3d velocity);

	Vector2d getRotation();

	void setRotation(Vector2d rotation);

	double getHeadPitch();

	void setHeadPitch(double headPitch);

}
