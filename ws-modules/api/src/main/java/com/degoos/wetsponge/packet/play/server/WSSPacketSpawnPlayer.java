package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.bridge.packet.BridgeServerPacket;
import com.degoos.wetsponge.entity.living.player.WSHuman;
import com.degoos.wetsponge.packet.WSPacket;
import com.flowpowered.math.vector.Vector2d;
import com.flowpowered.math.vector.Vector3d;

import java.util.UUID;

public interface WSSPacketSpawnPlayer extends WSPacket {

	public static WSSPacketSpawnPlayer of(WSHuman entity) {
		return BridgeServerPacket.newWSSPacketSpawnPlayer(entity);
	}

	public static WSSPacketSpawnPlayer of(WSHuman entity, Vector3d position, Vector2d rotation) {
		return BridgeServerPacket.newWSSPacketSpawnPlayer(entity, position, rotation);
	}

	void setEntity(WSHuman entity);

	int getEntityId();

	void setEntityId(int entityId);

	UUID getUniqueId();

	void setUniqueId(UUID uniqueId);

	Vector3d getPosition();

	void setPosition(Vector3d position);

	Vector2d getRotation();

	void setRotation(Vector2d rotation);


}
