package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.bridge.packet.BridgeServerPacket;
import com.degoos.wetsponge.item.WSItemStack;
import com.degoos.wetsponge.packet.WSPacket;

import java.util.List;

public interface WSSPacketWindowItems extends WSPacket {

	public static WSSPacketWindowItems of(int windowsId, List<WSItemStack> itemStacks) {
		return BridgeServerPacket.newWSSPacketWindowItems(windowsId, itemStacks);
	}

	int getWindowId();

	void setWindowId(int windowsId);

	List<WSItemStack> getItemStacks();
}
