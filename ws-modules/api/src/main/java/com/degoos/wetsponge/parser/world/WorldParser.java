package com.degoos.wetsponge.parser.world;

import com.degoos.wetsponge.bridge.parser.BridgeWorldParser;
import com.degoos.wetsponge.world.WSWorld;

import java.util.HashMap;
import java.util.Map;

public class WorldParser {

	private static Map<String, WSWorld> worlds = new HashMap<>();

	public static WSWorld addWorld(WSWorld world) {
		worlds.put(world.getName().toLowerCase(), world);
		return worlds.get(world.getName().toLowerCase());
	}

	public static WSWorld getOrCreateWorld(String name, Object world) {
		WSWorld wsWorld = worlds.get(name.toLowerCase());
		if (wsWorld != null) return wsWorld;
		return addWorld(BridgeWorldParser.getWorld(world));
	}
}
