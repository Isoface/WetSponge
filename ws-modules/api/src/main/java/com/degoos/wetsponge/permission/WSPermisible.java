package com.degoos.wetsponge.permission;


import java.util.Set;

public interface WSPermisible {

	boolean hasPermission (String name);

	Set<String> getPermissions();
}
