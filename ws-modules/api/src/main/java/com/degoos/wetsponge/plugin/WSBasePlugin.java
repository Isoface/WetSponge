package com.degoos.wetsponge.plugin;

import java.util.List;

/**
 * Represents a plugin from the server that runs WetSponge.
 */
public interface WSBasePlugin {

	/**
	 * Returns the name of the {@link WSBasePlugin base plugin}.
	 *
	 * @return the name of the {@link WSBasePlugin base plugin}.
	 */
	String getName();

	/**
	 * Returns the version of the {@link WSBasePlugin base plugin}.
	 *
	 * @return the version of the {@link WSBasePlugin base plugin}.
	 */
	String getVersion();

	/**
	 * Returns the description of the {@link WSBasePlugin base plugin}.
	 *
	 * @return the description of the {@link WSBasePlugin base plugin}.
	 */
	String getDescription();

	/**
	 * Returns the URL of the {@link WSBasePlugin base plugin}.
	 *
	 * @return the URL of the {@link WSBasePlugin base plugin}.
	 */
	String getUrl();

	/**
	 * Returns the list of authors of the {@link WSBasePlugin base plugin}.
	 *
	 * @return the list of authors of the {@link WSBasePlugin base plugin}.
	 */
	List<String> getAuthors();

	/**
	 * Returns the list with the names of the dependencies of the {@link WSBasePlugin base plugin}.
	 *
	 * @return the list with the names of the dependencies of the {@link WSBasePlugin base plugin}.
	 */
	List<String> getDependencies();

	/**
	 * Returns the list with the names of the optional dependencies of the {@link WSBasePlugin base plugin}.
	 *
	 * @return the list with the names of the optional dependencies of the {@link WSBasePlugin base plugin}.
	 */
	List<String> getSoftDependencies();

	/**
	 * Returns the original instance of the {@link WSBasePlugin base plugin}.
	 *
	 * @return the original instance of the {@link WSBasePlugin base plugin}.
	 */
	Object getHandled();
}
