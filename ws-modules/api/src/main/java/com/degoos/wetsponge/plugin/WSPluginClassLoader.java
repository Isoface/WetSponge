package com.degoos.wetsponge.plugin;


import com.degoos.wetsponge.exception.plugin.WSInvalidPluginException;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class WSPluginClassLoader extends URLClassLoader {

	private final WSPlugin plugin;
	private final Map<String, Class<?>> classes = new HashMap<>();
	private File file;


	WSPluginClassLoader(final ClassLoader parent, final WSPluginDescription description, final File file) throws WSInvalidPluginException, MalformedURLException {
		super(new URL[]{file.toURI().toURL()}, parent);
		this.file = file;
		try {
			Class<?> jarClass;
			try {
				jarClass = Class.forName(description.getMain(), true, this);
			} catch (ClassNotFoundException ex) {
				throw new WSInvalidPluginException(ex, "Cannot find main class `" + description.getMain() + "'");
			}
			Class<? extends WSPlugin> pluginClass;
			try {
				pluginClass = jarClass.asSubclass(WSPlugin.class);
			} catch (ClassCastException ex) {
				throw new WSInvalidPluginException(ex, "main class `" + description.getMain() + "' does not extend WSPlugin");
			}
			plugin = pluginClass.newInstance();
			plugin.init(this, description);
		} catch (IllegalAccessException ex) {
			throw new WSInvalidPluginException(ex, "No public constructor");
		} catch (InstantiationException ex) {
			throw new WSInvalidPluginException(ex, "Abnormal plugin item");
		}
	}

	@Override
	public Class<?> findClass(String name) throws ClassNotFoundException {
		return findClass(name, true);
	}

	public Class<?> findClass(String name, boolean checkGlobal) throws ClassNotFoundException {
		Class<?> result = classes.get(name);

		if (result == null) {
			if (checkGlobal) {
				result = WSPluginManager.getInstance().getPlugins().stream().map(target -> {
					try {
						return target.getClassLoader().findClass(name, false);
					} catch (ClassNotFoundException e) {
						return null;
					}
				}).filter(Objects::nonNull).findAny().orElse(null);
			}

			if (result == null) {
				result = super.findClass(name);

				if (result != null) {
					classes.put(name, result);
				}
			}

			classes.put(name, result);
		}

		return result;
	}

	public File getFile() {
		return file;
	}

	public WSPlugin getPlugin() {
		return plugin;
	}
}
