package com.degoos.wetsponge.plugin;


import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.enums.EnumTextColor;
import com.degoos.wetsponge.event.plugin.WSPluginDisableEvent;
import com.degoos.wetsponge.exception.plugin.WSInvalidDescriptionException;
import com.degoos.wetsponge.exception.plugin.WSInvalidPluginException;
import com.degoos.wetsponge.exception.plugin.WSUnknownDependencyException;
import com.degoos.wetsponge.task.WSTaskPool;
import com.degoos.wetsponge.text.WSText;
import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.Validate;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

public class WSPluginLoader {

	private static WSPluginLoader ourInstance = new WSPluginLoader();
	private final List<WSPluginClassLoader> loaders = new CopyOnWriteArrayList<>();


	private WSPluginLoader() {
	}


	public static WSPluginLoader getInstance() {
		return ourInstance;
	}


	public WSPlugin loadPlugin(final File file) throws WSInvalidPluginException {
		Validate.notNull(file, "File cannot be null");
		if (!file.exists()) throw new WSInvalidPluginException(new FileNotFoundException(file.getPath() + " does not exist"));

		final WSPluginDescription description;
		try {
			description = getPluginDescription(file);
		} catch (WSInvalidDescriptionException ex) {
			throw new WSInvalidPluginException(ex);
		}

		final File parentFile = file.getParentFile();
		final File dataFolder = new File(parentFile, description.getName());

		if (dataFolder.exists() && !dataFolder.isDirectory()) {
			throw new WSInvalidPluginException(String
				.format("Projected datafolder: `%s' for %s (%s) exists and is not a directory", dataFolder, description.getFullName(), file));
		}
		for (String pluginName : description.getDepend())
			if (!WetSponge.getPluginManager().getPlugin(pluginName).isPresent()) throw new WSInvalidPluginException(new WSUnknownDependencyException(pluginName));
		WSPluginClassLoader loader;
		try {
			loader = new WSPluginClassLoader(getClass().getClassLoader(), description, file);
		} catch (WSInvalidPluginException ex) {
			throw ex;
		} catch (Throwable ex) {
			throw new WSInvalidPluginException(ex);
		}

		loaders.add(loader);
		return loader.getPlugin();
	}


	public WSPluginDescription getPluginDescription(File file) throws WSInvalidDescriptionException {
		Validate.notNull(file, "File cannot be null");
		JarFile jar = null;
		InputStream stream = null;
		try {
			jar = new JarFile(file);
			JarEntry entry = jar.getJarEntry("plugin.yml");
			if (entry == null) throw new WSInvalidDescriptionException(new FileNotFoundException("Jar does not contain plugin.yml"));
			stream = jar.getInputStream(entry);
			return new WSPluginDescription(stream);
		} catch (Throwable ex) {
			throw new WSInvalidDescriptionException(ex);
		} finally {
			if (jar != null) {
				try {
					jar.close();
				} catch (IOException ignore) {
				}
			}
			if (stream != null) {
				try {
					stream.close();
				} catch (IOException ignore) {
				}
			}
		}
	}

	public WSPluginDescription getPluginFile(File file, String name) throws WSInvalidDescriptionException {
		Validate.notNull(file, "File cannot be null");
		JarFile jar = null;
		InputStream stream = null;
		try {
			jar = new JarFile(file);
			JarEntry entry = jar.getJarEntry(name);
			if (entry == null) throw new WSInvalidDescriptionException(new FileNotFoundException("Jar does not contain plugin.yml"));
			stream = jar.getInputStream(entry);
			return new WSPluginDescription(stream);
		} catch (Throwable ex) {
			throw new WSInvalidDescriptionException(ex);
		} finally {
			if (jar != null) {
				try {
					jar.close();
				} catch (IOException ignore) {
				}
			}
			if (stream != null) {
				try {
					stream.close();
				} catch (IOException ignore) {
				}
			}
		}
	}


	public void disablePlugin(WSPlugin plugin) {
		if (plugin.isEnabled()) {
//			plugin.getLogger().sendInfo(String.format("Disabling %s", plugin.getPluginDescription().getFullName()));
			WetSponge.getEventManager().callEvent(new WSPluginDisableEvent(plugin));
			WSPluginClassLoader classLoader = plugin.getClassLoader();
			loaders.remove(classLoader);
			WetSponge.getEventManager().unregisterListeners(plugin);
			WSTaskPool.removeTasks(plugin);
			try {
				plugin.setEnabled(false);
				classLoader.close();
			} catch (Throwable ex) {
				InternalLogger.printException(ex, WSText.builder("An error has occurred while WetSponge was unloading the plugin ")
					.append(WSText.of(plugin.getId(), EnumTextColor.YELLOW)).append(WSText.of("!")).build());
				ex.printStackTrace();
			} /*finally {
				if (plugin.timings != null) plugin.timings.stopTiming();
			}*/
		}
	}

}
