package com.degoos.wetsponge.plugin;


import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.bridge.plugin.BridgePluginManager;
import com.degoos.wetsponge.enums.EnumLoadPluginStatus;
import com.degoos.wetsponge.enums.EnumTextColor;
import com.degoos.wetsponge.event.plugin.WSPluginEnableEvent;
import com.degoos.wetsponge.exception.plugin.WSDependencyNotFoundException;
import com.degoos.wetsponge.exception.plugin.WSInvalidPluginException;
import com.degoos.wetsponge.plugin.preload.LanguageDependPostload;
import com.degoos.wetsponge.plugin.preload.LanguagesPreload;
import com.degoos.wetsponge.plugin.preload.Preload;
import com.degoos.wetsponge.text.WSText;
import com.degoos.wetsponge.util.InternalLogger;

import java.io.File;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

public class WSPluginManager {

	private static WSPluginManager ourInstance = new WSPluginManager();
	private List<WSPlugin> plugins;
	//private Map<WSPlugin, Timing> timings = new HashMap<>();
	private Map<String, File> pluginFiles;
	private Set<Preload> preloads;
	private boolean loaded;
	private File pluginFolder;


	private WSPluginManager() {
		loaded = false;
		plugins = new CopyOnWriteArrayList<>();
		pluginFiles = new ConcurrentHashMap<>();
		preloads = Collections.newSetFromMap(new ConcurrentHashMap<>());
		preloads.add(new LanguagesPreload());
	}


	public static WSPluginManager getInstance() {
		return ourInstance;
	}


	public void loadPlugins() {
		if (loaded) return;

		if (WetSponge.getPluginManager().getBasePlugin("Multiverse-Core").isPresent()) {
			InternalLogger.sendWarning("-----------------------------------------------------");
			InternalLogger.sendWarning("");
			InternalLogger.sendWarning("You're using Multiverse-Core! This plugin breaks minigame and world plugins.");
			InternalLogger.sendWarning("Please unistall it before sending an error.");
			InternalLogger.sendWarning("");
			InternalLogger.sendWarning("-----------------------------------------------------");
		}


		pluginFolder = new File("WetSpongePlugins");
		if (!pluginFolder.exists()) pluginFolder.mkdirs();
		else if (pluginFolder.isFile()) {
			pluginFolder.delete();
			pluginFolder.mkdirs();
		}
		Map<WSPluginDescription, File> descriptions = new HashMap<>();
		Arrays.stream(pluginFolder.listFiles()).filter(File::isFile).filter(file -> file.getName().toLowerCase().endsWith(".jar")).forEach(file -> {
			try {
				WSPluginDescription description = WSPluginLoader.getInstance().getPluginDescription(file);
				Optional<Preload> optional = preloads.stream().filter(preload -> preload.getPluginId().equals(description.getName())).findAny();
				if (optional.isPresent()) {
					File newFile = optional.get().onLoad(description, file);
					if (newFile == null) descriptions.put(description, file);
					else descriptions.put(WSPluginLoader.getInstance().getPluginDescription(newFile), newFile);
				} else descriptions.put(description, file);
			} catch (Throwable ex) {
				InternalLogger.sendError("Error while loading a plugin!");
				ex.printStackTrace();
			}
		});
		descriptions.entrySet().forEach(entry -> {
			try {
				if (plugins.stream().anyMatch(plugin -> plugin.getPluginDescription().getName().equalsIgnoreCase(entry.getKey().getName()))) {
					WSPlugin plugin = getPlugin(entry.getKey().getName()).get();
					if (plugin.getPluginDescription().getMain().equals(entry.getKey().getMain())) return;
					else throw new WSInvalidPluginException("The plugin " + entry.getKey().getName() + " already exist!");
				}
				loadPlugin(descriptions, entry);
			} catch (StackOverflowError ex) {
				InternalLogger.printException(ex, WSText.builder("An error has occurred while WetSponge was loading the plugin ")
						.append(WSText.of(entry.getKey().getName(), EnumTextColor.YELLOW)).append(WSText.of("! Circular dependency error.")).build());
			} catch (Throwable ex) {
				InternalLogger.printException(ex, WSText.builder("An error has occurred while WetSponge was loading the plugin ")
						.append(WSText.of(entry.getKey().getName(), EnumTextColor.YELLOW)).append(WSText.of("!")).build());
			}
		});
		loaded = true;
		for (WSPlugin plugin : plugins) {
			InternalLogger.sendInfo(WSText.builder("Loading plugin ").append(WSText.builder(plugin.getId() + " ").color(EnumTextColor.YELLOW).build())
					.append(WSText.builder(plugin.getPluginDescription().getVersion()).color(EnumTextColor.RED).build())
					.append(WSText.builder(".").color(EnumTextColor.AQUA).build()).build());
			try {
				plugin.setEnabled(true);
				if (plugin.getPluginDescription().getDepend().contains("Languages")) {
					if (!LanguageDependPostload.check(plugin)) continue;
				}
				//timings.put(plugin, WetSpongeTimingsFactory.ofSafe(plugin, plugin.getClass().getCanonicalName()));
			} catch (Throwable ex) {
				InternalLogger.printException(ex, WSText.builder("An error has occurred while WetSponge was loading the plugin ")
						.append(WSText.of(plugin.getId(), EnumTextColor.YELLOW)).append(WSText.of("!")).build());
			}
			WetSponge.getEventManager().callEvent(new WSPluginEnableEvent(plugin));
			//plugin.timings = Timings.of(plugin, plugin.getNumericalId()).getTimingHandler();
			//plugin.timings.startTiming();
		}
	}


	private void loadPlugin(Map<WSPluginDescription, File> descriptions, Map.Entry<WSPluginDescription, File> entry)
			throws WSDependencyNotFoundException, WSInvalidPluginException {
		for (String dependency : entry.getKey().getDepend()) {
			Optional<Map.Entry<WSPluginDescription, File>> optional = descriptions.entrySet().stream().filter(target -> target.getKey().getName().equals(dependency))
					.findAny();
			if (!optional.isPresent()) throw new WSDependencyNotFoundException("Cannot found dependency " + dependency);
			Optional<WSPlugin> targetedPlugin = plugins.stream().filter(plugin -> plugin.getPluginDescription().getName().equals(dependency)).findAny();
			if (!targetedPlugin.isPresent()) loadPlugin(descriptions, optional.get());
		}
		for (String dependency : entry.getKey().getSoftDepend()) {
			Optional<Map.Entry<WSPluginDescription, File>> optional = descriptions.entrySet().stream().filter(target -> target.getKey().getName().equals(dependency))
					.findAny();
			if (!optional.isPresent()) continue;
			Optional<WSPlugin> targetedPlugin = plugins.stream().filter(plugin -> plugin.getPluginDescription().getName().equals(dependency)).findAny();
			if (!targetedPlugin.isPresent()) loadPlugin(descriptions, optional.get());
		}
		WSPlugin plugin = WSPluginLoader.getInstance().loadPlugin(entry.getValue());
		plugins.add(plugin);
		pluginFiles.put(plugin.getId().toLowerCase(), entry.getValue());
	}

	public EnumLoadPluginStatus loadPlugin(File file) {
		return loadPlugin(file, true);
	}

	public EnumLoadPluginStatus loadPlugin(File file, boolean executePreload) {
		String name = file.getName();
		try {
			WSPluginDescription description = WSPluginLoader.getInstance().getPluginDescription(file);
			String pluginId = description.getName();
			if (plugins.stream().anyMatch(plugin -> plugin.getPluginDescription().getName().equalsIgnoreCase(pluginId))) return EnumLoadPluginStatus.ALREADY_LOADED;
			Optional<Preload> optional = !executePreload ? Optional.empty() : preloads.stream().filter(preload -> preload.getPluginId().equals(pluginId)).findAny();
			if (optional.isPresent()) {
				File newFile = optional.get().onLoad(description, file);
				if (newFile != null) return loadPlugin(newFile, false);
			}
			name = description.getName();
			WSPlugin plugin = WSPluginLoader.getInstance().loadPlugin(file);

			InternalLogger.sendInfo(WSText.builder("Loading plugin ").append(WSText.builder(plugin.getId() + " ").color(EnumTextColor.YELLOW).build())
					.append(WSText.builder(plugin.getPluginDescription().getVersion()).color(EnumTextColor.RED).build())
					.append(WSText.builder(".").color(EnumTextColor.AQUA).build()).build());

			plugins.add(plugin);
			pluginFiles.put(plugin.getId().toLowerCase(), file);
			plugin.setEnabled(true);
			if (plugin.getPluginDescription().getDepend().contains("Languages")) {
				if (!LanguageDependPostload.check(plugin)) return EnumLoadPluginStatus.ERROR;
			}
			//timings.put(plugin, WetSpongeTimingsFactory.ofSafe(plugin, plugin.getClass().getCanonicalName()));
			WetSponge.getEventManager().callEvent(new WSPluginEnableEvent(plugin));
		} catch (Throwable ex) {
			InternalLogger
					.printException(ex, WSText.builder("An error has occurred while WetSponge was loading the plugin ").append(WSText.of(name, EnumTextColor.YELLOW))
							.append(WSText.of("!")).build());
			return EnumLoadPluginStatus.ERROR;
		}
		return EnumLoadPluginStatus.LOADED;
	}


	public void unloadPlugin(WSPlugin plugin) {
		WSPluginLoader.getInstance().disablePlugin(plugin);
		plugins.remove(plugin);
		//if (timings.containsKey(plugin)) timings.remove(plugin).close();
	}


	public File getPluginFolder() {
		return pluginFolder;
	}

	public Set<WSPlugin> getPlugins() {
		return new HashSet<>(plugins);
	}


	public Optional<File> getPluginFile(String name) {
		return Optional.ofNullable(pluginFiles.get(name.toLowerCase()));
	}


	public Map<String, File> getPluginFiles() {
		return new HashMap<>(pluginFiles);
	}


	public Optional<WSPlugin> getPlugin(String name) {
		return plugins.stream().filter(plugin -> plugin.getId().equalsIgnoreCase(name)).findAny();
	}


	public boolean isPluginEnabled(String name) {
		return getPlugin(name).isPresent();
	}

	public Set<WSBasePlugin> getBasePlugins() {
		return BridgePluginManager.getBasePlugins();
	}

	public Optional<WSBasePlugin> getBasePlugin(String name) {
		return getBasePlugins().stream().filter(target -> target.getName().equals(name)).findAny();
	}


	public boolean isBasePluginEnabled(String name) {
		return BridgePluginManager.isBasePluginEnabled(name);
	}
}
