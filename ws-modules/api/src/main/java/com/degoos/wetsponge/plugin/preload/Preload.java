package com.degoos.wetsponge.plugin.preload;

import com.degoos.wetsponge.plugin.WSPluginDescription;
import java.io.File;

public abstract class Preload {

	private String pluginId;

	public Preload(String pluginId) {
		this.pluginId = pluginId;
	}

	public String getPluginId() {
		return pluginId;
	}

	public abstract File onLoad(WSPluginDescription description, File file);

}
