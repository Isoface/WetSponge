package com.degoos.wetsponge.resource;


import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.text.WSText;
import com.degoos.wetsponge.util.InternalLogger;

import java.sql.Driver;
import java.sql.DriverManager;

public class SQLDriverUtils {
	public static void loadDrivers() {
		try {
			DriverManager.registerDriver((Driver) Class.forName("org.sqlite.JDBC").newInstance());
			switch (WetSponge.getServerType()) {
				case SPIGOT:
				case PAPER_SPIGOT:
					DriverManager.registerDriver((Driver) Class.forName("com.mysql.jdbc.Driver").newInstance());
					break;
				case SPONGE:
					DriverManager.registerDriver((Driver) Class.forName("org.h2.Driver").newInstance());
					DriverManager.registerDriver((Driver) Class.forName("org.mariadb.jdbc.Driver").newInstance());
					break;
			}
		} catch (Throwable e) {
			InternalLogger.sendError(WSText.of("Error loading a JDBC driver"));
			e.printStackTrace();
		}
	}
}
