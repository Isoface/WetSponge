package com.degoos.wetsponge.resource;

import it.unimi.dsi.fastutil.objects.ObjectCollection;
import it.unimi.dsi.fastutil.objects.ObjectIterator;
import java.util.Collection;

public class WSObjectCollection<T> implements ObjectCollection<T> {

	private ObjectCollection<T> collection;

	public WSObjectCollection (ObjectCollection<T> collection) {
		this.collection = collection;
	}


	@Override
	public int size() {
		return collection.size();
	}

	@Override
	public boolean isEmpty() {
		return collection.isEmpty();
	}

	@Override
	public boolean contains(Object o) {
		return collection.contains(o);
	}

	@Override
	public ObjectIterator<T> iterator() {
		return collection.iterator();
	}

	@Override
	public Object[] toArray() {
		return collection.toArray();
	}

	@Override
	public ObjectIterator<T> objectIterator() {
		return collection.objectIterator();
	}

	@Override
	public <T1> T1[] toArray(T1[] a) {
		return collection.toArray(a);
	}

	@Override
	public boolean add(T t) {
		return collection.add(t);
	}

	@Override
	public boolean remove(Object o) {
		return collection.remove(o);
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		return collection.containsAll(c);
	}

	@Override
	public boolean addAll(Collection<? extends T> c) {
		return collection.addAll(c);
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		return collection.removeAll(c);
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		return collection.retainAll(c);
	}

	@Override
	public void clear() {
		collection.clear();
	}
}
