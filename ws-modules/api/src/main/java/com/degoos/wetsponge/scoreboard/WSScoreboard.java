package com.degoos.wetsponge.scoreboard;

import com.degoos.wetsponge.enums.EnumCriteria;
import com.degoos.wetsponge.enums.EnumDisplaySlot;
import com.degoos.wetsponge.text.WSText;

import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public interface WSScoreboard {


	Optional<WSObjective> getObjective(String name);

	Optional<WSObjective> getObjective(EnumDisplaySlot displaySlot);

	WSObjective getOrCreateObjective(String name, WSText displayName, EnumCriteria criteria);

	void updateDisplaySlot(WSObjective objective, EnumDisplaySlot displaySlot);

	default void clearSlot(EnumDisplaySlot displaySlot) {
		updateDisplaySlot(null, displaySlot);
	}

	Set<WSObjective> getObjectives();

	default Set<WSObjective> getObjectivesByCriteria(EnumCriteria criteria) {
		return getObjectives().stream().filter(objective -> objective.getCriteria().equals(criteria)).collect(Collectors.toSet());
	}

	void removeObjective(WSObjective objective);

	void clearObjectives();

	Optional<WSTeam> getTeam(String name);

	boolean hasTeam(String name);

	WSTeam getOrCreateTeam(String name);

	Set<WSTeam> getTeams();

	Optional<WSTeam> getMemberTeam(WSText member);

	boolean unregisterTeam(WSTeam team);

	void unregisterAllTeams();

	Object getHandled();
}
