package com.degoos.wetsponge.scoreboard;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.enums.EnumCriteria;
import com.degoos.wetsponge.enums.EnumServerType;
import com.degoos.wetsponge.enums.EnumTextColor;
import com.degoos.wetsponge.text.WSText;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;

public class WSUpdatableObjective implements WSObjective {

	private WSText title;
	private Set<WSObjectiveReplacement> titleReplacements;
	private Map<Integer, WSText> defaultTexts;
	private Map<WSObjectiveReplacement, Set<Integer>> replacements;
	private WSPlayer player;


	private WSObjective objective;

	public WSUpdatableObjective(WSPlayer player, WSScoreboard scoreboard, String name, WSText title, List<WSText> texts, WSObjectiveReplacement... replacements) {
		Map<Integer, WSText> textsMap = new HashMap<>();
		int i = texts.size();
		for (WSText text : texts) {
			textsMap.put(i, text);
			i--;
		}
		objective = scoreboard.getOrCreateObjective(name, WSText.empty(), EnumCriteria.DUMMY);
		this.player = player;
		this.title = title;
		this.defaultTexts = textsMap;
		configureReplacements(replacements);
		insertIntoObjective();
	}

	public WSUpdatableObjective(WSPlayer player, WSScoreboard scoreboard, String name, WSText title, Map<Integer, WSText> texts, WSObjectiveReplacement...
		replacements) {
		objective = scoreboard.getOrCreateObjective(name, WSText.empty(), EnumCriteria.DUMMY);
		this.player = player;
		this.title = title;
		this.defaultTexts = texts;
		configureReplacements(replacements);
		insertIntoObjective();
	}

	private void configureReplacements(WSObjectiveReplacement... replacements) {
		this.replacements = new HashMap<>();
		this.titleReplacements = new HashSet<>();
		for (WSObjectiveReplacement replacement : replacements) {
			Set<Integer> set = new HashSet<>();
			for (Entry<Integer, WSText> text : defaultTexts.entrySet())
				if (text.getValue().contains(replacement.getKey())) set.add(text.getKey());
			this.replacements.put(replacement, set);
			if (title.contains(replacement.getKey())) titleReplacements.add(replacement);
		}
	}

	private void insertIntoObjective() {
		WSText title = this.title;
		for (WSObjectiveReplacement replacement : titleReplacements)
			title = replacement.apply(title, player);
		setDisplayName(title);
		defaultTexts.forEach(this::insertIntoObjective);
	}

	private void insertIntoObjective(Integer index) {
		if (!defaultTexts.containsKey(index)) return;
		insertIntoObjective(index, defaultTexts.get(index));
	}

	private void insertIntoObjective(Integer index, WSText text) {
		if (WetSponge.getServerType() == EnumServerType.SPIGOT || WetSponge.getServerType() == EnumServerType.PAPER_SPIGOT) {
			for (Entry<WSObjectiveReplacement, Set<Integer>> entry : replacements.entrySet())
				if (entry.getValue().contains(index)) text = entry.getKey().apply(text, player);
			WSText newText = text.toBuilder().append(WSText.of("", EnumTextColor.getByChar(String.valueOf(index % 10).charAt(0)).orElse(EnumTextColor.WHITE)))
				.append(WSText.of("", EnumTextColor.getByChar(String.valueOf(index % 3).charAt(0)).orElse(EnumTextColor.WHITE))).build();
			if (text.toFormattingText().equals(newText.toFormattingText())) return;
			removeAllScoresWithScore(index);
			getOrCreateScore(newText).setScore(index);
			return;
		}
		for (Entry<WSObjectiveReplacement, Set<Integer>> entry : replacements.entrySet())
			if (entry.getValue().contains(index)) text = entry.getKey().apply(text, player);
		WSText newText = text.toBuilder().append(WSText.of("", EnumTextColor.getByChar(String.valueOf(index % 10).charAt(0)).orElse(EnumTextColor.WHITE)))
			.append(WSText.of("", EnumTextColor.getByChar(String.valueOf(index % 3).charAt(0)).orElse(EnumTextColor.WHITE))).build();

		Optional<WSScore> score = getScore(text);
		if (score.isPresent()) score.get().setScore(index);
		else {
			removeAllScoresWithScore(index);
			getOrCreateScore(newText).setScore(index);
		}
	}

	public void update(String replacementKey) {
		if (titleReplacements.stream().anyMatch(target -> target.getKey().equals(replacementKey))) {
			WSText title = this.title;
			for (WSObjectiveReplacement replacement : titleReplacements)
				title = replacement.apply(title, player);
			setDisplayName(title);
		}
		replacements.entrySet().stream().filter(entry -> entry.getKey().getKey().equals(replacementKey)).findAny()
			.ifPresent(entry -> entry.getValue().forEach(this::insertIntoObjective));
	}


	public WSPlayer getPlayer() {
		return player;
	}

	@Override
	public String getName() {
		return objective.getName();
	}

	@Override
	public WSText getDisplayName() {
		return objective.getDisplayName();
	}

	@Override
	public void setDisplayName(WSText text) {
		objective.setDisplayName(text);
	}

	@Override
	public EnumCriteria getCriteria() {
		return objective.getCriteria();
	}

	@Override
	public Map<WSText, WSScore> getScores() {
		return objective.getScores();
	}

	@Override
	public boolean hasScore(WSText name) {
		return objective.hasScore(name);
	}

	@Override
	public WSScore getOrCreateScore(WSText name) {
		return objective.getOrCreateScore(name);
	}

	@Override
	public boolean removeScore(WSText name) {
		return objective.removeScore(name);
	}

	@Override
	public boolean removeScore(WSScore score) {
		return objective.removeScore(score);
	}

	@Override
	public void removeAllScoresWithScore(int score) {
		objective.removeAllScoresWithScore(score);
	}

	@Override
	public Optional<WSScoreboard> getScoreboard() {
		return objective.getScoreboard();
	}


	public WSObjective getObjective() {
		return objective;
	}

	@Override
	public Object getHandled() {
		return objective.getHandled();
	}
}
