package com.degoos.wetsponge.server;

import com.degoos.wetsponge.bridge.server.BridgeFavicon;

import java.awt.image.BufferedImage;

/**
 * This class represents the server icon.
 */
public interface WSFavicon {

	public static WSFavicon ofEncoded(String encoded) {
		return BridgeFavicon.ofEncoded(encoded);
	}

	/**
	 * Returns the {@link BufferedImage image} of the {@link WSFavicon favicon}.
	 *
	 * @return the {@link BufferedImage image}.
	 */
	BufferedImage getImage();

	/**
	 * Returns the Base64 value of the {@link WSFavicon favicon}.
	 * WARNING! The string must be "data:image/png;base64,"+ encoded image!
	 *
	 * @return the Base64 value.
	 */
	String toBase64();

}
