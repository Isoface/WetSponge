package com.degoos.wetsponge.server.response;

import com.degoos.wetsponge.util.Validate;

public class WSStatusVersion {

	private String name;
	private int protocol;
	private boolean changed;

	public WSStatusVersion(String name, int protocol) {
		this.name = name;
		this.protocol = protocol;
		this.changed = false;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		Validate.notNull(name, "Name cannot be null!");
		this.name = name;
		changed = true;
	}

	public int getProtocol() {
		return protocol;
	}

	public void setProtocol(int protocol) {
		this.protocol = protocol;
		changed = true;
	}

	public boolean hasChanged() {
		return changed;
	}

}
