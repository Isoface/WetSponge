package com.degoos.wetsponge.skin;

import com.google.gson.annotations.SerializedName;

class JMineskin {

	public int id;
	public String name;
	public JSkinData data;
	public long timestamp;
	@SerializedName("private")
	public boolean prvate;
	public int views;
	public double nextRequest;

}
