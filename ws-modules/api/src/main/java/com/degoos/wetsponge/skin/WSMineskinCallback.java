package com.degoos.wetsponge.skin;

/**
 * Using an implementation of this interface you can get and upload {@link WSMineskin}s using a {@link WSMineskinClient} instance.
 * <p>
 * Methods are called along the upload / download process, being the method {@link #onDone(WSMineskin)} the last called, giving you
 * the skin instance.
 * <p>
 * When an error occurs, methods {@link #onCommonException(Exception)}, {@link #onParseException(Exception, String)}
 * and {@link #onThrowable(Throwable)} are called.
 */
public interface WSMineskinCallback {

	/**
	 * This method is called when the process is done, giving the {@link WSMineskin} instance.
	 *
	 * @param mineskin the skin.
	 */
	void onDone(WSMineskin mineskin);

	/**
	 * This method is called when the process has to wait before uploading a new skin.
	 *
	 * @param delay the amount of time the process has to wait before uploading in milliseconds.
	 */
	void onWaiting(long delay);

	/**
	 * This method is called when a new skin is about to being uploaded.
	 */
	void onUploading();

	/**
	 * This error is called when a website error has occurred.
	 *
	 * @param error the error.
	 */
	void onError(String error);

	/**
	 * This error is called when an unknown exception has occurred.
	 *
	 * @param ex the exception.
	 */
	void onCommonException(Exception ex);

	/**
	 * This method is called when a throwable error has occurred.
	 *
	 * @param throwable the throwable.
	 */
	void onThrowable(Throwable throwable);

	/**
	 * This method is called when a parse exception has occurred.
	 *
	 * @param ex   the exception.
	 * @param body the serialized json.
	 */
	void onParseException(Exception ex, String body);

}
