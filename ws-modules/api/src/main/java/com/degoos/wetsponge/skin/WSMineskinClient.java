package com.degoos.wetsponge.skin;

import com.degoos.wetsponge.util.Validate;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;
import org.jsoup.Connection;
import org.jsoup.Jsoup;

import java.io.File;
import java.io.FileInputStream;
import java.util.UUID;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/**
 * This class allows plugins to interact with the website https://mineskin.org/. If you upload a skin you will be
 * able to use it on heads or even profiles. You can also get already created skins, so don't create a new {@link WSMineskin}
 * every time you want to get it!
 */
public class WSMineskinClient {

	public static final String ID_FORMAT = "http://api.mineskin.org/get/id/%s";
	public static final String URL_FORMAT = "http://api.mineskin.org/generate/url?url=%s&%s";
	public static final String UPLOAD_FORMAT = "http://api.mineskin.org/generate/upload?%s";
	public static final String USER_FORMAT = "http://api.mineskin.org/generate/user/%s?%s";

	private Executor executor;
	private String userAgent;

	private JsonParser jsonParser;
	private Gson gson;

	private long nextRequest;

	/**
	 * Creates a new {@link WSMineskinClient} instance using default parameters.
	 */
	public WSMineskinClient() {
		this(Executors.newSingleThreadExecutor(), "MineSkin-JavaClient");
	}

	/**
	 * Creates a new {@link WSMineskinClient} instance using a default user agent and the given {@link Executor}.
	 *
	 * @param executor the executor.
	 */
	public WSMineskinClient(Executor executor) {
		this(executor, "MineSkin-JavaClient");
	}

	/**
	 * Creates a new {@link WSMineskinClient} instance using a default {@link Executor} and the given user agent.
	 *
	 * @param userAgent the user agent.
	 */
	public WSMineskinClient(String userAgent) {
		this(Executors.newSingleThreadExecutor(), userAgent);
	}

	/**
	 * Creates a new {@link WSMineskinClient} instance using given user agent and {@link Executor}.
	 *
	 * @param executor  the executor.
	 * @param userAgent the user agent.
	 */
	public WSMineskinClient(Executor executor, String userAgent) {
		Validate.notNull(executor, "Executor cannot be null!");
		Validate.notNull(userAgent, "User agent cannot be null!");
		this.executor = executor;
		this.userAgent = userAgent;
		this.gson = new Gson();
		this.jsonParser = new JsonParser();
	}

	/**
	 * Returns the time the client has to wait before uploading a new skin.
	 *
	 * @return the time in milliseconds.
	 */
	public long getNextRequest() {
		return nextRequest;
	}

	/**
	 * Tries to get an existent {@link WSMineskin}.
	 *
	 * @param skinId   the skin id.
	 * @param callback the callback which will receive the {@link WSMineskin} if present.
	 * @see WSMineskinCallback
	 */
	public void getMineskin(int skinId, WSMineskinCallback callback) {
		Validate.notNull(callback, "Callback cannot be null!");
		executor.execute(() -> {
			try {
				Connection connection = Jsoup.connect(String.format(ID_FORMAT, skinId))
						.userAgent(userAgent).method(Connection.Method.GET).ignoreContentType(true).ignoreContentType(true).timeout(10000);
				onResponse(connection.execute().body(), callback);
			} catch (Exception ex) {
				callback.onCommonException(ex);
			} catch (Throwable throwable) {
				callback.onThrowable(throwable);
			}
		});
	}


	/**
	 * Creates a new {@link WSMineskin} using an URL.
	 *
	 * @param url      the url.
	 * @param options  the options of the {@link WSMineskin}.
	 * @param callback the callback which will receive the {@link WSMineskin}.
	 */
	public void createSkinFromURL(String url, WSMineskinOptions options, WSMineskinCallback callback) {
		Validate.notNull(url, "URL cannot be null!");
		Validate.notNull(options, "Options cannot be null!");
		Validate.notNull(callback, "Callback cannot be null!");
		executor.execute(() -> {
			try {
				if (System.currentTimeMillis() < nextRequest) {
					long delay = (nextRequest - System.currentTimeMillis());
					callback.onWaiting(delay);
					Thread.sleep(delay + 1000);
				}
				callback.onUploading();
				Connection connection = Jsoup.connect(String.format(URL_FORMAT, url, options.toUrlParam()))
						.userAgent(userAgent).method(Connection.Method.POST).ignoreContentType(true).ignoreHttpErrors(true).timeout(10000);
				onResponse(connection.execute().body(), callback);
			} catch (Exception e) {
				callback.onCommonException(e);
			} catch (Throwable throwable) {
				callback.onThrowable(throwable);
			}
		});
	}

	/**
	 * Creates a new {@link WSMineskin} using a .png file.
	 *
	 * @param file     the .png file.
	 * @param options  the options of the {@link WSMineskin}.
	 * @param callback the callback which will receive the {@link WSMineskin}.
	 */
	public void createSkinFromFile(File file, WSMineskinOptions options, WSMineskinCallback callback) {
		Validate.notNull(file, "File cannot be null!");
		Validate.notNull(options, "Options cannot be null!");
		Validate.notNull(callback, "Callback cannot be null!");
		executor.execute(() -> {
			try {
				if (System.currentTimeMillis() < nextRequest) {
					long delay = (nextRequest - System.currentTimeMillis());
					callback.onWaiting(delay);
					Thread.sleep(delay + 1000);
				}
				callback.onUploading();
				Connection connection = Jsoup.connect(String.format(UPLOAD_FORMAT, options.toUrlParam()))
						.userAgent(userAgent).method(Connection.Method.POST).data("file", file.getName(), new FileInputStream(file))
						.ignoreContentType(true).ignoreHttpErrors(true).timeout(10000);
				onResponse(connection.execute().body(), callback);
			} catch (Exception e) {
				callback.onCommonException(e);
			} catch (Throwable throwable) {
				callback.onThrowable(throwable);
			}
		});
	}

	/**
	 * Creates a new {@link WSMineskin} using an existent and premium Minecraft user.
	 *
	 * @param userUniqueId user unique id.
	 * @param options      the options of the {@link WSMineskin}.
	 * @param callback     the callback which will receive the {@link WSMineskin}.
	 */
	public void createSkinFromUser(UUID userUniqueId, WSMineskinOptions options, WSMineskinCallback callback) {
		Validate.notNull(userUniqueId, "User unique id cannot be null!");
		Validate.notNull(options, "Options cannot be null!");
		Validate.notNull(callback, "Callback cannot be null!");
		executor.execute(() -> {
			try {
				if (System.currentTimeMillis() < nextRequest) {
					long delay = (nextRequest - System.currentTimeMillis());
					callback.onWaiting(delay);
					Thread.sleep(delay + 1000);
				}
				callback.onUploading();
				Connection connection = Jsoup
						.connect(String.format(USER_FORMAT, userUniqueId.toString(), options.toUrlParam())).userAgent(userAgent).method(Connection.Method.GET)
						.ignoreContentType(true).ignoreHttpErrors(true).timeout(10000);
				onResponse(connection.execute().body(), callback);
			} catch (Exception e) {
				callback.onCommonException(e);
			} catch (Throwable throwable) {
				throw new RuntimeException(throwable);
			}
		});
	}

	private void onResponse(String body, WSMineskinCallback callback) {
		try {
			JsonObject jsonObject = jsonParser.parse(body).getAsJsonObject();
			if (jsonObject.has("error")) {
				callback.onError(jsonObject.get("error").getAsString());
				return;
			}

			JMineskin jMineskin = gson.fromJson(jsonObject, JMineskin.class);
			WSMineskin mineskin = new WSMineskin(jMineskin);
			this.nextRequest = System.currentTimeMillis() + ((long) (jMineskin.nextRequest * 1000L));
			callback.onDone(mineskin);
		} catch (JsonParseException ex) {
			callback.onParseException(ex, body);
		} catch (Exception ex) {
			callback.onCommonException(ex);
		} catch (Throwable throwable) {
			callback.onThrowable(throwable);
		}
	}
}
