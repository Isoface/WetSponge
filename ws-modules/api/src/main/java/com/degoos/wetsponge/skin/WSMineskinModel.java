package com.degoos.wetsponge.skin;

/**
 * Represents the model type of a {@link WSMineskin} that will be displayed in the website https://mineskin.org/.
 */
public enum WSMineskinModel {

	STEVE("steve"),
	ALEX("slim");

	private String value;

	WSMineskinModel(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}
}
