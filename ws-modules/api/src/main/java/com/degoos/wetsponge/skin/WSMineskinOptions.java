package com.degoos.wetsponge.skin;

import com.degoos.wetsponge.util.Validate;

/**
 * This class represents a set of options that are required to upload a skin to https://mineskin.org/.
 */
public class WSMineskinOptions {

	private static final String PARAM_FORMAT = "name=%s&model=%s&visibility=%s";

	private String name;
	private WSMineskinModel model;
	private WSMineskinVisibility visibility;

	/**
	 * Creates a new instance by a name, a {@link WSMineskinModel model type} and a {@link WSMineskinVisibility visibility option}.
	 * Only the name can be null.
	 *
	 * @param name       the name.
	 * @param model      the model type.
	 * @param visibility the visibility option.
	 */
	public WSMineskinOptions(String name, WSMineskinModel model, WSMineskinVisibility visibility) {
		Validate.notNull(model, "Model cannot be null!");
		Validate.notNull(visibility, "Visibility cannot be null!");
		this.name = name == null ? "" : name;
		this.model = model;
		this.visibility = visibility;
	}

	/**
	 * Returns the displayed name of the skin.
	 *
	 * @return the displayed name.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the displayed name of the skin.
	 *
	 * @param name the displayed name.
	 */
	public void setName(String name) {
		this.name = name == null ? "" : name;
	}

	/**
	 * Returns the {@link WSMineskinModel model type} of the skin.
	 *
	 * @return the {@link WSMineskinModel model type}.
	 */
	public WSMineskinModel getModel() {
		return model;
	}

	/**
	 * Sets the {@link WSMineskinModel model type} of the skin.
	 *
	 * @param model the {@link WSMineskinModel model type}.
	 */
	public void setModel(WSMineskinModel model) {
		Validate.notNull(model, "Model cannot be null!");
		this.model = model;
	}

	/**
	 * Returns the {@link WSMineskinVisibility visibility option} of the skin.
	 *
	 * @return the {@link WSMineskinVisibility visibility option}.
	 */
	public WSMineskinVisibility getVisibility() {
		return visibility;
	}

	/**
	 * Sets the {@link WSMineskinVisibility visibility option} of the skin.
	 *
	 * @param visibility the {@link WSMineskinVisibility visibility option}.
	 */
	public void setVisibility(WSMineskinVisibility visibility) {
		Validate.notNull(visibility, "Visibility cannot be null!");
		this.visibility = visibility;
	}

	String toUrlParam() {
		return String.format(PARAM_FORMAT, this.name, this.model.getValue(), this.visibility.getValue());
	}
}
