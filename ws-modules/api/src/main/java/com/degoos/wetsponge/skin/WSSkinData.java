package com.degoos.wetsponge.skin;

import com.degoos.wetsponge.user.WSGameProfile;
import com.degoos.wetsponge.util.Validate;

import java.util.UUID;

/**
 * This class associates a {@link UUID user unique id} and a {@link WSSkinTexture skin texture}. It's used by {@link WSMineskinClient}
 */
public class WSSkinData {

	private UUID uniqueId;
	private WSSkinTexture texture;

	/**
	 * Creates a new {@link WSSkinData} using an {@link UUID user unique id} and a {@link WSSkinTexture texture}. They cannot be null.
	 *
	 * @param uniqueId the {@link UUID user unique id}.
	 * @param texture  the {@link WSSkinTexture texture}.
	 */
	public WSSkinData(UUID uniqueId, WSSkinTexture texture) {
		Validate.notNull(uniqueId, "Unique id cannot be null!");
		Validate.notNull(texture, "Texture cannot be null!");
		this.uniqueId = uniqueId;
		this.texture = texture;
	}

	WSSkinData(JSkinData skinData) {
		this.uniqueId = skinData.uuid;
		this.texture = new WSSkinTexture(skinData.texture);
	}

	/**
	 * Returns the {@link UUID user unique id} of the skin data.
	 *
	 * @return the {@link UUID user unique id}.
	 */
	public UUID getUniqueId() {
		return uniqueId;
	}

	/**
	 * Sets the {@link UUID user unique id} of the skin data.
	 *
	 * @param uniqueId the {@link UUID user unique id}.
	 */
	public void setUniqueId(UUID uniqueId) {
		Validate.notNull(uniqueId, "Unique id cannot be null!");
		this.uniqueId = uniqueId;
	}

	/**
	 * Returns the {@link WSSkinTexture texture} of the skin data.
	 *
	 * @return the {@link WSSkinTexture texture}.
	 */
	public WSSkinTexture getTexture() {
		return texture;
	}

	/**
	 * Sets the {@link WSSkinTexture texture} of the skin data.
	 *
	 * @param texture the {@link WSSkinTexture texture}.
	 */
	public void setTexture(WSSkinTexture texture) {
		Validate.notNull(texture, "Texture cannot be null!");
		this.texture = texture;
	}

	/**
	 * Transforms this skin into a {@link WSGameProfile game profile}. This can be used to create skulls and other things.
	 *
	 * @return the {@link WSGameProfile game profile}.
	 */
	public WSGameProfile toGameProfile() {
		return WSGameProfile.of(uniqueId).addProperty(texture.toProperty());
	}

	/**
	 * Transforms this skin into a {@link WSGameProfile game profile}. This can be used to create skulls and other things.
	 *
	 * @param name the name of the {@link WSGameProfile game profile}.
	 * @return the {@link WSGameProfile game profile}.
	 */
	public WSGameProfile toGameProfile(String name) {
		return WSGameProfile.of(uniqueId, name).addProperty(texture.toProperty());
	}

	/**
	 * Clones this skin data. This clones the texture too.
	 *
	 * @return the new instance.
	 */
	public WSSkinData clone() {
		return new WSSkinData(uniqueId, texture.clone());
	}
}
