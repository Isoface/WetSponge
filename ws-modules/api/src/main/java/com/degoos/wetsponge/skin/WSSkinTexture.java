package com.degoos.wetsponge.skin;

import com.degoos.wetsponge.user.WSProfileProperty;
import com.degoos.wetsponge.util.Validate;

/**
 * This class represents a skin texture. It can be transform into a {@link WSProfileProperty profile property}.
 * It's used by {@link WSMineskinClient}.
 */
public class WSSkinTexture {

	private String value;
	private String signature;
	private String url;

	/**
	 * Creates a new {@link WSSkinTexture} by a value, a signature and a url. The value cannot be null.
	 *
	 * @param value     the value.
	 * @param signature the signature.
	 * @param url       the url.
	 */
	public WSSkinTexture(String value, String signature, String url) {
		Validate.notNull(value, "Value cannot be null!");
		this.value = value;
		this.signature = signature;
		this.url = url;
	}

	WSSkinTexture(JSkinTexture texture) {
		this.value = texture.value;
		this.signature = texture.signature;
		this.url = texture.url;
	}

	/**
	 * Returns the value of the texture.
	 *
	 * @return the value.
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Sets the value of the texture.
	 *
	 * @param value the value.
	 */
	public void setValue(String value) {
		Validate.notNull(value, "Value cannot be null!");
		this.value = value;
	}

	/**
	 * Returns the signature of the texture.
	 * This is used by Minecraft to check whether the texture is a legacy texture.
	 *
	 * @return the signature.
	 */
	public String getSignature() {
		return signature;
	}

	/**
	 * Sets the signature of the texture.
	 * This is used by Minecraft to check whether the texture is a legacy texture.
	 *
	 * @param signature the signature.
	 */
	public void setSignature(String signature) {
		this.signature = signature;
	}

	/**
	 * Returns the URL of the texture. The value should be an Base 64 encoded version of this URL.
	 *
	 * @return the URL.
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * Sets the URL of the texture. The value should be an Base 64 encoded version of this URL.
	 *
	 * @param url the URL.
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * Transforms the {@link WSSkinTexture} into a {@link WSProfileProperty} instance.
	 *
	 * @return the {@link WSProfileProperty}.
	 */
	public WSProfileProperty toProperty() {
		return WSProfileProperty.of("textures", value, signature);
	}

	/**
	 * Clones this texture.
	 *
	 * @return the new instance.
	 */
	public WSSkinTexture clone() {
		return new WSSkinTexture(value, signature, url);
	}
}
