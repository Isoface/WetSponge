package com.degoos.wetsponge.text;

/**
 * Represents a nameable object.
 */
public interface WSNameable {

    /**
     * @return the {@link WSText custom name} of this {@link WSNameable nameable}.
     */
    WSText getCustomName();

    /**
     * Sets the {@link WSText custom name} of this {@link WSNameable nameable}.
     * @param customName the {@link WSText custom name}.
     */
    void setCustomName(WSText customName);

}
