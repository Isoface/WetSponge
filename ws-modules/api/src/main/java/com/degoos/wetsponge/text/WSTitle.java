package com.degoos.wetsponge.text;

import java.util.Optional;

/**
 * This class represents a title. A title contains the main title, the subtitle and the actionbar. If you don't want to use any of these variables,
 * just set them to null!
 */
public class WSTitle {

	private Optional<WSText> title, subtitle, actionBar;
	private Optional<Integer> fadeIn, stay, fadeOut;
	private boolean clear, reset;

	public WSTitle(WSText title, WSText subtitle, WSText actionBar,
				   Integer fadeIn, Integer stay, Integer fadeOut, boolean clear, boolean reset) {
		this.title = Optional.ofNullable(title);
		this.subtitle = Optional.ofNullable(subtitle);
		this.actionBar = Optional.ofNullable(actionBar);
		this.fadeIn = Optional.ofNullable(fadeIn);
		this.stay = Optional.ofNullable(stay);
		this.fadeOut = Optional.ofNullable(fadeOut);
		this.clear = clear;
		this.reset = reset;
	}

	public Optional<WSText> getTitle() {
		return title;
	}

	public void setTitle(WSText title) {
		this.title = Optional.ofNullable(title);
	}

	public Optional<WSText> getSubtitle() {
		return subtitle;
	}

	public void setSubtitle(WSText subtitle) {
		this.subtitle = Optional.ofNullable(subtitle);
	}


	public Optional<WSText> getActionBar() {
		return actionBar;
	}

	public void setActionBar(WSText actionBar) {
		this.actionBar = Optional.ofNullable(actionBar);
	}

	public Optional<Integer> getFadeIn() {
		return fadeIn;
	}

	public void setFadeIn(Integer fadeIn) {
		this.fadeIn = Optional.ofNullable(fadeIn);
	}

	public Optional<Integer> getStay() {
		return stay;
	}

	public void setStay(Integer stay) {
		this.stay = Optional.ofNullable(stay);
	}

	public Optional<Integer> getFadeOut() {
		return fadeOut;
	}

	public void setFadeOut(Integer fadeOut) {
		this.fadeOut = Optional.ofNullable(fadeOut);
	}

	/**
	 * Returns whether the {@link WSTitle title} is a clear title.
	 * <p>
	 * A clear {@link WSTitle title} clears every title in the current screen
	 * of the {@link com.degoos.wetsponge.entity.living.player.WSPlayer player}.
	 * <p>
	 * All other values of this object won't be used if the title is a clear title.
	 *
	 * @return whether the {@link WSTitle title} is a clear title.
	 */
	public boolean isClear() {
		return clear;
	}

	/**
	 * Sets if this {@link WSTitle title} is a clear title.
	 *
	 * @param clear the boolean.
	 * @see #isClear() Clear titles.
	 */
	public void setClear(boolean clear) {
		this.clear = clear;
	}

	/**
	 * Returns whether the {@link WSTitle title} is a reset title.
	 * <p>Reset titles work as the same way as a clear title, but these
	 * also reset all fade values to their default values.</p>
	 * <p>
	 * Default values:
	 * <ul>
	 * <li>- {@link #getFadeIn() fadeIn} 20 ticks.</li>
	 * <li>- {@link #getStay() stay} 60 ticks.</li>
	 * <li>- {@link #getFadeOut() fadeOut} 20 ticks.</li>
	 * </ul>
	 *
	 * @return whether the {@link WSTitle title} is a reset title.
	 * @see #isClear() Clear titles.
	 */
	public boolean isReset() {
		return reset;
	}

	/**
	 * Sets if this {@link WSTitle title} is a reset title.
	 *
	 * @param reset the boolean.
	 * @see #isReset() Reset titles.
	 */
	public void setReset(boolean reset) {
		this.reset = reset;
	}
}
