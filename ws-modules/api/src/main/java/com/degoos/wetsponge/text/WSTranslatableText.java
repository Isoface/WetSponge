package com.degoos.wetsponge.text;

import com.degoos.wetsponge.text.translation.WSTranslatable;

public interface WSTranslatableText extends WSText, WSTranslatable {}
