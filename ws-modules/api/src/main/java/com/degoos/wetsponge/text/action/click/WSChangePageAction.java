package com.degoos.wetsponge.text.action.click;

import com.degoos.wetsponge.bridge.text.action.BridgeTextAction;

public interface WSChangePageAction extends WSClickAction {

	static WSChangePageAction of(int page) {
		return BridgeTextAction.newWSChangePageAction(page);
	}

	int getPage();

}
