package com.degoos.wetsponge.text.action.click;

import com.degoos.wetsponge.bridge.text.action.BridgeTextAction;

import java.net.URL;

public interface WSOpenURLAction extends WSClickAction {

	public static WSOpenURLAction of(URL url) {
		return BridgeTextAction.newWSOpenURLAction(url);
	}

	URL getURL();

}
