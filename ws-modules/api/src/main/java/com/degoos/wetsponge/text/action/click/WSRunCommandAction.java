package com.degoos.wetsponge.text.action.click;

import com.degoos.wetsponge.bridge.text.action.BridgeTextAction;

public interface WSRunCommandAction extends WSClickAction {

	static WSRunCommandAction of(String command) {
		return BridgeTextAction.newWSRunCommandAction(command);
	}

	String getCommand();
}
