package com.degoos.wetsponge.text.action.click;

import com.degoos.wetsponge.bridge.text.action.BridgeTextAction;

public interface WSSuggestCommandAction extends WSClickAction {

	public static WSSuggestCommandAction of(String command) {
		return BridgeTextAction.newWSSuggestCommandAction(command);
	}

	String getCommand();
}
