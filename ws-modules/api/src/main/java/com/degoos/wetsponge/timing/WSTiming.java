package com.degoos.wetsponge.timing;

import co.aikar.wetspongeutils.JSONUtil;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class WSTiming {

	private WSTiming parent;
	private long timesExecuted;
	private long startTime, stopTime, maximumDelay, totalDelay;
	private List<WSTiming> subTimings;
	private String name;
	private long firstExecution, lastExecution;
	private StackTraceElement caller;

	public WSTiming(WSTiming parent, String name, StackTraceElement caller) {
		this.parent = parent;
		this.name = name;
		this.startTime = stopTime = maximumDelay = totalDelay = 0;
		this.subTimings = new LinkedList<>();
		this.caller = caller;
	}


	public void start() {
		startTime = System.nanoTime();
	}

	public void stop() {
		stopTime = System.nanoTime();
		long delay = stopTime - startTime;
		maximumDelay = Math.max(delay, maximumDelay);
		totalDelay += delay;

		lastExecution = System.currentTimeMillis();
		if (timesExecuted == 0) firstExecution = lastExecution;

		timesExecuted++;
	}

	public String getName() {
		return name;
	}

	public long getTimesExecuted() {
		return timesExecuted;
	}

	public long getStartTime() {
		return startTime;
	}

	public long getStopTime() {
		return stopTime;
	}

	public long getMaximumDelay() {
		return maximumDelay;
	}

	public long getTotalDelay() {
		return totalDelay;
	}

	public double getMillisMaximumDelay() {
		return maximumDelay / 1000000D;
	}

	public double getMillisTotalDelay() {
		return totalDelay / 1000000D;
	}

	public WSTiming getParent() {
		return parent;
	}

	public List<WSTiming> getSubTimings() {
		return subTimings;
	}

	public Map export() {
		Map map = JSONUtil.createObject(JSONUtil.pair("times", timesExecuted), JSONUtil.pair("maximum_delay", maximumDelay),
				JSONUtil.pair("total_delay", totalDelay), JSONUtil.pair("name", name),
				JSONUtil.pair("first_execution", firstExecution),
				JSONUtil.pair("last_execution", lastExecution));

		if (!subTimings.isEmpty()) map.put("timings", JSONUtil.toArrayMapper(subTimings, WSTiming::export));

		map.put("type", "simple");
		map.put("asynchronous", false);
		map.put("task_type", "instantaneous");
		map.put("delay", -1);
		map.put("interval", -1);
		map.put("times_to_execute", -1);
		map.put("caller", caller.toString());

		return map;
	}

	public void merge(WSTiming timing) {
		timesExecuted += timing.timesExecuted;
		maximumDelay = Math.max(maximumDelay, timing.maximumDelay);
		totalDelay += timing.totalDelay;
		timing.subTimings.forEach(toMerge -> {
			WSTiming subTiming = subTimings.stream().filter(target -> target.getName().equals(toMerge.getName())).findAny().orElse(null);
			if (subTiming == null) subTimings.add(toMerge);
			else subTiming.merge(toMerge);
		});
	}

}
