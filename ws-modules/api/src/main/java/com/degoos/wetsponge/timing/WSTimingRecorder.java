package com.degoos.wetsponge.timing;

import co.aikar.wetspongeutils.JSONUtil;
import com.degoos.wetsponge.plugin.WSPlugin;
import com.degoos.wetsponge.task.WSTask;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class WSTimingRecorder {


	private WSPlugin plugin;
	private List<WSTiming> timings;
	private WSTiming currentTiming;

	public WSTimingRecorder(WSPlugin plugin) {
		this.plugin = plugin;
		this.timings = new LinkedList<>();
		this.currentTiming = null;
	}

	public WSPlugin getPlugin() {
		return plugin;
	}

	public List<WSTiming> getTimings() {
		return timings;
	}


	public void startTiming(String name, StackTraceElement element) {
		if (currentTiming == null) {
			currentTiming = timings.stream().filter(target -> target.getName().equals(name)).findAny().orElse(null);
			if (currentTiming == null) {
				currentTiming = new WSTiming(null, name, element);
				timings.add(currentTiming);
			}
		} else {
			WSTiming timing = currentTiming.getSubTimings().stream().filter(target -> target.getName().equals(name)).findAny().orElse(null);
			if (timing == null) {
				timing = new WSTiming(currentTiming, name, element);
				currentTiming.getSubTimings().add(timing);
			}
			currentTiming = timing;
		}
		currentTiming.start();
	}

	public void startTiming(WSTask task) {
		String name = task.getPlugin().getId() + " task " + task.getUniqueId();
		if (currentTiming == null) {
			currentTiming = timings.stream().filter(target -> target.getName().equals(name)).findAny().orElse(null);
			if (currentTiming == null) {
				currentTiming = new WSTimingTask(null, task);
				timings.add(currentTiming);
			}
		} else {
			WSTiming timing = currentTiming.getSubTimings().stream().filter(target -> target.getName().equals(name)).findAny().orElse(null);
			if (timing == null) {
				timing = new WSTimingTask(currentTiming, task);
				currentTiming.getSubTimings().add(timing);
			}
			currentTiming = timing;
		}
		currentTiming.start();
	}

	public void stopTiming() {
		if (currentTiming == null) return;
		currentTiming.stop();
		currentTiming = currentTiming.getParent();
	}

	public Map export() {
		Map map = JSONUtil.createObject();
		map.put("id", plugin == null ? "WetSponge" : plugin.getId());
		map.put("timings", JSONUtil.toArrayMapper(timings, WSTiming::export));
		return map;
	}

	public void merge(WSTimingRecorder recorder) {
		recorder.timings.forEach(toMerge -> {
			WSTiming timing = timings.stream().filter(target -> target.getName().equals(toMerge.getName())).findAny().orElse(null);
			if (timing == null) timings.add(toMerge);
			else timing.merge(toMerge);
		});
	}
}
