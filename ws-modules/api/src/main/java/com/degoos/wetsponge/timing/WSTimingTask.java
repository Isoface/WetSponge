package com.degoos.wetsponge.timing;

import com.degoos.wetsponge.task.WSTask;

import java.util.Map;

public class WSTimingTask extends WSTiming {

	private WSTask task;

	public WSTimingTask(WSTiming parent, WSTask task) {
		super(parent, task.getPlugin().getId() + " task " + task.getUniqueId(), task.getCallerStackTraceElement());
		this.task = task;
	}

	@Override
	public Map export() {
		Map map = super.export();
		map.put("type", "task");
		map.put("asynchronous", task.isAsynchronous());
		map.put("task_type", task.isInstantaneous() ? "instantaneous" : task.isLater() ? "later" : "timer");
		map.put("delay", task.getDelay());
		map.put("interval", task.getInterval());
		map.put("times_to_execute", task.getTimesToExecute());
		return map;
	}
}
