package com.degoos.wetsponge.user;

import com.degoos.wetsponge.bridge.user.BridgeGameProfile;
import com.degoos.wetsponge.util.Validate;
import com.google.common.collect.Multimap;

import java.util.Optional;
import java.util.UUID;

public interface WSGameProfile {


	/**
	 * Creates a {@link WSGameProfile} from the provided ID.
	 * <p>
	 * The name of the created profile will be {@code null}.
	 *
	 * @param uniqueId the {@link UUID}.
	 * @return the created {@link WSGameProfile}.
	 */
	public static WSGameProfile of(UUID uniqueId) {
		return of(uniqueId, null);
	}

	/**
	 * Creates a {@link WSGameProfile} from the provided ID and name.
	 * The name can be {@code null}.
	 *
	 * @param uniqueId The {@link UUID}.
	 * @param name     The name.
	 * @return The created {@link WSGameProfile}.
	 */
	public static WSGameProfile of(UUID uniqueId, String name) {
		return BridgeGameProfile.of(uniqueId, name);
	}

	/**
	 * Returns the {@link UUID unique id} of the profile.
	 *
	 * @return the UUID.
	 */
	UUID getId();

	/**
	 * Gets the name associated with this profile.
	 *
	 * @return The associated name if present, otherwise {@link Optional#empty()}.
	 */
	Optional<String> getName();

	/**
	 * Gets the property {@link java.util.Map} for this profile.
	 * <p>
	 * This is an immutable map.
	 *
	 * @return The property map.
	 */
	Multimap<String, WSProfileProperty> getPropertyMap();

	/**
	 * Adds a profile property to this game profile.
	 * <p>
	 * The {@link WSProfileProperty#getName() name} of the property is used when
	 * adding the profile property to the {@link #getPropertyMap() property map}.
	 *
	 * @param property The profile property.
	 * @return The game profile.
	 */
	default WSGameProfile addProperty(WSProfileProperty property) {
		Validate.notNull(property, "Property cannot be null!");
		return this.addProperty(property.getName(), property);
	}

	/**
	 * Adds a profile property to this game profile.
	 *
	 * @param name     The name of the property.
	 * @param property The profile property.
	 * @return The game profile.
	 */
	WSGameProfile addProperty(String name, WSProfileProperty property);

	/**
	 * Removes a profile property to this game profile.
	 * <p>
	 * The {@link WSProfileProperty#getName() name} of the property is used when
	 * removing the profile property from the {@link #getPropertyMap() property map}.
	 *
	 * @param property The profile property.
	 * @return {@code true} if the property map changed.
	 */
	default boolean removeProperty(WSProfileProperty property) {
		Validate.notNull(property, "Property cannot be null!");
		return this.getPropertyMap().remove(property.getName(), property);
	}

	/**
	 * Removes a profile property to this game profile.
	 *
	 * @param name     The name of the property.
	 * @param property The profile property.
	 * @return {@code true} if the property map changed.
	 */
	default boolean removeProperty(String name, WSProfileProperty property) {
		Validate.notNull(name, "Name cannot be null!");
		Validate.notNull(property, "Property cannot be null!");
		return this.getPropertyMap().remove(name, property);
	}

	/**
	 * Checks if this profile is filled.
	 * <p>
	 * A filled profile contains both a unique id and name.
	 *
	 * @return whether this profile is filled.
	 */
	boolean isFilled();

	/**
	 * Returns the handled object of the {@link WSGameProfile}.
	 *
	 * @return the handled object.
	 */
	Object getHandled();

}

