package com.degoos.wetsponge.util;

import java.io.File;
import java.io.IOException;

public class FileUtils {

	public static void copyFolder(File source, File target) throws IOException {
		org.apache.commons.io.FileUtils.copyDirectory(source, target);
	}

	public static void copyFile (File source, File target) throws IOException {
		org.apache.commons.io.FileUtils.copyFile(source, target);
	}


}
