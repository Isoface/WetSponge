package com.degoos.wetsponge.util;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.command.wetspongecommand.WetSpongeSubcommandErrors;
import com.degoos.wetsponge.config.WetSpongeConfig;
import com.degoos.wetsponge.enums.EnumTextColor;
import com.degoos.wetsponge.text.WSText;

import java.io.*;
import java.util.*;

public class InternalLogger {

	private static Set<HasteStackTrace> stackTraces = new HashSet<>();
	private static HasteStackTrace lastStackTrace = null;

	private static final WSText wetSpongeBanner = WSText.builder("[WetSponge -> ").color(EnumTextColor.YELLOW).build();
	private static final WSText wetSpongeClose = WSText.builder("] ").color(EnumTextColor.YELLOW).build();

	public static Set<HasteStackTrace> getStackTraces() {
		return stackTraces;
	}

	public static HasteStackTrace getLastStackTrace() {
		return lastStackTrace;
	}

	public static void sendError(String string) {
		sendError(WSText.of(string));
	}

	public static void sendWarning(String string) {
		sendWarning(WSText.of(string));
	}

	public static void sendInfo(String string) {
		sendInfo(WSText.of(string));
	}

	public static void sendDone(String string) {
		sendDone(WSText.of(string));
	}

	public static void sendDebug(String string) {
		if (WetSpongeConfig.getConfig().getBoolean("debug", false))
			send(WSText.of(string), WSText.builder("Debug").color(EnumTextColor.GRAY).build(), EnumTextColor.GRAY);
	}

	public static void sendError(WSText text) {
		send(text, WSText.builder("Error").color(EnumTextColor.RED).build(), EnumTextColor.RED);
	}

	public static void sendWarning(WSText text) {
		send(text, WSText.builder("Warning").color(EnumTextColor.LIGHT_PURPLE).build(), EnumTextColor.LIGHT_PURPLE);
	}

	public static void sendInfo(WSText text) {
		send(text, WSText.builder("Info").color(EnumTextColor.AQUA).build(), EnumTextColor.AQUA);
	}

	public static void sendDone(WSText text) {
		send(text, WSText.builder("Done").color(EnumTextColor.GREEN).build(), EnumTextColor.GREEN);
	}

	public static void sendDebug(WSText text) {
		if (WetSpongeConfig.getConfig().getBoolean("debug", false))
			send(text, WSText.builder("Debug").color(EnumTextColor.GRAY).build(), EnumTextColor.GRAY);
	}


	public static void send(WSText text, WSText type, EnumTextColor color) {
		WetSponge.getServer().getConsole()
				.sendMessage(wetSpongeBanner.toBuilder()
						.append(type)
						.append(wetSpongeClose)
						.append(WSText.builder("")
								.color(color).append(text).build()).build());
	}

	public static void printException(Throwable ex, String info) {
		printException(ex, WSText.of(info));
	}

	public static void printException(Throwable ex, WSText info) {
		printException(ex, info, false);
	}

	private static void printException(Throwable ex, WSText info, boolean causedBy) {
		new Thread(() -> {
			try {
				if (!causedBy) {
					sendError("--------------------------------------------");
					sendError(info);
				}
				sendError("");
				sendError(WSText.of("Type: ", WSText.of(ex.getClass().getName(), EnumTextColor.YELLOW)));
				sendError("");
				sendError(WSText.of("Description: ", WSText.of(ex.getLocalizedMessage() == null ? "-" : ex.getLocalizedMessage(), EnumTextColor.YELLOW)));
				try {
					sendError(WSText.of("Cause: ", WSText.of((ex.getCause() == null || Objects.equals(ex.getCause().getLocalizedMessage(), "") ? "-" : ex.getCause()
							.getLocalizedMessage()), EnumTextColor.YELLOW)));
				} catch (Exception e) {
					sendError(WSText.of("Cause: -"));
				}
				sendError("");
				sendError("StackTrace:");
				for (StackTraceElement stackTraceElement : ex.getStackTrace())
					sendError(getColoredStackTrace(stackTraceElement));
				if (ex.getCause() == null) {
					sendError("");
					String[] pastedata = getHasteStackTraceURL(ex, info, ex.getStackTrace(), "WetSponge");
					sendError(WSText.builder(WSText.of("Error uploaded to: ", EnumTextColor.AQUA)).append(WSText.of(pastedata[0], EnumTextColor.YELLOW)).build());
					sendError(WSText.builder(WSText.of("Error saved in: ", EnumTextColor.AQUA)).append(WSText.of(pastedata[1], EnumTextColor.YELLOW)).build());
					sendError("");
					sendError("--------------------------------------------");
				} else {
					sendError("");
					sendError("Caused by:");
					printException(ex.getCause(), info, true);
				}
			} catch (Exception ex2) {
				ex.printStackTrace();
				ex2.printStackTrace();
			}
		}).start();
	}

	private static WSText getColoredStackTrace(StackTraceElement element) {
		WSText methodDataText;
		if (element.isNativeMethod()) {
			methodDataText = WSText.of(" (", EnumTextColor.GREEN, WSText.of("Native method", EnumTextColor.YELLOW, WSText.of(")", EnumTextColor.GREEN)));
		} else {
			WSText.Builder builder = WSText.builder(" (").color(EnumTextColor.GREEN);
			if (element.getFileName() != null && element.getLineNumber() >= 0) {
				builder.append(WSText.of(element.getFileName(), EnumTextColor.YELLOW));
				builder.append(WSText.of(":", EnumTextColor.GREEN));
				builder.append(WSText.of(String.valueOf(element.getLineNumber()), EnumTextColor.YELLOW));
				builder.append(WSText.of(")", EnumTextColor.GREEN));
			} else {
				builder.append(WSText.of(element.getFileName() != null ? element.getFileName() : "Unknown Source", EnumTextColor.YELLOW));
				builder.append(WSText.of(")", EnumTextColor.GREEN));
			}
			methodDataText = builder.build();
		}
		WSText classText = WSText.of(element.getMethodName(), EnumTextColor.LIGHT_PURPLE, methodDataText);
		return WSText.of("- " + element.getClassName() + ".", EnumTextColor.RED, classText);
	}


	public static String[] getHasteStackTraceURL(Throwable ex, WSText info, StackTraceElement[] elements, String id) {
		Optional<HasteStackTrace> optional = stackTraces.stream().filter(trace -> Arrays.equals(trace.lastStackTraces, elements)).findAny();
		if (optional.isPresent()) return new String[]{optional.get().url, optional.get().filepath};
		HasteStackTrace trace = new HasteStackTrace(ex, info, elements, id);
		return new String[]{trace.url, trace.filepath};
	}

	public static class HasteStackTrace {

		private StackTraceElement[] lastStackTraces;
		private String filepath;
		private String url;
		private File logFile;

		public HasteStackTrace(Throwable ex, WSText info, StackTraceElement[] lastStackTraces, String id) {
			this.lastStackTraces = lastStackTraces;
			//this.url = HastebinUtils.paste("WetSpongeError \n This is a test!");

			File folder = new File("WetSpongeErrors");
			if (!folder.exists()) folder.mkdirs();
			if (folder.isFile()) {
				folder.delete();
				folder.mkdirs();
			}
			Calendar calendar = Calendar.getInstance();

			String fileName = Calendar.DAY_OF_MONTH + "-" + calendar.get(Calendar.MONTH) + "-" + calendar.get(Calendar.YEAR) + " " + calendar.get(Calendar.HOUR) + "-" +
					calendar.get(Calendar.MINUTE) + "-" + calendar.get(Calendar.SECOND) + "." + calendar.get(Calendar.MILLISECOND);
			logFile = null;
			for (int i = 0; logFile == null; i++) {
				File target = new File(folder, fileName + (i == 0 ? "" : " (" + i + ")") + ".txt");
				if (!target.exists()) logFile = target;
			}

			try {
				logFile.createNewFile();
				BufferedWriter writer = new BufferedWriter(new FileWriter(logFile));
				writer.write(id + " error:");
				writer.newLine();
				writer.newLine();
				writeException(ex, info, false, writer);
				writer.close();

				filepath = logFile.getPath();
				stackTraces.add(this);
				lastStackTrace = this;
				url = HastebinUtils.paste(InternalLogger.readFiles());
				WetSponge.getServer().getOnlinePlayers().stream().filter(player -> player.hasPermission("wetsponge.admin"))
						.forEach(WetSpongeSubcommandErrors::sendErrors);

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		public StackTraceElement[] getLastStackTraces() {
			return lastStackTraces;
		}

		public String getFilepath() {
			return filepath;
		}

		public String getUrl() {
			return url;
		}

		public File getLogFile() {
			return logFile;
		}
	}

	private static void writeException(Throwable ex, WSText info, boolean causedBy, BufferedWriter writer) {
		try {
			if (!causedBy) {
				writer.write("--------------------------------------------");
				writer.newLine();
				writer.write(info.toPlain());
				writer.newLine();
			}
			writer.newLine();
			writer.write("Type: " + ex.getClass().getName());
			writer.newLine();
			writer.newLine();
			writer.write("Description: " + (ex.getLocalizedMessage() == null ? "-" : ex.getLocalizedMessage()));
			writer.newLine();
			try {
				writer.write("Cause: " + (ex.getCause() == null || Objects.equals(ex.getCause().getLocalizedMessage(), "") ? "-" : ex.getCause().getLocalizedMessage()));
				writer.newLine();
			} catch (Exception e) {
				writer.write("Cause: -");
				writer.newLine();
			}
			writer.newLine();
			writer.write("StackTrace:");
			writer.newLine();
			for (StackTraceElement stackTraceElement : ex.getStackTrace()) {
				writer.write(stackTraceElement.toString());
				writer.newLine();
			}
			if (ex.getCause() == null) {
				writer.newLine();
				writer.write("--------------------------------------------");
				writer.newLine();
			} else {
				writer.newLine();
				writer.write("Caused by:");
				writer.newLine();
				writeException(ex.getCause(), info, true, writer);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static String readFiles() {
		StringBuilder contentBuilder = new StringBuilder();

		contentBuilder.append("WetSponge version: ").append(WetSponge.getWetSpongeVersion()).append("\n");
		contentBuilder.append("Minecraft version: ").append(WetSponge.getVersion().getVersion()).append("\n");
		contentBuilder.append("Server item: ").append(WetSponge.getServerType()).append("\n").append("\n");

		contentBuilder.append("WetSponge plugins:").append("\n");
		WetSponge.getPluginManager().getPlugins()
				.forEach(plugin -> contentBuilder.append("- ").append(plugin.getId()).append(" ").append(plugin.getPluginDescription().getVersion()).append("\n"));
		contentBuilder.append("\n").append("Base plugins:").append("\n");
		WetSponge.getPluginManager().getBasePlugins()
				.forEach(plugin -> contentBuilder.append("- ").append(plugin.getName()).append(" ").append(plugin.getVersion()).append("\n"));

		int index = 1;

		try {
			for (HasteStackTrace hasteStackTrace : stackTraces) {
				contentBuilder.append("\n");
				contentBuilder.append("ERROR ").append(index).append(":").append("\n");
				BufferedReader br = new BufferedReader(new FileReader(hasteStackTrace.getLogFile()));
				String sCurrentLine;
				while ((sCurrentLine = br.readLine()) != null) {
					contentBuilder.append(sCurrentLine).append("\n");
				}
				index++;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return contentBuilder.toString();
	}
}
