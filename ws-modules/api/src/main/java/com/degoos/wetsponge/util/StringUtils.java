package com.degoos.wetsponge.util;

import com.degoos.wetsponge.enums.EnumTextColor;

public class StringUtils {

	public static String stripColors(String string) {
		if (string == null) return string;
		for (EnumTextColor textColor : EnumTextColor.values())
			string = string.replace("\u00A7" + textColor.getId(), "");
		return string;
	}

	public static String replace(String string, String regex, String replacement) {
		if (regex == null || regex.isEmpty()) return string;
		StringBuilder builder = new StringBuilder();

		StringBuilder regexMatcher = new StringBuilder();
		int regexIndex = 0;
		for (int i = 0; i < string.length(); i++) {
			char c = string.charAt(i);
			if (c == regex.charAt(regexIndex)) {
				regexMatcher.append(c);
				regexIndex++;
				if (regexIndex == regex.length()) {
					if (replacement != null) builder.append(replacement);
					regexIndex = 0;
					regexMatcher = new StringBuilder();
				}
				continue;
			}
			if (regexIndex != 0) {
				regexIndex = 0;
				builder.append(regexMatcher);
				regexMatcher = new StringBuilder();
			}
			builder.append(c);
		}
		return builder.toString();
	}

	public static String replaceFirst(String string, String regex, String replacement) {
		if (regex == null || regex.isEmpty()) return string;
		StringBuilder builder = new StringBuilder();

		StringBuilder regexMatcher = new StringBuilder();
		int regexIndex = 0;
		boolean replaced = false;
		for (int i = 0; i < string.length(); i++) {
			char c = string.charAt(i);
			if (!replaced && c == regex.charAt(regexIndex)) {
				regexMatcher.append(c);
				regexIndex++;
				if (regexIndex == regex.length()) {
					if (replacement != null) builder.append(replacement);
					regexIndex = 0;
					regexMatcher = new StringBuilder();
					replaced = true;
				}
				continue;
			}
			if (regexIndex != 0) {
				regexIndex = 0;
				builder.append(regexMatcher);
				regexMatcher = new StringBuilder();
			}
			builder.append(c);
		}
		return builder.toString();
	}

	public static String replaceChar(String string, Character regex, Character replacement) {
		if (regex == null) return string;
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < string.length(); i++) {
			char c = string.charAt(i);
			if (c == regex) {
				if (replacement != null) builder.append(replacement);
			} else builder.append(c);
		}
		return builder.toString();
	}

	public static String replaceFirstChar(String string, Character regex, Character replacement) {
		if (regex == null) return string;
		StringBuilder builder = new StringBuilder();
		boolean replaced = false;
		for (int i = 0; i < string.length(); i++) {
			char c = string.charAt(i);
			if (!replaced && c == regex) {
				if (replacement != null) builder.append(replacement);
				replaced = true;
			} else builder.append(c);
		}
		return builder.toString();
	}

}
