package com.degoos.wetsponge.util.reflection;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

public class FieldUtils {

    public static void setFinal(Field field, Object newValue, Object object) throws Exception {
        field.setAccessible(true);

        Field modifiersField = Field.class.getDeclaredField("modifiers");
        modifiersField.setAccessible(true);
        modifiersField.setInt(field, field.getModifiers() & ~Modifier.FINAL);

        field.set(object, newValue);
    }

}
