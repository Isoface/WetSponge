package com.degoos.wetsponge.world;

import com.degoos.wetsponge.block.WSBlock;
import com.degoos.wetsponge.block.tileentity.WSTileEntity;
import com.degoos.wetsponge.entity.WSEntity;
import java.util.Set;

/**
 * This class represents a Minecraft Chunk. A chunk is a 16x256x16 area in a {@link WSWorld world} which grids it.
 */
public interface WSChunk {

	/**
	 * Returns the {@link WSWorld world} of the {@link WSChunk chunk}.
	 *
	 * @return the {@link WSWorld world}.
	 */
	WSWorld getWorld();

	/**
	 * Returns the X coord of the {@link WSChunk chunk}.
	 *
	 * @return the X coord.
	 */
	int getX();

	/**
	 * Returns the Z coord of the {@link WSChunk chunk}.
	 *
	 * @return the Z coord.
	 */
	int getZ();

	/**
	 * Returns the {@link WSBlock block} in the selected corrds.
	 * <p>WARNING! X and Z range must be 0-16!</p>
	 *
	 * @param x the X coord.
	 * @param y the Y coord.
	 * @param z the Z coord.
	 *
	 * @return the {@link WSBlock block}.
	 */
	WSBlock getBlock(int x, int y, int z);

	/**
	 * Returns a {@link Set set} with all {@link WSEntity entities} in the {@link WSChunk chunk}.
	 * @return the {@link Set set}.
	 */
	Set<WSEntity> getEntities ();

	/**
	 * Returns a {@link Set set} with all {@link WSTileEntity tile entities} in the {@link WSChunk chunk}.
	 * @return the {@link Set set}.
	 */
	Set<WSTileEntity> getTileEntities ();

	/**
	 * Loads the {@link WSChunk chunk} if this is unloaded.
	 *
	 * @param generate whether the generation will be forced.
	 *
	 * @return whether the {@link WSChunk chunk} was loaded.
	 */
	boolean load(boolean generate);

	/**
	 * Adds the {@link WSChunk chunk} to the unload queue.
	 *
	 * @see WSWorld#flushUnloadedChunksQueue()
	 */
	void addToUnloadQueue();

	/**
	 * Returns whether the chunk is loaded.
	 *
	 * @return whether the chunk is loaded.
	 */
	boolean isLoaded();

	/**
	 * Returns whether the {@link WSChunk chunk} can be unloaded.
	 *
	 * @return whether the {@link WSChunk chunk} can be unloaded.
	 */
	boolean canBeUnloaded();

	/**
	 * Sets  whether the {@link WSChunk chunk} can be unloaded.
	 *
	 * @param canBeUnloaded whether the {@link WSChunk chunk} can be unloaded.
	 */
	void setCanBeUnloaded(boolean canBeUnloaded);

	/**
	 * Returns whether the {@link WSChunk chunk} can be saved.
	 *
	 * @return whether the {@link WSChunk chunk} can be saved.
	 */
	boolean canBeSaved();

	/**
	 * Sets whether the {@link WSChunk chunk} can be saved.
	 *
	 * @param canSave whether the {@link WSChunk chunk} can be saved.
	 */
	void setCanBeSaved(boolean canSave);

	/**
	 * Returns the handled object.
	 * @return the handled object.
	 */
	Object getHandled();

}
