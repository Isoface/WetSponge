package com.degoos.wetsponge.world;


import com.degoos.wetsponge.block.WSBlock;
import com.degoos.wetsponge.bridge.world.BridgeLocation;
import com.degoos.wetsponge.entity.WSEntity;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.enums.EnumBiomeType;
import com.flowpowered.math.vector.Vector2f;
import com.flowpowered.math.vector.Vector3d;
import com.flowpowered.math.vector.Vector3i;

import java.util.Collection;

/**
 * This class represents a position in a {@link WSWorld world}. You can create a {@link WSLocation location} using the "of" methods.
 */
public interface WSLocation extends Cloneable {

	/**
	 * Creates a new {@link WSLocation} from the world name, the position and the rotation.
	 *
	 * @param worldName the world name.
	 * @param x         the x coord.
	 * @param y         the y coord.
	 * @param z         the z coord.
	 * @param yaw       the yaw.
	 * @param pitch     the pitch.
	 * @return a new {@link WSLocation}.
	 */
	static WSLocation of(String worldName, double x, double y, double z, float yaw, float pitch) {
		return BridgeLocation.of(worldName, x, y, z, yaw, pitch);
	}

	/**
	 * Creates a new {@link WSLocation} from the world name and the position. Yaw and pitch will be 0.
	 *
	 * @param worldName the world name.
	 * @param x         the x coord.
	 * @param y         the y coord.
	 * @param z         the z coord.
	 * @return a new {@link WSLocation}.
	 */
	static WSLocation of(String worldName, double x, double y, double z) {
		return of(worldName, x, y, z, 0, 0);
	}

	/**
	 * Creates a new {@link WSLocation} from the world name and the position. Yaw and pitch will be 0.
	 *
	 * @param worldName the world name.
	 * @param position  the position.
	 * @return a new {@link WSLocation}
	 */
	static WSLocation of(String worldName, Vector3d position) {
		return of(worldName, position.getX(), position.getY(), position.getZ(), 0, 0);
	}

	/**
	 * Creates a new {@link WSLocation} from the world name and the position. Yaw and pitch will be 0.
	 *
	 * @param worldName the world name.
	 * @param position  the position.
	 * @return a new {@link WSLocation}
	 */
	static WSLocation of(String worldName, Vector3i position) {
		return of(worldName, position.getX(), position.getY(), position.getZ(), 0, 0);
	}

	/**
	 * Creates a new {@link WSLocation} from the world, the position and the rotation.
	 *
	 * @param world the world.
	 * @param x     the x coord.
	 * @param y     the y coord.
	 * @param z     the z coord.
	 * @param yaw   the yaw.
	 * @param pitch the pitch.
	 * @return a new {@link WSLocation}.
	 */
	static WSLocation of(WSWorld world, double x, double y, double z, float yaw, float pitch) {
		return of(world.getName(), x, y, z, yaw, pitch);
	}

	/**
	 * Creates a new {@link WSLocation} from the world and the position. Yaw and pitch will be 0.
	 *
	 * @param world the world.
	 * @param x     the x coord.
	 * @param y     the y coord.
	 * @param z     the z coord.
	 * @return a new {@link WSLocation}.
	 */
	static WSLocation of(WSWorld world, double x, double y, double z) {
		return of(world, x, y, z, 0, 0);
	}


	/**
	 * Creates a new {@link WSLocation} from the world and the position. Yaw and pitch will be 0.
	 *
	 * @param world    the world.
	 * @param position the position.
	 * @return a new {@link WSLocation}
	 */
	static WSLocation of(WSWorld world, Vector3d position) {
		return of(world, position.getX(), position.getY(), position.getZ(), 0, 0);
	}

	/**
	 * Creates a new {@link WSLocation} from the world and the position. Yaw and pitch will be 0.
	 *
	 * @param world    the world.
	 * @param position the position.
	 * @return a new {@link WSLocation}
	 */
	static WSLocation of(WSWorld world, Vector3i position) {
		return of(world, position.getX(), position.getY(), position.getZ(), 0, 0);
	}


	/**
	 * Creates a new {@link WSLocation} from a serialized {@link String}. You can get the serialized {@link String} of a {@link WSLocation} using the method {@link
	 * WSLocation#toString()}.
	 *
	 * @param string the serialized location.
	 * @return a new {@link WSLocation}.
	 */
	static WSLocation of(String string) {
		return BridgeLocation.of(string);
	}

	/**
	 * Serializes the {@link WSLocation location}.
	 *
	 * @return the serialized {@link WSLocation location}.
	 */
	String toString();

	/**
	 * Returns the world's name.
	 *
	 * @return the world's name.
	 */
	String getWorldName();

	/**
	 * Sets the world's name of this {@link WSLocation location}. WetSponge will find the {@link WSWorld world} instance and set it to the {@link WSLocation location} of
	 * prsent.
	 *
	 * @param worldName the world's name.
	 * @return the {@link WSLocation location}.
	 */
	WSLocation setWorldName(String worldName);

	/**
	 * Returns the {@link WSWorld} of the {@link WSLocation location}.
	 *
	 * @return the {@link WSWorld}.
	 */
	WSWorld getWorld();

	/**
	 * Sets the {@link WSWorld} of the {@link WSLocation location}.
	 *
	 * @param world the {@link WSWorld}.
	 * @return the {@link WSLocation}.
	 */
	WSLocation setWorld(WSWorld world);

	/**
	 * Returns the block in the {@link WSLocation location}.
	 *
	 * @return the block.
	 */
	WSBlock getBlock();

	/**
	 * Refresh the instance of the {@link WSWorld world} of the {@link WSLocation location}.
	 *
	 * @return the {@link WSLocation location}.
	 */
	WSLocation updateWorld();

	/**
	 * Returns the X coord of the {@link WSLocation location}.
	 *
	 * @return the X coord.
	 */
	double getX();

	/**
	 * Sets the X coord of the {@link WSLocation location}.
	 *
	 * @param x the X coord.
	 * @return the {@link WSLocation location}.
	 */
	WSLocation setX(double x);

	/**
	 * Returns the Y coord of the {@link WSLocation location}.
	 *
	 * @return the Y coord.
	 */
	double getY();

	/**
	 * Sets the Y coord of the {@link WSLocation location}.
	 *
	 * @param y the Y coord.
	 * @return the {@link WSLocation location}.
	 */
	WSLocation setY(double y);

	/**
	 * Returns the Z coord of the {@link WSLocation location}.
	 *
	 * @return the Z  coord.
	 */
	double getZ();

	/**
	 * Sets the Z coord of the {@link WSLocation location}.
	 *
	 * @param z the Z coord.
	 * @return the {@link WSLocation location}.
	 */
	WSLocation setZ(double z);

	/**
	 * Returns the yaw of the {@link WSLocation location}.
	 *
	 * @return the {@link WSLocation location}
	 */
	float getYaw();

	/**
	 * Sets the yaw of the {@link WSLocation location}.
	 *
	 * @param yaw the yaw.
	 * @return the {@link WSLocation location}.
	 */
	WSLocation setYaw(float yaw);

	/**
	 * Returns the pitch of the {@link WSLocation location}.
	 *
	 * @return the {@link WSLocation location}
	 */
	float getPitch();

	/**
	 * Sets the pitch of the {@link WSLocation location}.
	 *
	 * @param pitch the pitch.
	 * @return the {@link WSLocation location}.
	 */
	WSLocation setPitch(float pitch);

	/**
	 * Returns the rotation of the {@link WSLocation location}. The rotation has this format: pitch - yaw
	 *
	 * @return the rotation.
	 */
	Vector2f getRotation();

	/**
	 * Sets the rotation of the {@link WSLocation location}. The rotation has this format: pitch - yaw
	 *
	 * @param rotation the rotation.
	 * @return the {@link WSLocation}
	 */
	WSLocation setRotation(Vector2f rotation);

	/**
	 * Return the block X coord of the {@link WSLocation location}.
	 *
	 * @return the block X coord.
	 */
	int getBlockX();

	/**
	 * Return the block Y coord of the {@link WSLocation location}.
	 *
	 * @return the block Y coord.
	 */
	int getBlockY();

	/**
	 * Return the block Z coord of the {@link WSLocation location}.
	 *
	 * @return the block Z coord.
	 */
	int getBlockZ();

	/**
	 * Calculates the distance between two locations.
	 *
	 * @param location the second {@link WSLocation location}.
	 * @return the distance in meters.
	 */
	double distance(WSLocation location);

	/**
	 * Calculates the distance between two locations.
	 *
	 * @param x the X coord of the second location.
	 * @param y the Y coord of the second location.
	 * @param z the Z coord of the second location.
	 * @return the distance in meters.
	 */
	double distance(double x, double y, double z);

	/**
	 * Calculates the distance between two locations.
	 *
	 * @param location the second {@link Vector3d location}.
	 * @return the distance in meters.
	 */
	double distance(Vector3d location);

	/**
	 * Adds the numbers to the {@link WSLocation location}.
	 *
	 * @param x the number to add to the X coord.
	 * @param y the number to add to the Y coord.
	 * @param z the number to add to the Z coord.
	 * @return the {@link WSLocation location}.
	 */
	WSLocation add(double x, double y, double z);

	/**
	 * Adds the vector to the {@link WSLocation location}.
	 *
	 * @param vector3d the {@link Vector3d vector}.
	 * @return the {@link WSLocation location}.
	 */
	WSLocation add(Vector3d vector3d);

	/**
	 * Creates a new copy of the {@link WSLocation location}.
	 *
	 * @return the new copy.
	 */
	WSLocation clone();

	/**
	 * Returns a new instance of a {@link WSLocation location} with the block coords of the {@link WSLocation location}. Yaw and pitch will be 0.
	 *
	 * @return the new instance.
	 */
	WSLocation getBlockLocation();

	/**
	 * Returns the {@link EnumBiomeType biome} of the {@link WSLocation location}.
	 *
	 * @return the {@link EnumBiomeType biome}.
	 */
	EnumBiomeType getBiome();

	/**
	 * Sets the {@link EnumBiomeType biome} of the {@link WSLocation location}.
	 *
	 * @param biome the {@link EnumBiomeType biome}.
	 */
	void setBiome(EnumBiomeType biome);


	/**
	 * Returns a collection with all {@link WSEntity entities} in the area.
	 *
	 * @param area the area.
	 * @return the collection.
	 */
	Collection<WSEntity> getNearbyEntities(Vector3d area);

	/**
	 * Returns a collection with all {@link WSEntity entities} in a raius.
	 *
	 * @param radius the radius.
	 * @return the collection.
	 */
	Collection<WSEntity> getNearbyEntities(double radius);

	/**
	 * Returns a collection with all {@link WSEntity entities} of a item in the area.
	 *
	 * @param area the area.
	 * @param type the class of the {@link WSEntity entity}.
	 * @param <T>  Entity object
	 * @return the collection.
	 */
	<T extends WSEntity> Collection<T> getNearbyEntities(Vector3d area, Class<T> type);

	/**
	 * Returns a collection with all {@link WSEntity entities} of a item in a radius.
	 *
	 * @param radius the radius.
	 * @param type   the class of the {@link WSEntity entity}.
	 * @param <T>    Entity object
	 * @return the collection.
	 */
	<T extends WSEntity> Collection<T> getNearbyEntities(double radius, Class<T> type);


	/**
	 * Returns a collection with all {@link WSEntity player}s in the area.
	 *
	 * @param area the area.
	 * @return the collection.
	 */
	Collection<WSPlayer> getNearbyPlayers(Vector3d area);

	/**
	 * Returns a collection with all {@link WSEntity player}s in a radius.
	 *
	 * @param radius the radius.
	 * @return the collection.
	 */
	Collection<WSPlayer> getNearbyPlayers(double radius);

	/**
	 * Get the highest found block Y coord in the X and Z coord of the {@link WSLocation location}.
	 *
	 * @return the highest found block Y coord.
	 */
	int getHighestY();

	/**
	 * Updates the lines of the {@link com.degoos.wetsponge.block.tileentity.WSTileEntitySign sign} set in the {@link WSLocation location}. If the set {@link WSBlock
	 * block} in the {@link WSLocation location} is not a {@link com.degoos.wetsponge.block.tileentity.WSTileEntitySign sign}, this method will do nothing.
	 *
	 * @param lines the lines to set.
	 * @return the {@link WSLocation location}.
	 */
	WSLocation updateSign(String[] lines);

	/**
	 * Returns whether the block location of the {@link WSLocation location} equals to the block location of another {@link WSLocation location}.
	 *
	 * @param o the other {@link WSLocation location}.
	 * @return whether  the block location of the {@link WSLocation location} equals to the block location of another {@link WSLocation location}.
	 */
	boolean equalsBlock(Object o);

	/**
	 * Returns the facing direction of the {@link WSLocation location}. This method is equals to the "getDirection" method in {@link
	 * com.degoos.wetsponge.enums.EnumServerType#SPIGOT Spigot}.
	 *
	 * @return the facing direction.
	 */
	Vector3d getFacingDirection();

	/**
	 * Creates a new instance of a {@link Vector3d} with the position of the {@link WSLocation location}.
	 *
	 * @return the new {@link Vector3d}.
	 */
	Vector3d toVector3d();

	/**
	 * Creates a new instance of a {@link Vector3i} with the block position of the {@link WSLocation location}.
	 *
	 * @return the new {@link Vector3i}.
	 */
	Vector3i toVector3i();

	/**
	 * Returns the handled world.
	 *
	 * @return the handled world
	 */
	Object getHandledWorld();
}
