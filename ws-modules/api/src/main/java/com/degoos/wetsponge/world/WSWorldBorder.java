package com.degoos.wetsponge.world;

import com.flowpowered.math.vector.Vector2d;

/**
 * Represents the world border of a {@link WSWorld world}. It can be centered and resized.
 * More info: https://minecraft.gamepedia.com/World_border
 */
public interface WSWorldBorder {

	/**
	 * Copies the properties of another {@link WSWorldBorder world border} to this instance.
	 *
	 * @param worldBorder the copied {@link WSWorldBorder world border}.
	 */
	void copyPropertiesFrom(WSWorldBorder worldBorder);

	/**
	 * Returns the {@link Vector2d center} of the {@link WSWorldBorder world border}.
	 *
	 * @return the {@link Vector2d center} of the {@link WSWorldBorder world border}.
	 */
	Vector2d getCenter();

	/**
	 * Sets the {@link Vector2d center} of the {@link WSWorldBorder world border}.
	 * The coordinates of the vector are (x, z). Y coord is not used.
	 *
	 * @param center the {@link Vector2d center}.
	 */
	void setCenter(Vector2d center);

	/**
	 * Sets the center of the {@link WSWorldBorder world border}.
	 * Y coord is not used.
	 *
	 * @param x the X coord.
	 * @param z the Z coord.
	 *
	 * @see #setCenter(Vector2d)
	 */
	void setCenter(double x, double z);

	/**
	 * Returns the damage amount per second {@link com.degoos.wetsponge.entity.WSEntity entities} will take if they're outside the {@link WSWorldBorder world border}.
	 *
	 * @return the damage amount per second.
	 */
	double getDamageAmount();

	/**
	 * Sets the damage amount per second {@link com.degoos.wetsponge.entity.WSEntity entities} will take if they're outside the {@link WSWorldBorder world border}.
	 *
	 * @param damageAmount the damage amount per second.
	 */
	void setDamageAmount(double damageAmount);

	/**
	 * Returns the amount of blocks {@link com.degoos.wetsponge.entity.WSEntity entities} can be outside the {@link WSWorldBorder world border} without taking damage.
	 *
	 * @return the amount of blocks {@link com.degoos.wetsponge.entity.WSEntity entities} can be outside the {@link WSWorldBorder world border} without taking damage.
	 */
	double getDamageThreshold();

	/**
	 * Sets the amount of blocks {@link com.degoos.wetsponge.entity.WSEntity entities} can be outside the {@link WSWorldBorder world border} without taking damage.
	 *
	 * @param damageThreshold the amount of blocks {@link com.degoos.wetsponge.entity.WSEntity entities} can be outside the {@link WSWorldBorder world border} without
	 * taking damage.
	 */
	void setDamageThreshold(double damageThreshold);

	/**
	 * Returns the actual diameter of the {@link WSWorldBorder world border}.
	 *
	 * @return the actual diameter of the {@link WSWorldBorder world border}.
	 */
	double getDiameter();

	/**
	 * Sets the diameter of the {@link WSWorldBorder world border}.
	 *
	 * @param diameter the diameter.
	 */
	void setDiameter(double diameter);

	/**
	 * Sets the diameter of the {@link WSWorldBorder world border} and the time it will require to reach that diameter.
	 * The {@link WSWorldBorder will} shrink or expand in the given timestamp.
	 *
	 * @param diameter the diameter.
	 * @param time the timestamp.
	 */
	void setDiameter(double diameter, long time);

	/**
	 * Sets the actual diameter of the {@link WSWorldBorder world border}. Then, the {@link WSWorldBorder world border} wil shrink or expand to the end diameter in the
	 * given timestamp.
	 *
	 * @param startDiameter the start diameter.
	 * @param endDiameter the end diameter.
	 * @param time the timestamp.
	 */
	void setDiameter(double startDiameter, double endDiameter, long time);

	/**
	 * If the {@link WSWorldBorder world border} is shrinking or expanding this method will return the diameter the {@link WSWorldBorder world border} will reach.
	 * Else the given diameter will be the same as the one given by the method {@link #getDiameter()}.
	 *
	 * @return the explained diameter.
	 */
	double getNewDiameter();

	/**
	 * Returns the warning distance of the {@link WSWorldBorder world border}. If a {@link com.degoos.wetsponge.entity.living.player.WSPlayer player}'s distance is
	 * shorter their screen will be tinted red.
	 *
	 * @return the warning distance.
	 */
	int getWarningDistance();

	/**
	 * Sets the warning distance of the {@link WSWorldBorder world border}. If a {@link com.degoos.wetsponge.entity.living.player.WSPlayer player}'s distance is shorter
	 * their screen will be tinted red.
	 *
	 * @param warningDistance the warning distance.
	 */
	void setWarningDistance(int warningDistance);

	/**
	 * Causes the screen to be tinted red when a contracting {@link WSWorldBorder world border} will reach the
	 * {@link com.degoos.wetsponge.entity.living.player.WSPlayer player} within the returned time.
	 *
	 * @return the time.
	 */
	int getWarningTime();

	/**
	 * Causes the screen to be tinted red when a contracting {@link WSWorldBorder world border} will reach the
	 * * {@link com.degoos.wetsponge.entity.living.player.WSPlayer player} within the set time.
	 *
	 * @param waningTime the time.
	 */
	void setWaningTime(int waningTime);

	/**
	 * Returns the handled instance of the {@link WSWorldBorder world border}.
	 *
	 * @return the handled instance.
	 */
	Object getHandled();
}

