package com.degoos.wetsponge.world;

import com.degoos.wetsponge.enums.EnumDifficulty;
import com.degoos.wetsponge.enums.EnumGameMode;
import com.flowpowered.math.vector.Vector3d;
import com.flowpowered.math.vector.Vector3i;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

/**
 * The WSWorldProperties storages information about the {@link WSWorld world}.
 */
public interface WSWorldProperties {

	boolean isInitialized();

	String getWorldName();

	UUID getUniqueId();

	boolean isEnabled();

	void setEnabled(boolean enabled);

	boolean loadOnStartup();

	void setLoadOnStartup(boolean loadOnStartup);

	boolean doesKeepSpawnLoaded();

	void setKeepSpawnLoaded(boolean keepSpawnLoaded);

	boolean doesGenerateSpawnOnLoad();

	void setGenerateSpawnOnLoad(boolean generateSpawnOnLoad);

	Vector3i getSpawnPosition();

	void setSpawnPosition(Vector3i spawnPosition);

	long getSeed();

	void setSeed(long seed);

	long getTotalTime();

	long getWorldTime();

	void setWorldTime(long worldTime);

	boolean isPVPEnabled();

	void setPVPEnabled(boolean pvpEnabled);

	boolean isRaining();

	void setRaining(boolean raining);

	int getRainTime();

	void setRainTime(int rainTime);

	boolean isThundering();

	void setThundering(boolean thundering);

	int getThunderTime();

	void setThunderTime(int thunderTime);

	EnumGameMode getGameMode();

	void setGameMode(EnumGameMode gameMode);

	boolean usesMapFeatures();

	void setMapFeaturesEnabled(boolean mapFeaturesEnabled);

	boolean isHardcore();

	void setHardcore(boolean hardcore);

	boolean areCommandsAllowed();

	void setCommandsAllowed(boolean commandsAllowed);

	EnumDifficulty getDifficulty();

	void setDifficulty(EnumDifficulty difficulty);

	boolean doesGenerateBonusChest();

	Optional<String> getGameRule(String name);

	Map<String, String> getGameRules();

	void setGameRule(String name, String value);

	boolean removeGameRule(String name);

	int getMaxHeight();

	default void apply(WSWorldProperties properties) {
		setEnabled(properties.isEnabled());
		setLoadOnStartup(properties.loadOnStartup());
		setKeepSpawnLoaded(properties.doesKeepSpawnLoaded());
		setGenerateSpawnOnLoad(properties.doesGenerateSpawnOnLoad());
		setSpawnPosition(properties.getSpawnPosition());
		setSeed(properties.getSeed());
		setWorldTime(properties.getWorldTime());
		setPVPEnabled(properties.isPVPEnabled());
		setRaining(properties.isRaining());
		setRainTime(properties.getRainTime());
		setThundering(properties.isThundering());
		setThunderTime(properties.getThunderTime());
		setGameMode(properties.getGameMode());
		setMapFeaturesEnabled(properties.usesMapFeatures());
		setHardcore(properties.isHardcore());
		setCommandsAllowed(properties.areCommandsAllowed());
		setDifficulty(properties.getDifficulty());
		getGameRules().values().forEach(this::removeGameRule);
		properties.getGameRules().forEach(this::setGameRule);
	}

    /*DataContainer getAdditionalProperties();

    Optional<DataView> getPropertySection(DataQuery var1);

    void setPropertySection(DataQuery var1, DataView var2);

    Collection<WorldGeneratorModifier> getGeneratorModifiers();

    void setGeneratorModifiers(Collection<WorldGeneratorModifier> var1);

    DataContainer getGeneratorSettings();*/

}
