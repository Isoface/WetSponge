package com.degoos.wetsponge.world.edit;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.zip.Deflater;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

/**
 * WetSponge on 30/09/2017 by IhToN.
 */
public abstract class AbstractRegionFormat implements WSRegionFormat {

	private String      name;
	private int         compressionLevel;
	private Set<String> aliases;

	public AbstractRegionFormat(String name, int compressionLevel, String... aliases) {
		this.name = name;
		this.aliases = new HashSet(Arrays.asList(aliases));
		if (compressionLevel > 9) this.compressionLevel = Deflater.BEST_COMPRESSION;
		if (compressionLevel < 0) this.compressionLevel = Deflater.NO_COMPRESSION;
		else this.compressionLevel = compressionLevel;
	}

	public AbstractRegionFormat(String name, String... aliases) {
		this(name, Deflater.BEST_COMPRESSION, aliases);
	}

	@Override
	public InputStream getReader(InputStream inputStream) throws IOException {
		return new BufferedInputStream(new GZIPInputStream(inputStream));
	}

	@Override
	public OutputStream getWriter(OutputStream outputStream) throws IOException {
		return new BufferedOutputStream(new GZIPOutputStream(outputStream, compressionLevel));
	}

	@Override
	public boolean isFormat(File file) {
		return file.getName().endsWith("." + name) || aliases.stream().anyMatch(ext -> file.getName().endsWith("." + ext));
	}

	@Override
	public String getName() {
		return name;
	}

	public Set<String> getAliases() {
		return aliases;
	}
}
