package com.degoos.wetsponge.world.edit;

import com.degoos.wetsponge.bridge.world.generation.BridgeWorldQueue;
import com.degoos.wetsponge.material.block.WSBlockType;
import com.degoos.wetsponge.world.WSWorld;
import com.flowpowered.math.vector.Vector3i;

/**
 * This class represents a queue of actions to modify the world.
 */
public interface WSWorldQueue {

	/**
	 * Creates a new {@link WSWorldQueue world queue}.
	 *
	 * @param world the {@link WSWorld world}.
	 * @return the new {@link WSWorldQueue world queue}.
	 */
	public static WSWorldQueue of(WSWorld world) {
		return BridgeWorldQueue.of(world);
	}


	/**
	 * Sets a set block action to the queue.
	 *
	 * @param position  the {@link Vector3i position} of the block to change.
	 * @param blockType the new {@link WSBlockType block item}.
	 */
	void setBlock(Vector3i position, WSBlockType blockType);

	/**
	 * Flush the queue.
	 *
	 * @param sendPacket whether WetSponge will send the changes to all clients in the {@link WSWorld world}.
	 * @param async      whether the queue will be async.
	 */
	void flush(boolean sendPacket, boolean async);

}
