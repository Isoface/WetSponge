package com.degoos.wetsponge.world.generation;

import com.degoos.wetsponge.material.block.WSBlockType;
import com.flowpowered.math.vector.Vector3i;

public interface WSBlockVolume {

	Vector3i getBlockMin();

	Vector3i getBlockMax();

	Vector3i getBlockSize();

	default boolean containsBlock(Vector3i position) {
		return containsBlock(position.getX(), position.getY(), position.getZ());
	}

	boolean containsBlock(int x, int y, int z);

	default WSBlockType getBlock(Vector3i position) {
		return getBlock(position.getX(), position.getY(), position.getZ());
	}

	WSBlockType getBlock(int x, int y, int z);

	default boolean setBlock(Vector3i position, WSBlockType blockType) {
		return this.setBlock(position.getX(), position.getY(), position.getZ(), blockType);
	}

	boolean setBlock(int x, int y, int z, WSBlockType blockType);

	Object getHandled();

}
