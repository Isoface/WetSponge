package com.degoos.wetsponge.bridge.bar;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.bar.*;
import com.degoos.wetsponge.enums.EnumServerVersion;

public class BridgeBossBar {

	public static WSBossBar.Builder builder() {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				if (WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD)) {
					if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
						return new SpigotBossBar.Builder();
					else return new Spigot13BossBar.Builder();
				} else return new OldSpigotBossBar.Builder();
			case SPONGE:
				return new SpongeBossBar.Builder();
			default:
				return null;
		}
	}
}
