package com.degoos.wetsponge.bridge.effect.potion;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.effect.potion.Spigot13PotionEffect;
import com.degoos.wetsponge.effect.potion.SpigotPotionEffect;
import com.degoos.wetsponge.effect.potion.SpongePotionEffect;
import com.degoos.wetsponge.effect.potion.WSPotionEffect;
import com.degoos.wetsponge.enums.EnumServerVersion;

public class BridgePotionEffect {

	public static WSPotionEffect.Builder builder() {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
					return new SpigotPotionEffect.Builder();
				else
					return new Spigot13PotionEffect.Builder();
			case SPONGE:
				return new SpongePotionEffect.Builder();
			default:
				return null;
		}
	}

}
