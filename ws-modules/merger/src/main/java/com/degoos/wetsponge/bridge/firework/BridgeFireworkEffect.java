package com.degoos.wetsponge.bridge.firework;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.firework.Spigot13FireworkEffect;
import com.degoos.wetsponge.firework.SpigotFireworkEffect;
import com.degoos.wetsponge.firework.SpongeFireworkEffect;
import com.degoos.wetsponge.firework.WSFireworkEffect;

public class BridgeFireworkEffect {

	public static WSFireworkEffect.Builder builder() {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
					return SpigotFireworkEffect.builder();
				else
					return Spigot13FireworkEffect.builder();
			case SPONGE:
				return SpongeFireworkEffect.builder();
			default:
				return null;
		}
	}


}
