package com.degoos.wetsponge.bridge.inventory;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.inventory.Spigot13Inventory;
import com.degoos.wetsponge.inventory.SpigotInventory;
import com.degoos.wetsponge.inventory.SpongeInventory;
import com.degoos.wetsponge.inventory.WSInventory;
import com.degoos.wetsponge.text.WSText;

public class BridgeInventory {

	public static WSInventory of(int size) {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
					return SpigotInventory.of(size);
				else
					return Spigot13Inventory.of(size);
			case SPONGE:
				return SpongeInventory.of(size, false);
			default:
				return null;
		}
	}

	public static WSInventory of(int size, WSText title) {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
					return SpigotInventory.of(size, title);
				else
					return Spigot13Inventory.of(size, title);
			case SPONGE:
				return SpongeInventory.of(size, title, false);
			default:
				return null;
		}
	}

}
