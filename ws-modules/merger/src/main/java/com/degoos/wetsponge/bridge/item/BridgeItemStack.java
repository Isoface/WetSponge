package com.degoos.wetsponge.bridge.item;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.item.Spigot13ItemStack;
import com.degoos.wetsponge.item.SpigotItemStack;
import com.degoos.wetsponge.item.SpongeItemStack;
import com.degoos.wetsponge.item.WSItemStack;
import com.degoos.wetsponge.material.WSMaterial;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;

public class BridgeItemStack {

	public static WSItemStack of(WSMaterial material) {
		switch (WetSponge.getServerType()) {
			case SPONGE:
				return SpongeItemStack.of(material);
			case SPIGOT:
			case PAPER_SPIGOT:
				if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
					return SpigotItemStack.of(material);
				else return Spigot13ItemStack.of(material);
			default:
				return null;
		}
	}

	public static WSItemStack ofSerializedNBTTag(String nbt) throws Exception {
		switch (WetSponge.getServerType()) {
			case SPONGE:
				return new SpongeItemStack(nbt);
			case SPIGOT:
			case PAPER_SPIGOT:
				if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
					return new SpigotItemStack(nbt);
				else return new Spigot13ItemStack(nbt);
			default:
				return null;
		}
	}

	public static WSItemStack ofNBTTagCompound(WSNBTTagCompound nbtTagCompound) throws Exception {
		switch (WetSponge.getServerType()) {
			case SPONGE:
				return new SpongeItemStack(nbtTagCompound);
			case SPIGOT:
			case PAPER_SPIGOT:
				if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
					return new SpigotItemStack(nbtTagCompound);
				else return new Spigot13ItemStack(nbtTagCompound);
			default:
				return null;
		}
	}

	public static WSItemStack createSkull(String format) {
		switch (WetSponge.getServerType()) {
			case SPONGE:
				return SpongeItemStack.fromFormat(format);
			case SPIGOT:
			case PAPER_SPIGOT:
				if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
					return SpigotItemStack.fromFormat(format);
				else return Spigot13ItemStack.fromFormat(format);
			default:
				return null;
		}
	}

}
