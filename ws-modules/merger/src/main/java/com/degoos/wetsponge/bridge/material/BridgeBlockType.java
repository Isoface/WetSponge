package com.degoos.wetsponge.bridge.material;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.material.Spigot13BlockConverter;
import com.degoos.wetsponge.material.SpigotBlockConverter;
import com.degoos.wetsponge.material.SpongeBlockConverter;
import com.degoos.wetsponge.material.block.WSBlockType;

public class BridgeBlockType {

	public static WSBlockType getDefaultState(int numericalId, String oldStringId, String newStringId, int maxStackSize, Class<? extends WSBlockType> materialClass, Object[] extra) {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
					return SpigotBlockConverter.createWSBlockType(numericalId, oldStringId, newStringId, maxStackSize, materialClass, extra);
				else return Spigot13BlockConverter.createWSBlockType(numericalId, oldStringId, newStringId, maxStackSize, materialClass, extra);
			case SPONGE:
				return SpongeBlockConverter.createWSBlockType(numericalId, oldStringId, newStringId, maxStackSize, materialClass, extra);
			default:
				return null;
		}
	}

}