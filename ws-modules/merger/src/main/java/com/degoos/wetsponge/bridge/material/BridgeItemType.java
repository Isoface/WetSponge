package com.degoos.wetsponge.bridge.material;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.material.Spigot13ItemConverter;
import com.degoos.wetsponge.material.SpigotItemConverter;
import com.degoos.wetsponge.material.SpongeItemConverter;
import com.degoos.wetsponge.material.item.WSItemType;

public class BridgeItemType {

	public static WSItemType getDefaultState(int numericalId, String oldStringId, String newStringId, int maxStackSize, Class<? extends WSItemType> materialClass, Object[] extra) {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
					return SpigotItemConverter.createWSItemType(numericalId, oldStringId, newStringId, maxStackSize, materialClass, extra);
				else return Spigot13ItemConverter.createWSItemType(numericalId, oldStringId, newStringId, maxStackSize, materialClass, extra);
			case SPONGE:
				return SpongeItemConverter.createWSItemType(numericalId, oldStringId, newStringId, maxStackSize, materialClass, extra);
			default:
				return null;
		}
	}

}
