package com.degoos.wetsponge.bridge.material.item;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.resource.spigot.Spigot13SkullBuilder;
import com.degoos.wetsponge.resource.spigot.SpigotSkullBuilder;
import com.degoos.wetsponge.resource.sponge.SpongeSkullBuilder;
import com.degoos.wetsponge.user.WSGameProfile;

import java.net.URL;

public class BridgeItemTypeSkull {

	public static void setTexture(String texture, WSGameProfile profile) {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
					SpigotSkullBuilder.setTexture(texture, profile);
				else
					Spigot13SkullBuilder.setTexture(texture, profile);
				break;
			case SPONGE:
				SpongeSkullBuilder.setTexture(texture, profile);
			default:
				break;
		}
	}

	public static void setTexture(URL texture, WSGameProfile profile) {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
					SpigotSkullBuilder.setTexture(texture, profile);
				else
					Spigot13SkullBuilder.setTexture(texture, profile);
				break;
			case SPONGE:
				SpongeSkullBuilder.setTexture(texture, profile);
			default:
				break;
		}
	}

	public static WSGameProfile setTextureByPlayerName(String name) {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
					return SpigotSkullBuilder.getWSGameProfileByName(name);
				else
					return Spigot13SkullBuilder.getWSGameProfileByName(name);
			case SPONGE:
				return SpongeSkullBuilder.getWSGameProfileByName(name);
			default:
				return null;
		}
	}

}
