package com.degoos.wetsponge.bridge.merchant;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.merchant.*;

public class BridgeTrade {

	public static WSTrade.Builder builder() {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				if (!WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
					return new Spigot13Trade.Builder();
				return WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD) ? new SpigotTrade.Builder() : new OldSpigotTrade.Builder();
			case SPONGE:
				return new SpongeTrade.Builder();
			default:
				return null;
		}
	}

}
