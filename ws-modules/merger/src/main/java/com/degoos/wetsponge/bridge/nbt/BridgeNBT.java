package com.degoos.wetsponge.bridge.nbt;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.exception.nbt.NBTParseException;
import com.degoos.wetsponge.nbt.*;
import com.degoos.wetsponge.util.InternalLogger;

public class BridgeNBT {

	public static WSNBTTagByte ofByte(byte b) {
		switch (WetSponge.getServerType()) {
			case SPONGE:
				return new SpongeNBTTagByte(b);
			case SPIGOT:
			case PAPER_SPIGOT:
				try {
					if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
						return new SpigotNBTTagByte(b);
					else return new Spigot13NBTTagByte(b);
				} catch (Exception e) {
					InternalLogger.printException(e, "An error has occurred while WetSponge was creating a new NBTTag!");
					return null;
				}
			default:
				return null;
		}
	}

	public static WSNBTTagByteArray ofByteArray(byte[] bytes) {
		switch (WetSponge.getServerType()) {
			case SPONGE:
				return new SpongeNBTTagByteArray(bytes);
			case SPIGOT:
			case PAPER_SPIGOT:
				try {
					if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
						return new SpigotNBTTagByteArray(bytes);
					return new Spigot13NBTTagByteArray(bytes);
				} catch (Exception e) {
					InternalLogger.printException(e, "An error has occurred while WetSponge was creating a new NBTTag!");
					return null;
				}
			default:
				return null;
		}
	}

	public static WSNBTTagCompound newCompound() {
		switch (WetSponge.getServerType()) {
			case SPONGE:
				return new SpongeNBTTagCompound();
			case SPIGOT:
			case PAPER_SPIGOT:
				try {
					if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
						return new SpigotNBTTagCompound();
					return new Spigot13NBTTagCompound();
				} catch (Exception e) {
					InternalLogger.printException(e, "An error has occurred while WetSponge was creating a new NBTTag!");
					return null;
				}
			default:
				return null;
		}
	}

	public static WSNBTTagCompound ofCompound(String string) throws NBTParseException {
		try {
			switch (WetSponge.getServerType()) {
				case SPONGE:
					return new SpongeNBTTagCompound(string);
				case SPIGOT:
				case PAPER_SPIGOT:
					if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
						return new SpigotNBTTagCompound(string);
					return new Spigot13NBTTagCompound(string);

				default:
					return null;
			}

		} catch (Exception e) {
			throw new NBTParseException("An error has occurred while WetSponge was parsing a NBTTagCompound!", e);
		}
	}

	public static WSNBTTagDouble ofDouble(double d) {
		switch (WetSponge.getServerType()) {
			case SPONGE:
				return new SpongeNBTTagDouble(d);
			case SPIGOT:
			case PAPER_SPIGOT:
				try {
					if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
						return new SpigotNBTTagDouble(d);
					return new Spigot13NBTTagDouble(d);
				} catch (Exception e) {
					InternalLogger.printException(e, "An error has occurred while WetSponge was creating a new NBTTag!");
					return null;
				}
			default:
				return null;
		}
	}

	public static WSNBTTagEnd newEnd() {
		switch (WetSponge.getServerType()) {
			case SPONGE:
				return new SpongeNBTTagEnd();
			case SPIGOT:
			case PAPER_SPIGOT:
				if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
					return new SpigotNBTTagEnd();
				return new Spigot13NBTTagEnd();
			default:
				return null;
		}
	}

	public static WSNBTTagFloat ofFloat(float f) {
		switch (WetSponge.getServerType()) {
			case SPONGE:
				return new SpongeNBTTagFloat(f);
			case SPIGOT:
			case PAPER_SPIGOT:
				try {
					if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
						return new SpigotNBTTagFloat(f);
					return new Spigot13NBTTagFloat(f);
				} catch (Exception e) {
					InternalLogger.printException(e, "An error has occurred while WetSponge was creating a new NBTTag!");
					return null;
				}
			default:
				return null;
		}
	}

	public static WSNBTTagInt ofInt(int i) {
		switch (WetSponge.getServerType()) {
			case SPONGE:
				return new SpongeNBTTagInt(i);
			case SPIGOT:
			case PAPER_SPIGOT:
				try {
					if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
						return new SpigotNBTTagInt(i);
					return new Spigot13NBTTagInt(i);
				} catch (Exception e) {
					InternalLogger.printException(e, "An error has occurred while WetSponge was creating a new NBTTag!");
					return null;
				}
			default:
				return null;
		}
	}

	public static WSNBTTagIntArray ofIntArray(int[] ints) {
		switch (WetSponge.getServerType()) {
			case SPONGE:
				return new SpongeNBTTagIntArray(ints);
			case SPIGOT:
			case PAPER_SPIGOT:
				try {
					if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
						return new SpigotNBTTagIntArray(ints);
					return new Spigot13NBTTagIntArray(ints);
				} catch (Exception e) {
					InternalLogger.printException(e, "An error has occurred while WetSponge was creating a new NBTTag!");
					return null;
				}
			default:
				return null;
		}
	}

	public static WSNBTTagList newList() {
		switch (WetSponge.getServerType()) {
			case SPONGE:
				return new SpongeNBTTagList();
			case SPIGOT:
			case PAPER_SPIGOT:
				try {
					if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
						return new SpigotNBTTagList();
					return new Spigot13NBTTagList();
				} catch (Exception e) {
					InternalLogger.printException(e, "An error has occurred while WetSponge was creating a new NBTTag!");
					return null;
				}
			default:
				return null;
		}
	}

	public static WSNBTTagLong ofLong(long l) {
		switch (WetSponge.getServerType()) {
			case SPONGE:
				return new SpongeNBTTagLong(l);
			case SPIGOT:
			case PAPER_SPIGOT:
				try {
					if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
						return new SpigotNBTTagLong(l);
					return new Spigot13NBTTagLong(l);
				} catch (Exception e) {
					InternalLogger.printException(e, "An error has occurred while WetSponge was creating a new NBTTag!");
					return null;
				}
			default:
				return null;
		}
	}

	public static WSNBTTagLongArray ofLongArray(long[] longs) {
		switch (WetSponge.getServerType()) {
			case SPONGE:
				return new SpongeNBTTagLongArray(longs);
			case SPIGOT:
			case PAPER_SPIGOT:
				try {
					if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
						return new SpigotNBTTagLongArray(longs);
					return new Spigot13NBTTagLongArray(longs);
				} catch (Exception e) {
					InternalLogger.printException(e, "An error has occurred while WetSponge was creating a new NBTTag!");
					return null;
				}
			default:
				return null;
		}
	}

	public static WSNBTTagShort ofShort(short s) {
		switch (WetSponge.getServerType()) {
			case SPONGE:
				return new SpongeNBTTagShort(s);
			case SPIGOT:
			case PAPER_SPIGOT:
				try {
					if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
						return new SpigotNBTTagShort(s);
					return new Spigot13NBTTagShort(s);
				} catch (Exception e) {
					InternalLogger.printException(e, "An error has occurred while WetSponge was creating a new NBTTag!");
					return null;
				}
			default:
				return null;
		}
	}

	public static WSNBTTagString ofString(String string) {
		switch (WetSponge.getServerType()) {
			case SPONGE:
				return new SpongeNBTTagString(string);
			case SPIGOT:
			case PAPER_SPIGOT:
				try {
					if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
						return new SpigotNBTTagString(string);
					return new Spigot13NBTTagString(string);
				} catch (Exception e) {
					InternalLogger.printException(e, "An error has occurred while WetSponge was creating a new NBTTag!");
					return null;
				}
			default:
				return null;
		}
	}
}
