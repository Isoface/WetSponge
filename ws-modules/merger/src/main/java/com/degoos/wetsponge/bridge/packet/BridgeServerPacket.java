package com.degoos.wetsponge.bridge.packet;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.entity.WSEntity;
import com.degoos.wetsponge.entity.living.WSLivingEntity;
import com.degoos.wetsponge.entity.living.player.WSHuman;
import com.degoos.wetsponge.enums.EnumEquipType;
import com.degoos.wetsponge.enums.EnumPlayerListItemAction;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.item.WSItemStack;
import com.degoos.wetsponge.map.WSMapView;
import com.degoos.wetsponge.material.block.WSBlockType;
import com.degoos.wetsponge.packet.play.server.*;
import com.degoos.wetsponge.packet.play.server.extra.WSPlayerListItemData;
import com.degoos.wetsponge.server.response.WSServerStatusResponse;
import com.degoos.wetsponge.text.WSText;
import com.degoos.wetsponge.util.InternalLogger;
import com.flowpowered.math.vector.Vector2d;
import com.flowpowered.math.vector.Vector2i;
import com.flowpowered.math.vector.Vector3d;
import com.flowpowered.math.vector.Vector3i;

import java.util.Collection;
import java.util.List;
import java.util.Map;

public class BridgeServerPacket {

	public static WSSPacketAnimation newWSSPacketAnimation(WSEntity entity, int animationType) {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				try {
					if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
						return new SpigotSPacketAnimation(entity, animationType);
					return new Spigot13SPacketAnimation(entity, animationType);
				} catch (Throwable e) {
					e.printStackTrace();
				}
			case SPONGE:
				return new SpongeSPacketAnimation(entity, animationType);
			default:
				return null;
		}
	}

	public static WSSPacketAnimation newWSSPacketAnimation(int entityId, int animationType) {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				try {
					if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
						return new SpigotSPacketAnimation(entityId, animationType);
					return new Spigot13SPacketAnimation(entityId, animationType);
				} catch (Throwable e) {
					e.printStackTrace();
				}
			case SPONGE:
				return new SpongeSPacketAnimation(entityId, animationType);
			default:
				return null;
		}
	}

	public static WSSPacketBlockChange newWSSPacketBlockChange(WSBlockType material, Vector3i position) {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				try {
					if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
						return new SpigotSPacketBlockChange(material, position);
					return new Spigot13SPacketBlockChange(material, position);
				} catch (Throwable e) {
					e.printStackTrace();
				}
			case SPONGE:
				return new SpongeSPacketBlockChange(material, position);
			default:
				return null;
		}
	}

	public static WSSPacketCloseWindows newWSSPacketCloseWindows(int windowsId) {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				try {
					if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
						return new SpigotSPacketCloseWindows(windowsId);
					return new Spigot13SPacketCloseWindows(windowsId);
				} catch (Throwable e) {
					e.printStackTrace();
				}
			case SPONGE:
				return new SpongeSPacketCloseWindows(windowsId);
			default:
				return null;
		}
	}

	public static WSSPacketDestroyEntities newWSSPacketDestroyEntities(int... entityIds) {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				try {
					if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
						return new SpigotSPacketDestroyEntities(entityIds);
					return new Spigot13SPacketDestroyEntities(entityIds);
				} catch (Throwable e) {
					e.printStackTrace();
				}
			case SPONGE:
				return new SpongeSPacketDestroyEntities(entityIds);
			default:
				return null;
		}
	}

	public static WSSPacketEntityEquipment newWSSPacketEntityEquipment(int entityId, EnumEquipType equipType, WSItemStack itemStack) {
		switch (WetSponge.getServerType()) {
			case SPONGE:
				return new SpongeSPacketEntityEquipment(entityId, equipType, itemStack);
			case SPIGOT:
			case PAPER_SPIGOT:
				try {
					if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
						return new SpigotSPacketEntityEquipment(entityId, equipType, itemStack);
					return new Spigot13SPacketEntityEquipment(entityId, equipType, itemStack);
				} catch (Exception e) {
					InternalLogger.printException(e, "An error has occurred while WetSponge was creating the packet WSSPacketEntityHeadLook!");
				}
			default:
				return null;
		}
	}

	public static WSSPacketEntityHeadLook newWSSPacketEntityHeadLook(int entityId, int yaw) {
		switch (WetSponge.getServerType()) {
			case SPONGE:
				return new SpongeSPacketEntityHeadLook(entityId, yaw);
			case SPIGOT:
			case PAPER_SPIGOT:
				try {
					if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
						return new SpigotSPacketEntityHeadLook(entityId, yaw);
					return new Spigot13SPacketEntityHeadLook(entityId, yaw);
				} catch (Exception e) {
					InternalLogger.printException(e, "An error has occurred while WetSponge was creating the packet WSSPacketEntityHeadLook!");
				}
			default:
				return null;
		}
	}

	public static WSSPacketEntityHeadLook newWSSPacketEntityHeadLook(WSLivingEntity entity) {
		switch (WetSponge.getServerType()) {
			case SPONGE:
				return new SpongeSPacketEntityHeadLook(entity);
			case SPIGOT:
			case PAPER_SPIGOT:
				try {
					if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
						return new SpigotSPacketEntityHeadLook(entity);
					return new Spigot13SPacketEntityHeadLook(entity);
				} catch (Exception e) {
					InternalLogger.printException(e, "An error has occurred while WetSponge was creating the packet WSSPacketEntityHeadLook!");
				}
			default:
				return null;
		}
	}

	public static WSSPacketEntityLook newWSSPacketEntityLook(WSEntity entity, Vector2i rotation, boolean onGround) {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
					return new SpigotSPacketEntityLook(entity, rotation, onGround);
				return new Spigot13SPacketEntityLook(entity, rotation, onGround);
			case SPONGE:
				return new SpongeSPacketEntityLook(entity, rotation, onGround);
			default:
				return null;
		}
	}

	public static WSSPacketEntityLook newWSSPacketEntityLook(int entity, Vector2i rotation, boolean onGround) {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
					return new SpigotSPacketEntityLook(entity, rotation, onGround);
				return new Spigot13SPacketEntityLook(entity, rotation, onGround);
			case SPONGE:
				return new SpongeSPacketEntityLook(entity, rotation, onGround);
			default:
				return null;
		}
	}

	public static WSSPacketEntityLookMove newWSSPacketEntityLookMove(WSEntity entity, Vector3i position, Vector2i rotation, boolean onGround) {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
					return new SpigotSPacketEntityLookMove(entity, position, rotation, onGround);
				return new Spigot13SPacketEntityLookMove(entity, position, rotation, onGround);
			case SPONGE:
				return new SpongeSPacketEntityLookMove(entity, position, rotation, onGround);
			default:
				return null;
		}
	}

	public static WSSPacketEntityLookMove newWSSPacketEntityLookMove(int entity, Vector3i position, Vector2i rotation, boolean onGround) {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
					return new SpigotSPacketEntityLookMove(entity, position, rotation, onGround);
				return new Spigot13SPacketEntityLookMove(entity, position, rotation, onGround);
			case SPONGE:
				return new SpongeSPacketEntityLookMove(entity, position, rotation, onGround);
			default:
				return null;
		}
	}

	public static WSSPacketEntityMetadata newWSSPacketEntityMetadata(WSEntity entity) {
		switch (WetSponge.getServerType()) {
			case PAPER_SPIGOT:
			case SPIGOT:
				try {
					if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
						return new SpigotSPacketEntityMetadata(entity);
					return new Spigot13SPacketEntityMetadata(entity);
				} catch (Exception e) {
					e.printStackTrace();
				}
			case SPONGE:
				return new SpongeSPacketEntityMetadata(entity);
			default:
				return null;
		}
	}

	public static WSSPacketEntityProperties newWSSPacketEntityProperties(WSLivingEntity entity) {
		switch (WetSponge.getServerType()) {
			case PAPER_SPIGOT:
			case SPIGOT:
				try {
					if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
						return new SpigotSPacketEntityProperties(entity);
					return new Spigot13SPacketEntityProperties(entity);
				} catch (Exception e) {
					e.printStackTrace();
				}
			case SPONGE:
				return new SpongeSPacketEntityProperties(entity);
			default:
				return null;
		}
	}

	public static WSSPacketEntityRelMove newWSSPacketEntityRelMove(WSEntity entity, Vector3i position, boolean onGround) {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
					return new SpigotSPacketEntityRelMove(entity, position, onGround);
				return new Spigot13SPacketEntityRelMove(entity, position, onGround);
			case SPONGE:
				return new SpongeSPacketEntityRelMove(entity, position, onGround);
			default:
				return null;
		}
	}

	public static WSSPacketEntityRelMove newWSSPacketEntityRelMove(int entity, Vector3i position, boolean onGround) {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
					return new SpigotSPacketEntityRelMove(entity, position, onGround);
				return new Spigot13SPacketEntityRelMove(entity, position, onGround);
			case SPONGE:
				return new SpongeSPacketEntityRelMove(entity, position, onGround);
			default:
				return null;
		}
	}

	public static WSSPacketEntityTeleport newWSSPacketEntityTeleport(WSEntity entity) {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				try {
					if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
						return new SpigotSPacketEntityTeleport(entity);
					return new Spigot13SPacketEntityTeleport(entity);
				} catch (Exception e) {
					e.printStackTrace();
				}
			case SPONGE:
				return new SpongeSPacketEntityTeleport(entity);
			default:
				return null;
		}
	}

	public static WSSPacketEntityTeleport newWSSPacketEntityTeleport(int entityIds, Vector3d position, Vector2i rotation, boolean onGround) {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				try {
					if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
						return new SpigotSPacketEntityTeleport(entityIds, position, rotation, onGround);
					return new Spigot13SPacketEntityTeleport(entityIds, position, rotation, onGround);
				} catch (Exception e) {
					e.printStackTrace();
				}
			case SPONGE:
				return new SpongeSPacketEntityTeleport(entityIds, position, rotation, onGround);
			default:
				return null;
		}
	}

	public static WSSPacketHeldItemChange newWSSPacketHeldItemChange(int slot) {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				try {
					if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
						return new SpigotSPacketHeldItemChange(slot);
					return new Spigot13SPacketHeldItemChange(slot);
				} catch (Throwable e) {
					e.printStackTrace();
				}
			case SPONGE:
				return new SpongeSPacketHeldItemChange(slot);
			default:
				return null;
		}
	}

	public static WSSPacketMaps newWSSPacketMaps(int mapId, Vector2i origin, Vector2i size, WSMapView mapView) {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				try {
					if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
						return new SpigotSPacketMaps(mapId, origin, size, mapView);
					return new Spigot13SPacketMaps(mapId, origin, size, mapView);
				} catch (Throwable e) {
					e.printStackTrace();
				}
			case SPONGE:
				return new SpongeSPacketMaps(mapId, origin, size, mapView);
			default:
				return null;
		}
	}

	public static WSSPacketMultiBlockChange newWSSPacketMultiBlockChange(Vector2i chunkPos, Map<Vector3i, WSBlockType> materials) {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				try {
					if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
						return new SpigotSPacketMultiBlockChange(chunkPos, materials);
					return new Spigot13SPacketMultiBlockChange(chunkPos, materials);
				} catch (Throwable e) {
					e.printStackTrace();
				}
			case SPONGE:
				return new SpongeSPacketMultiBlockChange(chunkPos, materials);
			default:
				return null;
		}
	}

	public static WSSPacketOpenWindow newWSSPacketOpenWindow(int windowsId, String inventoryType, WSText windowTitle, int slotCount, int entityId) {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				try {
					if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
						return new SpigotSPacketOpenWindow(windowsId, inventoryType, windowTitle, slotCount, entityId);
					return new Spigot13SPacketOpenWindow(windowsId, inventoryType, windowTitle, slotCount, entityId);
				} catch (Throwable e) {
					e.printStackTrace();
				}
			case SPONGE:
				return new SpongeSPacketOpenWindow(windowsId, inventoryType, windowTitle, slotCount, entityId);
			default:
				return null;
		}
	}

	public static WSSPacketPlayerListItem newWSSPacketPlayerListItem(EnumPlayerListItemAction action, Collection<WSPlayerListItemData> collection) {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				try {
					if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
						return new SpigotSPacketPlayerListItem(action, collection);
					return new Spigot13SPacketPlayerListItem(action, collection);
				} catch (Throwable e) {
					e.printStackTrace();
				}
			case SPONGE:
				return new SpongeSPacketPlayerListItem(action, collection);
			default:
				return null;
		}
	}

	public static WSSPacketPlayerListItem newWSSPacketPlayerListItem(EnumPlayerListItemAction action, WSPlayerListItemData... itemData) {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				try {
					if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
						return new SpigotSPacketPlayerListItem(action, itemData);
					return new Spigot13SPacketPlayerListItem(action, itemData);
				} catch (Throwable e) {
					e.printStackTrace();
				}
			case SPONGE:
				return new SpongeSPacketPlayerListItem(action, itemData);
			default:
				return null;
		}
	}

	public static WSSPacketServerInfo newWSSPacketServerInfo(WSServerStatusResponse response) {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				try {
					if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
						return new SpigotSPacketServerInfo(response);
					return new Spigot13SPacketServerInfo(response);
				} catch (Throwable e) {
					e.printStackTrace();
				}
			case SPONGE:
				return new SpongeSPacketServerInfo(response);
			default:
				return null;
		}
	}

	public static WSSPacketSetSlot newWSSPacketSetSlot(int windowsId, int slot, WSItemStack itemStack) {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				try {
					if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
						return new SpigotSPacketSetSlot(windowsId, slot, itemStack);
					return new Spigot13SPacketSetSlot(windowsId, slot, itemStack);
				} catch (Throwable e) {
					e.printStackTrace();
				}
			case SPONGE:
				return new SpongeSPacketSetSlot(windowsId, slot, itemStack);
			default:
				return null;
		}
	}

	public static WSSPacketSignEditorOpen newWSSPacketSignEditorOpen(Vector3d position) {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				try {
					if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
						return new SpigotSPacketSignEditorOpen(position);
					return new Spigot13SPacketSignEditorOpen(position);
				} catch (Throwable e) {
					e.printStackTrace();
				}
			case SPONGE:
				return new SpongeSPacketSignEditorOpen(position);
			default:
				return null;
		}
	}

	public static WSSPacketSpawnExprerienceOrb newWSSPacketSpawnExprerienceOrb(Vector3d position, int entityId, int type) {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				try {
					if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
						return new SpigotSPacketSpawnExperienceOrb(position, entityId, type);
					return new Spigot13SPacketSpawnExperienceOrb(position, entityId, type);
				} catch (Throwable e) {
					e.printStackTrace();
				}
			case SPONGE:
				return new SpongeSPacketSpawnExperienceOrb(position, entityId, type);
			default:
				return null;
		}
	}

	public static WSSPacketSpawnGlobalEntity newWSSPacketSpawnGlobalEntity(Vector3d position, int entityId, int type) {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				try {
					if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
						return new SpigotSPacketSpawnGlobalEntity(position, entityId, type);
					return new Spigot13SPacketSpawnGlobalEntity(position, entityId, type);
				} catch (Throwable e) {
					e.printStackTrace();
				}
			case SPONGE:
				return new SpongeSPacketSpawnGlobalEntity(position, entityId, type);
			default:
				return null;
		}
	}


	public static WSSPacketSpawnMob newWSSPacketSpawnMob(WSLivingEntity entity) {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				try {
					if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
						return new SpigotSPacketSpawnMob(entity);
					return new Spigot13SPacketSpawnMob(entity);
				} catch (Throwable e) {
					e.printStackTrace();
				}
			case SPONGE:
				return new SpongeSPacketSpawnMob(entity);
			default:
				return null;
		}
	}

	public static WSSPacketSpawnMob newWSSPacketSpawnMob(WSLivingEntity entity, Vector3d position, Vector3d velocity, Vector2d rotation, float headPitch) {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				try {
					if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
						return new SpigotSPacketSpawnMob(entity, position, velocity, rotation, headPitch);
					return new Spigot13SPacketSpawnMob(entity, position, velocity, rotation, headPitch);
				} catch (Throwable e) {
					e.printStackTrace();
				}
			case SPONGE:
				return new SpongeSPacketSpawnMob(entity, position, velocity, rotation, headPitch);
			default:
				return null;
		}
	}

	public static WSSPacketSpawnObject newWSSPacketSpawnObject(WSEntity entity, int type, int data) {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				try {
					if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
						return new SpigotSPacketSpawnObject(entity, type, data);
					return new Spigot13SPacketSpawnObject(entity, type, data);
				} catch (Throwable e) {
					e.printStackTrace();
				}
			case SPONGE:
				return new SpongeSPacketSpawnObject(entity, type, data);
			default:
				return null;
		}
	}

	public static WSSPacketSpawnObject newWSSPacketSpawnObject(WSEntity entity, Vector3d position, Vector3d velocity, Vector2d rotation, int type, int data) {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				try {
					if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
						return new SpigotSPacketSpawnObject(entity, position, velocity, rotation, type, data);
					return new Spigot13SPacketSpawnObject(entity, position, velocity, rotation, type, data);
				} catch (Throwable e) {
					e.printStackTrace();
				}
			case SPONGE:
				return new SpongeSPacketSpawnObject(entity, position, velocity, rotation, type, data);
			default:
				return null;
		}
	}

	public static WSSPacketSpawnPlayer newWSSPacketSpawnPlayer(WSHuman entity) {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				try {
					if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
						return new SpigotSPacketSpawnPlayer(entity);
					return new Spigot13SPacketSpawnPlayer(entity);
				} catch (Throwable e) {
					e.printStackTrace();
				}
			case SPONGE:
				return new SpongeSPacketSpawnPlayer(entity);
			default:
				return null;
		}
	}

	public static WSSPacketSpawnPlayer newWSSPacketSpawnPlayer(WSHuman entity, Vector3d position, Vector2d rotation) {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				try {
					if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
						return new SpigotSPacketSpawnPlayer(entity, position, rotation);
					return new Spigot13SPacketSpawnPlayer(entity, position, rotation);
				} catch (Throwable e) {
					e.printStackTrace();
				}
			case SPONGE:
				return new SpongeSPacketSpawnPlayer(entity, position, rotation);
			default:
				return null;
		}
	}

	public static WSSPacketWindowItems newWSSPacketWindowItems(int windowsId, List<WSItemStack> itemStacks) {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				try {
					if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
						return new SpigotSPacketWindowItems(windowsId, itemStacks);
					return new Spigot13SPacketWindowItems(windowsId, itemStacks);
				} catch (Throwable e) {
					e.printStackTrace();
				}
			case SPONGE:
				return new SpongeSPacketWindowItems(windowsId, itemStacks);
			default:
				return null;
		}
	}

	public static WSSPacketUseBed newWSSPacketUseBed(int entityId, Vector3i position) {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				try {
					if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
						return new SpigotSPacketUseBed(entityId, position);
					return new Spigot13SPacketUseBed(entityId, position);
				} catch (Throwable e) {
					e.printStackTrace();
					return null;
				}
			case SPONGE:
				return new SpongeSPacketUseBed(entityId, position);
			default:
				return null;
		}
	}

	public static WSSPacketUseBed newWSSPacketUseBed(WSHuman human, Vector3i position) {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				try {
					if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
						return new SpigotSPacketUseBed(human, position);
					return new Spigot13SPacketUseBed(human, position);
				} catch (Throwable e) {
					e.printStackTrace();
					return null;
				}
			case SPONGE:
				return new SpongeSPacketUseBed(human, position);
			default:
				return null;
		}
	}
}
