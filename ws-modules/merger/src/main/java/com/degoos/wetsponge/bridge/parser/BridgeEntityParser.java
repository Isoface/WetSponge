package com.degoos.wetsponge.bridge.parser;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.enums.EnumEntityType;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.parser.entity.Spigot13EntityParser;
import com.degoos.wetsponge.parser.entity.SpigotEntityParser;
import com.degoos.wetsponge.parser.entity.SpongeEntityParser;

public class BridgeEntityParser {

	public static boolean containsValue(EnumEntityType type) {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
					return SpigotEntityParser.containsValue(type);
				return Spigot13EntityParser.containsValue(type);
			case SPONGE:
				return SpongeEntityParser.containsValue(type);
			default:
				return false;
		}
	}

}
