package com.degoos.wetsponge.bridge.particle;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.particle.Spigot13Particle;
import com.degoos.wetsponge.particle.SpigotParticle;
import com.degoos.wetsponge.particle.SpongeParticle;
import com.degoos.wetsponge.particle.WSParticle;

public class BridgeParticles {

	public static WSParticle of(String particleName, String stringId, String minecraftId) {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
					return new SpigotParticle(stringId, minecraftId, particleName);
				return new Spigot13Particle(stringId, minecraftId, particleName);
			case SPONGE:
				return new SpongeParticle(stringId, minecraftId, particleName);
			default:
				return null;
		}

	}
}
