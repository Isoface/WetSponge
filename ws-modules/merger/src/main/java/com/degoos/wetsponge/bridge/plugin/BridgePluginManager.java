package com.degoos.wetsponge.bridge.plugin;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.parser.plugin.Spigot13PluginParser;
import com.degoos.wetsponge.parser.plugin.SpigotPluginParser;
import com.degoos.wetsponge.parser.plugin.SpongePluginParser;
import com.degoos.wetsponge.plugin.WSBasePlugin;

import java.util.Set;

public class BridgePluginManager {

	public static Set<WSBasePlugin> getBasePlugins() {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
					return SpigotPluginParser.getBasePlugins();
				return Spigot13PluginParser.getBasePlugins();
			case SPONGE:
				return SpongePluginParser.getBasePlugins();
			default:
				return null;
		}
	}

	public static boolean isBasePluginEnabled(String name) {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
					return SpigotPluginParser.isBasePluginEnabled(name);
				return Spigot13PluginParser.isBasePluginEnabled(name);
			case SPONGE:
				return SpongePluginParser.isBasePluginEnabled(name);
			default:
				return false;
		}
	}

}
