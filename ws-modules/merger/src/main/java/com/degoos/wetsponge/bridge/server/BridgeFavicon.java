package com.degoos.wetsponge.bridge.server;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.server.Spigot13Favicon;
import com.degoos.wetsponge.server.SpigotFavicon;
import com.degoos.wetsponge.server.SpongeFavicon;
import com.degoos.wetsponge.server.WSFavicon;

public class BridgeFavicon {

	public static WSFavicon ofEncoded(String encoded) {
		switch (WetSponge.getServerType()) {
			case PAPER_SPIGOT:
			case SPIGOT:
				if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
					return new SpigotFavicon(encoded);
				return new Spigot13Favicon(encoded);
			case SPONGE:
				return new SpongeFavicon(encoded);
			default:
				return null;
		}
	}

}
