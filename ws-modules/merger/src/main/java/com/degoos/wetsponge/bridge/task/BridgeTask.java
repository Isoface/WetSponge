package com.degoos.wetsponge.bridge.task;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.task.Spigot13Task;
import com.degoos.wetsponge.task.SpigotTask;
import com.degoos.wetsponge.task.SpongeTask;
import com.degoos.wetsponge.task.WSTask;

import java.util.function.Consumer;

public class BridgeTask {

	public static WSTask of(Runnable runnable) {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
					return new SpigotTask(runnable);
				return new Spigot13Task(runnable);
			case SPONGE:
				return new SpongeTask(runnable);
			default:
				return null;
		}
	}

	public static WSTask of(Consumer<WSTask> consumer) {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
					return new SpigotTask(consumer);
				return new Spigot13Task(consumer);
			case SPONGE:
				return new SpongeTask(consumer);
			default:
				return null;
		}
	}

}
