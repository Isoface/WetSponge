package com.degoos.wetsponge.bridge.text;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.text.*;
import com.degoos.wetsponge.text.translation.WSTranslation;

public class BridgeText {

	public static WSText.Builder builder() {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
					return new SpigotText.Builder();
				return new Spigot13Text.Builder();
			case SPONGE:
				return new SpongeText.Builder();
			default:
				return null;
		}
	}

	public static WSText.Builder builder(String string) {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
					return new SpigotText.Builder(string);
				return new Spigot13Text.Builder(string);
			case SPONGE:
				return new SpongeText.Builder(string);
			default:
				return null;
		}
	}


	public static WSText.Builder builder(WSText text) {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
					return new SpigotText.Builder(text);
				return new Spigot13Text.Builder(text);
			case SPONGE:
				return new SpongeText.Builder(text);
			default:
				return null;
		}
	}


	public static WSText of(String string) {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
					return new SpigotText(string);
				return new Spigot13Text(string);
			case SPONGE:
				return new SpongeText(string);
			default:
				return null;
		}
	}

	public static WSTranslatableText of(WSTranslation translation, Object... objects) {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
					return new SpigotTranslatableText(translation, objects);
				return new Spigot13TranslatableText(translation, objects);
			case SPONGE:
				return new SpongeTranslatableText(translation, objects);
			default:
				return null;
		}
	}

	public static WSText getByFormattingText(String text) {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
					return SpigotText.getByFormattingText(text);
				return Spigot13Text.getByFormattingText(text);
			case SPONGE:
				return SpongeText.getByFormattingText(text);
			default:
				return null;
		}
	}

}
