package com.degoos.wetsponge.bridge.text.action;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.text.WSText;
import com.degoos.wetsponge.text.action.click.*;
import com.degoos.wetsponge.text.action.hover.Spigot13ShowTextAction;
import com.degoos.wetsponge.text.action.hover.SpigotShowTextAction;
import com.degoos.wetsponge.text.action.hover.SpongeShowTextAction;
import com.degoos.wetsponge.text.action.hover.WSShowTextAction;

import java.net.URL;

public class BridgeTextAction {

	public static WSChangePageAction newWSChangePageAction(int page) {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
					return new SpigotChangePageAction(page);
				return new Spigot13ChangePageAction(page);
			case SPONGE:
				return new SpongeChangePageAction(page);
			default:
				return null;
		}
	}

	public static WSOpenURLAction newWSOpenURLAction(URL url) {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
					return new SpigotOpenURLAction(url);
				return new Spigot13OpenURLAction(url);
			case SPONGE:
				return new SpongeOpenURLAction(url);
			default:
				return null;
		}
	}

	public static WSRunCommandAction newWSRunCommandAction(String command) {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
					return new SpigotRunCommandAction(command);
				return new Spigot13RunCommandAction(command);
			case SPONGE:
				return new SpongeRunCommandAction(command);
			default:
				return null;
		}
	}

	public static WSSuggestCommandAction newWSSuggestCommandAction(String command) {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
					return new SpigotSuggestCommandAction(command);
				return new Spigot13SuggestCommandAction(command);
			case SPONGE:
				return new SpongeSuggestCommandAction(command);
			default:
				return null;
		}
	}

	public static WSShowTextAction newWSShowTextAction(WSText text) {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
					return new SpigotShowTextAction(text);
				return new Spigot13ShowTextAction(text);
			case SPONGE:
				return new SpongeShowTextAction(text);
			default:
				return null;
		}
	}
}
