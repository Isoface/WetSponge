package com.degoos.wetsponge.bridge.user;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.user.Spigot13GameProfile;
import com.degoos.wetsponge.user.SpigotGameProfile;
import com.degoos.wetsponge.user.SpongeGameProfile;
import com.degoos.wetsponge.user.WSGameProfile;

import java.util.UUID;

public class BridgeGameProfile {

	public static WSGameProfile of(UUID uniqueId, String name) {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
					return SpigotGameProfile.of(uniqueId, name);
				return Spigot13GameProfile.of(uniqueId, name);
			case SPONGE:
				return SpongeGameProfile.of(uniqueId, name);
			default:
				return null;
		}
	}

}
