package com.degoos.wetsponge.bridge.user;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.user.Spigot13User;
import com.degoos.wetsponge.user.SpigotUser;
import com.degoos.wetsponge.user.SpongeUser;
import com.degoos.wetsponge.user.WSUser;

import java.util.UUID;

public class BridgeUser {

	public static WSUser of(UUID uuid) {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
					return SpigotUser.of(uuid);
				return Spigot13User.of(uuid);
			case SPONGE:
				return SpongeUser.of(uuid);
			default:
				return null;
		}
	}

	public static WSUser of(UUID uuid, String name) {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
					return SpigotUser.of(uuid);
				return Spigot13User.of(uuid);
			case SPONGE:
				return SpongeUser.of(uuid, name);
			default:
				return null;
		}
	}
}
