package com.degoos.wetsponge.bridge.world;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.world.Spigot13Explosion;
import com.degoos.wetsponge.world.SpigotExplosion;
import com.degoos.wetsponge.world.SpongeExplosion;
import com.degoos.wetsponge.world.WSExplosion;

public class BridgeExplosion {

	public static WSExplosion.Builder builder() {
		switch (WetSponge.getServerType()) {
			case PAPER_SPIGOT:
			case SPIGOT:
				if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
					return new SpigotExplosion.Builder();
				return new Spigot13Explosion.Builder();
			case SPONGE:
				return new SpongeExplosion.Builder();
			default:
				return null;
		}
	}

}
