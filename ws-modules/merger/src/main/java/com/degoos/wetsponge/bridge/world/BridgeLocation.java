package com.degoos.wetsponge.bridge.world;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.world.Spigot13Location;
import com.degoos.wetsponge.world.SpigotLocation;
import com.degoos.wetsponge.world.SpongeLocation;
import com.degoos.wetsponge.world.WSLocation;

public class BridgeLocation {

	public static WSLocation of(String worldName, double x, double y, double z, float yaw, float pitch) {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
					return new SpigotLocation(worldName, x, y, z, yaw, pitch);
				else return new Spigot13Location(worldName, x, y, z, yaw, pitch);
			case SPONGE:
				return new SpongeLocation(worldName, x, y, z, yaw, pitch);
			default:
				return null;
		}
	}

	public static WSLocation of(String string) {
		switch (WetSponge.getServerType()) {
			case SPIGOT:
			case PAPER_SPIGOT:
				if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
					return new SpigotLocation(string);
				else return new Spigot13Location(string);
			case SPONGE:
				return new SpongeLocation(string);
			default:
				return null;
		}
	}
}
