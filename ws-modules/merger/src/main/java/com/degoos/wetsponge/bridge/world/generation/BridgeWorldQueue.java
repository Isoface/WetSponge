package com.degoos.wetsponge.bridge.world.generation;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.world.WSWorld;
import com.degoos.wetsponge.world.edit.Spigot13WorldQueue;
import com.degoos.wetsponge.world.edit.SpigotWorldQueue;
import com.degoos.wetsponge.world.edit.SpongeWorldQueue;
import com.degoos.wetsponge.world.edit.WSWorldQueue;

public class BridgeWorldQueue {

	public static WSWorldQueue of(WSWorld world) {
		switch (WetSponge.getServerType()) {
			case SPONGE:
				return new SpongeWorldQueue(world);
			case SPIGOT:
			case PAPER_SPIGOT:
				if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13))
					return new SpigotWorldQueue(world);
				return new Spigot13WorldQueue(world);
			default:
				return null;
		}
	}

}
