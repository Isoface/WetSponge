package com.degoos.wetsponge;


import com.degoos.wetsponge.command.wetspongecommand.SpigotWetspongeCommand;
import com.degoos.wetsponge.enums.EnumEntityType;
import com.degoos.wetsponge.enums.EnumServerType;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.enums.EnumTextColor;
import com.degoos.wetsponge.loader.SpigotListenerLoader;
import com.degoos.wetsponge.loader.SpigotWetSpongeLoader;
import com.degoos.wetsponge.mixin.spigot.SpigotInjector;
import com.degoos.wetsponge.parser.entity.SpigotEntityParser;
import com.degoos.wetsponge.resource.spigot.SpigotBungeeCord;
import com.degoos.wetsponge.server.SpigotServer;
import com.degoos.wetsponge.text.WSText;
import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.reflection.SpigotServerUtils;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Arrays;

public class SpigotOldWetSponge implements SpigotWetSpongeLoader {


	public void onEnable(JavaPlugin instance, EnumServerVersion version) {
		new BukkitRunnable() {
			@Override
			public void run() {
				try {
					long millis = System.currentTimeMillis();
					EnumServerType type = EnumServerType.SPIGOT;
					try {
						Class.forName("com.destroystokyo.paper.PaperConfig");
						type = EnumServerType.PAPER_SPIGOT;
					} catch (ClassNotFoundException ignore) {
					}

					WetSponge.load(instance.getDescription().getVersion(), type, SpigotOldWetSponge.this, new SpigotServer(Bukkit.getServer()), version,
							new SpigotBungeeCord(instance), SpigotServerUtils.getMainThread());

					InternalLogger.sendInfo("Loading WetSponge " + instance.getDescription().getVersion() + "...");

					InternalLogger
							.sendInfo(WSText.builder("Using version ").append(WSText.builder("SPIGOT " + version.name()).color(EnumTextColor.GREEN).build()).build());

					InternalLogger.sendInfo("Loading entities.");
					SpigotEntityParser.load();
					Arrays.stream(EnumEntityType.values()).forEach(EnumEntityType::load);
					InternalLogger.sendInfo("Loading Spigot listeners.");
					SpigotListenerLoader.load();

					InternalLogger.sendInfo("Loading injectors.");
					SpigotInjector.inject(instance);

					InternalLogger.sendInfo("Loading common.");
					WetSponge.loadCommon();

					instance.getCommand("wetSpongeCmd").setExecutor(new SpigotWetspongeCommand());

					double secs = (System.currentTimeMillis() - millis) / 1000D;
					InternalLogger.sendDone(WSText.builder("WetSponge has been loaded in ").append(WSText.builder(String.valueOf(secs)).color(EnumTextColor.RED).build())
							.append(WSText.builder(" seconds!").color(EnumTextColor.GREEN).build()).build());
				} catch (Throwable ex) {
					InternalLogger.printException(ex, "An error has occurred while WetSponge was loading!");
				}
			}
		}.runTaskLater(instance, 1);
	}

	@Override
	public void onDisable() {
		try {
			WetSponge.unloadCommon();
		} catch (Throwable ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was unloading!");
		}
	}
}