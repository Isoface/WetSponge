package com.degoos.wetsponge.bar;

import com.degoos.wetsponge.SpigotWetSponge;
import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.effect.potion.WSPotionEffect;
import com.degoos.wetsponge.entity.living.complex.WSEnderDragon;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.enums.EnumBossBarColor;
import com.degoos.wetsponge.enums.EnumBossBarOverlay;
import com.degoos.wetsponge.enums.EnumPotionEffectType;
import com.degoos.wetsponge.packet.play.server.WSSPacketDestroyEntities;
import com.degoos.wetsponge.packet.play.server.WSSPacketSpawnMob;
import com.degoos.wetsponge.text.WSText;
import com.degoos.wetsponge.world.WSWorld;
import com.flowpowered.math.vector.Vector3d;
import java.util.HashSet;
import java.util.Set;
import org.bukkit.scheduler.BukkitRunnable;

public class OldSpigotBossBar implements WSBossBar {

	private static int count = 0;

	private WSEnderDragon enderDragon;
	private Set<WSPlayer> players;
	private boolean visible;
	private int id;

	public OldSpigotBossBar() {
		players = new HashSet<>();
		id = 1000 + count;
		count++;
		WSWorld world = WetSponge.getServer().getWorlds().stream().findAny().orElse(null);
		if (world == null) {
			enderDragon = null;
			return;
		}
		enderDragon = world.createEntity(WSEnderDragon.class, new Vector3d()).orElse(null);
		if (enderDragon != null)
			enderDragon.addPotionEffect(WSPotionEffect.builder().type(EnumPotionEffectType.INVISIBILITY).duration(Integer.MAX_VALUE).amplifier(2).build());

		new BukkitRunnable() {
			@Override
			public void run() {
				sendSpawnPacket(players);
			}
		}.runTaskTimer(SpigotWetSponge.getInstance(), 10, 10);
	}

	@Override
	public WSBossBar addPlayer(WSPlayer player) {
		players.add(player);
		sendSpawnPacket(player);
		return this;
	}

	@Override
	public WSBossBar removePlayer(WSPlayer player) {
		players.remove(player);
		sendDestroyPacket(player);
		return this;
	}

	@Override
	public WSBossBar clearPlayers() {
		sendDestroyPacket(players);
		players.clear();
		return this;
	}

	@Override
	public Set<WSPlayer> getPlayers() {
		return new HashSet<>(players);
	}

	@Override
	public EnumBossBarColor getColor() {
		return EnumBossBarColor.PURPLE;
	}

	@Override
	public WSBossBar setColor(EnumBossBarColor color) {
		return this;
	}

	@Override
	public EnumBossBarOverlay getOverlay() {
		return EnumBossBarOverlay.PROGRESS;
	}

	@Override
	public WSBossBar setOverlay(EnumBossBarOverlay overlay) {
		return this;
	}

	@Override
	public float getPercent() {
		return enderDragon == null ? 0 : (float) (enderDragon.getHealth() / enderDragon.getMaxHealth());
	}

	@Override
	public WSBossBar setPercent(float percent) {
		if (enderDragon != null) enderDragon.setHealth(enderDragon.getMaxHealth() * percent);
		return this;
	}

	@Override
	public boolean shouldCreateFog() {
		return false;
	}

	@Override
	public WSBossBar setCreateFog(boolean createFog) {
		return this;
	}

	@Override
	public boolean shouldDarkenSky(boolean darkenSky) {
		return false;
	}

	@Override
	public WSBossBar setDarkenSky(boolean darkenSky) {
		return this;
	}

	@Override
	public boolean shouldPlayEndBossMusic() {
		return false;
	}

	@Override
	public WSBossBar setPlayEndBossMusic(boolean playEndBossMusic) {
		return this;
	}

	@Override
	public boolean isVisible() {
		return visible;
	}

	@Override
	public WSBossBar setVisible(boolean visible) {
		this.visible = visible;
		if (visible) sendSpawnPacket(players);
		else sendDestroyPacket(players);
		return this;
	}

	@Override
	public WSText getName() {
		return enderDragon.getCustomName().orElse(WSText.empty());
	}

	@Override
	public WSBossBar setName(WSText name) {
		if (enderDragon != null) enderDragon.setCustomName(name);
		return this;
	}

	@Override
	public OldSpigotBossBar getHandled() {
		return this;
	}

	private void sendSpawnPacket(WSPlayer player) {
		if (!visible || enderDragon == null) return;
		WSSPacketSpawnMob spawnMob = WSSPacketSpawnMob.of(enderDragon);
		spawnMob.setEntityId(id);
		spawnMob.setPosition(player.getLocation().setY(Math.min(-10, player.getLocation().getY() - 100)).toVector3d());
		player.sendPacket(spawnMob);
	}

	private void sendSpawnPacket(Set<WSPlayer> players) {
		if (!visible || enderDragon == null) return;
		players.forEach(player -> {
			WSSPacketSpawnMob spawnMob = WSSPacketSpawnMob.of(enderDragon);
			spawnMob.setEntityId(id);
			spawnMob.setPosition(player.getLocation().setY(Math.min(-10, player.getLocation().getY() - 100)).toVector3d());
			player.sendPacket(spawnMob);
		});
	}

	private void sendDestroyPacket(WSPlayer player) {
		player.sendPacket(WSSPacketDestroyEntities.of(id));
	}

	private void sendDestroyPacket(Set<WSPlayer> players) {
		WSSPacketDestroyEntities packet = WSSPacketDestroyEntities.of(id);
		players.forEach(player -> player.sendPacket(packet));
	}


	public static class Builder implements WSBossBar.Builder {

		private EnumBossBarColor color;
		private EnumBossBarOverlay overlay;
		private float percent;
		private boolean createFog, darkenSky, endBossMusic, visible;
		private WSText name;

		public Builder() {
			color = EnumBossBarColor.PURPLE;
			overlay = EnumBossBarOverlay.PROGRESS;
			percent = 1;
			createFog = darkenSky = endBossMusic = visible = false;
			name = WSText.of("");
		}

		@Override
		public OldSpigotBossBar.Builder color(EnumBossBarColor color) {
			this.color = color;
			return this;
		}

		@Override
		public OldSpigotBossBar.Builder overlay(EnumBossBarOverlay overlay) {
			this.overlay = overlay;
			return this;
		}

		@Override
		public OldSpigotBossBar.Builder percent(float percent) {
			this.percent = percent;
			return this;
		}

		@Override
		public OldSpigotBossBar.Builder createFog(boolean createFog) {
			this.createFog = createFog;
			return this;
		}

		@Override
		public OldSpigotBossBar.Builder darkenSky(boolean darkenSky) {
			this.darkenSky = darkenSky;
			return this;
		}

		@Override
		public OldSpigotBossBar.Builder playEndBossMusic(boolean endBossMusic) {
			this.endBossMusic = endBossMusic;
			return this;
		}

		@Override
		public OldSpigotBossBar.Builder visible(boolean visible) {
			this.visible = visible;
			return this;
		}

		@Override
		public OldSpigotBossBar.Builder name(WSText name) {
			this.name = name;
			return this;
		}

		@Override
		public WSBossBar build() {
			OldSpigotBossBar bossBar = new OldSpigotBossBar();
			bossBar.setName(name);
			bossBar.setColor(color);
			bossBar.setOverlay(overlay);
			bossBar.setVisible(visible);
			bossBar.setPercent(percent);
			bossBar.setCreateFog(createFog);
			bossBar.setDarkenSky(darkenSky);
			bossBar.setPlayEndBossMusic(endBossMusic);
			return bossBar;
		}
	}
}
