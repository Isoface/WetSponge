package com.degoos.wetsponge.block;


import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.block.tileentity.*;
import com.degoos.wetsponge.enums.EnumMapBaseColor;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.material.WSBlockTypes;
import com.degoos.wetsponge.material.block.SpigotBlockType;
import com.degoos.wetsponge.material.block.WSBlockType;
import com.degoos.wetsponge.parser.world.WorldParser;
import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.reflection.ReflectionUtils;
import com.degoos.wetsponge.util.reflection.SpigotHandledUtils;
import com.degoos.wetsponge.util.reflection.SpigotMapUtils;
import com.degoos.wetsponge.world.SpigotLocation;
import com.degoos.wetsponge.world.WSLocation;
import com.degoos.wetsponge.world.WSWorld;
import org.bukkit.block.Block;
import org.bukkit.material.MaterialData;

import java.util.Optional;

public class SpigotBlock implements WSBlock {

	private Block block;


	public SpigotBlock(Block block) {
		this.block = block;
	}


	@Override
	public WSLocation getLocation() {
		return new SpigotLocation(block.getLocation());
	}


	@Override
	public WSWorld getWorld() {
		return WorldParser.getOrCreateWorld(block.getWorld().getName(), block.getWorld());
	}


	@Override
	public int getNumericalId() {
		return block.getTypeId();
	}


	@Override
	public String getStringId() {
		return WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13) ? getOldStringId() : getNewStringId();
	}

	@Override
	public String getNewStringId() {
		Optional<WSBlockType> optional = WSBlockTypes.getById(block.getType().getId());
		if (!optional.isPresent()) return "minecraft:air";
		MaterialData data = block.getType().getNewData(block.getData());
		return ((SpigotBlockType) optional.get()).readMaterialData(data).getNewStringId();
	}

	@Override
	public String getOldStringId() {
		Optional<WSBlockType> optional = WSBlockTypes.getById(block.getType().getId());
		return optional.map(WSBlockType::getOldStringId).orElse("minecraft:air");
	}


	@Override
	public WSBlockState createState() {
		return new SpigotBlockState(this);
	}


	@Override
	public SpigotTileEntity getTileEntity() {
		switch (getNumericalId()) {
			case 23:
				return new SpigotTileEntityDispenser(this);
			case 25:
				return new SpigotTileEntityNoteblock(this);
			case 52:
				return new SpigotTileEntityMonsterSpawner(this);
			case 54:
			case 146:
				return new SpigotTileEntityChest(this);
			case 61:
			case 62:
				return new SpigotTileEntityFurnace(this);
			case 63:
			case 68:
				return new SpigotTileEntitySign(this);
			case 84:
				return new SpigotTileEntityJukebox(this);
			case 116:
				return new SpigotTileEntityNameable(this);
			case 117:
				return new SpigotTileEntityBrewingStand(this);
			case 137:
				return new SpigotTileEntityCommandBlock(this);
			case 144:
				return new SpigotTileEntitySkull(this);
			case 154:
				return new SpigotTileEntityNameableInventory(this);
			default:
				return new SpigotTileEntity(this);
		}
	}

	@Override
	public WSBlockType getBlockType() {
		return ((SpigotBlockType) WSBlockTypes.getById(getNumericalId()).orElse(WSBlockTypes.AIR.getDefaultState())).readMaterialData(block.getType().getNewData(block.getData()));
	}

	@Override
	public WSBlock getRelative(EnumBlockFace direction) {
		return getLocation().add(direction.getRelative().toDouble()).getBlock();
	}

	@Override
	public EnumMapBaseColor getMapBaseColor() {
		Object world = SpigotHandledUtils.getWorldHandle(block.getWorld());
		Object blockPosition = SpigotHandledUtils.getBlockPosition(block.getLocation());
		try {
			Object iBlockData = ReflectionUtils.invokeMethod(world, "getType", blockPosition);
			return SpigotMapUtils.getMapBaseColor(iBlockData, world, blockPosition, getNumericalId());
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was getting the map base color of the block " + getStringId() + "!");
			return EnumMapBaseColor.AIR;
		}
	}

	@Override
	public Block getHandled() {
		return block;
	}

}
