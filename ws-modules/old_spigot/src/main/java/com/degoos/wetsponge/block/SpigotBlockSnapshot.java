package com.degoos.wetsponge.block;

import com.degoos.wetsponge.material.WSBlockTypes;
import com.degoos.wetsponge.material.block.SpigotBlockType;
import org.bukkit.block.Block;

public class SpigotBlockSnapshot extends WSBlockSnapshot {


	public SpigotBlockSnapshot(Block block) {
		super(((SpigotBlockType) WSBlockTypes.getById(block.getTypeId())
				.orElse(new SpigotBlockType(block.getTypeId(), "", "",
						block.getType().getMaxStackSize()))).readMaterialData(block.getType().getNewData(block.getData())));
	}
}
