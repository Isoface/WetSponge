package com.degoos.wetsponge.block.tileentity;


import com.degoos.wetsponge.block.SpigotBlock;
import com.degoos.wetsponge.inventory.SpigotInventory;
import org.bukkit.block.BlockState;
import org.bukkit.inventory.InventoryHolder;

public class SpigotTileEntityInventory extends SpigotTileEntity implements WSTileEntityInventory {

	public SpigotTileEntityInventory(SpigotBlock block) {
		super(block);
	}

	public SpigotTileEntityInventory(BlockState blockState) {
		super(blockState);
	}

	@Override
	public SpigotInventory getInventory() {
		return new SpigotInventory(getHandled().getInventory());
	}

	@Override
	public InventoryHolder getHandled() {
		return (InventoryHolder) super.getHandled();
	}
}
