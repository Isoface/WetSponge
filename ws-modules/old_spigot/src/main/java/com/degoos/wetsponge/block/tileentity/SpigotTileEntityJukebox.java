package com.degoos.wetsponge.block.tileentity;

import com.degoos.wetsponge.block.SpigotBlock;
import com.degoos.wetsponge.item.WSItemStack;
import org.bukkit.Material;
import org.bukkit.block.Jukebox;

public class SpigotTileEntityJukebox extends SpigotTileEntity implements WSTileEntityJukebox {


	public SpigotTileEntityJukebox(SpigotBlock block) {
		super(block);
	}

	@Override
	public void playRecord() {
		if (getHandled().isPlaying()) return;
		getHandled().setPlaying(getHandled().getPlaying());
		update();
	}

	@Override
	public void stopRecord() {
		getHandled().setPlaying(Material.AIR);
		update();
	}

	@Override
	public void ejectRecord() {
		getHandled().eject();
		update();
	}

	@Override
	public void insertRecord(WSItemStack record) {
		getHandled().setPlaying(Material.getMaterial(record.getMaterial().getNumericalId()));
		update();
	}

	@Override
	public Jukebox getHandled() {
		return (Jukebox) super.getHandled();
	}
}
