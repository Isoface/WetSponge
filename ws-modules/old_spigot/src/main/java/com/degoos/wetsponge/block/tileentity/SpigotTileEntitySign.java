package com.degoos.wetsponge.block.tileentity;


import com.degoos.wetsponge.SpigotWetSponge;
import com.degoos.wetsponge.block.SpigotBlock;
import com.degoos.wetsponge.entity.living.player.SpigotPlayer;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.text.WSText;
import com.degoos.wetsponge.util.reflection.SpigotHandledUtils;
import com.degoos.wetsponge.util.reflection.NMSUtils;
import org.bukkit.Location;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class SpigotTileEntitySign extends SpigotTileEntity implements WSTileEntitySign {

	public SpigotTileEntitySign(SpigotBlock block) {
		super(block);
	}


	@Override
	public void setLine(int line, WSText value) {
		if (line < 0 || line > 3) return;
		Sign sign = getHandled();
		sign.setLine(line, value.toFormattingText());
		update();
	}

	@Override
	public Optional<WSText> getLine(int line) {
		return Optional.ofNullable(WSText.getByFormattingText(getHandled().getLine(line)));
	}

	@Override
	public List<WSText> getLines() {
		return Arrays.stream(getHandled().getLines()).map(WSText::getByFormattingText).collect(Collectors.toList());
	}

	@Override
	public void setLines(WSText[] lines) {
		if (lines.length < 4) return;
		Sign sign = getHandled();
		for (int i = 0; i < 4; i++) sign.setLine(i, lines[i].toFormattingText());
		update();
	}

	@Override
	public void editSign(WSPlayer wsPlayer) {
		new BukkitRunnable() {
			@Override
			public void run() {
				try {
					Player player = ((SpigotPlayer) wsPlayer).getHandled();
					Location location = getHandled().getLocation();
					Object blockPosition = SpigotHandledUtils.getBlockPosition(location);
					Object sign = NMSUtils.getNMSClass("World")
							.getMethod("getTileEntity", blockPosition.getClass()).invoke(SpigotHandledUtils.getWorldHandle(player.getWorld()), blockPosition);
					Field field = NMSUtils.getNMSClass("TileEntitySign").getField("isEditable");
					field.setAccessible(true);
					field.setBoolean(sign, true);
					NMSUtils.getNMSClass("EntityPlayer").getMethod("openSign", sign.getClass()).invoke(SpigotHandledUtils.getPlayerHandle(player), sign);
				} catch (Throwable ex) {
					ex.printStackTrace();
				}
			}
		}.runTaskLater(SpigotWetSponge.getInstance(), 2);
	}

	@Override
	public Sign getHandled() {
		return (Sign) super.getHandled();
	}
}
