package com.degoos.wetsponge.entity;


import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.enums.EnumEntityType;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;
import com.degoos.wetsponge.parser.entity.SpigotEntityParser;
import com.degoos.wetsponge.parser.world.WorldParser;
import com.degoos.wetsponge.text.WSText;
import com.degoos.wetsponge.text.translation.WSTranslation;
import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.Validate;
import com.degoos.wetsponge.util.reflection.NMSUtils;
import com.degoos.wetsponge.util.reflection.ReflectionUtils;
import com.degoos.wetsponge.util.reflection.SpigotHandledUtils;
import com.degoos.wetsponge.world.SpigotLocation;
import com.degoos.wetsponge.world.WSLocation;
import com.degoos.wetsponge.world.WSWorld;
import com.flowpowered.math.vector.Vector3d;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.util.Vector;

import javax.annotation.Nullable;
import java.util.*;
import java.util.stream.Collectors;

public class SpigotEntity implements WSEntity {


	private Entity entity;
	private EnumEntityType entityType;
	private Map<String, Object> properties;
	private boolean invincible;
	private long invincibleMillis;


	public SpigotEntity(Entity entity) {
		Validate.notNull(entity, "Entity cannot be null!");
		this.entity = entity;
		this.entityType = SpigotEntityParser.getEntityType(entity);
		this.properties = new HashMap<>();
	}


	@Override
	public UUID getUniqueId() {
		return entity.getUniqueId();
	}


	@Override
	public EnumEntityType getEntityType() {
		return entityType;
	}


	@Override
	public boolean isDead() {
		return entity.isDead();
	}

	@Override
	public boolean isOnGround() {
		return entity.isOnGround();
	}


	@Override
	public WSLocation getLocation() {
		return new SpigotLocation(entity.getLocation());
	}


	@Override
	public void setLocation(WSLocation location) {
		entity.teleport(new Location(((SpigotLocation) location).getWorld().getHandled(), location.getX(), location.getY(), location.getZ(), location.getYaw(), location
				.getPitch()));
	}


	@Override
	public WSWorld getWorld() {
		return WorldParser.getOrCreateWorld(entity.getWorld().getName(), entity.getWorld());
	}


	@Override
	public Vector3d getVelocity() {
		Vector vector = entity.getVelocity();
		return new Vector3d(vector.getX(), vector.getY(), vector.getZ());
	}


	@Override
	public void setVelocity(Vector3d velocity) {
		entity.setVelocity(new Vector(velocity.getX(), velocity.getY(), velocity.getZ()));
	}

	@Override
	public Vector3d getRotation() {
		Location location = entity.getLocation();
		return new Vector3d(location.getYaw(), location.getPitch(), 0);
	}

	@Override
	public void setRotation(Vector3d rotation) {
		setLocation(getLocation().setYaw((float) rotation.getX()).setPitch((float) rotation.getY()));
	}

	@Override
	public Optional<WSText> getCustomName() {
		return Optional.ofNullable(entity.getCustomName()).map(WSText::getByFormattingText);
	}

	@Override
	public void setCustomName(@Nullable WSText customName) {
		if (customName == null) entity.setCustomName(null);
		else entity.setCustomName(customName.toFormattingText());
	}

	@Override
	public boolean isCustomNameVisible() {
		return entity.isCustomNameVisible();
	}

	@Override
	public void setCustomNameVisible(boolean customNameVisible) {
		entity.setCustomNameVisible(customNameVisible);
	}

	@Override
	public Set<WSEntity> getPassengers() {
		if (WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD))
			return entity.getPassengers().stream().map(SpigotEntityParser::getWSEntity).collect(Collectors.toSet());
		else {
			Entity target = entity.getPassenger();
			Set<WSEntity> set = new HashSet<>();
			if (target != null) set.add(SpigotEntityParser.getWSEntity(target));
			return set;
		}
	}

	@Override
	public boolean addPassenger(WSEntity entity) {
		if (WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD)) return this.entity.addPassenger(((SpigotEntity) entity).entity);
		else return getHandled().setPassenger(((SpigotEntity) entity).entity);
	}

	@Override
	public Optional<WSEntity> getRidingEntity() {
		try {

			return Optional.ofNullable(entity.getVehicle()).map(SpigotEntityParser::getWSEntity);

			/*Class<?> clazz = NMSUtils.getNMSClass("Entity");
			Object handled = SpigotHandledUtils.getEntityHandle(entity);
			Object riding;
			if (WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD)) riding = clazz.getMethod("bB").invoke(handled);
			else riding = clazz.getField("vehicle").get(handled);
			if (riding == null) return Optional.empty();
			return Optional.of(SpigotEntityParser.getWSEntity((Entity) clazz.getMethod("getBukkitEntity").invoke(riding)));*/
		} catch (Throwable ex) {
			ex.printStackTrace();
			return Optional.empty();
		}
	}

	@Override
	public void mountEntity(WSEntity entity) {
		try {
			Class<?> clazz = NMSUtils.getNMSClass("Entity");
			clazz.getMethod(WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD) ? "startRiding" : "mount", clazz)
					.invoke(SpigotHandledUtils.getEntityHandle(this.entity), SpigotHandledUtils.getEntityHandle(((SpigotEntity) entity).entity));
		} catch (Throwable ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public void dismountRidingEntity() {
		if (WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD)) try {
			Object handled = SpigotHandledUtils.getEntityHandle(entity);
			NMSUtils.getNMSClass("Entity").getMethod("stopRiding").invoke(handled);
		} catch (Throwable ex) {
			ex.printStackTrace();
		}
		else try {
			Class<?> clazz = NMSUtils.getNMSClass("Entity");
			clazz.getMethod("mount", clazz).invoke(SpigotHandledUtils.getEntityHandle(this.entity), (Object) null);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public boolean isRiding() {
		try {
			if (WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD)) {
				return (boolean) NMSUtils.getNMSClass("Entity").getMethod("isPassenger").invoke(SpigotHandledUtils.getEntityHandle(entity));
			} else return NMSUtils.getNMSClass("Entity").getField("vehicle").get(SpigotHandledUtils.getEntityHandle(entity)) != null;
		} catch (Throwable ex) {
			ex.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean isBeingRidden() {
		if (WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD)) try {
			return (boolean) NMSUtils.getNMSClass("Entity").getMethod("isVehicle").invoke(entity);
		} catch (Throwable ex) {
			ex.printStackTrace();
			return false;
		}
		else return entity.getPassengers().isEmpty();
	}

	@Override
	public int getEntityId() {
		return entity.getEntityId();
	}

	@Override
	public void addProperty(String id, Object value) {
		properties.putIfAbsent(id, value);
	}

	@Override
	public void addProperty(String id, Object value, boolean force) {
		if (force) properties.put(id, value);
		else properties.putIfAbsent(id, value);
	}

	@Override
	public Optional<Object> getProperty(String id) {
		return Optional.ofNullable(properties.get(id));
	}

	@Override
	public <T> Optional<T> getProperty(String id, Class<T> expected) {
		Optional<Object> optional = Optional.ofNullable(properties.get(id));
		if (!optional.isPresent() || !expected.isInstance(optional.get())) return Optional.empty();
		return optional.map(o -> (T) o);
	}

	@Override
	public void removeProperty(String id) {
		properties.remove(id);
	}

	@Override
	public Map<String, Object> getProperties() {
		return new HashMap<>(properties);
	}

	@Override
	public boolean hasGravity() {
		return entity.hasGravity();
	}

	@Override
	public void setGravity(boolean gravity) {
		entity.setGravity(gravity);
	}

	@Override
	public boolean isGlowing() {
		return entity.isGlowing();
	}

	@Override
	public void setGlowing(boolean glowing) {
		entity.setGlowing(glowing);
	}

	@Override
	public boolean isSilent() {
		return entity.isSilent();
	}

	@Override
	public void setSilent(boolean silent) {
		if (WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD)) entity.setSilent(silent);
	}

	@Override
	public boolean isInvisible() {
		Object handled = SpigotHandledUtils.getEntityHandle(entity);
		try {
			return (boolean) handled.getClass().getMethod("isInvisible").invoke(handled);
		} catch (Throwable e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public void setInvisible(boolean invisible) {
		Object handled = SpigotHandledUtils.getEntityHandle(entity);
		try {
			handled.getClass().getMethod("setInvisible", boolean.class).invoke(handled, invisible);
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean isInvincible() {
		return invincible || invincibleMillis > System.currentTimeMillis();
	}

	@Override
	public void setInvincible(boolean invincible) {
		invincibleMillis = 0;
		this.invincible = invincible;
	}

	@Override
	public void setInvincibleMillis(int millis) {
		invincibleMillis = System.currentTimeMillis() + millis;
		invincible = false;
	}

	@Override
	public int getAir() {
		try {
			Object handled = SpigotHandledUtils.getEntityHandle(entity);
			return (int) handled.getClass().getMethod("getAir").invoke(entity);
		} catch (Throwable e) {
			e.printStackTrace();
			return 0;
		}
	}

	@Override
	public void setAir(int air) {
		try {
			Object handled = SpigotHandledUtils.getEntityHandle(entity);
			handled.getClass().getMethod("setAir", int.class).invoke(entity, air);
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	@Override
	public int getFireTicks() {
		return entity.getFireTicks();
	}

	@Override
	public void setFireTicks(int fireTicks) {
		entity.setFireTicks(fireTicks);
	}

	@Override
	public int getMaxFireTicks() {
		return entity.getMaxFireTicks();
	}

	@Override
	public float getFallDistance() {
		return entity.getFallDistance();
	}

	@Override
	public void setFallDistance(float fallDistance) {
		entity.setFallDistance(fallDistance);
	}

	@Override
	public Vector3d getEntitySize() {
		if (WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD))
			return new Vector3d(entity.getWidth(), entity.getHeight(), entity.getWidth());
		else {
			Object handled = SpigotHandledUtils.getEntityHandle(getHandled());
			Class<?> entityClass = NMSUtils.getNMSClass("Entity");
			try {
				float width = entityClass.getField("width").getFloat(handled);
				float length = entityClass.getField("length").getFloat(handled);
				return new Vector3d(width, length, width);
			} catch (Exception ex) {
				InternalLogger.printException(ex, "An exception has occurred while WetSponge was getting the size of an Entity!");
				return new Vector3d();
			}
		}
	}

	@Override
	public WSNBTTagCompound writeToNBTTagCompound(WSNBTTagCompound nbtTagCompound) {
		Object handled = SpigotHandledUtils.getHandle(entity);
		try {
			ReflectionUtils.invokeMethod(handled, WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD) ? "save" : "e", nbtTagCompound.getHandled());
		} catch (Exception e) {
			InternalLogger.printException(e, "An error has occurred while WetSponge was saving an Entity on a NBTTagCompound!");
		}
		return nbtTagCompound;
	}

	@Override
	public WSNBTTagCompound writeToNBTTagCompoundAtomically(WSNBTTagCompound nbtTagCompound) {
		Object handled = SpigotHandledUtils.getHandle(entity);
		try {
			ReflectionUtils.invokeMethod(handled, "c", nbtTagCompound.getHandled());
		} catch (Exception e) {
			InternalLogger.printException(e, "An error has occurred while WetSponge was saving an Entity on a NBTTagCompound!");
		}
		return nbtTagCompound;
	}

	@Override
	public WSNBTTagCompound writeToNBTTagCompoundOptional(WSNBTTagCompound nbtTagCompound) {
		Object handled = SpigotHandledUtils.getHandle(entity);
		try {
			ReflectionUtils.invokeMethod(handled, "d", nbtTagCompound.getHandled());
		} catch (Exception e) {
			InternalLogger.printException(e, "An error has occurred while WetSponge was saving an Entity on a NBTTagCompound!");
		}
		return nbtTagCompound;
	}

	@Override
	public WSNBTTagCompound readFromNBTTagCompound(WSNBTTagCompound nbtTagCompound) {
		Object handled = SpigotHandledUtils.getHandle(entity);
		try {
			ReflectionUtils.invokeMethod(handled, "f", nbtTagCompound.getHandled());
		} catch (Exception e) {
			InternalLogger.printException(e, "An error has occurred while WetSponge was saving an Entity on a NBTTagCompound!");
		}
		return nbtTagCompound;
	}

	@Override
	public void remove() {
		entity.remove();
	}

	@Override
	public Entity getHandled() {
		return entity;
	}

	@Override
	public WSTranslation getTranslation() {
		throw new IllegalAccessError("Not supported by Spigot");
	}


	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		SpigotEntity that = (SpigotEntity) o;

		return entity.equals(that.entity);
	}

	@Override
	public int hashCode() {
		return entity.hashCode();
	}
}
