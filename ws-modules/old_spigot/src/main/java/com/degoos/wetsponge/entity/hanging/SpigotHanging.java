package com.degoos.wetsponge.entity.hanging;

import com.degoos.wetsponge.entity.SpigotEntity;
import com.degoos.wetsponge.enums.block.EnumBlockFace;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Hanging;

public class SpigotHanging extends SpigotEntity implements WSHanging {


	public SpigotHanging(Hanging entity) {
		super(entity);
	}

	@Override
	public EnumBlockFace getDirection() {
		return EnumBlockFace.valueOf(getHandled().getFacing().name());
	}

	@Override
	public void setDirection(EnumBlockFace direction) {
		getHandled().setFacingDirection(BlockFace.valueOf(direction.name()));
	}

	@Override
	public Hanging getHandled() {
		return (Hanging) super.getHandled();
	}
}
