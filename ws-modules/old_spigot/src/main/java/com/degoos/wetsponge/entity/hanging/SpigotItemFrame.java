package com.degoos.wetsponge.entity.hanging;

import com.degoos.wetsponge.enums.EnumRotation;
import com.degoos.wetsponge.item.SpigotItemStack;
import com.degoos.wetsponge.item.WSItemStack;
import org.bukkit.Material;
import org.bukkit.Rotation;
import org.bukkit.entity.ItemFrame;
import org.bukkit.inventory.ItemStack;

import java.util.Optional;

public class SpigotItemFrame extends SpigotHanging implements WSItemFrame {


    public SpigotItemFrame(ItemFrame entity) {
        super(entity);
    }

    @Override
    public Optional<WSItemStack> getItemStack() {
        if(getHandled().getItem() == null || getHandled().getItem().getType() == Material.AIR) return Optional.empty();
        return Optional.ofNullable(getHandled().getItem()).map(ItemStack::clone).map(SpigotItemStack::new);
    }

    @Override
    public void setItemStack(WSItemStack itemStack) {
        if (itemStack == null) getHandled().setItem(null);
        else getHandled().setItem(((SpigotItemStack) itemStack).getHandled().clone());
    }

    @Override
    public EnumRotation getFrameRotation() {
        return EnumRotation.getBySpigotName(getHandled().getRotation().name()).orElse(EnumRotation.TOP);
    }

    @Override
    public void setRotation(EnumRotation rotation) {
        getHandled().setRotation(Rotation.valueOf(rotation.getSpigotName()));
    }

    public ItemFrame getHandled() {
        return (ItemFrame) super.getHandled();
    }


}
