package com.degoos.wetsponge.entity.hanging;

import com.degoos.wetsponge.enums.EnumArtType;
import org.bukkit.Art;
import org.bukkit.entity.Painting;

public class SpigotPainting extends SpigotHanging implements WSPainting {


    public SpigotPainting(Painting entity) {
        super(entity);
    }

    @Override
    public EnumArtType getArt() {
        return EnumArtType.getById(getHandled().getArt().getId()).orElse(EnumArtType.KEBAB);
    }

    @Override
    public void setArt(EnumArtType art) {
        getHandled().setArt(Art.getById(art.getId()));
    }

    @Override
    public Painting getHandled() {
        return (Painting) super.getHandled();
    }


}
