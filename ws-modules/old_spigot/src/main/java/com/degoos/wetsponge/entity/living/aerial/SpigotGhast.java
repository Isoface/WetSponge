package com.degoos.wetsponge.entity.living.aerial;

import com.degoos.wetsponge.entity.WSEntity;
import com.degoos.wetsponge.entity.living.SpigotLivingEntity;
import java.util.Optional;
import org.bukkit.entity.Ghast;

public class SpigotGhast extends SpigotLivingEntity implements WSGhast {


	public SpigotGhast(Ghast entity) {
		super(entity);
	}

	@Override
	public void setAI(boolean ai) {
		getHandled().setAI(ai);
	}

	@Override
	public boolean hasAI() {
		return getHandled().hasAI();
	}

	@Override
	public Optional<WSEntity> getTarget() {
		return Optional.empty();
	}

	@Override
	public void setTarget(WSEntity entity) {

	}

	@Override
	public Ghast getHandled() {
		return (Ghast) super.getHandled();
	}
}
