package com.degoos.wetsponge.entity.living.ambient;


import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.entity.WSEntity;
import com.degoos.wetsponge.entity.living.SpigotLivingEntity;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.util.reflection.SpigotEntityUtils;
import org.bukkit.entity.Ambient;

import java.util.Optional;

public class SpigotAmbient extends SpigotLivingEntity implements WSAmbient {

	public SpigotAmbient (Ambient entity) {
		super(entity);
	}


	@Override
	public void setAI(boolean ai) {
		if (WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD))
			getHandled().setAI(ai);
		else SpigotEntityUtils.setAI(getHandled(), ai);
	}


	@Override
	public boolean hasAI() {
		if (WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD))
			return getHandled().hasAI();
		else return SpigotEntityUtils.hasAI(getHandled());
	}


	@Override
	public Optional<WSEntity> getTarget () {
		return null;
	}


	@Override
	public void setTarget (WSEntity entity) {
	}


	@Override
	public Ambient getHandled () {
		return (Ambient) super.getHandled();
	}
}
