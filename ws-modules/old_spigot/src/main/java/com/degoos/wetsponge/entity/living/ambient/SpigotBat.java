package com.degoos.wetsponge.entity.living.ambient;


import org.bukkit.entity.Bat;

public class SpigotBat extends SpigotAmbient implements WSBat {

	public SpigotBat (Bat entity) {
		super(entity);
	}


	@Override
	public boolean isAwake () {
		return getHandled().isAwake();
	}


	@Override
	public void setAwake (boolean awake) {
		getHandled().setAwake(awake);
	}


	@Override
	public Bat getHandled () {
		return (Bat) super.getHandled();
	}

}
