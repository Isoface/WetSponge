package com.degoos.wetsponge.entity.living.animal;

import org.bukkit.entity.Donkey;

public class SpigotDonkey extends SpigotAbstractHorse implements WSDonkey {

	public SpigotDonkey(Donkey entity) {
		super(entity);
	}

	@Override
	public boolean hasChest() {
		return getHandled().isCarryingChest();
	}

	@Override
	public void setChested(boolean chested) {
		getHandled().setCarryingChest(chested);
	}

	@Override
	public Donkey getHandled() {
		return (Donkey) super.getHandled();
	}
}
