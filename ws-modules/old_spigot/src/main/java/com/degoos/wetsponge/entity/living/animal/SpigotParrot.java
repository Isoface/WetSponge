package com.degoos.wetsponge.entity.living.animal;

import com.degoos.wetsponge.enums.EnumParrotVariant;
import org.bukkit.Bukkit;
import org.bukkit.entity.AnimalTamer;
import org.bukkit.entity.Parrot;

import java.util.Optional;
import java.util.UUID;

public class SpigotParrot extends SpigotAnimal implements WSParrot {


	public SpigotParrot(Parrot entity) {
		super(entity);
	}

	@Override
	public EnumParrotVariant getVariant() {
		return EnumParrotVariant.valueOf(getHandled().getVariant().name());
	}

	@Override
	public void setVariant(EnumParrotVariant variant) {
		getHandled().setVariant(Parrot.Variant.valueOf(variant.name()));
	}

	@Override
	public boolean isSitting() {
		return getHandled().isSitting();
	}

	@Override
	public void setSitting(boolean sitting) {
		getHandled().setSitting(sitting);
	}

	@Override
	public boolean isTamed() {
		return getHandled().isTamed();
	}

	@Override
	public Optional<UUID> getTamer() {
		AnimalTamer tamer = getHandled().getOwner();
		if (tamer == null) return Optional.empty();
		return Optional.of(tamer.getUniqueId());
	}

	@Override
	public void setTamer(UUID tamer) {
		getHandled().setOwner(tamer == null ? null : Bukkit.getOfflinePlayer(tamer));
	}

	@Override
	public Parrot getHandled() {
		return (Parrot) super.getHandled();
	}
}
