package com.degoos.wetsponge.entity.living.animal;


import com.degoos.wetsponge.enums.EnumRabbitType;
import org.bukkit.entity.Rabbit;

import java.util.Optional;

public class SpigotRabbit extends SpigotAnimal implements WSRabbit {


	public SpigotRabbit (Rabbit entity) {
		super(entity);
	}


	@Override
	public Optional<EnumRabbitType> getRabbitType () {
		return EnumRabbitType.getRabbitType(getHandled().getRabbitType().name());
	}


	@Override
	public void setRabbitType (EnumRabbitType rabbitType) {
		getHandled().setRabbitType(Rabbit.Type.valueOf(rabbitType.getSpigotName()));
	}


	@Override
	public Rabbit getHandled () {
		return (Rabbit) super.getHandled();
	}
}
