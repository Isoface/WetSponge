package com.degoos.wetsponge.entity.living.animal;

import org.bukkit.entity.ZombieHorse;

public class SpigotZombieHorse extends SpigotAbstractHorse implements WSSkeletonHorse {

	public SpigotZombieHorse(ZombieHorse entity) {
		super(entity);
	}

	@Override
	public ZombieHorse getHandled() {
		return (ZombieHorse) super.getHandled();
	}
}
