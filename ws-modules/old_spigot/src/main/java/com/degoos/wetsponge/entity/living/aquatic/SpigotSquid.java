package com.degoos.wetsponge.entity.living.aquatic;

import com.degoos.wetsponge.entity.WSEntity;
import com.degoos.wetsponge.entity.living.SpigotLivingEntity;
import org.bukkit.entity.Squid;

import java.util.Optional;

public class SpigotSquid extends SpigotLivingEntity implements WSSquid {


	public SpigotSquid(Squid entity) {
		super(entity);
	}

	@Override
	public void setAI(boolean ai) {
		getHandled().setAI(ai);
	}

	@Override
	public boolean hasAI() {
		return getHandled().hasAI();
	}

	@Override
	public Optional<WSEntity> getTarget() {
		return Optional.empty();
	}

	@Override
	public void setTarget(WSEntity entity) {

	}

	@Override
	public Squid getHandled() {
		return (Squid) super.getHandled();
	}
}
