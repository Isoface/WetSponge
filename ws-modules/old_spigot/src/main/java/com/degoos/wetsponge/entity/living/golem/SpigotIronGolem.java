package com.degoos.wetsponge.entity.living.golem;

import com.degoos.wetsponge.entity.living.SpigotCreature;
import org.bukkit.entity.IronGolem;

public class SpigotIronGolem extends SpigotCreature implements WSIronGolem {


    public SpigotIronGolem(IronGolem entity) {
        super(entity);
    }

    @Override
    public boolean isPlayerCreated() {
        return getHandled().isPlayerCreated();
    }

    @Override
    public void setPlayerCreated(boolean playerCreated) {
        getHandled().setPlayerCreated(playerCreated);
    }

    @Override
    public IronGolem getHandled() {
        return (IronGolem) super.getHandled();
    }


}
