package com.degoos.wetsponge.entity.living.golem;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.entity.living.SpigotCreature;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.reflection.SpigotHandledUtils;
import com.degoos.wetsponge.util.reflection.NMSUtils;
import org.bukkit.entity.Snowman;

public class SpigotSnowGolem extends SpigotCreature implements WSSnowGolem {


	public SpigotSnowGolem(Snowman entity) {
		super(entity);
	}

	@Override
	public boolean hasPumpkinEquipped() {
		if (!WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD)) return true;
		try {
			return (boolean) NMSUtils.getNMSClass("EntitySnowman").getMethod("hasPumpkin").invoke(SpigotHandledUtils.getEntityHandle(getHandled()));
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was checking if a snow golem has a pumpkin!");
			return true;
		}
	}

	@Override
	public void setPumpkinEquipped(boolean pumpkinEquipped) {
		if (!WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD)) return;
		try {
			NMSUtils.getNMSClass("EntitySnowman").getMethod("setHasPumpkin", boolean.class).invoke(SpigotHandledUtils.getEntityHandle(getHandled()), pumpkinEquipped);
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was setting whether a snow golem has a pumpkin!");
		}
	}

	@Override
	public Snowman getHandled() {
		return (Snowman) super.getHandled();
	}
}
