package com.degoos.wetsponge.entity.living.monster;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.reflection.SpigotHandledUtils;
import com.degoos.wetsponge.util.reflection.NMSUtils;
import com.degoos.wetsponge.util.reflection.ReflectionUtils;
import java.lang.reflect.Field;

import org.bukkit.entity.Creeper;

public class SpigotCreeper extends SpigotMonster implements WSCreeper {

	private static Field[] fields = NMSUtils.getNMSClass("EntityCreeper").getDeclaredFields();
	private Object handled;

	public SpigotCreeper(Creeper entity) {
		super(entity);
		handled = SpigotHandledUtils.getEntityHandle(entity);
	}

	@Override
	public boolean isPowered() {
		return getHandled().isPowered();
	}

	@Override
	public void setPowered(boolean powered) {
		getHandled().setPowered(powered);
	}

	@Override
	public int getFuseDuration() {
		try {
			return ReflectionUtils.setAccessible(fields[2]).getInt(handled);
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was getting the fuse duration of a creeper!");
			return 0;
		}
	}

	@Override
	public void setFuseDuration(int fuseDuration) {
		try {
			ReflectionUtils.setAccessible(fields[2]).setInt(handled, fuseDuration);
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was setting the fuse duration of a creeper!");
		}
	}

	@Override
	public int getTicksRemaining() {
		try {
			return getFuseDuration() - ReflectionUtils.setAccessible(fields[1]).getInt(handled);
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was getting the remaining ticks of a creeper!");
			return 0;
		}
	}

	@Override
	public void setTicksRemaining(int ticksRemaining) {
		try {
			ReflectionUtils.setAccessible(fields[1]).setInt(handled, getFuseDuration() - ticksRemaining);
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was setting the remaining ticks of a creeper!");
		}
	}

	@Override
	public boolean isPrimed() {
		try {
			return (boolean) ReflectionUtils.invokeMethod(handled, WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD) ? "isIgnited" : "cn");
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was checking if a creeper is primed!");
			return false;
		}
	}

	@Override
	public void prime() {
		try {
			ReflectionUtils.invokeMethod(handled, WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD) ? "co" : "do_");
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was priming a creeper!");
			return;
		}
	}

	@Override
	public void defuse() {
	}

	@Override
	public int setExplosionRadius() {
		try {
			return ReflectionUtils.setAccessible(fields[3]).getInt(handled);
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was getting the explosion radius of a creeper!");
			return 0;
		}
	}

	@Override
	public void setExplosionRadius(int explosionRadius) {
		try {
			ReflectionUtils.setAccessible(fields[3]).setInt(handled, explosionRadius);
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was setting the explosion radius of a creeper!");
		}
	}

	@Override
	public void detonate() {
		try {
			ReflectionUtils.invokeMethod(handled, WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD) ? "cr" : "dr");
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was detonating a creeper!");
			return;
		}
	}

	@Override
	public Creeper getHandled() {
		return (Creeper) super.getHandled();
	}
}
