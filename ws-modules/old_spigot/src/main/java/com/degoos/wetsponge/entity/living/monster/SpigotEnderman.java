package com.degoos.wetsponge.entity.living.monster;

import com.degoos.wetsponge.material.SpigotMaterial;
import com.degoos.wetsponge.material.WSBlockTypes;
import com.degoos.wetsponge.material.block.SpigotBlockType;
import com.degoos.wetsponge.material.block.WSBlockType;
import org.bukkit.entity.Enderman;
import org.bukkit.material.MaterialData;

import java.util.Optional;

public class SpigotEnderman extends SpigotMonster implements WSEnderman {


	public SpigotEnderman(Enderman entity) {
		super(entity);
	}

	@Override
	public Optional<WSBlockType> getCarriedBlock() {
		MaterialData materialData = getHandled().getCarriedMaterial();
		if (materialData == null) return Optional.empty();
		WSBlockType material = WSBlockTypes.getById(materialData.getItemTypeId()).orElse(null);
		((SpigotMaterial) material).readMaterialData(materialData);
		if (material == null) material = new SpigotBlockType(materialData.getItemTypeId(), "", "", 64);
		return Optional.of(material);
	}

	@Override
	public void setCarriedBlock(WSBlockType carriedMaterial) {
		if (carriedMaterial == null) getHandled().setCarriedMaterial(null);
		else getHandled().setCarriedMaterial(((SpigotMaterial) carriedMaterial).toMaterialData());
	}

	@Override
	public Enderman getHandled() {
		return (Enderman) super.getHandled();
	}


}
