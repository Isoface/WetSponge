package com.degoos.wetsponge.entity.living.monster;

import org.bukkit.entity.Husk;

public class SpigotHusk extends SpigotZombie implements WSHusk {

	public SpigotHusk(Husk entity) {
		super(entity);
	}

	@Override
	public Husk getHandled() {
		return (Husk) super.getHandled();
	}
}
