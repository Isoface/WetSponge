package com.degoos.wetsponge.entity.living.monster;


import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.reflection.ReflectionUtils;
import com.degoos.wetsponge.util.reflection.SpigotHandledUtils;
import org.bukkit.entity.Spider;

public class SpigotSpider extends SpigotMonster implements WSSpider {


	public SpigotSpider(Spider entity) {
		super(entity);
	}


	@Override
	public boolean isClimbing() {
		Object handled = SpigotHandledUtils.getEntityHandle(getHandled());

		try {
			return (boolean) ReflectionUtils.invokeMethod(handled, WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD) ? "p" : "n");
		} catch (Exception e) {
			InternalLogger.printException(e, "An exception has occurred while WetSponge was checking if a spigot was climbing!");
			return false;
		}
	}

	@Override
	public void setClimbing(boolean climbing) {
		Object handled = SpigotHandledUtils.getEntityHandle(getHandled());
		try {
			ReflectionUtils.invokeMethod(handled, "a", climbing);
		} catch (Exception e) {
			InternalLogger.printException(e, "An exception has occurred while WetSponge was setting whether a spigot was climbing!");
		}
	}

	@Override
	public Spider getHandled() {
		return (Spider) super.getHandled();
	}
}
