package com.degoos.wetsponge.entity.living.monster;

import org.bukkit.entity.Stray;

public class SpigotStray extends SpigotSkeleton implements WSStray {

	public SpigotStray(Stray entity) {
		super(entity);
	}

	@Override
	public Stray getHandled() {
		return (Stray) super.getHandled();
	}
}
