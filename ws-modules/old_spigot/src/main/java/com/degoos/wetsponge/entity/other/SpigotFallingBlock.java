package com.degoos.wetsponge.entity.other;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.entity.SpigotEntity;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.material.WSBlockTypes;
import com.degoos.wetsponge.material.block.SpigotBlockType;
import com.degoos.wetsponge.material.block.WSBlockType;
import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.Validate;
import com.degoos.wetsponge.util.reflection.NMSUtils;
import com.degoos.wetsponge.util.reflection.ReflectionUtils;
import com.degoos.wetsponge.util.reflection.SpigotHandledUtils;
import org.bukkit.entity.FallingBlock;

public class SpigotFallingBlock extends SpigotEntity implements WSFallingBlock {

	private static Class<?> NMS_CLASS = NMSUtils.getNMSClass("EntityFallingBlock");

	public SpigotFallingBlock(FallingBlock entity) {
		super(entity);
	}

	@Override
	public WSBlockType getBlockType() {
		return ((SpigotBlockType) WSBlockTypes.getById(getHandled().getMaterial().getId())
				.orElse(WSBlockTypes.AIR.getDefaultState())).readMaterialData(getHandled().getMaterial().getNewData(getHandled().getBlockData()));
	}

	@Override
	public void setBlockType(WSBlockType blockType) {
		Validate.notNull(blockType, "Block type cannot be null!");
		try {
			ReflectionUtils
					.setFirstObject(NMS_CLASS, NMSUtils.getNMSClass("IBlockData"), SpigotHandledUtils.getEntityHandle(getHandled()), SpigotHandledUtils.getBlockState(blockType));
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was modifying a FallingBlock!");
		}
	}

	@Override
	public double getFallDamagePerBlock() {
		try {
			return (double) ReflectionUtils.getObject(SpigotHandledUtils.getEntityHandle(getHandled()), "fallHurtAmount");
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was modifying a FallingBlock!");
			return 0;
		}
	}

	@Override
	public void setFallDamagerPerBlock(double fallDamagerPerBlock) {
		try {
			ReflectionUtils.setAccessible(ReflectionUtils.getField(NMS_CLASS, "fallHurtAmount"))
					.setDouble(SpigotHandledUtils.getEntityHandle(getHandled()), fallDamagerPerBlock);
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was modifying a FallingBlock!");
		}
	}

	@Override
	public double getMaxFallDamage() {
		try {
			return (double) ReflectionUtils.getObject(SpigotHandledUtils.getEntityHandle(getHandled()), "fallHurtMax");
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was modifying a FallingBlock!");
			return 0;
		}
	}

	@Override
	public void setMaxFallDamage(double maxFallDamage) {
		try {
			ReflectionUtils.setAccessible(ReflectionUtils.getField(NMS_CLASS, "fallHurtMax")).setDouble(SpigotHandledUtils.getEntityHandle(getHandled()), maxFallDamage);
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was modifying a FallingBlock!");
		}
	}

	@Override
	public boolean canPlaceAsBlock() {
		try {
			return (boolean) ReflectionUtils
					.getObject(SpigotHandledUtils.getEntityHandle(getHandled()), WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD) ? "f" : "e");
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was modifying a FallingBlock!");
			return false;
		}
	}

	@Override
	public void setCanPlaceAsBlock(boolean canPlaceAsBlock) {
		try {
			ReflectionUtils.setAccessible(ReflectionUtils.getField(NMS_CLASS, WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD) ? "f" : "e"))
					.setBoolean(SpigotHandledUtils.getEntityHandle(getHandled()), canPlaceAsBlock);
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was modifying a FallingBlock!");
		}
	}

	@Override
	public boolean canDropAsItem() {
		return getHandled().getDropItem();
	}

	@Override
	public void setCanDropAsItem(boolean canDropAsItem) {
		getHandled().setDropItem(canDropAsItem);
	}

	@Override
	public int getFallTime() {
		try {
			return (int) ReflectionUtils.getObject(SpigotHandledUtils.getEntityHandle(getHandled()), "ticksLived");
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was modifying a FallingBlock!");
			return 0;
		}
	}

	@Override
	public void setFallTime(int fallTime) {
		try {
			ReflectionUtils.setAccessible(ReflectionUtils.getField(NMS_CLASS, "ticksLived")).setInt(SpigotHandledUtils.getEntityHandle(getHandled()), fallTime);
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was modifying a FallingBlock!");
		}
	}

	@Override
	public boolean canHurtEntities() {
		return getHandled().canHurtEntities();
	}

	@Override
	public void setCanHurtEntities(boolean canHurtEntities) {
		getHandled().setHurtEntities(canHurtEntities);
	}

	@Override
	public FallingBlock getHandled() {
		return (FallingBlock) super.getHandled();
	}

}
