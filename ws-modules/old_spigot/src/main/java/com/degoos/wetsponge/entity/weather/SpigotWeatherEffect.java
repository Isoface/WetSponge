package com.degoos.wetsponge.entity.weather;

import com.degoos.wetsponge.entity.SpigotEntity;
import org.bukkit.entity.Weather;

public class SpigotWeatherEffect extends SpigotEntity implements WSWeatherEffect {

	public SpigotWeatherEffect(Weather entity) {
		super(entity);
	}

	@Override
	public Weather getHandled() {
		return (Weather) super.getHandled();
	}
}
