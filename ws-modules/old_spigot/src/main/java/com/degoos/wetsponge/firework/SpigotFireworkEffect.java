package com.degoos.wetsponge.firework;

import com.degoos.wetsponge.color.WSColor;
import com.degoos.wetsponge.enums.EnumFireworkShape;
import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.ListUtils;
import com.degoos.wetsponge.util.Validate;
import org.bukkit.Color;
import org.bukkit.FireworkEffect;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class SpigotFireworkEffect implements WSFireworkEffect {

    public static WSFireworkEffect.Builder builder() {
        return new Builder(FireworkEffect.builder());
    }

    private FireworkEffect effect;

    public SpigotFireworkEffect(FireworkEffect effect) {
        Validate.notNull(effect, "Effect cannot be null!");
        this.effect = effect;
    }

    @Override
    public boolean flickers() {
        return effect.hasFlicker();
    }

    @Override
    public boolean hasTrail() {
        return effect.hasTrail();
    }

    @Override
    public List<WSColor> getColors() {
        return effect.getColors().stream().map(color -> WSColor.ofRGB(color.asRGB())).collect(Collectors.toList());
    }

    @Override
    public List<WSColor> getFadeColors() {
        return effect.getFadeColors().stream().map(color -> WSColor.ofRGB(color.asRGB())).collect(Collectors.toList());
    }

    @Override
    public EnumFireworkShape getShape() {
        Optional<EnumFireworkShape> optional = EnumFireworkShape.getBySpigotName(effect.getType().name());
        if (!optional.isPresent()) {
            InternalLogger.sendError("Cannot found EnumFireworkShape for "
                    + effect.getType().name() + ". Using the default one: " + EnumFireworkShape.BALL + ".");
            return EnumFireworkShape.BALL;
        }
        return optional.get();
    }

    @Override
    public Builder toBuilder() {
        return new Builder(FireworkEffect.builder()
                .flicker(effect.hasFlicker()).trail(effect.hasTrail()).with(effect.getType())
                .withColor(effect.getColors()).withFade(effect.getFadeColors()));
    }

    @Override
    public WSFireworkEffect clone() {
        return toBuilder().build();
    }

    @Override
    public FireworkEffect getHandled() {
        return effect;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SpigotFireworkEffect that = (SpigotFireworkEffect) o;

        return effect.equals(that.effect);
    }

    @Override
    public int hashCode() {
        return effect.hashCode();
    }

    protected static class Builder implements WSFireworkEffect.Builder {

        private FireworkEffect.Builder builder;

        public Builder(FireworkEffect.Builder builder) {
            this.builder = builder;
        }

        @Override
        public WSFireworkEffect.Builder trail(boolean trail) {
            builder.trail(trail);
            return this;
        }

        @Override
        public WSFireworkEffect.Builder flicker(boolean flicker) {
            builder.flicker(flicker);
            return this;
        }

        @Override
        public WSFireworkEffect.Builder color(WSColor color) {
            builder.withColor(Color.fromRGB(color.toRGB()));
            return this;
        }

        @Override
        public WSFireworkEffect.Builder colors(WSColor... colors) {
            builder.withColor(ListUtils.toArray(Color.class, Arrays.stream(colors).map(color -> Color.fromRGB(color.toRGB())).collect(Collectors.toList())));
            return this;
        }

        @Override
        public WSFireworkEffect.Builder colors(Collection<WSColor> colors) {
            builder.withColor(colors.stream().map(color -> Color.fromRGB(color.toRGB())).collect(Collectors.toList()));
            return this;
        }

        @Override
        public WSFireworkEffect.Builder fade(WSColor color) {
            builder.withFade(Color.fromRGB(color.toRGB()));
            return this;
        }

        @Override
        public WSFireworkEffect.Builder fades(WSColor... colors) {
            builder.withFade(ListUtils.toArray(Color.class, Arrays.stream(colors).map(color -> Color.fromRGB(color.toRGB())).collect(Collectors.toList())));
            return this;
        }

        @Override
        public WSFireworkEffect.Builder fades(Collection<WSColor> colors) {
            builder.withFade(colors.stream().map(color -> Color.fromRGB(color.toRGB())).collect(Collectors.toList()));
            return this;
        }

        @Override
        public WSFireworkEffect.Builder shape(EnumFireworkShape shape) {
            try {
                FireworkEffect.Type type = FireworkEffect.Type.valueOf(shape.getSpigotName());
                builder.with(type);
            } catch (Throwable ex) {
                InternalLogger.sendError("Cannot found FireworkShape (Sponge) for "
                        + shape + ". Using the default one: " + FireworkEffect.Type.BALL.name() + ".");
                builder.with(FireworkEffect.Type.BALL);
            }
            return this;
        }

        @Override
        public WSFireworkEffect build() {
            return new SpigotFireworkEffect(builder.build());
        }
    }
}
