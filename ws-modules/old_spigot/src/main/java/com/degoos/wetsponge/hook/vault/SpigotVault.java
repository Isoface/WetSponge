package com.degoos.wetsponge.hook.vault;

import com.degoos.wetsponge.enums.EnumServerType;
import java.util.Optional;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.Bukkit;
import org.bukkit.plugin.RegisteredServiceProvider;

public class SpigotVault extends WSVault {

	private WSVaultEconomy economy;

	public SpigotVault() {
		super(EnumServerType.SPIGOT, EnumServerType.PAPER_SPIGOT);
		RegisteredServiceProvider<Economy> provider = Bukkit.getServer().getServicesManager().getRegistration(Economy.class);
		Economy rawEconomy = provider == null ? null : provider.getProvider();
		economy = rawEconomy == null ? null : new SpigotVaultEconomy(rawEconomy);
	}

	@Override
	public void onUnload() {
		economy = null;
	}

	@Override
	public Optional<WSVaultEconomy> getEconomy() {
		return Optional.ofNullable(economy);
	}
}
