package com.degoos.wetsponge.item;


import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.item.enchantment.SpigotEnchantment;
import com.degoos.wetsponge.item.enchantment.WSEnchantment;
import com.degoos.wetsponge.material.SpigotMaterial;
import com.degoos.wetsponge.material.WSMaterial;
import com.degoos.wetsponge.material.item.SpigotItemType;
import com.degoos.wetsponge.material.item.type.WSItemTypeDamageable;
import com.degoos.wetsponge.material.item.type.WSItemTypeMap;
import com.degoos.wetsponge.nbt.SpigotNBTTagCompound;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;
import com.degoos.wetsponge.resource.spigot.SpigotSkullBuilder;
import com.degoos.wetsponge.text.SpigotText;
import com.degoos.wetsponge.text.WSText;
import com.degoos.wetsponge.text.translation.SpigotTranslation;
import com.degoos.wetsponge.text.translation.WSTranslation;
import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.Validate;
import com.degoos.wetsponge.util.reflection.NMSUtils;
import com.degoos.wetsponge.util.reflection.ReflectionUtils;
import com.degoos.wetsponge.util.reflection.SpigotItemStackUtils;
import org.bukkit.Material;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.material.MaterialData;

import java.util.*;
import java.util.stream.Collectors;

public class SpigotItemStack implements WSItemStack {

	private ItemStack itemStack;
	private WSText displayName;
	private List<WSText> lore;
	private WSMaterial material;
	private int quantity;
	private boolean unbreakable, hideEnchantments, hideAttributes, hideUnbreakable, hideCanDestroy, hideCanBePlacedOn, hidePotionEffects;
	private WSTranslation translation;

	private static final Class<?> ITEM_STACK_CLASS = NMSUtils.getNMSClass("ItemStack");

	public static SpigotItemStack fromFormat(String format) {
		return new SpigotItemStack(SpigotSkullBuilder.createItemStackByUnknownFormat(format));
	}

	public SpigotItemStack(ItemStack itemStack) {
		Validate.notNull(itemStack, "ItemStack cannot be null!");
		this.itemStack = itemStack;
		refresh();
	}


	public SpigotItemStack(WSMaterial material) {
		Validate.notNull(material, "Material cannot be null!");
		this.material = material;
		this.displayName = null;
		this.lore = new ArrayList<>();
		this.quantity = 1;
		update();
		/*MaterialData data = ((SpigotMaterial) material).toMaterialData();
		this.itemStack = new ItemStack(data.getItemType(), 1, data.getData());*/
	}


	public SpigotItemStack(String nbt) throws Exception {
		this(WSNBTTagCompound.of(nbt));
	}

	public SpigotItemStack(WSNBTTagCompound nbt) throws Exception {
		Validate.notNull(nbt, "NBT cannot be null!");
		if (!nbt.hasKey("id")) throw new NullPointerException("NBTTagCompound doesn't contain key id!");
		WSMaterial.getById(nbt.getString("id")).ifPresent(material -> nbt.setString("id", material.getStringId()));


		Object nmsItemStack;
		if (WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD))
			nmsItemStack = NMSUtils.getNMSClass("ItemStack").getConstructor(nbt.getHandled().getClass()).newInstance(nbt.getHandled());
		else nmsItemStack = NMSUtils.getNMSClass("ItemStack").getMethod("createStack", nbt.getHandled().getClass()).invoke(null, nbt.getHandled());

		itemStack = (ItemStack) NMSUtils.getOBCClass("inventory.CraftItemStack").getMethod("asBukkitCopy", ITEM_STACK_CLASS).invoke(null, nmsItemStack);
		refresh();
	}


	public static WSItemStack of(WSMaterial material) {
		return new SpigotItemStack(material);
	}


	@Override
	public SpigotText getDisplayName() {
		return (SpigotText) displayName;
	}


	@Override
	public SpigotItemStack setDisplayName(WSText displayName) {
		this.displayName = displayName;
		return this;
	}


	@Override
	public List<WSText> getLore() {
		return lore;
	}


	@Override
	public SpigotItemStack setLore(List<WSText> lore) {
		this.lore = lore == null ? new ArrayList<>() : new ArrayList<>(lore);
		return this;
	}

	@Override
	public WSItemStack addLoreLine(WSText line) {
		lore.add(line == null ? WSText.empty() : line);
		return this;
	}

	@Override
	public WSItemStack clearLore() {
		lore.clear();
		return this;
	}

	@Override
	public Map<WSEnchantment, Integer> getEnchantments() {
		Map<WSEnchantment, Integer> newMap = new HashMap<>();
		getHandled().getEnchantments().forEach((enchantment, level) -> newMap.put(new SpigotEnchantment(enchantment), level));
		return newMap;
	}

	@Override
	public Optional<Integer> getEnchantmentLevel(WSEnchantment enchantment) {
		if (!containsEnchantment(enchantment)) return Optional.empty();
		return Optional.of(getHandled().getEnchantmentLevel(((SpigotEnchantment) enchantment).getHandled()));
	}

	@Override
	public boolean containsEnchantment(WSEnchantment enchantment) {
		return getHandled().containsEnchantment(((SpigotEnchantment) enchantment).getHandled());
	}

	@Override
	public WSItemStack addEnchantment(WSEnchantment enchantment, int level) {
		getHandled().addUnsafeEnchantment(((SpigotEnchantment) enchantment).getHandled(), level);
		return this;
	}

	@Override
	public WSItemStack removeEnchantment(WSEnchantment enchantment) {
		getHandled().removeEnchantment(((SpigotEnchantment) enchantment).getHandled());
		return this;
	}

	@Override
	public WSItemStack clearEnchantments() {
		getHandled().getEnchantments().forEach((enchantment, integer) -> getHandled().removeEnchantment(enchantment));
		return this;
	}


	public WSMaterial getMaterial() {
		return material;
	}

	@Override
	public WSItemStack setMaterial(WSMaterial material) {
		this.material = material;
		return this;
	}


	@Override
	public int getQuantity() {
		return quantity;
	}


	@Override
	public SpigotItemStack setQuantity(int quantity) {
		this.quantity = quantity;
		return this;
	}

	@Override
	public boolean isUnbreakable() {
		return unbreakable;
	}

	@Override
	public WSItemStack setUnbreakable(boolean unbreakable) {
		this.unbreakable = unbreakable;
		return this;
	}

	@Override
	public boolean isHidingEnchantments() {
		return hideEnchantments;
	}

	@Override
	public WSItemStack hideEnchantments(boolean hideEnchantments) {
		this.hideEnchantments = hideEnchantments;
		return this;
	}

	@Override
	public boolean isHidingAttributes() {
		return hideAttributes;
	}

	@Override
	public WSItemStack hideAttributes(boolean hideAttributes) {
		this.hideAttributes = hideAttributes;
		return this;
	}

	@Override
	public boolean isHidingUnbreakable() {
		return hideUnbreakable;
	}

	@Override
	public WSItemStack hideUnbreakable(boolean hideUnbreakable) {
		this.hideUnbreakable = hideUnbreakable;
		return this;
	}

	@Override
	public boolean isHidingCanDestroy() {
		return hideCanDestroy;
	}

	@Override
	public WSItemStack hideCanDestroy(boolean hideCanDestroy) {
		this.hideCanDestroy = hideCanDestroy;
		return this;
	}

	@Override
	public boolean isHidingCanBePlacedOn() {
		return hideCanBePlacedOn;
	}

	@Override
	public WSItemStack hideCanBePlacedOn(boolean hideCanBePlacedOn) {
		this.hideCanBePlacedOn = hideCanBePlacedOn;
		return this;
	}

	@Override
	public boolean isHidingPotionEffects() {
		return hidePotionEffects;
	}

	@Override
	public WSItemStack hidePotionEffects(boolean hidePotionEffects) {
		this.hidePotionEffects = hidePotionEffects;
		return this;
	}

	@Override
	public WSNBTTagCompound toNBTTag() {
		if (material.getStringId().equals("minecraft:air")) return WSNBTTagCompound.of();
		try {
			Object nbtTag = NMSUtils.getNMSClass("NBTTagCompound").newInstance();
			Object nmsItemStack = NMSUtils.getOBCClass("inventory.CraftItemStack").getMethod("asNMSCopy", ItemStack.class).invoke(null, itemStack);
			ReflectionUtils.getMethod(ITEM_STACK_CLASS, "save", nbtTag.getClass()).invoke(nmsItemStack, nbtTag);
			return new SpigotNBTTagCompound(nbtTag);
		} catch (Throwable ex) {
			ex.printStackTrace();
			return WSNBTTagCompound.of();
		}
	}

	@Override
	public String toSerializedNBTTag() {
		return toNBTTag().toString();
	}


	@Override
	public SpigotItemStack update() {
		WSNBTTagCompound compound = WSNBTTagCompound.of();
		compound.setInteger("Count", quantity);
		compound.setString("id", material.getStringId());

		WSNBTTagCompound tag = WSNBTTagCompound.of();

		((SpigotMaterial) material).writeNBTTag(tag);

		compound.setTag("tag", tag);


		try {
			Class<?> clazz = NMSUtils.getOBCClass("inventory.CraftItemStack");
			if (itemStack != null && clazz.isAssignableFrom(itemStack.getClass())) {
				Object handle = ReflectionUtils.setAccessible(clazz.getDeclaredField("handle")).get(itemStack);
				if (handle == null) return this;
				handle.getClass().getMethod(WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD) ? "load" : "c",
						compound.getHandled().getClass()).invoke(handle, compound.getHandled());
			} else {
				Object nmsItemStack;
				if (WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD))
					nmsItemStack = NMSUtils.getNMSClass("ItemStack").getConstructor(compound.getHandled().getClass()).newInstance(compound.getHandled());
				else nmsItemStack = NMSUtils.getNMSClass("ItemStack").getMethod("createStack",
						compound.getHandled().getClass()).invoke(null, compound.getHandled());

				itemStack = (ItemStack) NMSUtils.getOBCClass("inventory.CraftItemStack")
						.getMethod("asBukkitCopy", ITEM_STACK_CLASS).invoke(null, nmsItemStack);
			}
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was creating an ItemStack!");
		}

		if (material instanceof WSItemTypeDamageable)
			itemStack.setDurability((short) ((WSItemTypeDamageable) material).getDamage());
		else if (material instanceof WSItemTypeMap)
			itemStack.setDurability((short) ((WSItemTypeMap) material).getMapId());

		if (!itemStack.getType().equals(Material.AIR)) {
			ItemMeta meta = itemStack.getItemMeta();
			meta.setDisplayName(displayName == null ? null : displayName.toFormattingText());
			meta.setLore(lore.stream().map(WSText::toFormattingText).collect(Collectors.toList()));

			try {
				meta.spigot().setUnbreakable(unbreakable);
			} catch (Exception ignore) {
			}

			meta.getItemFlags().forEach(meta::removeItemFlags);
			if (hideEnchantments) meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
			if (hideAttributes) meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
			if (hideUnbreakable) meta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
			if (hideCanDestroy) meta.addItemFlags(ItemFlag.HIDE_DESTROYS);
			if (hideCanBePlacedOn) meta.addItemFlags(ItemFlag.HIDE_PLACED_ON);
			if (hidePotionEffects) meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);

			((SpigotMaterial) material).writeItemMeta(meta);

			itemStack.setItemMeta(meta);
		}
		itemStack.setAmount(quantity);
		try {
			translation = new SpigotTranslation(SpigotItemStackUtils.getDisplayName(itemStack));
		} catch (Exception ignore) {
		}
		return this;
	}


	@Override
	public SpigotItemStack refresh() {
		Optional<? extends WSMaterial> optional = WSMaterial.getById(itemStack.getTypeId());
		this.material = optional.isPresent() ? optional.get() : new SpigotItemType(itemStack.getTypeId(), "unknown", "unknown", itemStack.getMaxStackSize());

		((SpigotMaterial) material).readMaterialData(new MaterialData(itemStack.getType(), (byte) itemStack.getDurability()));

		if (material instanceof WSItemTypeDamageable)
			((WSItemTypeDamageable) material).setDamage(itemStack.getDurability());
		else if (material instanceof WSItemTypeMap)
			((WSItemTypeMap) material).setMapId(itemStack.getDurability());

		this.quantity = itemStack.getAmount();

		((SpigotMaterial) material).readNBTTag(toNBTTag());

		if (itemStack.hasItemMeta()) {
			ItemMeta meta = itemStack.getItemMeta();
			this.displayName = meta.hasDisplayName() ? WSText.getByFormattingText(meta.getDisplayName()) : null;
			this.lore = meta.hasLore() ? meta.getLore().stream().map(SpigotText::getByFormattingText).collect(Collectors.toList()) : new ArrayList<>();
			try {
				this.unbreakable = meta.spigot().isUnbreakable();
			} catch (Exception ex) {
				this.unbreakable = false;
			}

			this.hideEnchantments = meta.hasItemFlag(ItemFlag.HIDE_ENCHANTS);
			this.hideAttributes = meta.hasItemFlag(ItemFlag.HIDE_ATTRIBUTES);
			this.hideUnbreakable = meta.hasItemFlag(ItemFlag.HIDE_UNBREAKABLE);
			this.hideCanDestroy = meta.hasItemFlag(ItemFlag.HIDE_DESTROYS);
			this.hideCanBePlacedOn = meta.hasItemFlag(ItemFlag.HIDE_PLACED_ON);
			this.hidePotionEffects = meta.hasItemFlag(ItemFlag.HIDE_POTION_EFFECTS);

			((SpigotMaterial) material).readItemMeta(meta);

		} else {
			this.displayName = null;
			this.lore = new ArrayList<>();
			this.unbreakable = false;
			this.hideEnchantments = false;
			this.hideAttributes = false;
			this.hideUnbreakable = false;
			this.hideCanDestroy = false;
			this.hideCanBePlacedOn = false;
			this.hidePotionEffects = false;
		}
		try {
			translation = new SpigotTranslation(SpigotItemStackUtils.getDisplayName(itemStack));
		} catch (Exception ignore) {
		}
		return this;
	}


	@Override
	public WSItemStack clone() {
		return new SpigotItemStack(itemStack.clone());
	}

	@Override
	public boolean isSimilar(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		SpigotItemStack that = (SpigotItemStack) o;

		if (unbreakable != that.unbreakable) return false;
		if (hideEnchantments != that.hideEnchantments) return false;
		if (hideAttributes != that.hideAttributes) return false;
		if (hideUnbreakable != that.hideUnbreakable) return false;
		if (hideCanDestroy != that.hideCanDestroy) return false;
		if (hideCanBePlacedOn != that.hideCanBePlacedOn) return false;
		if (hidePotionEffects != that.hidePotionEffects) return false;
		if (displayName != null ? !displayName.equals(that.displayName) : that.displayName != null) return false;
		if (lore != null ? !lore.equals(that.lore) : that.lore != null) return false;
		return material != null ? material.equals(that.material) : that.material == null;
	}

	@Override
	public WSTranslation getTranslation() {
		return translation;
	}

	@Override
	public ItemStack getHandled() {
		return itemStack;
	}


	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		SpigotItemStack that = (SpigotItemStack) o;

		if (quantity != that.quantity) return false;
		if (unbreakable != that.unbreakable) return false;
		if (hideEnchantments != that.hideEnchantments) return false;
		if (hideAttributes != that.hideAttributes) return false;
		if (hideUnbreakable != that.hideUnbreakable) return false;
		if (hideCanDestroy != that.hideCanDestroy) return false;
		if (hideCanBePlacedOn != that.hideCanBePlacedOn) return false;
		if (hidePotionEffects != that.hidePotionEffects) return false;
		if (displayName != null ? !displayName.equals(that.displayName) : that.displayName != null) return false;
		if (lore != null ? !lore.equals(that.lore) : that.lore != null) return false;
		return material != null ? material.equals(that.material) : that.material == null;
	}

	@Override
	public int hashCode() {
		int result = displayName != null ? displayName.hashCode() : 0;
		result = 31 * result + (lore != null ? lore.hashCode() : 0);
		result = 31 * result + (material != null ? material.hashCode() : 0);
		result = 31 * result + quantity;
		result = 31 * result + (unbreakable ? 1 : 0);
		result = 31 * result + (hideEnchantments ? 1 : 0);
		result = 31 * result + (hideAttributes ? 1 : 0);
		result = 31 * result + (hideUnbreakable ? 1 : 0);
		result = 31 * result + (hideCanDestroy ? 1 : 0);
		result = 31 * result + (hideCanBePlacedOn ? 1 : 0);
		result = 31 * result + (hidePotionEffects ? 1 : 0);
		return result;
	}
}
