package com.degoos.wetsponge.listener.spigot;


import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.event.world.WSChunkLoadEvent;
import com.degoos.wetsponge.event.world.WSChunkUnloadEvent;
import com.degoos.wetsponge.parser.world.WorldParser;
import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.reflection.FieldUtils;
import com.degoos.wetsponge.util.reflection.NMSUtils;
import com.degoos.wetsponge.world.SpigotChunk;
import com.degoos.wetsponge.world.SpigotLocation;
import com.degoos.wetsponge.world.SpigotWorld;
import com.degoos.wetsponge.world.WSWorld;
import com.degoos.wetsponge.world.generation.SpigotBlockVolume;
import com.degoos.wetsponge.world.generation.SpigotWorldGenerator;
import com.degoos.wetsponge.world.generation.WSBlockVolume;
import com.degoos.wetsponge.world.generation.WSWorldGenerator;
import com.degoos.wetsponge.world.generation.populator.WSGenerationPopulator;
import com.flowpowered.math.vector.Vector3i;
import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.World;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.world.ChunkLoadEvent;
import org.bukkit.event.world.ChunkUnloadEvent;
import org.bukkit.event.world.WorldLoadEvent;
import org.bukkit.event.world.WorldUnloadEvent;
import org.bukkit.generator.ChunkGenerator;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.*;

public class SpigotWorldListener implements Listener {

	private static Map<String, World> worlds = new HashMap<>();
	public static final List<Chunk> dontUnloadChunks = new ArrayList<>();

	public SpigotWorldListener() {
		Bukkit.getWorlds().forEach(world -> {
			injectGenerator(world);
			worlds.put(world.getName().toLowerCase(), world);
		});
	}


	public static Optional<World> getWorld(String name) {
		return worlds.keySet().stream().filter(worldName -> worldName.equalsIgnoreCase(name)).findAny().map(s -> worlds.get(s));
	}

	public static void refreshGenerator(World world, ChunkGenerator generator, boolean isNull) {
		try {
			FieldUtils.setFinal(world.getClass().getDeclaredField("generator"), generator, world);
			Object handled = world.getClass().getDeclaredMethod("getHandle").invoke(world);
			NMSUtils.getNMSClass("World").getDeclaredField("generator").set(handled, isNull ? null : generator);
			Field chunkProvider = NMSUtils.getNMSClass("World").getDeclaredField("chunkProvider");
			chunkProvider.setAccessible(true);
			Method n = WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD) ?
					WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13) ? NMSUtils.getNMSClass("WorldServer").getDeclaredMethod("n")
							: NMSUtils.getNMSClass("WorldServer").getDeclaredMethod("q") : NMSUtils.getNMSClass("WorldServer").getDeclaredMethod("k");
			n.setAccessible(true);
			chunkProvider.set(handled, n.invoke(handled));
		} catch (Throwable e1) {
			e1.printStackTrace();
		}
	}


	@EventHandler(priority = EventPriority.LOWEST)
	public void load(WorldLoadEvent e) {
		if (e.getWorld() == null) {
			InternalLogger.sendWarning("Null world in WorldLoadEvent!");
			return;
		}
		try {
			World world = e.getWorld();
			injectGenerator(world);
			worlds.put(world.getName().toLowerCase(), e.getWorld());
			if (SpigotLocation.locations.containsKey(world.getName())) SpigotLocation.locations.get(world.getName()).forEach(SpigotLocation::updateWorld);
		} catch (Throwable ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was parsing the event Spigot-WorldLoadEvent!");
		}
	}


	@EventHandler(priority = EventPriority.LOWEST)
	public void unload(WorldUnloadEvent e) {
		try {
			worlds.remove(e.getWorld().getName().toLowerCase());
		} catch (Throwable ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was parsing the event Spigot-WorldUnloadEvent!");
		}
	}


	@EventHandler(priority = EventPriority.LOWEST)
	public void onChunkLoad(ChunkLoadEvent event) {
		WetSponge.getEventManager()
				.callEvent(new WSChunkLoadEvent(WorldParser.getOrCreateWorld(event.getWorld().getName(), event.getWorld()), new SpigotChunk(event.getChunk())));
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void onChunkUnload(ChunkUnloadEvent event) {
		WSChunkUnloadEvent wetSpongeEvent = new WSChunkUnloadEvent(WorldParser.getOrCreateWorld(event.getWorld().getName(), event.getWorld()), new SpigotChunk(event
				.getChunk()), dontUnloadChunks.contains(event.getChunk()));
		WetSponge.getEventManager().callEvent(wetSpongeEvent);
		event.setCancelled(wetSpongeEvent.isCancelled());
	}


	private void injectGenerator(World world) {
		ChunkGenerator chunkGenerator = world.getGenerator();

		if (!(chunkGenerator instanceof WSWorldGenerator)) {

			SpigotWorldGenerator generator = new SpigotWorldGenerator();
			generator.setBaseGenerationPopulator(new WSGenerationPopulator() {
				private ChunkGenerator generator = chunkGenerator;


				@Override
				public void populate(WSWorld world, WSBlockVolume volume) {
					if (generator == null) return;
					SpigotBlockVolume spigotBlockVolume = (SpigotBlockVolume) volume;

					ChunkGenerator.ChunkData data = generator
							.generateChunkData(((SpigotWorld) world).getHandled(), spigotBlockVolume.getRandom(), spigotBlockVolume.getX(), spigotBlockVolume
									.getZ(), spigotBlockVolume.getBiome());


					Vector3i min = volume.getBlockMin();
					Vector3i max = volume.getBlockMax();

					for (int x = min.getX(); x < max.getX(); x++)
						for (int y = min.getY(); y <= max.getY(); y++)
							for (int z = min.getZ(); z < max.getZ(); z++)
								spigotBlockVolume.getHandled().setBlock(x, y, z, data.getTypeAndData(x, y, z));
				}
			});
			refreshGenerator(world, generator, chunkGenerator == null);
		}
	}
}
