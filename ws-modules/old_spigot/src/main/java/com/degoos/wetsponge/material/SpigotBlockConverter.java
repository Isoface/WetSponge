package com.degoos.wetsponge.material;

import com.degoos.wetsponge.enums.EnumDyeColor;
import com.degoos.wetsponge.enums.EnumInstrument;
import com.degoos.wetsponge.enums.block.*;
import com.degoos.wetsponge.material.block.*;
import com.degoos.wetsponge.material.block.type.*;

import java.util.Map;
import java.util.Set;

public class SpigotBlockConverter {

	@SuppressWarnings("unchecked")
	public static WSBlockType createWSBlockType(int numericalId, String oldStringId, String newStringId, int maxStackSize,
												Class<? extends WSBlockType> materialClass, Object[] extra) {

		if (materialClass.equals(WSBlockType.class))
			return new SpigotBlockType(numericalId, oldStringId, newStringId, maxStackSize);
		if (materialClass.equals(WSBlockTypeAgeable.class))
			return new SpigotBlockTypeAgeable(numericalId, oldStringId, newStringId, maxStackSize, (int) extra[0], (int) extra[1]);
		if (materialClass.equals(WSBlockTypeAnaloguePowerable.class))
			return new SpigotBlockTypeAnaloguePowerable(numericalId, oldStringId, newStringId, maxStackSize, (int) extra[0], (int) extra[1]);
		if (materialClass.equals(WSBlockTypeAttachable.class))
			return new SpigotBlockTypeAttachable(numericalId, oldStringId, newStringId, maxStackSize, (boolean) extra[0]);
		if (materialClass.equals(WSBlockTypeBisected.class))
			return new SpigotBlockTypeBisected(numericalId, oldStringId, newStringId, maxStackSize, (EnumBlockTypeBisectedHalf) extra[0]);
		if (materialClass.equals(WSBlockTypeDirectional.class))
			return new SpigotBlockTypeDirectional(numericalId, oldStringId, newStringId, maxStackSize, (EnumBlockFace) extra[0], (Set<EnumBlockFace>) extra[1]);
		if (materialClass.equals(WSBlockTypeDyeColored.class))
			return new SpigotBlockTypeDyeColored(numericalId, oldStringId, newStringId, maxStackSize, (EnumDyeColor) extra[0]);
		if (materialClass.equals(WSBlockTypeLevelled.class))
			return new SpigotBlockTypeLevelled(numericalId, oldStringId, newStringId, maxStackSize, (int) extra[0], (int) extra[1]);
		if (materialClass.equals(WSBlockTypeLightable.class))
			return new SpigotBlockTypeLightable(numericalId, oldStringId, newStringId, maxStackSize, (boolean) extra[0]);
		if (materialClass.equals(WSBlockTypeMultipleFacing.class))
			return new SpigotBlockTypeMultipleFacing(numericalId, oldStringId, newStringId, maxStackSize, (Set<EnumBlockFace>) extra[0], (Set<EnumBlockFace>) extra[1]);
		if (materialClass.equals(WSBlockTypeOpenable.class))
			return new SpigotBlockTypeOpenable(numericalId, oldStringId, newStringId, maxStackSize, (boolean) extra[0]);
		if (materialClass.equals(WSBlockTypeOrientable.class))
			return new SpigotBlockTypeOrientable(numericalId, oldStringId, newStringId, maxStackSize, (EnumAxis) extra[0], (Set<EnumAxis>) extra[1]);
		if (materialClass.equals(WSBlockTypePowerable.class))
			return new SpigotBlockTypePowerable(numericalId, oldStringId, newStringId, maxStackSize, (boolean) extra[0]);
		if (materialClass.equals(WSBlockTypeRail.class))
			return new SpigotBlockTypeRail(numericalId, oldStringId, newStringId, maxStackSize, (EnumBlockTypeRailShape) extra[0], (Set<EnumBlockTypeRailShape>) extra[1]);
		if (materialClass.equals(WSBlockTypeRotatable.class))
			return new SpigotBlockTypeRotatable(numericalId, oldStringId, newStringId, maxStackSize, (EnumBlockFace) extra[0]);
		if (materialClass.equals(WSBlockTypeSnowable.class))
			return new SpigotBlockTypeSnowable(numericalId, oldStringId, newStringId, maxStackSize, (boolean) extra[0]);
		if (materialClass.equals(WSBlockTypeWaterlogged.class))
			return new SpigotBlockTypeWaterlogged(numericalId, oldStringId, newStringId, maxStackSize, (boolean) extra[0]);

		if (materialClass.equals(WSBlockTypeAnvil.class))
			return new SpigotBlockTypeAnvil((EnumBlockFace) extra[0], (Set<EnumBlockFace>) extra[1], (EnumBlockTypeAnvilDamage) extra[2]);
		if (materialClass.equals(WSBlockTypeBed.class))
			return new SpigotBlockTypeBed((EnumBlockFace) extra[0], (Set<EnumBlockFace>) extra[1], (EnumBlockTypeBedPart) extra[2], (boolean) extra[3], (EnumDyeColor) extra[4]);
		if (materialClass.equals(WSBlockTypeBrewingStand.class))
			return new SpigotBlockTypeBrewingStand((Set<Integer>) extra[0], (int) extra[1]);
		if (materialClass.equals(WSBlockTypeBubbleColumn.class))
			return new SpigotBlockTypeBubbleColumn((boolean) extra[0]);
		if (materialClass.equals(WSBlockTypeCake.class))
			return new SpigotBlockTypeCake((int) extra[0], (int) extra[1]);
		if (materialClass.equals(WSBlockTypeChest.class))
			return new SpigotBlockTypeChest(numericalId, oldStringId, newStringId, maxStackSize, (EnumBlockFace) extra[0], (Set<EnumBlockFace>) extra[1], (EnumBlockTypeChestType) extra[2], (boolean) extra[3]);
		if (materialClass.equals(WSBlockTypeCobblestoneWall.class))
			return new SpigotBlockTypeCobblestoneWall((Set<EnumBlockFace>) extra[0], (Set<EnumBlockFace>) extra[1], (boolean) extra[2], (boolean) extra[3]);
		if (materialClass.equals(WSBlockTypeCocoa.class))
			return new SpigotBlockTypeCocoa((EnumBlockFace) extra[0], (Set<EnumBlockFace>) extra[1], (int) extra[2], (int) extra[3]);
		if (materialClass.equals(WSBlockTypeCommandBlock.class))
			return new SpigotBlockTypeCommandBlock(numericalId, oldStringId, newStringId, maxStackSize, (EnumBlockFace) extra[0], (Set<EnumBlockFace>) extra[1], (boolean) extra[2]);
		if (materialClass.equals(WSBlockTypeComparator.class))
			return new SpigotBlockTypeComparator((EnumBlockFace) extra[0], (Set<EnumBlockFace>) extra[1], (EnumBlockTypeComparatorMode) extra[2], (boolean) extra[3]);
		if (materialClass.equals(WSBlockTypeCoralWallFan.class))
			return new SpigotBlockTypeCoralWallFan(numericalId, oldStringId, newStringId, maxStackSize, (EnumBlockFace) extra[0], (Set<EnumBlockFace>) extra[1], (boolean) extra[2]);
		if (materialClass.equals(WSBlockTypeDaylightDetector.class))
			return new SpigotBlockTypeDaylightDetector((int) extra[0], (int) extra[1], (boolean) extra[2]);
		if (materialClass.equals(WSBlockTypeDirt.class))
			return new SpigotBlockTypeDirt((EnumBlockTypeDirtType) extra[0]);
		if (materialClass.equals(WSBlockTypeDispenser.class))
			return new SpigotBlockTypeDispenser(numericalId, oldStringId, newStringId, maxStackSize, (EnumBlockFace) extra[0], (Set<EnumBlockFace>) extra[1], (boolean) extra[2]);
		if (materialClass.equals(WSBlockTypeDoor.class))
			return new SpigotBlockTypeDoor(numericalId, oldStringId, newStringId, maxStackSize, (EnumBlockFace) extra[0], (Set<EnumBlockFace>) extra[1], (EnumBlockTypeDoorHinge) extra[2], (EnumBlockTypeBisectedHalf) extra[3], (boolean) extra[4], (boolean) extra[5]);
		if (materialClass.equals(WSBlockTypeDoublePlant.class))
			return new SpigotBlockTypeDoublePlant((EnumBlockTypeBisectedHalf) extra[0], (EnumBlockTypeDoublePlantType) extra[1]);
		if (materialClass.equals(WSBlockTypeEnderchest.class))
			return new SpigotBlockTypeEnderchest((EnumBlockFace) extra[0], (Set<EnumBlockFace>) extra[1], (boolean) extra[2]);
		if (materialClass.equals(WSBlockTypeEndPortalFrame.class))
			return new SpigotBlockTypeEndPortalFrame((EnumBlockFace) extra[0], (Set<EnumBlockFace>) extra[1], (boolean) extra[2]);
		if (materialClass.equals(WSBlockTypeFarmland.class))
			return new SpigotBlockTypeFarmland((int) extra[0], (int) extra[1]);
		if (materialClass.equals(WSBlockTypeFence.class))
			return new SpigotBlockTypeFence(numericalId, oldStringId, newStringId, maxStackSize, (Set<EnumBlockFace>) extra[0], (Set<EnumBlockFace>) extra[1], (boolean) extra[2]);
		if (materialClass.equals(WSBlockTypeFire.class))
			return new SpigotBlockTypeFire((Set<EnumBlockFace>) extra[0], (Set<EnumBlockFace>) extra[1], (int) extra[2], (int) extra[3]);
		if (materialClass.equals(WSBlockTypeFlower.class))
			return new SpigotBlockTypeFlower((EnumBlockTypeFlowerType) extra[0]);
		if (materialClass.equals(WSBlockTypeFlowerPot.class))
			return new SpigotBlockTypeFlowerPot((EnumBlockTypePottedPlant) extra[0]);
		if (materialClass.equals(WSBlockTypeFurnace.class))
			return new SpigotBlockTypeFurnace((EnumBlockFace) extra[0], (Set<EnumBlockFace>) extra[1], (boolean) extra[2]);
		if (materialClass.equals(WSBlockTypeGate.class))
			return new SpigotBlockTypeGate(numericalId, oldStringId, newStringId, maxStackSize, (EnumBlockFace) extra[0], (Set<EnumBlockFace>) extra[1], (boolean) extra[2], (boolean) extra[3], (boolean) extra[4]);
		if (materialClass.equals(WSBlockTypeGlassPane.class))
			return new SpigotBlockTypeGlassPane(numericalId, oldStringId, newStringId, maxStackSize, (Set<EnumBlockFace>) extra[0], (Set<EnumBlockFace>) extra[1], (boolean) extra[2]);
		if (materialClass.equals(WSBlockTypeHopper.class))
			return new SpigotBlockTypeHopper((EnumBlockFace) extra[0], (Set<EnumBlockFace>) extra[1], (boolean) extra[2]);
		if (materialClass.equals(WSBlockTypeInfestedStone.class))
			return new SpigotBlockTypeInfestedStone((EnumBlockTypeDisguiseType) extra[0]);
		if (materialClass.equals(WSBlockTypeJukebox.class))
			return new SpigotBlockTypeJukebox((boolean) extra[0]);
		if (materialClass.equals(WSBlockTypeLadder.class))
			return new SpigotBlockTypeLadder((EnumBlockFace) extra[0], (Set<EnumBlockFace>) extra[1], (boolean) extra[2]);
		if (materialClass.equals(WSBlockTypeLava.class))
			return new SpigotBlockTypeLava((int) extra[0], (int) extra[1]);
		if (materialClass.equals(WSBlockTypeLeaves.class))
			return new SpigotBlockTypeLeaves((boolean) extra[0], (int) extra[1], (EnumWoodType) extra[2]);
		if (materialClass.equals(WSBlockTypeLog.class))
			return new SpigotBlockTypeLog((EnumAxis) extra[0], (Set<EnumAxis>) extra[1], (EnumWoodType) extra[2], (boolean) extra[3], (boolean) extra[4]);
		if (materialClass.equals(WSBlockTypeNoteBlock.class))
			return new SpigotBlockTypeNoteBlock((boolean) extra[0], (EnumInstrument) extra[1], (int) extra[2]);
		if (materialClass.equals(WSBlockTypeObserver.class))
			return new SpigotBlockTypeObserver((EnumBlockFace) extra[0], (Set<EnumBlockFace>) extra[1], (boolean) extra[2]);
		if (materialClass.equals(WSBlockTypePiston.class))
			return new SpigotBlockTypePiston(numericalId, oldStringId, newStringId, maxStackSize, (EnumBlockFace) extra[0], (Set<EnumBlockFace>) extra[1], (boolean) extra[2]);
		if (materialClass.equals(WSBlockTypePistonHead.class))
			return new SpigotBlockTypePistonHead((EnumBlockFace) extra[0], (Set<EnumBlockFace>) extra[1], (EnumBlockTypePistonType) extra[2], (boolean) extra[3]);
		if (materialClass.equals(WSBlockTypePrismarine.class))
			return new SpigotBlockTypePrismarine((EnumBlockTypePrismarineType) extra[0]);
		if (materialClass.equals(WSBlockTypeQuartz.class))
			return new SpigotBlockTypeQuartz((EnumAxis) extra[0], (Set<EnumAxis>) extra[1], (EnumBlockTypeQuartzType) extra[2]);
		if (materialClass.equals(WSBlockTypeRedstoneLamp.class))
			return new SpigotBlockTypeRedstoneLamp((boolean) extra[0]);
		if (materialClass.equals(WSBlockTypeRedstoneOre.class))
			return new SpigotBlockTypeRedstoneOre((boolean) extra[0]);
		if (materialClass.equals(WSBlockTypeRedstoneRail.class))
			return new SpigotBlockTypeRedstoneRail(numericalId, oldStringId, newStringId, maxStackSize, (EnumBlockTypeRailShape) extra[0], (Set<EnumBlockTypeRailShape>) extra[1], (boolean) extra[2]);
		if (materialClass.equals(WSBlockTypeRedstoneTorch.class))
			return new SpigotBlockTypeRedstoneTorch((EnumBlockFace) extra[0], (Set<EnumBlockFace>) extra[1], (boolean) extra[2]);
		if (materialClass.equals(WSBlockTypeRedstoneWire.class))
			return new SpigotBlockTypeRedstoneWire((int) extra[0], (int) extra[1], (Map<EnumBlockFace, EnumBlockTypeRedstoneWireConnection>) extra[2], (Set<EnumBlockFace>) extra[3]);
		if (materialClass.equals(WSBlockTypeRepeater.class))
			return new SpigotBlockTypeRepeater((EnumBlockFace) extra[0], (Set<EnumBlockFace>) extra[1], (int) extra[2], (int) extra[3], (int) extra[4], (boolean) extra[5], (boolean) extra[6]);
		if (materialClass.equals(WSBlockTypeSand.class))
			return new SpigotBlockTypeSand((EnumBlockTypeSandType) extra[0]);
		if (materialClass.equals(WSBlockTypeSandstone.class))
			return new SpigotBlockTypeSandstone((EnumBlockTypeSandType) extra[0], (EnumBlockTypeSandstoneType) extra[1]);
		if (materialClass.equals(WSBlockTypeSapling.class))
			return new SpigotBlockTypeSapling((EnumWoodType) extra[0], (int) extra[1], (int) extra[2]);
		if (materialClass.equals(WSBlockTypeSeaPickle.class))
			return new SpigotBlockTypeSeaPickle((boolean) extra[0], (int) extra[1], (int) extra[2], (int) extra[3]);
		if (materialClass.equals(WSBlockTypeSign.class))
			return new SpigotBlockTypeSign((EnumBlockFace) extra[0], (boolean) extra[1]);
		if (materialClass.equals(WSBlockTypeSkull.class))
			return new SpigotBlockTypeSkull((EnumBlockFace) extra[0], (Set<EnumBlockFace>) extra[1], (EnumBlockFace) extra[2], (EnumBlockTypeSkullType) extra[3]);
		if (materialClass.equals(WSBlockTypeSlab.class))
			return new SpigotBlockTypeSlab((boolean) extra[0], (EnumBlockTypeSlabType) extra[1], (EnumBlockTypeSlabPosition) extra[2]);
		if (materialClass.equals(WSBlockTypeSnow.class))
			return new SpigotBlockTypeSnow((int) extra[0], (int) extra[1], (int) extra[2]);
		if (materialClass.equals(WSBlockTypeSponge.class))
			return new SpigotBlockTypeSponge((boolean) extra[0]);
		if (materialClass.equals(WSBlockTypeStainedGlassPane.class))
			return new SpigotBlockTypeStainedGlassPane((Set<EnumBlockFace>) extra[0], (Set<EnumBlockFace>) extra[1], (boolean) extra[2], (EnumDyeColor) extra[3]);
		if (materialClass.equals(WSBlockTypeStairs.class))
			return new SpigotBlockTypeStairs(numericalId, oldStringId, newStringId, maxStackSize, (EnumBlockFace) extra[0], (Set<EnumBlockFace>) extra[1], (EnumBlockTypeStairShape) extra[2], (EnumBlockTypeBisectedHalf) extra[3], (boolean) extra[4]);
		if (materialClass.equals(WSBlockTypeStone.class))
			return new SpigotBlockTypeStone((EnumBlockTypeStoneType) extra[0]);
		if (materialClass.equals(WSBlockTypeStoneBrick.class))
			return new SpigotBlockTypeStoneBrick((EnumBlockTypeStoneBrickType) extra[0]);
		if (materialClass.equals(WSBlockTypeStructureBlock.class))
			return new SpigotBlockTypeStructureBlock((EnumBlockTypeStructureBlockMode) extra[0]);
		if (materialClass.equals(WSBlockTypeSwitch.class))
			return new SpigotBlockTypeSwitch(numericalId, oldStringId, newStringId, maxStackSize, (EnumBlockFace) extra[0], (Set<EnumBlockFace>) extra[1], (EnumBlockTypeSwitchFace) extra[2], (boolean) extra[3]);
		if (materialClass.equals(WSBlockTypeTallGrass.class))
			return new SpigotBlockTypeTallGrassType((EnumBlockTypeTallGrassType) extra[0]);
		if (materialClass.equals(WSBlockTypeTechnicalPiston.class))
			return new SpigotBlockTypeTechnicalPiston(numericalId, oldStringId, newStringId, maxStackSize, (EnumBlockFace) extra[0], (Set<EnumBlockFace>) extra[1], (EnumBlockTypePistonType) extra[2]);
		if (materialClass.equals(WSBlockTypeTorch.class))
			return new SpigotBlockTypeTorch(numericalId, oldStringId, newStringId, maxStackSize, (String) extra[0], (EnumBlockFace) extra[1], (Set<EnumBlockFace>) extra[2]);
		if (materialClass.equals(WSBlockTypeTrapDoor.class))
			return new SpigotBlockTypeTrapDoor(numericalId, oldStringId, newStringId, maxStackSize, (EnumBlockFace) extra[0], (Set<EnumBlockFace>) extra[1], (EnumBlockTypeBisectedHalf) extra[2], (boolean) extra[3], (boolean) extra[4], (boolean) extra[5]);
		if (materialClass.equals(WSBlockTypeTripwire.class))
			return new SpigotBlockTypeTripwire((Set<EnumBlockFace>) extra[0], (Set<EnumBlockFace>) extra[1], (boolean) extra[2], (boolean) extra[3], (boolean) extra[4]);
		if (materialClass.equals(WSBlockTypeTripwireHook.class))
			return new SpigotBlockTypeTripwireHook((EnumBlockFace) extra[0], (Set<EnumBlockFace>) extra[1], (boolean) extra[2], (boolean) extra[3]);
		if (materialClass.equals(WSBlockTypeTurtleEgg.class))
			return new SpigotBlockTypeTurtleEgg((int) extra[0], (int) extra[1], (int) extra[2], (int) extra[3], (int) extra[4]);
		if (materialClass.equals(WSBlockTypeWallSign.class))
			return new SpigotBlockTypeWallSign((EnumBlockFace) extra[0], (Set<EnumBlockFace>) extra[1], (boolean) extra[2]);
		if (materialClass.equals(WSBlockTypeWater.class))
			return new SpigotBlockTypeWater((int) extra[0], (int) extra[1]);
		if (materialClass.equals(WSBlockTypeWoodPlanks.class))
			return new SpigotBlockTypeWoodPlanks((EnumWoodType) extra[0]);

		return new SpigotBlockType(numericalId, oldStringId, newStringId, maxStackSize);
	}

}
