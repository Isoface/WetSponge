package com.degoos.wetsponge.material.block;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.material.SpigotMaterial;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;
import org.bukkit.Material;
import org.bukkit.block.BlockState;
import org.bukkit.inventory.meta.BlockStateMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.material.MaterialData;

import java.util.Objects;

public class SpigotBlockType implements SpigotMaterial, WSBlockType {


	private int numericalId;
	private String oldStringId, newStringId;
	private int maxStackSize;

	public SpigotBlockType(int numericalId, String oldStringId, String newStringId, int maxStackSize) {
		this.numericalId = numericalId < 0 ? -1 : numericalId;
		this.oldStringId = oldStringId == null || oldStringId.equals("") ? null : oldStringId;
		this.newStringId = newStringId;
		this.maxStackSize = Math.max(1, maxStackSize);
	}

	@Override
	public SpigotBlockType clone() {
		return new SpigotBlockType(numericalId, oldStringId, newStringId, maxStackSize);
	}

	@Override
	public int getNumericalId() {
		return numericalId;
	}

	@Override
	public String getStringId() {
		return WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13) ? getOldStringId() : getNewStringId();
	}

	@Override
	public String getNewStringId() {
		return newStringId;
	}

	@Override
	public String getOldStringId() {
		return oldStringId;
	}

	@Override
	public int getMaxStackSize() {
		return maxStackSize;
	}

	@Override
	public MaterialData toMaterialData() {
		return Material.getMaterial(getNumericalId()).getNewData((byte) 0);
	}

	@Override
	public SpigotBlockType readMaterialData(MaterialData materialData) {
		return this;
	}

	@Override
	public void writeItemMeta(ItemMeta itemMeta) {
		if (itemMeta instanceof BlockStateMeta) {
			BlockState state = ((BlockStateMeta) itemMeta).getBlockState();
			if (state == null) return;
			state.setData(toMaterialData());
			((BlockStateMeta) itemMeta).setBlockState(state);
		}
	}

	@Override
	public void readItemMeta(ItemMeta itemMeta) {
		if (itemMeta instanceof BlockStateMeta) {
			BlockState state = ((BlockStateMeta) itemMeta).getBlockState();
			if (state == null) return;
			MaterialData data = state.getData();
			if (data != null) readMaterialData(data);
		}
	}

	@Override
	public void writeNBTTag(WSNBTTagCompound compound) {

	}

	@Override
	public void readNBTTag(WSNBTTagCompound compound) {

	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		SpigotBlockType that = (SpigotBlockType) o;
		return numericalId == that.numericalId &&
				maxStackSize == that.maxStackSize &&
				Objects.equals(oldStringId, that.oldStringId) &&
				Objects.equals(newStringId, that.newStringId);
	}

	@Override
	public int hashCode() {

		return Objects.hash(numericalId, oldStringId, newStringId, maxStackSize);
	}
}
