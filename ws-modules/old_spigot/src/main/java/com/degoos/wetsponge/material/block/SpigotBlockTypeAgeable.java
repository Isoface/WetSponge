package com.degoos.wetsponge.material.block;

import org.bukkit.material.MaterialData;

import java.util.Objects;

public class SpigotBlockTypeAgeable extends SpigotBlockType implements WSBlockTypeAgeable {

	private int age, maximumAge;

	public SpigotBlockTypeAgeable(int numericalId, String oldStringId, String newStringId, int maxStackSize, int age, int maximumAge) {
		super(numericalId, oldStringId, newStringId, maxStackSize);
		this.age = age;
		this.maximumAge = maximumAge;
	}

	@Override
	public int getAge() {
		return age;
	}

	@Override
	public void setAge(int age) {
		this.age = Math.min(maximumAge, Math.max(0, age));
	}

	@Override
	public int getMaximumAge() {
		return maximumAge;
	}

	@Override
	public SpigotBlockTypeAgeable clone() {
		return new SpigotBlockTypeAgeable(getNumericalId(), getOldStringId(), getNewStringId(), getMaxStackSize(), age, maximumAge);
	}

	@Override
	public MaterialData toMaterialData() {
		MaterialData data = super.toMaterialData();
		data.setData((byte) age);
		return data;
	}

	@Override
	public SpigotBlockTypeAgeable readMaterialData(MaterialData materialData) {
		age = materialData.getData();
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		SpigotBlockTypeAgeable that = (SpigotBlockTypeAgeable) o;
		return age == that.age &&
				maximumAge == that.maximumAge;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), age, maximumAge);
	}
}
