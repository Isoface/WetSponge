package com.degoos.wetsponge.material.block;

import org.bukkit.material.MaterialData;

import java.util.Objects;

public class SpigotBlockTypeAnaloguePowerable extends SpigotBlockType implements WSBlockTypeAnaloguePowerable {

	private int power, maximumPower;

	public SpigotBlockTypeAnaloguePowerable(int numericalId, String oldStringId, String newStringId, int maxStackSize, int power, int maximumPower) {
		super(numericalId, oldStringId, newStringId, maxStackSize);
		this.power = power;
		this.maximumPower = maximumPower;
	}

	@Override
	public int getPower() {
		return power;
	}

	@Override
	public void setPower(int power) {
		this.power = Math.min(maximumPower, Math.max(0, power));
	}

	@Override
	public int gerMaximumPower() {
		return maximumPower;
	}

	@Override
	public SpigotBlockTypeAnaloguePowerable clone() {
		return new SpigotBlockTypeAnaloguePowerable(getNumericalId(), getOldStringId(), getNewStringId(), getMaxStackSize(), power, maximumPower);
	}


	@Override
	public MaterialData toMaterialData() {
		MaterialData data = super.toMaterialData();
		data.setData((byte) power);
		return data;
	}

	@Override
	public SpigotBlockTypeAnaloguePowerable readMaterialData(MaterialData materialData) {
		power = materialData.getData();
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		SpigotBlockTypeAnaloguePowerable that = (SpigotBlockTypeAnaloguePowerable) o;
		return power == that.power &&
				maximumPower == that.maximumPower;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), power, maximumPower);
	}
}
