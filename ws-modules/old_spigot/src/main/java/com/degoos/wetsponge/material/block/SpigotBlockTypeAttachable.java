package com.degoos.wetsponge.material.block;

import org.bukkit.material.MaterialData;

import java.util.Objects;

public class SpigotBlockTypeAttachable extends SpigotBlockType implements WSBlockTypeAttachable {

	private boolean attached;

	public SpigotBlockTypeAttachable(int numericalId, String oldStringId, String newStringId, int maxStackSize, boolean attached) {
		super(numericalId, oldStringId, newStringId, maxStackSize);
		this.attached = attached;
	}

	@Override
	public boolean isAttached() {
		return attached;
	}

	@Override
	public void setAttached(boolean attached) {
		this.attached = attached;
	}

	@Override
	public SpigotBlockTypeAttachable clone() {
		return new SpigotBlockTypeAttachable(getNumericalId(), getOldStringId(), getNewStringId(), getMaxStackSize(), attached);
	}

	@Override
	public MaterialData toMaterialData() {
		return super.toMaterialData();
	}

	@Override
	public SpigotBlockTypeAttachable readMaterialData(MaterialData materialData) {
		super.readMaterialData(materialData);
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		SpigotBlockTypeAttachable that = (SpigotBlockTypeAttachable) o;
		return attached == that.attached;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), attached);
	}
}
