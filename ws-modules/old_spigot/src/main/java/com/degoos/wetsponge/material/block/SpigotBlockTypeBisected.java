package com.degoos.wetsponge.material.block;

import com.degoos.wetsponge.enums.block.EnumBlockTypeBisectedHalf;
import com.degoos.wetsponge.util.Validate;
import org.bukkit.material.MaterialData;

import java.util.Objects;

public class SpigotBlockTypeBisected extends SpigotBlockType implements WSBlockTypeBisected {

	private EnumBlockTypeBisectedHalf half;

	public SpigotBlockTypeBisected(int numericalId, String oldStringId, String newStringId, int maxStackSize, EnumBlockTypeBisectedHalf half) {
		super(numericalId, oldStringId, newStringId, maxStackSize);
		Validate.notNull(half, "Half cannot be null!");
		this.half = half;
	}

	@Override
	public EnumBlockTypeBisectedHalf getHalf() {
		return half;
	}

	@Override
	public void setHalf(EnumBlockTypeBisectedHalf half) {
		Validate.notNull(half, "Half cannot be null!");
		this.half = half;
	}

	@Override
	public SpigotBlockTypeBisected clone() {
		return new SpigotBlockTypeBisected(getNumericalId(), getOldStringId(), getNewStringId(), getMaxStackSize(), half);
	}

	@Override
	public MaterialData toMaterialData() {
		MaterialData data = super.toMaterialData();
		data.setData(half == EnumBlockTypeBisectedHalf.TOP ? (byte) 8 : 0);
		return data;
	}

	@Override
	public SpigotBlockTypeBisected readMaterialData(MaterialData materialData) {
		half = materialData.getData() > 7 ? EnumBlockTypeBisectedHalf.TOP : EnumBlockTypeBisectedHalf.BOTTOM;
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		SpigotBlockTypeBisected that = (SpigotBlockTypeBisected) o;
		return half == that.half;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), half);
	}
}
