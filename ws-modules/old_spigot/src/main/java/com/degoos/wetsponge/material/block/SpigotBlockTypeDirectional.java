package com.degoos.wetsponge.material.block;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.util.Validate;
import org.bukkit.block.BlockFace;
import org.bukkit.material.Directional;
import org.bukkit.material.MaterialData;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class SpigotBlockTypeDirectional extends SpigotBlockType implements WSBlockTypeDirectional {

	private EnumBlockFace facing;
	private Set<EnumBlockFace> faces;

	public SpigotBlockTypeDirectional(int numericalId, String oldStringId, String newStringId, int maxStackSize, EnumBlockFace facing, Set<EnumBlockFace> faces) {
		super(numericalId, oldStringId, newStringId, maxStackSize);
		Validate.notNull(facing, "Facing cannot be null!");
		this.facing = facing;
		this.faces = faces;
	}

	@Override
	public EnumBlockFace getFacing() {
		return facing;
	}

	@Override
	public void setFacing(EnumBlockFace blockFace) {
		Validate.notNull(blockFace, "Facing cannot be null!");
		this.facing = blockFace;
	}

	@Override
	public Set<EnumBlockFace> getFaces() {
		return new HashSet<>(faces);
	}

	@Override
	public SpigotBlockTypeDirectional clone() {
		return new SpigotBlockTypeDirectional(getNumericalId(), getOldStringId(), getNewStringId(), getMaxStackSize(), facing, new HashSet<>(faces));
	}

	@Override
	public MaterialData toMaterialData() {
		MaterialData data = super.toMaterialData();
		if (data instanceof Directional) {
			((Directional) data).setFacingDirection(BlockFace.valueOf(facing.name()));
		}
		return data;
	}

	@Override
	public SpigotBlockTypeDirectional readMaterialData(MaterialData materialData) {
		if (materialData instanceof Directional) {
			facing = EnumBlockFace.valueOf(((Directional) materialData).getFacing().name());
		}
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		SpigotBlockTypeDirectional that = (SpigotBlockTypeDirectional) o;
		return facing == that.facing;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), facing);
	}
}
