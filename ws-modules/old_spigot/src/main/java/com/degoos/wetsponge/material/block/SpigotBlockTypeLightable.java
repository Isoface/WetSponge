package com.degoos.wetsponge.material.block;

import org.bukkit.material.MaterialData;

import java.util.Objects;

public class SpigotBlockTypeLightable extends SpigotBlockType implements WSBlockTypeLightable {

	private boolean lit;

	public SpigotBlockTypeLightable(int numericalId, String oldStringId, String newStringId, int maxStackSize, boolean lit) {
		super(numericalId, oldStringId, newStringId, maxStackSize);
		this.lit = lit;
	}

	@Override
	public boolean isLit() {
		return lit;
	}

	@Override
	public void setLit(boolean lit) {
		this.lit = lit;
	}

	@Override
	public SpigotBlockTypeLightable clone() {
		return new SpigotBlockTypeLightable(getNumericalId(), getOldStringId(), getNewStringId(), getMaxStackSize(), lit);
	}

	@Override
	public MaterialData toMaterialData() {
		return super.toMaterialData();
	}

	@Override
	public SpigotBlockTypeLightable readMaterialData(MaterialData materialData) {
		super.readMaterialData(materialData);
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		SpigotBlockTypeLightable that = (SpigotBlockTypeLightable) o;
		return lit == that.lit;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), lit);
	}
}
