package com.degoos.wetsponge.material.block;

import org.bukkit.material.MaterialData;

import java.util.Objects;

public class SpigotBlockTypeOpenable extends SpigotBlockType implements WSBlockTypeOpenable {

	private boolean open;

	public SpigotBlockTypeOpenable(int numericalId, String oldStringId, String newStringId, int maxStackSize, boolean open) {
		super(numericalId, oldStringId, newStringId, maxStackSize);
		this.open = open;
	}

	@Override
	public boolean isOpen() {
		return open;
	}

	@Override
	public void setOpen(boolean open) {
		this.open = open;
	}

	@Override
	public SpigotBlockTypeOpenable clone() {
		return new SpigotBlockTypeOpenable(getNumericalId(), getOldStringId(), getNewStringId(), getMaxStackSize(), open);
	}

	@Override
	public MaterialData toMaterialData() {
		return super.toMaterialData();
	}

	@Override
	public SpigotBlockTypeOpenable readMaterialData(MaterialData materialData) {
		super.readMaterialData(materialData);
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		SpigotBlockTypeOpenable that = (SpigotBlockTypeOpenable) o;
		return open == that.open;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), open);
	}
}
