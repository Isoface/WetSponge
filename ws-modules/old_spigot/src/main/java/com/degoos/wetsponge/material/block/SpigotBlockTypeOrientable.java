package com.degoos.wetsponge.material.block;

import com.degoos.wetsponge.enums.block.EnumAxis;
import com.degoos.wetsponge.util.Validate;
import org.bukkit.material.MaterialData;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class SpigotBlockTypeOrientable extends SpigotBlockType implements WSBlockTypeOrientable {

	private EnumAxis axis;
	private Set<EnumAxis> axes;

	public SpigotBlockTypeOrientable(int numericalId, String oldStringId, String newStringId, int maxStackSize, EnumAxis axis, Set<EnumAxis> axes) {
		super(numericalId, oldStringId, newStringId, maxStackSize);
		Validate.notNull(axis, "Axis cannot be null!");
		this.axis = axis;
		this.axes = axes;
	}

	@Override
	public EnumAxis getAxis() {
		return axis;
	}

	@Override
	public void setAxis(EnumAxis axis) {
		Validate.notNull(axis, "Axis cannot be null!");
		this.axis = axis;
	}

	@Override
	public Set<EnumAxis> getAxes() {
		return new HashSet<>(axes);
	}

	@Override
	public SpigotBlockTypeOrientable clone() {
		return new SpigotBlockTypeOrientable(getNumericalId(), getOldStringId(), getNewStringId(), getMaxStackSize(), axis, axes);
	}

	@Override
	public MaterialData toMaterialData() {
		return super.toMaterialData();
	}

	@Override
	public SpigotBlockTypeOrientable readMaterialData(MaterialData materialData) {
		super.readMaterialData(materialData);
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		SpigotBlockTypeOrientable that = (SpigotBlockTypeOrientable) o;
		return axis == that.axis;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), axis);
	}
}
