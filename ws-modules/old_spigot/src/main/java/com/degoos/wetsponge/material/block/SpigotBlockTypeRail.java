package com.degoos.wetsponge.material.block;

import com.degoos.wetsponge.enums.block.EnumBlockTypeRailShape;
import com.degoos.wetsponge.util.Validate;
import org.bukkit.block.BlockFace;
import org.bukkit.material.MaterialData;
import org.bukkit.material.Rails;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class SpigotBlockTypeRail extends SpigotBlockType implements WSBlockTypeRail {

	private EnumBlockTypeRailShape shape;
	private Set<EnumBlockTypeRailShape> allowedShapes;

	public SpigotBlockTypeRail(int numericalId, String oldStringId, String newStringId, int maxStackSize, EnumBlockTypeRailShape shape, Set<EnumBlockTypeRailShape> allowedShapes) {
		super(numericalId, oldStringId, newStringId, maxStackSize);
		Validate.notNull(shape, "Shape cannot be null!");
		this.shape = shape;
		this.allowedShapes = allowedShapes;
	}

	@Override
	public EnumBlockTypeRailShape getShape() {
		return shape;
	}

	@Override
	public void setShape(EnumBlockTypeRailShape shape) {
		Validate.notNull(shape, "Shape cannot be null!");
		this.shape = shape;
	}

	@Override
	public Set<EnumBlockTypeRailShape> allowedShapes() {
		return new HashSet<>(allowedShapes);
	}

	@Override
	public SpigotBlockTypeRail clone() {
		return new SpigotBlockTypeRail(getNumericalId(), getOldStringId(), getNewStringId(), getMaxStackSize(), shape, new HashSet<>(allowedShapes));
	}

	@Override
	public MaterialData toMaterialData() {

		MaterialData data = super.toMaterialData();

		if (data instanceof Rails) {
			BlockFace face;
			boolean ascending;
			switch (shape) {
				case EAST_WEST:
					face = BlockFace.EAST;
					ascending = false;
					break;
				case NORTH_EAST:
					face = BlockFace.NORTH_EAST;
					ascending = false;
					break;
				case NORTH_WEST:
					face = BlockFace.NORTH_WEST;
					ascending = false;
					break;
				case SOUTH_EAST:
					face = BlockFace.SOUTH_EAST;
					ascending = false;
					break;
				case SOUTH_WEST:
					face = BlockFace.SOUTH_WEST;
					ascending = false;
					break;
				case ASCENDING_EAST:
					face = BlockFace.EAST;
					ascending = true;
					break;
				case ASCENDING_WEST:
					face = BlockFace.WEST;
					ascending = true;
					break;
				case ASCENDING_NORTH:
					face = BlockFace.NORTH;
					ascending = true;
					break;
				case ASCENDING_SOUTH:
					face = BlockFace.SOUTH;
					ascending = true;
					break;
				default:
				case NORTH_SOUTH:
					face = BlockFace.NORTH;
					ascending = false;
					break;
			}
			((Rails) data).setDirection(face, ascending);
		}
		return data;
	}

	@Override
	public SpigotBlockTypeRail readMaterialData(MaterialData materialData) {
		if (materialData instanceof Rails) {
			if (((Rails) materialData).isOnSlope()) {
				switch (((Rails) materialData).getDirection()) {
					case SOUTH:
						shape = EnumBlockTypeRailShape.ASCENDING_SOUTH;
						break;
					case WEST:
						shape = EnumBlockTypeRailShape.ASCENDING_WEST;
						break;
					case EAST:
						shape = EnumBlockTypeRailShape.ASCENDING_EAST;
						break;
					case NORTH:
					default:
						shape = EnumBlockTypeRailShape.ASCENDING_NORTH;
						break;
				}
			} else {
				switch (((Rails) materialData).getDirection()) {
					case SOUTH_WEST:
						shape = EnumBlockTypeRailShape.SOUTH_WEST;
						break;
					case SOUTH_EAST:
						shape = EnumBlockTypeRailShape.SOUTH_EAST;
						break;
					case NORTH_WEST:
						shape = EnumBlockTypeRailShape.NORTH_WEST;
						break;
					case NORTH_EAST:
						shape = EnumBlockTypeRailShape.NORTH_EAST;
						break;
					case WEST:
					case EAST:
						shape = EnumBlockTypeRailShape.EAST_WEST;
					case SOUTH:
					case NORTH:
					default:
						shape = EnumBlockTypeRailShape.NORTH_SOUTH;
						break;
				}
			}
		}

		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		SpigotBlockTypeRail that = (SpigotBlockTypeRail) o;
		return shape == that.shape &&
				Objects.equals(allowedShapes, that.allowedShapes);
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), shape, allowedShapes);
	}
}
