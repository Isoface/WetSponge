package com.degoos.wetsponge.material.block;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.util.Validate;
import org.bukkit.material.MaterialData;

import java.util.Objects;

public class SpigotBlockTypeRotatable extends SpigotBlockType implements WSBlockTypeRotatable {

	private EnumBlockFace rotation;

	public SpigotBlockTypeRotatable(int numericalId, String oldStringId, String newStringId, int maxStackSize, EnumBlockFace rotation) {
		super(numericalId, oldStringId, newStringId, maxStackSize);
		this.rotation = rotation;
	}

	@Override
	public EnumBlockFace getRotation() {
		return rotation;
	}

	@Override
	public void setRotation(EnumBlockFace rotation) {
		Validate.notNull(rotation, "Rotation cannot be null!");
		this.rotation = rotation;
	}

	@Override
	public SpigotBlockTypeRotatable clone() {
		return new SpigotBlockTypeRotatable(getNumericalId(), getOldStringId(), getNewStringId(), getMaxStackSize(), rotation);
	}

	@Override
	public MaterialData toMaterialData() {
		return super.toMaterialData();
	}

	@Override
	public SpigotBlockTypeRotatable readMaterialData(MaterialData materialData) {
		super.readMaterialData(materialData);
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		SpigotBlockTypeRotatable that = (SpigotBlockTypeRotatable) o;
		return rotation == that.rotation;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), rotation);
	}
}
