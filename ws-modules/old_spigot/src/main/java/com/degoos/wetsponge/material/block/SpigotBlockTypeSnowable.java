package com.degoos.wetsponge.material.block;

import org.bukkit.material.MaterialData;

import java.util.Objects;

public class SpigotBlockTypeSnowable extends SpigotBlockType implements WSBlockTypeSnowable {

	public boolean snowy;

	public SpigotBlockTypeSnowable(int numericalId, String oldStringId, String newStringId, int maxStackSize, boolean snowy) {
		super(numericalId, oldStringId, newStringId, maxStackSize);
		this.snowy = snowy;
	}

	@Override
	public boolean isSnowy() {
		return snowy;
	}

	@Override
	public void setSnowy(boolean snowy) {
		this.snowy = snowy;
	}

	@Override
	public SpigotBlockTypeSnowable clone() {
		return new SpigotBlockTypeSnowable(getNumericalId(), getOldStringId(), getNewStringId(), getMaxStackSize(), snowy);
	}

	@Override
	public MaterialData toMaterialData() {
		return super.toMaterialData();
	}

	@Override
	public SpigotBlockTypeSnowable readMaterialData(MaterialData materialData) {
		super.readMaterialData(materialData);
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		SpigotBlockTypeSnowable that = (SpigotBlockTypeSnowable) o;
		return snowy == that.snowy;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), snowy);
	}
}
