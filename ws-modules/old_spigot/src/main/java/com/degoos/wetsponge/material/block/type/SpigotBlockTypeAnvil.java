package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.enums.block.EnumBlockTypeAnvilDamage;
import com.degoos.wetsponge.material.block.SpigotBlockTypeDirectional;
import com.degoos.wetsponge.util.Validate;
import org.bukkit.material.MaterialData;

import java.util.Objects;
import java.util.Set;

public class SpigotBlockTypeAnvil extends SpigotBlockTypeDirectional implements WSBlockTypeAnvil {

	private EnumBlockTypeAnvilDamage damage;

	public SpigotBlockTypeAnvil(EnumBlockFace facing, Set<EnumBlockFace> faces, EnumBlockTypeAnvilDamage damage) {
		super(145, "minecraft:anvil", "minecraft:anvil", 64, facing, faces);
		Validate.notNull(damage, "Damage cannot be null!");
		this.damage = damage;
	}

	@Override
	public String getNewStringId() {
		switch (damage) {
			case DAMAGED:
				return "minecraft:chipped_anvil";
			case VERY_DAMAGED:
				return "minecraft:damaged_anvil";
			case NORMAL:
			default:
				return "minecraft:anvil";
		}
	}

	@Override
	public EnumBlockTypeAnvilDamage getDamage() {
		return damage;
	}

	@Override
	public void setDamage(EnumBlockTypeAnvilDamage damage) {
		Validate.notNull(damage, "Damage cannot be null!");
		this.damage = damage;
	}

	@Override
	public SpigotBlockTypeAnvil clone() {
		return new SpigotBlockTypeAnvil(getFacing(), getFaces(), damage);
	}

	@Override
	public MaterialData toMaterialData() {
		MaterialData data = super.toMaterialData();
		data.setData((byte) damage.getValue());
		return data;
	}

	@Override
	public SpigotBlockTypeAnvil readMaterialData(MaterialData materialData) {
		damage = EnumBlockTypeAnvilDamage.getByValue(materialData.getData()).orElse(EnumBlockTypeAnvilDamage.NORMAL);
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		SpigotBlockTypeAnvil that = (SpigotBlockTypeAnvil) o;
		return damage == that.damage;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), damage);
	}
}
