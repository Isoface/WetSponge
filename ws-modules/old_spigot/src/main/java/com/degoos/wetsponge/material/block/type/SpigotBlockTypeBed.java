package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.EnumDyeColor;
import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.enums.block.EnumBlockTypeBedPart;
import com.degoos.wetsponge.material.block.SpigotBlockTypeDirectional;
import com.degoos.wetsponge.util.Validate;
import org.bukkit.material.Bed;
import org.bukkit.material.MaterialData;

import java.util.Objects;
import java.util.Set;

public class SpigotBlockTypeBed extends SpigotBlockTypeDirectional implements WSBlockTypeBed {

	private EnumBlockTypeBedPart bedPart;
	private boolean occupied;
	private EnumDyeColor dyeColor;

	public SpigotBlockTypeBed(EnumBlockFace facing, Set<EnumBlockFace> faces, EnumBlockTypeBedPart bedPart, boolean occupied, EnumDyeColor dyeColor) {
		super(26, "minecraft:bed", "bed", 1, facing, faces);
		Validate.notNull(bedPart, "Bed part cannot be null!");
		Validate.notNull(dyeColor, "Dye color cannot be null!");
		this.bedPart = bedPart;
		this.occupied = occupied;
		this.dyeColor = dyeColor;
	}

	@Override
	public String getNewStringId() {
		return "minecraft:" + dyeColor.getMinecraftName().toLowerCase() + "_" + super.getNewStringId();
	}

	@Override
	public EnumBlockTypeBedPart getPart() {
		return bedPart;
	}

	@Override
	public void setPart(EnumBlockTypeBedPart part) {
		Validate.notNull(part, "Bed part cannot be null!");
		this.bedPart = part;
	}

	@Override
	public boolean isOccupied() {
		return occupied;
	}

	@Override
	public EnumDyeColor getDyeColor() {
		return dyeColor;
	}

	@Override
	public void setDyeColor(EnumDyeColor dyeColor) {
		Validate.notNull(dyeColor, "Dye color cannot be null!");
		this.dyeColor = dyeColor;
	}

	@Override
	public SpigotBlockTypeBed clone() {
		return new SpigotBlockTypeBed(getFacing(), getFaces(), bedPart, occupied, dyeColor);
	}

	@Override
	public MaterialData toMaterialData() {
		MaterialData data = super.toMaterialData();
		if (data instanceof Bed) {
			((Bed) data).setHeadOfBed(bedPart == EnumBlockTypeBedPart.HEAD);
		}
		return data;
	}

	@Override
	public SpigotBlockTypeBed readMaterialData(MaterialData materialData) {
		super.readMaterialData(materialData);
		if (materialData instanceof Bed) {
			bedPart = ((Bed) materialData).isHeadOfBed() ? EnumBlockTypeBedPart.HEAD : EnumBlockTypeBedPart.FOOT;
		}
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		SpigotBlockTypeBed that = (SpigotBlockTypeBed) o;
		return occupied == that.occupied &&
				bedPart == that.bedPart &&
				dyeColor == that.dyeColor;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), bedPart, occupied, dyeColor);
	}
}
