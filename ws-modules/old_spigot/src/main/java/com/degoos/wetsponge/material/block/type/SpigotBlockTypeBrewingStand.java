package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.material.block.SpigotBlockType;
import org.bukkit.material.MaterialData;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class SpigotBlockTypeBrewingStand extends SpigotBlockType implements WSBlockTypeBrewingStand {

	private Set<Integer> bottles;
	private int maximumBottles;

	public SpigotBlockTypeBrewingStand(Set<Integer> bottles, int maximumBottles) {
		super(117, "minecraft:brewing_stand", "minecraft:brewing_stand", 64);
		this.bottles = bottles == null ? new HashSet<>() : bottles;
		this.maximumBottles = maximumBottles;
	}

	@Override
	public boolean hasBottle(int index) {
		return bottles.contains(index);
	}

	@Override
	public void setBottle(int index, boolean bottle) {
		if (bottle) bottles.add(index);
		else bottles.remove(index);
	}

	@Override
	public Set<Integer> getBottles() {
		return new HashSet<>(bottles);
	}

	@Override
	public int getMaximumBottles() {
		return maximumBottles;
	}

	@Override
	public SpigotBlockTypeBrewingStand clone() {
		return new SpigotBlockTypeBrewingStand(new HashSet<>(bottles), maximumBottles);
	}

	@Override
	public MaterialData toMaterialData() {
		return super.toMaterialData();
	}

	@Override
	public SpigotBlockTypeBrewingStand readMaterialData(MaterialData materialData) {
		super.readMaterialData(materialData);
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		SpigotBlockTypeBrewingStand that = (SpigotBlockTypeBrewingStand) o;
		return maximumBottles == that.maximumBottles &&
				Objects.equals(bottles, that.bottles);
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), bottles, maximumBottles);
	}
}
