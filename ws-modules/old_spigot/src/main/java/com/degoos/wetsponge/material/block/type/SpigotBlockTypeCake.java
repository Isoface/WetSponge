package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.material.block.SpigotBlockType;
import org.bukkit.material.Cake;
import org.bukkit.material.MaterialData;

import java.util.Objects;

public class SpigotBlockTypeCake extends SpigotBlockType implements WSBlockTypeCake {

	private int bites, maximumBites;

	public SpigotBlockTypeCake(int bites, int maximumBites) {
		super(92, "minecraft:cake", "minecraft:cake", 64);
		this.bites = bites;
		this.maximumBites = maximumBites;
	}

	@Override
	public int getBites() {
		return bites;
	}

	@Override
	public void setBites(int bites) {
		this.bites = Math.min(maximumBites, Math.max(0, bites));
	}

	@Override
	public int getMaximumBites() {
		return maximumBites;
	}

	@Override
	public SpigotBlockTypeCake clone() {
		return new SpigotBlockTypeCake(bites, maximumBites);
	}

	@Override
	public MaterialData toMaterialData() {
		MaterialData data = super.toMaterialData();
		if (data instanceof Cake) {
			((Cake) data).setSlicesEaten(bites);
		}
		return data;
	}

	@Override
	public SpigotBlockTypeCake readMaterialData(MaterialData materialData) {
		super.readMaterialData(materialData);
		if (materialData instanceof Cake) {
			bites = ((Cake) materialData).getSlicesEaten();
		}
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		SpigotBlockTypeCake that = (SpigotBlockTypeCake) o;
		return bites == that.bites &&
				maximumBites == that.maximumBites;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), bites, maximumBites);
	}
}
