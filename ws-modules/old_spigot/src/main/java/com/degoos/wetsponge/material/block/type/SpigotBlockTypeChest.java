package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.enums.block.EnumBlockTypeChestType;
import com.degoos.wetsponge.material.block.SpigotBlockTypeDirectional;
import com.degoos.wetsponge.util.Validate;
import org.bukkit.material.MaterialData;

import java.util.Objects;
import java.util.Set;

public class SpigotBlockTypeChest extends SpigotBlockTypeDirectional implements WSBlockTypeChest {

	private EnumBlockTypeChestType chestType;
	private boolean waterlogged;

	public SpigotBlockTypeChest(int numericalId, String oldStringId, String newStringId, int maxStackSize, EnumBlockFace facing, Set<EnumBlockFace> faces,
								EnumBlockTypeChestType chestType, boolean waterlogged) {
		super(numericalId, oldStringId, newStringId, maxStackSize, facing, faces);
		Validate.notNull(chestType, "ChestType cannot be null!");
		this.chestType = chestType;
		this.waterlogged = waterlogged;
	}

	@Override
	public EnumBlockTypeChestType getType() {
		return chestType;
	}

	@Override
	public void setType(EnumBlockTypeChestType type) {
		Validate.notNull(type, "ChestType cannot be null!");
		this.chestType = type;
	}

	@Override
	public boolean isWaterlogged() {
		return waterlogged;
	}

	@Override
	public void setWaterlogged(boolean waterlogged) {
		this.waterlogged = waterlogged;
	}

	@Override
	public SpigotBlockTypeChest clone() {
		return new SpigotBlockTypeChest(getNumericalId(), getOldStringId(), getNewStringId(), getMaxStackSize(), getFacing(), getFaces(), chestType, waterlogged);
	}

	@Override
	public MaterialData toMaterialData() {
		return super.toMaterialData();
	}

	@Override
	public SpigotBlockTypeChest readMaterialData(MaterialData materialData) {
		super.readMaterialData(materialData);
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		SpigotBlockTypeChest that = (SpigotBlockTypeChest) o;
		return waterlogged == that.waterlogged &&
				chestType == that.chestType;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), chestType, waterlogged);
	}
}
