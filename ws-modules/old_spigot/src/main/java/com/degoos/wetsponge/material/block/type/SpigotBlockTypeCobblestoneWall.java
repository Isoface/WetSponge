package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import org.bukkit.material.MaterialData;

import java.util.Objects;
import java.util.Set;

public class SpigotBlockTypeCobblestoneWall extends SpigotBlockTypeFence implements WSBlockTypeCobblestoneWall {

	private boolean mossy;

	public SpigotBlockTypeCobblestoneWall(Set<EnumBlockFace> faces, Set<EnumBlockFace> allowedFaces, boolean waterlogged, boolean mossy) {
		super(139, "minecraft:cobblestone_wall", "minecraft:cobblestone_wall", 64, faces, allowedFaces, waterlogged);
		this.mossy = mossy;
	}

	@Override
	public String getNewStringId() {
		return mossy ? "minecraft:mossy_cobblestone_wall" : "minecraft:cobblestone_wall";
	}

	@Override
	public boolean isMossy() {
		return mossy;
	}

	@Override
	public void setMossy(boolean mossy) {
		this.mossy = mossy;
	}

	@Override
	public SpigotBlockTypeCobblestoneWall clone() {
		return new SpigotBlockTypeCobblestoneWall(getFaces(), getAllowedFaces(), isWaterlogged(), mossy);
	}

	@Override
	public MaterialData toMaterialData() {
		MaterialData data = super.toMaterialData();
		data.setData(mossy ? (byte) 1 : 0);
		return data;
	}

	@Override
	public SpigotBlockTypeCobblestoneWall readMaterialData(MaterialData materialData) {
		super.readMaterialData(materialData);
		mossy = materialData.getData() == 1;
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		SpigotBlockTypeCobblestoneWall that = (SpigotBlockTypeCobblestoneWall) o;
		return mossy == that.mossy;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), mossy);
	}
}
