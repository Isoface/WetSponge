package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.material.block.SpigotBlockTypeDirectional;
import org.bukkit.material.CocoaPlant;
import org.bukkit.material.MaterialData;

import java.util.Objects;
import java.util.Set;

public class SpigotBlockTypeCocoa extends SpigotBlockTypeDirectional implements WSBlockTypeCocoa {

	private int age, maximumAge;

	public SpigotBlockTypeCocoa(EnumBlockFace facing, Set<EnumBlockFace> faces, int age, int maximumAge) {
		super(127, "minecraft:cocoa", "minecraft:cocoa", 64, facing, faces);
		this.age = age;
		this.maximumAge = maximumAge;
	}

	@Override
	public int getAge() {
		return age;
	}

	@Override
	public void setAge(int age) {
		this.age = Math.min(maximumAge, Math.max(0, age));
	}

	@Override
	public int getMaximumAge() {
		return maximumAge;
	}

	@Override
	public SpigotBlockTypeCocoa clone() {
		return new SpigotBlockTypeCocoa(getFacing(), getFaces(), age, maximumAge);
	}

	@Override
	public MaterialData toMaterialData() {
		MaterialData data = super.toMaterialData();
		if (data instanceof CocoaPlant) {
			((CocoaPlant) data).setSize(age == 0 ? CocoaPlant.CocoaPlantSize.SMALL : age == 1 ? CocoaPlant.CocoaPlantSize.MEDIUM : CocoaPlant.CocoaPlantSize.LARGE);
		}
		return data;
	}

	@Override
	public SpigotBlockTypeCocoa readMaterialData(MaterialData materialData) {
		super.readMaterialData(materialData);

		if (materialData instanceof CocoaPlant) {
			switch (((CocoaPlant) materialData).getSize()) {
				case LARGE:
					age = 2;
					break;
				case MEDIUM:
					age = 1;
					break;
				case SMALL:
				default:
					age = 0;
					break;
			}
		}
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		SpigotBlockTypeCocoa that = (SpigotBlockTypeCocoa) o;
		return age == that.age &&
				maximumAge == that.maximumAge;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), age, maximumAge);
	}
}
