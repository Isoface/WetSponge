package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.material.block.SpigotBlockTypeDirectional;
import org.bukkit.material.MaterialData;

import java.util.Objects;
import java.util.Set;

public class SpigotBlockTypeCommandBlock extends SpigotBlockTypeDirectional implements WSBlockTypeCommandBlock {

	private boolean conditional;

	public SpigotBlockTypeCommandBlock(int numericalId, String oldStringId, String newStringId, int maxStackSize, EnumBlockFace facing, Set<EnumBlockFace> faces, boolean conditional) {
		super(numericalId, oldStringId, newStringId, maxStackSize, facing, faces);
		this.conditional = conditional;
	}

	@Override
	public boolean isConditional() {
		return conditional;
	}

	@Override
	public void setConditional(boolean conditional) {
		this.conditional = conditional;
	}

	@Override
	public SpigotBlockTypeCommandBlock clone() {
		return new SpigotBlockTypeCommandBlock(getNumericalId(), getOldStringId(), getNewStringId(), getMaxStackSize(), getFacing(), getFaces(), conditional);
	}

	@Override
	public MaterialData toMaterialData() {
		return super.toMaterialData();
	}

	@Override
	public SpigotBlockTypeDirectional readMaterialData(MaterialData materialData) {
		super.readMaterialData(materialData);
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		SpigotBlockTypeCommandBlock that = (SpigotBlockTypeCommandBlock) o;
		return conditional == that.conditional;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), conditional);
	}
}
