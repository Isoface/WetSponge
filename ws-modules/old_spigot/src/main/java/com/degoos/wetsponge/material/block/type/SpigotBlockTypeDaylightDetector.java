package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.material.block.SpigotBlockTypeAnaloguePowerable;
import org.bukkit.material.MaterialData;

import java.util.Objects;

public class SpigotBlockTypeDaylightDetector extends SpigotBlockTypeAnaloguePowerable implements WSBlockTypeDaylightDetector {

	private boolean inverted;

	public SpigotBlockTypeDaylightDetector(int power, int maximumPower, boolean inverted) {
		super(151, "minecraft:daylight_detector", "minecraft:daylight_detector", 64, power, maximumPower);
		this.inverted = inverted;
	}

	@Override
	public int getNumericalId() {
		return isInverted() ? 178 : 151;
	}

	@Override
	public String getOldStringId() {
		return isInverted() ? "minecraft:daylight_detector_inverted" : "minecraft:daylight_detector";
	}

	@Override
	public boolean isInverted() {
		return inverted;
	}

	@Override
	public void setInverted(boolean inverted) {
		this.inverted = inverted;
	}

	@Override
	public SpigotBlockTypeDaylightDetector clone() {
		return new SpigotBlockTypeDaylightDetector(getPower(), gerMaximumPower(), inverted);
	}

	@Override
	public MaterialData toMaterialData() {
		return super.toMaterialData();
	}

	@Override
	public SpigotBlockTypeDaylightDetector readMaterialData(MaterialData materialData) {
		super.readMaterialData(materialData);
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		SpigotBlockTypeDaylightDetector that = (SpigotBlockTypeDaylightDetector) o;
		return inverted == that.inverted;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), inverted);
	}
}
