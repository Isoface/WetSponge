package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockTypeDirtType;
import com.degoos.wetsponge.material.block.SpigotBlockType;
import com.degoos.wetsponge.util.Validate;
import org.bukkit.material.MaterialData;

import java.util.Objects;

public class SpigotBlockTypeDirt extends SpigotBlockType implements WSBlockTypeDirt {

	private EnumBlockTypeDirtType dirtType;

	public SpigotBlockTypeDirt(EnumBlockTypeDirtType dirtType) {
		super(3, "minecraft:dirt", "minecraft:dirt", 64);
		Validate.notNull(dirtType, "Dirt type cannot be null!");
		this.dirtType = dirtType;
	}

	@Override
	public String getNewStringId() {
		switch (dirtType) {
			case PODZOL:
				return "minecraft:podzol";
			case COARSE_DIRT:
				return "minecraft:coarse_dirt";
			case DIRT:
			default:
				return "minecraft:dirt";
		}
	}

	@Override
	public EnumBlockTypeDirtType getDirtType() {
		return dirtType;
	}

	@Override
	public void setDirtType(EnumBlockTypeDirtType dirtType) {
		Validate.notNull(dirtType, "Dirt type cannot be null!");
		this.dirtType = dirtType;
	}

	@Override
	public SpigotBlockTypeDirt clone() {
		return new SpigotBlockTypeDirt(dirtType);
	}

	@Override
	public MaterialData toMaterialData() {
		MaterialData data = super.toMaterialData();
		data.setData((byte) dirtType.getValue());
		return data;
	}

	@Override
	public SpigotBlockTypeDirt readMaterialData(MaterialData materialData) {
		super.readMaterialData(materialData);
		dirtType = EnumBlockTypeDirtType.getByValue(materialData.getData()).orElse(EnumBlockTypeDirtType.DIRT);
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		SpigotBlockTypeDirt that = (SpigotBlockTypeDirt) o;
		return dirtType == that.dirtType;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), dirtType);
	}
}
