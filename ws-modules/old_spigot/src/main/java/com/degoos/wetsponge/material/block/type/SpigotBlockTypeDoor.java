package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.enums.block.EnumBlockTypeBisectedHalf;
import com.degoos.wetsponge.enums.block.EnumBlockTypeDoorHinge;
import com.degoos.wetsponge.material.block.SpigotBlockTypeDirectional;
import com.degoos.wetsponge.util.Validate;
import org.bukkit.material.Door;
import org.bukkit.material.MaterialData;

import java.util.Objects;
import java.util.Set;

public class SpigotBlockTypeDoor extends SpigotBlockTypeDirectional implements WSBlockTypeDoor {

	private EnumBlockTypeDoorHinge hinge;
	private EnumBlockTypeBisectedHalf half;
	private boolean open, powered;

	public SpigotBlockTypeDoor(int numericalId, String oldStringId, String newStringId, int maxStackSize, EnumBlockFace facing, Set<EnumBlockFace> faces,
							   EnumBlockTypeDoorHinge hinge, EnumBlockTypeBisectedHalf half, boolean open, boolean powered) {
		super(numericalId, oldStringId, newStringId, maxStackSize, facing, faces);
		Validate.notNull(hinge, "Hinge cannot be null!");
		Validate.notNull(half, "Half cannot be null!");
		this.hinge = hinge;
		this.half = half;
		this.open = open;
		this.powered = powered;
	}

	@Override
	public EnumBlockTypeDoorHinge getHinge() {
		return hinge;
	}

	@Override
	public void setHinge(EnumBlockTypeDoorHinge hinge) {
		Validate.notNull(hinge, "Hinge cannot be null!");
		this.hinge = hinge;
	}

	@Override
	public EnumBlockTypeBisectedHalf getHalf() {
		return half;
	}

	@Override
	public void setHalf(EnumBlockTypeBisectedHalf half) {
		Validate.notNull(half, "Half cannot be null!");
		this.half = half;
	}

	@Override
	public boolean isOpen() {
		return open;
	}

	@Override
	public void setOpen(boolean open) {
		this.open = open;
	}

	@Override
	public boolean isPowered() {
		return powered;
	}

	@Override
	public void setPowered(boolean powered) {
		this.powered = powered;
	}

	@Override
	public SpigotBlockTypeDoor clone() {
		return new SpigotBlockTypeDoor(getNumericalId(), getOldStringId(), getNewStringId(), getMaxStackSize(), getFacing(), getFaces(), hinge, half, open, powered);
	}

	@Override
	public MaterialData toMaterialData() {
		MaterialData data = super.toMaterialData();

		if (data instanceof Door) {
			((Door) data).setOpen(open);
			((Door) data).setHinge(hinge == EnumBlockTypeDoorHinge.RIGHT);
			((Door) data).setTopHalf(half == EnumBlockTypeBisectedHalf.TOP);
		}
		return data;
	}

	@Override
	public SpigotBlockTypeDirectional readMaterialData(MaterialData materialData) {
		super.readMaterialData(materialData);
		if (materialData instanceof Door) {
			half = ((Door) materialData).isTopHalf() ? EnumBlockTypeBisectedHalf.TOP : EnumBlockTypeBisectedHalf.BOTTOM;
			hinge = ((Door) materialData).getHinge() ? EnumBlockTypeDoorHinge.RIGHT : EnumBlockTypeDoorHinge.LEFT;
			open = ((Door) materialData).isOpen();
		}
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		SpigotBlockTypeDoor that = (SpigotBlockTypeDoor) o;
		return open == that.open &&
				powered == that.powered &&
				hinge == that.hinge &&
				half == that.half;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), hinge, half, open, powered);
	}
}
