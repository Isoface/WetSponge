package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.material.block.SpigotBlockTypeDirectional;
import org.bukkit.material.MaterialData;

import java.util.Objects;
import java.util.Set;

public class SpigotBlockTypeEndPortalFrame extends SpigotBlockTypeDirectional implements WSBlockTypeEndPortalFrame {

	private boolean eye;

	public SpigotBlockTypeEndPortalFrame(EnumBlockFace facing, Set<EnumBlockFace> faces, boolean eye) {
		super(120, "minecraft:end_portal_frame", "minecraft:end_portal_frame", 64, facing, faces);
		this.eye = eye;
	}

	@Override
	public boolean hasEye() {
		return eye;
	}

	@Override
	public void setEye(boolean eye) {
		this.eye = eye;
	}

	@Override
	public SpigotBlockTypeEndPortalFrame clone() {
		return new SpigotBlockTypeEndPortalFrame(getFacing(), getFaces(), eye);
	}

	@Override
	public MaterialData toMaterialData() {
		MaterialData materialData = super.toMaterialData();
		int data;
		switch (getFacing()) {
			case EAST:
				data = 1;
				break;
			case SOUTH:
				data = 2;
				break;
			case WEST:
				data = 3;
				break;
			case NORTH:
			default:
				data = 0;
				break;
		}
		materialData.setData((byte) (data + (eye ? 4 : 0)));
		return materialData;
	}

	@Override
	public SpigotBlockTypeEndPortalFrame readMaterialData(MaterialData materialData) {
		super.readMaterialData(materialData);
		int data = materialData.getData();
		eye = data > 3;
		switch (data % 4) {
			case 1:
				setFacing(EnumBlockFace.EAST);
				break;
			case 2:
				setFacing(EnumBlockFace.SOUTH);
				break;
			case 3:
				setFacing(EnumBlockFace.WEST);
				break;
			case 0:
			default:
				setFacing(EnumBlockFace.NORTH);
				break;
		}
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		SpigotBlockTypeEndPortalFrame that = (SpigotBlockTypeEndPortalFrame) o;
		return eye == that.eye;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), eye);
	}
}
