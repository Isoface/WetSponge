package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.material.block.SpigotBlockType;
import org.bukkit.material.MaterialData;

import java.util.Objects;

public class SpigotBlockTypeFarmland extends SpigotBlockType implements WSBlockTypeFarmland {

	private int moisture, maximumMoisture;

	public SpigotBlockTypeFarmland(int moisture, int maximumMoisture) {
		super(60, "minecraft:farmland", "minecraft:farmland", 64);
		this.moisture = moisture;
		this.maximumMoisture = maximumMoisture;
	}

	@Override
	public int getMoisture() {
		return moisture;
	}

	@Override
	public void setMoisture(int moisture) {
		this.moisture = moisture;
	}

	@Override
	public int getMaximumMoisture() {
		return maximumMoisture;
	}

	@Override
	public SpigotBlockTypeFarmland clone() {
		return new SpigotBlockTypeFarmland(moisture, maximumMoisture);
	}

	@Override
	public MaterialData toMaterialData() {
		MaterialData data = super.toMaterialData();
		data.setData((byte) moisture);
		return data;
	}

	@Override
	public SpigotBlockTypeFarmland readMaterialData(MaterialData materialData) {
		super.readMaterialData(materialData);
		moisture = materialData.getData();
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		SpigotBlockTypeFarmland that = (SpigotBlockTypeFarmland) o;
		return moisture == that.moisture &&
				maximumMoisture == that.maximumMoisture;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), moisture, maximumMoisture);
	}
}
