package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.material.block.SpigotBlockTypeMultipleFacing;
import org.bukkit.material.MaterialData;

import java.util.Objects;
import java.util.Set;

public class SpigotBlockTypeFence extends SpigotBlockTypeMultipleFacing implements WSBlockTypeFence {

	private boolean waterlogged;

	public SpigotBlockTypeFence(int numericalId, String oldStringId, String newStringId, int maxStackSize, Set<EnumBlockFace> faces, Set<EnumBlockFace> allowedFaces, boolean waterlogged) {
		super(numericalId, oldStringId, newStringId, maxStackSize, faces, allowedFaces);
		this.waterlogged = waterlogged;
	}

	@Override
	public boolean isWaterlogged() {
		return waterlogged;
	}

	@Override
	public void setWaterlogged(boolean waterlogged) {
		this.waterlogged = waterlogged;
	}

	@Override
	public SpigotBlockTypeFence clone() {
		return new SpigotBlockTypeFence(getNumericalId(), getOldStringId(), getNewStringId(), getMaxStackSize(), getFaces(), getAllowedFaces(), waterlogged);
	}

	@Override
	public MaterialData toMaterialData() {
		return super.toMaterialData();
	}

	@Override
	public SpigotBlockTypeFence readMaterialData(MaterialData materialData) {
		super.readMaterialData(materialData);
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		SpigotBlockTypeFence that = (SpigotBlockTypeFence) o;
		return waterlogged == that.waterlogged;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), waterlogged);
	}
}
