package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.material.block.SpigotBlockTypeMultipleFacing;
import org.bukkit.material.MaterialData;

import java.util.Objects;
import java.util.Set;

public class SpigotBlockTypeFire extends SpigotBlockTypeMultipleFacing implements WSBlockTypeFire {

	private int age, maximumAge;

	public SpigotBlockTypeFire(Set<EnumBlockFace> faces, Set<EnumBlockFace> allowedFaces, int age, int maximumAge) {
		super(51, "minecraft:fire", "minecraft:fire", 64, faces, allowedFaces);
		this.age = age;
		this.maximumAge = maximumAge;
	}

	@Override
	public int getAge() {
		return age;
	}

	@Override
	public void setAge(int age) {
		this.age = Math.min(maximumAge, Math.max(0, age));
	}

	@Override
	public int getMaximumAge() {
		return maximumAge;
	}

	@Override
	public SpigotBlockTypeFire clone() {
		return new SpigotBlockTypeFire(getFaces(), getAllowedFaces(), age, maximumAge);
	}

	@Override
	public MaterialData toMaterialData() {
		return super.toMaterialData();
	}

	@Override
	public SpigotBlockTypeFire readMaterialData(MaterialData materialData) {
		super.readMaterialData(materialData);
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		SpigotBlockTypeFire that = (SpigotBlockTypeFire) o;
		return age == that.age &&
				maximumAge == that.maximumAge;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), age, maximumAge);
	}
}
