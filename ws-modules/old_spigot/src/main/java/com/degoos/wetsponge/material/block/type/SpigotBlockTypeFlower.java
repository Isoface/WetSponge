package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockTypeFlowerType;
import com.degoos.wetsponge.material.block.SpigotBlockType;
import com.degoos.wetsponge.util.Validate;
import org.bukkit.material.MaterialData;

import java.util.Objects;

public class SpigotBlockTypeFlower extends SpigotBlockType implements WSBlockTypeFlower {

	private EnumBlockTypeFlowerType flowerType;

	public SpigotBlockTypeFlower(EnumBlockTypeFlowerType flowerType) {
		super(37, "minecraft:yellow_flower", "minecraft:dandelion", 64);
		Validate.notNull(flowerType, "Flower type cannot be null!");
		this.flowerType = flowerType;
	}

	@Override
	public int getNumericalId() {
		return flowerType == EnumBlockTypeFlowerType.DANDELION ? 37 : 38;
	}

	@Override
	public String getOldStringId() {
		return flowerType == EnumBlockTypeFlowerType.DANDELION ? "minecraft:yellow_flower" : "minecraft:red_flower";
	}

	@Override
	public String getNewStringId() {
		switch (flowerType) {
			case ALLIUM:
				return "minecraft:allium";
			case HOUSTONIA:
				return "minecraft:azure_bluet";
			case BLUE_ORCHID:
				return "minecraft:blue_orchid";
			case OXEYE_DAISY:
				return "minecraft:oxeye_daisy";
			case RED_TULIP:
				return "minecraft:red_tulip";
			case PINK_TULIP:
				return "minecraft:pink_tulip";
			case WHITE_TULIP:
				return "minecraft:white_tulip";
			case ORANGE_TULIP:
				return "minecraft:orange_tulip";
			case POPPY:
				return "minecraft:poppy";
			case DANDELION:
			default:
				return "minecraft:dandelion";
		}
	}


	@Override
	public EnumBlockTypeFlowerType getFlowerType() {
		return flowerType;
	}

	@Override
	public void setFlowerType(EnumBlockTypeFlowerType flowerType) {
		Validate.notNull(flowerType, "Flower type cannot be null!");
		this.flowerType = flowerType;
	}

	@Override
	public SpigotBlockTypeFlower clone() {
		return new SpigotBlockTypeFlower(flowerType);
	}

	@Override
	public MaterialData toMaterialData() {
		MaterialData materialData = super.toMaterialData();
		materialData.setData((byte) flowerType.getValue());
		return materialData;
	}

	@Override
	public SpigotBlockTypeFlower readMaterialData(MaterialData materialData) {
		super.readMaterialData(materialData);
		flowerType = EnumBlockTypeFlowerType.getByValue(materialData.getData()).orElse(EnumBlockTypeFlowerType.POPPY);
		return this;
	}


	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		SpigotBlockTypeFlower that = (SpigotBlockTypeFlower) o;
		return flowerType == that.flowerType;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), flowerType);
	}
}