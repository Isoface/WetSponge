package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.material.block.SpigotBlockTypeDirectional;
import org.bukkit.material.Gate;
import org.bukkit.material.MaterialData;

import java.util.Objects;
import java.util.Set;

public class SpigotBlockTypeGate extends SpigotBlockTypeDirectional implements WSBlockTypeGate {

	private boolean inWall, open, powered;

	public SpigotBlockTypeGate(int numericalId, String oldStringId, String newStringId, int maxStackSize, EnumBlockFace facing, Set<EnumBlockFace> faces,
							   boolean inWall, boolean open, boolean powered) {
		super(numericalId, oldStringId, newStringId, maxStackSize, facing, faces);
		this.inWall = inWall;
		this.open = open;
		this.powered = powered;
	}

	@Override
	public boolean isInWall() {
		return inWall;
	}

	@Override
	public void setInWall(boolean inWall) {
		this.inWall = inWall;
	}

	@Override
	public boolean isOpen() {
		return open;
	}

	@Override
	public void setOpen(boolean open) {
		this.open = open;
	}

	@Override
	public boolean isPowered() {
		return powered;
	}

	@Override
	public void setPowered(boolean powered) {
		this.powered = powered;
	}

	@Override
	public SpigotBlockTypeGate clone() {
		return new SpigotBlockTypeGate(getNumericalId(), getOldStringId(), getNewStringId(), getMaxStackSize(), getFacing(), getFaces(), inWall, open, powered);
	}

	@Override
	public MaterialData toMaterialData() {
		MaterialData data = super.toMaterialData();
		if (data instanceof Gate) {
			((Gate) data).setOpen(open);
		}
		return data;
	}

	@Override
	public SpigotBlockTypeGate readMaterialData(MaterialData materialData) {
		super.readMaterialData(materialData);
		if (materialData instanceof Gate) {
			open = ((Gate) materialData).isOpen();
		}
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		SpigotBlockTypeGate that = (SpigotBlockTypeGate) o;
		return inWall == that.inWall &&
				open == that.open &&
				powered == that.powered;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), inWall, open, powered);
	}
}
