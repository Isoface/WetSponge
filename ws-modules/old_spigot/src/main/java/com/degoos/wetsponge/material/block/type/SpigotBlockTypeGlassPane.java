package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.material.block.SpigotBlockTypeMultipleFacing;
import org.bukkit.material.MaterialData;

import java.util.Objects;
import java.util.Set;

public class SpigotBlockTypeGlassPane extends SpigotBlockTypeMultipleFacing implements WSBlockTypeGlassPane {

	private boolean waterlogged;

	public SpigotBlockTypeGlassPane(int numericalId, String oldStringId, String newStringId, int maxStackSize, Set<EnumBlockFace> faces, Set<EnumBlockFace> allowedFaces, boolean waterlogged) {
		super(numericalId, oldStringId, newStringId, maxStackSize, faces, allowedFaces);
		this.waterlogged = waterlogged;
	}

	@Override
	public boolean isWaterlogged() {
		return waterlogged;
	}

	@Override
	public void setWaterlogged(boolean waterlogged) {
		this.waterlogged = waterlogged;
	}

	@Override
	public SpigotBlockTypeGlassPane clone() {
		return new SpigotBlockTypeGlassPane(getNumericalId(), getOldStringId(), getNewStringId(), getMaxStackSize(), getFaces(), getAllowedFaces(), waterlogged);
	}

	@Override
	public MaterialData toMaterialData() {
		return super.toMaterialData();
	}

	@Override
	public SpigotBlockTypeGlassPane readMaterialData(MaterialData materialData) {
		super.readMaterialData(materialData);
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		SpigotBlockTypeGlassPane that = (SpigotBlockTypeGlassPane) o;
		return waterlogged == that.waterlogged;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), waterlogged);
	}
}
