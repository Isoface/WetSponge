package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.material.block.SpigotBlockTypeDirectional;
import org.bukkit.material.Hopper;
import org.bukkit.material.MaterialData;

import java.util.Objects;
import java.util.Set;

public class SpigotBlockTypeHopper extends SpigotBlockTypeDirectional implements WSBlockTypeHopper {

	private boolean enabled;

	public SpigotBlockTypeHopper(EnumBlockFace facing, Set<EnumBlockFace> faces, boolean enabled) {
		super(154, "minecraft:hopper", "minecraft:hopper", 64, facing, faces);
		this.enabled = enabled;
	}

	@Override
	public boolean isEnabled() {
		return enabled;
	}

	@Override
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	@Override
	public SpigotBlockTypeHopper clone() {
		return new SpigotBlockTypeHopper(getFacing(), getFaces(), enabled);
	}

	@Override
	public MaterialData toMaterialData() {
		MaterialData materialData = super.toMaterialData();
		if (materialData instanceof Hopper) {
			((Hopper) materialData).setActive(enabled);
		}
		return materialData;
	}

	@Override
	public SpigotBlockTypeHopper readMaterialData(MaterialData materialData) {
		super.readMaterialData(materialData);
		if (materialData instanceof Hopper) {
			enabled = ((Hopper) materialData).isActive();
		}
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		SpigotBlockTypeHopper that = (SpigotBlockTypeHopper) o;
		return enabled == that.enabled;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), enabled);
	}
}
