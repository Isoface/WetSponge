package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.material.block.SpigotBlockType;
import org.bukkit.material.MaterialData;

import java.util.Objects;

public class SpigotBlockTypeJukebox extends SpigotBlockType implements WSBlockTypeJukebox {

	private boolean record;

	public SpigotBlockTypeJukebox(boolean record) {
		super(84, "minecraft:jukebox", "minecraft:jukebox", 64);
		this.record = record;
	}

	@Override
	public boolean hasRecord() {
		return record;
	}

	@Override
	public SpigotBlockTypeJukebox clone() {
		return new SpigotBlockTypeJukebox(record);
	}


	@Override
	public SpigotBlockTypeJukebox readMaterialData(MaterialData materialData) {
		super.readMaterialData(materialData);
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		SpigotBlockTypeJukebox that = (SpigotBlockTypeJukebox) o;
		return record == that.record;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), record);
	}
}
