package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumWoodType;
import com.degoos.wetsponge.material.block.SpigotBlockType;
import com.degoos.wetsponge.util.Validate;
import org.bukkit.material.MaterialData;

import java.util.Objects;

public class SpigotBlockTypeLeaves extends SpigotBlockType implements WSBlockTypeLeaves {

	private boolean persistent;
	private int distance;
	private EnumWoodType woodType;

	public SpigotBlockTypeLeaves(boolean persistent, int distance, EnumWoodType woodType) {
		super(18, "minecraft_leaves", "minecraft:leaves", 64);
		Validate.notNull(woodType, "Wood type cannot be null!");
		this.persistent = persistent;
		this.distance = Math.min(7, Math.max(1, distance));
		this.woodType = woodType;
	}


	@Override
	public int getNumericalId() {
		return getWoodType() == EnumWoodType.ACACIA || getWoodType() == EnumWoodType.DARK_OAK ? 161 : 18;
	}

	@Override
	public String getNewStringId() {
		return getWoodType() == EnumWoodType.ACACIA || getWoodType() == EnumWoodType.DARK_OAK ? "minecraft:leaves2" : "minecraft:leaves";
	}

	@Override
	public String getOldStringId() {
		return "minecraft:" + getWoodType().name().toLowerCase() + "_leaves";
	}

	@Override
	public boolean isPersistent() {
		return persistent;
	}

	@Override
	public void setPersistent(boolean persistent) {
		this.persistent = persistent;
	}

	@Override
	public int getDistance() {
		return distance;
	}

	@Override
	public void setDistance(int distance) {
		this.distance = Math.min(7, Math.max(1, distance));
	}

	@Override
	public EnumWoodType getWoodType() {
		return woodType;
	}

	@Override
	public void setWoodType(EnumWoodType woodType) {
		Validate.notNull(woodType, "Wood type cannot be null!");
		this.woodType = woodType;
	}

	@Override
	public SpigotBlockTypeLeaves clone() {
		return new SpigotBlockTypeLeaves(persistent, distance, woodType);
	}

	@Override
	public MaterialData toMaterialData() {
		MaterialData data = super.toMaterialData();
		data.setData((byte) ((woodType.getValue() % 4) + (persistent ? 4 : 0)));
		return data;
	}

	@Override
	public SpigotBlockTypeLeaves readMaterialData(MaterialData materialData) {
		super.readMaterialData(materialData);
		int data = materialData.getData();
		if (materialData.getItemType().getId() == 161) {
			woodType = data % 4 == 0 ? EnumWoodType.ACACIA : EnumWoodType.DARK_OAK;
		} else woodType = EnumWoodType.getByValue(data % 4).orElse(EnumWoodType.OAK);
		persistent = data > 3;
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		SpigotBlockTypeLeaves that = (SpigotBlockTypeLeaves) o;
		return persistent == that.persistent &&
				distance == that.distance &&
				woodType == that.woodType;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), persistent, distance, woodType);
	}
}
