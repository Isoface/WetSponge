package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.EnumInstrument;
import com.degoos.wetsponge.material.block.SpigotBlockTypePowerable;
import com.degoos.wetsponge.util.Validate;
import org.bukkit.material.MaterialData;

import java.util.Objects;

public class SpigotBlockTypeNoteBlock extends SpigotBlockTypePowerable implements WSBlockTypeNoteBlock {

	private EnumInstrument instrument;
	private int note;

	public SpigotBlockTypeNoteBlock(boolean powered, EnumInstrument instrument, int note) {
		super(25, "minecraft:noteblock", "minecraft:note_block", 64, powered);
		Validate.notNull(instrument, "Instrument cannot be null!");
		this.instrument = instrument;
		this.note = Math.max(0, Math.min(25, note));
	}

	@Override
	public EnumInstrument getInstrument() {
		return instrument;
	}

	@Override
	public void setInstrument(EnumInstrument instrument) {
		Validate.notNull(instrument, "Instrument cannot be null!");
		this.instrument = instrument;
	}

	@Override
	public int getNote() {
		return note;
	}

	@Override
	public void setNote(int note) {
		this.note = Math.max(0, Math.min(25, note));
	}

	@Override
	public SpigotBlockTypeNoteBlock clone() {
		return new SpigotBlockTypeNoteBlock(isPowered(), instrument, note);
	}

	@Override
	public MaterialData toMaterialData() {
		return super.toMaterialData();
	}

	@Override
	public SpigotBlockTypeNoteBlock readMaterialData(MaterialData materialData) {
		super.readMaterialData(materialData);
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		SpigotBlockTypeNoteBlock that = (SpigotBlockTypeNoteBlock) o;
		return note == that.note &&
				instrument == that.instrument;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), instrument, note);
	}
}
