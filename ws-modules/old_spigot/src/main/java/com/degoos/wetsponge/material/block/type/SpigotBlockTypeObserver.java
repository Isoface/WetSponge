package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.material.block.SpigotBlockTypeDirectional;
import org.bukkit.material.MaterialData;

import java.util.Objects;
import java.util.Set;

public class SpigotBlockTypeObserver extends SpigotBlockTypeDirectional implements WSBlockTypeObserver {

	private boolean powered;

	public SpigotBlockTypeObserver(EnumBlockFace facing, Set<EnumBlockFace> faces, boolean powered) {
		super(218, "minecraft:observer", "minecraft:observer", 64, facing, faces);
		this.powered = powered;
	}

	@Override
	public boolean isPowered() {
		return powered;
	}

	@Override
	public void setPowered(boolean powered) {
		this.powered = powered;
	}

	@Override
	public SpigotBlockTypeObserver clone() {
		return new SpigotBlockTypeObserver(getFacing(), getFaces(), powered);
	}

	@Override
	public MaterialData toMaterialData() {
		return super.toMaterialData();
	}

	@Override
	public SpigotBlockTypeObserver readMaterialData(MaterialData materialData) {
		super.readMaterialData(materialData);
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		SpigotBlockTypeObserver that = (SpigotBlockTypeObserver) o;
		return powered == that.powered;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), powered);
	}
}
