package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.material.block.SpigotBlockTypeDirectional;
import org.bukkit.material.MaterialData;
import org.bukkit.material.PistonBaseMaterial;

import java.util.Objects;
import java.util.Set;

public class SpigotBlockTypePiston extends SpigotBlockTypeDirectional implements WSBlockTypePiston {

	private boolean extended;

	public SpigotBlockTypePiston(int numericalId, String oldStringId, String newStringId, int maxStackSize, EnumBlockFace facing, Set<EnumBlockFace> faces, boolean extended) {
		super(numericalId, oldStringId, newStringId, maxStackSize, facing, faces);
		this.extended = extended;
	}

	@Override
	public boolean isExtended() {
		return extended;
	}

	@Override
	public void setExtended(boolean extended) {
		this.extended = extended;
	}

	@Override
	public SpigotBlockTypePiston clone() {
		return new SpigotBlockTypePiston(getNumericalId(), getOldStringId(), getNewStringId(), getMaxStackSize(), getFacing(), getFaces(), extended);
	}

	@Override
	public MaterialData toMaterialData() {
		MaterialData data = super.toMaterialData();

		if (data instanceof PistonBaseMaterial) {
			((PistonBaseMaterial) data).setPowered(extended);
		}
		return data;
	}

	@Override
	public SpigotBlockTypePiston readMaterialData(MaterialData materialData) {
		super.readMaterialData(materialData);
		if (materialData instanceof PistonBaseMaterial) {
			extended = ((PistonBaseMaterial) materialData).isPowered();
		}
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		SpigotBlockTypePiston that = (SpigotBlockTypePiston) o;
		return extended == that.extended;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), extended);
	}
}
