package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.enums.block.EnumBlockTypePistonType;
import org.bukkit.material.MaterialData;

import java.util.Objects;
import java.util.Set;

public class SpigotBlockTypePistonHead extends SpigotBlockTypeTechnicalPiston implements WSBlockTypePistonHead {

	private boolean shortPiston;

	public SpigotBlockTypePistonHead(EnumBlockFace facing, Set<EnumBlockFace> faces, EnumBlockTypePistonType pistonType, boolean shortPiston) {
		super(34, "minecraft:piston_head", "minecraft:piston_head", 64, facing, faces, pistonType);
		this.shortPiston = shortPiston;
	}

	@Override
	public boolean isShort() {
		return shortPiston;
	}

	@Override
	public void setShort(boolean s) {
		this.shortPiston = s;
	}

	@Override
	public SpigotBlockTypePistonHead clone() {
		return new SpigotBlockTypePistonHead(getFacing(), getFaces(), getType(), shortPiston);
	}

	@Override
	public MaterialData toMaterialData() {
		return super.toMaterialData();
	}

	@Override
	public SpigotBlockTypePistonHead readMaterialData(MaterialData materialData) {
		super.readMaterialData(materialData);
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		SpigotBlockTypePistonHead that = (SpigotBlockTypePistonHead) o;
		return shortPiston == that.shortPiston;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), shortPiston);
	}
}
