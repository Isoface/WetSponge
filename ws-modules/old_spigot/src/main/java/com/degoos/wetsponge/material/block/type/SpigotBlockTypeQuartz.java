package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumAxis;
import com.degoos.wetsponge.enums.block.EnumBlockTypeQuartzType;
import com.degoos.wetsponge.material.block.SpigotBlockTypeOrientable;
import com.degoos.wetsponge.util.Validate;
import org.bukkit.material.MaterialData;

import java.util.Objects;
import java.util.Set;

public class SpigotBlockTypeQuartz extends SpigotBlockTypeOrientable implements WSBlockTypeQuartz {

	private EnumBlockTypeQuartzType quartzType;

	public SpigotBlockTypeQuartz(EnumAxis axis, Set<EnumAxis> axes, EnumBlockTypeQuartzType quartzType) {
		super(155, "minecraft:quartz_block", "minecraft:quartz_block", 64, axis, axes);
		Validate.notNull(quartzType, "Quartz type cannot be null!");
		this.quartzType = quartzType;
	}

	@Override
	public String getNewStringId() {
		switch (quartzType) {
			case CHISELED:
				return "minecraft:chiseled_quartz_block";
			case PILLAR:
				return "minecraft:quartz_pillar";
			case NORMAL:
			default:
				return "minecraft:quartz_block";
		}
	}

	@Override
	public EnumBlockTypeQuartzType getQuartzType() {
		return quartzType;
	}

	@Override
	public void setQuartzType(EnumBlockTypeQuartzType quartzType) {
		Validate.notNull(quartzType, "Quartz type cannot be null!");
		this.quartzType = quartzType;
	}

	@Override
	public SpigotBlockTypeQuartz clone() {
		return new SpigotBlockTypeQuartz(getAxis(), getAxes(), quartzType);
	}

	@Override
	public MaterialData toMaterialData() {
		MaterialData data = super.toMaterialData();
		if (quartzType == EnumBlockTypeQuartzType.NORMAL) data.setData((byte) 0);
		else {
			switch (getAxis()) {
				case X:
					data.setData((byte) (4 + quartzType.getValue()));
					break;
				case Z:
					data.setData((byte) (8 + quartzType.getValue()));
					break;
				case Y:
				default:
					data.setData((byte) quartzType.getValue());
					break;
			}
		}
		return data;
	}

	@Override
	public SpigotBlockTypeQuartz readMaterialData(MaterialData materialData) {
		super.readMaterialData(materialData);

		int data = materialData.getData();
		quartzType = EnumBlockTypeQuartzType.getByValue(data % 4).orElse(EnumBlockTypeQuartzType.NORMAL);
		switch (data / 4) {
			case 1:
				setAxis(EnumAxis.X);
				break;
			case 2:
				setAxis(EnumAxis.Z);
				break;
			case 0:
				setAxis(EnumAxis.Y);
				break;
		}
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		SpigotBlockTypeQuartz that = (SpigotBlockTypeQuartz) o;
		return quartzType == that.quartzType;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), quartzType);
	}
}
