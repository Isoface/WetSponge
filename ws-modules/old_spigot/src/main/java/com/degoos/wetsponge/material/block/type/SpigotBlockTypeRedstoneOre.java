package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.material.block.SpigotBlockTypeLightable;

public class SpigotBlockTypeRedstoneOre extends SpigotBlockTypeLightable implements WSBlockTypeRedstoneOre {


	public SpigotBlockTypeRedstoneOre(boolean lit) {
		super(73, "minecraft:redstone_ore", "minecraft:redstone_ore", 64, lit);
	}

	@Override
	public String getOldStringId() {
		return isLit() ? "minecraft:lit_redstone_ore" : "minecraft:redstone_ore";
	}

	@Override
	public SpigotBlockTypeRedstoneOre clone() {
		return new SpigotBlockTypeRedstoneOre(isLit());
	}
}
