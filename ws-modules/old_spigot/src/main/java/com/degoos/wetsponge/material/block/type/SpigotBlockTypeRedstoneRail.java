package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockTypeRailShape;
import com.degoos.wetsponge.material.block.SpigotBlockTypeRail;
import org.bukkit.material.DetectorRail;
import org.bukkit.material.MaterialData;
import org.bukkit.material.PoweredRail;

import java.util.Objects;
import java.util.Set;

public class SpigotBlockTypeRedstoneRail extends SpigotBlockTypeRail implements WSBlockTypeRedstoneRail {

	private boolean powered;

	public SpigotBlockTypeRedstoneRail(int numericalId, String oldStringId, String newStringId, int maxStackSize,
									   EnumBlockTypeRailShape shape, Set<EnumBlockTypeRailShape> allowedShapes, boolean powered) {
		super(numericalId, oldStringId, newStringId, maxStackSize, shape, allowedShapes);
		this.powered = powered;
	}

	@Override
	public boolean isPowered() {
		return powered;
	}

	@Override
	public void setPowered(boolean powered) {
		this.powered = powered;
	}

	@Override
	public SpigotBlockTypeRedstoneRail clone() {
		return new SpigotBlockTypeRedstoneRail(getNumericalId(), getOldStringId(), getNewStringId(), getMaxStackSize(), getShape(), allowedShapes(), powered);
	}

	@Override
	public MaterialData toMaterialData() {
		MaterialData data = super.toMaterialData();
		if (data instanceof PoweredRail) {
			((PoweredRail) data).setPowered(powered);
		} else if (data instanceof DetectorRail) {
			((DetectorRail) data).setPressed(powered);
		}
		return data;
	}

	@Override
	public SpigotBlockTypeRedstoneRail readMaterialData(MaterialData materialData) {
		super.readMaterialData(materialData);
		if (materialData instanceof PoweredRail) {
			powered = ((PoweredRail) materialData).isPowered();
		}
		if (materialData instanceof DetectorRail) {
			powered = ((DetectorRail) materialData).isPressed();
		}
		return this;
	}


	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		SpigotBlockTypeRedstoneRail that = (SpigotBlockTypeRedstoneRail) o;
		return powered == that.powered;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), powered);
	}
}
