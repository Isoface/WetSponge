package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import org.bukkit.material.MaterialData;

import java.util.Objects;
import java.util.Set;

public class SpigotBlockTypeRedstoneTorch extends SpigotBlockTypeTorch implements WSBlockTypeRedstoneTorch {

	private boolean lit;

	public SpigotBlockTypeRedstoneTorch(EnumBlockFace facing, Set<EnumBlockFace> faces, boolean lit) {
		super(75, "minecraft:redstone_torch", "minecraft:redstone_torch", 64, "minecraft:redstone_wall_torch", facing, faces);
		this.lit = lit;
	}

	@Override
	public int getNumericalId() {
		return lit ? 76 : 75;
	}

	@Override
	public String getOldStringId() {
		return lit ? "minecraft:redstone_torch" : "minecraft:unlit_redstone_torch";
	}


	@Override
	public boolean isLit() {
		return lit;
	}

	@Override
	public void setLit(boolean lit) {
		this.lit = lit;
	}

	@Override
	public SpigotBlockTypeRedstoneTorch clone() {
		return new SpigotBlockTypeRedstoneTorch(getFacing(), getFaces(), lit);
	}

	@Override
	public MaterialData toMaterialData() {
		return super.toMaterialData();
	}

	@Override
	public SpigotBlockTypeRedstoneTorch readMaterialData(MaterialData materialData) {
		super.readMaterialData(materialData);
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		SpigotBlockTypeRedstoneTorch that = (SpigotBlockTypeRedstoneTorch) o;
		return lit == that.lit;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), lit);
	}
}
