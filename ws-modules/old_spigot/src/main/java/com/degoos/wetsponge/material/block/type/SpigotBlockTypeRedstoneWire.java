package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.enums.block.EnumBlockTypeRedstoneWireConnection;
import com.degoos.wetsponge.material.block.SpigotBlockTypeAnaloguePowerable;
import com.degoos.wetsponge.util.Validate;
import org.bukkit.material.MaterialData;

import java.util.*;

public class SpigotBlockTypeRedstoneWire extends SpigotBlockTypeAnaloguePowerable implements WSBlockTypeRedstoneWire {

	private Map<EnumBlockFace, EnumBlockTypeRedstoneWireConnection> connections;
	private Set<EnumBlockFace> allowedFaces;

	public SpigotBlockTypeRedstoneWire(int power, int maximumPower, Map<EnumBlockFace, EnumBlockTypeRedstoneWireConnection> connections, Set<EnumBlockFace> allowedFaces) {
		super(55, "minecraft:redstone_wire", "minecraft:redstone_wire", 64, power, maximumPower);
		this.connections = connections == null ? new HashMap<>() : connections;
		this.allowedFaces = allowedFaces == null ? new HashSet<>() : allowedFaces;
	}

	@Override
	public EnumBlockTypeRedstoneWireConnection getFaceConnection(EnumBlockFace face) {
		Validate.notNull(face, "Face cannot be null!");
		return connections.getOrDefault(face, EnumBlockTypeRedstoneWireConnection.NONE);
	}

	@Override
	public void setFaceConnection(EnumBlockFace face, EnumBlockTypeRedstoneWireConnection connection) {
		Validate.notNull(face, "Face cannot be null!");
		connections.put(face, connection == null ? EnumBlockTypeRedstoneWireConnection.NONE : connection);
	}

	@Override
	public Set<EnumBlockFace> getAllowedFaces() {
		return allowedFaces;
	}

	@Override
	public SpigotBlockTypeRedstoneWire clone() {
		return new SpigotBlockTypeRedstoneWire(getPower(), gerMaximumPower(), connections, allowedFaces);
	}

	@Override
	public MaterialData toMaterialData() {
		return super.toMaterialData();
	}

	@Override
	public SpigotBlockTypeRedstoneWire readMaterialData(MaterialData materialData) {
		super.readMaterialData(materialData);
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		SpigotBlockTypeRedstoneWire that = (SpigotBlockTypeRedstoneWire) o;
		return Objects.equals(connections, that.connections) &&
				Objects.equals(allowedFaces, that.allowedFaces);
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), connections, allowedFaces);
	}
}
