package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.material.block.SpigotBlockTypeDirectional;
import org.bukkit.material.Diode;
import org.bukkit.material.MaterialData;

import java.util.Objects;
import java.util.Set;

public class SpigotBlockTypeRepeater extends SpigotBlockTypeDirectional implements WSBlockTypeRepeater {

	private int delay, minimumDelay, maximumDelay;
	private boolean locked, powered;

	public SpigotBlockTypeRepeater(EnumBlockFace facing, Set<EnumBlockFace> faces, int delay, int minimumDelay, int maximumDelay, boolean locked, boolean powered) {
		super(93, "minecraft:unpowered_repeater", "minecraft:repeater", 64, facing, faces);
		this.delay = delay;
		this.minimumDelay = minimumDelay;
		this.maximumDelay = maximumDelay;
		this.locked = locked;
		this.powered = powered;
	}

	@Override
	public int getNumericalId() {
		return powered ? 94 : 93;
	}

	@Override
	public String getOldStringId() {
		return powered ? "minecraft:powered_repeater" : "minecraft:unpowered_repeater";
	}

	@Override
	public int getDelay() {
		return delay;
	}

	@Override
	public void setDelay(int delay) {
		this.delay = Math.max(minimumDelay, Math.min(minimumDelay, delay));
	}

	@Override
	public int getMinimumDelay() {
		return minimumDelay;
	}

	@Override
	public int getMaximumDelay() {
		return maximumDelay;
	}

	@Override
	public boolean isLocked() {
		return locked;
	}

	@Override
	public void setLocked(boolean locked) {
		this.locked = locked;
	}

	@Override
	public boolean isPowered() {
		return powered;
	}

	@Override
	public void setPowered(boolean powered) {
		this.powered = powered;
	}

	@Override
	public SpigotBlockTypeRepeater clone() {
		return new SpigotBlockTypeRepeater(getFacing(), getFaces(), delay, minimumDelay, maximumDelay, locked, powered);
	}

	@Override
	public MaterialData toMaterialData() {
		MaterialData data = super.toMaterialData();
		if (data instanceof Diode) {
			((Diode) data).setDelay(delay);
		}
		return data;
	}

	@Override
	public SpigotBlockTypeRepeater readMaterialData(MaterialData materialData) {
		super.readMaterialData(materialData);
		if (materialData instanceof Diode) {
			delay = ((Diode) materialData).getDelay();
		}
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		SpigotBlockTypeRepeater that = (SpigotBlockTypeRepeater) o;
		return delay == that.delay &&
				minimumDelay == that.minimumDelay &&
				maximumDelay == that.maximumDelay &&
				locked == that.locked &&
				powered == that.powered;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), delay, minimumDelay, maximumDelay, locked, powered);
	}
}
