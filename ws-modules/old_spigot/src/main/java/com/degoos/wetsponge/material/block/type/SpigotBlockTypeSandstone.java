package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockTypeSandType;
import com.degoos.wetsponge.enums.block.EnumBlockTypeSandstoneType;
import com.degoos.wetsponge.material.block.SpigotBlockType;
import com.degoos.wetsponge.util.Validate;
import org.bukkit.material.MaterialData;

import java.util.Objects;

public class SpigotBlockTypeSandstone extends SpigotBlockType implements WSBlockTypeSandstone {

	private EnumBlockTypeSandType sandType;
	private EnumBlockTypeSandstoneType sandstoneType;

	public SpigotBlockTypeSandstone(EnumBlockTypeSandType sandType, EnumBlockTypeSandstoneType sandstoneType) {
		super(24, "minecraft:sandstone", "minecraft:sandstone", 64);
		Validate.notNull(sandType, "Sand type cannot be null!");
		Validate.notNull(sandstoneType, "Sandstone type cannot be null!");
		this.sandType = sandType;
		this.sandstoneType = sandstoneType;
	}

	@Override
	public int getNumericalId() {
		return sandType == EnumBlockTypeSandType.NORMAL ? 24 : 179;
	}

	@Override
	public String getOldStringId() {
		return sandType == EnumBlockTypeSandType.RED ? "minecraft:red_sandstone" : "minecraft:sandstone";
	}

	@Override
	public String getNewStringId() {
		String prefix = sandstoneType == EnumBlockTypeSandstoneType.DEFAULT ? "" : sandstoneType == EnumBlockTypeSandstoneType.SMOOTH ? "cut_" : "chiseled_";
		return prefix + (sandType == EnumBlockTypeSandType.RED ? "red_sandstone" : "sandstone");
	}

	@Override
	public EnumBlockTypeSandType getSandType() {
		return sandType;
	}

	@Override
	public void setSandType(EnumBlockTypeSandType sandType) {
		Validate.notNull(sandType, "Sand type cannot be null!");
		this.sandType = sandType;
	}

	@Override
	public EnumBlockTypeSandstoneType getSandstoneType() {
		return sandstoneType;
	}

	@Override
	public void setSandstoneType(EnumBlockTypeSandstoneType sandstoneType) {
		Validate.notNull(sandstoneType, "Sandstone type cannot be null!");
		this.sandstoneType = sandstoneType;
	}

	@Override
	public SpigotBlockTypeSandstone clone() {
		return new SpigotBlockTypeSandstone(sandType, sandstoneType);
	}

	@Override
	public MaterialData toMaterialData() {
		MaterialData data = super.toMaterialData();
		data.setData((byte) sandstoneType.getValue());
		return data;
	}

	@Override
	public SpigotBlockTypeSandstone readMaterialData(MaterialData materialData) {
		super.readMaterialData(materialData);
		sandstoneType = EnumBlockTypeSandstoneType.getByValue(materialData.getData()).orElse(EnumBlockTypeSandstoneType.DEFAULT);
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		SpigotBlockTypeSandstone that = (SpigotBlockTypeSandstone) o;
		return sandType == that.sandType &&
				sandstoneType == that.sandstoneType;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), sandType, sandstoneType);
	}
}
