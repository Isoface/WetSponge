package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.material.block.SpigotBlockTypeWaterlogged;
import org.bukkit.material.MaterialData;

import java.util.Objects;

public class SpigotBlockTypeSeaPickle extends SpigotBlockTypeWaterlogged implements WSBlockTypeSeaPickle {

	private int pickles, minimumPickles, maximumPickles;

	public SpigotBlockTypeSeaPickle(boolean waterLogged, int pickles, int minimumPickles, int maximumPickles) {
		super(-1, null, "minecraft:sea_pickle", 64, waterLogged);
		this.pickles = pickles;
		this.minimumPickles = minimumPickles;
		this.maximumPickles = maximumPickles;
	}

	@Override
	public int getPickles() {
		return pickles;
	}

	@Override
	public void setPickles(int pickles) {
		this.pickles = Math.min(maximumPickles, Math.max(minimumPickles, pickles));
	}

	@Override
	public int getMinimumPickles() {
		return minimumPickles;
	}

	@Override
	public int getMaximumPickles() {
		return maximumPickles;
	}

	@Override
	public SpigotBlockTypeSeaPickle clone() {
		return new SpigotBlockTypeSeaPickle(isWaterlogged(), pickles, minimumPickles, maximumPickles);
	}

	@Override
	public MaterialData toMaterialData() {
		return super.toMaterialData();
	}

	@Override
	public SpigotBlockTypeSeaPickle readMaterialData(MaterialData materialData) {
		super.readMaterialData(materialData);
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		SpigotBlockTypeSeaPickle that = (SpigotBlockTypeSeaPickle) o;
		return pickles == that.pickles &&
				minimumPickles == that.minimumPickles &&
				maximumPickles == that.maximumPickles;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), pickles, minimumPickles, maximumPickles);
	}
}
