package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.material.block.SpigotBlockTypeRotatable;
import org.bukkit.block.BlockFace;
import org.bukkit.material.MaterialData;
import org.bukkit.material.Sign;

import java.util.Objects;

public class SpigotBlockTypeSign extends SpigotBlockTypeRotatable implements WSBlockTypeSign {

	private boolean waterlogged;

	public SpigotBlockTypeSign(EnumBlockFace rotation, boolean waterlogged) {
		super(63, "minecraft:standing_sign", "minecraft:sign", 64, rotation);
		this.waterlogged = waterlogged;
	}

	@Override
	public boolean isWaterlogged() {
		return waterlogged;
	}

	@Override
	public void setWaterlogged(boolean waterlogged) {
		this.waterlogged = waterlogged;
	}

	@Override
	public SpigotBlockTypeSign clone() {
		return new SpigotBlockTypeSign(getRotation(), waterlogged);
	}

	@Override
	public MaterialData toMaterialData() {
		MaterialData data = super.toMaterialData();
		if (data instanceof Sign) {
			((Sign) data).setFacingDirection(BlockFace.valueOf(getRotation().name()));
		}
		return data;
	}

	@Override
	public SpigotBlockTypeSign readMaterialData(MaterialData materialData) {
		super.readMaterialData(materialData);
		if (materialData instanceof Sign) {
			setRotation(EnumBlockFace.valueOf(((Sign) materialData).getFacing().name()));
		}
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		SpigotBlockTypeSign that = (SpigotBlockTypeSign) o;
		return waterlogged == that.waterlogged;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), waterlogged);
	}
}
