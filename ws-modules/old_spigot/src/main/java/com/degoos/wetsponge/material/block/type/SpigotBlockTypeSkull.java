package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.enums.block.EnumBlockTypeSkullType;
import com.degoos.wetsponge.material.block.SpigotBlockTypeDirectional;
import com.degoos.wetsponge.util.Validate;
import org.bukkit.material.MaterialData;

import java.util.Objects;
import java.util.Set;

public class SpigotBlockTypeSkull extends SpigotBlockTypeDirectional implements WSBlockTypeSkull {

	private EnumBlockFace rotation;
	private EnumBlockTypeSkullType skullType;

	public SpigotBlockTypeSkull(EnumBlockFace facing, Set<EnumBlockFace> faces, EnumBlockFace rotation, EnumBlockTypeSkullType skullType) {
		super(144, "minecraft:skull", "minecraft:skull", 64, facing, faces);
		Validate.notNull(rotation, "Rotation cannot be null!");
		Validate.notNull(skullType, "Skull type cannot be null!");
		this.rotation = rotation;
		this.skullType = skullType;
	}

	@Override
	public String getNewStringId() {
		return "minecraft:" + skullType.getMinecraftName() + (getFacing() != EnumBlockFace.DOWN ? "_wall" : "") + "_head";
	}

	@Override
	public EnumBlockTypeSkullType getSkullType() {
		return skullType;
	}

	@Override
	public void setSkullType(EnumBlockTypeSkullType skullType) {
		Validate.notNull(skullType, "Skull type cannot be null!");
		this.skullType = skullType;
	}

	@Override
	public EnumBlockFace getRotation() {
		return rotation;
	}

	@Override
	public void setRotation(EnumBlockFace rotation) {
		Validate.notNull(rotation, "Rotation cannot be null!");
		this.rotation = rotation;
	}

	@Override
	public Set<EnumBlockFace> getFaces() {
		Set<EnumBlockFace> faces = super.getFaces();
		faces.add(EnumBlockFace.UP);
		return faces;
	}

	@Override
	public SpigotBlockTypeSkull clone() {
		return new SpigotBlockTypeSkull(getFacing(), getFaces(), rotation, skullType);
	}

	@Override
	public MaterialData toMaterialData() {
		return super.toMaterialData();
	}

	@Override
	public SpigotBlockTypeSkull readMaterialData(MaterialData materialData) {
		super.readMaterialData(materialData);
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		SpigotBlockTypeSkull that = (SpigotBlockTypeSkull) o;
		return rotation == that.rotation &&
				skullType == that.skullType;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), rotation, skullType);
	}
}
