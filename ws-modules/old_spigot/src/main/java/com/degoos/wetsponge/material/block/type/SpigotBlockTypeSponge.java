package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.material.block.SpigotBlockType;
import org.bukkit.material.MaterialData;

import java.util.Objects;

public class SpigotBlockTypeSponge extends SpigotBlockType implements WSBlockTypeSponge {

	private boolean wet;

	public SpigotBlockTypeSponge(boolean wet) {
		super(19, "minecraft:sponge", "minecraft:sponge", 64);
		this.wet = wet;
	}

	@Override
	public String getNewStringId() {
		return wet ? "minecraft:wet_sponge" : "minecraft:sponge";
	}

	@Override
	public boolean isWet() {
		return wet;
	}

	@Override
	public void setWet(boolean wet) {
		this.wet = wet;
	}

	@Override
	public SpigotBlockTypeSponge clone() {
		return new SpigotBlockTypeSponge(wet);
	}

	@Override
	public MaterialData toMaterialData() {
		MaterialData data = super.toMaterialData();
		data.setData((byte) (wet ? 1 : 0));
		return data;
	}

	@Override
	public SpigotBlockTypeSponge readMaterialData(MaterialData materialData) {
		super.readMaterialData(materialData);
		wet = materialData.getData() > 0;
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		SpigotBlockTypeSponge that = (SpigotBlockTypeSponge) o;
		return wet == that.wet;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), wet);
	}
}
