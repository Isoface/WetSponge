package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.enums.block.EnumBlockTypeBisectedHalf;
import com.degoos.wetsponge.enums.block.EnumBlockTypeStairShape;
import com.degoos.wetsponge.material.block.SpigotBlockTypeDirectional;
import com.degoos.wetsponge.util.Validate;
import org.bukkit.material.MaterialData;
import org.bukkit.material.Stairs;

import java.util.Objects;
import java.util.Set;

public class SpigotBlockTypeStairs extends SpigotBlockTypeDirectional implements WSBlockTypeStairs {


	private EnumBlockTypeStairShape shape;
	private EnumBlockTypeBisectedHalf half;
	private boolean waterlogged;

	public SpigotBlockTypeStairs(int numericalId, String oldStringId, String newStringId, int maxStackSize, EnumBlockFace facing, Set<EnumBlockFace> faces,
								 EnumBlockTypeStairShape shape, EnumBlockTypeBisectedHalf half, boolean waterlogged) {
		super(numericalId, oldStringId, newStringId, maxStackSize, facing, faces);
		Validate.notNull(shape, "Shape cannot be null!");
		Validate.notNull(half, "Half cannot be null!");
		this.shape = shape;
		this.half = half;
		this.waterlogged = waterlogged;
	}

	@Override
	public EnumBlockTypeStairShape getShape() {
		return shape;
	}

	@Override
	public void setShape(EnumBlockTypeStairShape shape) {
		Validate.notNull(shape, "Shape cannot be null!");
		this.shape = shape;
	}

	@Override
	public EnumBlockTypeBisectedHalf getHalf() {
		return half;
	}

	@Override
	public void setHalf(EnumBlockTypeBisectedHalf half) {
		Validate.notNull(half, "Half cannot be null!");
		this.half = half;
	}

	@Override
	public boolean isWaterlogged() {
		return waterlogged;
	}

	@Override
	public void setWaterlogged(boolean waterlogged) {
		this.waterlogged = waterlogged;
	}

	@Override
	public SpigotBlockTypeStairs clone() {
		return new SpigotBlockTypeStairs(getNumericalId(), getOldStringId(), getNewStringId(), getMaxStackSize(), getFacing(), getFaces(), shape, half, waterlogged);
	}

	@Override
	public MaterialData toMaterialData() {
		MaterialData data = super.toMaterialData();
		if (data instanceof Stairs) {
			((Stairs) data).setInverted(half == EnumBlockTypeBisectedHalf.TOP);
		}
		return data;
	}

	@Override
	public SpigotBlockTypeStairs readMaterialData(MaterialData materialData) {
		super.readMaterialData(materialData);
		if (materialData instanceof Stairs) {
			half = ((Stairs) materialData).isInverted() ? EnumBlockTypeBisectedHalf.TOP : EnumBlockTypeBisectedHalf.BOTTOM;
		}
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		SpigotBlockTypeStairs that = (SpigotBlockTypeStairs) o;
		return waterlogged == that.waterlogged &&
				shape == that.shape &&
				half == that.half;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), shape, half, waterlogged);
	}
}
