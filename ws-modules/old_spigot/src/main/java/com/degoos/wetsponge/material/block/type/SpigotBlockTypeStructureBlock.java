package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockTypeStructureBlockMode;
import com.degoos.wetsponge.material.block.SpigotBlockType;
import com.degoos.wetsponge.util.Validate;
import org.bukkit.material.MaterialData;

import java.util.Objects;

public class SpigotBlockTypeStructureBlock extends SpigotBlockType implements WSBlockTypeStructureBlock {

	private EnumBlockTypeStructureBlockMode mode;

	public SpigotBlockTypeStructureBlock(EnumBlockTypeStructureBlockMode mode) {
		super(255, "minecraft:structure_block", "minecraft:structure_block", 64);
		Validate.notNull(mode, "Mode cannot be null!");
		this.mode = mode;
	}

	@Override
	public EnumBlockTypeStructureBlockMode getMode() {
		return mode;
	}

	@Override
	public void setMode(EnumBlockTypeStructureBlockMode mode) {
		Validate.notNull(mode, "Mode cannot be null!");
		this.mode = mode;
	}

	@Override
	public SpigotBlockTypeStructureBlock clone() {
		return new SpigotBlockTypeStructureBlock(mode);
	}

	@Override
	public MaterialData toMaterialData() {
		MaterialData data = super.toMaterialData();
		data.setData((byte) mode.getValue());
		return data;
	}

	@Override
	public SpigotBlockTypeStructureBlock readMaterialData(MaterialData materialData) {
		super.readMaterialData(materialData);
		mode = EnumBlockTypeStructureBlockMode.getByValue(materialData.getData()).orElse(EnumBlockTypeStructureBlockMode.DATA);
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		SpigotBlockTypeStructureBlock that = (SpigotBlockTypeStructureBlock) o;
		return mode == that.mode;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), mode);
	}
}
