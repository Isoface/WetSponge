package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.enums.block.EnumBlockTypeSwitchFace;
import com.degoos.wetsponge.material.block.SpigotBlockTypeDirectional;
import com.degoos.wetsponge.util.Validate;
import org.bukkit.material.Button;
import org.bukkit.material.Lever;
import org.bukkit.material.MaterialData;
import org.bukkit.material.Redstone;

import java.util.Objects;
import java.util.Set;

public class SpigotBlockTypeSwitch extends SpigotBlockTypeDirectional implements WSBlockTypeSwitch {

	private EnumBlockTypeSwitchFace switchFace;
	private boolean powered;

	public SpigotBlockTypeSwitch(int numericalId, String oldStringId, String newStringId, int maxStackSize,
								 EnumBlockFace facing, Set<EnumBlockFace> faces, EnumBlockTypeSwitchFace switchFace, boolean powered) {
		super(numericalId, oldStringId, newStringId, maxStackSize, facing, faces);
		Validate.notNull(switchFace, "Switch face cannot be null!");
		this.switchFace = switchFace;
		this.powered = powered;
	}

	@Override
	public EnumBlockTypeSwitchFace getSwitchFace() {
		return switchFace;
	}

	@Override
	public void setSwitchFace(EnumBlockTypeSwitchFace face) {
		Validate.notNull(face, "Switch face cannot be null!");
		this.switchFace = face;
	}

	@Override
	public boolean isPowered() {
		return powered;
	}

	@Override
	public void setPowered(boolean powered) {
		this.powered = powered;
	}

	@Override
	public SpigotBlockTypeSwitch clone() {
		return new SpigotBlockTypeSwitch(getNumericalId(), getOldStringId(), getNewStringId(), getMaxStackSize(), getFacing(), getFaces(), switchFace, powered);
	}

	@Override
	public MaterialData toMaterialData() {
		MaterialData data = super.toMaterialData();
		if (data instanceof Lever) {
			((Lever) data).setPowered(powered);
		}
		if (data instanceof Button) {
			((Button) data).setPowered(powered);
		}
		return data;
	}

	@Override
	public SpigotBlockTypeSwitch readMaterialData(MaterialData materialData) {
		super.readMaterialData(materialData);
		if (materialData instanceof Redstone) {
			powered = ((Redstone) materialData).isPowered();
		}
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		SpigotBlockTypeSwitch that = (SpigotBlockTypeSwitch) o;
		return powered == that.powered &&
				switchFace == that.switchFace;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), switchFace, powered);
	}
}
