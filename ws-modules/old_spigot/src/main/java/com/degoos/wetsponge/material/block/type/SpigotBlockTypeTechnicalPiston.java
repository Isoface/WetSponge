package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.enums.block.EnumBlockTypePistonType;
import com.degoos.wetsponge.material.block.SpigotBlockTypeDirectional;
import org.bukkit.material.MaterialData;
import org.bukkit.material.PistonExtensionMaterial;

import java.util.Objects;
import java.util.Set;

public class SpigotBlockTypeTechnicalPiston extends SpigotBlockTypeDirectional implements WSBlockTypeTechnicalPiston {

	private EnumBlockTypePistonType pistonType;

	public SpigotBlockTypeTechnicalPiston(int numericalId, String oldStringId, String newStringId, int maxStackSize, EnumBlockFace facing, Set<EnumBlockFace> faces, EnumBlockTypePistonType pistonType) {
		super(numericalId, oldStringId, newStringId, maxStackSize, facing, faces);
		this.pistonType = pistonType;
	}

	@Override
	public EnumBlockTypePistonType getType() {
		return pistonType;
	}

	@Override
	public void setType(EnumBlockTypePistonType type) {
		this.pistonType = type;
	}

	@Override
	public SpigotBlockTypeTechnicalPiston clone() {
		return new SpigotBlockTypeTechnicalPiston(getNumericalId(), getOldStringId(), getNewStringId(), getMaxStackSize(), getFacing(), getFaces(), pistonType);
	}

	@Override
	public MaterialData toMaterialData() {
		MaterialData data = super.toMaterialData();
		if (data instanceof PistonExtensionMaterial) {
			((PistonExtensionMaterial) data).setSticky(pistonType == EnumBlockTypePistonType.STICKY);
		}
		return data;
	}

	@Override
	public SpigotBlockTypeTechnicalPiston readMaterialData(MaterialData materialData) {
		super.readMaterialData(materialData);
		if (materialData instanceof PistonExtensionMaterial) {
			pistonType = ((PistonExtensionMaterial) materialData).isSticky() ? EnumBlockTypePistonType.STICKY : EnumBlockTypePistonType.NORMAL;
		}
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		SpigotBlockTypeTechnicalPiston that = (SpigotBlockTypeTechnicalPiston) o;
		return pistonType == that.pistonType;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), pistonType);
	}
}
