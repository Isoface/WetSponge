package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.material.block.SpigotBlockTypeDirectional;
import org.bukkit.material.MaterialData;

import java.util.Objects;
import java.util.Set;

public class SpigotBlockTypeTorch extends SpigotBlockTypeDirectional implements WSBlockTypeTorch {

	private String wallNewStringId;

	public SpigotBlockTypeTorch(int numericalId, String oldStringId, String newStringId,
								int maxStackSize, String wallNewStringId, EnumBlockFace facing, Set<EnumBlockFace> faces) {
		super(numericalId, oldStringId, newStringId, maxStackSize, facing, faces);
		this.wallNewStringId = wallNewStringId;
	}

	@Override
	public String getNewStringId() {
		return getFacing() == EnumBlockFace.UP ? wallNewStringId : super.getNewStringId();
	}

	@Override
	public Set<EnumBlockFace> getFaces() {
		Set<EnumBlockFace> faces = super.getFaces();
		faces.add(EnumBlockFace.UP);
		return faces;
	}

	@Override
	public SpigotBlockTypeTorch clone() {
		return new SpigotBlockTypeTorch(getNumericalId(), getOldStringId(), getNewStringId(), getMaxStackSize(), wallNewStringId, getFacing(), getFaces());
	}

	@Override
	public MaterialData toMaterialData() {
		return super.toMaterialData();
	}

	@Override
	public SpigotBlockTypeTorch readMaterialData(MaterialData materialData) {
		super.readMaterialData(materialData);
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		SpigotBlockTypeTorch that = (SpigotBlockTypeTorch) o;
		return Objects.equals(wallNewStringId, that.wallNewStringId);
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), wallNewStringId);
	}
}
