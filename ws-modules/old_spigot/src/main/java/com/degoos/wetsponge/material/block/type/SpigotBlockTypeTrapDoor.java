package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.enums.block.EnumBlockTypeBisectedHalf;
import com.degoos.wetsponge.material.block.SpigotBlockTypeDirectional;
import com.degoos.wetsponge.util.Validate;
import org.bukkit.material.MaterialData;
import org.bukkit.material.TrapDoor;

import java.util.Objects;
import java.util.Set;

public class SpigotBlockTypeTrapDoor extends SpigotBlockTypeDirectional implements WSBlockTypeTrapDoor {

	private EnumBlockTypeBisectedHalf half;
	private boolean open, powered, waterlogged;

	public SpigotBlockTypeTrapDoor(int numericalId, String oldStringId, String newStringId, int maxStackSize, EnumBlockFace facing,
								   Set<EnumBlockFace> faces, EnumBlockTypeBisectedHalf half, boolean open, boolean powered, boolean waterlogged) {
		super(numericalId, oldStringId, newStringId, maxStackSize, facing, faces);
		Validate.notNull(half, "Half cannot be null!");
		this.half = half;
		this.open = open;
		this.powered = powered;
		this.waterlogged = waterlogged;
	}

	@Override
	public EnumBlockTypeBisectedHalf getHalf() {
		return half;
	}

	@Override
	public void setHalf(EnumBlockTypeBisectedHalf half) {
		Validate.notNull(half, "Half cannot be null!");
		this.half = half;
	}

	@Override
	public boolean isOpen() {
		return open;
	}

	@Override
	public void setOpen(boolean open) {
		this.open = open;
	}

	@Override
	public boolean isPowered() {
		return powered;
	}

	@Override
	public void setPowered(boolean powered) {
		this.powered = powered;
	}

	@Override
	public boolean isWaterlogged() {
		return waterlogged;
	}

	@Override
	public void setWaterlogged(boolean waterlogged) {
		this.waterlogged = waterlogged;
	}

	@Override
	public SpigotBlockTypeTrapDoor clone() {
		return new SpigotBlockTypeTrapDoor(getNumericalId(), getOldStringId(), getNewStringId(),
				getMaxStackSize(), getFacing(), getFaces(), half, open, powered, waterlogged);
	}

	@Override
	public MaterialData toMaterialData() {
		MaterialData data = super.toMaterialData();
		if (data instanceof TrapDoor) {
			((TrapDoor) data).setInverted(half == EnumBlockTypeBisectedHalf.TOP);
			((TrapDoor) data).setOpen(open);
		}
		return data;
	}

	@Override
	public SpigotBlockTypeTrapDoor readMaterialData(MaterialData materialData) {
		super.readMaterialData(materialData);
		if (materialData instanceof TrapDoor) {
			half = ((TrapDoor) materialData).isInverted() ? EnumBlockTypeBisectedHalf.TOP : EnumBlockTypeBisectedHalf.BOTTOM;
			open = ((TrapDoor) materialData).isOpen();
		}
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		SpigotBlockTypeTrapDoor that = (SpigotBlockTypeTrapDoor) o;
		return open == that.open &&
				powered == that.powered &&
				waterlogged == that.waterlogged &&
				half == that.half;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), half, open, powered, waterlogged);
	}
}
