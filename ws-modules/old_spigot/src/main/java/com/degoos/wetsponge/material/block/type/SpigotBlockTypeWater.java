package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.material.block.SpigotBlockTypeLevelled;
import org.bukkit.material.MaterialData;

public class SpigotBlockTypeWater extends SpigotBlockTypeLevelled implements WSBlockTypeWater {

	public SpigotBlockTypeWater(int level, int maximumLevel) {
		super(9, "minecraft:water", "minecraft:water", 64, level, maximumLevel);
	}

	@Override
	public int getNumericalId() {
		return getLevel() == 0 ? 9 : 8;
	}

	@Override
	public String getOldStringId() {
		return getLevel() == 0 ? "minecraft:water" : "minecraft:flowing_water";
	}

	@Override
	public SpigotBlockTypeWater clone() {
		return new SpigotBlockTypeWater(getLevel(), getMaximumLevel());
	}

	@Override
	public SpigotBlockTypeWater readMaterialData(MaterialData materialData) {
		super.readMaterialData(materialData);
		return this;
	}
}
