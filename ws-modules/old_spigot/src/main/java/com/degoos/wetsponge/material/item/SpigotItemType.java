package com.degoos.wetsponge.material.item;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.material.SpigotMaterial;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;
import org.bukkit.Material;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.material.MaterialData;

import java.util.Objects;

public class SpigotItemType implements SpigotMaterial, WSItemType {


	private int numericalId;
	private String oldStringId, newStringId;
	private int maxStackSize;

	public SpigotItemType(int numericalId, String oldStringId, String newStringId, int maxStackSize) {
		this.numericalId = numericalId < 0 ? -1 : numericalId;
		this.oldStringId = oldStringId == null || oldStringId.equals("") ? null : oldStringId;
		this.newStringId = newStringId;
		this.maxStackSize = Math.max(1, maxStackSize);
	}

	@Override
	public SpigotItemType clone() {
		return new SpigotItemType(numericalId, oldStringId, newStringId, maxStackSize);
	}

	@Override
	public int getNumericalId() {
		return numericalId;
	}

	@Override
	public String getStringId() {
		return WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13) ? getOldStringId() : getNewStringId();
	}

	@Override
	public String getNewStringId() {
		return newStringId;
	}

	@Override
	public String getOldStringId() {
		return oldStringId;
	}

	@Override
	public int getMaxStackSize() {
		return maxStackSize;
	}

	@Override
	public void writeItemMeta(ItemMeta itemMeta) {
	}

	@Override
	public void readItemMeta(ItemMeta itemMeta) {
	}

	@Override
	public MaterialData toMaterialData() {
		return Material.getMaterial(getNumericalId()).getNewData((byte) 0);
	}

	@Override
	public SpigotItemType readMaterialData(MaterialData materialData) {
		return this;
	}

	@Override
	public void writeNBTTag(WSNBTTagCompound compound) {

	}

	@Override
	public void readNBTTag(WSNBTTagCompound compound) {

	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		SpigotItemType that = (SpigotItemType) o;
		return numericalId == that.numericalId &&
				maxStackSize == that.maxStackSize &&
				Objects.equals(oldStringId, that.oldStringId) &&
				Objects.equals(newStringId, that.newStringId);
	}

	@Override
	public int hashCode() {

		return Objects.hash(numericalId, oldStringId, newStringId, maxStackSize);
	}
}
