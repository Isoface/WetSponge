package com.degoos.wetsponge.material.item.type;

import com.degoos.wetsponge.enums.item.EnumItemTypeCoalType;
import com.degoos.wetsponge.material.item.SpigotItemType;
import com.degoos.wetsponge.util.Validate;

import java.util.Objects;

public class SpigotItemTypeCoal extends SpigotItemType implements WSItemTypeCoal {

	private EnumItemTypeCoalType coalType;

	public SpigotItemTypeCoal(EnumItemTypeCoalType coalType) {
		super(263, "minecraft:coal", "minecraft:coal", 64);
		Validate.notNull(coalType, "Coal type cannot be null!");
		this.coalType = coalType;
	}

	@Override
	public String getNewStringId() {
		return coalType == EnumItemTypeCoalType.COAL ? "minecraft:coal" : "minecraft:charcoal";
	}

	@Override
	public EnumItemTypeCoalType getCoalType() {
		return coalType;
	}

	@Override
	public void setCoalType(EnumItemTypeCoalType coalType) {
		Validate.notNull(coalType, "Coal type cannot be null!");
		this.coalType = coalType;
	}

	@Override
	public SpigotItemTypeCoal clone() {
		return new SpigotItemTypeCoal(coalType);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		SpigotItemTypeCoal that = (SpigotItemTypeCoal) o;
		return coalType == that.coalType;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), coalType);
	}
}
