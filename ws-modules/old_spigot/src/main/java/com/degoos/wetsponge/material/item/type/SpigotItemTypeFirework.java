package com.degoos.wetsponge.material.item.type;

import com.degoos.wetsponge.firework.SpigotFireworkEffect;
import com.degoos.wetsponge.firework.WSFireworkEffect;
import com.degoos.wetsponge.material.item.SpigotItemType;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class SpigotItemTypeFirework extends SpigotItemType implements WSItemTypeFirework {

	private int power;
	private List<WSFireworkEffect> effects;

	public SpigotItemTypeFirework(int power, List<WSFireworkEffect> effects) {
		super(401, "minecraft:fireworks", "minecraft:firework_rocket", 64);
		this.power = power;
		this.effects = effects == null ? new ArrayList<>() : new ArrayList<>(effects);
	}

	@Override
	public int getPower() {
		return power;
	}

	@Override
	public void setPower(int power) {
		this.power = power;
	}

	@Override
	public List<WSFireworkEffect> getEffects() {
		return effects;
	}

	@Override
	public void setEffects(List<WSFireworkEffect> effects) {
		this.effects = effects == null ? new ArrayList<>() : effects;
	}

	@Override
	public SpigotItemTypeFirework clone() {
		return new SpigotItemTypeFirework(power, effects);
	}

	@Override
	public void writeItemMeta(ItemMeta itemMeta) {
		super.writeItemMeta(itemMeta);
		if (itemMeta instanceof FireworkMeta) {
			((FireworkMeta) itemMeta).setPower(power);
			((FireworkMeta) itemMeta).clearEffects();
			((FireworkMeta) itemMeta).addEffects(effects.stream().map(target -> ((SpigotFireworkEffect) target).getHandled()).collect(Collectors.toList()));
		}
	}

	@Override
	public void readItemMeta(ItemMeta itemMeta) {
		super.readItemMeta(itemMeta);
		if (itemMeta instanceof FireworkMeta) {
			power = ((FireworkMeta) itemMeta).getPower();
			effects = ((FireworkMeta) itemMeta).getEffects().stream().map(SpigotFireworkEffect::new).collect(Collectors.toList());
		}
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		SpigotItemTypeFirework that = (SpigotItemTypeFirework) o;
		return power == that.power &&
				Objects.equals(effects, that.effects);
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), power, effects);
	}
}
