package com.degoos.wetsponge.material.item.type;

import com.degoos.wetsponge.enums.item.EnumItemTypeFishType;
import com.degoos.wetsponge.material.item.SpigotItemType;
import com.degoos.wetsponge.util.Validate;

import java.util.Objects;

public class SpigotItemTypeFish extends SpigotItemType implements WSItemTypeFish {

	private EnumItemTypeFishType fishType;

	public SpigotItemTypeFish(EnumItemTypeFishType fishType) {
		super(263, "minecraft:fish", "minecraft:cod", 64);
		Validate.notNull(fishType, "Fish type cannot be null!");
		this.fishType = fishType;
	}

	@Override
	public String getNewStringId() {
		switch (fishType) {
			case SALMON:
				return "minecraft:salmon";
			case TROPICAL_FISH:
				return "minecraft:tropical_fish";
			case PUFFERFISH:
				return "minecraft:pufferfish";
			case COD:
			default:
				return "minecraft:cod";
		}
	}

	@Override
	public EnumItemTypeFishType getFishType() {
		return fishType;
	}

	@Override
	public void setFishType(EnumItemTypeFishType fishType) {
		Validate.notNull(fishType, "Fish type cannot be null!");
		this.fishType = fishType;
	}

	@Override
	public SpigotItemTypeFish clone() {
		return new SpigotItemTypeFish(fishType);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		SpigotItemTypeFish that = (SpigotItemTypeFish) o;
		return fishType == that.fishType;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), fishType);
	}
}
