package com.degoos.wetsponge.material.item.type;

import com.degoos.wetsponge.color.WSColor;
import org.bukkit.Color;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;

import java.util.Objects;
import java.util.Optional;

public class SpigotItemTypeLeatherArmor extends SpigotItemTypeDamageable implements WSItemTypeLeatherArmor {

	private WSColor color;

	public SpigotItemTypeLeatherArmor(int numericalId, String oldStringId, String newStringId, int damage, int maxUses, WSColor color) {
		super(numericalId, oldStringId, newStringId, damage, maxUses);
		this.color = color;
	}


	@Override
	public WSColor getColor() {
		return color;
	}

	@Override
	public void setColor(WSColor color) {
		this.color = color;
	}

	@Override
	public SpigotItemTypeLeatherArmor clone() {
		return new SpigotItemTypeLeatherArmor(getNumericalId(), getOldStringId(), getNewStringId(), getDamage(), getMaxUses(), color);
	}

	@Override
	public void writeItemMeta(ItemMeta itemMeta) {
		super.writeItemMeta(itemMeta);
		if (itemMeta instanceof LeatherArmorMeta) {
			((LeatherArmorMeta) itemMeta).setColor(color == null ? null : Color.fromBGR(color.toRGB()));
		}
	}

	@Override
	public void readItemMeta(ItemMeta itemMeta) {
		super.readItemMeta(itemMeta);
		if (itemMeta instanceof LeatherArmorMeta) {
			color = Optional.of(((LeatherArmorMeta) itemMeta).getColor()).map(target -> WSColor.ofRGB(target.asRGB())).orElse(null);
		}
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		SpigotItemTypeLeatherArmor that = (SpigotItemTypeLeatherArmor) o;
		return Objects.equals(color, that.color);
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), color);
	}
}
