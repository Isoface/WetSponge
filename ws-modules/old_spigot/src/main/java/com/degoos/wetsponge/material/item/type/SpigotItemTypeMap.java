package com.degoos.wetsponge.material.item.type;

import com.degoos.wetsponge.color.WSColor;
import com.degoos.wetsponge.material.item.SpigotItemType;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;
import org.bukkit.Color;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.MapMeta;

import java.util.Objects;
import java.util.Optional;

public class SpigotItemTypeMap extends SpigotItemType implements WSItemTypeMap {

	private int mapId;
	private WSColor color;

	public SpigotItemTypeMap(int mapId, WSColor color) {
		super(358, "minecraft:filled_map", "minecraft:filled_map", 1);
		this.mapId = mapId;
		this.color = color;
	}

	@Override
	public int getMapId() {
		return mapId;
	}

	@Override
	public void setMapId(int mapId) {
		this.mapId = Math.max(0, mapId);
	}

	@Override
	public WSColor getMapColor() {
		return color;
	}

	@Override
	public void setMapColor(WSColor mapColor) {
		this.color = mapColor;
	}

	@Override
	public SpigotItemTypeMap clone() {
		return new SpigotItemTypeMap(mapId, color);
	}

	@Override
	public void writeItemMeta(ItemMeta itemMeta) {
		super.writeItemMeta(itemMeta);
		if (itemMeta instanceof MapMeta) {
			((MapMeta) itemMeta).setColor(color == null ? null : Color.fromBGR(color.toRGB()));
		}
	}

	@Override
	public void readItemMeta(ItemMeta itemMeta) {
		super.readItemMeta(itemMeta);
		if (itemMeta instanceof MapMeta) {
			color = Optional.ofNullable(((MapMeta) itemMeta).getColor()).map(target -> WSColor.ofRGB(target.asRGB())).orElse(null);
		}
	}

	@Override
	public void writeNBTTag(WSNBTTagCompound compound) {
		super.writeNBTTag(compound);
		compound.setInteger("map", mapId);
	}

	@Override
	public void readNBTTag(WSNBTTagCompound compound) {
		super.readNBTTag(compound);
		mapId = compound.getInteger("map");
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		SpigotItemTypeMap that = (SpigotItemTypeMap) o;
		return mapId == that.mapId &&
				Objects.equals(color, that.color);
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), mapId, color);
	}
}
