package com.degoos.wetsponge.material.item.type;

import com.degoos.wetsponge.bridge.material.item.BridgeItemTypeSkull;
import com.degoos.wetsponge.enums.block.EnumBlockTypeSkullType;
import com.degoos.wetsponge.material.item.SpigotItemType;
import com.degoos.wetsponge.resource.spigot.SpigotSkullBuilder;
import com.degoos.wetsponge.user.SpigotGameProfile;
import com.degoos.wetsponge.user.WSGameProfile;
import com.degoos.wetsponge.util.Validate;
import com.mojang.authlib.GameProfile;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.material.MaterialData;

import java.net.URL;
import java.util.Objects;
import java.util.Optional;

public class SpigotItemTypeSkull extends SpigotItemType implements WSItemTypeSkull {


	private WSGameProfile profile;
	private EnumBlockTypeSkullType skullType;

	public SpigotItemTypeSkull(WSGameProfile profile, EnumBlockTypeSkullType skullType) {
		super(397, "minecraft:skull", "minecraft:skull", 64);
		Validate.notNull(skullType, "Skull type cannot be null!");
		this.profile = profile;
		this.skullType = skullType;
	}

	@Override
	public String getNewStringId() {
		return skullType.getMinecraftName() + (skullType == EnumBlockTypeSkullType.SKELETON || skullType == EnumBlockTypeSkullType.WITHER_SKELETON ? "_skull" : "_head");
	}

	public Optional<WSGameProfile> getProfile() {
		return Optional.ofNullable(profile);
	}

	public void setProfile(WSGameProfile profile) {
		this.profile = profile;
	}

	public void setTexture(String texture) {
		BridgeItemTypeSkull.setTexture(texture, profile);
	}

	public void setTexture(URL texture) {
		BridgeItemTypeSkull.setTexture(texture, profile);
	}

	public void setTextureByPlayerName(String name) {
		profile = BridgeItemTypeSkull.setTextureByPlayerName(name);
	}

	public void findFormatAndSetTexture(String texture) {
		if (texture.toLowerCase().startsWith("http:") || texture.toLowerCase().startsWith("https:")) setTexture(getTexture(texture));
		else if (texture.length() <= 16) setTextureByPlayerName(texture);
		else setTexture(texture);
	}

	public EnumBlockTypeSkullType getSkullType() {
		return skullType;
	}

	public void setSkullType(EnumBlockTypeSkullType skullType) {
		Validate.notNull(skullType, "Skull type cannot be null!");
		this.skullType = skullType;
	}

	private String getTexture(String url) {
		if (url == null) return null;
		return String.format("{textures:{SKIN:{url:\"%s\"}}}", url);
	}

	@Override
	public SpigotItemTypeSkull clone() {
		return new SpigotItemTypeSkull(profile, skullType);
	}

	@Override
	public MaterialData toMaterialData() {
		MaterialData data = super.toMaterialData();
		data.setData((byte) skullType.getValue());
		return data;
	}

	@Override
	public SpigotItemTypeSkull readMaterialData(MaterialData materialData) {
		super.readMaterialData(materialData);
		skullType = EnumBlockTypeSkullType.getByValue(materialData.getData()).orElse(EnumBlockTypeSkullType.SKELETON);
		return this;
	}

	@Override
	public void writeItemMeta(ItemMeta itemMeta) {
		super.writeItemMeta(itemMeta);
		if (itemMeta instanceof SkullMeta) {
			SpigotSkullBuilder.injectGameProfile(itemMeta, profile == null ? null : ((SpigotGameProfile) profile).getHandled());
		}
	}

	@Override
	public void readItemMeta(ItemMeta itemMeta) {
		super.readItemMeta(itemMeta);
		if (itemMeta instanceof SkullMeta) {
			GameProfile profile = SpigotSkullBuilder.getGameProfile(itemMeta);
			this.profile = profile == null ? null : new SpigotGameProfile(profile);
		}
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		SpigotItemTypeSkull that = (SpigotItemTypeSkull) o;
		return Objects.equals(profile, that.profile) &&
				skullType == that.skullType;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), profile, skullType);
	}
}
