package com.degoos.wetsponge.material.item.type;

import com.degoos.wetsponge.enums.EnumEntityType;
import com.degoos.wetsponge.material.item.SpigotItemType;
import com.degoos.wetsponge.util.Validate;
import org.bukkit.entity.EntityType;
import org.bukkit.material.MaterialData;
import org.bukkit.material.SpawnEgg;

import java.util.Objects;

public class SpigotItemTypeSpawnEgg extends SpigotItemType implements WSItemTypeSpawnEgg {

	private EnumEntityType entityType;

	public SpigotItemTypeSpawnEgg(EnumEntityType entityType) {
		super(383, "minecraft:spawn_egg", "minecraft:$entity_spawn_egg", 64);
		Validate.notNull(entityType, "Entity type cannot be null!");
		this.entityType = entityType;
	}

	@Override
	public String getNewStringId() {
		return "minecraft:" + entityType.getName().toLowerCase() + "_spawn_egg";
	}


	@Override
	public EnumEntityType getEntityType() {
		return entityType;
	}

	@Override
	public void setEntityType(EnumEntityType entityType) {
		Validate.notNull(entityType, "Entity type cannot be null!");
		this.entityType = entityType;
	}


	@Override
	public SpigotItemTypeSpawnEgg clone() {
		return new SpigotItemTypeSpawnEgg(entityType);
	}

	@Override
	public MaterialData toMaterialData() {
		MaterialData data = super.toMaterialData();
		if (data instanceof SpawnEgg) {
			((SpawnEgg) data).setSpawnedType(EntityType.fromId(entityType.getTypeId()));
		}
		return data;
	}

	@Override
	public SpigotItemTypeSpawnEgg readMaterialData(MaterialData materialData) {
		super.readMaterialData(materialData);
		entityType = EnumEntityType.getByTypeId(materialData.getData()).orElse(EnumEntityType.PIG);
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		SpigotItemTypeSpawnEgg that = (SpigotItemTypeSpawnEgg) o;
		return entityType == that.entityType;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), entityType);
	}
}
