package com.degoos.wetsponge.mixin.spigot;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.enums.EnumServerType;
import org.bukkit.plugin.java.JavaPlugin;

public class SpigotInjector {

	public static void inject(JavaPlugin javaPlugin) {
		if (WetSponge.getServerType() == EnumServerType.SPIGOT || WetSponge.getServerType() == EnumServerType.PAPER_SPIGOT) {
			SpigotAsyncCatcherRemover.load(javaPlugin);
			SpigotWSChunkProviderAssist.load();
			SpigotWSEntityHumanAssist.load();
			SpigotTimingsAssist.load();
		}
	}

}
