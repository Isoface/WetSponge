package com.degoos.wetsponge.nbt;


import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.MathHelper;
import com.degoos.wetsponge.util.reflection.NMSUtils;
import com.degoos.wetsponge.util.reflection.ReflectionUtils;
import java.lang.reflect.InvocationTargetException;

public class SpigotNBTTagDouble extends SpigotNBTBase implements WSNBTTagDouble {

	private static final Class<?> clazz = NMSUtils.getNMSClass("NBTTagDouble");

	public SpigotNBTTagDouble(double d) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
		this(clazz.getConstructor(double.class).newInstance(d));
	}

	public SpigotNBTTagDouble(Object object) {
		super(object);
	}

	private double getValue() {
		try {
			return ReflectionUtils.getFirstObject(clazz, double.class, getHandled());
		} catch (Exception e) {
			InternalLogger.printException(e, "An error has occurred while WetSponge was getting the value of a NBTTag!");
			return 0;
		}
	}

	@Override
	public long getLong() {
		return (long) getValue();
	}

	@Override
	public int getInt() {
		return MathHelper.floor(getValue());
	}

	@Override
	public short getShort() {
		return (short) MathHelper.floor(getValue());
	}

	@Override
	public byte getByte() {
		return (byte) (MathHelper.floor(getValue()) & 255);
	}

	@Override
	public double getDouble() {
		return getValue();
	}

	@Override
	public float getFloat() {
		return (float) getValue();
	}

	@Override
	public WSNBTTagDouble copy() {
		try {
			return new SpigotNBTTagDouble(getValue());
		} catch (Exception e) {
			InternalLogger.printException(e, "An error has occurred while WetSponge was copying a NBTTag!");
			return null;
		}
	}

	@Override
	public Object getHandled() {
		return super.getHandled();
	}
}
