package com.degoos.wetsponge.nbt;

public class SpigotNBTTagParser {

	public static WSNBTBase parse(Object nbtBase) {
		switch (new SpigotNBTBase(nbtBase) {
			@Override
			public WSNBTBase copy() {
				return null;
			}
		}.getId()) {
			case 0:
				return new SpigotNBTTagEnd(nbtBase);
			case 1:
				return new SpigotNBTTagByte(nbtBase);
			case 2:
				return new SpigotNBTTagShort(nbtBase);
			case 3:
				return new SpigotNBTTagInt(nbtBase);
			case 4:
				return new SpigotNBTTagLong(nbtBase);
			case 5:
				return new SpigotNBTTagFloat(nbtBase);
			case 6:
				return new SpigotNBTTagDouble(nbtBase);
			case 7:
				return new SpigotNBTTagByteArray(nbtBase);
			case 8:
				return new SpigotNBTTagString(nbtBase);
			case 9:
				return new SpigotNBTTagList(nbtBase);
			case 10:
				return new SpigotNBTTagCompound(nbtBase);
			case 11:
				return new SpigotNBTTagIntArray(nbtBase);
			case 12:
				return new SpigotNBTTagLongArray(nbtBase);
			default:
				return null;
		}
	}

}
