package com.degoos.wetsponge.packet;

public abstract class SpigotPacket implements WSPacket {

    private Object packet;

    public SpigotPacket(Object packet) {
        this.packet = packet;
    }

    @Override
    public Object getHandler() {
        return packet;
    }
}
