package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.material.WSBlockTypes;
import com.degoos.wetsponge.material.block.WSBlockType;
import com.degoos.wetsponge.packet.SpigotPacket;
import com.degoos.wetsponge.util.reflection.NMSUtils;
import com.degoos.wetsponge.util.reflection.ReflectionUtils;
import com.degoos.wetsponge.util.reflection.SpigotHandledUtils;
import com.flowpowered.math.vector.Vector3i;

public class SpigotSPacketBlockChange extends SpigotPacket implements WSSPacketBlockChange {

	private WSBlockType material;
	private Vector3i position;
	private boolean changed;

	public SpigotSPacketBlockChange(WSBlockType type, Vector3i position) throws IllegalAccessException, InstantiationException {
		super(NMSUtils.getNMSClass("PacketPlayOutBlockChange").newInstance());
		this.position = position;
		this.material = type.clone();
		update();
	}

	public SpigotSPacketBlockChange(Object packet) {
		super(packet);
		refresh();
	}

	public void update() {
		try {
			Object state = SpigotHandledUtils.getBlockState(material);
			ReflectionUtils.setFirstObject(getHandler().getClass(), NMSUtils.getNMSClass("BlockPosition"), getHandler(),
					SpigotHandledUtils.getBlockPosition(position));
			ReflectionUtils.setFirstObject(getHandler().getClass(), NMSUtils.getNMSClass("IBlockData"), getHandler(), state);
		} catch (Throwable ex) {
			ex.printStackTrace();
		}
	}

	public void refresh() {
		try {
			material = SpigotHandledUtils.getMaterial(
					ReflectionUtils.getFirstObject(getHandler().getClass(), NMSUtils.getNMSClass("IBlockData"), getHandler()));
			position = SpigotHandledUtils
					.getBlockPositionVector(ReflectionUtils.getFirstObject(getHandler().getClass(), NMSUtils.getNMSClass("BlockPosition"), getHandler()));
		} catch (Throwable ex) {
			ex.printStackTrace();
			material = WSBlockTypes.AIR.getDefaultState();
			position = new Vector3i(0, 0, 0);
		}
	}

	@Override
	public Vector3i getBlockPosition() {
		return position;
	}

	@Override
	public void setBlockPosition(Vector3i position) {
		this.position = position;
		changed = true;
	}

	@Override
	public WSBlockType getMaterial() {
		changed = true;
		return material;
	}

	@Override
	public void setMaterial(WSBlockType material) {
		this.material = material;
		changed = true;
	}

	@Override
	public boolean hasChanged() {
		return changed;
	}
}
