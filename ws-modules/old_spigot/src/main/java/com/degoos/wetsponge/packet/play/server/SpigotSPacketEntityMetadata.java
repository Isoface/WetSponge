package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.entity.SpigotEntity;
import com.degoos.wetsponge.entity.WSEntity;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.packet.SpigotPacket;
import com.degoos.wetsponge.text.WSText;
import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.Validate;
import com.degoos.wetsponge.util.reflection.NMSUtils;
import com.degoos.wetsponge.util.reflection.ReflectionUtils;
import com.degoos.wetsponge.util.reflection.SpigotHandledUtils;
import org.bukkit.entity.Entity;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SpigotSPacketEntityMetadata extends SpigotPacket implements WSSPacketEntityMetadata {

	private int entityId;
	private List entries;
	private boolean changed;

	private static final Class<?> ITEM_CLASS = NMSUtils.getNMSClass("DataWatcher$" +
			(WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD) ? "Item" : "WatchableObject"));
	private static Field dataWatcherObject;

	static {
		try {
			if (WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD))
				dataWatcherObject = ReflectionUtils.setAccessible(NMSUtils.getNMSClass("Entity").getDeclaredField("aB"));
			else dataWatcherObject = null;
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was initializing a packet class!");
			dataWatcherObject = null;
		}
	}

	public SpigotSPacketEntityMetadata(WSEntity entity) throws IllegalAccessException, InstantiationException {
		super(NMSUtils.getNMSClass("PacketPlayOutEntityMetadata").newInstance());
		Validate.notNull(entity, "Entity cannot be null!");
		this.entityId = entity.getEntityId();
		this.entries = getEntityMetadata(((SpigotEntity) entity).getHandled());
		if (entries == null) entries = createListOfType(ITEM_CLASS);
		this.changed = false;
		update();
	}

	public SpigotSPacketEntityMetadata(Object packet) {
		super(packet);
		this.changed = false;
		refresh();
		if (entries == null) entries = createListOfType(ITEM_CLASS);
	}

	@Override
	public int getEntityId() {
		return entityId;
	}

	@Override
	public void setEntityId(int entityId) {
		if (entityId != this.entityId) {
			this.entityId = entityId;
			changed = true;
		}
	}

	@Override
	public void setMetadataOf(WSEntity entity) {
		Validate.notNull(entity, "Entity cannot be null!");
		this.entries = getEntityMetadata(((SpigotEntity) entity).getHandled());
		changed = true;
	}

	@Override
	public void setCustomName(WSText text) {
		try {
			if (WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD)) {
				for (Object o : entries) {
					Class<?> clazz = o.getClass();
					Object key = clazz.getMethod("a").invoke(o);
					int i = (int) key.getClass().getMethod("a").invoke(key);
					if (i == 2) {
						ReflectionUtils.setAccessible(clazz.getDeclaredField("b")).set(o, text == null ? null : text.toFormattingText());
						return;
					}
				}
				Object watcherObject = dataWatcherObject.get(null);
				entries.add(ITEM_CLASS.getConstructors()[0].newInstance(watcherObject, text == null ? null : text.toFormattingText()));
			} else {
				for (Object o : entries) {
					Class<?> clazz = o.getClass();
					int i = (int) clazz.getMethod("a").invoke(o);
					if (i == 2) {
						ReflectionUtils.setAccessible(clazz.getDeclaredField("c")).set(o, text == null ? null : text.toFormattingText());
						return;
					}
				}
				entries.add(ITEM_CLASS.getConstructors()[0].newInstance(4, 2, text == null ? null : text.toFormattingText()));
			}
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was setting a custom name to a metadata packet!");
		}
	}


	@Override
	public void update() {
		try {
			Field[] fields = getHandler().getClass().getDeclaredFields();
			Arrays.stream(fields).forEach(field -> field.setAccessible(true));
			fields[0].setInt(getHandler(), entityId);
			fields[1].set(getHandler(), entries);
		} catch (Throwable ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public void refresh() {
		try {
			Field[] fields = getHandler().getClass().getDeclaredFields();
			Arrays.stream(fields).forEach(field -> field.setAccessible(true));
			this.entityId = fields[0].getInt(getHandler());
			this.entries = (List) fields[1].get(getHandler());
		} catch (Throwable ex) {
			ex.printStackTrace();
		}

	}

	@Override
	public boolean hasChanged() {
		return changed;
	}


	private List<?> getEntityMetadata(Entity entity) {
		try {
			Object handle = SpigotHandledUtils.getHandle(entity);
			Object watcher = NMSUtils.getNMSClass("Entity").getMethod("getDataWatcher").invoke(handle);
			return (List) NMSUtils.getNMSClass("DataWatcher").getMethod("b").invoke(watcher);
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}

	private <T> List<T> createListOfType(Class<T> type) {
		return new ArrayList<T>();
	}
}
