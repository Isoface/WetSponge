package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.entity.SpigotEntity;
import com.degoos.wetsponge.entity.living.WSLivingEntity;
import com.degoos.wetsponge.packet.SpigotPacket;
import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.Validate;
import com.degoos.wetsponge.util.reflection.SpigotHandledUtils;
import com.degoos.wetsponge.util.reflection.NMSUtils;
import com.degoos.wetsponge.util.reflection.ReflectionUtils;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class SpigotSPacketEntityProperties extends SpigotPacket implements WSSPacketEntityProperties {

	private int entityId;
	private List<Object> entries = new ArrayList<>();
	private boolean changed;

	public SpigotSPacketEntityProperties(WSLivingEntity entity) throws IllegalAccessException, InstantiationException {
		super(NMSUtils.getNMSClass("PacketPlayOutUpdateAttributes").newInstance());
		setPropertiesOf(entity);
		refresh();
	}

	public SpigotSPacketEntityProperties(Object packet) {
		super(packet);
		refresh();
	}

	@Override
	public int getEntityId() {
		return entityId;
	}

	@Override
	public void setEntityId(int entityId) {
		if (entityId != this.entityId) {
			this.entityId = entityId;
			changed = true;
		}
	}

	@Override
	public void setPropertiesOf(WSLivingEntity entity) {
		Validate.notNull(entity, "Entity cannot be null!");
		this.entries.clear();

		try {
			Object entityHandle = SpigotHandledUtils.getEntityHandle(((SpigotEntity) entity).getHandled());
			Object attributeMap = ReflectionUtils.invokeMethod(entityHandle, "getAttributeMap");
			Collection<?> attributes = (Collection<?>) ReflectionUtils.invokeMethod(attributeMap, "a");
			attributes.forEach(attribute -> {
				try {
					Object iAttribute = ReflectionUtils.invokeMethod(attribute, "getAttribute");
					entries.add(getHandler().getClass().getDeclaredClasses()[0].getConstructors()[0]
						.newInstance(getHandler(), ReflectionUtils.invokeMethod(iAttribute, "getName"), ReflectionUtils.invokeMethod(attribute, "b"), ReflectionUtils
							.invokeMethod(attribute, "c")));
				} catch (Exception ex) {
					InternalLogger.printException(ex, "An error has occurred while WetSponge was setting the properties of a packet!");
				}
			});
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was setting the properties of a packet!");
		}
		changed = true;
	}


	@Override
	public void update() {
		try {
			Field[] fields = getHandler().getClass().getDeclaredFields();
			Arrays.stream(fields).forEach(field -> field.setAccessible(true));
			fields[0].setInt(getHandler(), entityId);
			Collection<?> snapshots = (Collection<?>) fields[1].get(getHandler());
			snapshots.clear();
			Method addMethod = ReflectionUtils.getMethodByName(List.class, "add");
			for (Object o : entries) addMethod.invoke(snapshots, o);
		} catch (Throwable ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public void refresh() {
		try {
			Field[] fields = getHandler().getClass().getDeclaredFields();
			Arrays.stream(fields).forEach(field -> field.setAccessible(true));
			this.entityId = fields[0].getInt(getHandler());
			List<?> snapshots = (List<?>) fields[1].get(getHandler());
			entries.clear();
			entries.addAll(snapshots);
		} catch (Throwable ex) {
			ex.printStackTrace();
		}

	}

	@Override
	public boolean hasChanged() {
		return changed;
	}
}
