package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.packet.SpigotPacket;
import com.degoos.wetsponge.text.SpigotText;
import com.degoos.wetsponge.text.WSText;
import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.reflection.NMSUtils;
import com.degoos.wetsponge.util.reflection.SpigotTextUtils;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.chat.ComponentSerializer;

import java.lang.reflect.Field;
import java.util.Arrays;

public class SpigotSPacketOpenWindow extends SpigotPacket implements WSSPacketOpenWindow {

	private boolean changed;
	private int windowId;
	private String inventoryType;
	private WSText windowTitle;
	private int slotCount;
	private int entityId;

	public SpigotSPacketOpenWindow(int windowId, String inventoryType, WSText windowTitle, int slotCount, int entityId) throws IllegalAccessException, InstantiationException {
		super(NMSUtils.getNMSClass("PacketPlayOutOpenWindow").newInstance());
		this.windowId = windowId;
		this.inventoryType = inventoryType;
		this.windowTitle = windowTitle;
		this.slotCount = slotCount;
		this.entityId = entityId;
		this.changed = false;
		update();
	}

	public SpigotSPacketOpenWindow(Object packet) {
		super(packet);
		refresh();
		changed = false;
	}

	@Override
	public int getWindowId() {
		return windowId;
	}

	@Override
	public void setWindowId(int windowId) {
		this.windowId = windowId;
		changed = true;
	}

	@Override
	public String getInventoryType() {
		return inventoryType;
	}

	@Override
	public void setInventoryType(String inventoryType) {
		this.inventoryType = inventoryType;
		changed = true;
	}

	@Override
	public WSText getWindowTitle() {
		return windowTitle;
	}

	@Override
	public void setWindowTitle(WSText windowTitle) {
		this.windowTitle = windowTitle;
		changed = true;
	}

	@Override
	public int getSlotCount() {
		return slotCount;
	}

	@Override
	public void setSlotCount(int slotCount) {
		this.slotCount = slotCount;
		changed = true;
	}

	@Override
	public int getEntityId() {
		return entityId;
	}

	@Override
	public void setEntityId(int entityId) {
		this.entityId = entityId;
		changed = true;
	}

	@Override
	public void update() {
		Field[] fields = getHandler().getClass().getDeclaredFields();
		Arrays.stream(fields).forEach(field -> field.setAccessible(true));

		try {
			fields[0].setInt(getHandler(), windowId);
			fields[1].set(getHandler(), inventoryType);
			fields[2].set(getHandler(), SpigotTextUtils.toIChatBaseComponentFromFormattedText(windowTitle.toFormattingText()));
			fields[3].setInt(getHandler(), slotCount);
			fields[4].setInt(getHandler(), entityId);
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was updating a packet!");
		}
	}

	@Override
	public void refresh() {
		Field[] fields = getHandler().getClass().getDeclaredFields();
		Arrays.stream(fields).forEach(field -> field.setAccessible(true));

		try {
			windowId = fields[0].getInt(getHandler());
			inventoryType = (String) fields[1].get(getHandler());
			windowTitle = SpigotText.of(new TextComponent(ComponentSerializer.parse(SpigotTextUtils.toJSON(fields[2].get(getHandler())))));
			slotCount = fields[3].getInt(getHandler());
			entityId = fields[4].getInt(getHandler());
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was refreshing a packet!");
		}
	}

	@Override
	public boolean hasChanged() {
		return changed;
	}
}
