package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.item.SpigotItemStack;
import com.degoos.wetsponge.item.WSItemStack;
import com.degoos.wetsponge.packet.SpigotPacket;
import com.degoos.wetsponge.resource.spigot.SpigotMerchantUtils;
import com.degoos.wetsponge.util.reflection.NMSUtils;
import java.lang.reflect.Field;
import java.util.Arrays;
import org.bukkit.inventory.ItemStack;

public class SpigotSPacketSetSlot extends SpigotPacket implements WSSPacketSetSlot {

	private int windowId, slot;
	private WSItemStack itemStack;
	private boolean changed;

	public SpigotSPacketSetSlot(int windowsId, int slot, WSItemStack itemStack) {
		super(NMSUtils.getNMSClass("PacketPlayOutSetSlot"));
		this.windowId = windowsId;
		this.slot = slot;
		this.itemStack = itemStack;
		update();
	}

	public SpigotSPacketSetSlot(Object packet) {
		super(packet);
		refresh();
	}

	@Override
	public void update() {
		try {
			Field[] fields = getHandler().getClass().getDeclaredFields();
			Arrays.stream(fields).forEach(field -> field.setAccessible(true));
			fields[0].setInt(getHandler(), windowId);
			fields[1].setInt(getHandler(), slot);
			fields[2].set(getHandler(), SpigotMerchantUtils.asNMSCopy((ItemStack) itemStack.getHandled()));
		} catch (Throwable ex) {
			ex.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void refresh() {
		try {
			Field[] fields = getHandler().getClass().getDeclaredFields();
			Arrays.stream(fields).forEach(field -> field.setAccessible(true));
			windowId = fields[0].getInt(getHandler());
			slot = fields[1].getInt(getHandler());
			itemStack = new SpigotItemStack(SpigotMerchantUtils.asBukkitCopy(fields[2].get(getHandler())).clone());
		} catch (Throwable ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public boolean hasChanged() {
		return changed;
	}

	@Override
	public int getWindowId() {
		return windowId;
	}

	@Override
	public void setWindowId(int windowsId) {
		this.windowId = windowsId;
		changed = true;
	}

	@Override
	public int getSlot() {
		return slot;
	}

	@Override
	public void setSlot(int slot) {
		this.slot = slot;
		changed = true;
	}

	@Override
	public WSItemStack getItemStack() {
		changed = true;
		return itemStack;
	}

	@Override
	public void setItemStack(WSItemStack itemStack) {
		this.itemStack = itemStack;
		changed = true;
	}
}
