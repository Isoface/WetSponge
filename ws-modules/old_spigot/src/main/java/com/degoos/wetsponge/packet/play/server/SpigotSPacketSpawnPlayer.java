package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.entity.living.SpigotLivingEntity;
import com.degoos.wetsponge.entity.living.WSLivingEntity;
import com.degoos.wetsponge.entity.living.player.SpigotHuman;
import com.degoos.wetsponge.entity.living.player.WSHuman;
import com.degoos.wetsponge.enums.EnumEquipType;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.packet.SpigotPacket;
import com.degoos.wetsponge.util.Validate;
import com.degoos.wetsponge.util.reflection.NMSUtils;
import com.degoos.wetsponge.util.reflection.ReflectionUtils;
import com.degoos.wetsponge.util.reflection.SpigotHandledUtils;
import com.degoos.wetsponge.world.WSLocation;
import com.flowpowered.math.vector.Vector2d;
import com.flowpowered.math.vector.Vector3d;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.LivingEntity;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Optional;
import java.util.UUID;

public class SpigotSPacketSpawnPlayer extends SpigotPacket implements WSSPacketSpawnPlayer {

	private Optional<WSLivingEntity> entity;
	private int entityId;
	private UUID uniqueId;
	private Vector3d position;
	private Vector2d rotation;
	private int itemId;
	private boolean changed;

	public SpigotSPacketSpawnPlayer(WSHuman entity) throws IllegalAccessException, InstantiationException {
		super(NMSUtils.getNMSClass("PacketPlayOutNamedEntitySpawn").newInstance());
		updateEntity(entity);
		WSLocation location = entity.getLocation();
		this.position = location.toVector3d();
		this.rotation = location.getRotation().toDouble();
		update();
	}

	public SpigotSPacketSpawnPlayer(WSHuman entity, Vector3d position, Vector2d rotation) throws IllegalAccessException, InstantiationException {
		super(NMSUtils.getNMSClass("PacketPlayOutNamedEntitySpawn").newInstance());
		updateEntity(entity);
		this.position = position;
		this.rotation = rotation;
		update();
	}

	public SpigotSPacketSpawnPlayer(Object packet) {
		super(packet);
		this.entity = Optional.empty();
		refresh();
	}

	public Optional<WSLivingEntity> getEntity() {
		return entity;
	}

	public void setEntity(WSHuman entity) {
		Validate.notNull(entity, "Entity cannot be null!");
		updateEntity(entity);
	}

	public int getEntityId() {
		return entityId;
	}

	public void setEntityId(int entityId) {
		changed = true;
		this.entityId = entityId;
	}

	public UUID getUniqueId() {
		return uniqueId;
	}

	public void setUniqueId(UUID uniqueId) {
		changed = true;
		this.uniqueId = uniqueId;
	}

	public Vector3d getPosition() {
		return position;
	}

	public void setPosition(Vector3d position) {
		changed = true;
		this.position = position;
	}

	public Vector2d getRotation() {
		return rotation;
	}

	public void setRotation(Vector2d rotation) {
		changed = true;
		this.rotation = rotation;
	}

	private void updateEntity(WSHuman entity) {
		HumanEntity spigotEntity = ((SpigotHuman) entity).getHandled();
		this.entity = Optional.ofNullable(entity);
		this.entityId = entity.getEntityId();
		this.uniqueId = spigotEntity.getUniqueId();
		try {
			/*Object inventory = handle.getClass().getField("inventory").get(handle);
			Object itemStack = ReflectionUtils.getMethod(inventory.getClass(), "getItemInHand").invoke(inventory);
			Object item = ReflectionUtils.getMethod(itemStack.getClass(), "getItem").invoke(itemStack);
			itemId = (int) NMSUtils.getNMSClass("Item").getMethod("getNumericalId", item.getClass()).invoke(item);*/
			itemId = entity.getEquippedItem(EnumEquipType.MAIN_HAND).map(target -> target.getMaterial().getNumericalId()).orElse(0);
		} catch (Throwable ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public void update() {
		try {
			Field[] fields = getHandler().getClass().getDeclaredFields();
			Arrays.stream(fields).forEach(field -> field.setAccessible(true));
			fields[0].setInt(getHandler(), entityId);
			fields[1].set(getHandler(), uniqueId);

			if (WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD)) {
				fields[2].setDouble(getHandler(), position.getX());
				fields[3].setDouble(getHandler(), position.getY());
				fields[4].setDouble(getHandler(), position.getZ());
			} else {
				fields[2].setInt(getHandler(), (int) position.getX() * 32);
				fields[3].setInt(getHandler(), (int) position.getY() * 32);
				fields[4].setInt(getHandler(), (int) position.getZ() * 32);

				fields[7].setInt(getHandler(), itemId);
			}

			fields[5].setByte(getHandler(), (byte) ((int) (rotation.getX() * 256.0F / 360.0F)));
			fields[6].setByte(getHandler(), (byte) ((int) (rotation.getY() * 256.0F / 360.0F)));

			if (entity.isPresent()) {
				LivingEntity spigotEntity = ((SpigotLivingEntity) entity.get()).getHandled();
				Object livingBase = SpigotHandledUtils.getEntityHandle(spigotEntity);
				fields[WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD) ? 7 : 8]
						.set(getHandler(), ReflectionUtils.getMethod(livingBase.getClass(), "getDataWatcher").invoke(livingBase));
			}

		} catch (Throwable ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public void refresh() {
		try {
			Field[] fields = getHandler().getClass().getDeclaredFields();
			Arrays.stream(fields).forEach(field -> field.setAccessible(true));
			entityId = fields[0].getInt(getHandler());
			uniqueId = (UUID) fields[1].get(getHandler());
			position = new Vector3d(fields[2].getDouble(getHandler()), fields[3].getDouble(getHandler()), fields[4].getDouble(getHandler()));
			rotation = new Vector2d(fields[5].getByte(getHandler()) * 360.0D / 256.0D, fields[6].getByte(getHandler()) * 360.0D / 256.0D);
			if (WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_9))
				itemId = fields[7].getInt(getHandler());
			entity = Optional.empty();
		} catch (Throwable ex) {
			ex.printStackTrace();
		}

	}

	@Override
	public boolean hasChanged() {
		return changed;
	}

}
