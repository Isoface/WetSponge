package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.item.SpigotItemStack;
import com.degoos.wetsponge.item.WSItemStack;
import com.degoos.wetsponge.packet.SpigotPacket;
import com.degoos.wetsponge.resource.spigot.SpigotMerchantUtils;
import com.degoos.wetsponge.util.ListUtils;
import com.degoos.wetsponge.util.reflection.NMSUtils;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class SpigotSPacketWindowItems extends SpigotPacket implements WSSPacketWindowItems {

	private int windowId;
	private List<WSItemStack> itemStacks;
	private boolean changed;

	public SpigotSPacketWindowItems(int windowsId, List<WSItemStack> itemStacks) {
		super(NMSUtils.getNMSClass("PacketPlayOutWindowItems"));
		this.windowId = windowsId;
		this.itemStacks = itemStacks;
		update();
	}

	public SpigotSPacketWindowItems(Object packet) {
		super(packet);
		refresh();
	}

	@SuppressWarnings("unchecked")
	@Override
	public void update() {
		try {
			Field[] fields = getHandler().getClass().getDeclaredFields();
			Arrays.stream(fields).forEach(field -> field.setAccessible(true));
			fields[0].setInt(getHandler(), windowId);
			if (WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD)) fields[1]
				.set(getHandler(), itemStacks.stream().map(item -> item == null ? null : SpigotMerchantUtils.asNMSCopy(((SpigotItemStack) item).getHandled()))
					.collect(Collectors.toList()));
			else fields[1].set(getHandler(), ListUtils.toArray((Class<Object>) NMSUtils.getNMSClass("ItemStack"), itemStacks.stream()
				.map(item -> item == null ? null : SpigotMerchantUtils.asNMSCopy(((SpigotItemStack) item).getHandled())).collect(Collectors.toList())));
		} catch (Throwable ex) {
			ex.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void refresh() {
		try {
			Field[] fields = getHandler().getClass().getDeclaredFields();
			Arrays.stream(fields).forEach(field -> field.setAccessible(true));
			windowId = fields[0].getInt(getHandler());

			if (WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD)) {
				itemStacks = ((List<?>) fields[1].get(getHandler())).stream()
					.map(item -> item == null ? null : new SpigotItemStack(SpigotMerchantUtils.asBukkitCopy(item).clone())).collect(Collectors.toList());
			} else {
				itemStacks = new ArrayList<>();
				Object[] array = (Object[]) fields[1].get(getHandler());
				int index = 0;
				for (Object o : array) {
					itemStacks.add(index, o == null ? null : new SpigotItemStack(SpigotMerchantUtils.asBukkitCopy(o).clone()));
					index++;
				}
			}

		} catch (Throwable ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public boolean hasChanged() {
		return changed;
	}

	@Override
	public int getWindowId() {
		return windowId;
	}

	@Override
	public void setWindowId(int windowsId) {
		this.windowId = windowsId;
		changed = true;
	}

	@Override
	public List<WSItemStack> getItemStacks() {
		changed = true;
		return itemStacks;
	}
}
