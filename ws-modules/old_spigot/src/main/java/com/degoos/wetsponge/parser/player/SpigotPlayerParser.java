package com.degoos.wetsponge.parser.player;

import com.degoos.wetsponge.entity.living.player.SpigotPlayer;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import java.util.Optional;
import java.util.UUID;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class SpigotPlayerParser {

	protected static Optional<WSPlayer> checkPlayer(String name) {
		return Optional.ofNullable(Bukkit.getServer().getPlayer(name)).map(SpigotPlayer::new);
	}

	protected static Optional<WSPlayer> checkPlayer(UUID uuid) {
		return Optional.ofNullable(Bukkit.getServer().getPlayer(uuid)).map(SpigotPlayer::new);
	}

	protected static WSPlayer newInstance(Object player) {
		return new SpigotPlayer((Player) player);
	}
}
