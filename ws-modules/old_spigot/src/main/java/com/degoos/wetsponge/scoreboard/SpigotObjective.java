package com.degoos.wetsponge.scoreboard;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.enums.EnumCriteria;
import com.degoos.wetsponge.text.WSText;
import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.Validate;
import com.degoos.wetsponge.util.reflection.NMSUtils;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import net.minecraft.server.v1_12_R1.ScoreboardScore;
import org.bukkit.scoreboard.Objective;

public class SpigotObjective implements WSObjective {

	private Objective objective;


	public SpigotObjective(Objective objective) {
		Validate.notNull(objective, "Objective cannot be null!");
		this.objective = objective;
	}

	@Override
	public String getName() {
		return objective.getName();
	}

	@Override
	public WSText getDisplayName() {
		return WSText.getByFormattingText(objective.getDisplayName());
	}

	@Override
	public void setDisplayName(WSText text) {
		objective.setDisplayName(text.toFormattingText());
	}

	@Override
	public EnumCriteria getCriteria() {
		return EnumCriteria.getBySpigotName(objective.getCriteria()).orElse(EnumCriteria.DUMMY);
	}

	@Override
	public Map<WSText, WSScore> getScores() {
		Map<WSText, WSScore> scores = new HashMap<>();
		try {
			Class<?> spigotObjectiveClass = NMSUtils.getOBCClass("scoreboard.CraftObjective");
			Class<?> spigotScoreboardClass = NMSUtils.getOBCClass("scoreboard.CraftScoreboard");
			Class<?> scoreClass = NMSUtils.getNMSClass("ScoreboardScore");

			Method getHandleObjectiveMethod = spigotObjectiveClass.getDeclaredMethod("getHandle");
			Method getHandleScoreboardMethod = spigotScoreboardClass.getDeclaredMethod("getHandle");

			getHandleObjectiveMethod.setAccessible(true);
			getHandleScoreboardMethod.setAccessible(true);

			Object objectiveHandled = getHandleObjectiveMethod.invoke(objective);
			Object scoreboardHandled = getHandleScoreboardMethod.invoke(objective.getScoreboard());

			Collection<?> netScores = (Collection<?>) scoreboardHandled.getClass().getMethod("getScoresForObjective", objectiveHandled.getClass())
				.invoke(scoreboardHandled, objectiveHandled);

			netScores.forEach(score -> {
				try {
					String name = (String) scoreClass.getMethod("getPlayerName").invoke(score);
					int value = (int) scoreClass.getDeclaredMethod("getScore").invoke(score);
					if (value < 1) return;
					scores.put(WSText.getByFormattingText(name), new SpigotScore(objective.getScore(name)));
				} catch (Throwable ex) {
					ex.printStackTrace();
				}
			});
		} catch (Throwable ex) {
			ex.printStackTrace();
		}
		return scores;
	}

	@Override
	public boolean hasScore(WSText name) {
		return getScores().containsKey(name);
	}

	@Override
	public WSScore getOrCreateScore(WSText name) {
		return new SpigotScore(objective.getScore(name.toFormattingText()));
	}

	@Override
	public boolean removeScore(WSText name) {
		try {
			Class<?> spigotObjectiveClass = NMSUtils.getOBCClass("scoreboard.CraftObjective");
			Class<?> spigotScoreboardClass = NMSUtils.getOBCClass("scoreboard.CraftScoreboard");
			Method getHandleObjectiveMethod = spigotObjectiveClass.getDeclaredMethod("getHandle");
			Method getHandleScoreboardMethod = spigotScoreboardClass.getDeclaredMethod("getHandle");
			getHandleObjectiveMethod.setAccessible(true);
			getHandleScoreboardMethod.setAccessible(true);
			Object objectiveHandled = getHandleObjectiveMethod.invoke(objective);
			Object scoreboardHandled = getHandleScoreboardMethod.invoke(objective.getScoreboard());
			NMSUtils.getNMSClass("Scoreboard").getDeclaredMethod("resetPlayerScores", String.class, objectiveHandled.getClass())
				.invoke(scoreboardHandled, name.toFormattingText(), objectiveHandled);
		} catch (Throwable ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was trying to remove a score!");
			return false;
		}
		return true;
	}

	@Override
	public boolean removeScore(WSScore score) {
		try {
			Class<?> spigotObjectiveClass = NMSUtils.getOBCClass("scoreboard.CraftObjective");
			Class<?> spigotScoreboardClass = NMSUtils.getOBCClass("scoreboard.CraftScoreboard");
			Method getHandleObjectiveMethod = spigotObjectiveClass.getDeclaredMethod("getHandle");
			Method getHandleScoreboardMethod = spigotScoreboardClass.getDeclaredMethod("getHandle");
			getHandleObjectiveMethod.setAccessible(true);
			getHandleScoreboardMethod.setAccessible(true);
			Object objectiveHandled = getHandleObjectiveMethod.invoke(objective);
			Object scoreboardHandled = getHandleScoreboardMethod.invoke(objective.getScoreboard());
			NMSUtils.getNMSClass("Scoreboard").getDeclaredMethod("resetPlayerScores", String.class, objectiveHandled.getClass())
				.invoke(scoreboardHandled, ((SpigotScore)score).getHandled().getEntry(), objectiveHandled);
		} catch (Throwable ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was trying to remove a score!");
			return false;
		}
		return true;
	}

	@Override
	public void removeAllScoresWithScore(int score) {
		getScores().values().stream().filter(target -> target.getScore() == score).forEach(this::removeScore);
	}

	@Override
	public Optional<WSScoreboard> getScoreboard() {
		try {
			return Optional.of(new SpigotScoreboard(objective.getScoreboard()));
		} catch (Throwable ex) {
			return Optional.empty();
		}
	}

	@Override
	public Objective getHandled() {
		return objective;
	}


	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		SpigotObjective objective1 = (SpigotObjective) o;

		return objective.equals(objective1.objective);
	}

	@Override
	public int hashCode() {
		return objective.hashCode();
	}
}
