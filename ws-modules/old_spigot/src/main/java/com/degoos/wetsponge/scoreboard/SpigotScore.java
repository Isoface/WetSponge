package com.degoos.wetsponge.scoreboard;

import com.degoos.wetsponge.text.WSText;
import com.degoos.wetsponge.util.Validate;
import org.bukkit.scoreboard.Score;

public class SpigotScore implements WSScore {

    private Score score;

    public SpigotScore(Score score) {
        Validate.notNull(score, "Score cannot be null!");
        this.score = score;
    }

    public WSText getName() {
        return WSText.getByFormattingText(score.getEntry());
    }

    public int getScore() {
        return score.getScore();
    }

    @Override
    public void setScore(int score) {
        this.score.setScore(score);
    }

    @Override
    public Score getHandled() {
        return score;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SpigotScore that = (SpigotScore) o;

        return score.equals(that.score);
    }

    @Override
    public int hashCode() {
        return score.hashCode();
    }
}
