package com.degoos.wetsponge.server;


import com.degoos.wetsponge.command.WSCommandSource;
import com.degoos.wetsponge.console.SpigotConsoleSource;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.enums.EnumEnvironment;
import com.degoos.wetsponge.parser.player.PlayerParser;
import com.degoos.wetsponge.parser.world.WorldParser;
import com.degoos.wetsponge.scoreboard.SpigotScoreboard;
import com.degoos.wetsponge.scoreboard.WSScoreboard;
import com.degoos.wetsponge.world.SpigotWorld;
import com.degoos.wetsponge.world.WSWorld;
import com.degoos.wetsponge.world.WSWorldProperties;
import com.degoos.wetsponge.world.generation.SpigotWorldGenerator;
import com.degoos.wetsponge.world.generation.populator.WSGenerationPopulator;
import org.apache.commons.io.FileUtils;
import org.bukkit.*;

import java.io.File;
import java.io.IOException;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

public class SpigotServer implements WSServer {

	private Server server;
	private WSServerInfo serverInfo;
	private WSServerProperties properties;


	public SpigotServer(Server server) {
		this.server = server;
		this.serverInfo = new SpigotServerInfo(server);
		this.properties = new SpigotServerProperties(server);

		//TimingsManager.FULL_SERVER_TICK.startTiming();
	}


	@Override
	public Optional<WSWorld> getWorld(String name) {
		return Optional.ofNullable(server.getWorld(name)).map(world -> WorldParser.getOrCreateWorld(world.getName(), world));
	}


	@Override
	public Optional<WSWorld> getWorld(UUID uuid) {
		return Optional.ofNullable(server.getWorld(uuid)).map(world -> WorldParser.getOrCreateWorld(world.getName(), world));
	}


	@Override
	public Optional<WSWorld> loadWorld(String name) {
		return Optional.ofNullable(server.getWorld(name)).map(world -> WorldParser.getOrCreateWorld(world.getName(), world));
	}


	@Override
	public Optional<WSWorld> loadWorld(UUID uuid) {
		return Optional.ofNullable(server.getWorld(uuid)).map(world -> WorldParser.getOrCreateWorld(world.getName(), world));
	}


	@Override
	public boolean unloadWorld(WSWorld world) {
		return server.unloadWorld(((SpigotWorld) world).getHandled(), true);
	}


	@Override
	public Set<WSWorld> getWorlds() {
		return server.getWorlds().stream().map(world -> WorldParser.getOrCreateWorld(world.getName(), world)).collect(Collectors.toSet());
	}


	@Override
	public Optional<WSWorld> createWorld(WSWorldProperties properties, EnumEnvironment environment) {
		properties.setEnabled(true);
		Optional<WSWorld> optional = getWorld(properties.getWorldName());
		if (optional.isPresent()) {
			WSWorld world = optional.get();
			world.getProperties().apply(properties);
			return optional;
		}
		WorldCreator creator = new WorldCreator(properties.getWorldName());
		creator.environment(World.Environment.getEnvironment(environment.getValue()));
		creator.seed(10000);
		return Optional.ofNullable(creator.createWorld()).map(world -> WorldParser.getOrCreateWorld(world.getName(), world));
	}


	@Override
	public Optional<WSWorld> createWorld(WSWorldProperties properties, EnumEnvironment environment, WSGenerationPopulator populator) {
		properties.setEnabled(true);
		Optional<WSWorld> optional = getWorld(properties.getWorldName());
		if (optional.isPresent()) {
			WSWorld world = optional.get();
			world.getProperties().apply(properties);
			return optional;
		}
		WorldCreator creator = new WorldCreator(properties.getWorldName());
		creator.generator(new SpigotWorldGenerator(populator));
		creator.environment(World.Environment.getEnvironment(environment.getValue()));
		creator.seed(10000);
		return Optional.ofNullable(creator.createWorld()).map(world -> WorldParser.getOrCreateWorld(world.getName(), world));
	}


	@Override
	public boolean deleteWorld(WSWorld world) {
		if (this.unloadWorld(world)) {
			for (Chunk c : Bukkit.getWorld(world.getName()).getLoadedChunks()) {
				c.unload();
			}

			File folder = new File(Bukkit.getWorldContainer() + "/" + world.getName());
			try {
				FileUtils.deleteDirectory(folder);
			} catch (IOException e) {
				e.printStackTrace();
				return false;
			}

			return true;
		}
		return false;
	}


	@Override
	public Optional<WSPlayer> getPlayer(String name) {
		return PlayerParser.getPlayer(name);
	}


	@Override
	public Optional<WSPlayer> getPlayer(UUID uuid) {
		return PlayerParser.getPlayer(uuid);
	}


	@Override
	public Set<WSPlayer> getOnlinePlayers() {
		return server.getOnlinePlayers().stream().map(player -> PlayerParser.getOrCreatePlayer(player, player.getUniqueId())).collect(Collectors.toSet());
	}


	@Override
	public WSScoreboard getMainScoreboard() {
		return new SpigotScoreboard(server.getScoreboardManager().getMainScoreboard());
	}


	@Override
	public WSScoreboard createScoreboard() {
		return new SpigotScoreboard(server.getScoreboardManager().getNewScoreboard());
	}


	@Override
	public WSCommandSource getConsole() {
		return new SpigotConsoleSource(Bukkit.getServer().getConsoleSender());
	}


	@Override
	public WSServerInfo getServerInfo() {
		return serverInfo;
	}

	@Override
	public WSServerProperties getProperties() {
		return properties;
	}


	@Override
	public void shutdown() {
		//TimingsManager.FULL_SERVER_TICK.stopTiming();
		server.shutdown();
	}
}
