package com.degoos.wetsponge.text.action.click;

import com.degoos.wetsponge.text.action.SpigotTextAction;
import net.md_5.bungee.api.chat.ClickEvent;

public class SpigotClickAction extends SpigotTextAction implements WSClickAction {

    public static SpigotClickAction of(ClickEvent event) {
        switch (event.getAction()) {
            case CHANGE_PAGE:
                return new SpigotChangePageAction(event);
            case OPEN_URL:
            case OPEN_FILE:
                return new SpigotOpenURLAction(event);
            case RUN_COMMAND:
                return new SpigotRunCommandAction(event);
            case SUGGEST_COMMAND:
                return new SpigotSuggestCommandAction(event);
            default:
                return new SpigotClickAction(event);
        }
    }


    private ClickEvent clickEvent;


    public SpigotClickAction(ClickEvent clickEvent) {
        this.clickEvent = clickEvent;
    }

    @Override
    public ClickEvent getHandled() {
        return clickEvent;
    }
}
