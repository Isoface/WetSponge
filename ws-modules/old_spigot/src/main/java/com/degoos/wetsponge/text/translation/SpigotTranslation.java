package com.degoos.wetsponge.text.translation;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.util.reflection.NMSUtils;
import com.degoos.wetsponge.util.reflection.ReflectionUtils;

import java.util.Locale;

public class SpigotTranslation implements WSTranslation {

	private static Class<?> localeInstance = WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13) ? NMSUtils.getNMSClass("LocaleI18n") : null;

	private String id;

	public SpigotTranslation(String id) {
		this.id = id;
	}

	@Override
	public String get() {
		if (localeInstance == null) return "";
		try {
			return (String) ReflectionUtils.invokeMethod(null, localeInstance, "get", id);
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

	@Override
	public String get(Locale locale) {
		if (localeInstance == null) return "";
		try {
			return (String) ReflectionUtils.invokeMethod(null, localeInstance, "get", id);
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

	@Override
	public String get(Locale locale, Object... args) {
		if (localeInstance == null) return "";
		try {
			return (String) ReflectionUtils.invokeMethod(null, localeInstance, "a", id, args);
		} catch (Exception ex) {
			ex.printStackTrace();
			return "";
		}
	}

	@Override
	public String get(Object... args) {
		if (localeInstance == null) return "";
		try {
			return (String) ReflectionUtils.invokeMethod(null, localeInstance, "a", id, args);
		} catch (Exception ex) {
			ex.printStackTrace();
			return "";
		}
	}

	@Override
	public String getId() {
		return id;
	}
}
