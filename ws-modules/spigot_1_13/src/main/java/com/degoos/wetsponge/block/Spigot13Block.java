package com.degoos.wetsponge.block;


import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.block.tileentity.*;
import com.degoos.wetsponge.enums.EnumMapBaseColor;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.material.WSBlockTypes;
import com.degoos.wetsponge.material.block.Spigot13BlockType;
import com.degoos.wetsponge.material.block.WSBlockType;
import com.degoos.wetsponge.parser.world.WorldParser;
import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.reflection.ReflectionUtils;
import com.degoos.wetsponge.util.reflection.Spigot13HandledUtils;
import com.degoos.wetsponge.util.reflection.Spigot13MapUtils;
import com.degoos.wetsponge.world.Spigot13Location;
import com.degoos.wetsponge.world.WSLocation;
import com.degoos.wetsponge.world.WSWorld;
import org.bukkit.block.Block;

import java.util.Optional;

public class Spigot13Block implements WSBlock {

	private Block block;


	public Spigot13Block(Block block) {
		this.block = block;
	}


	@Override
	public WSLocation getLocation() {
		return new Spigot13Location(block.getLocation());
	}


	@Override
	public WSWorld getWorld() {
		return WorldParser.getOrCreateWorld(block.getWorld().getName(), block.getWorld());
	}


	@Override
	public int getNumericalId() {
		Optional<WSBlockType> optional = WSBlockTypes.getById(block.getType().getKey().toString());
		return optional.map(WSBlockType::getNumericalId).orElse(0);
	}


	@Override
	public String getStringId() {
		return WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13) ? getOldStringId() : getNewStringId();
	}

	@Override
	public String getNewStringId() {
		return block.getType().getKey().toString();
	}

	@Override
	public String getOldStringId() {
		Optional<WSBlockType> optional = WSBlockTypes.getById(block.getType().getKey().toString());
		return optional.map(WSBlockType::getNewStringId).orElse("minecraft:air");
	}


	@Override
	public WSBlockState createState() {
		return new Spigot13BlockState(this);
	}


	@Override
	public Spigot13TileEntity getTileEntity() {
		switch (getNumericalId()) {
			case 23:
				return new Spigot13TileEntityDispenser(this);
			case 25:
				return new Spigot13TileEntityNoteblock(this);
			case 52:
				return new Spigot13TileEntityMonsterSpawner(this);
			case 54:
			case 146:
				return new Spigot13TileEntityChest(this);
			case 61:
			case 62:
				return new Spigot13TileEntityFurnace(this);
			case 63:
			case 68:
				return new Spigot13TileEntitySign(this);
			case 84:
				return new Spigot13TileEntityJukebox(this);
			case 116:
				return new Spigot13TileEntityNameable(this);
			case 117:
				return new Spigot13TileEntityBrewingStand(this);
			case 137:
				return new Spigot13TileEntityCommandBlock(this);
			case 144:
				return new Spigot13TileEntitySkull(this);
			case 154:
				return new Spigot13TileEntityNameableInventory(this);
			default:
				return new Spigot13TileEntity(this);
		}
	}

	@Override
	public WSBlockType getBlockType() {
		String id = block.getType().getKey().toString();
		Optional<WSBlockType> optional = WSBlockTypes.getById(id);
		if (optional.isPresent()) return ((Spigot13BlockType) optional.get()).readBlockData(block.getBlockData());
		else return new Spigot13BlockType(-1, id, id, block.getType().getMaxStackSize());
	}

	@Override
	public WSBlock getRelative(EnumBlockFace direction) {
		return getLocation().add(direction.getRelative().toDouble()).getBlock();
	}

	@Override
	public EnumMapBaseColor getMapBaseColor() {
		Object world = Spigot13HandledUtils.getWorldHandle(block.getWorld());
		Object blockPosition = Spigot13HandledUtils.getBlockPosition(block.getLocation());
		try {
			Object iBlockData = ReflectionUtils.invokeMethod(world, "getType", blockPosition);
			return Spigot13MapUtils.getMapBaseColor(iBlockData, world, blockPosition, getStringId());
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was getting the map base color of the block " + getStringId() + "!");
			return EnumMapBaseColor.AIR;
		}
	}

	@Override
	public Block getHandled() {
		return block;
	}

}
