package com.degoos.wetsponge.block;


import com.degoos.wetsponge.material.WSBlockTypes;
import com.degoos.wetsponge.material.block.Spigot13BlockType;
import com.degoos.wetsponge.material.block.WSBlockType;
import com.degoos.wetsponge.world.WSLocation;
import com.degoos.wetsponge.world.WSWorld;
import org.bukkit.block.Block;

public class Spigot13BlockState implements WSBlockState {

	private WSLocation location;
	private WSBlockType blockType;
	private Spigot13Block block;


	public Spigot13BlockState(Spigot13Block block) {
		this.location = block.getLocation();
		this.block = block;
		refresh();
	}


	public WSLocation getLocation() {
		return location;
	}


	@Override
	public WSWorld getWorld() {
		return location.getWorld();
	}


	public WSBlock getBlock() {
		return block;
	}


	public WSBlockType getBlockType() {
		return blockType;
	}


	public Spigot13BlockState setBlockType(WSBlockType blockType) {
		this.blockType = blockType;
		return this;
	}


	public void update() {
		update(true);
	}


	public void update(boolean applyPhysics) {
		Block spigotBlock = block.getHandled();
		spigotBlock.setBlockData(((Spigot13BlockType) blockType).toBlockData());
		spigotBlock.getState().update(true, applyPhysics);
	}


	@Override
	public void refresh() {
		this.blockType = WSBlockTypes.getById(block.getNewStringId()).orElse(null);
		if (blockType == null) blockType = new Spigot13BlockType(-1, block.getNewStringId(), block.getNewStringId(), block.getHandled().getType().getMaxStackSize());
		((Spigot13BlockType) blockType).readBlockData(block.getHandled().getBlockData());
	}

}
