package com.degoos.wetsponge.block.tileentity;


import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.block.Spigot13Block;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;
import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.reflection.ReflectionUtils;
import org.bukkit.block.BlockState;

public class Spigot13TileEntity implements WSTileEntity {

	private Spigot13Block block;
	private BlockState blockState;


	public Spigot13TileEntity(Spigot13Block block) {
		this.block = block;
		this.blockState = block.getHandled().getState();
	}

	public Spigot13TileEntity(BlockState block) {
		this.block = new Spigot13Block(block.getBlock());
		this.blockState = block;
	}

	public void update() {
		blockState.update(true);
	}

	@Override
	public Spigot13Block getBlock() {
		return block;
	}

	@Override
	public WSNBTTagCompound writeToNBTTagCompound(WSNBTTagCompound nbtTagCompound) {
		try {
			Object tileEntity = ReflectionUtils.invokeMethod(blockState, "getTileEntity");
			ReflectionUtils.invokeMethod(tileEntity, WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD) ? "save" : "b", nbtTagCompound.getHandled());
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was getting the NBTTag of a tile entity!");
		}
		return nbtTagCompound;
	}

	@Override
	public WSNBTTagCompound readFromNBTTagCompound(WSNBTTagCompound nbtTagCompound) {
		try {
			Object tileEntity = ReflectionUtils.invokeMethod(blockState, "getTileEntity");
			ReflectionUtils.invokeMethod(tileEntity, WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD) ? "load" : "a", nbtTagCompound.getHandled());
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was setting the NBTTag of a tile entity!");
		}
		return nbtTagCompound;
	}

	@Override
	public Object getHandled() {
		return blockState;
	}
}
