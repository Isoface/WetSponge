package com.degoos.wetsponge.block.tileentity;

import com.degoos.wetsponge.block.Spigot13Block;
import org.bukkit.block.BrewingStand;

public class Spigot13TileEntityBrewingStand extends Spigot13TileEntityNameableInventory implements WSTileEntityBrewingStand {


    public Spigot13TileEntityBrewingStand(Spigot13Block block) {
        super(block);
    }

    @Override
    public int getFuelLevel() {
        return getHandled().getFuelLevel();
    }

    @Override
    public void setFuelLevel(int fuelLevel) {
        getHandled().setFuelLevel(fuelLevel);
        update();
    }

    @Override
    public int getBrewingTime() {
        return getHandled().getBrewingTime();
    }

    @Override
    public void setBrewingTime(int brewingTime) {
        getHandled().setBrewingTime(brewingTime);
        update();
    }


    @Override
    public BrewingStand getHandled() {
        return (BrewingStand) super.getHandled();
    }
}
