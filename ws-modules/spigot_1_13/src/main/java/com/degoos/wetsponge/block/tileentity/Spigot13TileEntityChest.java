package com.degoos.wetsponge.block.tileentity;


import com.degoos.wetsponge.block.Spigot13Block;
import com.degoos.wetsponge.inventory.Spigot13Inventory;
import com.degoos.wetsponge.inventory.WSInventory;
import org.bukkit.block.Chest;
import org.bukkit.inventory.DoubleChestInventory;
import org.bukkit.inventory.Inventory;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

public class Spigot13TileEntityChest extends Spigot13TileEntityInventory implements WSTileEntityChest {

	public Spigot13TileEntityChest(Spigot13Block block) {
		super(block);
	}

	private Spigot13TileEntityChest(Chest tileEntity) {
		super(tileEntity);
	}

	@Override
	public WSInventory getSoloInventory() {
		return new Spigot13Inventory(getHandled().getBlockInventory());
	}

	@Override
	public Optional<WSInventory> getDoubleChestInventory() {
		Inventory inventory = getHandled().getInventory();
		if (inventory instanceof DoubleChestInventory) return Optional.of(new Spigot13Inventory(inventory));
		return Optional.empty();
	}

	@Override
	public Set<WSTileEntityChest> getConnectedChests() {

		Set<WSTileEntityChest> chests = new HashSet<>();

		WSTileEntity south = getBlock().getLocation().add(-1, 0, 0).getBlock().getTileEntity();
		WSTileEntity north = getBlock().getLocation().add(1, 0, 0).getBlock().getTileEntity();
		WSTileEntity west = getBlock().getLocation().add(0, 0, -1).getBlock().getTileEntity();
		WSTileEntity east = getBlock().getLocation().add(0, 0, 1).getBlock().getTileEntity();

		if (south instanceof WSTileEntityChest && south.getBlock().getBlockType().equals(getBlock().getBlockType())) chests.add((WSTileEntityChest) south);
		if (north instanceof WSTileEntityChest && north.getBlock().getBlockType().equals(getBlock().getBlockType())) chests.add((WSTileEntityChest) north);
		if (west instanceof WSTileEntityChest && west.getBlock().getBlockType().equals(getBlock().getBlockType())) chests.add((WSTileEntityChest) west);
		if (east instanceof WSTileEntityChest && east.getBlock().getBlockType().equals(getBlock().getBlockType())) chests.add((WSTileEntityChest) east);

		return chests;
	}

	@Override
	public Chest getHandled() {
		return (Chest) super.getHandled();
	}
}
