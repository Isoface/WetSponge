package com.degoos.wetsponge.block.tileentity;

import com.degoos.wetsponge.block.Spigot13Block;
import com.degoos.wetsponge.item.WSItemStack;
import com.degoos.wetsponge.util.Spigot13MaterialUtils;
import org.bukkit.Material;
import org.bukkit.block.Jukebox;

public class Spigot13TileEntityJukebox extends Spigot13TileEntity implements WSTileEntityJukebox {


	public Spigot13TileEntityJukebox(Spigot13Block block) {
		super(block);
	}

	@Override
	public void playRecord() {
		if (getHandled().isPlaying()) return;
		getHandled().setPlaying(getHandled().getPlaying());
		update();
	}

	@Override
	public void stopRecord() {
		getHandled().setPlaying(Material.AIR);
		update();
	}

	@Override
	public void ejectRecord() {
		getHandled().eject();
		update();
	}

	@Override
	public void insertRecord(WSItemStack record) {
		getHandled().setPlaying(Spigot13MaterialUtils.getByKey(record.getMaterial().getStringId()).orElse(Material.AIR));
		update();
	}

	@Override
	public Jukebox getHandled() {
		return (Jukebox) super.getHandled();
	}
}
