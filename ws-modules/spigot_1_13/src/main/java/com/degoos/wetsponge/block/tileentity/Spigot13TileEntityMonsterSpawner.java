package com.degoos.wetsponge.block.tileentity;


import com.degoos.wetsponge.block.Spigot13Block;
import com.degoos.wetsponge.enums.EnumEntityType;
import com.degoos.wetsponge.parser.entity.Spigot13EntityParser;
import org.bukkit.block.CreatureSpawner;
import org.bukkit.entity.EntityType;

public class Spigot13TileEntityMonsterSpawner extends Spigot13TileEntity implements WSTileEntityMonsterSpawner {

    public Spigot13TileEntityMonsterSpawner(Spigot13Block block) {
        super(block);
    }

    @Override
    public EnumEntityType getEntity() {
        return Spigot13EntityParser.getEntityType(getHandled().getSpawnedType());
    }

    @Override
    public void setEntity(EnumEntityType type) {
        CreatureSpawner spawner = getHandled();
        spawner.setSpawnedType(EntityType.fromName(type.getName()));
        update();
    }

    @Override
    public CreatureSpawner getHandled() {
        return (CreatureSpawner) super.getHandled();
    }

}
