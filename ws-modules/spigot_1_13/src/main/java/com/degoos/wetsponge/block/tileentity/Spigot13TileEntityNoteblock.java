package com.degoos.wetsponge.block.tileentity;


import com.degoos.wetsponge.block.Spigot13Block;
import com.degoos.wetsponge.enums.EnumNotePitch;
import com.degoos.wetsponge.util.Validate;
import org.bukkit.Note;
import org.bukkit.block.NoteBlock;

public class Spigot13TileEntityNoteblock extends Spigot13TileEntity implements WSTileEntityNoteblock {

    public Spigot13TileEntityNoteblock(Spigot13Block block) {
        super(block);
    }


    @Override
    public void playNote() {
        getHandled().play();
    }


    @Override
    public EnumNotePitch getNote() {
        Note note = getHandled().getNote();
        return EnumNotePitch.getByArguments(note.getOctave(), note.getTone().name().charAt(0), note.isSharped()).orElse(EnumNotePitch.F_SHARP0);
    }


    @Override
    public void setNote(EnumNotePitch note) {
        Validate.notNull(note, "Note cannot be null!");
        getHandled().setNote(new Note(note.getOctave(), Note.Tone.valueOf(new String(new char[]{note.getTone()})), note.isSharped()));
        update();
    }


    @Override
    public NoteBlock getHandled() {
        return (NoteBlock) super.getHandled();
    }
}
