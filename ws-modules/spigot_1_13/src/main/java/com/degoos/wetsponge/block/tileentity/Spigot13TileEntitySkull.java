package com.degoos.wetsponge.block.tileentity;

import com.degoos.wetsponge.block.Spigot13Block;
import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.enums.block.EnumBlockTypeSkullType;
import com.degoos.wetsponge.resource.spigot.Spigot13SkullBuilder;
import com.degoos.wetsponge.user.Spigot13GameProfile;
import com.degoos.wetsponge.user.WSGameProfile;
import com.mojang.authlib.GameProfile;
import org.bukkit.Bukkit;
import org.bukkit.SkullType;
import org.bukkit.block.BlockFace;
import org.bukkit.block.Skull;

import java.net.URL;

public class Spigot13TileEntitySkull extends Spigot13TileEntity implements WSTileEntitySkull {


	public Spigot13TileEntitySkull(Spigot13Block block) {
		super(block);
	}

	@Override
	public WSGameProfile getGameProfile() {
		Skull skull = getHandled();
		return WSGameProfile.of(skull.getOwningPlayer().getUniqueId(), skull.getOwningPlayer().getName());
	}

	@Override
	public void setGameProfile(WSGameProfile gameProfile) {
		GameProfile handledProfile = ((Spigot13GameProfile) gameProfile).getHandled();
		getHandled().setOwningPlayer(Bukkit.getOfflinePlayer(handledProfile.getId()));
		Spigot13SkullBuilder.injectGameProfile(getHandled(), ((Spigot13GameProfile) gameProfile).getHandled());
		update();
	}

	@Override
	public EnumBlockFace getOrientation() {
		return EnumBlockFace.valueOf(getHandled().getRotation().name());
	}

	@Override
	public void setOrientation(EnumBlockFace orientation) {
		getHandled().setRotation(BlockFace.valueOf(orientation.name()));
		update();
	}

	@Override
	public EnumBlockTypeSkullType getSkullType() {
		return EnumBlockTypeSkullType.getBySpigotName(getHandled().getSkullType().name()).orElse(EnumBlockTypeSkullType.SKELETON);
	}

	@Override
	public void setSkullType(EnumBlockTypeSkullType skullType) {
		getHandled().setSkullType(SkullType.valueOf(skullType.getSpigotName()));
		update();
	}

	@Override
	public void setTexture(String texture) {
		Spigot13SkullBuilder.updateSkullByTexture(getHandled(), texture);
		update();
	}

	@Override
	public void setTexture(URL texture) {
		Spigot13SkullBuilder.updateSkullByURL(getHandled(), texture);
		update();
	}

	@Override
	public void setTextureByPlayerName(String name) {
		Spigot13SkullBuilder.updateSkullByPlayerName(getHandled(), name);
		update();
	}

	@Override
	public void findFormatAndSetTexture(String texture) {
		Spigot13SkullBuilder.updateSkullByUnknownFormat(getHandled(), texture);
		update();
	}

	@Override
	public Skull getHandled() {
		return (Skull) super.getHandled();
	}
}
