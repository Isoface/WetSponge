package com.degoos.wetsponge.console;


import com.degoos.wetsponge.command.Spigot13CommandSource;
import org.bukkit.command.ConsoleCommandSender;

public class Spigot13ConsoleSource extends Spigot13CommandSource implements WSConsoleSource {

	public Spigot13ConsoleSource(ConsoleCommandSender source) {
		super(source);
	}


	@Override
	public String getName () {
		return getHandled().getName();
	}


	@Override
	public ConsoleCommandSender getHandled () {
		return (ConsoleCommandSender) super.getHandled();
	}
}
