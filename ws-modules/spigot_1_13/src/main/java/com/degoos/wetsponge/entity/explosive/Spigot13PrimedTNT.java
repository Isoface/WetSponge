package com.degoos.wetsponge.entity.explosive;

import com.degoos.wetsponge.entity.Spigot13Entity;
import com.degoos.wetsponge.entity.living.WSLivingEntity;
import com.degoos.wetsponge.parser.entity.Spigot13EntityParser;
import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.reflection.NMSUtils;
import com.degoos.wetsponge.util.reflection.ReflectionUtils;
import com.degoos.wetsponge.util.reflection.Spigot13HandledUtils;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.TNTPrimed;

import java.util.Optional;

public class Spigot13PrimedTNT extends Spigot13Entity implements WSPrimedTNT {


	public Spigot13PrimedTNT(TNTPrimed entity) {
		super(entity);
	}

	@Override
	public Optional<WSLivingEntity> getDetonator() {
		Entity entity = getHandled().getSource();
		if (entity == null || !(entity instanceof LivingEntity)) return Optional.empty();
		return Optional.of((WSLivingEntity) Spigot13EntityParser.getWSEntity(entity));
	}

	@Override
	public void setDetonator(WSLivingEntity entity) {
		try {
			Object handled = Spigot13HandledUtils.getEntityHandle(getHandled());
			ReflectionUtils.setFirstObject(handled.getClass(), NMSUtils.getNMSClass("EntityLiving"), handled,
				entity == null ? null : Spigot13HandledUtils.getEntityHandle(((Spigot13Entity) entity).getHandled()));
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was setting the detonator of a primed TNT!");
		}
	}

	@Override
	public int getFuseDuration() {
		return 100;
	}

	@Override
	public void setFuseDuration(int fuseDuration) {
	}

	@Override
	public int getTicksRemaining() {
		return getHandled().getFuseTicks();
	}

	@Override
	public void setTicksRemaining(int ticksRemaining) {
		getHandled().setFuseTicks(ticksRemaining);
	}

	@Override
	public boolean isPrimed() {
		return true;
	}

	@Override
	public void prime() {
	}

	@Override
	public void defuse() {
		getHandled().remove();
	}

	@Override
	public int setExplosionRadius() {
		return (int) getHandled().getYield();
	}

	@Override
	public void setExplosionRadius(int explosionRadius) {
		getHandled().setYield(explosionRadius);
	}

	@Override
	public void detonate() {
		try {
			ReflectionUtils.invokeMethod(Spigot13HandledUtils.getEntityHandle(getHandled()), "explode");
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was exploding a primed TNT!");
		}
	}

	@Override
	public TNTPrimed getHandled() {
		return (TNTPrimed) super.getHandled();
	}
}
