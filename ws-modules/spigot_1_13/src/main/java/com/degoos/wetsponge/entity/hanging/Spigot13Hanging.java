package com.degoos.wetsponge.entity.hanging;

import com.degoos.wetsponge.entity.Spigot13Entity;
import com.degoos.wetsponge.enums.block.EnumBlockFace;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Hanging;

public class Spigot13Hanging extends Spigot13Entity implements WSHanging {


	public Spigot13Hanging(Hanging entity) {
		super(entity);
	}

	@Override
	public EnumBlockFace getDirection() {
		return EnumBlockFace.valueOf(getHandled().getFacing().name());
	}

	@Override
	public void setDirection(EnumBlockFace direction) {
		getHandled().setFacingDirection(BlockFace.valueOf(direction.name()));
	}

	@Override
	public Hanging getHandled() {
		return (Hanging) super.getHandled();
	}
}
