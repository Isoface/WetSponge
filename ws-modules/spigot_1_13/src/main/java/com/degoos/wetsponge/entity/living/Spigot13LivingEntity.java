package com.degoos.wetsponge.entity.living;


import com.degoos.wetsponge.effect.potion.Spigot13PotionEffect;
import com.degoos.wetsponge.effect.potion.WSPotionEffect;
import com.degoos.wetsponge.entity.Spigot13Entity;
import com.degoos.wetsponge.entity.WSEntity;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.entity.projectile.WSProjectile;
import com.degoos.wetsponge.enums.EnumEntityType;
import com.degoos.wetsponge.enums.EnumPotionEffectType;
import com.degoos.wetsponge.packet.WSPacket;
import com.degoos.wetsponge.packet.play.server.WSSPacketDestroyEntities;
import com.degoos.wetsponge.packet.play.server.WSSPacketSpawnMob;
import com.degoos.wetsponge.packet.play.server.WSSPacketSpawnPlayer;
import com.degoos.wetsponge.parser.entity.Spigot13EntityParser;
import com.flowpowered.math.vector.Vector2d;
import com.flowpowered.math.vector.Vector3d;
import org.bukkit.Location;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Projectile;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class Spigot13LivingEntity extends Spigot13Entity implements WSLivingEntity {

	private WSLivingEntity disguise;

	public Spigot13LivingEntity(LivingEntity entity) {
		super(entity);
		this.disguise = null;
	}


	@Override
	public boolean isAlive() {
		return !getHandled().isDead();
	}


	@Override
	public void kill() {
		setHealth(0);
	}


	@Override
	public double getHealth() {
		return getHandled().getHealth();
	}


	@Override
	public void setHealth(double health) {
		getHandled().setHealth(health);
	}


	@Override
	public double getMaxHealth() {
		return getHandled().getMaxHealth();
	}


	@Override
	public void setMaxHealth(double maxHealth) {
		getHandled().setMaxHealth(maxHealth);
	}

	@Override
	public Vector3d getRotation() {
		return new Vector3d(getHandled().getLocation().getPitch(), getHandled().getLocation().getYaw(), 0);
	}

	@Override
	public void setRotation(Vector3d rotation) {
		Location location = getHandled().getLocation();
		location.setPitch((float) rotation.getX());
		location.setYaw((float) rotation.getY());
		getHandled().teleport(location);
	}

	@Override
	public Vector3d getHeadRotation() {
		return getRotation();
	}

	@Override
	public double getEyeHeight() {
		return getHandled().getEyeHeight();
	}

	@Override
	public void setHeadRotation(Vector3d rotation) {
		setRotation(rotation);
	}

	@Override
	public void lookAt(Vector3d targetPosition) {
		Vector bukkitEyePos = getHandled().getEyeLocation().toVector();
		Vector3d eyePos = new Vector3d(bukkitEyePos.getX(), bukkitEyePos.getY(), bukkitEyePos.getZ());

		if (eyePos == null) return;

		Vector2d xz1 = eyePos.toVector2(true);
		Vector2d xz2 = targetPosition.toVector2(true);
		double distance = xz1.distance(xz2);

		// calculate pitch
		Vector2d p1 = Vector2d.UNIT_Y.mul(eyePos.getY());
		Vector2d p2 = new Vector2d(distance, targetPosition.getY());
		Vector2d v1 = p2.sub(p1);
		Vector2d v2 = Vector2d.UNIT_X.mul(distance);
		final double pitchRad = Math.acos(v1.dot(v2) / (v1.length() * v2.length()));
		final double pitchDeg = pitchRad * 180 / Math.PI * (-v1.getY() / Math.abs(v1.getY()));

		// calculate yaw
		p1 = xz1;
		p2 = xz2;
		v1 = p2.sub(p1);
		v2 = Vector2d.UNIT_Y.mul(v1.getY());
		double yawRad = Math.acos(v1.dot(v2) / (v1.length() * v2.length()));
		double yawDeg = yawRad * 180 / Math.PI;
		if (v1.getX() < 0 && v1.getY() < 0) {
			yawDeg = 180 - yawDeg;
		} else if (v1.getX() > 0 && v1.getY() < 0) {
			yawDeg = 270 - (90 - yawDeg);
		} else if (v1.getX() > 0 && v1.getY() > 0) {
			yawDeg = 270 + (90 - yawDeg);
		}

		setRotation(new Vector3d(pitchDeg, yawDeg, getRotation().getZ()));
	}

	@Override
	public Optional<WSLivingEntity> getDisguise() {
		return Optional.ofNullable(disguise);
	}


	@Override
	public void setDisguise(WSLivingEntity disguise) {
		if (this.disguise != null && disguise == null) {
			WSPacket removePacket = WSSPacketDestroyEntities.of(getEntityId());
			WSPacket spawnPacket = this instanceof WSPlayer ? WSSPacketSpawnPlayer.of((WSPlayer) this) : WSSPacketSpawnMob.of(this);
			getWorld().getPlayers().forEach(player -> {
				if (player.equals(this)) return;
				player.sendPacket(removePacket);
				player.sendPacket(spawnPacket);
			});
		}
		if (disguise != null) {
			WSPacket removePacket = WSSPacketDestroyEntities.of(getEntityId());
			WSPacket spawnPacket = WSSPacketSpawnMob.of(this);
			getWorld().getPlayers().forEach(player -> {
				if (player.equals(this)) return;
				player.sendPacket(removePacket);
				player.sendPacket(spawnPacket);
			});
		}
		this.disguise = disguise;
	}

	@Override
	public boolean hasDisguise() {
		return disguise != null;
	}

	@Override
	public void clearDisguise() {
		setDisguise(null);
	}

	@Override
	public Optional<WSEntity> getLeashHolder() {
		return Optional.ofNullable(getHandled().getLeashHolder()).map(Spigot13EntityParser::getWSEntity);
	}

	@Override
	public void setLeashHolder(WSEntity entity) {
		getHandled().setLeashHolder(entity == null ? null : ((Spigot13Entity) entity).getHandled());
	}

	@Override
	public void addPotionEffect(WSPotionEffect effect) {
		getHandled().addPotionEffect(((Spigot13PotionEffect) effect).getHandled());
	}

	@Override
	public List<WSPotionEffect> getPotionEffects() {
		return getHandled().getActivePotionEffects().stream().map(Spigot13PotionEffect::new).collect(Collectors.toList());
	}

	@Override
	public void clearAllPotionEffects() {
		getHandled().getActivePotionEffects().forEach(effect -> {
			try {
				getHandled().removePotionEffect(effect.getType());
			} catch (Throwable ex) {
				ex.printStackTrace();
			}
		});
	}

	@Override
	public void removePotionEffect(EnumPotionEffectType potionEffectType) {
		getHandled().removePotionEffect(PotionEffectType.getById(potionEffectType.getValue()));
	}

	@Override
	public <T extends WSProjectile> Optional<T> launchProjectile(Class<T> projectile) {
		return launchProjectile(projectile, new Vector3d(0, 0, 0));
	}

	@Override
	public <T extends WSProjectile> Optional<T> launchProjectile(Class<T> projectile, Vector3d velocity) {
		EnumEntityType type = EnumEntityType.getByClass(projectile).orElse(EnumEntityType.UNKNOWN);
		if (type == EnumEntityType.UNKNOWN) return Optional.empty();
		try {
			Class<? extends Projectile> spigotClass = (Class<? extends Projectile>) Spigot13EntityParser.getEntityData(type).getEntityClass();
			return Optional.ofNullable(getHandled().launchProjectile(spigotClass, new Vector(velocity.getX(), velocity.getY(), velocity.getZ())))
				.map(entity -> (T) Spigot13EntityParser.getWSEntity(entity));
		} catch (Throwable ex) {
			ex.printStackTrace();
			return Optional.empty();
		}
	}

	@Override
	public LivingEntity getHandled() {
		return (LivingEntity) super.getHandled();
	}
}
