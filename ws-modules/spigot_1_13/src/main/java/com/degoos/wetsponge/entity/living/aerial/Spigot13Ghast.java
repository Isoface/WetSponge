package com.degoos.wetsponge.entity.living.aerial;

import com.degoos.wetsponge.entity.WSEntity;
import com.degoos.wetsponge.entity.living.Spigot13LivingEntity;
import org.bukkit.entity.Ghast;

import java.util.Optional;

public class Spigot13Ghast extends Spigot13LivingEntity implements WSGhast {


	public Spigot13Ghast(Ghast entity) {
		super(entity);
	}

	@Override
	public void setAI(boolean ai) {
		getHandled().setAI(ai);
	}

	@Override
	public boolean hasAI() {
		return getHandled().hasAI();
	}

	@Override
	public Optional<WSEntity> getTarget() {
		return Optional.empty();
	}

	@Override
	public void setTarget(WSEntity entity) {

	}

	@Override
	public Ghast getHandled() {
		return (Ghast) super.getHandled();
	}
}
