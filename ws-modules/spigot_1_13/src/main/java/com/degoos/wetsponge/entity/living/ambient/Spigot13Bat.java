package com.degoos.wetsponge.entity.living.ambient;


import org.bukkit.entity.Bat;

public class Spigot13Bat extends Spigot13Ambient implements WSBat {

	public Spigot13Bat(Bat entity) {
		super(entity);
	}


	@Override
	public boolean isAwake () {
		return getHandled().isAwake();
	}


	@Override
	public void setAwake (boolean awake) {
		getHandled().setAwake(awake);
	}


	@Override
	public Bat getHandled () {
		return (Bat) super.getHandled();
	}

}
