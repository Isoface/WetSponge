package com.degoos.wetsponge.entity.living.animal;

import org.bukkit.entity.Donkey;

public class Spigot13Donkey extends Spigot13AbstractHorse implements WSDonkey {

	public Spigot13Donkey(Donkey entity) {
		super(entity);
	}

	@Override
	public boolean hasChest() {
		return getHandled().isCarryingChest();
	}

	@Override
	public void setChested(boolean chested) {
		getHandled().setCarryingChest(chested);
	}

	@Override
	public Donkey getHandled() {
		return (Donkey) super.getHandled();
	}
}
