package com.degoos.wetsponge.entity.living.animal;

import com.degoos.wetsponge.enums.EnumHorseArmorType;
import com.degoos.wetsponge.enums.EnumHorseColor;
import com.degoos.wetsponge.enums.EnumHorseStyle;
import com.degoos.wetsponge.inventory.Spigot13Inventory;
import com.degoos.wetsponge.inventory.WSInventory;
import com.degoos.wetsponge.item.Spigot13ItemStack;
import org.bukkit.entity.Horse;

public class Spigot13Horse extends Spigot13AbstractHorse implements WSHorse {

	public Spigot13Horse(Horse entity) {
		super(entity);
	}

	@Override
	public EnumHorseColor getHorseColor() {
		return EnumHorseColor.getByName(getHandled().getColor().name()).orElse(EnumHorseColor.WHITE);
	}

	@Override
	public void setHorseColor(EnumHorseColor horseColor) {
		getHandled().setColor(Horse.Color.valueOf(horseColor.name()));
	}

	@Override
	public EnumHorseStyle getHorseStyle() {
		return EnumHorseStyle.getByName(getHandled().getStyle().name()).orElse(EnumHorseStyle.NONE);
	}

	@Override
	public void setHorseStyle(EnumHorseStyle horseStyle) {
		getHandled().setStyle(Horse.Style.valueOf(horseStyle.name()));
	}

	@Override
	public EnumHorseArmorType getArmor() {
		return EnumHorseArmorType.getByMaterial(getHandled().getInventory().getArmor().getType().getId());
	}

	@Override
	public void setArmor(EnumHorseArmorType armor) {
		getHandled().getInventory().setArmor(((Spigot13ItemStack) armor.getArmorItemStack()).getHandled());
	}

	@Override
	public WSInventory getInventory() {
		return new Spigot13Inventory(getHandled().getInventory());
	}

	@Override
	public Horse getHandled() {
		return (Horse) super.getHandled();
	}
}
