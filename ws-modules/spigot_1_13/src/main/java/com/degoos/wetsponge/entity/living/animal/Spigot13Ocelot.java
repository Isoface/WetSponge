package com.degoos.wetsponge.entity.living.animal;

import com.degoos.wetsponge.enums.EnumOcelotType;
import org.bukkit.Bukkit;
import org.bukkit.entity.AnimalTamer;
import org.bukkit.entity.Ocelot;

import java.util.Optional;
import java.util.UUID;

public class Spigot13Ocelot extends Spigot13Animal implements WSOcelot {


    public Spigot13Ocelot(Ocelot entity) {
        super(entity);
    }


    @Override
    public EnumOcelotType getOcelotType() {
        return EnumOcelotType.getByName(getHandled().getCatType().name()).orElse(EnumOcelotType.WILD_OCELOT);
    }

    @Override
    public void setOcelotType(EnumOcelotType ocelotType) {
        getHandled().setCatType(Ocelot.Type.valueOf(ocelotType.name()));
    }

    @Override
    public boolean isSitting() {
        return getHandled().isSitting();
    }

    @Override
    public void setSitting(boolean sitting) {
        getHandled().setSitting(sitting);
    }

    @Override
    public boolean isTamed() {
        return getHandled().isTamed();
    }

    @Override
    public Optional<UUID> getTamer() {
        AnimalTamer tamer = getHandled().getOwner();
        if (tamer == null) return Optional.empty();
        return Optional.of(tamer.getUniqueId());
    }

    @Override
    public void setTamer(UUID tamer) {
        getHandled().setOwner(tamer == null ? null : Bukkit.getOfflinePlayer(tamer));
    }

    @Override
    public Ocelot getHandled() {
        return (Ocelot) super.getHandled();
    }
}
