package com.degoos.wetsponge.entity.living.animal;


import org.bukkit.entity.Pig;

public class Spigot13Pig extends Spigot13Animal implements WSPig {


	public Spigot13Pig(Pig entity) {
		super(entity);
	}


	@Override
	public boolean isSaddled () {
		return getHandled().hasSaddle();
	}


	@Override
	public void setSaddled (boolean saddled) {
		getHandled().setSaddle(saddled);
	}


	@Override
	public Pig getHandled () {
		return (Pig) super.getHandled();
	}
}
