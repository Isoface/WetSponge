package com.degoos.wetsponge.entity.living.animal;

import org.bukkit.entity.SkeletonHorse;

public class Spigot13SkeletonHorse extends Spigot13AbstractHorse implements WSSkeletonHorse {

	public Spigot13SkeletonHorse(SkeletonHorse entity) {
		super(entity);
	}

	@Override
	public SkeletonHorse getHandled() {
		return (SkeletonHorse) super.getHandled();
	}
}
