package com.degoos.wetsponge.entity.living.animal;

import org.bukkit.entity.ZombieHorse;

public class Spigot13ZombieHorse extends Spigot13AbstractHorse implements WSSkeletonHorse {

	public Spigot13ZombieHorse(ZombieHorse entity) {
		super(entity);
	}

	@Override
	public ZombieHorse getHandled() {
		return (ZombieHorse) super.getHandled();
	}
}
