package com.degoos.wetsponge.entity.living.aquatic;

import com.degoos.wetsponge.entity.living.Spigot13Creature;
import org.bukkit.entity.Dolphin;

public class Spigot13Dolphin extends Spigot13Creature implements WSDolphin {

	public Spigot13Dolphin(Dolphin entity) {
		super(entity);
	}

	@Override
	public Dolphin getHandled() {
		return (Dolphin) super.getHandled();
	}
}
