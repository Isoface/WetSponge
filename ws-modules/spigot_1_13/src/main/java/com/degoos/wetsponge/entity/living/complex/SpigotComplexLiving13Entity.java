package com.degoos.wetsponge.entity.living.complex;

import com.degoos.wetsponge.entity.living.Spigot13LivingEntity;
import com.degoos.wetsponge.parser.entity.Spigot13EntityParser;
import org.bukkit.entity.ComplexLivingEntity;

import java.util.Set;
import java.util.stream.Collectors;

public class SpigotComplexLiving13Entity extends Spigot13LivingEntity implements WSComplexLivingEntity {


	public SpigotComplexLiving13Entity(ComplexLivingEntity entity) {
		super(entity);
	}

	@Override
	public Set<? extends WSComplexLivingEntityPart> getParts() {
		return getHandled().getParts().stream().map(Spigot13EntityParser::getWSEntity).map(entity -> (WSComplexLivingEntityPart) entity).collect(Collectors.toSet());
	}

	@Override
	public ComplexLivingEntity getHandled() {
		return (ComplexLivingEntity) super.getHandled();
	}
}
