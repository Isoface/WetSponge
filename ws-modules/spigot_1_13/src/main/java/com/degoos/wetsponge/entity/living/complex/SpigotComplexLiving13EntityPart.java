package com.degoos.wetsponge.entity.living.complex;

import com.degoos.wetsponge.entity.Spigot13Entity;
import com.degoos.wetsponge.parser.entity.Spigot13EntityParser;
import org.bukkit.entity.ComplexEntityPart;

public class SpigotComplexLiving13EntityPart extends Spigot13Entity implements WSComplexLivingEntityPart {


	public SpigotComplexLiving13EntityPart(ComplexEntityPart entity) {
		super(entity);
	}

	@Override
	public WSComplexLivingEntity getParent() {
		return (WSComplexLivingEntity) Spigot13EntityParser.getWSEntity(getHandled().getParent());
	}

	@Override
	public ComplexEntityPart getHandled() {
		return (ComplexEntityPart) super.getHandled();
	}
}
