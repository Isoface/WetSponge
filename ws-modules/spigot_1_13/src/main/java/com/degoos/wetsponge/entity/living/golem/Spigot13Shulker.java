package com.degoos.wetsponge.entity.living.golem;

import com.degoos.wetsponge.entity.living.Spigot13Creature;
import com.degoos.wetsponge.enums.EnumDyeColor;
import org.bukkit.DyeColor;
import org.bukkit.entity.Shulker;

public class Spigot13Shulker extends Spigot13Creature implements WSShulker {

	public Spigot13Shulker(Shulker entity) {
		super(entity);
	}

	@Override
	public EnumDyeColor getDyeColor() {
		return EnumDyeColor.getByDyeData(getHandled().getColor().getDyeData()).orElse(EnumDyeColor.WHITE);
	}

	@Override
	public void setDyeColor(EnumDyeColor color) {
		getHandled().setColor(DyeColor.getByDyeData(color.getDyeData()));
	}

	@Override
	public Shulker getHandled() {
		return (Shulker) super.getHandled();
	}
}
