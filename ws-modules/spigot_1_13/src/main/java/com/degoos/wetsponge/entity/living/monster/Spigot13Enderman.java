package com.degoos.wetsponge.entity.living.monster;

import com.degoos.wetsponge.material.WSBlockTypes;
import com.degoos.wetsponge.material.block.Spigot13BlockType;
import com.degoos.wetsponge.material.block.WSBlockType;
import org.bukkit.Material;
import org.bukkit.block.data.BlockData;
import org.bukkit.entity.Enderman;

import java.util.Optional;

public class Spigot13Enderman extends Spigot13Monster implements WSEnderman {


	public Spigot13Enderman(Enderman entity) {
		super(entity);
	}

	@Override
	public Optional<WSBlockType> getCarriedBlock() {
		BlockData blockData = getHandled().getCarriedBlock();
		if (blockData == null || blockData.getMaterial().equals(Material.AIR)) return Optional.empty();
		String id = blockData.getMaterial().getKey().toString();
		Optional<WSBlockType> optional = WSBlockTypes.getById(id);

		if (optional.isPresent()) return optional.map(target -> ((Spigot13BlockType) target).readBlockData(blockData));
		return Optional.of(new Spigot13BlockType(-1, id, id, blockData.getMaterial().getMaxStackSize()));
	}

	@Override
	public void setCarriedBlock(WSBlockType carriedMaterial) {
		if (carriedMaterial == null) getHandled().setCarriedBlock(null);
		else getHandled().setCarriedBlock(((Spigot13BlockType) carriedMaterial).toBlockData());
	}

	@Override
	public Enderman getHandled() {
		return (Enderman) super.getHandled();
	}


}
