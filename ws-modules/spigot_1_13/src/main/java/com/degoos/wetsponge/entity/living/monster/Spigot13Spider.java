package com.degoos.wetsponge.entity.living.monster;


import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.reflection.ReflectionUtils;
import com.degoos.wetsponge.util.reflection.Spigot13HandledUtils;
import org.bukkit.entity.Spider;

public class Spigot13Spider extends Spigot13Monster implements WSSpider {


	public Spigot13Spider(Spider entity) {
		super(entity);
	}


	@Override
	public boolean isClimbing() {
		Object handled = Spigot13HandledUtils.getEntityHandle(getHandled());

		try {
			return (boolean) ReflectionUtils.invokeMethod(handled, "l");
		} catch (Exception e) {
			InternalLogger.printException(e, "An exception has occurred while WetSponge was checking if a spigot was climbing!");
			return false;
		}
	}

	@Override
	public void setClimbing(boolean climbing) {
		Object handled = Spigot13HandledUtils.getEntityHandle(getHandled());
		try {
			ReflectionUtils.invokeMethod(handled, "a", climbing);
		} catch (Exception e) {
			InternalLogger.printException(e, "An exception has occurred while WetSponge was setting whether a spigot was climbing!");
		}
	}

	@Override
	public Spider getHandled() {
		return (Spider) super.getHandled();
	}
}
