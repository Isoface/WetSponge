package com.degoos.wetsponge.entity.living.monster;

import com.degoos.wetsponge.enums.EnumEquipType;
import com.degoos.wetsponge.item.Spigot13ItemStack;
import com.degoos.wetsponge.item.WSItemStack;
import org.bukkit.entity.Zombie;
import org.bukkit.inventory.ItemStack;

import java.util.Optional;

public class Spigot13Zombie extends Spigot13Monster implements WSZombie {


    public Spigot13Zombie(Zombie entity) {
        super(entity);
    }

    @Override
    public int getAge() {
        return getHandled().isBaby() ? -1 : 0;
    }

    @Override
    public void setAge(int age) {
        setBaby(age < 0);
    }

    @Override
    public boolean isBaby() {
        return getHandled().isBaby();
    }

    @Override
    public void setBaby(boolean baby) {
        getHandled().setBaby(baby);
    }

    @Override
    public boolean isAdult() {
        return !getHandled().isBaby();
    }

    @Override
    public void setAdult(boolean adult) {
        getHandled().setBaby(!adult);
    }


    @Override
    public Optional<WSItemStack> getEquippedItem(EnumEquipType type) {
        ItemStack itemStack = null;
        switch (type) {
            case HELMET:
                itemStack = getHandled().getEquipment().getHelmet();
                break;
            case CHESTPLATE:
                itemStack = getHandled().getEquipment().getChestplate();
                break;
            case LEGGINGS:
                itemStack = getHandled().getEquipment().getLeggings();
                break;
            case BOOTS:
                itemStack = getHandled().getEquipment().getBoots();
                break;
            case MAIN_HAND:
                itemStack = getHandled().getEquipment().getItemInHand();
                break;
            case OFF_HAND:
                itemStack = getHandled().getEquipment().getItemInOffHand();
                break;
        }
        return Optional.ofNullable(itemStack).map(Spigot13ItemStack::new);
    }


    @Override
    public void setEquippedItem(EnumEquipType type, WSItemStack itemStack) {
        switch (type) {
            case HELMET:
                getHandled().getEquipment().setHelmet(itemStack == null ? null : ((Spigot13ItemStack) itemStack).getHandled().clone());
                break;
            case CHESTPLATE:
                getHandled().getEquipment().setChestplate(itemStack == null ? null : ((Spigot13ItemStack) itemStack).getHandled().clone());
                break;
            case LEGGINGS:
                getHandled().getEquipment().setLeggings(itemStack == null ? null : ((Spigot13ItemStack) itemStack).getHandled().clone());
                break;
            case BOOTS:
                getHandled().getEquipment().setBoots(itemStack == null ? null : ((Spigot13ItemStack) itemStack).getHandled().clone());
                break;
            case MAIN_HAND:
                getHandled().getEquipment().setItemInHand(itemStack == null ? null : ((Spigot13ItemStack) itemStack).getHandled().clone());
                break;
            case OFF_HAND:
                getHandled().getEquipment().setItemInOffHand(itemStack == null ? null : ((Spigot13ItemStack) itemStack).getHandled().clone());
                break;
        }
    }

    @Override
    public Zombie getHandled() {
        return (Zombie) super.getHandled();
    }
}
