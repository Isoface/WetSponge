package com.degoos.wetsponge.entity.living.player;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.entity.living.Spigot13LivingEntity;
import com.degoos.wetsponge.enums.EnumEquipType;
import com.degoos.wetsponge.enums.EnumGameMode;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.inventory.Spigot13Inventory;
import com.degoos.wetsponge.inventory.WSInventory;
import com.degoos.wetsponge.item.Spigot13ItemStack;
import com.degoos.wetsponge.item.WSItemStack;
import com.degoos.wetsponge.user.Spigot13GameProfile;
import com.degoos.wetsponge.user.WSGameProfile;
import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.reflection.ReflectionUtils;
import com.degoos.wetsponge.util.reflection.Spigot13HandledUtils;
import com.mojang.authlib.GameProfile;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.HumanEntity;
import org.bukkit.inventory.ItemStack;
import org.bukkit.permissions.PermissionAttachmentInfo;

import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public class Spigot13Human extends Spigot13LivingEntity implements WSHuman {

	public Spigot13Human(HumanEntity entity) {
		super(entity);
	}

	@Override
	public int getFoodLevel() {
		return 20;
	}

	@Override
	public void setFoodLevel(int foodLevel) {

	}

	@Override
	public WSInventory getInventory() {
		return new Spigot13Inventory(getHandled().getInventory());
	}

	@Override
	public WSInventory getEnderChestInventory() {
		return new Spigot13Inventory(getHandled().getEnderChest());
	}

	@Override
	public Optional<WSInventory> getOpenInventory() {
		return Optional.ofNullable(getHandled().getOpenInventory().getTopInventory()).map(Spigot13Inventory::new);
	}


	@Override
	public void openInventory(WSInventory inventory) {
		if (inventory == null) closeInventory();
		else getHandled().openInventory(((Spigot13Inventory) inventory).getHandled());
	}

	@Override
	public void closeInventory() {
		getHandled().closeInventory();
	}

	@Override
	public Optional<WSItemStack> getItemOnCursor() {
		ItemStack itemStack = getHandled().getItemOnCursor();
		if (itemStack == null || itemStack.getType() == Material.AIR) return Optional.empty();
		return Optional.of(new Spigot13ItemStack(itemStack));
	}

	@Override
	public void setItemOnCursor(WSItemStack itemOnCursor) {
		if (itemOnCursor == null) getHandled().setItemOnCursor(null);
		else getHandled().setItemOnCursor(((Spigot13ItemStack) itemOnCursor).getHandled());
	}

	@Override
	public EnumGameMode getGameMode() {
		return EnumGameMode.getByValue(getHandled().getGameMode().getValue()).orElse(EnumGameMode.SURVIVAL);
	}

	@Override
	public void setGameMode(EnumGameMode gameMode) {
		getHandled().setGameMode(GameMode.getByValue(gameMode.getValue()));
	}

	@Override
	public WSGameProfile getProfile() {
		Object handled = Spigot13HandledUtils.getHandle(getHandled());
		try {
			return new Spigot13GameProfile((GameProfile) ReflectionUtils.invokeMethod(handled, "getProfile"));
		} catch (Exception e) {
			InternalLogger.printException(e, "An exception has occurred while WetSponge was getting a human's profile!");
			return null;
		}
	}

	@Override
	public Optional<WSItemStack> getEquippedItem(EnumEquipType type) {
		ItemStack itemStack = null;
		switch (type) {
			case HELMET:
				itemStack = getHandled().getEquipment().getHelmet();
				break;
			case CHESTPLATE:
				itemStack = getHandled().getEquipment().getChestplate();
				break;
			case LEGGINGS:
				itemStack = getHandled().getEquipment().getLeggings();
				break;
			case BOOTS:
				itemStack = getHandled().getEquipment().getBoots();
				break;
			case MAIN_HAND:
				itemStack = getHandled().getEquipment().getItemInHand();
				break;
			case OFF_HAND:
				itemStack = !WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD) ? null : getHandled().getEquipment().getItemInOffHand();
				break;
		}
		return Optional.ofNullable(itemStack).map(Spigot13ItemStack::new);
	}


	@Override
	public void setEquippedItem(EnumEquipType type, WSItemStack itemStack) {
		switch (type) {
			case HELMET:
				getHandled().getEquipment().setHelmet(itemStack == null ? null : ((Spigot13ItemStack) itemStack).getHandled().clone());
				break;
			case CHESTPLATE:
				getHandled().getEquipment().setChestplate(itemStack == null ? null : ((Spigot13ItemStack) itemStack).getHandled().clone());
				break;
			case LEGGINGS:
				getHandled().getEquipment().setLeggings(itemStack == null ? null : ((Spigot13ItemStack) itemStack).getHandled().clone());
				break;
			case BOOTS:
				getHandled().getEquipment().setBoots(itemStack == null ? null : ((Spigot13ItemStack) itemStack).getHandled().clone());
				break;
			case MAIN_HAND:
				getHandled().getEquipment().setItemInHand(itemStack == null ? null : ((Spigot13ItemStack) itemStack).getHandled().clone());
				break;
			case OFF_HAND:
				if (WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD))
					getHandled().getEquipment().setItemInOffHand(itemStack == null ? null : ((Spigot13ItemStack) itemStack).getHandled().clone());
				break;
		}
	}

	@Override
	public boolean hasPermission(String name) {
		return getHandled().hasPermission(name);
	}

	@Override
	public Set<String> getPermissions() {
		return getHandled().getEffectivePermissions().stream().map(PermissionAttachmentInfo::getPermission).collect(Collectors.toSet());
	}


	@Override
	public HumanEntity getHandled() {
		return (HumanEntity) super.getHandled();
	}
}
