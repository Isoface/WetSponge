package com.degoos.wetsponge.entity.other;

import com.degoos.wetsponge.color.WSColor;
import com.degoos.wetsponge.effect.potion.Spigot13PotionEffect;
import com.degoos.wetsponge.effect.potion.WSPotionEffect;
import com.degoos.wetsponge.entity.Spigot13Entity;
import com.degoos.wetsponge.enums.EnumPotionEffectType;
import com.degoos.wetsponge.particle.WSParticle;
import com.degoos.wetsponge.particle.WSParticles;
import org.bukkit.Particle;
import org.bukkit.entity.AreaEffectCloud;
import org.bukkit.potion.PotionEffectType;

import java.util.List;
import java.util.stream.Collectors;

public class Spigot13AreaEffectCloud extends Spigot13Entity implements WSAreaEffectCloud {


    public Spigot13AreaEffectCloud(AreaEffectCloud entity) {
        super(entity);
    }

    @Override
    public WSColor getColor() {
        return WSColor.ofRGB(getHandled().getColor().asRGB());
    }

    @Override
    public void setColor(WSColor color) {
        getHandled().setColor(org.bukkit.Color.fromRGB(color.toRGB()));
    }

    @Override
    public double getRadius() {
        return getHandled().getRadius();
    }

    @Override
    public void setRadius(double radius) {
        getHandled().setRadius((float) radius);
    }

    @Override
    public WSParticle getParticle() {
        return WSParticles.getByParticleName(getHandled().getParticle().name()).orElse(WSParticles.SPLASH_POTION);
    }

    @Override
    public void setParticle(WSParticle particle) {
        getHandled().setParticle(Particle.valueOf(particle.getSpigotName()));
    }

    @Override
    public int getDuration() {
        return getHandled().getDuration();
    }

    @Override
    public void setDuration(int duration) {
        getHandled().setDuration(duration);
    }

    @Override
    public int getWaitTime() {
        return getHandled().getWaitTime();
    }

    @Override
    public void setWaitTime(int waitTime) {
        getHandled().setWaitTime(waitTime);
    }

    @Override
    public double getRadiusOnUse() {
        return getHandled().getRadiusOnUse();
    }

    @Override
    public void setRadiusOnUse(double radiusOnUse) {
        getHandled().setRadiusOnUse((float) radiusOnUse);
    }

    @Override
    public double getRadiusPerTick() {
        return getHandled().getRadiusPerTick();
    }

    @Override
    public void setRadiusPerTick(double radiusPerTick) {
        getHandled().setRadiusPerTick((float) radiusPerTick);
    }

    @Override
    public int getDurationOnUse() {
        return getHandled().getDurationOnUse();
    }

    @Override
    public void setDurationOnUse(int durationOnUse) {
        getHandled().setDurationOnUse(durationOnUse);
    }

    @Override
    public int getApplicationDelay() {
        return getHandled().getReapplicationDelay();
    }

    @Override
    public void setApplicationDelay(int applicationDelay) {
        getHandled().setReapplicationDelay(applicationDelay);
    }

    @Override
    public int getAge() {
        return 0;
    }

    @Override
    public void setAge(int age) {

    }

    @Override
    public void addPotionEffect(WSPotionEffect effect) {
        getHandled().addCustomEffect(((Spigot13PotionEffect) effect).getHandled(), true);
    }

    @Override
    public List<WSPotionEffect> getPotionEffects() {
        return getHandled().getCustomEffects().stream().map(Spigot13PotionEffect::new).collect(Collectors.toList());
    }

    @Override
    public void clearAllPotionEffects() {
        getHandled().clearCustomEffects();
    }

    @Override
    public void removePotionEffect(EnumPotionEffectType potionEffectType) {
        getHandled().removeCustomEffect(PotionEffectType.getById(potionEffectType.getValue()));
    }


    @Override
    public AreaEffectCloud getHandled() {
        return (AreaEffectCloud) super.getHandled();
    }
}
