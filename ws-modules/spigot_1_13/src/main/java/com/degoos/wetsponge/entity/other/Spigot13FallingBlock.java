package com.degoos.wetsponge.entity.other;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.entity.Spigot13Entity;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.material.WSBlockTypes;
import com.degoos.wetsponge.material.block.Spigot13BlockType;
import com.degoos.wetsponge.material.block.WSBlockType;
import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.Validate;
import com.degoos.wetsponge.util.reflection.NMSUtils;
import com.degoos.wetsponge.util.reflection.ReflectionUtils;
import com.degoos.wetsponge.util.reflection.Spigot13HandledUtils;
import org.bukkit.Material;
import org.bukkit.block.data.BlockData;
import org.bukkit.entity.FallingBlock;

import java.util.Optional;

public class Spigot13FallingBlock extends Spigot13Entity implements WSFallingBlock {

	private static Class<?> NMS_CLASS = NMSUtils.getNMSClass("EntityFallingBlock");

	public Spigot13FallingBlock(FallingBlock entity) {
		super(entity);
	}

	@Override
	public WSBlockType getBlockType() {

		BlockData blockData = getHandled().getBlockData();
		if (blockData == null || blockData.getMaterial().equals(Material.AIR)) return WSBlockTypes.AIR.getDefaultState();
		String id = getHandled().getBlockData().getMaterial().getKey().toString();
		Optional<WSBlockType> optional = WSBlockTypes.getById(id);
		if (!optional.isPresent()) return new Spigot13BlockType(-1, id, id, blockData.getMaterial().getMaxStackSize());
		return ((Spigot13BlockType) optional.get()).readBlockData(blockData);
	}

	@Override
	public void setBlockType(WSBlockType blockType) {
		Validate.notNull(blockType, "Block type cannot be null!");
		try {
			ReflectionUtils
					.setFirstObject(NMS_CLASS, NMSUtils.getNMSClass("IBlockData"), Spigot13HandledUtils.getEntityHandle(getHandled()),
							Spigot13HandledUtils.getBlockState(blockType));
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was modifying a FallingBlock!");
		}
	}

	@Override
	public double getFallDamagePerBlock() {
		try {
			return (double) ReflectionUtils.getObject(Spigot13HandledUtils.getEntityHandle(getHandled()), "fallHurtAmount");
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was modifying a FallingBlock!");
			return 0;
		}
	}

	@Override
	public void setFallDamagerPerBlock(double fallDamagerPerBlock) {
		try {
			ReflectionUtils.setAccessible(ReflectionUtils.getField(NMS_CLASS, "fallHurtAmount"))
					.setDouble(Spigot13HandledUtils.getEntityHandle(getHandled()), fallDamagerPerBlock);
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was modifying a FallingBlock!");
		}
	}

	@Override
	public double getMaxFallDamage() {
		try {
			return (double) ReflectionUtils.getObject(Spigot13HandledUtils.getEntityHandle(getHandled()), "fallHurtMax");
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was modifying a FallingBlock!");
			return 0;
		}
	}

	@Override
	public void setMaxFallDamage(double maxFallDamage) {
		try {
			ReflectionUtils.setAccessible(ReflectionUtils.getField(NMS_CLASS, "fallHurtMax")).setDouble(Spigot13HandledUtils.getEntityHandle(getHandled()), maxFallDamage);
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was modifying a FallingBlock!");
		}
	}

	@Override
	public boolean canPlaceAsBlock() {
		try {
			return (boolean) ReflectionUtils
					.getObject(Spigot13HandledUtils.getEntityHandle(getHandled()), WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD) ? "f" : "e");
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was modifying a FallingBlock!");
			return false;
		}
	}

	@Override
	public void setCanPlaceAsBlock(boolean canPlaceAsBlock) {
		try {
			ReflectionUtils.setAccessible(ReflectionUtils.getField(NMS_CLASS, WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD) ? "f" : "e"))
					.setBoolean(Spigot13HandledUtils.getEntityHandle(getHandled()), canPlaceAsBlock);
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was modifying a FallingBlock!");
		}
	}

	@Override
	public boolean canDropAsItem() {
		return getHandled().getDropItem();
	}

	@Override
	public void setCanDropAsItem(boolean canDropAsItem) {
		getHandled().setDropItem(canDropAsItem);
	}

	@Override
	public int getFallTime() {
		try {
			return (int) ReflectionUtils.getObject(Spigot13HandledUtils.getEntityHandle(getHandled()), "ticksLived");
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was modifying a FallingBlock!");
			return 0;
		}
	}

	@Override
	public void setFallTime(int fallTime) {
		try {
			ReflectionUtils.setAccessible(ReflectionUtils.getField(NMS_CLASS, "ticksLived")).setInt(Spigot13HandledUtils.getEntityHandle(getHandled()), fallTime);
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was modifying a FallingBlock!");
		}
	}

	@Override
	public boolean canHurtEntities() {
		return getHandled().canHurtEntities();
	}

	@Override
	public void setCanHurtEntities(boolean canHurtEntities) {
		getHandled().setHurtEntities(canHurtEntities);
	}

	@Override
	public FallingBlock getHandled() {
		return (FallingBlock) super.getHandled();
	}

}
