package com.degoos.wetsponge.entity.projectile;

import com.degoos.wetsponge.block.Spigot13Block;
import com.degoos.wetsponge.block.tileentity.Spigot13TileEntityDispenser;
import com.degoos.wetsponge.block.tileentity.WSTileEntityDispenser;
import com.degoos.wetsponge.entity.Spigot13Entity;
import com.degoos.wetsponge.entity.living.Spigot13LivingEntity;
import com.degoos.wetsponge.entity.living.WSLivingEntity;
import com.degoos.wetsponge.parser.entity.Spigot13EntityParser;
import org.bukkit.block.Dispenser;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Projectile;

public class Spigot13Projectile extends Spigot13Entity implements WSProjectile {


    public Spigot13Projectile(Projectile entity) {
        super(entity);
    }


    @Override
    public Projectile getHandled() {
        return (Projectile) super.getHandled();
    }

    @Override
    public WSProjectileSource getShooter() {
        if (getHandled().getShooter() instanceof LivingEntity)
            return (WSProjectileSource) Spigot13EntityParser.getWSEntity((LivingEntity) getHandled().getShooter());
        if (getHandled().getShooter() instanceof Dispenser)
            return new Spigot13TileEntityDispenser(new Spigot13Block(((Dispenser) getHandled().getShooter()).getBlock()));
        else return new WSUnkownProjectileSource();
    }

    @Override
    public void setShooter(WSProjectileSource source) {
        if (source instanceof WSLivingEntity)
            getHandled().setShooter(((Spigot13LivingEntity) source).getHandled());
        if (source instanceof WSTileEntityDispenser)
            getHandled().setShooter(((Spigot13TileEntityDispenser) source).getHandled().getBlockProjectileSource());
    }
}
