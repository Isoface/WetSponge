package com.degoos.wetsponge.entity.weather;

import com.degoos.wetsponge.entity.Spigot13Entity;
import org.bukkit.entity.Weather;

public class Spigot13WeatherEffect extends Spigot13Entity implements WSWeatherEffect {

	public Spigot13WeatherEffect(Weather entity) {
		super(entity);
	}

	@Override
	public Weather getHandled() {
		return (Weather) super.getHandled();
	}
}
