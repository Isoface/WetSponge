package com.degoos.wetsponge.hook.placeholderapi;

import com.degoos.wetsponge.entity.living.player.WSPlayer;
import me.clip.placeholderapi.expansion.PlaceholderExpansion;
import org.bukkit.entity.Player;

public class Spigot13PlaceholderAPIExpansion extends Spigot13PlaceholderAPIHook implements WSPlaceholderAPIExpansion {

	private PlaceholderExpansion expansion;

	public Spigot13PlaceholderAPIExpansion(PlaceholderExpansion expansion) {
		super(expansion, expansion.getIdentifier());
	}

	@Override
	public String getIdentifier() {
		return expansion.getIdentifier();
	}

	@Override
	public String getPlugin() {
		return expansion.getPlugin();
	}

	@Override
	public String getAuthor() {
		return expansion.getAuthor();
	}

	@Override
	public String getVersion() {
		return expansion.getVersion();
	}

	@Override
	public String onPlaceholderRequest(WSPlayer player, String identifier) {
		return expansion.onPlaceholderRequest((Player) player.getHandled(), identifier);
	}
}
