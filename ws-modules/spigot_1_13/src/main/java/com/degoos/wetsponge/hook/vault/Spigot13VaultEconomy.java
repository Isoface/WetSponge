package com.degoos.wetsponge.hook.vault;

import com.degoos.wetsponge.user.WSUser;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.OfflinePlayer;

import java.util.List;

public class Spigot13VaultEconomy implements WSVaultEconomy {

	private Economy economy;

	public Spigot13VaultEconomy(Economy economy) {
		this.economy = economy;
	}

	@Override
	public boolean isEnabled() {
		return economy.isEnabled();
	}

	@Override
	public String getName() {
		return economy.getName();
	}

	@Override
	public boolean hasBankSupport() {
		return economy.hasBankSupport();
	}

	@Override
	public int getFractionalDigits() {
		return economy.fractionalDigits();
	}

	@Override
	public String getCurrencyNamePlural() {
		return economy.currencyNamePlural();
	}

	@Override
	public String getCurrencyNameSingular() {
		return economy.currencyNameSingular();
	}

	@Override
	public String formatValue(double value) {
		return economy.format(value);
	}

	@Override
	public boolean hasAccount(WSUser user) {
		return economy.hasAccount((OfflinePlayer) user.getHandled());
	}

	@Override
	public boolean hasAccount(WSUser user, String account) {
		return economy.hasAccount((OfflinePlayer) user.getHandled(), account);
	}

	@Override
	public double getBalance(WSUser user) {
		return economy.getBalance((OfflinePlayer) user.getHandled());
	}

	@Override
	public double getBalance(WSUser user, String account) {
		return economy.getBalance((OfflinePlayer) user.getHandled(), account);
	}

	@Override
	public boolean hasMoney(WSUser user, double amount) {
		return economy.has((OfflinePlayer) user.getHandled(), amount);
	}

	@Override
	public boolean hasMoney(WSUser user, double amount, String account) {
		return economy.has((OfflinePlayer) user.getHandled(), account, amount);
	}

	@Override
	public WSVaultEconomyResponse withdrawPlayer(WSUser user, double amount) {
		return new Spigot13VaultEconomyResponse(economy.withdrawPlayer((OfflinePlayer) user.getHandled(), amount));
	}

	@Override
	public WSVaultEconomyResponse withdrawPlayer(WSUser user, double amount, String account) {
		return new Spigot13VaultEconomyResponse(economy.withdrawPlayer((OfflinePlayer) user.getHandled(), account, amount));
	}

	@Override
	public WSVaultEconomyResponse depositPlayer(WSUser user, double amount) {
		return new Spigot13VaultEconomyResponse(economy.depositPlayer((OfflinePlayer) user.getHandled(), amount));
	}

	@Override
	public WSVaultEconomyResponse depositPlayer(WSUser user, double amount, String account) {
		return new Spigot13VaultEconomyResponse(economy.depositPlayer((OfflinePlayer) user.getHandled(), account, amount));
	}

	@Override
	public WSVaultEconomyResponse createBank(String bank, WSUser user) {
		return new Spigot13VaultEconomyResponse(economy.createBank(bank, (OfflinePlayer) user.getHandled()));
	}

	@Override
	public WSVaultEconomyResponse deleteBank(String bank) {
		return new Spigot13VaultEconomyResponse(economy.deleteBank(bank));
	}

	@Override
	public WSVaultEconomyResponse bankBalance(String bank) {
		return new Spigot13VaultEconomyResponse(economy.bankBalance(bank));
	}

	@Override
	public WSVaultEconomyResponse bankHasMoney(String bank, double amount) {
		return new Spigot13VaultEconomyResponse(economy.bankHas(bank, amount));
	}

	@Override
	public WSVaultEconomyResponse bankWithdraw(String bank, double amount) {
		return new Spigot13VaultEconomyResponse(economy.bankWithdraw(bank, amount));
	}

	@Override
	public WSVaultEconomyResponse bankDeposit(String bank, double amount) {
		return new Spigot13VaultEconomyResponse(economy.bankDeposit(bank, amount));
	}

	@Override
	public WSVaultEconomyResponse isBankOwner(String bank, WSUser user) {
		return new Spigot13VaultEconomyResponse(economy.isBankOwner(bank, (OfflinePlayer) user.getHandled()));
	}

	@Override
	public WSVaultEconomyResponse isBankMember(String bank, WSUser user) {
		return new Spigot13VaultEconomyResponse(economy.isBankMember(bank, (OfflinePlayer) user.getHandled()));
	}

	@Override
	public List<String> getBanks() {
		return economy.getBanks();
	}

	@Override
	public boolean createPlayerAccount(WSUser user) {
		return economy.createPlayerAccount((OfflinePlayer) user.getHandled());
	}

	@Override
	public boolean createPlayerAccount(WSUser user, String account) {
		return economy.createPlayerAccount((OfflinePlayer) user.getHandled(), account);
	}
}
