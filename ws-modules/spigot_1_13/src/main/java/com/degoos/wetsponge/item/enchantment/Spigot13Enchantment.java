package com.degoos.wetsponge.item.enchantment;

import com.degoos.wetsponge.item.Spigot13ItemStack;
import com.degoos.wetsponge.item.WSItemStack;
import com.degoos.wetsponge.text.translation.WSTranslation;
import org.bukkit.NamespacedKey;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.enchantments.EnchantmentTarget;
import org.bukkit.inventory.ItemStack;

public class Spigot13Enchantment implements WSEnchantment {

	private Enchantment enchantment;

	public Spigot13Enchantment(Enchantment enchantment) {
		this.enchantment = enchantment;
	}

	public Spigot13Enchantment(String name) {
		String[] split = name.split(":");
		if (split.length == 1)
			enchantment = Enchantment.getByKey(new NamespacedKey("minecraft", split[0]));
		else enchantment = Enchantment.getByKey(new NamespacedKey(split[0], split[1]));
		if (enchantment == null) enchantment = Enchantment.PROTECTION_ENVIRONMENTAL;
	}

	public Spigot13Enchantment(String key, String name, int minimumLevel, int maximumLevel, WSEnchantmentPrototype prototype) {

		String[] split = name.split(":");
		NamespacedKey namespacedKey = split.length == 1 ? new NamespacedKey("minecraft", split[0]) : new NamespacedKey(split[0], split[1]);

		enchantment = new Enchantment(namespacedKey) {
			@Override
			public String getName() {
				return name;
			}

			@Override
			public int getMaxLevel() {
				return maximumLevel;
			}

			@Override
			public int getStartLevel() {
				return minimumLevel;
			}

			@Override
			public EnchantmentTarget getItemTarget() {
				return EnchantmentTarget.ALL;
			}

			@Override
			public boolean isTreasure() {
				return false;
			}

			@Override
			public boolean isCursed() {
				return false;
			}

			@Override
			public boolean conflictsWith(Enchantment enchantment) {
				return !prototype.isCompatibleWith(new Spigot13Enchantment(enchantment));
			}

			@Override
			public boolean canEnchantItem(ItemStack itemStack) {
				return prototype.canBeAppliedToStack(new Spigot13ItemStack(itemStack));
			}
		};
	}

	@Override
	public WSTranslation getTranslation() {
		throw new IllegalAccessError("Not supported by Spigot");
	}

	@Override
	public String getName() {
		return enchantment.getKey().toString();
	}

	@Override
	public int getMaximumLevel() {
		return enchantment.getMaxLevel();
	}

	@Override
	public int getMinimumLevel() {
		return enchantment.getStartLevel();
	}

	@Override
	public boolean canBeAppliedToStack(WSItemStack itemStack) {
		return enchantment.canEnchantItem(((Spigot13ItemStack) itemStack).getHandled());
	}

	@Override
	public boolean isCompatibleWith(WSEnchantment enchantment) {
		return !this.enchantment.conflictsWith(((Spigot13Enchantment) enchantment).getHandled());
	}

	@Override
	public Enchantment getHandled() {
		return enchantment;
	}
}
