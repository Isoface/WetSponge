package com.degoos.wetsponge.listener.spigot;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.block.Spigot13Block;
import com.degoos.wetsponge.block.Spigot13BlockSnapshot;
import com.degoos.wetsponge.block.WSBlockSnapshot;
import com.degoos.wetsponge.data.WSTransaction;
import com.degoos.wetsponge.event.block.WSBlockBreakEvent;
import com.degoos.wetsponge.event.block.WSBlockChangeEvent;
import com.degoos.wetsponge.event.block.WSBlockModifyEvent;
import com.degoos.wetsponge.event.block.WSBlockPlaceEvent;
import com.degoos.wetsponge.material.WSBlockTypes;
import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.SpigotEventUtils;
import com.degoos.wetsponge.world.Spigot13Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockFromToEvent;
import org.bukkit.event.block.BlockPlaceEvent;

import java.util.Optional;

public class Spigot13BlockListener implements Listener {

	@EventHandler(priority = EventPriority.LOWEST)
	public void onBlockBreak(BlockBreakEvent event) {
		if (!SpigotEventUtils.shouldBeExecuted()) return;
		try {
			WSTransaction<WSBlockSnapshot> transaction = new WSTransaction<>(new Spigot13BlockSnapshot(event.getBlock()), new WSBlockSnapshot(WSBlockTypes.AIR
					.getDefaultState()));

			WSBlockBreakEvent wetSpongeEvent = new WSBlockBreakEvent(transaction, new Spigot13Location(event.getBlock().getLocation()), Optional
					.ofNullable(event.getPlayer())
					.map(player -> WetSponge.getServer().getPlayer(player.getName()).<NullPointerException>orElseThrow(NullPointerException::new)));
			WetSponge.getEventManager().callEvent(wetSpongeEvent);

			event.setCancelled(wetSpongeEvent.isCancelled());

			if (!wetSpongeEvent.getTransaction().getNewData().getMaterial().getStringId().equalsIgnoreCase(WSBlockTypes.AIR.getStringId())) {
				wetSpongeEvent.setCancelled(true);
				wetSpongeEvent.getTransaction().getNewData().setToBlock(new Spigot13Block(event.getBlock()));
			}
		} catch (Throwable ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was parsing the event Spigot-BlockBreakEvent!");
		}
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void onBlockPlaced(BlockPlaceEvent event) {
		if (!SpigotEventUtils.shouldBeExecuted()) return;
		try {
			WSTransaction<WSBlockSnapshot> transaction = new WSTransaction<>(new Spigot13BlockSnapshot(event.getBlockAgainst()), new Spigot13BlockSnapshot(event
					.getBlockPlaced()));

			WSBlockPlaceEvent wetSpongeEvent = new WSBlockPlaceEvent(transaction, new Spigot13Location(event.getBlock().getLocation()), Optional
					.ofNullable(event.getPlayer())
					.map(player -> WetSponge.getServer().getPlayer(player.getName()).<NullPointerException>orElseThrow(NullPointerException::new)));
			WetSponge.getEventManager().callEvent(wetSpongeEvent);

			event.setCancelled(wetSpongeEvent.isCancelled());

			if (!wetSpongeEvent.isCancelled() && wetSpongeEvent.getTransaction().getNewData().hasChanged())
				wetSpongeEvent.getTransaction().getNewData().setToBlock(new Spigot13Block(event.getBlockPlaced()));
		} catch (Throwable ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was parsing the event Spigot-BlockPlaceEvent!");
		}
	}

	@EventHandler
	public void onBlockFromTo(BlockFromToEvent event) {
		if (!SpigotEventUtils.shouldBeExecuted()) return;
		try {
			WSTransaction<WSBlockSnapshot> transaction = new WSTransaction<>(new Spigot13BlockSnapshot(event.getBlock()), new Spigot13BlockSnapshot(event.getToBlock()));

			WSBlockChangeEvent wetSpongeEvent;
			if (transaction.getNewData().getMaterial().getStringId().equalsIgnoreCase(transaction.getOldData().getMaterial().getStringId()))
				wetSpongeEvent = new WSBlockModifyEvent(transaction, new Spigot13Location(event.getBlock().getLocation()), Optional.empty());
			else wetSpongeEvent = new WSBlockChangeEvent(transaction, new Spigot13Location(event.getBlock().getLocation()));
			WetSponge.getEventManager().callEvent(wetSpongeEvent);
			event.setCancelled(wetSpongeEvent.isCancelled());

			if (!wetSpongeEvent.isCancelled() && wetSpongeEvent.getTransaction().getNewData().hasChanged())
				wetSpongeEvent.getTransaction().getNewData().setToBlock(new Spigot13Block(event.getToBlock()));
		} catch (Throwable ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was parsing the event Spigot-BlockFromToEvent!");
		}
	}

}
