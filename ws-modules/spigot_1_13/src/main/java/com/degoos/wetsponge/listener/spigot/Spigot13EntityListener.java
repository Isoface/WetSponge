package com.degoos.wetsponge.listener.spigot;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.block.Spigot13Block;
import com.degoos.wetsponge.entity.WSEntity;
import com.degoos.wetsponge.entity.living.WSLivingEntity;
import com.degoos.wetsponge.enums.EnumDamageType;
import com.degoos.wetsponge.enums.EnumHealingType;
import com.degoos.wetsponge.event.entity.*;
import com.degoos.wetsponge.event.entity.WSEntityDeathEvent.Post;
import com.degoos.wetsponge.event.entity.WSEntityDeathEvent.Pre;
import com.degoos.wetsponge.parser.entity.Spigot13EntityParser;
import com.degoos.wetsponge.text.WSText;
import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.SpigotEventUtils;
import com.degoos.wetsponge.util.reflection.ReflectionUtils;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.*;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

public class Spigot13EntityListener implements Listener {

	@EventHandler(priority = EventPriority.LOWEST)
	public void onEntitySpawn(EntitySpawnEvent event) {
		if (!SpigotEventUtils.shouldBeExecuted()) return;
		try {
			WSEntity wsEntity = Spigot13EntityParser.getWSEntity(event.getEntity());
			WSEntitySpawnEvent wetSpongeEvent = new WSEntitySpawnEvent(wsEntity);
			WetSponge.getEventManager().callEvent(wetSpongeEvent);
			event.setCancelled(wetSpongeEvent.isCancelled());
		} catch (Throwable ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was parsing the event Spigot-EntitySpawnEvent!");
		}
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void onEntityDespawn(EntityDeathEvent event) {
		if (!SpigotEventUtils.shouldBeExecuted()) return;
		try {
			WSEntity entity = Spigot13EntityParser.getWSEntity(event.getEntity());
			WSEntityDestructEvent wetSpongeEvent = new WSEntityDestructEvent(entity);
			WetSponge.getEventManager().callEvent(wetSpongeEvent);
			Spigot13EntityParser.removeEntity(event.getEntity());
		} catch (Throwable ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was parsing the event Spigot-EntityDeathEvent!");
		}
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void onEntityDamage(EntityDamageEvent event) {
		if (!SpigotEventUtils.shouldBeExecuted()) return;
		try {
			WSEntity entity = Spigot13EntityParser.getWSEntity(event.getEntity());
			if (entity.isInvincible()) {
				event.setCancelled(true);
				return;
			}
			WSEntityDamageEvent wetSpongeEvent;
			if (event instanceof EntityDamageByEntityEvent) {
				wetSpongeEvent = new WSEntityDamageByEntityEvent(entity, event.getFinalDamage(), new HashSet<>(), EnumDamageType.getBySpigotName(event.getCause().name())
					.orElse(EnumDamageType.CUSTOM), Spigot13EntityParser.getWSEntity(((EntityDamageByEntityEvent) event).getDamager()), event.getFinalDamage());
			} else if (event instanceof EntityDamageByBlockEvent) {
				wetSpongeEvent = new WSEntityDamageByBlockEvent(entity, event.getFinalDamage(), new HashSet<>(), EnumDamageType.getBySpigotName(event.getCause().name())
					.orElse(EnumDamageType.CUSTOM), new Spigot13Block(((EntityDamageByBlockEvent) event).getDamager()), event.getFinalDamage());
			} else wetSpongeEvent = new WSEntityDamageEvent(entity, event.getFinalDamage(), new HashSet<>(), EnumDamageType.getBySpigotName(event.getCause().name())
				.orElse(EnumDamageType.CUSTOM), event.getFinalDamage());
			WetSponge.getEventManager().callEvent(wetSpongeEvent);

			Map<EntityDamageEvent.DamageModifier, Double> modifiers = (Map<EntityDamageEvent.DamageModifier, Double>) ReflectionUtils
				.getFirstObject(EntityDamageEvent.class, Map.class, event);
			Map<EntityDamageEvent.DamageModifier, Double> newMap = new HashMap<>(modifiers);
			newMap.forEach((damageModifier, aDouble) -> modifiers.put(damageModifier, 0.0));

			event.setDamage(EntityDamageEvent.DamageModifier.BASE, wetSpongeEvent.getBaseDamage());
			event.setCancelled(wetSpongeEvent.isCancelled());
			if (entity instanceof WSLivingEntity && !event.isCancelled() && ((WSLivingEntity) entity).getHealth() - event.getFinalDamage() <= 0) {
				Pre deathEvent = new Pre(entity);
				WetSponge.getEventManager().callEvent(deathEvent);
				event.setCancelled(deathEvent.isCancelled());
			}
		} catch (Throwable ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was parsing the event Spigot-EntityDamageEvent!");
		}
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void onEntityHeal(EntityRegainHealthEvent event) {
		if (!SpigotEventUtils.shouldBeExecuted()) return;
		try {
			WSEntityHealEvent wetSpongeEvent = new WSEntityHealEvent(Spigot13EntityParser.getWSEntity(event.getEntity()), event
				.getAmount(), new HashSet<>(), EnumHealingType.getBySpigotName(event.getRegainReason().name()).orElse(EnumHealingType.GENERIC));
			WetSponge.getEventManager().callEvent(wetSpongeEvent);
			event.setAmount(wetSpongeEvent.getFinalHealth());
			event.setCancelled(wetSpongeEvent.isCancelled());
		} catch (Throwable ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was parsing the event Spigot-EntityRegainHealthEvent!");
		}
	}

	@EventHandler
	public void onEntityDeath(EntityDeathEvent event) {
		Post wetSpongeEvent = new Post(Spigot13EntityParser.getWSEntity(event.getEntity()),
			event instanceof PlayerDeathEvent && ((PlayerDeathEvent) event).getKeepInventory(),
			event instanceof PlayerDeathEvent ? WSText.getByFormattingText(((PlayerDeathEvent) event).getDeathMessage()) : WSText.empty());
		WetSponge.getEventManager().callEvent(wetSpongeEvent);
		if (event instanceof PlayerDeathEvent) {
			((PlayerDeathEvent) event).setDeathMessage(wetSpongeEvent.getDeathMessage().toFormattingText());
			((PlayerDeathEvent) event).setKeepInventory(wetSpongeEvent.keepInventory());
		}
	}

}
