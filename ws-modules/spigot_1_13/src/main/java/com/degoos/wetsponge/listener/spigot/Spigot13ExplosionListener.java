package com.degoos.wetsponge.listener.spigot;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.entity.WSEntity;
import com.degoos.wetsponge.entity.explosive.WSExplosive;
import com.degoos.wetsponge.event.world.WSExplosionEvent.Detonate;
import com.degoos.wetsponge.event.world.WSExplosionEvent.Pre;
import com.degoos.wetsponge.parser.entity.Spigot13EntityParser;
import com.degoos.wetsponge.util.SpigotEventUtils;
import com.degoos.wetsponge.world.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityExplodeEvent;

import java.util.stream.Collectors;

public class Spigot13ExplosionListener implements Listener {

	@EventHandler(priority = EventPriority.LOWEST)
	public void onExplosion(EntityExplodeEvent event) {
		if(!SpigotEventUtils.shouldBeExecuted()) return;
		WSLocation location = new Spigot13Location(event.getLocation());
		WSWorld world = location.getWorld();
		WSEntity wsEntity = event.getEntity() == null ? null : Spigot13EntityParser.getWSEntity(event.getEntity());
		WSExplosive explosive = wsEntity instanceof WSExplosive ? (WSExplosive) wsEntity : null;
		Pre pre = new Pre(world, new Spigot13Explosion(explosive, location, event.getYield(), false, true, event.blockList().isEmpty(), true));
		WetSponge.getEventManager().callEvent(pre);

		WSExplosion explosion = pre.getExplosion();

		event.setYield(explosion.getRadius());
		if (!explosion.shouldBreakBlocks()) event.blockList().clear();

		if (pre.isCancelled()) {
			event.setCancelled(true);
			return;
		}

		Detonate detonate = new Detonate(world, explosion, event.blockList().stream().map(b -> new Spigot13Location(b.getLocation())).collect(Collectors.toList()));
		WetSponge.getEventManager().callEvent(detonate);
		event.blockList().clear();
		event.blockList().addAll(detonate.getAffectedLocations().stream().map(l -> ((Spigot13Location) l).getLocation().getBlock()).collect(Collectors.toList()));
	}

}
