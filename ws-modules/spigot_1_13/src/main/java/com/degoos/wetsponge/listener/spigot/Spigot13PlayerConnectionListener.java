package com.degoos.wetsponge.listener.spigot;


import com.degoos.wetsponge.SpigotWetSponge;
import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.command.wetspongecommand.WetSpongeSubcommandErrors;
import com.degoos.wetsponge.entity.living.WSLivingEntity;
import com.degoos.wetsponge.entity.living.player.Spigot13Player;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.event.entity.player.connection.WSPlayerJoinEvent;
import com.degoos.wetsponge.event.entity.player.connection.WSPlayerLoginEvent;
import com.degoos.wetsponge.event.entity.player.connection.WSPlayerQuitEvent;
import com.degoos.wetsponge.inventory.multiinventory.MultiInventoryListener;
import com.degoos.wetsponge.packet.play.server.WSSPacketDestroyEntities;
import com.degoos.wetsponge.packet.play.server.WSSPacketSpawnMob;
import com.degoos.wetsponge.parser.player.PlayerParser;
import com.degoos.wetsponge.text.WSText;
import com.degoos.wetsponge.user.Spigot13User;
import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.SpigotEventUtils;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Optional;

public class Spigot13PlayerConnectionListener implements Listener {

	@EventHandler(priority = EventPriority.LOWEST)
	public void onLogin(PlayerLoginEvent event) {
		if (!SpigotEventUtils.shouldBeExecuted()) return;
		try {
			WSPlayerLoginEvent wetSpongeEvent = new WSPlayerLoginEvent(new Spigot13User(event.getPlayer()), WSText.of(""), WSText
					.getByFormattingText(event.getKickMessage()), WSText.of(""));
			WetSponge.getEventManager().callEvent(wetSpongeEvent);
			if (wetSpongeEvent.isCancelled()) event.disallow(PlayerLoginEvent.Result.KICK_OTHER,
					wetSpongeEvent.getCancelledMessageHeader().toFormattingText() + "\\n" + wetSpongeEvent.getCancelledMessage().toFormattingText() + "\\n" +
							wetSpongeEvent.getCancelledMessageFooter().toFormattingText());
		} catch (Throwable ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was parsing the event Spigot-PlayerLoginEvent!");
		}
	}


	@EventHandler(priority = EventPriority.LOWEST)
	public void onJoin(PlayerJoinEvent event) {
		if (!SpigotEventUtils.shouldBeExecuted()) return;
		try {
			Spigot13Player player = new Spigot13Player(event.getPlayer());
			PlayerParser.addPlayer(player);
			Spigot13PacketListener.inject(player);
			WSPlayerJoinEvent wetSpongeEvent = new WSPlayerJoinEvent(player, WSText.getByFormattingText(event.getJoinMessage()), WSText
					.getByFormattingText(event.getJoinMessage()));
			WetSponge.getEventManager().callEvent(wetSpongeEvent);
			if (wetSpongeEvent.getMessage() == null) event.setJoinMessage(null);
			else event.setJoinMessage(wetSpongeEvent.getMessage().toFormattingText());
			new BukkitRunnable() {
				@Override
				public void run() {
					player.getWorld().getPlayers().stream().filter(WSLivingEntity::hasDisguise).forEach(target -> {
						player.sendPacket(WSSPacketDestroyEntities.of(target.getEntityId()));
						player.sendPacket(WSSPacketSpawnMob.of(target));
					});
				}
			}.runTaskLater(SpigotWetSponge.getInstance(), 10);
			if (player.hasPermission("wetsponge.admin") && InternalLogger.getLastStackTrace() != null) WetSpongeSubcommandErrors.sendErrors(player);
		} catch (Throwable ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was parsing the event Spigot-PlayerJoinEvent!");
		}
	}


	@EventHandler(priority = EventPriority.LOWEST)
	public void onQuit(PlayerQuitEvent event) {
		if (!SpigotEventUtils.shouldBeExecuted()) return;
		try {
			Optional<WSPlayer> player = WetSponge.getServer().getPlayer(event.getPlayer().getUniqueId());
			if (!player.isPresent()) return;
			WSPlayerQuitEvent wetSpongeEvent = new WSPlayerQuitEvent(player.get(), WSText.getByFormattingText(event.getQuitMessage()), WSText
					.getByFormattingText(event.getQuitMessage()));
			WetSponge.getEventManager().callEvent(wetSpongeEvent);
			MultiInventoryListener.leave(wetSpongeEvent);
			if (wetSpongeEvent.getMessage() == null) event.setQuitMessage(null);
			else event.setQuitMessage(wetSpongeEvent.getMessage().toFormattingText());
			Spigot13PacketListener.uninject(player.get());
			PlayerParser.removePlayer(player.get());
		} catch (Throwable ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was parsing the event Spigot-PlayerQuitEvent!");
		}
	}

}
