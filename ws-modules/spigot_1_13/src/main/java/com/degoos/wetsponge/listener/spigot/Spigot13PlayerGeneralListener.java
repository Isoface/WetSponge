package com.degoos.wetsponge.listener.spigot;

import com.degoos.wetsponge.SpigotWetSponge;
import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.block.Spigot13Block;
import com.degoos.wetsponge.data.WSTransaction;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.entity.other.WSItem;
import com.degoos.wetsponge.enums.EnumResourcePackStatus;
import com.degoos.wetsponge.event.entity.WSEntityDismountEvent;
import com.degoos.wetsponge.event.entity.WSEntityMountEvent;
import com.degoos.wetsponge.event.entity.player.*;
import com.degoos.wetsponge.event.entity.player.bed.WSPlayerEnterBedEvent;
import com.degoos.wetsponge.event.entity.player.bed.WSPlayerLeaveBedEvent;
import com.degoos.wetsponge.parser.entity.Spigot13EntityParser;
import com.degoos.wetsponge.parser.player.PlayerParser;
import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.SpigotEventUtils;
import com.degoos.wetsponge.world.Spigot13Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.player.*;
import org.bukkit.scheduler.BukkitRunnable;
import org.spigotmc.event.entity.EntityDismountEvent;
import org.spigotmc.event.entity.EntityMountEvent;

public class Spigot13PlayerGeneralListener implements Listener {


	@EventHandler(priority = EventPriority.LOWEST)
	public void onBedEnter(PlayerBedEnterEvent event) {
		if (!SpigotEventUtils.shouldBeExecuted()) return;
		try {
			WSPlayerEnterBedEvent wetSpongeEvent = new WSPlayerEnterBedEvent(PlayerParser.getPlayer(event.getPlayer().getUniqueId()).orElse(null), new Spigot13Block(event
				.getBed()));
			WetSponge.getEventManager().callEvent(wetSpongeEvent);
			event.setCancelled(wetSpongeEvent.isCancelled());
		} catch (Throwable ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was parsing the event Spigot-PlayerBedEnterEvent!");
		}
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void onBedLeave(PlayerBedLeaveEvent event) {
		if (!SpigotEventUtils.shouldBeExecuted()) return;
		try {
			WSPlayerLeaveBedEvent wetSpongeEvent = new WSPlayerLeaveBedEvent(PlayerParser.getPlayer(event.getPlayer().getUniqueId()).orElse(null), new Spigot13Block(event
				.getBed()));
			WetSponge.getEventManager().callEvent(wetSpongeEvent);
		} catch (Throwable ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was parsing the event Spigot-PlayerBedLeaveEvent!");
		}
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void onResourcePackStatus(PlayerResourcePackStatusEvent event) {
		if (!SpigotEventUtils.shouldBeExecuted()) return;
		try {
			WetSponge.getEventManager()
				.callEvent(new WSPlayerResourcePackStatusEvent(WetSponge.getServer().getPlayer(event.getPlayer().getUniqueId()).orElse(null), EnumResourcePackStatus
					.getBySpigotName(event.getStatus().name()).orElseThrow(NullPointerException::new)));
		} catch (Throwable ex) {
			InternalLogger.printException(ex,
				"An error has occurred while WetSponge was parsing the event Spigot-PlayerResourcePackStatusEvent! (" + event.getStatus().name() + ")");
		}
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void onRespawn(PlayerRespawnEvent event) {
		if (!SpigotEventUtils.shouldBeExecuted()) return;
		try {
			PlayerParser.resetPlayer(event.getPlayer(), event.getPlayer().getUniqueId());
			WSPlayer player = WetSponge.getServer().getPlayer(event.getPlayer().getUniqueId()).orElse(null);
			WSPlayerRespawnEvent wetSpongeEvent = new WSPlayerRespawnEvent(player, new WSTransaction<>(null, new Spigot13Location(event.getRespawnLocation())), event
				.isBedSpawn());
			WetSponge.getEventManager().callEvent(wetSpongeEvent);
			event.setRespawnLocation(((Spigot13Location) wetSpongeEvent.getLocationTransform().getNewData()).getLocation());

			player.getFakeBlocks().forEach((location, type) -> {
				if (location.distance(player.getLocation()) <= 100) player.refreshFakeBlock(location);
			});
		} catch (Throwable ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was parsing the event Spigot-PlayerRespawnEvent!");
		}
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void onDropItem(PlayerDropItemEvent event) {
		if (!SpigotEventUtils.shouldBeExecuted()) return;
		try {
			WSPlayerDropItemEvent wetSpongeEvent = new WSPlayerDropItemEvent(PlayerParser.getPlayer(event.getPlayer().getUniqueId())
				.orElseThrow(NullPointerException::new), (WSItem) Spigot13EntityParser.getWSEntity(event.getItemDrop()));
			WetSponge.getEventManager().callEvent(wetSpongeEvent);
			event.setCancelled(wetSpongeEvent.isCancelled());
		} catch (Throwable ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was parsing the event Spigot-PlayerDropItemEvent!");
		}
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void onItemPickup(PlayerPickupItemEvent event) {
		if (!SpigotEventUtils.shouldBeExecuted()) return;
		try {
			WSPlayerPickupItemEvent wetSpongeEvent = new WSPlayerPickupItemEvent(PlayerParser.getPlayer(event.getPlayer().getUniqueId())
				.orElseThrow(NullPointerException::new), (WSItem) Spigot13EntityParser.getWSEntity(event.getItem()));
			WetSponge.getEventManager().callEvent(wetSpongeEvent);
			event.setCancelled(wetSpongeEvent.isCancelled());
		} catch (Throwable ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was parsing the event Spigot-PlayerPickupItemEvent!");
		}
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void onFoodLevelChange(FoodLevelChangeEvent event) {
		if (!SpigotEventUtils.shouldBeExecuted()) return;
		if (!(event.getEntity() instanceof Player)) return;
		try {
			WSPlayerFoodLevelChangeEvent wetSpongeEvent = new WSPlayerFoodLevelChangeEvent(PlayerParser.getPlayer(event.getEntity().getUniqueId())
				.orElseThrow(NullPointerException::new), event.getFoodLevel(), event.getFoodLevel());
			WetSponge.getEventManager().callEvent(wetSpongeEvent);
			event.setFoodLevel(event.getFoodLevel());
			event.setCancelled(wetSpongeEvent.isCancelled());
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was parsing the event Spigot-FoodLevelChangeEvent!");
		}
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void onEntityRide(EntityMountEvent event) {
		if (!SpigotEventUtils.shouldBeExecuted()) return;
		WSEntityMountEvent wetSpongeEvent = new WSEntityMountEvent(Spigot13EntityParser.getWSEntity(event.getEntity()), Spigot13EntityParser.getWSEntity(event.getMount()));
		WetSponge.getEventManager().callEvent(wetSpongeEvent);
		event.setCancelled(wetSpongeEvent.isCancelled());
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void onEntityRide(EntityDismountEvent event) {
		if (!SpigotEventUtils.shouldBeExecuted()) return;
		WSEntityDismountEvent wetSpongeEvent = new WSEntityDismountEvent(Spigot13EntityParser.getWSEntity(event.getEntity()), Spigot13EntityParser
			.getWSEntity(event.getDismounted()));
		WetSponge.getEventManager().callEvent(wetSpongeEvent);
		if (wetSpongeEvent.isCancelled()) new BukkitRunnable() {
			@Override
			public void run() {
				wetSpongeEvent.getVehicle().addPassenger(wetSpongeEvent.getEntity());
			}
		}.runTaskLater(SpigotWetSponge.getInstance(), 5);
	}
}
