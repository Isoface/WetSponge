package com.degoos.wetsponge.listener.spigot;


import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.event.world.WSChunkLoadEvent;
import com.degoos.wetsponge.event.world.WSChunkUnloadEvent;
import com.degoos.wetsponge.parser.world.WorldParser;
import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.reflection.FieldUtils;
import com.degoos.wetsponge.util.reflection.NMSUtils;
import com.degoos.wetsponge.world.Spigot13Chunk;
import com.degoos.wetsponge.world.Spigot13Location;
import com.degoos.wetsponge.world.Spigot13World;
import com.degoos.wetsponge.world.WSWorld;
import com.degoos.wetsponge.world.generation.Spigot13BlockVolume;
import com.degoos.wetsponge.world.generation.Spigot13WorldGenerator;
import com.degoos.wetsponge.world.generation.WSBlockVolume;
import com.degoos.wetsponge.world.generation.WSWorldGenerator;
import com.degoos.wetsponge.world.generation.populator.WSGenerationPopulator;
import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.World;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.world.ChunkLoadEvent;
import org.bukkit.event.world.ChunkUnloadEvent;
import org.bukkit.event.world.WorldLoadEvent;
import org.bukkit.event.world.WorldUnloadEvent;
import org.bukkit.generator.ChunkGenerator;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.*;

public class Spigot13WorldListener implements Listener {

	private static Map<String, World> worlds = new HashMap<>();
	public static final List<Chunk> dontUnloadChunks = new ArrayList<>();

	public Spigot13WorldListener() {
		Bukkit.getWorlds().forEach(world -> {
			injectGenerator(world);
			worlds.put(world.getName().toLowerCase(), world);
		});
	}


	public static Optional<World> getWorld(String name) {
		return worlds.keySet().stream().filter(worldName -> worldName.equalsIgnoreCase(name)).findAny().map(s -> worlds.get(s));
	}

	public static void refreshGenerator(World world, ChunkGenerator generator, boolean isNull) {
		try {
			FieldUtils.setFinal(world.getClass().getDeclaredField("generator"), generator, world);
			Object handled = world.getClass().getDeclaredMethod("getHandle").invoke(world);
			NMSUtils.getNMSClass("World").getDeclaredField("generator").set(handled, isNull ? null : generator);
			Field chunkProvider = NMSUtils.getNMSClass("World").getDeclaredField("chunkProvider");
			chunkProvider.setAccessible(true);
			Method n = NMSUtils.getNMSClass("WorldServer").getDeclaredMethod("r");
			n.setAccessible(true);
			chunkProvider.set(handled, n.invoke(handled));
		} catch (Throwable e1) {
			e1.printStackTrace();
		}
	}


	@EventHandler(priority = EventPriority.LOWEST)
	public void load(WorldLoadEvent e) {
		if (e.getWorld() == null) {
			InternalLogger.sendWarning("Null world in WorldLoadEvent!");
			return;
		}
		try {
			World world = e.getWorld();
			injectGenerator(world);
			worlds.put(world.getName().toLowerCase(), e.getWorld());
			if (Spigot13Location.locations.containsKey(world.getName())) Spigot13Location.locations.get(world.getName()).forEach(Spigot13Location::updateWorld);
		} catch (Throwable ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was parsing the event Spigot-WorldLoadEvent!");
		}
	}


	@EventHandler(priority = EventPriority.LOWEST)
	public void unload(WorldUnloadEvent e) {
		try {
			worlds.remove(e.getWorld().getName().toLowerCase());
		} catch (Throwable ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was parsing the event Spigot-WorldUnloadEvent!");
		}
	}


	@EventHandler(priority = EventPriority.LOWEST)
	public void onChunkLoad(ChunkLoadEvent event) {
		WetSponge.getEventManager()
				.callEvent(new WSChunkLoadEvent(WorldParser.getOrCreateWorld(event.getWorld().getName(), event.getWorld()), new Spigot13Chunk(event.getChunk())));
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void onChunkLoad(ChunkUnloadEvent event) {
		WSChunkUnloadEvent wetSpongeEvent = new WSChunkUnloadEvent(WorldParser.getOrCreateWorld(event.getWorld().getName(), event.getWorld()), new Spigot13Chunk(event
				.getChunk()), dontUnloadChunks.contains(event.getChunk()));
		WetSponge.getEventManager().callEvent(wetSpongeEvent);
		event.setCancelled(wetSpongeEvent.isCancelled());
	}


	private void injectGenerator(World world) {
		ChunkGenerator chunkGenerator = world.getGenerator();

		if (!(chunkGenerator instanceof WSWorldGenerator)) {

			Spigot13WorldGenerator generator = new Spigot13WorldGenerator();
			generator.setBaseGenerationPopulator(new WSGenerationPopulator() {
				private ChunkGenerator generator = chunkGenerator;


				@Override
				public void populate(WSWorld world, WSBlockVolume volume) {
					if (generator == null) return;
					Spigot13BlockVolume spigot13BlockVolume = (Spigot13BlockVolume) volume;
					ChunkGenerator.ChunkData data = generator
							.generateChunkData(((Spigot13World) world).getHandled(), spigot13BlockVolume.getRandom(), spigot13BlockVolume.getX(), spigot13BlockVolume
									.getZ(), spigot13BlockVolume.getBiome());
					for (int x = 0; x < 16; x++)
						for (int y = 0; y <= data.getMaxHeight(); y++)
							for (int z = 0; z < 16; z++)
								spigot13BlockVolume.getHandled().setBlock(x, y, z, data.getTypeAndData(x, y, z));
				}
			});
			refreshGenerator(world, generator, chunkGenerator == null);
		}
	}
}
