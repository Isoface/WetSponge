package com.degoos.wetsponge.loader;


import com.degoos.wetsponge.SpigotWetSponge;
import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.listener.spigot.*;
import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;

public class Spigot13ListenerLoader {

	private static boolean loaded = false;


	public static void load() {
		if (loaded) return;
		PluginManager manager = Bukkit.getPluginManager();
		manager.registerEvents(new Spigot13WorldListener(), SpigotWetSponge.getInstance());
		manager.registerEvents(new Spigot13SendCommandListener(), SpigotWetSponge.getInstance());
		manager.registerEvents(new Spigot13InventoryListener(), SpigotWetSponge.getInstance());
		manager.registerEvents(new Spigot13PlayerConnectionListener(), SpigotWetSponge.getInstance());
		manager.registerEvents(new Spigot13MovementListener(), SpigotWetSponge.getInstance());
		manager.registerEvents(new Spigot13PlayerGeneralListener(), SpigotWetSponge.getInstance());
		manager.registerEvents(new Spigot13EntityListener(), SpigotWetSponge.getInstance());
		manager.registerEvents(new Spigot13SignListener(), SpigotWetSponge.getInstance());
		manager.registerEvents(new Spigot13BlockListener(), SpigotWetSponge.getInstance());
		manager.registerEvents(new Spigot13PlayerInteractListener(), SpigotWetSponge.getInstance());
		manager.registerEvents(new Spigot13ServerListener(), SpigotWetSponge.getInstance());
		manager.registerEvents(new Spigot13ExplosionListener(), SpigotWetSponge.getInstance());
		manager.registerEvents(new Spigot13PluginListener(), SpigotWetSponge.getInstance());
		manager.registerEvents(new Spigot13ChatListener(), SpigotWetSponge.getInstance());
		if (WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD)) {
			manager.registerEvents(new Spigot13NewVersionListener(), SpigotWetSponge.getInstance());
			manager.registerEvents(new Spigot13TabCompleteListener(), SpigotWetSponge.getInstance());
		}

		loaded = true;
	}
}
