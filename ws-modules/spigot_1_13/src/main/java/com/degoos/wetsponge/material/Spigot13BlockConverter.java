package com.degoos.wetsponge.material;

import com.degoos.wetsponge.enums.EnumDyeColor;
import com.degoos.wetsponge.enums.EnumInstrument;
import com.degoos.wetsponge.enums.block.*;
import com.degoos.wetsponge.material.block.*;
import com.degoos.wetsponge.material.block.type.*;

import java.util.Map;
import java.util.Set;

public class Spigot13BlockConverter {

	@SuppressWarnings("unchecked")
	public static WSBlockType createWSBlockType(int numericalId, String oldStringId, String newStringId, int maxStackSize,
												Class<? extends WSBlockType> materialClass, Object[] extra) {

		if (materialClass.equals(WSBlockType.class))
			return new Spigot13BlockType(numericalId, oldStringId, newStringId, maxStackSize);
		if (materialClass.equals(WSBlockTypeAgeable.class))
			return new Spigot13BlockTypeAgeable(numericalId, oldStringId, newStringId, maxStackSize, (int) extra[0], (int) extra[1]);
		if (materialClass.equals(WSBlockTypeAnaloguePowerable.class))
			return new Spigot13BlockTypeAnaloguePowerable(numericalId, oldStringId, newStringId, maxStackSize, (int) extra[0], (int) extra[1]);
		if (materialClass.equals(WSBlockTypeAttachable.class))
			return new Spigot13BlockTypeAttachable(numericalId, oldStringId, newStringId, maxStackSize, (boolean) extra[0]);
		if (materialClass.equals(WSBlockTypeBisected.class))
			return new Spigot13BlockTypeBisected(numericalId, oldStringId, newStringId, maxStackSize, (EnumBlockTypeBisectedHalf) extra[0]);
		if (materialClass.equals(WSBlockTypeDirectional.class))
			return new Spigot13BlockTypeDirectional(numericalId, oldStringId, newStringId, maxStackSize, (EnumBlockFace) extra[0], (Set<EnumBlockFace>) extra[1]);
		if (materialClass.equals(WSBlockTypeDyeColored.class))
			return new Spigot13BlockTypeDyeColored(numericalId, oldStringId, newStringId, maxStackSize, (EnumDyeColor) extra[0]);
		if (materialClass.equals(WSBlockTypeLevelled.class))
			return new Spigot13BlockTypeLevelled(numericalId, oldStringId, newStringId, maxStackSize, (int) extra[0], (int) extra[1]);
		if (materialClass.equals(WSBlockTypeLightable.class))
			return new Spigot13BlockTypeLightable(numericalId, oldStringId, newStringId, maxStackSize, (boolean) extra[0]);
		if (materialClass.equals(WSBlockTypeMultipleFacing.class))
			return new Spigot13BlockTypeMultipleFacing(numericalId, oldStringId, newStringId, maxStackSize, (Set<EnumBlockFace>) extra[0], (Set<EnumBlockFace>) extra[1]);
		if (materialClass.equals(WSBlockTypeOpenable.class))
			return new Spigot13BlockTypeOpenable(numericalId, oldStringId, newStringId, maxStackSize, (boolean) extra[0]);
		if (materialClass.equals(WSBlockTypeOrientable.class))
			return new Spigot13BlockTypeOrientable(numericalId, oldStringId, newStringId, maxStackSize, (EnumAxis) extra[0], (Set<EnumAxis>) extra[1]);
		if (materialClass.equals(WSBlockTypePowerable.class))
			return new Spigot13BlockTypePowerable(numericalId, oldStringId, newStringId, maxStackSize, (boolean) extra[0]);
		if (materialClass.equals(WSBlockTypeRail.class))
			return new Spigot13BlockTypeRail(numericalId, oldStringId, newStringId, maxStackSize, (EnumBlockTypeRailShape) extra[0], (Set<EnumBlockTypeRailShape>) extra[1]);
		if (materialClass.equals(WSBlockTypeRotatable.class))
			return new Spigot13BlockTypeRotatable(numericalId, oldStringId, newStringId, maxStackSize, (EnumBlockFace) extra[0]);
		if (materialClass.equals(WSBlockTypeSnowable.class))
			return new Spigot13BlockTypeSnowable(numericalId, oldStringId, newStringId, maxStackSize, (boolean) extra[0]);
		if (materialClass.equals(WSBlockTypeWaterlogged.class))
			return new Spigot13BlockTypeWaterlogged(numericalId, oldStringId, newStringId, maxStackSize, (boolean) extra[0]);

		if (materialClass.equals(WSBlockTypeAnvil.class))
			return new Spigot13BlockTypeAnvil((EnumBlockFace) extra[0], (Set<EnumBlockFace>) extra[1], (EnumBlockTypeAnvilDamage) extra[2]);
		if (materialClass.equals(WSBlockTypeBed.class))
			return new Spigot13BlockTypeBed((EnumBlockFace) extra[0], (Set<EnumBlockFace>) extra[1], (EnumBlockTypeBedPart) extra[2], (boolean) extra[3], (EnumDyeColor) extra[4]);
		if (materialClass.equals(WSBlockTypeBrewingStand.class))
			return new Spigot13BlockTypeBrewingStand((Set<Integer>) extra[0], (int) extra[1]);
		if (materialClass.equals(WSBlockTypeBubbleColumn.class))
			return new Spigot13BlockTypeBubbleColumn((boolean) extra[0]);
		if (materialClass.equals(WSBlockTypeCake.class))
			return new Spigot13BlockTypeCake((int) extra[0], (int) extra[1]);
		if (materialClass.equals(WSBlockTypeChest.class))
			return new Spigot13BlockTypeChest(numericalId, oldStringId, newStringId, maxStackSize, (EnumBlockFace) extra[0], (Set<EnumBlockFace>) extra[1], (EnumBlockTypeChestType) extra[2], (boolean) extra[3]);
		if (materialClass.equals(WSBlockTypeCobblestoneWall.class))
			return new Spigot13BlockTypeCobblestoneWall((Set<EnumBlockFace>) extra[0], (Set<EnumBlockFace>) extra[1], (boolean) extra[2], (boolean) extra[3]);
		if (materialClass.equals(WSBlockTypeCocoa.class))
			return new Spigot13BlockTypeCocoa((EnumBlockFace) extra[0], (Set<EnumBlockFace>) extra[1], (int) extra[2], (int) extra[3]);
		if (materialClass.equals(WSBlockTypeCommandBlock.class))
			return new Spigot13BlockTypeCommandBlock(numericalId, oldStringId, newStringId, maxStackSize, (EnumBlockFace) extra[0], (Set<EnumBlockFace>) extra[1], (boolean) extra[2]);
		if (materialClass.equals(WSBlockTypeComparator.class))
			return new Spigot13BlockTypeComparator((EnumBlockFace) extra[0], (Set<EnumBlockFace>) extra[1], (EnumBlockTypeComparatorMode) extra[2], (boolean) extra[3]);
		if (materialClass.equals(WSBlockTypeCoralWallFan.class))
			return new Spigot13BlockTypeCoralWallFan(numericalId, oldStringId, newStringId, maxStackSize, (EnumBlockFace) extra[0], (Set<EnumBlockFace>) extra[1], (boolean) extra[2]);
		if (materialClass.equals(WSBlockTypeDaylightDetector.class))
			return new Spigot13BlockTypeDaylightDetector((int) extra[0], (int) extra[1], (boolean) extra[2]);
		if (materialClass.equals(WSBlockTypeDirt.class))
			return new Spigot13BlockTypeDirt((EnumBlockTypeDirtType) extra[0]);
		if (materialClass.equals(WSBlockTypeDispenser.class))
			return new Spigot13BlockTypeDispenser(numericalId, oldStringId, newStringId, maxStackSize, (EnumBlockFace) extra[0], (Set<EnumBlockFace>) extra[1], (boolean) extra[2]);
		if (materialClass.equals(WSBlockTypeDoor.class))
			return new Spigot13BlockTypeDoor(numericalId, oldStringId, newStringId, maxStackSize, (EnumBlockFace) extra[0], (Set<EnumBlockFace>) extra[1], (EnumBlockTypeDoorHinge) extra[2], (EnumBlockTypeBisectedHalf) extra[3], (boolean) extra[4], (boolean) extra[5]);
		if (materialClass.equals(WSBlockTypeDoublePlant.class))
			return new Spigot13BlockTypeDoublePlant((EnumBlockTypeBisectedHalf) extra[0], (EnumBlockTypeDoublePlantType) extra[1]);
		if (materialClass.equals(WSBlockTypeEnderchest.class))
			return new Spigot13BlockTypeEnderchest((EnumBlockFace) extra[0], (Set<EnumBlockFace>) extra[1], (boolean) extra[2]);
		if (materialClass.equals(WSBlockTypeEndPortalFrame.class))
			return new Spigot13BlockTypeEndPortalFrame((EnumBlockFace) extra[0], (Set<EnumBlockFace>) extra[1], (boolean) extra[2]);
		if (materialClass.equals(WSBlockTypeFarmland.class))
			return new Spigot13BlockTypeFarmland((int) extra[0], (int) extra[1]);
		if (materialClass.equals(WSBlockTypeFence.class))
			return new Spigot13BlockTypeFence(numericalId, oldStringId, newStringId, maxStackSize, (Set<EnumBlockFace>) extra[0], (Set<EnumBlockFace>) extra[1], (boolean) extra[2]);
		if (materialClass.equals(WSBlockTypeFire.class))
			return new Spigot13BlockTypeFire((Set<EnumBlockFace>) extra[0], (Set<EnumBlockFace>) extra[1], (int) extra[2], (int) extra[3]);
		if (materialClass.equals(WSBlockTypeFlower.class))
			return new Spigot13BlockTypeFlower((EnumBlockTypeFlowerType) extra[0]);
		if (materialClass.equals(WSBlockTypeFlowerPot.class))
			return new Spigot13BlockTypeFlowerPot((EnumBlockTypePottedPlant) extra[0]);
		if (materialClass.equals(WSBlockTypeFurnace.class))
			return new Spigot13BlockTypeFurnace((EnumBlockFace) extra[0], (Set<EnumBlockFace>) extra[1], (boolean) extra[2]);
		if (materialClass.equals(WSBlockTypeGate.class))
			return new Spigot13BlockTypeGate(numericalId, oldStringId, newStringId, maxStackSize, (EnumBlockFace) extra[0], (Set<EnumBlockFace>) extra[1], (boolean) extra[2], (boolean) extra[3], (boolean) extra[4]);
		if (materialClass.equals(WSBlockTypeGlassPane.class))
			return new Spigot13BlockTypeGlassPane(numericalId, oldStringId, newStringId, maxStackSize, (Set<EnumBlockFace>) extra[0], (Set<EnumBlockFace>) extra[1], (boolean) extra[2]);
		if (materialClass.equals(WSBlockTypeHopper.class))
			return new Spigot13BlockTypeHopper((EnumBlockFace) extra[0], (Set<EnumBlockFace>) extra[1], (boolean) extra[2]);
		if (materialClass.equals(WSBlockTypeInfestedStone.class))
			return new Spigot13BlockTypeInfestedStone((EnumBlockTypeDisguiseType) extra[0]);
		if (materialClass.equals(WSBlockTypeJukebox.class))
			return new Spigot13BlockTypeJukebox((boolean) extra[0]);
		if (materialClass.equals(WSBlockTypeLadder.class))
			return new Spigot13BlockTypeLadder((EnumBlockFace) extra[0], (Set<EnumBlockFace>) extra[1], (boolean) extra[2]);
		if (materialClass.equals(WSBlockTypeLava.class))
			return new Spigot13BlockTypeLava((int) extra[0], (int) extra[1]);
		if (materialClass.equals(WSBlockTypeLeaves.class))
			return new Spigot13BlockTypeLeaves((boolean) extra[0], (int) extra[1], (EnumWoodType) extra[2]);
		if (materialClass.equals(WSBlockTypeLog.class))
			return new Spigot13BlockTypeLog((EnumAxis) extra[0], (Set<EnumAxis>) extra[1], (EnumWoodType) extra[2], (boolean) extra[3], (boolean) extra[4]);
		if (materialClass.equals(WSBlockTypeNoteBlock.class))
			return new Spigot13BlockTypeNoteBlock((boolean) extra[0], (EnumInstrument) extra[1], (int) extra[2]);
		if (materialClass.equals(WSBlockTypeObserver.class))
			return new Spigot13BlockTypeObserver((EnumBlockFace) extra[0], (Set<EnumBlockFace>) extra[1], (boolean) extra[2]);
		if (materialClass.equals(WSBlockTypePiston.class))
			return new Spigot13BlockTypePiston(numericalId, oldStringId, newStringId, maxStackSize, (EnumBlockFace) extra[0], (Set<EnumBlockFace>) extra[1], (boolean) extra[2]);
		if (materialClass.equals(WSBlockTypePistonHead.class))
			return new Spigot13BlockTypePistonHead((EnumBlockFace) extra[0], (Set<EnumBlockFace>) extra[1], (EnumBlockTypePistonType) extra[2], (boolean) extra[3]);
		if (materialClass.equals(WSBlockTypePrismarine.class))
			return new Spigot13BlockTypePrismarine((EnumBlockTypePrismarineType) extra[0]);
		if (materialClass.equals(WSBlockTypeQuartz.class))
			return new Spigot13BlockTypeQuartz((EnumAxis) extra[0], (Set<EnumAxis>) extra[1], (EnumBlockTypeQuartzType) extra[2]);
		if (materialClass.equals(WSBlockTypeRedstoneLamp.class))
			return new Spigot13BlockTypeRedstoneLamp((boolean) extra[0]);
		if (materialClass.equals(WSBlockTypeRedstoneOre.class))
			return new Spigot13BlockTypeRedstoneOre((boolean) extra[0]);
		if (materialClass.equals(WSBlockTypeRedstoneRail.class))
			return new Spigot13BlockTypeRedstoneRail(numericalId, oldStringId, newStringId, maxStackSize, (EnumBlockTypeRailShape) extra[0], (Set<EnumBlockTypeRailShape>) extra[1], (boolean) extra[2]);
		if (materialClass.equals(WSBlockTypeRedstoneTorch.class))
			return new Spigot13BlockTypeRedstoneTorch((EnumBlockFace) extra[0], (Set<EnumBlockFace>) extra[1], (boolean) extra[2]);
		if (materialClass.equals(WSBlockTypeRedstoneWire.class))
			return new Spigot13BlockTypeRedstoneWire((int) extra[0], (int) extra[1], (Map<EnumBlockFace, EnumBlockTypeRedstoneWireConnection>) extra[2], (Set<EnumBlockFace>) extra[3]);
		if (materialClass.equals(WSBlockTypeRepeater.class))
			return new Spigot13BlockTypeRepeater((EnumBlockFace) extra[0], (Set<EnumBlockFace>) extra[1], (int) extra[2], (int) extra[3], (int) extra[4], (boolean) extra[5], (boolean) extra[6]);
		if (materialClass.equals(WSBlockTypeSand.class))
			return new Spigot13BlockTypeSand((EnumBlockTypeSandType) extra[0]);
		if (materialClass.equals(WSBlockTypeSandstone.class))
			return new Spigot13BlockTypeSandstone((EnumBlockTypeSandType) extra[0], (EnumBlockTypeSandstoneType) extra[1]);
		if (materialClass.equals(WSBlockTypeSapling.class))
			return new Spigot13BlockTypeSapling((EnumWoodType) extra[0], (int) extra[1], (int) extra[2]);
		if (materialClass.equals(WSBlockTypeSeaPickle.class))
			return new Spigot13BlockTypeSeaPickle((boolean) extra[0], (int) extra[1], (int) extra[2], (int) extra[3]);
		if (materialClass.equals(WSBlockTypeSign.class))
			return new Spigot13BlockTypeSign((EnumBlockFace) extra[0], (boolean) extra[1]);
		if (materialClass.equals(WSBlockTypeSkull.class))
			return new Spigot13BlockTypeSkull((EnumBlockFace) extra[0], (Set<EnumBlockFace>) extra[1], (EnumBlockFace) extra[2], (EnumBlockTypeSkullType) extra[3]);
		if (materialClass.equals(WSBlockTypeSlab.class))
			return new Spigot13BlockTypeSlab((boolean) extra[0], (EnumBlockTypeSlabType) extra[1], (EnumBlockTypeSlabPosition) extra[2]);
		if (materialClass.equals(WSBlockTypeSnow.class))
			return new Spigot13BlockTypeSnow((int) extra[0], (int) extra[1], (int) extra[2]);
		if (materialClass.equals(WSBlockTypeSponge.class))
			return new Spigot13BlockTypeSponge((boolean) extra[0]);
		if (materialClass.equals(WSBlockTypeStainedGlassPane.class))
			return new Spigot13BlockTypeStainedGlassPane((Set<EnumBlockFace>) extra[0], (Set<EnumBlockFace>) extra[1], (boolean) extra[2], (EnumDyeColor) extra[3]);
		if (materialClass.equals(WSBlockTypeStairs.class))
			return new Spigot13BlockTypeStairs(numericalId, oldStringId, newStringId, maxStackSize, (EnumBlockFace) extra[0], (Set<EnumBlockFace>) extra[1], (EnumBlockTypeStairShape) extra[2], (EnumBlockTypeBisectedHalf) extra[3], (boolean) extra[4]);
		if (materialClass.equals(WSBlockTypeStone.class))
			return new Spigot13BlockTypeStone((EnumBlockTypeStoneType) extra[0]);
		if (materialClass.equals(WSBlockTypeStoneBrick.class))
			return new Spigot13BlockTypeStoneBrick((EnumBlockTypeStoneBrickType) extra[0]);
		if (materialClass.equals(WSBlockTypeStructureBlock.class))
			return new Spigot13BlockTypeStructureBlock((EnumBlockTypeStructureBlockMode) extra[0]);
		if (materialClass.equals(WSBlockTypeSwitch.class))
			return new Spigot13BlockTypeSwitch(numericalId, oldStringId, newStringId, maxStackSize, (EnumBlockFace) extra[0], (Set<EnumBlockFace>) extra[1], (EnumBlockTypeSwitchFace) extra[2], (boolean) extra[3]);
		if (materialClass.equals(WSBlockTypeTallGrass.class))
			return new Spigot13BlockTypeTallGrassType((EnumBlockTypeTallGrassType) extra[0]);
		if (materialClass.equals(WSBlockTypeTechnicalPiston.class))
			return new Spigot13BlockTypeTechnicalPiston(numericalId, oldStringId, newStringId, maxStackSize, (EnumBlockFace) extra[0], (Set<EnumBlockFace>) extra[1], (EnumBlockTypePistonType) extra[2]);
		if (materialClass.equals(WSBlockTypeTorch.class))
			return new Spigot13BlockTypeTorch(numericalId, oldStringId, newStringId, maxStackSize, (String) extra[0], (EnumBlockFace) extra[1], (Set<EnumBlockFace>) extra[2]);
		if (materialClass.equals(WSBlockTypeTrapDoor.class))
			return new Spigot13BlockTypeTrapDoor(numericalId, oldStringId, newStringId, maxStackSize, (EnumBlockFace) extra[0], (Set<EnumBlockFace>) extra[1], (EnumBlockTypeBisectedHalf) extra[2], (boolean) extra[3], (boolean) extra[4], (boolean) extra[5]);
		if (materialClass.equals(WSBlockTypeTripwire.class))
			return new Spigot13BlockTypeTripwire((Set<EnumBlockFace>) extra[0], (Set<EnumBlockFace>) extra[1], (boolean) extra[2], (boolean) extra[3], (boolean) extra[4]);
		if (materialClass.equals(WSBlockTypeTripwireHook.class))
			return new Spigot13BlockTypeTripwireHook((EnumBlockFace) extra[0], (Set<EnumBlockFace>) extra[1], (boolean) extra[2], (boolean) extra[3]);
		if (materialClass.equals(WSBlockTypeTurtleEgg.class))
			return new Spigot13BlockTypeTurtleEgg((int) extra[0], (int) extra[1], (int) extra[2], (int) extra[3], (int) extra[4]);
		if (materialClass.equals(WSBlockTypeWallSign.class))
			return new Spigot13BlockTypeWallSign((EnumBlockFace) extra[0], (Set<EnumBlockFace>) extra[1], (boolean) extra[2]);
		if (materialClass.equals(WSBlockTypeWater.class))
			return new Spigot13BlockTypeWater((int) extra[0], (int) extra[1]);
		if (materialClass.equals(WSBlockTypeWoodPlanks.class))
			return new Spigot13BlockTypeWoodPlanks((EnumWoodType) extra[0]);

		return new Spigot13BlockType(numericalId, oldStringId, newStringId, maxStackSize);
	}

}
