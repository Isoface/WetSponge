package com.degoos.wetsponge.material.block;

import com.degoos.wetsponge.material.Spigot13Material;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;
import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.Spigot13MaterialUtils;
import org.bukkit.Material;
import org.bukkit.block.BlockState;
import org.bukkit.block.data.BlockData;
import org.bukkit.inventory.meta.BlockStateMeta;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Objects;

public class Spigot13BlockType implements Spigot13Material, WSBlockType {


	private int numericalId;
	private String oldStringId, newStringId;
	private int maxStackSize;

	public Spigot13BlockType(int numericalId, String oldStringId, String newStringId, int maxStackSize) {
		this.numericalId = numericalId < 0 ? -1 : numericalId;
		this.oldStringId = oldStringId == null || oldStringId.equals("") ? null : oldStringId;
		this.newStringId = newStringId;
		this.maxStackSize = Math.max(1, maxStackSize);
	}

	@Override
	public Spigot13BlockType clone() {
		return new Spigot13BlockType(numericalId, oldStringId, newStringId, maxStackSize);
	}

	@Override
	public int getNumericalId() {
		return numericalId;
	}

	@Override
	public String getStringId() {
		try {
			return getNewStringId();
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was getting the string id of a block type!");
			return newStringId;
		}
	}

	@Override
	public String getNewStringId() {
		return newStringId;
	}

	@Override
	public String getOldStringId() {
		return oldStringId;
	}

	@Override
	public int getMaxStackSize() {
		return maxStackSize;
	}

	public BlockData toBlockData() {
		Material material = Spigot13MaterialUtils.getByKey(getNewStringId()).orElse(null);
		if (material == null) return null;
		return material.createBlockData();
	}

	public Spigot13BlockType readBlockData(BlockData blockData) {
		return this;
	}

	@Override
	public void writeItemMeta(ItemMeta itemMeta) {
		if (itemMeta instanceof BlockStateMeta) {
			BlockState state = ((BlockStateMeta) itemMeta).getBlockState();
			if (state == null) return;
			state.setBlockData(toBlockData());
			((BlockStateMeta) itemMeta).setBlockState(state);
		}
	}

	@Override
	public void readItemMeta(ItemMeta itemMeta) {
		if (itemMeta instanceof BlockStateMeta) {
			BlockState state = ((BlockStateMeta) itemMeta).getBlockState();
			if (state == null) return;
			BlockData data = state.getBlockData();
			if (data != null) readBlockData(data);
		}
	}

	@Override
	public void writeNBTTag(WSNBTTagCompound compound) {

	}

	@Override
	public void readNBTTag(WSNBTTagCompound compound) {

	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Spigot13BlockType that = (Spigot13BlockType) o;
		return numericalId == that.numericalId &&
				maxStackSize == that.maxStackSize &&
				Objects.equals(oldStringId, that.oldStringId) &&
				Objects.equals(newStringId, that.newStringId);
	}

	@Override
	public int hashCode() {

		return Objects.hash(numericalId, oldStringId, newStringId, maxStackSize);
	}
}
