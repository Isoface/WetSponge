package com.degoos.wetsponge.material.block;

import com.degoos.wetsponge.util.Spigot13MaterialUtils;
import org.bukkit.Material;
import org.bukkit.block.data.AnaloguePowerable;
import org.bukkit.block.data.BlockData;

import java.util.Objects;

public class Spigot13BlockTypeAnaloguePowerable extends Spigot13BlockType implements WSBlockTypeAnaloguePowerable {

	private int power, maximumPower;

	public Spigot13BlockTypeAnaloguePowerable(int numericalId, String oldStringId, String newStringId, int maxStackSize, int power, int maximumPower) {
		super(numericalId, oldStringId, newStringId, maxStackSize);
		this.power = power;
		this.maximumPower = maximumPower;
	}

	@Override
	public int getPower() {
		return power;
	}

	@Override
	public void setPower(int power) {
		this.power = Math.min(maximumPower, Math.max(0, power));
	}

	@Override
	public int gerMaximumPower() {
		return maximumPower;
	}

	@Override
	public Spigot13BlockTypeAnaloguePowerable clone() {
		return new Spigot13BlockTypeAnaloguePowerable(getNumericalId(), getOldStringId(), getNewStringId(), getMaxStackSize(), power, maximumPower);
	}


	@Override
	public BlockData toBlockData() {
		Material material = Spigot13MaterialUtils.getByKey(getNewStringId()).orElse(null);
		if (material == null) return null;
		BlockData data = material.createBlockData();
		if (data instanceof AnaloguePowerable) {
			((AnaloguePowerable) data).setPower(power);
		}
		return data;
	}

	@Override
	public Spigot13BlockTypeAnaloguePowerable readBlockData(BlockData blockData) {
		if (blockData instanceof AnaloguePowerable) {
			power = ((AnaloguePowerable) blockData).getPower();
			maximumPower = ((AnaloguePowerable) blockData).getMaximumPower();
		}
		return this;
	}


	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Spigot13BlockTypeAnaloguePowerable that = (Spigot13BlockTypeAnaloguePowerable) o;
		return power == that.power &&
				maximumPower == that.maximumPower;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), power, maximumPower);
	}
}
