package com.degoos.wetsponge.material.block;

import com.degoos.wetsponge.util.Spigot13MaterialUtils;
import org.bukkit.Material;
import org.bukkit.block.data.Attachable;
import org.bukkit.block.data.BlockData;

import java.util.Objects;

public class Spigot13BlockTypeAttachable extends Spigot13BlockType implements WSBlockTypeAttachable {

	private boolean attached;

	public Spigot13BlockTypeAttachable(int numericalId, String oldStringId, String newStringId, int maxStackSize, boolean attached) {
		super(numericalId, oldStringId, newStringId, maxStackSize);
		this.attached = attached;
	}

	@Override
	public boolean isAttached() {
		return attached;
	}

	@Override
	public void setAttached(boolean attached) {
		this.attached = attached;
	}

	@Override
	public Spigot13BlockTypeAttachable clone() {
		return new Spigot13BlockTypeAttachable(getNumericalId(), getOldStringId(), getNewStringId(), getMaxStackSize(), attached);
	}

	@Override
	public BlockData toBlockData() {
		Material material = Spigot13MaterialUtils.getByKey(getNewStringId()).orElse(null);
		if (material == null) return null;
		BlockData data = material.createBlockData();
		if (data instanceof Attachable) {
			((Attachable) data).setAttached(attached);
		}
		return data;
	}

	@Override
	public Spigot13BlockTypeAttachable readBlockData(BlockData blockData) {
		if (blockData instanceof Attachable) {
			attached = ((Attachable) blockData).isAttached();
		}
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Spigot13BlockTypeAttachable that = (Spigot13BlockTypeAttachable) o;
		return attached == that.attached;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), attached);
	}
}
