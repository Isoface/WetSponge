package com.degoos.wetsponge.material.block;

import com.degoos.wetsponge.enums.block.EnumBlockTypeBisectedHalf;
import com.degoos.wetsponge.util.Spigot13MaterialUtils;
import com.degoos.wetsponge.util.Validate;
import org.bukkit.Material;
import org.bukkit.block.data.Bisected;
import org.bukkit.block.data.BlockData;

import java.util.Objects;

public class Spigot13BlockTypeBisected extends Spigot13BlockType implements WSBlockTypeBisected {

	private EnumBlockTypeBisectedHalf half;

	public Spigot13BlockTypeBisected(int numericalId, String oldStringId, String newStringId, int maxStackSize, EnumBlockTypeBisectedHalf half) {
		super(numericalId, oldStringId, newStringId, maxStackSize);
		Validate.notNull(half, "Half cannot be null!");
		this.half = half;
	}

	@Override
	public EnumBlockTypeBisectedHalf getHalf() {
		return half;
	}

	@Override
	public void setHalf(EnumBlockTypeBisectedHalf half) {
		Validate.notNull(half, "Half cannot be null!");
		this.half = half;
	}

	@Override
	public Spigot13BlockTypeBisected clone() {
		return new Spigot13BlockTypeBisected(getNumericalId(), getOldStringId(), getNewStringId(), getMaxStackSize(), half);
	}

	@Override
	public BlockData toBlockData() {
		Material material = Spigot13MaterialUtils.getByKey(getNewStringId()).orElse(null);
		if (material == null) return null;
		BlockData data = material.createBlockData();
		if (data instanceof Bisected) {
			((Bisected) data).setHalf(Bisected.Half.valueOf(half.name()));
		}
		return data;
	}

	@Override
	public Spigot13BlockTypeBisected readBlockData(BlockData blockData) {
		if (blockData instanceof Bisected) {
			half = EnumBlockTypeBisectedHalf.valueOf(((Bisected) blockData).getHalf().name());
		}
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Spigot13BlockTypeBisected that = (Spigot13BlockTypeBisected) o;
		return half == that.half;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), half);
	}
}
