package com.degoos.wetsponge.material.block;

import com.degoos.wetsponge.enums.EnumDyeColor;
import com.degoos.wetsponge.material.block.type.WSBlockTypeDyeColored;
import com.degoos.wetsponge.util.Validate;

import java.util.Objects;

public class Spigot13BlockTypeDyeColored extends Spigot13BlockType implements WSBlockTypeDyeColored {

	private EnumDyeColor dyeColor;

	public Spigot13BlockTypeDyeColored(int numericalId, String oldStringId, String newStringId, int maxStackSize, EnumDyeColor dyeColor) {
		super(numericalId, oldStringId, newStringId.startsWith("minecraft:") ? newStringId.substring(10) : newStringId, maxStackSize);
		Validate.notNull(dyeColor, "Dye color cannot be null!");
		this.dyeColor = dyeColor;
	}

	@Override
	public String getNewStringId() {
		return "minecraft:" + dyeColor.getMinecraftName().toLowerCase() + "_" + super.getNewStringId();
	}

	@Override
	public EnumDyeColor getDyeColor() {
		return dyeColor;
	}

	@Override
	public void setDyeColor(EnumDyeColor dyeColor) {
		Validate.notNull(dyeColor, "Dye color cannot be null!");
		this.dyeColor = dyeColor;
	}

	@Override
	public Spigot13BlockTypeDyeColored clone() {
		return new Spigot13BlockTypeDyeColored(getNumericalId(), getOldStringId(), super.getNewStringId(), getMaxStackSize(), dyeColor);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Spigot13BlockTypeDyeColored that = (Spigot13BlockTypeDyeColored) o;
		return dyeColor == that.dyeColor;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), dyeColor);
	}
}
