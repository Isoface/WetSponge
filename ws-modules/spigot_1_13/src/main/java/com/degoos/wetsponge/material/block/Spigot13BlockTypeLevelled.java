package com.degoos.wetsponge.material.block;

import com.degoos.wetsponge.util.Spigot13MaterialUtils;
import org.bukkit.Material;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.Levelled;

import java.util.Objects;

public class Spigot13BlockTypeLevelled extends Spigot13BlockType implements WSBlockTypeLevelled {

	private int level, maximumLevel;

	public Spigot13BlockTypeLevelled(int numericalId, String oldStringId, String newStringId, int maxStackSize, int level, int maximumLevel) {
		super(numericalId, oldStringId, newStringId, maxStackSize);
		this.level = level;
		this.maximumLevel = maximumLevel;
	}

	@Override
	public int getLevel() {
		return level;
	}

	@Override
	public void setLevel(int level) {
		this.level = Math.min(maximumLevel, Math.max(0, level));
	}

	@Override
	public int getMaximumLevel() {
		return maximumLevel;
	}

	@Override
	public Spigot13BlockTypeLevelled clone() {
		return new Spigot13BlockTypeLevelled(getNumericalId(), getOldStringId(), getNewStringId(), getMaxStackSize(), level, maximumLevel);
	}

	@Override
	public BlockData toBlockData() {
		Material material = Spigot13MaterialUtils.getByKey(getNewStringId()).orElse(null);
		if (material == null) return null;
		BlockData data = material.createBlockData();
		if (data instanceof Levelled) {
			((Levelled) data).setLevel(level);
		}
		return data;
	}

	@Override
	public Spigot13BlockTypeLevelled readBlockData(BlockData blockData) {
		if (blockData instanceof Levelled) {
			level = ((Levelled) blockData).getLevel();
			maximumLevel = ((Levelled) blockData).getMaximumLevel();
		}
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Spigot13BlockTypeLevelled that = (Spigot13BlockTypeLevelled) o;
		return level == that.level &&
				maximumLevel == that.maximumLevel;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), level, maximumLevel);
	}
}
