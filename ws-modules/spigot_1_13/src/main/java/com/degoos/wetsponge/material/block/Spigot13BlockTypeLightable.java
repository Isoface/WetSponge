package com.degoos.wetsponge.material.block;

import com.degoos.wetsponge.util.Spigot13MaterialUtils;
import org.bukkit.Material;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.Lightable;

import java.util.Objects;

public class Spigot13BlockTypeLightable extends Spigot13BlockType implements WSBlockTypeLightable {

	private boolean lit;

	public Spigot13BlockTypeLightable(int numericalId, String oldStringId, String newStringId, int maxStackSize, boolean lit) {
		super(numericalId, oldStringId, newStringId, maxStackSize);
		this.lit = lit;
	}

	@Override
	public boolean isLit() {
		return lit;
	}

	@Override
	public void setLit(boolean lit) {
		this.lit = lit;
	}

	@Override
	public Spigot13BlockTypeLightable clone() {
		return new Spigot13BlockTypeLightable(getNumericalId(), getOldStringId(), getNewStringId(), getMaxStackSize(), lit);
	}

	@Override
	public BlockData toBlockData() {
		Material material = Spigot13MaterialUtils.getByKey(getNewStringId()).orElse(null);
		if (material == null) return null;
		BlockData data = material.createBlockData();
		if (data instanceof Lightable) {
			((Lightable) data).setLit(lit);
		}
		return data;
	}

	@Override
	public Spigot13BlockTypeLightable readBlockData(BlockData blockData) {
		if (blockData instanceof Lightable) {
			lit = ((Lightable) blockData).isLit();
		}
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Spigot13BlockTypeLightable that = (Spigot13BlockTypeLightable) o;
		return lit == that.lit;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), lit);
	}
}
