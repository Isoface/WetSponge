package com.degoos.wetsponge.material.block;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.util.Spigot13MaterialUtils;
import org.bukkit.Material;
import org.bukkit.block.BlockFace;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.MultipleFacing;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

public class Spigot13BlockTypeMultipleFacing extends Spigot13BlockType implements WSBlockTypeMultipleFacing {

	private Set<EnumBlockFace> faces, allowedFaces;

	public Spigot13BlockTypeMultipleFacing(int numericalId, String oldStringId, String newStringId, int maxStackSize, Set<EnumBlockFace> faces, Set<EnumBlockFace> allowedFaces) {
		super(numericalId, oldStringId, newStringId, maxStackSize);
		this.faces = faces;
		this.allowedFaces = allowedFaces;
	}

	@Override
	public boolean hasFace(EnumBlockFace face) {
		return faces.contains(face);
	}

	@Override
	public void setFace(EnumBlockFace face, boolean enabled) {
		if (enabled) faces.add(face);
		else faces.remove(face);
	}

	@Override
	public Set<EnumBlockFace> getFaces() {
		return new HashSet<>(faces);
	}

	@Override
	public Set<EnumBlockFace> getAllowedFaces() {
		return new HashSet<>(allowedFaces);
	}

	@Override
	public Spigot13BlockTypeMultipleFacing clone() {
		return new Spigot13BlockTypeMultipleFacing(getNumericalId(), getOldStringId(), getNewStringId(), getMaxStackSize(), new HashSet<>(faces), new HashSet<>(allowedFaces));
	}

	@Override
	public BlockData toBlockData() {
		Material material = Spigot13MaterialUtils.getByKey(getNewStringId()).orElse(null);
		if (material == null) return null;
		BlockData data = material.createBlockData();
		if (data instanceof MultipleFacing) {
			((MultipleFacing) data).getFaces().forEach(target -> ((MultipleFacing) data).setFace(target, false));
			faces.forEach(target -> ((MultipleFacing) data).setFace(BlockFace.valueOf(target.name()), true));
		}
		return data;
	}

	@Override
	public Spigot13BlockTypeMultipleFacing readBlockData(BlockData blockData) {
		if (blockData instanceof MultipleFacing) {
			faces = ((MultipleFacing) blockData).getFaces().stream().map(target -> EnumBlockFace.valueOf(target.name())).collect(Collectors.toSet());
			allowedFaces = ((MultipleFacing) blockData).getAllowedFaces().stream().map(target -> EnumBlockFace.valueOf(target.name())).collect(Collectors.toSet());
		}
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Spigot13BlockTypeMultipleFacing that = (Spigot13BlockTypeMultipleFacing) o;
		return Objects.equals(faces, that.faces);
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), faces);
	}
}
