package com.degoos.wetsponge.material.block;

import com.degoos.wetsponge.util.Spigot13MaterialUtils;
import org.bukkit.Material;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.Openable;

import java.util.Objects;

public class Spigot13BlockTypeOpenable extends Spigot13BlockType implements WSBlockTypeOpenable {

	private boolean open;

	public Spigot13BlockTypeOpenable(int numericalId, String oldStringId, String newStringId, int maxStackSize, boolean open) {
		super(numericalId, oldStringId, newStringId, maxStackSize);
		this.open = open;
	}

	@Override
	public boolean isOpen() {
		return open;
	}

	@Override
	public void setOpen(boolean open) {
		this.open = open;
	}

	@Override
	public Spigot13BlockTypeOpenable clone() {
		return new Spigot13BlockTypeOpenable(getNumericalId(), getOldStringId(), getNewStringId(), getMaxStackSize(), open);
	}

	@Override
	public BlockData toBlockData() {
		Material material = Spigot13MaterialUtils.getByKey(getNewStringId()).orElse(null);
		if (material == null) return null;
		BlockData data = material.createBlockData();
		if (data instanceof Openable) {
			((Openable) data).setOpen(open);
		}
		return data;
	}

	@Override
	public Spigot13BlockTypeOpenable readBlockData(BlockData blockData) {
		if (blockData instanceof Openable) {
			open = ((Openable) blockData).isOpen();
		}
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Spigot13BlockTypeOpenable that = (Spigot13BlockTypeOpenable) o;
		return open == that.open;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), open);
	}
}
