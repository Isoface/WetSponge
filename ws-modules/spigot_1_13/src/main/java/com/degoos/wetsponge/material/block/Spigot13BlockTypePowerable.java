package com.degoos.wetsponge.material.block;

import com.degoos.wetsponge.util.Spigot13MaterialUtils;
import org.bukkit.Material;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.Powerable;

import java.util.Objects;

public class Spigot13BlockTypePowerable extends Spigot13BlockType implements WSBlockTypePowerable {

	private boolean powered;

	public Spigot13BlockTypePowerable(int numericalId, String oldStringId, String newStringId, int maxStackSize, boolean powered) {
		super(numericalId, oldStringId, newStringId, maxStackSize);
		this.powered = powered;
	}

	@Override
	public boolean isPowered() {
		return powered;
	}

	@Override
	public void setPowered(boolean powered) {
		this.powered = powered;
	}

	@Override
	public Spigot13BlockTypePowerable clone() {
		return new Spigot13BlockTypePowerable(getNumericalId(), getOldStringId(), getNewStringId(), getMaxStackSize(), powered);
	}

	@Override
	public BlockData toBlockData() {
		Material material = Spigot13MaterialUtils.getByKey(getNewStringId()).orElse(null);
		if (material == null) return null;
		BlockData data = material.createBlockData();
		if (data instanceof Powerable) {
			((Powerable) data).setPowered(powered);
		}
		return data;
	}

	@Override
	public Spigot13BlockTypePowerable readBlockData(BlockData blockData) {
		if (blockData instanceof Powerable) {
			powered = ((Powerable) blockData).isPowered();
		}
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Spigot13BlockTypePowerable that = (Spigot13BlockTypePowerable) o;
		return powered == that.powered;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), powered);
	}
}
