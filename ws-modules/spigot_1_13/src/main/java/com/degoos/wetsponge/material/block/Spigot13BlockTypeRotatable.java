package com.degoos.wetsponge.material.block;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.util.Spigot13MaterialUtils;
import com.degoos.wetsponge.util.Validate;
import org.bukkit.Material;
import org.bukkit.block.BlockFace;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.Rotatable;

import java.util.Objects;

public class Spigot13BlockTypeRotatable extends Spigot13BlockType implements WSBlockTypeRotatable {

	private EnumBlockFace rotation;

	public Spigot13BlockTypeRotatable(int numericalId, String oldStringId, String newStringId, int maxStackSize, EnumBlockFace rotation) {
		super(numericalId, oldStringId, newStringId, maxStackSize);
		this.rotation = rotation;
	}

	@Override
	public EnumBlockFace getRotation() {
		return rotation;
	}

	@Override
	public void setRotation(EnumBlockFace rotation) {
		Validate.notNull(rotation, "Rotation cannot be null!");
		this.rotation = rotation;
	}

	@Override
	public Spigot13BlockTypeRotatable clone() {
		return new Spigot13BlockTypeRotatable(getNumericalId(), getOldStringId(), getNewStringId(), getMaxStackSize(), rotation);
	}

	@Override
	public BlockData toBlockData() {
		Material material = Spigot13MaterialUtils.getByKey(getNewStringId()).orElse(null);
		if (material == null) return null;
		BlockData data = material.createBlockData();
		if (data instanceof Rotatable) {
			((Rotatable) data).setRotation(BlockFace.valueOf(rotation.name()));
		}
		return data;
	}

	@Override
	public Spigot13BlockTypeRotatable readBlockData(BlockData blockData) {
		if (blockData instanceof Rotatable) {
			rotation = EnumBlockFace.valueOf(((Rotatable) blockData).getRotation().name());
		}
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Spigot13BlockTypeRotatable that = (Spigot13BlockTypeRotatable) o;
		return rotation == that.rotation;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), rotation);
	}
}
