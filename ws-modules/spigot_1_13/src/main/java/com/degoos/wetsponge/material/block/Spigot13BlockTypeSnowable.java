package com.degoos.wetsponge.material.block;

import com.degoos.wetsponge.util.Spigot13MaterialUtils;
import org.bukkit.Material;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.Snowable;

import java.util.Objects;

public class Spigot13BlockTypeSnowable extends Spigot13BlockType implements WSBlockTypeSnowable {

	public boolean snowy;

	public Spigot13BlockTypeSnowable(int numericalId, String oldStringId, String newStringId, int maxStackSize, boolean snowy) {
		super(numericalId, oldStringId, newStringId, maxStackSize);
		this.snowy = snowy;
	}

	@Override
	public boolean isSnowy() {
		return snowy;
	}

	@Override
	public void setSnowy(boolean snowy) {
		this.snowy = snowy;
	}

	@Override
	public Spigot13BlockTypeSnowable clone() {
		return new Spigot13BlockTypeSnowable(getNumericalId(), getOldStringId(), getNewStringId(), getMaxStackSize(), snowy);
	}

	@Override
	public BlockData toBlockData() {
		Material material = Spigot13MaterialUtils.getByKey(getNewStringId()).orElse(null);
		if (material == null) return null;
		BlockData data = material.createBlockData();
		if (data instanceof Snowable) {
			((Snowable) data).setSnowy(snowy);
		}
		return data;
	}

	@Override
	public Spigot13BlockTypeSnowable readBlockData(BlockData blockData) {
		if (blockData instanceof Snowable) {
			snowy = ((Snowable) blockData).isSnowy();
		}
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Spigot13BlockTypeSnowable that = (Spigot13BlockTypeSnowable) o;
		return snowy == that.snowy;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), snowy);
	}
}
