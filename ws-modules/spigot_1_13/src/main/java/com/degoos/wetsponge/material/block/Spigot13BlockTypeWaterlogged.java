package com.degoos.wetsponge.material.block;

import com.degoos.wetsponge.util.Spigot13MaterialUtils;
import org.bukkit.Material;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.Waterlogged;

import java.util.Objects;

public class Spigot13BlockTypeWaterlogged extends Spigot13BlockType implements WSBlockTypeWaterlogged {

	private boolean waterLogged;

	public Spigot13BlockTypeWaterlogged(int numericalId, String oldStringId, String newStringId, int maxStackSize, boolean waterLogged) {
		super(numericalId, oldStringId, newStringId, maxStackSize);
		this.waterLogged = waterLogged;
	}

	@Override
	public boolean isWaterlogged() {
		return waterLogged;
	}

	@Override
	public void setWaterlogged(boolean waterlogged) {
		this.waterLogged = waterlogged;
	}

	@Override
	public Spigot13BlockTypeWaterlogged clone() {
		return new Spigot13BlockTypeWaterlogged(getNumericalId(), getOldStringId(), getNewStringId(), getMaxStackSize(), waterLogged);
	}

	@Override
	public BlockData toBlockData() {
		Material material = Spigot13MaterialUtils.getByKey(getNewStringId()).orElse(null);
		if (material == null) return null;
		BlockData data = material.createBlockData();
		if (data instanceof Waterlogged) {
			((Waterlogged) data).setWaterlogged(waterLogged);
		}
		return data;
	}

	@Override
	public Spigot13BlockTypeWaterlogged readBlockData(BlockData blockData) {
		if (blockData instanceof Waterlogged) {
			waterLogged = ((Waterlogged) blockData).isWaterlogged();
		}
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Spigot13BlockTypeWaterlogged that = (Spigot13BlockTypeWaterlogged) o;
		return waterLogged == that.waterLogged;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), waterLogged);
	}
}
