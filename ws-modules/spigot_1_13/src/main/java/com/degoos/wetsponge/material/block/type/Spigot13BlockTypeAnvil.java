package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.enums.block.EnumBlockTypeAnvilDamage;
import com.degoos.wetsponge.material.block.Spigot13BlockTypeDirectional;
import com.degoos.wetsponge.util.Validate;
import org.bukkit.block.data.BlockData;

import java.util.Objects;
import java.util.Set;

public class Spigot13BlockTypeAnvil extends Spigot13BlockTypeDirectional implements WSBlockTypeAnvil {

	private EnumBlockTypeAnvilDamage damage;

	public Spigot13BlockTypeAnvil(EnumBlockFace facing, Set<EnumBlockFace> faces, EnumBlockTypeAnvilDamage damage) {
		super(145, "minecraft:anvil", "minecraft:anvil", 64, facing, faces);
		Validate.notNull(damage, "Damage cannot be null!");
		this.damage = damage;
	}

	@Override
	public String getNewStringId() {
		switch (damage) {
			case DAMAGED:
				return "minecraft:chipped_anvil";
			case VERY_DAMAGED:
				return "minecraft:damaged_anvil";
			case NORMAL:
			default:
				return "minecraft:anvil";
		}
	}

	@Override
	public EnumBlockTypeAnvilDamage getDamage() {
		return damage;
	}

	@Override
	public void setDamage(EnumBlockTypeAnvilDamage damage) {
		Validate.notNull(damage, "Damage cannot be null!");
		this.damage = damage;
	}

	@Override
	public Spigot13BlockTypeAnvil clone() {
		return new Spigot13BlockTypeAnvil(getFacing(), getFaces(), damage);
	}

	@Override
	public Spigot13BlockTypeAnvil readBlockData(BlockData blockData) {
		super.readBlockData(blockData);
		return this;
	}


	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Spigot13BlockTypeAnvil that = (Spigot13BlockTypeAnvil) o;
		return damage == that.damage;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), damage);
	}
}
