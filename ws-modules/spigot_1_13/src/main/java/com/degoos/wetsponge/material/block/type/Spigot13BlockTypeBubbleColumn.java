package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.material.block.Spigot13BlockType;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.type.BubbleColumn;

import java.util.Objects;

public class Spigot13BlockTypeBubbleColumn extends Spigot13BlockType implements WSBlockTypeBubbleColumn {

	private boolean drag;

	public Spigot13BlockTypeBubbleColumn(boolean drag) {
		super(-1, null, "minecraft:bubble_column", 64);
		this.drag = drag;
	}

	@Override
	public boolean isDrag() {
		return drag;
	}

	@Override
	public void setDrag(boolean drag) {
		this.drag = drag;
	}

	@Override
	public Spigot13BlockTypeBubbleColumn clone() {
		return new Spigot13BlockTypeBubbleColumn(drag);
	}

	@Override
	public BlockData toBlockData() {
		BlockData blockData = super.toBlockData();
		if (blockData instanceof BubbleColumn) {
			((BubbleColumn) blockData).setDrag(drag);
		}
		return blockData;
	}

	@Override
	public Spigot13BlockTypeBubbleColumn readBlockData(BlockData blockData) {
		if (blockData instanceof BubbleColumn) {
			drag = ((BubbleColumn) blockData).isDrag();
		}
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Spigot13BlockTypeBubbleColumn that = (Spigot13BlockTypeBubbleColumn) o;
		return drag == that.drag;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), drag);
	}
}
