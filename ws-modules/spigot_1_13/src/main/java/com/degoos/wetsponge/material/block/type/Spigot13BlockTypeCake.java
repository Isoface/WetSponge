package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.material.block.Spigot13BlockType;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.type.Cake;

import java.util.Objects;

public class Spigot13BlockTypeCake extends Spigot13BlockType implements WSBlockTypeCake {

	private int bites, maximumBites;

	public Spigot13BlockTypeCake(int bites, int maximumBites) {
		super(92, "minecraft:cake", "minecraft:cake", 64);
		this.bites = bites;
		this.maximumBites = maximumBites;
	}

	@Override
	public int getBites() {
		return bites;
	}

	@Override
	public void setBites(int bites) {
		this.bites = Math.min(maximumBites, Math.max(0, bites));
	}

	@Override
	public int getMaximumBites() {
		return maximumBites;
	}

	@Override
	public Spigot13BlockTypeCake clone() {
		return new Spigot13BlockTypeCake(bites, maximumBites);
	}

	@Override
	public BlockData toBlockData() {
		BlockData blockData = super.toBlockData();
		if (blockData instanceof Cake) {
			((Cake) blockData).setBites(bites);
		}
		return blockData;
	}

	@Override
	public Spigot13BlockTypeCake readBlockData(BlockData blockData) {
		if (blockData instanceof Cake) {
			bites = ((Cake) blockData).getBites();
			maximumBites = ((Cake) blockData).getMaximumBites();
		}
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Spigot13BlockTypeCake that = (Spigot13BlockTypeCake) o;
		return bites == that.bites &&
				maximumBites == that.maximumBites;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), bites, maximumBites);
	}
}
