package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.enums.block.EnumBlockTypeChestType;
import com.degoos.wetsponge.material.block.Spigot13BlockTypeDirectional;
import com.degoos.wetsponge.util.Validate;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.type.Chest;

import java.util.Objects;
import java.util.Set;

public class Spigot13BlockTypeChest extends Spigot13BlockTypeDirectional implements WSBlockTypeChest {

	private EnumBlockTypeChestType chestType;
	private boolean waterlogged;

	public Spigot13BlockTypeChest(int numericalId, String oldStringId, String newStringId, int maxStackSize, EnumBlockFace facing, Set<EnumBlockFace> faces,
								  EnumBlockTypeChestType chestType, boolean waterlogged) {
		super(numericalId, oldStringId, newStringId, maxStackSize, facing, faces);
		Validate.notNull(chestType, "ChestType cannot be null!");
		this.chestType = chestType;
		this.waterlogged = waterlogged;
	}

	@Override
	public EnumBlockTypeChestType getType() {
		return chestType;
	}

	@Override
	public void setType(EnumBlockTypeChestType type) {
		Validate.notNull(type, "ChestType cannot be null!");
		this.chestType = type;
	}

	@Override
	public boolean isWaterlogged() {
		return waterlogged;
	}

	@Override
	public void setWaterlogged(boolean waterlogged) {
		this.waterlogged = waterlogged;
	}

	@Override
	public Spigot13BlockTypeChest clone() {
		return new Spigot13BlockTypeChest(getNumericalId(), getOldStringId(), getNewStringId(), getMaxStackSize(), getFacing(), getFaces(), chestType, waterlogged);
	}

	@Override
	public BlockData toBlockData() {
		BlockData blockData = super.toBlockData();
		if (blockData instanceof Chest) {
			((Chest) blockData).setType(Chest.Type.valueOf(chestType.name()));
			((Chest) blockData).setWaterlogged(waterlogged);
		}
		return blockData;
	}

	@Override
	public Spigot13BlockTypeChest readBlockData(BlockData blockData) {
		super.readBlockData(blockData);
		if (blockData instanceof Chest) {
			chestType = EnumBlockTypeChestType.valueOf(((Chest) blockData).getType().name());
			waterlogged = ((Chest) blockData).isWaterlogged();
		}
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Spigot13BlockTypeChest that = (Spigot13BlockTypeChest) o;
		return waterlogged == that.waterlogged &&
				chestType == that.chestType;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), chestType, waterlogged);
	}
}
