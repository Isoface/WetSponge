package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.material.block.Spigot13BlockTypeDirectional;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.type.Cocoa;

import java.util.Objects;
import java.util.Set;

public class Spigot13BlockTypeCocoa extends Spigot13BlockTypeDirectional implements WSBlockTypeCocoa {

	private int age, maximumAge;

	public Spigot13BlockTypeCocoa(EnumBlockFace facing, Set<EnumBlockFace> faces, int age, int maximumAge) {
		super(127, "minecraft:cocoa", "minecraft:cocoa", 64, facing, faces);
		this.age = age;
		this.maximumAge = maximumAge;
	}

	@Override
	public int getAge() {
		return age;
	}

	@Override
	public void setAge(int age) {
		this.age = Math.min(maximumAge, Math.max(0, age));
	}

	@Override
	public int getMaximumAge() {
		return maximumAge;
	}

	@Override
	public Spigot13BlockTypeCocoa clone() {
		return new Spigot13BlockTypeCocoa(getFacing(), getFaces(), age, maximumAge);
	}

	@Override
	public BlockData toBlockData() {
		BlockData blockData = super.toBlockData();
		if (blockData instanceof Cocoa) {
			((Cocoa) blockData).setAge(age);
		}
		return blockData;
	}

	@Override
	public Spigot13BlockTypeCocoa readBlockData(BlockData blockData) {
		super.readBlockData(blockData);
		if (blockData instanceof Cocoa) {
			age = ((Cocoa) blockData).getAge();
			maximumAge = ((Cocoa) blockData).getMaximumAge();
		}
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Spigot13BlockTypeCocoa that = (Spigot13BlockTypeCocoa) o;
		return age == that.age &&
				maximumAge == that.maximumAge;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), age, maximumAge);
	}
}
