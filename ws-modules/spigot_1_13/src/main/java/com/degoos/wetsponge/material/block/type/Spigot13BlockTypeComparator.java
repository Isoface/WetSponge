package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.enums.block.EnumBlockTypeComparatorMode;
import com.degoos.wetsponge.material.block.Spigot13BlockTypeDirectional;
import com.degoos.wetsponge.util.Validate;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.type.Comparator;

import java.util.Objects;
import java.util.Set;

public class Spigot13BlockTypeComparator extends Spigot13BlockTypeDirectional implements WSBlockTypeComparator {

	private EnumBlockTypeComparatorMode mode;
	private boolean powered;

	public Spigot13BlockTypeComparator(EnumBlockFace facing, Set<EnumBlockFace> faces, EnumBlockTypeComparatorMode mode, boolean powered) {
		super(149, "minecraft:unpowered_comparator", "minecraft:comparator", 64, facing, faces);
		Validate.notNull(mode, "Mode cannot be null!");
		this.mode = mode;
		this.powered = powered;
	}

	@Override
	public int getNumericalId() {
		return isPowered() ? 150 : 149;
	}

	@Override
	public String getOldStringId() {
		return isPowered() ? "minecraft:powered_comparator" : "minecraft:unpowered_comparator";
	}

	@Override
	public EnumBlockTypeComparatorMode getMode() {
		return mode;
	}

	@Override
	public void setMode(EnumBlockTypeComparatorMode mode) {
		Validate.notNull(mode, "Mode cannot be null!");
		this.mode = mode;
	}

	@Override
	public boolean isPowered() {
		return powered;
	}

	@Override
	public void setPowered(boolean powered) {
		this.powered = powered;
	}

	@Override
	public Spigot13BlockTypeComparator clone() {
		return new Spigot13BlockTypeComparator(getFacing(), getFaces(), mode, powered);
	}

	@Override
	public BlockData toBlockData() {
		BlockData blockData = super.toBlockData();
		if (blockData instanceof Comparator) {
			((Comparator) blockData).setPowered(powered);
			((Comparator) blockData).setMode(Comparator.Mode.valueOf(mode.name()));
		}
		return blockData;
	}

	@Override
	public Spigot13BlockTypeDirectional readBlockData(BlockData blockData) {
		super.readBlockData(blockData);
		if (blockData instanceof Comparator) {
			mode = EnumBlockTypeComparatorMode.valueOf(((Comparator) blockData).getMode().name());
			powered = ((Comparator) blockData).isPowered();
		}
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Spigot13BlockTypeComparator that = (Spigot13BlockTypeComparator) o;
		return powered == that.powered &&
				mode == that.mode;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), mode, powered);
	}
}
