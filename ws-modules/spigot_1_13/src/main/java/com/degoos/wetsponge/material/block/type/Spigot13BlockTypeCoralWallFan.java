package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.material.block.Spigot13BlockTypeDirectional;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.type.CoralWallFan;

import java.util.Objects;
import java.util.Set;

public class Spigot13BlockTypeCoralWallFan extends Spigot13BlockTypeDirectional implements WSBlockTypeCoralWallFan {

	private boolean waterlogged;

	public Spigot13BlockTypeCoralWallFan(int numericalId, String oldStringId, String newStringId, int maxStackSize, EnumBlockFace facing, Set<EnumBlockFace> faces, boolean waterlogged) {
		super(numericalId, oldStringId, newStringId, maxStackSize, facing, faces);
		this.waterlogged = waterlogged;
	}

	@Override
	public boolean isWaterlogged() {
		return waterlogged;
	}

	@Override
	public void setWaterlogged(boolean waterlogged) {
		this.waterlogged = waterlogged;
	}

	@Override
	public Spigot13BlockTypeCoralWallFan clone() {
		return new Spigot13BlockTypeCoralWallFan(getNumericalId(), getOldStringId(), getNewStringId(), getMaxStackSize(), getFacing(), getFaces(), waterlogged);
	}

	@Override
	public BlockData toBlockData() {
		BlockData blockData = super.toBlockData();
		if (blockData instanceof CoralWallFan) {
			((CoralWallFan) blockData).setWaterlogged(waterlogged);
		}
		return blockData;
	}

	@Override
	public Spigot13BlockTypeCoralWallFan readBlockData(BlockData blockData) {
		super.readBlockData(blockData);
		if (blockData instanceof CoralWallFan) {
			waterlogged = ((CoralWallFan) blockData).isWaterlogged();
		}
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Spigot13BlockTypeCoralWallFan that = (Spigot13BlockTypeCoralWallFan) o;
		return waterlogged == that.waterlogged;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), waterlogged);
	}
}
