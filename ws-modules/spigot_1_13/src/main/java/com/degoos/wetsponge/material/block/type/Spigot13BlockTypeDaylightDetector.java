package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.material.block.Spigot13BlockTypeAnaloguePowerable;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.type.DaylightDetector;

import java.util.Objects;

public class Spigot13BlockTypeDaylightDetector extends Spigot13BlockTypeAnaloguePowerable implements WSBlockTypeDaylightDetector {

	private boolean inverted;

	public Spigot13BlockTypeDaylightDetector(int power, int maximumPower, boolean inverted) {
		super(151, "minecraft:daylight_detector", "minecraft:daylight_detector", 64, power, maximumPower);
		this.inverted = inverted;
	}

	@Override
	public int getNumericalId() {
		return isInverted() ? 178 : 151;
	}

	@Override
	public String getOldStringId() {
		return isInverted() ? "minecraft:daylight_detector_inverted" : "minecraft:daylight_detector";
	}

	@Override
	public boolean isInverted() {
		return inverted;
	}

	@Override
	public void setInverted(boolean inverted) {
		this.inverted = inverted;
	}

	@Override
	public Spigot13BlockTypeDaylightDetector clone() {
		return new Spigot13BlockTypeDaylightDetector(getPower(), gerMaximumPower(), inverted);
	}

	@Override
	public BlockData toBlockData() {
		BlockData blockData = super.toBlockData();
		if (blockData instanceof WSBlockTypeDaylightDetector) {
			((WSBlockTypeDaylightDetector) blockData).setInverted(inverted);
		}
		return blockData;
	}

	@Override
	public Spigot13BlockTypeDaylightDetector readBlockData(BlockData blockData) {
		super.readBlockData(blockData);
		if (blockData instanceof DaylightDetector) {
			inverted = ((DaylightDetector) blockData).isInverted();
		}
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Spigot13BlockTypeDaylightDetector that = (Spigot13BlockTypeDaylightDetector) o;
		return inverted == that.inverted;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), inverted);
	}
}
