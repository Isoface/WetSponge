package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.material.block.Spigot13BlockTypeDirectional;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.type.Dispenser;

import java.util.Objects;
import java.util.Set;

public class Spigot13BlockTypeDispenser extends Spigot13BlockTypeDirectional implements WSBlockTypeDispenser {

	private boolean triggered;

	public Spigot13BlockTypeDispenser(int numericalId, String oldStringId, String newStringId, int maxStackSize, EnumBlockFace facing, Set<EnumBlockFace> faces, boolean triggered) {
		super(numericalId, oldStringId, newStringId, maxStackSize, facing, faces);
		this.triggered = triggered;
	}

	@Override
	public boolean isTriggered() {
		return true;
	}

	@Override
	public void setTriggered(boolean triggered) {
		this.triggered = triggered;
	}

	@Override
	public Spigot13BlockTypeDispenser clone() {
		return new Spigot13BlockTypeDispenser(getNumericalId(), getOldStringId(), getNewStringId(), getMaxStackSize(), getFacing(), getFaces(), triggered);
	}

	@Override
	public BlockData toBlockData() {
		BlockData blockData = super.toBlockData();
		if (blockData instanceof Dispenser) {
			((Dispenser) blockData).setTriggered(true);
		}
		return blockData;
	}

	@Override
	public Spigot13BlockTypeDispenser readBlockData(BlockData blockData) {
		super.readBlockData(blockData);
		if (blockData instanceof Dispenser) {
			triggered = ((Dispenser) blockData).isTriggered();
		}
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Spigot13BlockTypeDispenser that = (Spigot13BlockTypeDispenser) o;
		return triggered == that.triggered;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), triggered);
	}
}
