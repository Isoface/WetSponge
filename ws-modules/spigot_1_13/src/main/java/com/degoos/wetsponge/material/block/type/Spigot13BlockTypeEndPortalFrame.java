package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.material.block.Spigot13BlockTypeDirectional;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.type.EndPortalFrame;

import java.util.Objects;
import java.util.Set;

public class Spigot13BlockTypeEndPortalFrame extends Spigot13BlockTypeDirectional implements WSBlockTypeEndPortalFrame {

	private boolean eye;

	public Spigot13BlockTypeEndPortalFrame(EnumBlockFace facing, Set<EnumBlockFace> faces, boolean eye) {
		super(120, "minecraft:end_portal_frame", "minecraft:end_portal_frame", 64, facing, faces);
		this.eye = eye;
	}

	@Override
	public boolean hasEye() {
		return eye;
	}

	@Override
	public void setEye(boolean eye) {
		this.eye = eye;
	}

	@Override
	public Spigot13BlockTypeEndPortalFrame clone() {
		return new Spigot13BlockTypeEndPortalFrame(getFacing(), getFaces(), eye);
	}

	@Override
	public BlockData toBlockData() {
		BlockData blockData = super.toBlockData();
		if (blockData instanceof EndPortalFrame) {
			((EndPortalFrame) blockData).setEye(eye);
		}
		return blockData;
	}

	@Override
	public Spigot13BlockTypeEndPortalFrame readBlockData(BlockData blockData) {
		super.readBlockData(blockData);
		if (blockData instanceof EndPortalFrame) {
			eye = ((EndPortalFrame) blockData).hasEye();
		}
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Spigot13BlockTypeEndPortalFrame that = (Spigot13BlockTypeEndPortalFrame) o;
		return eye == that.eye;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), eye);
	}
}
