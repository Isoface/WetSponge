package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.material.block.Spigot13BlockTypeDirectional;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.type.EnderChest;

import java.util.Objects;
import java.util.Set;

public class Spigot13BlockTypeEnderchest extends Spigot13BlockTypeDirectional implements WSBlockTypeEnderchest {

	private boolean waterlogged;

	public Spigot13BlockTypeEnderchest(EnumBlockFace facing, Set<EnumBlockFace> faces, boolean waterlogged) {
		super(130, "minecraft:ender_chest", "minecraft:ender_chest", 64, facing, faces);
		this.waterlogged = waterlogged;
	}

	@Override
	public boolean isWaterlogged() {
		return waterlogged;
	}

	@Override
	public void setWaterlogged(boolean waterlogged) {
		this.waterlogged = waterlogged;
	}

	@Override
	public Spigot13BlockTypeEnderchest clone() {
		return new Spigot13BlockTypeEnderchest(getFacing(), getFaces(), waterlogged);
	}

	@Override
	public BlockData toBlockData() {
		BlockData blockData = super.toBlockData();
		if (blockData instanceof EnderChest) {
			((EnderChest) blockData).setWaterlogged(waterlogged);
		}
		return blockData;
	}

	@Override
	public Spigot13BlockTypeEnderchest readBlockData(BlockData blockData) {
		super.readBlockData(blockData);
		if (blockData instanceof EnderChest) {
			waterlogged = ((EnderChest) blockData).isWaterlogged();
		}
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Spigot13BlockTypeEnderchest that = (Spigot13BlockTypeEnderchest) o;
		return waterlogged == that.waterlogged;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), waterlogged);
	}
}
