package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.material.block.Spigot13BlockTypeMultipleFacing;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.type.Fire;

import java.util.Objects;
import java.util.Set;

public class Spigot13BlockTypeFire extends Spigot13BlockTypeMultipleFacing implements WSBlockTypeFire {

	private int age, maximumAge;

	public Spigot13BlockTypeFire(Set<EnumBlockFace> faces, Set<EnumBlockFace> allowedFaces, int age, int maximumAge) {
		super(51, "minecraft:fire", "minecraft:fire", 64, faces, allowedFaces);
		this.age = age;
		this.maximumAge = maximumAge;
	}

	@Override
	public int getAge() {
		return age;
	}

	@Override
	public void setAge(int age) {
		this.age = Math.min(maximumAge, Math.max(0, age));
	}

	@Override
	public int getMaximumAge() {
		return maximumAge;
	}

	@Override
	public Spigot13BlockTypeFire clone() {
		return new Spigot13BlockTypeFire(getFaces(), getAllowedFaces(), age, maximumAge);
	}

	@Override
	public BlockData toBlockData() {
		BlockData blockData = super.toBlockData();
		if (blockData instanceof Fire) {
			((Fire) blockData).setAge(age);
		}
		return blockData;
	}

	@Override
	public Spigot13BlockTypeFire readBlockData(BlockData blockData) {
		super.readBlockData(blockData);
		if (blockData instanceof Fire) {
			age = ((Fire) blockData).getAge();
			maximumAge = ((Fire) blockData).getMaximumAge();
		}
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Spigot13BlockTypeFire that = (Spigot13BlockTypeFire) o;
		return age == that.age &&
				maximumAge == that.maximumAge;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), age, maximumAge);
	}
}
