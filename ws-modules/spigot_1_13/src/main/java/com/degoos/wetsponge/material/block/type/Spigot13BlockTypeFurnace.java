package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.material.block.Spigot13BlockTypeDirectional;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.type.Furnace;

import java.util.Objects;
import java.util.Set;

public class Spigot13BlockTypeFurnace extends Spigot13BlockTypeDirectional implements WSBlockTypeFurnace {

	private boolean lit;

	public Spigot13BlockTypeFurnace(EnumBlockFace facing, Set<EnumBlockFace> faces, boolean lit) {
		super(61, "minecraft:furnace", "minecraft:furnace", 64, facing, faces);
		this.lit = lit;
	}

	@Override
	public int getNumericalId() {
		return isLit() ? 62 : 61;
	}

	@Override
	public String getOldStringId() {
		return isLit() ? "minecraft:lit_furnace" : "minecraft:furnace";
	}

	@Override
	public boolean isLit() {
		return lit;
	}

	@Override
	public void setLit(boolean lit) {
		this.lit = lit;
	}

	@Override
	public Spigot13BlockTypeFurnace clone() {
		return new Spigot13BlockTypeFurnace(getFacing(), getFaces(), lit);
	}

	@Override
	public BlockData toBlockData() {
		BlockData blockData = super.toBlockData();
		if (blockData instanceof Furnace) {
			((Furnace) blockData).setLit(lit);
		}
		return blockData;
	}

	@Override
	public Spigot13BlockTypeFurnace readBlockData(BlockData blockData) {
		super.readBlockData(blockData);
		if (blockData instanceof Furnace) {
			lit = ((Furnace) blockData).isLit();
		}
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Spigot13BlockTypeFurnace that = (Spigot13BlockTypeFurnace) o;
		return lit == that.lit;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), lit);
	}
}
