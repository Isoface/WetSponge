package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.material.block.Spigot13BlockTypeDirectional;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.type.Hopper;

import java.util.Objects;
import java.util.Set;

public class Spigot13BlockTypeHopper extends Spigot13BlockTypeDirectional implements WSBlockTypeHopper {

	private boolean enabled;

	public Spigot13BlockTypeHopper(EnumBlockFace facing, Set<EnumBlockFace> faces, boolean enabled) {
		super(154, "minecraft:hopper", "minecraft:hopper", 64, facing, faces);
		this.enabled = enabled;
	}

	@Override
	public boolean isEnabled() {
		return enabled;
	}

	@Override
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	@Override
	public Spigot13BlockTypeHopper clone() {
		return new Spigot13BlockTypeHopper(getFacing(), getFaces(), enabled);
	}

	@Override
	public BlockData toBlockData() {
		BlockData blockData = super.toBlockData();
		if (blockData instanceof Hopper) {
			((Hopper) blockData).setEnabled(enabled);
		}
		return blockData;
	}

	@Override
	public Spigot13BlockTypeHopper readBlockData(BlockData blockData) {
		super.readBlockData(blockData);
		if (blockData instanceof Hopper) {
			enabled = ((Hopper) blockData).isEnabled();
		}
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Spigot13BlockTypeHopper that = (Spigot13BlockTypeHopper) o;
		return enabled == that.enabled;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), enabled);
	}
}
