package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockTypeDisguiseType;
import com.degoos.wetsponge.material.block.Spigot13BlockType;
import com.degoos.wetsponge.util.Validate;
import org.bukkit.block.data.BlockData;

import java.util.Objects;

public class Spigot13BlockTypeInfestedStone extends Spigot13BlockType implements WSBlockTypeInfestedStone {

	private EnumBlockTypeDisguiseType disguiseType;

	public Spigot13BlockTypeInfestedStone(EnumBlockTypeDisguiseType disguiseType) {
		super(97, "minecraft:monster_egg", "minecraft:infested_stone", 64);
		Validate.notNull(disguiseType, "Disguise type cannot be null!");
		this.disguiseType = disguiseType;
	}

	@Override
	public String getNewStringId() {
		switch (disguiseType) {
			case STONE_BRICK:
				return "minecraft:infested_stone_bricks";
			case COBBLESTONE:
				return "minecraft:infested_cobblestone";
			case MOSSY_STONE_BRICK:
				return "minecraft:infested_mossy_stone_bricks";
			case CRACKED_STONE_BRICK:
				return "minecraft:infested_cracked_stone_bricks";
			case CHISELED_STONE_BRICK:
				return "minecraft:infested_chiseled_stone_bricks";
			case STONE:
			default:
				return "minecraft:infested_stone";
		}
	}

	@Override
	public EnumBlockTypeDisguiseType getDisguiseType() {
		return disguiseType;
	}

	@Override
	public void setDisguiseType(EnumBlockTypeDisguiseType disguiseType) {
		Validate.notNull(disguiseType, "Disguise type cannot be null!");
		this.disguiseType = disguiseType;
	}

	@Override
	public Spigot13BlockTypeInfestedStone clone() {
		return new Spigot13BlockTypeInfestedStone(disguiseType);
	}

	@Override
	public Spigot13BlockTypeInfestedStone readBlockData(BlockData blockData) {
		super.readBlockData(blockData);
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Spigot13BlockTypeInfestedStone that = (Spigot13BlockTypeInfestedStone) o;
		return disguiseType == that.disguiseType;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), disguiseType);
	}
}
