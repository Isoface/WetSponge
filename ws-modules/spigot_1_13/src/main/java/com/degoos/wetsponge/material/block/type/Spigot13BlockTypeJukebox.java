package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.material.block.Spigot13BlockType;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.type.Jukebox;

import java.util.Objects;

public class Spigot13BlockTypeJukebox extends Spigot13BlockType implements WSBlockTypeJukebox {

	private boolean record;

	public Spigot13BlockTypeJukebox(boolean record) {
		super(84, "minecraft:jukebox", "minecraft:jukebox", 64);
		this.record = record;
	}

	@Override
	public boolean hasRecord() {
		return record;
	}

	@Override
	public Spigot13BlockTypeJukebox clone() {
		return new Spigot13BlockTypeJukebox(record);
	}

	@Override
	public Spigot13BlockTypeJukebox readBlockData(BlockData blockData) {
		if (blockData instanceof Jukebox) {
			record = ((Jukebox) blockData).hasRecord();
		}
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Spigot13BlockTypeJukebox that = (Spigot13BlockTypeJukebox) o;
		return record == that.record;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), record);
	}
}
