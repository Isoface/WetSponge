package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.material.block.Spigot13BlockTypeLevelled;
import org.bukkit.block.data.BlockData;

public class Spigot13BlockTypeLava extends Spigot13BlockTypeLevelled implements WSBlockTypeLava {

	public Spigot13BlockTypeLava(int level, int maximumLevel) {
		super(9, "minecraft:lava", "minecraft:lava", 64, level, maximumLevel);
	}

	@Override
	public int getNumericalId() {
		return getLevel() == 0 ? 11 : 10;
	}

	@Override
	public String getOldStringId() {
		return getLevel() == 0 ? "minecraft:lava" : "minecraft:flowing_lava";
	}

	@Override
	public Spigot13BlockTypeLava clone() {
		return new Spigot13BlockTypeLava(getLevel(), getMaximumLevel());
	}

	@Override
	public Spigot13BlockTypeLava readBlockData(BlockData blockData) {
		super.readBlockData(blockData);
		return this;
	}
}
