package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumAxis;
import com.degoos.wetsponge.enums.block.EnumWoodType;
import com.degoos.wetsponge.material.block.Spigot13BlockTypeOrientable;
import org.bukkit.block.data.BlockData;

import java.util.Objects;
import java.util.Set;

public class Spigot13BlockTypeLog extends Spigot13BlockTypeOrientable implements WSBlockTypeLog {

	private EnumWoodType woodType;
	private boolean stripped, allFacesCovered;

	public Spigot13BlockTypeLog(EnumAxis axis, Set<EnumAxis> axes, EnumWoodType woodType, boolean stripped, boolean allFacesCovered) {
		super(17, "minecraft:log", "minecraft:log", 64, axis, axes);
		this.woodType = woodType;
		this.stripped = stripped;
		this.allFacesCovered = allFacesCovered;
	}

	@Override
	public int getNumericalId() {
		return getWoodType() == EnumWoodType.ACACIA || getWoodType() == EnumWoodType.DARK_OAK ? 162 : 17;
	}

	@Override
	public String getOldStringId() {
		return getWoodType() == EnumWoodType.ACACIA || getWoodType() == EnumWoodType.DARK_OAK ? "minecraft:log2" : "minecraft:log";
	}

	@Override
	public String getNewStringId() {
		return "minecraft:" + (stripped ? "stripped_" : "") + getWoodType().name().toLowerCase() + (allFacesCovered ? "_wood" : "_log");
	}

	@Override
	public EnumWoodType getWoodType() {
		return woodType;
	}

	@Override
	public void setWoodType(EnumWoodType woodType) {
		this.woodType = woodType;
	}

	@Override
	public boolean isStripped() {
		return stripped;
	}

	@Override
	public void setStripped(boolean stripped) {
		this.stripped = stripped;
	}

	@Override
	public boolean hasAllFacesCovered() {
		return allFacesCovered;
	}

	@Override
	public void setAllFacesCovered(boolean allFacesCovered) {
		this.allFacesCovered = allFacesCovered;
	}

	@Override
	public Spigot13BlockTypeLog clone() {
		return new Spigot13BlockTypeLog(getAxis(), getAxes(), woodType, stripped, allFacesCovered);
	}

	@Override
	public Spigot13BlockTypeLog readBlockData(BlockData blockData) {
		super.readBlockData(blockData);
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Spigot13BlockTypeLog that = (Spigot13BlockTypeLog) o;
		return stripped == that.stripped &&
				allFacesCovered == that.allFacesCovered &&
				woodType == that.woodType;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), woodType, stripped, allFacesCovered);
	}
}
