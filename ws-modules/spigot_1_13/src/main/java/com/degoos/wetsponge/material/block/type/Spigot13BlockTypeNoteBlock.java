package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.EnumInstrument;
import com.degoos.wetsponge.material.block.Spigot13BlockTypePowerable;
import com.degoos.wetsponge.util.Validate;
import org.bukkit.Instrument;
import org.bukkit.Note;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.type.NoteBlock;

import java.util.Objects;

public class Spigot13BlockTypeNoteBlock extends Spigot13BlockTypePowerable implements WSBlockTypeNoteBlock {

	private EnumInstrument instrument;
	private int note;

	public Spigot13BlockTypeNoteBlock(boolean powered, EnumInstrument instrument, int note) {
		super(25, "minecraft:noteblock", "minecraft:note_block", 64, powered);
		Validate.notNull(instrument, "Instrument cannot be null!");
		this.instrument = instrument;
		this.note = Math.max(0, Math.min(25, note));
	}

	@Override
	public EnumInstrument getInstrument() {
		return instrument;
	}

	@Override
	public void setInstrument(EnumInstrument instrument) {
		Validate.notNull(instrument, "Instrument cannot be null!");
		this.instrument = instrument;
	}

	@Override
	public int getNote() {
		return note;
	}

	@Override
	public void setNote(int note) {
		this.note = Math.max(0, Math.min(25, note));
	}

	@Override
	public Spigot13BlockTypeNoteBlock clone() {
		return new Spigot13BlockTypeNoteBlock(isPowered(), instrument, note);
	}

	@Override
	public BlockData toBlockData() {
		BlockData blockData = super.toBlockData();
		if (blockData instanceof NoteBlock) {
			((NoteBlock) blockData).setInstrument(Instrument.valueOf(instrument.name()));
			((NoteBlock) blockData).setNote(new Note(note));
		}
		return blockData;
	}

	@Override
	public Spigot13BlockTypeNoteBlock readBlockData(BlockData blockData) {
		super.readBlockData(blockData);
		if (blockData instanceof NoteBlock) {
			note = ((NoteBlock) blockData).getNote().getId();
			instrument = EnumInstrument.valueOf(((NoteBlock) blockData).getInstrument().name());
		}
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Spigot13BlockTypeNoteBlock that = (Spigot13BlockTypeNoteBlock) o;
		return note == that.note &&
				instrument == that.instrument;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), instrument, note);
	}
}
