package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.material.block.Spigot13BlockTypeDirectional;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.type.Observer;

import java.util.Objects;
import java.util.Set;

public class Spigot13BlockTypeObserver extends Spigot13BlockTypeDirectional implements WSBlockTypeObserver {

	private boolean powered;

	public Spigot13BlockTypeObserver(EnumBlockFace facing, Set<EnumBlockFace> faces, boolean powered) {
		super(218, "minecraft:observer", "minecraft:observer", 64, facing, faces);
		this.powered = powered;
	}

	@Override
	public boolean isPowered() {
		return powered;
	}

	@Override
	public void setPowered(boolean powered) {
		this.powered = powered;
	}

	@Override
	public Spigot13BlockTypeObserver clone() {
		return new Spigot13BlockTypeObserver(getFacing(), getFaces(), powered);
	}

	@Override
	public BlockData toBlockData() {
		BlockData blockData = super.toBlockData();
		if (blockData instanceof Observer) {
			((Observer) blockData).setPowered(powered);
		}
		return blockData;
	}

	@Override
	public Spigot13BlockTypeObserver readBlockData(BlockData blockData) {
		super.readBlockData(blockData);
		if (blockData instanceof Observer) {
			powered = ((Observer) blockData).isPowered();
		}
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Spigot13BlockTypeObserver that = (Spigot13BlockTypeObserver) o;
		return powered == that.powered;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), powered);
	}
}
