package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.material.block.Spigot13BlockTypeDirectional;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.type.Piston;

import java.util.Objects;
import java.util.Set;

public class Spigot13BlockTypePiston extends Spigot13BlockTypeDirectional implements WSBlockTypePiston {

	private boolean extended;

	public Spigot13BlockTypePiston(int numericalId, String oldStringId, String newStringId, int maxStackSize, EnumBlockFace facing, Set<EnumBlockFace> faces, boolean extended) {
		super(numericalId, oldStringId, newStringId, maxStackSize, facing, faces);
		this.extended = extended;
	}

	@Override
	public boolean isExtended() {
		return extended;
	}

	@Override
	public void setExtended(boolean extended) {
		this.extended = extended;
	}

	@Override
	public Spigot13BlockTypePiston clone() {
		return new Spigot13BlockTypePiston(getNumericalId(), getOldStringId(), getNewStringId(), getMaxStackSize(), getFacing(), getFaces(), extended);
	}

	@Override
	public BlockData toBlockData() {
		BlockData blockData = super.toBlockData();
		if(blockData instanceof Piston) {
			((Piston) blockData).setExtended(extended);
		}
		return blockData;
	}

	@Override
	public Spigot13BlockTypePiston readBlockData(BlockData blockData) {
		super.readBlockData(blockData);
		if(blockData instanceof Piston) {
			extended = ((Piston) blockData).isExtended();
		}
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Spigot13BlockTypePiston that = (Spigot13BlockTypePiston) o;
		return extended == that.extended;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), extended);
	}
}
