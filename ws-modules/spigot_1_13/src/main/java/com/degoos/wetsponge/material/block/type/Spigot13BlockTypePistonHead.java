package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.enums.block.EnumBlockTypePistonType;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.type.PistonHead;

import java.util.Objects;
import java.util.Set;

public class Spigot13BlockTypePistonHead extends Spigot13BlockTypeTechnicalPiston implements WSBlockTypePistonHead {

	private boolean shortPiston;

	public Spigot13BlockTypePistonHead(EnumBlockFace facing, Set<EnumBlockFace> faces, EnumBlockTypePistonType pistonType, boolean shortPiston) {
		super(34, "minecraft:piston_head", "minecraft:piston_head", 64, facing, faces, pistonType);
		this.shortPiston = shortPiston;
	}

	@Override
	public boolean isShort() {
		return shortPiston;
	}

	@Override
	public void setShort(boolean s) {
		this.shortPiston = s;
	}

	@Override
	public Spigot13BlockTypePistonHead clone() {
		return new Spigot13BlockTypePistonHead(getFacing(), getFaces(), getType(), shortPiston);
	}

	@Override
	public BlockData toBlockData() {
		BlockData blockData = super.toBlockData();
		if (blockData instanceof PistonHead) {
			((PistonHead) blockData).setShort(shortPiston);
		}
		return blockData;
	}

	@Override
	public Spigot13BlockTypePistonHead readBlockData(BlockData blockData) {
		super.readBlockData(blockData);
		if (blockData instanceof PistonHead) {
			shortPiston = ((PistonHead) blockData).isShort();
		}
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Spigot13BlockTypePistonHead that = (Spigot13BlockTypePistonHead) o;
		return shortPiston == that.shortPiston;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), shortPiston);
	}
}
