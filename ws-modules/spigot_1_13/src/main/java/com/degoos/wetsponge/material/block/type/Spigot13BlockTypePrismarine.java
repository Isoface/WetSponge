package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockTypePrismarineType;
import com.degoos.wetsponge.material.block.Spigot13BlockType;
import com.degoos.wetsponge.util.Validate;
import org.bukkit.block.data.BlockData;

import java.util.Objects;

public class Spigot13BlockTypePrismarine extends Spigot13BlockType implements WSBlockTypePrismarine {

	private EnumBlockTypePrismarineType prismarineType;

	public Spigot13BlockTypePrismarine(EnumBlockTypePrismarineType prismarineType) {
		super(168, "minecraft:prismarine", "minecraft:prismarine", 64);
		Validate.notNull(prismarineType, "Prismarine type cannot be null!");
		this.prismarineType = prismarineType;
	}

	@Override
	public String getNewStringId() {
		switch (prismarineType) {
			case DARK:
				return "minecraft:dark_prismarine";
			case BRICKS:
				return "minecraft:prismarine_bricks";
			case ROUGH:
			default:
				return "minecraft:prismarine";
		}
	}

	@Override
	public EnumBlockTypePrismarineType getPrismarineType() {
		return prismarineType;
	}

	@Override
	public void setPrismarineType(EnumBlockTypePrismarineType prismarineType) {
		Validate.notNull(prismarineType, "Prismarine type cannot be null!");
		this.prismarineType = prismarineType;
	}

	@Override
	public Spigot13BlockTypePrismarine clone() {
		return new Spigot13BlockTypePrismarine(prismarineType);
	}

	@Override
	public Spigot13BlockTypePrismarine readBlockData(BlockData blockData) {
		super.readBlockData(blockData);
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Spigot13BlockTypePrismarine that = (Spigot13BlockTypePrismarine) o;
		return prismarineType == that.prismarineType;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), prismarineType);
	}
}
