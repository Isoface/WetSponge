package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumAxis;
import com.degoos.wetsponge.enums.block.EnumBlockTypeQuartzType;
import com.degoos.wetsponge.material.block.Spigot13BlockTypeOrientable;
import com.degoos.wetsponge.util.Validate;
import org.bukkit.block.data.BlockData;

import java.util.Objects;
import java.util.Set;

public class Spigot13BlockTypeQuartz extends Spigot13BlockTypeOrientable implements WSBlockTypeQuartz {

	private EnumBlockTypeQuartzType quartzType;

	public Spigot13BlockTypeQuartz(EnumAxis axis, Set<EnumAxis> axes, EnumBlockTypeQuartzType quartzType) {
		super(155, "minecraft:quartz_block", "minecraft:quartz_block", 64, axis, axes);
		Validate.notNull(quartzType, "Quartz type cannot be null!");
		this.quartzType = quartzType;
	}

	@Override
	public String getNewStringId() {
		switch (quartzType) {
			case CHISELED:
				return "minecraft:chiseled_quartz_block";
			case PILLAR:
				return "minecraft:quartz_pillar";
			case NORMAL:
			default:
				return "minecraft:quartz_block";
		}
	}

	@Override
	public EnumBlockTypeQuartzType getQuartzType() {
		return quartzType;
	}

	@Override
	public void setQuartzType(EnumBlockTypeQuartzType quartzType) {
		Validate.notNull(quartzType, "Quartz type cannot be null!");
		this.quartzType = quartzType;
	}

	@Override
	public Spigot13BlockTypeQuartz clone() {
		return new Spigot13BlockTypeQuartz(getAxis(), getAxes(), quartzType);
	}

	@Override
	public Spigot13BlockTypeQuartz readBlockData(BlockData blockData) {
		super.readBlockData(blockData);
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Spigot13BlockTypeQuartz that = (Spigot13BlockTypeQuartz) o;
		return quartzType == that.quartzType;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), quartzType);
	}
}
