package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.material.block.Spigot13BlockTypeLightable;

public class Spigot13BlockTypeRedstoneLamp extends Spigot13BlockTypeLightable implements WSBlockTypeRedstoneLamp {


	public Spigot13BlockTypeRedstoneLamp(boolean lit) {
		super(123, "minecraft:redstone_lamp", "minecraft:redstone_lamp", 64, lit);
	}

	@Override
	public int getNumericalId() {
		return isLit() ? 124 : 123;
	}

	@Override
	public String getOldStringId() {
		return isLit() ? "minecraft:redstone_lamp" : "minecraft:lit_redstone_lamp";
	}

	@Override
	public Spigot13BlockTypeRedstoneLamp clone() {
		return new Spigot13BlockTypeRedstoneLamp(isLit());
	}
}
