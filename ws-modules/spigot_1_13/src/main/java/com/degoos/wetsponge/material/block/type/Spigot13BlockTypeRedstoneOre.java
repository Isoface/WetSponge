package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.material.block.Spigot13BlockTypeLightable;

public class Spigot13BlockTypeRedstoneOre extends Spigot13BlockTypeLightable implements WSBlockTypeRedstoneOre {


	public Spigot13BlockTypeRedstoneOre(boolean lit) {
		super(73, "minecraft:redstone_ore", "minecraft:redstone_ore", 64, lit);
	}

	@Override
	public String getOldStringId() {
		return isLit() ? "minecraft:lit_redstone_ore" : "minecraft:redstone_ore";
	}

	@Override
	public Spigot13BlockTypeRedstoneOre clone() {
		return new Spigot13BlockTypeRedstoneOre(isLit());
	}
}
