package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockTypeRailShape;
import com.degoos.wetsponge.material.block.Spigot13BlockTypeRail;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.type.RedstoneRail;

import java.util.Objects;
import java.util.Set;

public class Spigot13BlockTypeRedstoneRail extends Spigot13BlockTypeRail implements WSBlockTypeRedstoneRail {

	private boolean powered;

	public Spigot13BlockTypeRedstoneRail(int numericalId, String oldStringId, String newStringId, int maxStackSize,
										 EnumBlockTypeRailShape shape, Set<EnumBlockTypeRailShape> allowedShapes, boolean powered) {
		super(numericalId, oldStringId, newStringId, maxStackSize, shape, allowedShapes);
		this.powered = powered;
	}

	@Override
	public boolean isPowered() {
		return powered;
	}

	@Override
	public void setPowered(boolean powered) {
		this.powered = powered;
	}

	@Override
	public Spigot13BlockTypeRedstoneRail clone() {
		return new Spigot13BlockTypeRedstoneRail(getNumericalId(), getOldStringId(), getNewStringId(), getMaxStackSize(), getShape(), allowedShapes(), powered);
	}

	@Override
	public BlockData toBlockData() {
		BlockData blockData = super.toBlockData();
		if (blockData instanceof RedstoneRail) {
			((RedstoneRail) blockData).setPowered(powered);
		}
		return blockData;
	}

	@Override
	public Spigot13BlockTypeRedstoneRail readBlockData(BlockData blockData) {
		super.readBlockData(blockData);
		if (blockData instanceof RedstoneRail) {
			powered = ((RedstoneRail) blockData).isPowered();
		}
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Spigot13BlockTypeRedstoneRail that = (Spigot13BlockTypeRedstoneRail) o;
		return powered == that.powered;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), powered);
	}
}
