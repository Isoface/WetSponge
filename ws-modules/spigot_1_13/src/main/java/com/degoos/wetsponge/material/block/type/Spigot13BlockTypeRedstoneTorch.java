package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.Lightable;

import java.util.Objects;
import java.util.Set;

public class Spigot13BlockTypeRedstoneTorch extends Spigot13BlockTypeTorch implements WSBlockTypeRedstoneTorch {

	private boolean lit;

	public Spigot13BlockTypeRedstoneTorch(EnumBlockFace facing, Set<EnumBlockFace> faces, boolean lit) {
		super(75, "minecraft:redstone_torch", "minecraft:redstone_torch", 64, "minecraft:redstone_wall_torch", facing, faces);
		this.lit = lit;
	}

	@Override
	public int getNumericalId() {
		return lit ? 76 : 75;
	}

	@Override
	public String getOldStringId() {
		return lit ? "minecraft:redstone_torch" : "minecraft:unlit_redstone_torch";
	}


	@Override
	public boolean isLit() {
		return lit;
	}

	@Override
	public void setLit(boolean lit) {
		this.lit = lit;
	}

	@Override
	public Spigot13BlockTypeRedstoneTorch clone() {
		return new Spigot13BlockTypeRedstoneTorch(getFacing(), getFaces(), lit);
	}

	@Override
	public BlockData toBlockData() {
		BlockData blockData = super.toBlockData();
		if (blockData instanceof Lightable) {
			((Lightable) blockData).setLit(lit);
		}
		return blockData;
	}

	@Override
	public Spigot13BlockTypeRedstoneTorch readBlockData(BlockData blockData) {
		super.readBlockData(blockData);
		if (blockData instanceof Lightable) {
			lit = ((Lightable) blockData).isLit();
		}
		return this;
	}


	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Spigot13BlockTypeRedstoneTorch that = (Spigot13BlockTypeRedstoneTorch) o;
		return lit == that.lit;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), lit);
	}
}
