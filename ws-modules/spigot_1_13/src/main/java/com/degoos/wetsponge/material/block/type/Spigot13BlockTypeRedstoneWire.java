package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.enums.block.EnumBlockTypeRedstoneWireConnection;
import com.degoos.wetsponge.material.block.Spigot13BlockTypeAnaloguePowerable;
import com.degoos.wetsponge.util.Validate;
import org.bukkit.block.BlockFace;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.type.RedstoneWire;

import java.util.*;
import java.util.stream.Collectors;

public class Spigot13BlockTypeRedstoneWire extends Spigot13BlockTypeAnaloguePowerable implements WSBlockTypeRedstoneWire {

	private Map<EnumBlockFace, EnumBlockTypeRedstoneWireConnection> connections;
	private Set<EnumBlockFace> allowedFaces;

	public Spigot13BlockTypeRedstoneWire(int power, int maximumPower, Map<EnumBlockFace, EnumBlockTypeRedstoneWireConnection> connections, Set<EnumBlockFace> allowedFaces) {
		super(55, "minecraft:redstone_wire", "minecraft:redstone_wire", 64, power, maximumPower);
		this.connections = connections == null ? new HashMap<>() : connections;
		this.allowedFaces = allowedFaces == null ? new HashSet<>() : allowedFaces;
	}

	@Override
	public EnumBlockTypeRedstoneWireConnection getFaceConnection(EnumBlockFace face) {
		Validate.notNull(face, "Face cannot be null!");
		return connections.getOrDefault(face, EnumBlockTypeRedstoneWireConnection.NONE);
	}

	@Override
	public void setFaceConnection(EnumBlockFace face, EnumBlockTypeRedstoneWireConnection connection) {
		Validate.notNull(face, "Face cannot be null!");
		connections.put(face, connection == null ? EnumBlockTypeRedstoneWireConnection.NONE : connection);
	}

	@Override
	public Set<EnumBlockFace> getAllowedFaces() {
		return allowedFaces;
	}

	@Override
	public Spigot13BlockTypeRedstoneWire clone() {
		return new Spigot13BlockTypeRedstoneWire(getPower(), gerMaximumPower(), connections, allowedFaces);
	}

	@Override
	public BlockData toBlockData() {
		BlockData blockData = super.toBlockData();
		if (blockData instanceof RedstoneWire) {
			((RedstoneWire) blockData).getAllowedFaces().forEach(target -> ((RedstoneWire) blockData).setFace(target, RedstoneWire.Connection.NONE));
			connections.forEach((face, connection) -> ((RedstoneWire) blockData)
					.setFace(BlockFace.valueOf(face.name()), RedstoneWire.Connection.valueOf(connection.name())));
		}
		return blockData;
	}

	@Override
	public Spigot13BlockTypeRedstoneWire readBlockData(BlockData blockData) {
		super.readBlockData(blockData);
		if (blockData instanceof RedstoneWire) {
			allowedFaces = ((RedstoneWire) blockData).getAllowedFaces().stream().map(target -> EnumBlockFace.valueOf(target.name())).collect(Collectors.toSet());
			connections = new HashMap<>();
			((RedstoneWire) blockData).getAllowedFaces().forEach(target -> connections.put(EnumBlockFace.valueOf(target.name()),
					EnumBlockTypeRedstoneWireConnection.valueOf(((RedstoneWire) blockData).getFace(target).name())));
		}
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Spigot13BlockTypeRedstoneWire that = (Spigot13BlockTypeRedstoneWire) o;
		return Objects.equals(connections, that.connections) &&
				Objects.equals(allowedFaces, that.allowedFaces);
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), connections, allowedFaces);
	}
}
