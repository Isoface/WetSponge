package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockTypeSandType;
import com.degoos.wetsponge.enums.block.EnumBlockTypeSandstoneType;
import com.degoos.wetsponge.material.block.Spigot13BlockType;
import com.degoos.wetsponge.util.Validate;
import org.bukkit.block.data.BlockData;

import java.util.Objects;

public class Spigot13BlockTypeSandstone extends Spigot13BlockType implements WSBlockTypeSandstone {

	private EnumBlockTypeSandType sandType;
	private EnumBlockTypeSandstoneType sandstoneType;

	public Spigot13BlockTypeSandstone(EnumBlockTypeSandType sandType, EnumBlockTypeSandstoneType sandstoneType) {
		super(24, "minecraft:sandstone", "minecraft:sandstone", 64);
		Validate.notNull(sandType, "Sand type cannot be null!");
		Validate.notNull(sandstoneType, "Sandstone type cannot be null!");
		this.sandType = sandType;
		this.sandstoneType = sandstoneType;
	}

	@Override
	public int getNumericalId() {
		return sandType == EnumBlockTypeSandType.NORMAL ? 24 : 179;
	}

	@Override
	public String getOldStringId() {
		return sandType == EnumBlockTypeSandType.RED ? "minecraft:red_sandstone" : "minecraft:sandstone";
	}

	@Override
	public String getNewStringId() {
		String prefix = sandstoneType == EnumBlockTypeSandstoneType.DEFAULT ? "" : sandstoneType == EnumBlockTypeSandstoneType.SMOOTH ? "cut_" : "chiseled_";
		return prefix + (sandType == EnumBlockTypeSandType.RED ? "red_sandstone" : "sandstone");
	}

	@Override
	public EnumBlockTypeSandType getSandType() {
		return sandType;
	}

	@Override
	public void setSandType(EnumBlockTypeSandType sandType) {
		Validate.notNull(sandType, "Sand type cannot be null!");
		this.sandType = sandType;
	}

	@Override
	public EnumBlockTypeSandstoneType getSandstoneType() {
		return sandstoneType;
	}

	@Override
	public void setSandstoneType(EnumBlockTypeSandstoneType sandstoneType) {
		Validate.notNull(sandstoneType, "Sandstone type cannot be null!");
		this.sandstoneType = sandstoneType;
	}

	@Override
	public Spigot13BlockTypeSandstone clone() {
		return new Spigot13BlockTypeSandstone(sandType, sandstoneType);
	}

	@Override
	public Spigot13BlockTypeSandstone readBlockData(BlockData blockData) {
		super.readBlockData(blockData);
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Spigot13BlockTypeSandstone that = (Spigot13BlockTypeSandstone) o;
		return sandType == that.sandType &&
				sandstoneType == that.sandstoneType;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), sandType, sandstoneType);
	}
}
