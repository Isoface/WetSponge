package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumWoodType;
import com.degoos.wetsponge.material.block.Spigot13BlockType;
import com.degoos.wetsponge.util.Validate;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.type.Sapling;

import java.util.Objects;

public class Spigot13BlockTypeSapling extends Spigot13BlockType implements WSBlockTypeSapling {

	private EnumWoodType woodType;
	private int stage, maximumStage;

	public Spigot13BlockTypeSapling(EnumWoodType woodType, int stage, int maximumStage) {
		super(6, "minecraft:sapling", "minecraft:sapling", 64);
		Validate.notNull(woodType, "Wood type cannot be null!");
		this.woodType = woodType;
		this.stage = stage;
		this.maximumStage = maximumStage;
	}

	@Override
	public String getNewStringId() {
		switch (getWoodType()) {
			case SPRUCE:
				return "minecraft:spruce_sapling";
			case BIRCH:
				return "minecraft:birch_sapling";
			case JUNGLE:
				return "minecraft:jungle_sapling";
			case ACACIA:
				return "minecraft:acacia_sapling";
			case DARK_OAK:
				return "minecraft:dark_oak_sapling";
			case OAK:
			default:
				return "minecraft:oak_sapling";
		}
	}

	@Override
	public EnumWoodType getWoodType() {
		return woodType;
	}

	@Override
	public void setWoodType(EnumWoodType woodType) {
		Validate.notNull(woodType, "Wood type cannot be null!");
		this.woodType = woodType;
	}

	@Override
	public int getStage() {
		return stage;
	}

	@Override
	public void setStage(int stage) {
		this.stage = Math.min(maximumStage, Math.max(0, stage));
	}

	@Override
	public int getMaximumStage() {
		return maximumStage;
	}

	@Override
	public Spigot13BlockTypeSapling clone() {
		return new Spigot13BlockTypeSapling(woodType, stage, maximumStage);
	}

	@Override
	public BlockData toBlockData() {
		BlockData blockData = super.toBlockData();
		if (blockData instanceof Sapling) {
			((Sapling) blockData).setStage(stage);
		}
		return blockData;
	}

	@Override
	public Spigot13BlockTypeSapling readBlockData(BlockData blockData) {
		super.readBlockData(blockData);
		if (blockData instanceof Sapling) {
			stage = ((Sapling) blockData).getStage();
			maximumStage = ((Sapling) blockData).getMaximumStage();
		}
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Spigot13BlockTypeSapling that = (Spigot13BlockTypeSapling) o;
		return stage == that.stage &&
				maximumStage == that.maximumStage &&
				woodType == that.woodType;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), woodType, stage, maximumStage);
	}
}
