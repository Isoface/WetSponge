package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.material.block.Spigot13BlockTypeWaterlogged;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.type.SeaPickle;

import java.util.Objects;

public class Spigot13BlockTypeSeaPickle extends Spigot13BlockTypeWaterlogged implements WSBlockTypeSeaPickle {

	private int pickles, minimumPickles, maximumPickles;

	public Spigot13BlockTypeSeaPickle(boolean waterLogged, int pickles, int minimumPickles, int maximumPickles) {
		super(-1, null, "minecraft:sea_pickle", 64, waterLogged);
		this.pickles = pickles;
		this.minimumPickles = minimumPickles;
		this.maximumPickles = maximumPickles;
	}

	@Override
	public int getPickles() {
		return pickles;
	}

	@Override
	public void setPickles(int pickles) {
		this.pickles = Math.min(maximumPickles, Math.max(minimumPickles, pickles));
	}

	@Override
	public int getMinimumPickles() {
		return minimumPickles;
	}

	@Override
	public int getMaximumPickles() {
		return maximumPickles;
	}

	@Override
	public Spigot13BlockTypeSeaPickle clone() {
		return new Spigot13BlockTypeSeaPickle(isWaterlogged(), pickles, minimumPickles, maximumPickles);
	}

	@Override
	public BlockData toBlockData() {
		BlockData blockData = super.toBlockData();
		if (blockData instanceof SeaPickle) {
			((SeaPickle) blockData).setPickles(pickles);
		}
		return blockData;
	}

	@Override
	public Spigot13BlockTypeSeaPickle readBlockData(BlockData blockData) {
		super.readBlockData(blockData);
		if (blockData instanceof SeaPickle) {
			pickles = ((SeaPickle) blockData).getPickles();
			minimumPickles = ((SeaPickle) blockData).getMinimumPickles();
			maximumPickles = ((SeaPickle) blockData).getMaximumPickles();
		}
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Spigot13BlockTypeSeaPickle that = (Spigot13BlockTypeSeaPickle) o;
		return pickles == that.pickles &&
				minimumPickles == that.minimumPickles &&
				maximumPickles == that.maximumPickles;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), pickles, minimumPickles, maximumPickles);
	}
}
