package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.material.block.Spigot13BlockTypeRotatable;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.type.Sign;

import java.util.Objects;

public class Spigot13BlockTypeSign extends Spigot13BlockTypeRotatable implements WSBlockTypeSign {

	private boolean waterlogged;

	public Spigot13BlockTypeSign(EnumBlockFace rotation, boolean waterlogged) {
		super(63, "minecraft:standing_sign", "minecraft:sign", 64, rotation);
		this.waterlogged = waterlogged;
	}

	@Override
	public boolean isWaterlogged() {
		return waterlogged;
	}

	@Override
	public void setWaterlogged(boolean waterlogged) {
		this.waterlogged = waterlogged;
	}

	@Override
	public Spigot13BlockTypeSign clone() {
		return new Spigot13BlockTypeSign(getRotation(), waterlogged);
	}

	@Override
	public BlockData toBlockData() {
		BlockData blockData = super.toBlockData();
		if (blockData instanceof Sign) {
			((Sign) blockData).setWaterlogged(waterlogged);
		}
		return blockData;
	}

	@Override
	public Spigot13BlockTypeSign readBlockData(BlockData blockData) {
		super.readBlockData(blockData);
		if (blockData instanceof Sign) {
			waterlogged = ((Sign) blockData).isWaterlogged();
		}
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Spigot13BlockTypeSign that = (Spigot13BlockTypeSign) o;
		return waterlogged == that.waterlogged;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), waterlogged);
	}
}
