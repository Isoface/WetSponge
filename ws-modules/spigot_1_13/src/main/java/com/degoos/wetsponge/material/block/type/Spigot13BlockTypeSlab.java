package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockTypeSlabPosition;
import com.degoos.wetsponge.enums.block.EnumBlockTypeSlabType;
import com.degoos.wetsponge.material.block.Spigot13BlockTypeWaterlogged;
import com.degoos.wetsponge.util.Validate;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.type.Slab;

import java.util.Objects;

public class Spigot13BlockTypeSlab extends Spigot13BlockTypeWaterlogged implements WSBlockTypeSlab {

	private EnumBlockTypeSlabType type;
	private EnumBlockTypeSlabPosition position;

	public Spigot13BlockTypeSlab(boolean waterLogged, EnumBlockTypeSlabType type, EnumBlockTypeSlabPosition position) {
		super(43, "minecraft:double_stone_slab", "minecraft:stone_slab", 64, waterLogged);
		Validate.notNull(type, "Type cannot be null!");
		Validate.notNull(position, "Position cannot be null!");
		this.type = type;
		this.position = position;
	}


	@Override
	public int getNumericalId() {
		switch (type) {
			case OAK:
			case SPRUCE:
			case BIRCH:
			case JUNGLE:
			case ACACIA:
			case DARK_OAK:
				return position == EnumBlockTypeSlabPosition.DOUBLE ? 125 : 126;
			case STONE:
			case RED_SANDSTONE:
				return position == EnumBlockTypeSlabPosition.DOUBLE ? 181 : 182;
			case PURPUR:
				return position == EnumBlockTypeSlabPosition.DOUBLE ? 204 : 205;
			default:
				return position == EnumBlockTypeSlabPosition.DOUBLE ? 43 : 44;
		}
	}

	@Override
	public String getOldStringId() {
		switch (type) {
			case OAK:
			case SPRUCE:
			case BIRCH:
			case JUNGLE:
			case ACACIA:
			case DARK_OAK:
				return position == EnumBlockTypeSlabPosition.DOUBLE ? "minecraft:double_wooden_slab" : "minecraft:wooden_slab";
			case STONE:
			case RED_SANDSTONE:
				return position == EnumBlockTypeSlabPosition.DOUBLE ? "minecraft:double_stone_slab" : "minecraft:stone_slab";
			case PURPUR:
				return position == EnumBlockTypeSlabPosition.DOUBLE ? "minecraft:double_purpur_slab" : "minecraft:purpur_slab";
			default:
				return position == EnumBlockTypeSlabPosition.DOUBLE ? "minecraft:double_stone_slab2" : "minecraft:stone_slab2";
		}
	}

	@Override
	public String getNewStringId() {
		return type.name().toLowerCase() + "_slab";
	}


	@Override
	public EnumBlockTypeSlabType getType() {
		return type;
	}

	@Override
	public void setType(EnumBlockTypeSlabType type) {
		Validate.notNull(type, "Type cannot be null!");
		this.type = type;
	}

	@Override
	public EnumBlockTypeSlabPosition getPosition() {
		return position;
	}

	@Override
	public void setPosition(EnumBlockTypeSlabPosition position) {
		Validate.notNull(position, "Position cannot be null!");
		this.position = position;
	}

	@Override
	public Spigot13BlockTypeSlab clone() {
		return new Spigot13BlockTypeSlab(isWaterlogged(), type, position);
	}

	@Override
	public BlockData toBlockData() {
		BlockData blockData = super.toBlockData();
		if (blockData instanceof Slab) {
			((Slab) blockData).setType(Slab.Type.valueOf(position.name()));
		}
		return blockData;
	}

	@Override
	public Spigot13BlockTypeSlab readBlockData(BlockData blockData) {
		super.readBlockData(blockData);
		if (blockData instanceof Slab) {
			position = EnumBlockTypeSlabPosition.valueOf(((Slab) blockData).getType().name());
		}
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Spigot13BlockTypeSlab that = (Spigot13BlockTypeSlab) o;
		return type == that.type &&
				position == that.position;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), type, position);
	}
}
