package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.material.block.Spigot13BlockType;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.type.Snow;

import java.util.Objects;

public class Spigot13BlockTypeSnow extends Spigot13BlockType implements WSBlockTypeSnow {

	private int layers, minimumLayers, maximumLayers;

	public Spigot13BlockTypeSnow(int layers, int minimumLayers, int maximumLayers) {
		super(78, "minecraft:snow_layer", "minecraft:snow", 64);
		this.layers = layers;
		this.minimumLayers = minimumLayers;
		this.maximumLayers = maximumLayers;
	}

	@Override
	public int getLayers() {
		return layers;
	}

	@Override
	public void setLayers(int layers) {
		layers = Math.max(minimumLayers, Math.min(maximumLayers, layers));
	}

	@Override
	public int getMinimumLayers() {
		return minimumLayers;
	}

	@Override
	public int getMaximumLayers() {
		return maximumLayers;
	}

	@Override
	public Spigot13BlockTypeSnow clone() {
		return new Spigot13BlockTypeSnow(layers, minimumLayers, maximumLayers);
	}

	@Override
	public BlockData toBlockData() {
		BlockData blockData = super.toBlockData();
		if (blockData instanceof Snow) {
			((Snow) blockData).setLayers(layers);
		}
		return blockData;
	}

	@Override
	public Spigot13BlockTypeSnow readBlockData(BlockData blockData) {
		super.readBlockData(blockData);
		if (blockData instanceof Snow) {
			layers = ((Snow) blockData).getLayers();
			minimumLayers = ((Snow) blockData).getMinimumLayers();
			maximumLayers = ((Snow) blockData).getMaximumLayers();
		}
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Spigot13BlockTypeSnow that = (Spigot13BlockTypeSnow) o;
		return layers == that.layers &&
				minimumLayers == that.minimumLayers &&
				maximumLayers == that.maximumLayers;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), layers, minimumLayers, maximumLayers);
	}
}
