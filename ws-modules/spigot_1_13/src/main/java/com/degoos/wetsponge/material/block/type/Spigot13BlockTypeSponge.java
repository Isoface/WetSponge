package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.material.block.Spigot13BlockType;
import org.bukkit.block.data.BlockData;

import java.util.Objects;

public class Spigot13BlockTypeSponge extends Spigot13BlockType implements WSBlockTypeSponge {

	private boolean wet;

	public Spigot13BlockTypeSponge(boolean wet) {
		super(19, "minecraft:sponge", "minecraft:sponge", 64);
		this.wet = wet;
	}

	@Override
	public String getNewStringId() {
		return wet ? "minecraft:wet_sponge" : "minecraft:sponge";
	}

	@Override
	public boolean isWet() {
		return wet;
	}

	@Override
	public void setWet(boolean wet) {
		this.wet = wet;
	}

	@Override
	public Spigot13BlockTypeSponge clone() {
		return new Spigot13BlockTypeSponge(wet);
	}

	@Override
	public Spigot13BlockTypeSponge readBlockData(BlockData blockData) {
		super.readBlockData(blockData);
		return this;
	}


	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Spigot13BlockTypeSponge that = (Spigot13BlockTypeSponge) o;
		return wet == that.wet;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), wet);
	}
}
