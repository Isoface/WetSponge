package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.EnumDyeColor;
import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.util.Validate;

import java.util.Objects;
import java.util.Set;

public class Spigot13BlockTypeStainedGlassPane extends Spigot13BlockTypeGlassPane implements WSBlockTypeStainedGlassPane {

	private EnumDyeColor dyeColor;

	public Spigot13BlockTypeStainedGlassPane(Set<EnumBlockFace> faces, Set<EnumBlockFace> allowedFaces, boolean waterlogged, EnumDyeColor dyeColor) {
		super(160, "minecraft:stained_glass_pane", "stained_glass_pane", 64, faces, allowedFaces, waterlogged);
		Validate.notNull(dyeColor, "Dye color cannot be null!");
		this.dyeColor = dyeColor;
	}

	@Override
	public String getNewStringId() {
		return "minecraft:" + dyeColor.getMinecraftName().toLowerCase() + "_" + super.getNewStringId();
	}

	@Override
	public EnumDyeColor getDyeColor() {
		return dyeColor;
	}

	@Override
	public void setDyeColor(EnumDyeColor dyeColor) {
		Validate.notNull(dyeColor, "Dye color cannot be null!");
		this.dyeColor = dyeColor;
	}


	@Override
	public Spigot13BlockTypeStainedGlassPane clone() {
		return new Spigot13BlockTypeStainedGlassPane(getFaces(), getAllowedFaces(), isWaterlogged(), dyeColor);
	}


	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Spigot13BlockTypeStainedGlassPane that = (Spigot13BlockTypeStainedGlassPane) o;
		return dyeColor == that.dyeColor;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), dyeColor);
	}
}
