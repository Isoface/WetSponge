package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.enums.block.EnumBlockTypeBisectedHalf;
import com.degoos.wetsponge.enums.block.EnumBlockTypeStairShape;
import com.degoos.wetsponge.material.block.Spigot13BlockTypeDirectional;
import com.degoos.wetsponge.util.Validate;
import org.bukkit.block.data.Bisected;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.type.Stairs;

import java.util.Objects;
import java.util.Set;

public class Spigot13BlockTypeStairs extends Spigot13BlockTypeDirectional implements WSBlockTypeStairs {


	private EnumBlockTypeStairShape shape;
	private EnumBlockTypeBisectedHalf half;
	private boolean waterlogged;

	public Spigot13BlockTypeStairs(int numericalId, String oldStringId, String newStringId, int maxStackSize, EnumBlockFace facing, Set<EnumBlockFace> faces,
								   EnumBlockTypeStairShape shape, EnumBlockTypeBisectedHalf half, boolean waterlogged) {
		super(numericalId, oldStringId, newStringId, maxStackSize, facing, faces);
		Validate.notNull(shape, "Shape cannot be null!");
		Validate.notNull(half, "Half cannot be null!");
		this.shape = shape;
		this.half = half;
		this.waterlogged = waterlogged;
	}

	@Override
	public EnumBlockTypeStairShape getShape() {
		return shape;
	}

	@Override
	public void setShape(EnumBlockTypeStairShape shape) {
		Validate.notNull(shape, "Shape cannot be null!");
		this.shape = shape;
	}

	@Override
	public EnumBlockTypeBisectedHalf getHalf() {
		return half;
	}

	@Override
	public void setHalf(EnumBlockTypeBisectedHalf half) {
		Validate.notNull(half, "Half cannot be null!");
		this.half = half;
	}

	@Override
	public boolean isWaterlogged() {
		return waterlogged;
	}

	@Override
	public void setWaterlogged(boolean waterlogged) {
		this.waterlogged = waterlogged;
	}

	@Override
	public Spigot13BlockTypeStairs clone() {
		return new Spigot13BlockTypeStairs(getNumericalId(), getOldStringId(), getNewStringId(), getMaxStackSize(), getFacing(), getFaces(), shape, half, waterlogged);
	}

	@Override
	public BlockData toBlockData() {
		BlockData blockData = super.toBlockData();
		if (blockData instanceof Stairs) {
			((Stairs) blockData).setWaterlogged(waterlogged);
			((Stairs) blockData).setHalf(Bisected.Half.valueOf(half.name()));
			((Stairs) blockData).setShape(Stairs.Shape.valueOf(shape.name()));
		}
		return blockData;
	}

	@Override
	public Spigot13BlockTypeStairs readBlockData(BlockData blockData) {
		super.readBlockData(blockData);
		if (blockData instanceof Stairs) {
			waterlogged = ((Stairs) blockData).isWaterlogged();
			half = EnumBlockTypeBisectedHalf.valueOf(((Stairs) blockData).getHalf().name());
			shape = EnumBlockTypeStairShape.valueOf(((Stairs) blockData).getShape().name());
		}
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Spigot13BlockTypeStairs that = (Spigot13BlockTypeStairs) o;
		return waterlogged == that.waterlogged &&
				shape == that.shape &&
				half == that.half;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), shape, half, waterlogged);
	}
}
