package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockTypeStoneBrickType;
import com.degoos.wetsponge.material.block.Spigot13BlockType;
import com.degoos.wetsponge.util.Validate;
import org.bukkit.block.data.BlockData;

import java.util.Objects;

public class Spigot13BlockTypeStoneBrick extends Spigot13BlockType implements WSBlockTypeStoneBrick {

	private EnumBlockTypeStoneBrickType stoneBrickType;

	public Spigot13BlockTypeStoneBrick(EnumBlockTypeStoneBrickType stoneBrickType) {
		super(98, "minecraft:stonebrick", "minecraft:stone_bricks", 64);
		Validate.notNull(stoneBrickType, "Stone brick type cannot be null!");
		this.stoneBrickType = stoneBrickType;
	}

	@Override
	public String getNewStringId() {
		switch (stoneBrickType) {
			case MOSSY:
				return "minecraft:mossy_stone_bricks";
			case CRACKED:
				return "minecraft:cracked_stone_bricks";
			case CHISELED:
				return "minecraft:chiseled_stone_bricks";
			case DEFAULT:
			default:
				return "minecraft:stone_bricks";
		}
	}

	@Override
	public EnumBlockTypeStoneBrickType getStoneBrickType() {
		return stoneBrickType;
	}

	@Override
	public void setStoneBrickType(EnumBlockTypeStoneBrickType stoneBrickType) {
		Validate.notNull(stoneBrickType, "Stone brick type cannot be null!");
		this.stoneBrickType = stoneBrickType;
	}

	@Override
	public Spigot13BlockTypeStoneBrick clone() {
		return new Spigot13BlockTypeStoneBrick(stoneBrickType);
	}

	@Override
	public Spigot13BlockTypeStoneBrick readBlockData(BlockData blockData) {
		super.readBlockData(blockData);
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Spigot13BlockTypeStoneBrick that = (Spigot13BlockTypeStoneBrick) o;
		return stoneBrickType == that.stoneBrickType;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), stoneBrickType);
	}
}
