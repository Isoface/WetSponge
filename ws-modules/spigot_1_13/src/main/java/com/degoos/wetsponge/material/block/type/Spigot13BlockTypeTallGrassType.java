package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockTypeTallGrassType;
import com.degoos.wetsponge.material.block.Spigot13BlockType;
import com.degoos.wetsponge.util.Validate;
import org.bukkit.block.data.BlockData;

import java.util.Objects;

public class Spigot13BlockTypeTallGrassType extends Spigot13BlockType implements WSBlockTypeTallGrass {

	private EnumBlockTypeTallGrassType tallGrassType;

	public Spigot13BlockTypeTallGrassType(EnumBlockTypeTallGrassType tallGrassType) {
		super(31, "minecraft:tallgrass", "minecraft:grass", 64);
		Validate.notNull(tallGrassType, "Tall grass type cannot be null!");
		this.tallGrassType = tallGrassType;
	}

	@Override
	public String getNewStringId() {
		switch (tallGrassType) {
			case FERN:
				return "minecraft:fern";
			case DEAD_BUSH:
				return "minecraft:dead_bush";
			case TALL_GRASS:
			default:
				return "minecraft:grass";
		}
	}

	@Override
	public EnumBlockTypeTallGrassType getTallGrassType() {
		return tallGrassType;
	}

	@Override
	public void setTallGrassType(EnumBlockTypeTallGrassType tallGrassType) {
		Validate.notNull(tallGrassType, "Tall grass type cannot be null!");
		this.tallGrassType = tallGrassType;
	}

	@Override
	public Spigot13BlockTypeTallGrassType clone() {
		return new Spigot13BlockTypeTallGrassType(tallGrassType);
	}

	@Override
	public Spigot13BlockTypeTallGrassType readBlockData(BlockData blockData) {
		super.readBlockData(blockData);
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Spigot13BlockTypeTallGrassType that = (Spigot13BlockTypeTallGrassType) o;
		return tallGrassType == that.tallGrassType;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), tallGrassType);
	}
}
