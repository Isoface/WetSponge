package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.material.block.Spigot13BlockTypeDirectional;
import org.bukkit.block.data.BlockData;

import java.util.Objects;
import java.util.Set;

public class Spigot13BlockTypeTorch extends Spigot13BlockTypeDirectional implements WSBlockTypeTorch {

	private String wallNewStringId;

	public Spigot13BlockTypeTorch(int numericalId, String oldStringId, String newStringId,
								  int maxStackSize, String wallNewStringId, EnumBlockFace facing, Set<EnumBlockFace> faces) {
		super(numericalId, oldStringId, newStringId, maxStackSize, facing, faces);
		this.wallNewStringId = wallNewStringId;
	}

	@Override
	public String getNewStringId() {
		return getFacing() == EnumBlockFace.UP ? wallNewStringId : super.getNewStringId();
	}

	@Override
	public Set<EnumBlockFace> getFaces() {
		Set<EnumBlockFace> faces = super.getFaces();
		faces.add(EnumBlockFace.UP);
		return faces;
	}

	@Override
	public Spigot13BlockTypeTorch clone() {
		return new Spigot13BlockTypeTorch(getNumericalId(), getOldStringId(), getNewStringId(), getMaxStackSize(), wallNewStringId, getFacing(), getFaces());
	}

	@Override
	public Spigot13BlockTypeTorch readBlockData(BlockData blockData) {
		super.readBlockData(blockData);
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Spigot13BlockTypeTorch that = (Spigot13BlockTypeTorch) o;
		return Objects.equals(wallNewStringId, that.wallNewStringId);
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), wallNewStringId);
	}
}
