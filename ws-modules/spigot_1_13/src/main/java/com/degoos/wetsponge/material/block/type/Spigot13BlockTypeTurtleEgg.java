package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.material.block.Spigot13BlockType;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.type.TurtleEgg;

import java.util.Objects;

public class Spigot13BlockTypeTurtleEgg extends Spigot13BlockType implements WSBlockTypeTurtleEgg {

	private int eggs, minimumEggs, maximumEggs, hatch, maximumHatch;

	public Spigot13BlockTypeTurtleEgg(int eggs, int minimumEggs, int maximumEggs, int hatch, int maximumHatch) {
		super(-1, null, "minecraft:turtle_egg", 64);
		this.eggs = eggs;
		this.minimumEggs = minimumEggs;
		this.maximumEggs = maximumEggs;
		this.hatch = hatch;
		this.maximumHatch = maximumHatch;
	}

	@Override
	public int getEggs() {
		return eggs;
	}

	@Override
	public void setEggs(int eggs) {
		this.eggs = Math.max(minimumEggs, Math.min(maximumEggs, eggs));
	}

	@Override
	public int getMinimumEggs() {
		return minimumEggs;
	}

	@Override
	public int getMaximumEggs() {
		return maximumEggs;
	}

	@Override
	public int getHatch() {
		return hatch;
	}

	@Override
	public void setHatch(int hatch) {
		this.hatch = Math.max(0, Math.min(maximumHatch, hatch));
	}

	@Override
	public int getMaximumHatch() {
		return maximumHatch;
	}

	@Override
	public Spigot13BlockTypeTurtleEgg clone() {
		return new Spigot13BlockTypeTurtleEgg(eggs, minimumEggs, maximumEggs, hatch, maximumHatch);
	}

	@Override
	public BlockData toBlockData() {
		BlockData blockData = super.toBlockData();
		if (blockData instanceof TurtleEgg) {
			((TurtleEgg) blockData).setEggs(eggs);
			((TurtleEgg) blockData).setHatch(hatch);
		}
		return blockData;
	}

	@Override
	public Spigot13BlockTypeTurtleEgg readBlockData(BlockData blockData) {
		super.readBlockData(blockData);
		if (blockData instanceof TurtleEgg) {
			eggs = ((TurtleEgg) blockData).getEggs();
			minimumEggs = ((TurtleEgg) blockData).getMinimumEggs();
			maximumHatch = ((TurtleEgg) blockData).getMaximumHatch();
			hatch = ((TurtleEgg) blockData).getHatch();
			maximumHatch = ((TurtleEgg) blockData).getMaximumHatch();
		}
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Spigot13BlockTypeTurtleEgg that = (Spigot13BlockTypeTurtleEgg) o;
		return eggs == that.eggs &&
				minimumEggs == that.minimumEggs &&
				maximumEggs == that.maximumEggs &&
				hatch == that.hatch &&
				maximumHatch == that.maximumHatch;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), eggs, minimumEggs, maximumEggs, hatch, maximumHatch);
	}
}
