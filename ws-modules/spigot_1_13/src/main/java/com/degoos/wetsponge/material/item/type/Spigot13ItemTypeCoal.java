package com.degoos.wetsponge.material.item.type;

import com.degoos.wetsponge.enums.item.EnumItemTypeCoalType;
import com.degoos.wetsponge.material.item.Spigot13ItemType;
import com.degoos.wetsponge.util.Validate;

import java.util.Objects;

public class Spigot13ItemTypeCoal extends Spigot13ItemType implements WSItemTypeCoal {

	private EnumItemTypeCoalType coalType;

	public Spigot13ItemTypeCoal(EnumItemTypeCoalType coalType) {
		super(263, "minecraft:coal", "minecraft:coal", 64);
		Validate.notNull(coalType, "Coal type cannot be null!");
		this.coalType = coalType;
	}

	@Override
	public String getNewStringId() {
		return coalType == EnumItemTypeCoalType.COAL ? "minecraft:coal" : "minecraft:charcoal";
	}

	@Override
	public EnumItemTypeCoalType getCoalType() {
		return coalType;
	}

	@Override
	public void setCoalType(EnumItemTypeCoalType coalType) {
		Validate.notNull(coalType, "Coal type cannot be null!");
		this.coalType = coalType;
	}

	@Override
	public Spigot13ItemTypeCoal clone() {
		return new Spigot13ItemTypeCoal(coalType);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Spigot13ItemTypeCoal that = (Spigot13ItemTypeCoal) o;
		return coalType == that.coalType;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), coalType);
	}
}
