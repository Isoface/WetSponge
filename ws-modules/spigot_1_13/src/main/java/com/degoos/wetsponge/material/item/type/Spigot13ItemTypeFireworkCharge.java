package com.degoos.wetsponge.material.item.type;

import com.degoos.wetsponge.firework.Spigot13FireworkEffect;
import com.degoos.wetsponge.firework.WSFireworkEffect;
import com.degoos.wetsponge.material.item.Spigot13ItemType;
import org.bukkit.FireworkEffect;
import org.bukkit.inventory.meta.FireworkEffectMeta;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Objects;
import java.util.Optional;

public class Spigot13ItemTypeFireworkCharge extends Spigot13ItemType implements WSItemTypeFireworkCharge {

	private WSFireworkEffect effect;

	public Spigot13ItemTypeFireworkCharge(WSFireworkEffect effect) {
		super(402, "minecraft:firework_charge", "minecraft:firework_star", 64);
		this.effect = effect;
	}

	@Override
	public Optional<WSFireworkEffect> getEffect() {
		return Optional.ofNullable(effect);
	}

	@Override
	public void setEffect(WSFireworkEffect effect) {
		this.effect = effect;
	}

	@Override
	public Spigot13ItemTypeFireworkCharge clone() {
		return new Spigot13ItemTypeFireworkCharge(effect == null ? null : effect.clone());
	}

	@Override
	public void writeItemMeta(ItemMeta itemMeta) {
		super.writeItemMeta(itemMeta);
		if (itemMeta instanceof FireworkEffectMeta) {
			((FireworkEffectMeta) itemMeta).setEffect(effect == null ? null : (FireworkEffect) effect.getHandled());
		}
	}

	@Override
	public void readItemMeta(ItemMeta itemMeta) {
		super.readItemMeta(itemMeta);
		if(itemMeta instanceof FireworkEffectMeta) {
			effect = Optional.ofNullable(((FireworkEffectMeta) itemMeta).getEffect()).map(Spigot13FireworkEffect::new).orElse(null);
		}
	}


	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Spigot13ItemTypeFireworkCharge that = (Spigot13ItemTypeFireworkCharge) o;
		return Objects.equals(effect, that.effect);
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), effect);
	}
}
