package com.degoos.wetsponge.material.item.type;

import com.degoos.wetsponge.enums.item.EnumItemTypeFishType;
import com.degoos.wetsponge.material.item.Spigot13ItemType;
import com.degoos.wetsponge.util.Validate;

import java.util.Objects;

public class Spigot13ItemTypeFish extends Spigot13ItemType implements WSItemTypeFish {

	private EnumItemTypeFishType fishType;

	public Spigot13ItemTypeFish(EnumItemTypeFishType fishType) {
		super(263, "minecraft:fish", "minecraft:cod", 64);
		Validate.notNull(fishType, "Fish type cannot be null!");
		this.fishType = fishType;
	}

	@Override
	public String getNewStringId() {
		switch (fishType) {
			case SALMON:
				return "minecraft:salmon";
			case TROPICAL_FISH:
				return "minecraft:tropical_fish";
			case PUFFERFISH:
				return "minecraft:pufferfish";
			case COD:
			default:
				return "minecraft:cod";
		}
	}

	@Override
	public EnumItemTypeFishType getFishType() {
		return fishType;
	}

	@Override
	public void setFishType(EnumItemTypeFishType fishType) {
		Validate.notNull(fishType, "Fish type cannot be null!");
		this.fishType = fishType;
	}

	@Override
	public Spigot13ItemTypeFish clone() {
		return new Spigot13ItemTypeFish(fishType);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Spigot13ItemTypeFish that = (Spigot13ItemTypeFish) o;
		return fishType == that.fishType;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), fishType);
	}
}
