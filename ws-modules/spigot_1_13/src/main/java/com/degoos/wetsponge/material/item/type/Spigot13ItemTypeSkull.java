package com.degoos.wetsponge.material.item.type;

import com.degoos.wetsponge.bridge.material.item.BridgeItemTypeSkull;
import com.degoos.wetsponge.enums.block.EnumBlockTypeSkullType;
import com.degoos.wetsponge.material.item.Spigot13ItemType;
import com.degoos.wetsponge.resource.spigot.Spigot13SkullBuilder;
import com.degoos.wetsponge.user.Spigot13GameProfile;
import com.degoos.wetsponge.user.WSGameProfile;
import com.degoos.wetsponge.util.Validate;
import com.mojang.authlib.GameProfile;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.net.URL;
import java.util.Objects;
import java.util.Optional;

public class Spigot13ItemTypeSkull extends Spigot13ItemType implements WSItemTypeSkull {


	private WSGameProfile profile;
	private EnumBlockTypeSkullType skullType;

	public Spigot13ItemTypeSkull(WSGameProfile profile, EnumBlockTypeSkullType skullType) {
		super(397, "minecraft:skull", "minecraft:skull", 64);
		Validate.notNull(skullType, "Skull type cannot be null!");
		this.profile = profile;
		this.skullType = skullType;
	}

	@Override
	public String getNewStringId() {
		return skullType.getMinecraftName() + (skullType == EnumBlockTypeSkullType.SKELETON || skullType == EnumBlockTypeSkullType.WITHER_SKELETON ? "_skull" : "_head");
	}

	public Optional<WSGameProfile> getProfile() {
		return Optional.ofNullable(profile);
	}

	public void setProfile(WSGameProfile profile) {
		this.profile = profile;
	}

	public void setTexture(String texture) {
		BridgeItemTypeSkull.setTexture(texture, profile);
	}

	public void setTexture(URL texture) {
		BridgeItemTypeSkull.setTexture(texture, profile);
	}

	public void setTextureByPlayerName(String name) {
		profile = BridgeItemTypeSkull.setTextureByPlayerName(name);
	}

	public void findFormatAndSetTexture(String texture) {
		if (texture.toLowerCase().startsWith("http:") || texture.toLowerCase().startsWith("https:")) setTexture(getTexture(texture));
		else if (texture.length() <= 16) setTextureByPlayerName(texture);
		else setTexture(texture);
	}

	public EnumBlockTypeSkullType getSkullType() {
		return skullType;
	}

	public void setSkullType(EnumBlockTypeSkullType skullType) {
		Validate.notNull(skullType, "Skull type cannot be null!");
		this.skullType = skullType;
	}

	private String getTexture(String url) {
		if (url == null) return null;
		return String.format("{textures:{SKIN:{url:\"%s\"}}}", url);
	}

	@Override
	public Spigot13ItemTypeSkull clone() {
		return new Spigot13ItemTypeSkull(profile, skullType);
	}

	@Override
	public void writeItemMeta(ItemMeta itemMeta) {
		super.writeItemMeta(itemMeta);
		if (itemMeta instanceof SkullMeta) {
			Spigot13SkullBuilder.injectGameProfile(itemMeta, profile == null ? null : ((Spigot13GameProfile) profile).getHandled());
		}
	}

	@Override
	public void readItemMeta(ItemMeta itemMeta) {
		super.readItemMeta(itemMeta);
		if (itemMeta instanceof SkullMeta) {
			GameProfile profile = Spigot13SkullBuilder.getGameProfile(itemMeta);
			this.profile = profile == null ? null : new Spigot13GameProfile(profile);
		}
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Spigot13ItemTypeSkull that = (Spigot13ItemTypeSkull) o;
		return Objects.equals(profile, that.profile) &&
				skullType == that.skullType;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), profile, skullType);
	}
}
