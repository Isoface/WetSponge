package com.degoos.wetsponge.material.item.type;

import com.degoos.wetsponge.enums.EnumEntityType;
import com.degoos.wetsponge.material.item.Spigot13ItemType;
import com.degoos.wetsponge.util.Validate;

import java.util.Objects;

public class Spigot13ItemTypeSpawnEgg extends Spigot13ItemType implements WSItemTypeSpawnEgg {

	private EnumEntityType entityType;

	public Spigot13ItemTypeSpawnEgg(EnumEntityType entityType) {
		super(383, "minecraft:spawn_egg", "minecraft:$entity_spawn_egg", 64);
		Validate.notNull(entityType, "Entity type cannot be null!");
		this.entityType = entityType;
	}

	@Override
	public String getNewStringId() {
		return "minecraft:" + entityType.getName().toLowerCase() + "_spawn_egg";
	}


	@Override
	public EnumEntityType getEntityType() {
		return entityType;
	}

	@Override
	public void setEntityType(EnumEntityType entityType) {
		Validate.notNull(entityType, "Entity type cannot be null!");
		this.entityType = entityType;
	}


	@Override
	public Spigot13ItemTypeSpawnEgg clone() {
		return new Spigot13ItemTypeSpawnEgg(entityType);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Spigot13ItemTypeSpawnEgg that = (Spigot13ItemTypeSpawnEgg) o;
		return entityType == that.entityType;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), entityType);
	}
}
