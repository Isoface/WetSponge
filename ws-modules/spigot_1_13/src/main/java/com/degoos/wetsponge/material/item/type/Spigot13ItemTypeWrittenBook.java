package com.degoos.wetsponge.material.item.type;

import com.degoos.wetsponge.enums.item.EnumItemTypeBookGeneration;
import com.degoos.wetsponge.material.item.Spigot13ItemType;
import com.degoos.wetsponge.text.WSText;
import com.degoos.wetsponge.util.Validate;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Spigot13ItemTypeWrittenBook extends Spigot13ItemType implements WSItemTypeWrittenBook {


	private WSText title;
	private WSText author;
	private List<WSText> pages;
	private EnumItemTypeBookGeneration generation;


	public Spigot13ItemTypeWrittenBook(int id, String stringId, String stringId13, WSText title, WSText author, List<WSText> pages, EnumItemTypeBookGeneration generation) {
		super(id, stringId, stringId13, 1);
		Validate.notNull(generation, "Generation cannot be null!");
		this.title = title;
		this.author = author;
		this.pages = pages == null ? new ArrayList<>() : pages;
		this.generation = generation;
	}

	@Override
	public WSText getAuthor() {
		return author;
	}

	@Override
	public WSItemTypeWrittenBook setAuthor(WSText author) {
		this.author = author;
		return this;
	}

	@Override
	public WSText getTitle() {
		return title;
	}

	@Override
	public WSItemTypeWrittenBook setTitle(WSText title) {
		this.title = title;
		return this;
	}

	@Override
	public List<WSText> getPages() {
		return pages;
	}

	@Override
	public WSItemTypeWrittenBook addPage(WSText pageText) {
		pages.add(pageText);
		return this;
	}

	@Override
	public WSItemTypeWrittenBook setPages(List<WSText> pages) {
		this.pages = pages;
		return this;
	}

	@Override
	public EnumItemTypeBookGeneration getGeneration() {
		return generation;
	}

	@Override
	public void setGeneration(EnumItemTypeBookGeneration generation) {
		Validate.notNull(generation, "Generation cannot be null!");
		this.generation = generation;
	}

	@Override
	public Spigot13ItemTypeWrittenBook clone() {
		return new Spigot13ItemTypeWrittenBook(getNumericalId(), getStringId(), getStringId(), title, author, new ArrayList<>(pages), generation);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Spigot13ItemTypeWrittenBook that = (Spigot13ItemTypeWrittenBook) o;
		return Objects.equals(title, that.title) &&
				Objects.equals(author, that.author) &&
				Objects.equals(pages, that.pages) &&
				generation == that.generation;
	}

	@Override
	public int hashCode() {

		return Objects.hash(super.hashCode(), title, author, pages, generation);
	}
}
