package com.degoos.wetsponge.mixin.spigot;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.enums.EnumServerType;
import org.bukkit.plugin.java.JavaPlugin;

public class Spigot13Injector {

	public static void inject(JavaPlugin javaPlugin) {
		if (WetSponge.getServerType() == EnumServerType.SPIGOT || WetSponge.getServerType() == EnumServerType.PAPER_SPIGOT) {
			Spigot13AsyncCatcherRemover.load(javaPlugin);
			Spigot13WSChunkProviderAssist.load();
			Spigot13WSEntityHumanAssist.load();
			Spigot13TimingsAssist.load();
		}
	}

}
