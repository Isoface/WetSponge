package com.degoos.wetsponge.mixin.spigot;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.reflection.NMSUtils;
import javassist.*;

public class Spigot13WSChunkProviderAssist {

	public static Class<?> CHUNK_PROVIDER_CLASS;

	public static void load() {
		try {
			ClassPool pool = ClassPool.getDefault();
			CtClass cWSChunkProvider = pool.makeClass("WSChunkProvider");
			Class<?> chunkClass = NMSUtils.getNMSClass("Chunk");
			Class<?> chunkProviderServerClass = NMSUtils.getNMSClass("ChunkProviderServer");

			pool.insertClassPath(new ClassClassPath(chunkClass));
			pool.insertClassPath(new ClassClassPath(chunkProviderServerClass));
			pool.insertClassPath(new ClassClassPath(Spigot13MixinChunkProviderServer.class));

			CtClass cChunk = pool.get(chunkClass.getName());
			CtClass cChunkProviderServer = pool.get(chunkProviderServerClass.getName());
			CtClass cMixin = pool.get(Spigot13MixinChunkProviderServer.class.getName());
			CtClass cArrayList = ClassPool.getDefault().get("java.util.ArrayList");
			CtClass cBoolean = CtClass.booleanType;

			cWSChunkProvider.setSuperclass(cChunkProviderServer);

			CtField cantSaveChunksField = CtField.make("private java.util.List/*<Object>*/ cantSaveChunks;", cWSChunkProvider);
			CtField canSaveField = CtField.make("public boolean canSave;", cWSChunkProvider);
			cWSChunkProvider.addField(cantSaveChunksField);
			cWSChunkProvider.addField(canSaveField);

			cWSChunkProvider.addMethod(CtNewMethod.copy(cMixin.getDeclaredMethod("hasChunk"), cWSChunkProvider, null));

			CtMethod oldSaveChunkMethod = cChunkProviderServer.getDeclaredMethod("saveChunk");
			CtMethod saveChunkMethod = oldSaveChunkMethod.getParameterTypes().length == 2 ? CtNewMethod.make(CtClass.voidType, "saveChunk", new CtClass[]{cChunk,
				cBoolean}, new CtClass[]{}, "if(!cantSaveChunks.contains($1) && canSave) super.saveChunk($1, $2);", cWSChunkProvider) : CtNewMethod
				                           .make(CtClass.voidType, "saveChunk", new CtClass[]{
					                           cChunk}, new CtClass[]{}, "if(!cantSaveChunks.contains($1) && canSave) super.saveChunk($1);", cWSChunkProvider);
			/*CtMethod saveChunkNOPMethod = CtNewMethod.make(CtClass.voidType, "saveChunkNOP", new CtClass[]{
				cChunk}, new CtClass[]{}, "if(!cantSaveChunks.contains($1)) super.saveChunkNOP($1);", cWSChunkProvider);*/
			cWSChunkProvider.addMethod(CtNewMethod
				.make(CtClass.voidType, "initLists", new CtClass[]{cArrayList}, new CtClass[]{}, "{cantSaveChunks = $1; canSave = true;}", cWSChunkProvider));

			cWSChunkProvider.addMethod(CtNewMethod.getter("getCantSave", cantSaveChunksField));

			cWSChunkProvider.addMethod(saveChunkMethod);
			//cWSChunkProvider.addMethod(saveChunkNOPMethod);
			CHUNK_PROVIDER_CLASS = cWSChunkProvider.toClass();

			if (WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD)) {

			}
		} catch (Throwable ex) {
			InternalLogger.printException(ex, "An exception has occurred while WetSponge was injecting code!");
		}
	}
}
