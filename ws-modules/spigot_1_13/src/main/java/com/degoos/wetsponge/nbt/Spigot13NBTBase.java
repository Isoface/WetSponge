package com.degoos.wetsponge.nbt;


import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.reflection.NMSUtils;
import com.degoos.wetsponge.util.reflection.ReflectionUtils;

public abstract class Spigot13NBTBase implements WSNBTBase {

	private Object nbtBase;
	private static final Class<?> clazz = NMSUtils.getNMSClass("NBTBase");


	public Spigot13NBTBase(Object nbtBase) {
		this.nbtBase = nbtBase;
	}

	@Override
	public byte getId() {
		try {
			return (byte) ReflectionUtils.invokeMethod(nbtBase, clazz, "getTypeId");
		} catch (Exception e) {
			InternalLogger.printException(e, "An error has occurred while WetSponge was getting a NBTTag id!");
			return 0;
		}
	}

	@Override
	public abstract WSNBTBase copy();

	@Override
	public boolean hasNoTags() {
		try {
			return (boolean) ReflectionUtils.invokeMethod(nbtBase, clazz, "isEmpty");
		} catch (Exception e) {
			InternalLogger.printException(e, "An error has occurred while WetSponge was checking if a NBTTag has no tags!");
			return true;
		}
	}

	@Override
	public String toString() {
		return nbtBase == null ? super.toString() : nbtBase.toString();
	}

	@Override
	public Object getHandled() {
		return nbtBase;
	}
}
