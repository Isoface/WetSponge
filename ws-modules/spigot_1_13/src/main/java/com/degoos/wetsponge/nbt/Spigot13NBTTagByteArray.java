package com.degoos.wetsponge.nbt;


import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.reflection.NMSUtils;
import com.degoos.wetsponge.util.reflection.ReflectionUtils;

import java.lang.reflect.InvocationTargetException;

public class Spigot13NBTTagByteArray extends Spigot13NBTBase implements WSNBTTagByteArray {

	private static final Class<?> clazz = NMSUtils.getNMSClass("NBTTagByteArray");

	public Spigot13NBTTagByteArray(byte[] bytes) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
		this(clazz.getConstructor(byte[].class).newInstance((Object) bytes));
	}

	public Spigot13NBTTagByteArray(Object nbtTagByteArray) {
		super(nbtTagByteArray);
	}

	@Override
	public byte[] getByteArray() {
		try {
			return ReflectionUtils.getFirstObject(clazz, byte[].class, getHandled());
		} catch (Exception e) {
			InternalLogger.printException(e, "An error has occurred while WetSponge was getting the value of a NBTTag!");
			return new byte[0];
		}
	}


	@Override
	public WSNBTTagByteArray copy() {
		try {
			return new Spigot13NBTTagByteArray(ReflectionUtils.invokeMethod(getHandled(), clazz, "clone"));
		} catch (Exception e) {
			InternalLogger.printException(e, "An exception has occurred while WetSponge was cloning a NBTTag!");
			return null;
		}
	}

	@Override
	public Object getHandled() {
		return super.getHandled();
	}
}
