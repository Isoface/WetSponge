package com.degoos.wetsponge.nbt;

import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.reflection.NMSUtils;
import com.degoos.wetsponge.util.reflection.ReflectionUtils;

import javax.annotation.Nullable;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

public class Spigot13NBTTagCompound extends Spigot13NBTBase implements WSNBTTagCompound {

	private static final Class<?> clazz = NMSUtils.getNMSClass("NBTTagCompound");
	private Map map;

	public Spigot13NBTTagCompound(String nbt) throws Exception {
		this(NMSUtils.getNMSClass("MojangsonParser").getMethod("parse", String.class).invoke(null, NBTTagUpdater.update(nbt)));
	}

	public Spigot13NBTTagCompound(Object object) {
		super(object);
		try {
			map = ReflectionUtils.getFirstObject(clazz, Map.class, getHandled());
		} catch (Exception e) {
			InternalLogger.printException(e, "An exception has occurred while WetSponge was initializing a NBTTagCompound!");
		}
	}

	public Spigot13NBTTagCompound() throws IllegalAccessException, InstantiationException {
		this(clazz.newInstance());
	}

	@Override
	@SuppressWarnings("unchecked")
	public Set<String> getKeySet() {
		return (Set<String>) map.keySet();
	}

	@Override
	public int getSize() {
		return map.size();
	}

	@Override
	@SuppressWarnings("unchecked")
	public void setTag(String key, WSNBTBase wsnbt) {
		map.put(key, wsnbt.getHandled());
	}

	@Override
	@SuppressWarnings("unchecked")
	public void setByte(String key, byte b) {
		try {
			map.put(key, new Spigot13NBTTagByte(b).getHandled());
		} catch (Exception e) {
			InternalLogger.printException(e, "An error has occurred while WetSponge was modifying a NBTTagCompound!");
			e.printStackTrace();
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public void setShort(String key, short s) {
		try {
			map.put(key, new Spigot13NBTTagShort(s).getHandled());
		} catch (Exception e) {
			InternalLogger.printException(e, "An error has occurred while WetSponge was modifying a NBTTagCompound!");
			e.printStackTrace();
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public void setInteger(String key, int i) {
		try {
			map.put(key, new Spigot13NBTTagInt(i).getHandled());
		} catch (Exception e) {
			InternalLogger.printException(e, "An error has occurred while WetSponge was modifying a NBTTagCompound!");
			e.printStackTrace();
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public void setLong(String key, long l) {
		try {
			map.put(key, new Spigot13NBTTagLong(l).getHandled());
		} catch (Exception e) {
			InternalLogger.printException(e, "An error has occurred while WetSponge was modifying a NBTTagCompound!");
			e.printStackTrace();
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public void setUniqueId(String key, UUID uuid) {
		setLong(key + "Most", uuid.getMostSignificantBits());
		setLong(key + "Least", uuid.getLeastSignificantBits());
	}

	@Override
	@SuppressWarnings("unchecked")
	public void setFloat(String key, float f) {
		try {
			map.put(key, new Spigot13NBTTagFloat(f).getHandled());
		} catch (Exception e) {
			InternalLogger.printException(e, "An error has occurred while WetSponge was modifying a NBTTagCompound!");
			e.printStackTrace();
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public void setDouble(String key, double d) {
		try {
			map.put(key, new Spigot13NBTTagDouble(d).getHandled());
		} catch (Exception e) {
			InternalLogger.printException(e, "An error has occurred while WetSponge was modifying a NBTTagCompound!");
			e.printStackTrace();
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public void setString(String key, String string) {
		try {
			map.put(key, new Spigot13NBTTagString(string).getHandled());
		} catch (Exception e) {
			InternalLogger.printException(e, "An error has occurred while WetSponge was modifying a NBTTagCompound!");
			e.printStackTrace();
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public void setByteArray(String key, byte[] bytes) {
		try {
			map.put(key, new Spigot13NBTTagByteArray(bytes).getHandled());
		} catch (Exception e) {
			InternalLogger.printException(e, "An error has occurred while WetSponge was modifying a NBTTagCompound!");
			e.printStackTrace();
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public void setIntArray(String key, int[] ints) {
		try {
			map.put(key, new Spigot13NBTTagIntArray(ints).getHandled());
		} catch (Exception e) {
			InternalLogger.printException(e, "An error has occurred while WetSponge was modifying a NBTTagCompound!");
			e.printStackTrace();
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public void setBoolean(String key, boolean b) {
		setByte(key, (byte) (b ? 1 : 0));
	}

	@Override
	public WSNBTBase getTag(String key) {
		return Optional.ofNullable(map.get(key)).map(Spigot13NBTTagParser::parse).orElse(null);
	}

	@Override
	public byte getTagId(String key) {
		return Optional.ofNullable(getTag(key)).map(WSNBTBase::getId).orElse((byte) 0);
	}

	@Override
	public boolean hasKey(String key) {
		return map.containsKey(key);
	}

	@Override
	public boolean hasKeyOfType(String key, int id) {
		byte tagId = this.getTagId(key);
		if (tagId == id) {
			return true;
		} else if (id != 99) {
			return false;
		} else {
			return tagId == 1 || tagId == 2 || tagId == 3 || tagId == 4 || tagId == 5 || tagId == 6;
		}
	}

	@Override
	public byte getByte(String key) {
		try {
			if (hasKeyOfType(key, 99)) {
				return ((WSNBTPrimitive) getTag(key)).getByte();
			}
		} catch (ClassCastException ignore) {
		}
		return 0;
	}

	@Override
	public short getShort(String key) {
		try {
			if (hasKeyOfType(key, 99)) {
				return ((WSNBTPrimitive) getTag(key)).getShort();
			}
		} catch (ClassCastException ignore) {
		}
		return 0;
	}

	@Override
	public int getInteger(String key) {
		try {
			if (hasKeyOfType(key, 99)) {
				return ((WSNBTPrimitive) getTag(key)).getInt();
			}
		} catch (ClassCastException ignore) {
		}
		return 0;
	}

	@Override
	public long getLong(String key) {
		try {
			if (hasKeyOfType(key, 99)) {
				return ((WSNBTPrimitive) getTag(key)).getLong();
			}
		} catch (ClassCastException ignore) {
		}
		return 0;
	}

	@Nullable
	@Override
	public UUID getUniqueId(String key) {
		return new UUID(this.getLong(key + "Most"), this.getLong(key + "Least"));
	}

	@Override
	public boolean hasUniqueId(String key) {
		return this.hasKeyOfType(key + "Most", 99) && this.hasKeyOfType(key + "Least", 99);
	}

	@Override
	public float getFloat(String key) {
		try {
			if (hasKeyOfType(key, 99)) {
				return ((WSNBTPrimitive) getTag(key)).getFloat();
			}
		} catch (ClassCastException ignore) {
		}
		return 0;
	}

	@Override
	public double getDouble(String key) {
		try {
			if (hasKeyOfType(key, 99)) {
				return ((WSNBTPrimitive) getTag(key)).getDouble();
			}
		} catch (ClassCastException ignore) {
		}
		return 0;
	}

	@Override
	public String getString(String key) {
		try {
			if (hasKeyOfType(key, 8)) {
				return ((WSNBTTagString) getTag(key)).getString();
			}
		} catch (ClassCastException ignore) {
		}
		return "";
	}

	@Override
	public byte[] getByteArray(String key) {
		try {
			return (byte[]) ReflectionUtils.invokeMethod(getHandled(), "getByteArray", key);
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was getting an array from a NBTTagCompound!");
			return new byte[0];
		}
	}

	@Override
	public int[] getIntArray(String key) {
		try {
			return (int[]) ReflectionUtils.invokeMethod(getHandled(), "getIntArray", key);
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was getting an array from a NBTTagCompound!");
			return new int[0];
		}
	}

	@Override
	public WSNBTTagCompound getCompoundTag(String key) {
		try {
			return new Spigot13NBTTagCompound(ReflectionUtils.invokeMethod(getHandled(), "getCompound", key));
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was getting an array from a NBTTagCompound!");
			try {
				return new Spigot13NBTTagCompound();
			} catch (Exception ex2) {
				InternalLogger.printException(ex2, "An error has occurred while WetSponge was getting an array from a NBTTagCompound!");
				return null;
			}
		}
	}

	@Override
	public WSNBTTagList getTagList(String key, int expectedTagId) {
		return null; //TODO
	}

	@Override
	public boolean getBoolean(String key) {
		return getByte(key) != 0;
	}

	@Override
	public void removeTag(String key) {
		map.remove(key);
	}

	@Override
	public void merge(WSNBTTagCompound wsnbtTagCompound) {
		for (Object o : ((Spigot13NBTTagCompound) wsnbtTagCompound).map.keySet()) {
			String key = (String) o;
			WSNBTBase wsnbtBase = wsnbtTagCompound.getTag(key);
			if (wsnbtBase.getId() == 10 && hasKeyOfType(key, 10)) {
				WSNBTTagCompound compound = getCompoundTag(key);
				compound.merge((WSNBTTagCompound) wsnbtBase);
			} else setTag(key, wsnbtBase.copy());
		}
	}

	@Override
	public WSNBTTagCompound copy() {
		try {
			Spigot13NBTTagCompound compound = new Spigot13NBTTagCompound();
			for (Object o : map.keySet()) {
				String key = (String) o;
				WSNBTBase tag = getTag(key);
				if (tag != null) compound.setTag(key, tag.copy());
			}
			return compound;
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was copying a NBTTagCompound!");
			return null;
		}
	}
}
