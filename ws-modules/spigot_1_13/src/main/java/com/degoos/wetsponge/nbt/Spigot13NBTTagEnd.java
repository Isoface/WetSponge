package com.degoos.wetsponge.nbt;

import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.reflection.NMSUtils;

import java.lang.reflect.Constructor;

public class Spigot13NBTTagEnd extends Spigot13NBTBase implements WSNBTTagEnd {

	private static final Class<?> clazz = NMSUtils.getNMSClass("NBTTagEnd");

	public Spigot13NBTTagEnd(Object nbtTagEnd) {
		super(nbtTagEnd);
	}

	public Spigot13NBTTagEnd() {
		this(newInstance());
	}

	@Override
	public WSNBTTagEnd copy() {
		return new Spigot13NBTTagEnd(newInstance());
	}

	private static Object newInstance() {
		try {
			Constructor constructor = clazz.getDeclaredConstructor();
			constructor.setAccessible(true);
			return constructor.newInstance();
		} catch (Exception e) {
			InternalLogger.printException(e, "An error has occurred while WetSponge was creating a new instance of a NBTTagEnd!");
			return null;
		}
	}
}
