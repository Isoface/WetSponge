package com.degoos.wetsponge.nbt;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.reflection.NMSUtils;
import com.degoos.wetsponge.util.reflection.ReflectionUtils;

import java.util.ArrayList;
import java.util.List;

public class Spigot13NBTTagList extends Spigot13NBTBase implements WSNBTTagList {

	private static final Class<?> clazz = NMSUtils.getNMSClass("NBTTagList");
	private List list;

	public Spigot13NBTTagList() throws IllegalAccessException, InstantiationException {
		this(clazz.newInstance());
		try {
			list = ReflectionUtils.getFirstObject(clazz, List.class, getHandled());
		} catch (Exception e) {
			InternalLogger.printException(e, "An error has occurred while WetSponge vas initializing a NBTTagList!");
			list = new ArrayList();
		}
	}

	public Spigot13NBTTagList(Object object) {
		super(object);
	}

	@Override
	public void appendTag(WSNBTBase wsnbt) {
		try {
			ReflectionUtils.invokeMethod(getHandled(), "add", wsnbt.getHandled());
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was modifying a NBTTagList!");
		}
	}

	@Override
	public void set(int index, WSNBTBase wsnbt) {
		try {
			ReflectionUtils.invokeMethod(getHandled(), "a", index, wsnbt.getHandled());
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was modifying a NBTTagList!");
		}
	}

	@Override
	public void removeTag(int index) {
		try {
			ReflectionUtils.invokeMethod(getHandled(), WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD) ? "remove" : "a", index);
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was modifying a NBTTagList!");
		}
	}

	@Override
	public WSNBTTagCompound getCompoundTagAt(int index) {
		try {
			return new Spigot13NBTTagCompound(ReflectionUtils.invokeMethod(getHandled(), "get", index));
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was getting a NBTTagCompound from a NBTTagList!");
			try {
				return new Spigot13NBTTagCompound();
			} catch (Exception e) {
				InternalLogger.printException(ex, "An error has occurred while WetSponge was creating a new NBTTagCompound!");
				return null;
			}
		}
	}

	@Override
	public int getIntAt(int index) {
		if (index >= 0 && index < this.list.size()) {
			WSNBTBase tag = get(index);
			if (tag.getId() == 3) {
				return ((WSNBTTagInt) tag).getInt();
			}
		}
		return 0;
	}

	@Override
	public int[] getIntArrayAt(int index) {
		if (index >= 0 && index < this.list.size()) {
			WSNBTBase tag = get(index);
			if (tag.getId() == 11) {
				return ((WSNBTTagIntArray) tag).getIntArray();
			}
		}
		return new int[0];
	}

	@Override
	public double getDoubleAt(int index) {
		if (index >= 0 && index < this.list.size()) {
			WSNBTBase tag = get(index);
			if (tag.getId() == 6) {
				return ((WSNBTTagDouble) tag).getDouble();
			}
		}
		return 0;
	}

	@Override
	public float getFloatAt(int index) {
		if (index >= 0 && index < this.list.size()) {
			WSNBTBase tag = get(index);
			if (tag.getId() == 5) {
				return ((WSNBTTagFloat) tag).getFloat();
			}
		}
		return 0;
	}

	@Override
	public String getStringAt(int index) {
		if (index >= 0 && index < this.list.size()) {
			WSNBTBase tag = get(index);
			return tag.getId() == 8 ? ((WSNBTTagString) tag).getString() : tag.toString();
		}
		return "";
	}

	@Override
	public WSNBTBase get(int index) {
		return index >= 0 && index < list.size() ? Spigot13NBTTagParser.parse(list.get(index)) : new Spigot13NBTTagEnd();
	}

	@Override
	public int tagCount() {
		return list.size();
	}

	@Override
	public int getTagType() {
		try {
			return (Byte) ReflectionUtils.setAccessible(clazz.getDeclaredField("item")).get(getHandled());
		} catch (Exception e) {
			InternalLogger.printException(e, "An error has occurred while WetSponge was getting the type of a NBTTagList!");
			return 0;
		}
	}

	@Override
	public WSNBTTagList copy() {
		try {
			Spigot13NBTTagList list = new Spigot13NBTTagList();

			for (Object o : list.list) {
				WSNBTBase base = Spigot13NBTTagParser.parse(o);
				list.appendTag(base);
			}
			return list;

		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while Wetsponge was copying a NBTTagList!");
			return null;
		}
	}
}
