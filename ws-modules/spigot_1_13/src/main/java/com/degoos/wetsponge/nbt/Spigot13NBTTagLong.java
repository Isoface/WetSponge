package com.degoos.wetsponge.nbt;


import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.reflection.NMSUtils;
import com.degoos.wetsponge.util.reflection.ReflectionUtils;

import java.lang.reflect.InvocationTargetException;

public class Spigot13NBTTagLong extends Spigot13NBTBase implements WSNBTTagLong {

	private static final Class<?> clazz = NMSUtils.getNMSClass("NBTTagLong");

	public Spigot13NBTTagLong(long k) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
		this(clazz.getConstructor(long.class).newInstance(k));
	}

	public Spigot13NBTTagLong(Object object) {
		super(object);
	}

	private long getValue() {
		try {
			return ReflectionUtils.getFirstObject(clazz, long.class, getHandled());
		} catch (Exception e) {
			InternalLogger.printException(e, "An error has occurred while WetSponge was getting the value of a NBTTag!");
			return 0;
		}
	}

	@Override
	public long getLong() {
		return getValue();
	}

	@Override
	public int getInt() {
		return (int) (getValue() & -1L);
	}

	@Override
	public short getShort() {
		return (short) (getValue() & 65535L);
	}

	@Override
	public byte getByte() {
		return (byte) (getValue() & 255);
	}

	@Override
	public double getDouble() {
		return getValue();
	}

	@Override
	public float getFloat() {
		return getValue();
	}

	@Override
	public WSNBTTagLong copy() {
		try {
			return new Spigot13NBTTagLong(getValue());
		} catch (Exception e) {
			InternalLogger.printException(e, "An error has occurred while WetSponge was copying a NBTTag!");
			return null;
		}
	}

	@Override
	public Object getHandled() {
		return super.getHandled();
	}
}
