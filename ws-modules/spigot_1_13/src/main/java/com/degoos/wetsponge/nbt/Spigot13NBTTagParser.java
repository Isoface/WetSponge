package com.degoos.wetsponge.nbt;

public class Spigot13NBTTagParser {

	public static WSNBTBase parse(Object nbtBase) {
		switch (new Spigot13NBTBase(nbtBase) {
			@Override
			public WSNBTBase copy() {
				return null;
			}
		}.getId()) {
			case 0:
				return new Spigot13NBTTagEnd(nbtBase);
			case 1:
				return new Spigot13NBTTagByte(nbtBase);
			case 2:
				return new Spigot13NBTTagShort(nbtBase);
			case 3:
				return new Spigot13NBTTagInt(nbtBase);
			case 4:
				return new Spigot13NBTTagLong(nbtBase);
			case 5:
				return new Spigot13NBTTagFloat(nbtBase);
			case 6:
				return new Spigot13NBTTagDouble(nbtBase);
			case 7:
				return new Spigot13NBTTagByteArray(nbtBase);
			case 8:
				return new Spigot13NBTTagString(nbtBase);
			case 9:
				return new Spigot13NBTTagList(nbtBase);
			case 10:
				return new Spigot13NBTTagCompound(nbtBase);
			case 11:
				return new Spigot13NBTTagIntArray(nbtBase);
			case 12:
				return new Spigot13NBTTagLongArray(nbtBase);
			default:
				return null;
		}
	}

}
