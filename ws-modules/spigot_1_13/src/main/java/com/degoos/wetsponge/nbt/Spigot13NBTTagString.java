package com.degoos.wetsponge.nbt;

import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.reflection.NMSUtils;
import com.degoos.wetsponge.util.reflection.ReflectionUtils;

import java.lang.reflect.InvocationTargetException;

public class Spigot13NBTTagString extends Spigot13NBTBase implements WSNBTTagString {

	private static final Class<?> clazz = NMSUtils.getNMSClass("NBTTagString");

	public Spigot13NBTTagString(String s) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
		this(clazz.getConstructor(String.class).newInstance(s));
	}

	public Spigot13NBTTagString(Object object) {
		super(object);
	}

	private String getValue() {
		try {
			return ReflectionUtils.getFirstObject(clazz, String.class, getHandled());
		} catch (Exception e) {
			InternalLogger.printException(e, "An error has occurred while WetSponge was getting the value of a NBTTag!");
			return "";
		}
	}

	@Override
	public String getString() {
		return getValue();
	}

	@Override
	public WSNBTTagString copy() {
		try {
			return new Spigot13NBTTagString(getValue());
		} catch (Exception e) {
			InternalLogger.printException(e, "An error has occurred while WetSponge was copying a NBTTag!");
			return null;
		}
	}

	@Override
	public Object getHandled() {
		return super.getHandled();
	}
}
