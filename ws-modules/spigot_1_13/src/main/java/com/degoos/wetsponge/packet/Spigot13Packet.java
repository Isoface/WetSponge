package com.degoos.wetsponge.packet;

public abstract class Spigot13Packet implements WSPacket {

    private Object packet;

    public Spigot13Packet(Object packet) {
        this.packet = packet;
    }

    @Override
    public Object getHandler() {
        return packet;
    }
}
