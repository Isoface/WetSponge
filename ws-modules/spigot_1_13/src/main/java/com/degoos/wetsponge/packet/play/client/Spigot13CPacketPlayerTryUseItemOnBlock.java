package com.degoos.wetsponge.packet.play.client;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.packet.Spigot13Packet;
import com.degoos.wetsponge.util.reflection.NMSUtils;
import com.degoos.wetsponge.util.reflection.Spigot13HandledUtils;
import com.flowpowered.math.vector.Vector3i;

import java.lang.reflect.Field;
import java.util.Arrays;

public class Spigot13CPacketPlayerTryUseItemOnBlock extends Spigot13Packet implements WSCPacketPlayerTryUseItemOnBlock {

	private Vector3i position;
	private EnumBlockFace placedBlockDirection;
	private boolean mainHand;
	private float facingX, facingY, facingZ;
	private boolean changed;

	public Spigot13CPacketPlayerTryUseItemOnBlock(Vector3i position, EnumBlockFace placedBlockDirection, boolean mainHand, float facingX, float
			facingY, float facingZ) throws IllegalAccessException, InstantiationException {
		super(NMSUtils.getNMSClass("PacketPlayInUseItem").newInstance());
		this.position = position;
		this.placedBlockDirection = placedBlockDirection;
		this.mainHand = mainHand;
		this.facingX = facingX;
		this.facingY = facingY;
		this.facingZ = facingZ;
		update();
	}

	public Spigot13CPacketPlayerTryUseItemOnBlock(Object packet) {
		super(packet);
		refresh();
	}


	@Override
	public Vector3i getPosition() {
		return position;
	}

	@Override
	public void setPosition(Vector3i position) {
		this.position = position;
		changed = true;
	}

	@Override
	public EnumBlockFace getPlacedBlockDirection() {
		return placedBlockDirection;
	}

	@Override
	public void setPlacedBlockDirection(EnumBlockFace direction) {
		this.placedBlockDirection = direction;
		changed = true;
	}

	@Override
	public boolean isMainHand() {
		return mainHand;
	}

	@Override
	public void setMainHand(boolean mainHand) {
		this.mainHand = mainHand;
		changed = true;
	}

	@Override
	public float getFacingX() {
		return facingX;
	}

	@Override
	public void setFacingX(float facingX) {
		this.facingX = facingX;
		changed = true;
	}

	@Override
	public float getFacingY() {
		return facingY;
	}

	@Override
	public void setFacingY(float facingY) {
		this.facingY = facingY;
		changed = true;
	}

	@Override
	public float getFacingZ() {
		return facingZ;
	}

	@Override
	public void setFacingZ(float facingZ) {
		this.facingZ = facingZ;
		changed = true;
	}

	@Override
	public void update() {
		try {
			Field[] fields = getHandler().getClass().getDeclaredFields();
			Arrays.stream(fields).forEach(field -> field.setAccessible(true));
			fields[0].set(getHandler(), Spigot13HandledUtils.getBlockPosition(position));
			fields[1].set(getHandler(), NMSUtils.getNMSClass("EnumDirection")
					.getMethod("valueOf", String.class).invoke(null, placedBlockDirection.name()));
			fields[2].set(getHandler(), NMSUtils.getNMSClass("EnumHand")
					.getMethod("valueOf", String.class).invoke(null, mainHand ? "MAIN_HAND" : "OFF_HAND"));
			fields[3].setFloat(getHandler(), facingX);
			fields[4].setFloat(getHandler(), facingY);
			fields[5].setFloat(getHandler(), facingZ);
		} catch (Throwable ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public void refresh() {
		try {
			Field[] fields = getHandler().getClass().getDeclaredFields();
			Arrays.stream(fields).forEach(field -> field.setAccessible(true));
			position = Spigot13HandledUtils.getBlockPositionVector(fields[0].get(getHandler()));
			placedBlockDirection = EnumBlockFace.valueOf((String) NMSUtils.getNMSClass("EnumDirection").getMethod("name").invoke(fields[1].get(getHandler())));
			mainHand = NMSUtils.getNMSClass("EnumHand").getMethod("name").invoke(fields[2].get(getHandler())).equals("MAIN_HAND");
			facingX = fields[3].getFloat(getHandler());
			facingY = fields[4].getFloat(getHandler());
			facingZ = fields[5].getFloat(getHandler());
		} catch (Throwable ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public boolean hasChanged() {
		return changed;
	}
}
