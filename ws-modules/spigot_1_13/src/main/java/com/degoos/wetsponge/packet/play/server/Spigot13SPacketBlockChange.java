package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.material.WSBlockTypes;
import com.degoos.wetsponge.material.block.WSBlockType;
import com.degoos.wetsponge.packet.Spigot13Packet;
import com.degoos.wetsponge.util.reflection.NMSUtils;
import com.degoos.wetsponge.util.reflection.ReflectionUtils;
import com.degoos.wetsponge.util.reflection.Spigot13HandledUtils;
import com.flowpowered.math.vector.Vector3i;

public class Spigot13SPacketBlockChange extends Spigot13Packet implements WSSPacketBlockChange {

	private WSBlockType material;
	private Vector3i position;
	private boolean changed;

	public Spigot13SPacketBlockChange(WSBlockType type, Vector3i position) throws IllegalAccessException, InstantiationException {
		super(NMSUtils.getNMSClass("PacketPlayOutBlockChange").newInstance());
		this.position = position;
		this.material = type.clone();
		update();
	}

	public Spigot13SPacketBlockChange(Object packet) {
		super(packet);
		refresh();
	}

	public void update() {
		try {
			Object state = Spigot13HandledUtils.getBlockState(material);
			ReflectionUtils.setFirstObject(getHandler().getClass(), NMSUtils.getNMSClass("BlockPosition"), getHandler(),
					Spigot13HandledUtils.getBlockPosition(position));
			ReflectionUtils.setFirstObject(getHandler().getClass(), NMSUtils.getNMSClass("IBlockData"), getHandler(), state);
		} catch (Throwable ex) {
			ex.printStackTrace();
		}
	}

	public void refresh() {
		try {
			material = Spigot13HandledUtils.getMaterial(
					ReflectionUtils.getFirstObject(getHandler().getClass(), NMSUtils.getNMSClass("IBlockData"), getHandler()));
			position = Spigot13HandledUtils
					.getBlockPositionVector(ReflectionUtils.getFirstObject(getHandler().getClass(), NMSUtils.getNMSClass("BlockPosition"), getHandler()));
		} catch (Throwable ex) {
			ex.printStackTrace();
			material = WSBlockTypes.AIR.getDefaultState();
			position = new Vector3i(0, 0, 0);
		}
	}

	@Override
	public Vector3i getBlockPosition() {
		return position;
	}

	@Override
	public void setBlockPosition(Vector3i position) {
		this.position = position;
		changed = true;
	}

	@Override
	public WSBlockType getMaterial() {
		changed = true;
		return material;
	}

	@Override
	public void setMaterial(WSBlockType material) {
		this.material = material;
		changed = true;
	}

	@Override
	public boolean hasChanged() {
		return changed;
	}
}
