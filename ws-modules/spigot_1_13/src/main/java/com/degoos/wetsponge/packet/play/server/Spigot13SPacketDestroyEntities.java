package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.packet.Spigot13Packet;
import com.degoos.wetsponge.util.reflection.NMSUtils;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Spigot13SPacketDestroyEntities extends Spigot13Packet implements WSSPacketDestroyEntities {

	private List<Integer> entityIds;
	private boolean changed;

	public Spigot13SPacketDestroyEntities(int... entities) throws IllegalAccessException, InstantiationException {
		super(NMSUtils.getNMSClass("PacketPlayOutEntityDestroy").newInstance());
		entityIds = new ArrayList<>();
		for (int i : entities) entityIds.add(i);
		update();
	}

	public Spigot13SPacketDestroyEntities(Object packet) {
		super(packet);
		entityIds = new ArrayList<>();
		refresh();
	}

	@Override
	public void update() {
		try {
			Field[] fields = getHandler().getClass().getDeclaredFields();
			Arrays.stream(fields).forEach(field -> field.setAccessible(true));
			int[] ids = new int[entityIds.size()];
			for (int i = 0; i < entityIds.size(); i++) ids[i] = entityIds.get(i);
			fields[0].set(getHandler(), ids);
		} catch (Throwable ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public void refresh() {
		try {
			Field[] fields = getHandler().getClass().getDeclaredFields();
			Arrays.stream(fields).forEach(field -> field.setAccessible(true));
			entityIds.clear();
			int[] ids = (int[]) fields[0].get(getHandler());
			for (int i : ids) entityIds.add(i);
		} catch (Throwable ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public List<Integer> getEntityIds() {
		changed = true;
		return entityIds;
	}

	@Override
	public void setEntityIds(List<Integer> entityIds) {
		changed = true;
		this.entityIds = entityIds;
	}

	@Override
	public void addEntityId(int entityId) {
		changed = true;
		entityIds.add(entityId);
	}

	@Override
	public void removeEntityId(int entityId) {
		changed = true;
		entityIds.add(entityId);
	}

	@Override
	public boolean hasChanged() {
		return changed;
	}
}
