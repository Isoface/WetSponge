package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.entity.WSEntity;
import com.degoos.wetsponge.util.reflection.NMSUtils;
import com.flowpowered.math.vector.Vector2i;

public class Spigot13SPacketEntityLook extends Spigot13SPacketEntity implements WSSPacketEntityLook {

    public Spigot13SPacketEntityLook(WSEntity entity, Vector2i rotation, boolean onGround) {
        this(entity.getEntityId(), rotation, onGround);
    }

    public Spigot13SPacketEntityLook(int entity, Vector2i rotation, boolean onGround) {
        super(getPacket(entity, rotation, onGround));
    }

    public Spigot13SPacketEntityLook(Object packet) {
        super(packet);
    }

    public static Object getPacket(int entity, Vector2i rotation, boolean onGround) {
        try {
            return NMSUtils.getNMSClass("PacketPlayOutEntity$PacketPlayOutEntityLook").getConstructor(int.class, byte.class, byte.class, boolean.class)
                    .newInstance(entity, (byte) rotation.getY(), (byte) rotation.getX(), onGround);
        } catch (Throwable ex) {
            ex.printStackTrace();
            return null;
        }
    }
}
