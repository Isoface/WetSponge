package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.entity.Spigot13Entity;
import com.degoos.wetsponge.entity.WSEntity;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.packet.Spigot13Packet;
import com.degoos.wetsponge.text.WSText;
import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.Validate;
import com.degoos.wetsponge.util.reflection.NMSUtils;
import com.degoos.wetsponge.util.reflection.ReflectionUtils;
import com.degoos.wetsponge.util.reflection.Spigot13HandledUtils;
import com.degoos.wetsponge.util.reflection.Spigot13TextUtils;
import org.bukkit.entity.Entity;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class Spigot13SPacketEntityMetadata extends Spigot13Packet implements WSSPacketEntityMetadata {

	private int entityId;
	private List entries;
	private boolean changed;

	private static final Class<?> ITEM_CLASS = NMSUtils.getNMSClass("DataWatcher$Item");
	private static Field dataWatcherObject;

	static {
		try {
			if (WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD))
				dataWatcherObject = ReflectionUtils.setAccessible(NMSUtils.getNMSClass("Entity").getDeclaredField("aE"));
			else dataWatcherObject = null;
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was initializing a packet class!");
			dataWatcherObject = null;
		}
	}

	public Spigot13SPacketEntityMetadata(WSEntity entity) throws IllegalAccessException, InstantiationException {
		super(NMSUtils.getNMSClass("PacketPlayOutEntityMetadata").newInstance());
		Validate.notNull(entity, "Entity cannot be null!");
		this.entityId = entity.getEntityId();
		this.entries = getEntityMetadata(((Spigot13Entity) entity).getHandled());
		if (entries == null) entries = createListOfType(ITEM_CLASS);
		this.changed = false;
		update();
	}

	public Spigot13SPacketEntityMetadata(Object packet) {
		super(packet);
		this.changed = false;
		refresh();
		if (entries == null) entries = createListOfType(ITEM_CLASS);
	}

	@Override
	public int getEntityId() {
		return entityId;
	}

	@Override
	public void setEntityId(int entityId) {
		if (entityId != this.entityId) {
			this.entityId = entityId;
			changed = true;
		}
	}

	@Override
	public void setMetadataOf(WSEntity entity) {
		Validate.notNull(entity, "Entity cannot be null!");
		this.entries = getEntityMetadata(((Spigot13Entity) entity).getHandled());
		changed = true;
	}

	@Override
	public void setCustomName(WSText text) {
		try {
			for (Object o : entries) {
				Class<?> clazz = o.getClass();
				Object key = clazz.getMethod("a").invoke(o);
				int i = (int) key.getClass().getMethod("a").invoke(key);
				if (i == 2) {
					ReflectionUtils.setAccessible(clazz.getDeclaredField("b")).set(o, Optional.ofNullable(text == null ? null : text.toFormattingText())
							.map(Spigot13TextUtils::toIChatBaseComponentFromFormattedText));
					break;
				}
			}
			Object watcherObject = dataWatcherObject.get(null);
			entries.add(ITEM_CLASS.getConstructors()[0].newInstance(watcherObject, Optional.ofNullable(text == null ? null : text.toFormattingText())
					.map(Spigot13TextUtils::toIChatBaseComponentFromFormattedText)));
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was setting a custom name to a metadata packet!");
		}
	}


	@Override
	public void update() {
		try {
			Field[] fields = getHandler().getClass().getDeclaredFields();
			Arrays.stream(fields).forEach(field -> field.setAccessible(true));
			fields[0].setInt(getHandler(), entityId);
			fields[1].set(getHandler(), entries);
		} catch (Throwable ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public void refresh() {
		try {
			Field[] fields = getHandler().getClass().getDeclaredFields();
			Arrays.stream(fields).forEach(field -> field.setAccessible(true));
			this.entityId = fields[0].getInt(getHandler());
			this.entries = (List) fields[1].get(getHandler());
		} catch (Throwable ex) {
			ex.printStackTrace();
		}

	}

	@Override
	public boolean hasChanged() {
		return changed;
	}


	private List<?> getEntityMetadata(Entity entity) {
		try {
			Object handle = Spigot13HandledUtils.getHandle(entity);
			Object watcher = NMSUtils.getNMSClass("Entity").getMethod("getDataWatcher").invoke(handle);
			return (List) NMSUtils.getNMSClass("DataWatcher").getMethod("b").invoke(watcher);
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}

	private <T> List<T> createListOfType(Class<T> type) {
		return new ArrayList<T>();
	}
}
