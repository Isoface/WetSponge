package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.entity.WSEntity;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.packet.Spigot13Packet;
import com.degoos.wetsponge.util.reflection.NMSUtils;
import com.flowpowered.math.vector.Vector2i;
import com.flowpowered.math.vector.Vector3d;

import java.lang.reflect.Field;
import java.util.Arrays;

public class Spigot13SPacketEntityTeleport extends Spigot13Packet implements WSSPacketEntityTeleport {

	private int entityId;
	private Vector3d position;
	private Vector2i rotation;
	private boolean onGround, changed;

	public Spigot13SPacketEntityTeleport(int entityId, Vector3d position, Vector2i rotation, boolean onGround) throws IllegalAccessException, InstantiationException {
		super(NMSUtils.getNMSClass("PacketPlayOutEntityTeleport").newInstance());
		this.entityId = entityId;
		this.position = position;
		this.rotation = rotation;
		this.onGround = onGround;
		update();
	}

	public Spigot13SPacketEntityTeleport(WSEntity entity) throws InstantiationException, IllegalAccessException {
		this(entity.getEntityId(), entity.getLocation().toVector3d(), new Vector2i(entity.getRotation().getY(), entity.getRotation().getX()), entity.isOnGround());
	}

	public Spigot13SPacketEntityTeleport(Object packet) {
		super(packet);
		refresh();
	}

	@Override
	public int getEntityId() {
		return entityId;
	}

	@Override
	public void setEntityId(int entityId) {
		this.entityId = entityId;
		changed = true;
	}

	public Vector3d getPosition() {
		return position;
	}

	public void setPosition(Vector3d position) {
		this.position = position;
		changed = true;
	}

	public Vector2i getRotation() {
		return rotation;
	}

	public void setRotation(Vector2i rotation) {
		this.rotation = rotation;
		changed = true;
	}

	public boolean isOnGround() {
		return onGround;
	}

	public void setOnGround(boolean onGround) {
		this.onGround = onGround;
		changed = true;
	}

	@Override
	public void update() {
		try {
			Field[] fields = getHandler().getClass().getDeclaredFields();
			Arrays.stream(fields).forEach(field -> field.setAccessible(true));
			fields[0].setInt(getHandler(), entityId);
			fields[4].setByte(getHandler(), (byte) (rotation.getX() * 256F / 360F));
			fields[5].setByte(getHandler(), (byte) (rotation.getY() * 256F / 360F));
			fields[6].setBoolean(getHandler(), onGround);

			if (WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD)) {
				fields[1].setDouble(getHandler(), position.getX());
				fields[2].setDouble(getHandler(), position.getY());
				fields[3].setDouble(getHandler(), position.getZ());
			} else {
				fields[1].setInt(getHandler(), (int) Math.floor(position.getX() * 32));
				fields[2].setInt(getHandler(), (int) Math.floor(position.getY() * 32));
				fields[3].setInt(getHandler(), (int) Math.floor(position.getZ() * 32));
			}
		} catch (Throwable ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public void refresh() {
		try {
			Field[] fields = getHandler().getClass().getDeclaredFields();
			Arrays.stream(fields).forEach(field -> field.setAccessible(true));
			entityId = fields[0].getInt(getHandler());
			if (WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD))
				position = new Vector3d(fields[1].getDouble(getHandler()), fields[2].getDouble(getHandler()), fields[3].getDouble(getHandler()));
			else position = new Vector3d(fields[1].getInt(getHandler()) / 32D, fields[2].getInt(getHandler()) / 32D, fields[3].getInt(getHandler()) / 32D);
			rotation = new Vector2i(((int) fields[4].getByte(getHandler())) * 360 / 256, ((int) fields[5].getByte(getHandler())) * 360 / 256);
			onGround = fields[6].getBoolean(getHandler());
		} catch (Throwable ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public boolean hasChanged() {
		return changed;
	}
}
