package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.packet.Spigot13Packet;
import com.degoos.wetsponge.util.reflection.NMSUtils;

import java.lang.reflect.Field;
import java.util.Arrays;

public class Spigot13SPacketHeldItemChange extends Spigot13Packet implements WSSPacketHeldItemChange {

    private int slot;
    private boolean changed;

    public Spigot13SPacketHeldItemChange(int slot) throws IllegalAccessException, InstantiationException {
        super(NMSUtils.getNMSClass("PacketPlayOutHeldItemSlot").newInstance());
        this.slot = slot;
        update();
    }

    public Spigot13SPacketHeldItemChange(Object packet) {
        super(packet);
        refresh();
    }

    @Override
    public void update() {
        try {
            Field[] fields = getHandler().getClass().getDeclaredFields();
            Arrays.stream(fields).forEach(field -> field.setAccessible(true));
            fields[0].setInt(getHandler(), slot);
        } catch (Throwable ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void refresh() {
        try {
            Field[] fields = getHandler().getClass().getDeclaredFields();
            Arrays.stream(fields).forEach(field -> field.setAccessible(true));
            slot = fields[0].getInt(getHandler());
        } catch (Throwable ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public boolean hasChanged() {
        return changed;
    }

    @Override
    public int getSlot() {
        return slot;
    }

    @Override
    public void setSlot(int slot) {
        this.slot = slot;
        changed = true;
    }
}
