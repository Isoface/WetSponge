package com.degoos.wetsponge.parser.entity;


import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.entity.Spigot13Entity;
import com.degoos.wetsponge.entity.WSEntity;
import com.degoos.wetsponge.entity.living.Spigot13Creature;
import com.degoos.wetsponge.entity.living.Spigot13LivingEntity;
import com.degoos.wetsponge.entity.living.player.Spigot13Human;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.enums.EnumEntityType;
import com.degoos.wetsponge.parser.player.PlayerParser;
import com.degoos.wetsponge.util.Validate;
import com.degoos.wetsponge.util.reflection.ReflectionUtils;
import org.bukkit.entity.*;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class Spigot13EntityParser {

	private static Set<Spigot13EntityData> types;
	private static Map<Entity, WSEntity> entities;

	public static void load() {
		types = new HashSet<>();
		entities = new ConcurrentHashMap<>();
		types.add(new Spigot13EntityData(EntityType.AREA_EFFECT_CLOUD, EnumEntityType.AREA_EFFECT_CLOUD));
		types.add(new Spigot13EntityData(EntityType.DRAGON_FIREBALL, EnumEntityType.DRAGON_FIREBALL));
		types.add(new Spigot13EntityData(EntityType.SPLASH_POTION, EnumEntityType.LINGERING_POTION));
		types.add(new Spigot13EntityData(EntityType.SHULKER, EnumEntityType.SHULKER));
		types.add(new Spigot13EntityData(EntityType.SHULKER_BULLET, EnumEntityType.SHULKER_BULLET));
		types.add(new Spigot13EntityData(EntityType.TIPPED_ARROW, EnumEntityType.TIPPED_ARROW));
		types.add(new Spigot13EntityData(EntityType.SPECTRAL_ARROW, EnumEntityType.SPECTRAL_ARROW));
		types.add(new Spigot13EntityData(EntityType.POLAR_BEAR, EnumEntityType.POLAR_BEAR));
		types.add(new Spigot13EntityData(EntityType.ELDER_GUARDIAN, EnumEntityType.ELDER_GUARDIAN));
		types.add(new Spigot13EntityData(EntityType.EVOKER, EnumEntityType.EVOCATION_ILLAGER));
		types.add(new Spigot13EntityData(EntityType.EVOKER_FANGS, EnumEntityType.EVOCATION_FANGS));
		types.add(new Spigot13EntityData(EntityType.HUSK, EnumEntityType.HUSK));
		types.add(new Spigot13EntityData(EntityType.LLAMA, EnumEntityType.LLAMA));
		types.add(new Spigot13EntityData(EntityType.LLAMA_SPIT, EnumEntityType.LLAMA_SPIT));
		types.add(new Spigot13EntityData(EntityType.MULE, EnumEntityType.MULE));
		types.add(new Spigot13EntityData(EntityType.SKELETON_HORSE, EnumEntityType.SKELETON_HORSE));
		types.add(new Spigot13EntityData(EntityType.STRAY, EnumEntityType.STRAY));
		types.add(new Spigot13EntityData(EntityType.VEX, EnumEntityType.VEX));
		types.add(new Spigot13EntityData(EntityType.VINDICATOR, EnumEntityType.VINDICATION_ILLAGER));
		types.add(new Spigot13EntityData(EntityType.WITHER_SKELETON, EnumEntityType.WITHER_SKELETON));
		types.add(new Spigot13EntityData(EntityType.ZOMBIE_HORSE, EnumEntityType.ZOMBIE_HORSE));
		types.add(new Spigot13EntityData(EntityType.ZOMBIE_VILLAGER, EnumEntityType.ZOMBIE_VILLAGER));
		types.add(new Spigot13EntityData(EntityType.DOLPHIN, EnumEntityType.DOLPHIN));
		types.add(new Spigot13EntityData(EntityType.DONKEY, EnumEntityType.DONKEY));
		types.add(new Spigot13EntityData(EntityType.PARROT, EnumEntityType.PARROT));
		types.add(new Spigot13EntityData(EntityType.ARMOR_STAND, EnumEntityType.ARMOR_STAND));
		types.add(new Spigot13EntityData(EntityType.ARROW, EnumEntityType.ARROW, Arrow.class)); //ARROW???
		types.add(new Spigot13EntityData(EntityType.BAT, EnumEntityType.BAT));
		types.add(new Spigot13EntityData(EntityType.BLAZE, EnumEntityType.BLAZE));
		types.add(new Spigot13EntityData(EntityType.BOAT, EnumEntityType.BOAT));
		types.add(new Spigot13EntityData(EntityType.CAVE_SPIDER, EnumEntityType.CAVE_SPIDER));
		types.add(new Spigot13EntityData(EntityType.CHICKEN, EnumEntityType.CHICKEN));
		types.add(new Spigot13EntityData(EntityType.MINECART_COMMAND, EnumEntityType.COMMAND_BLOCK_MINECART));
		types.add(new Spigot13EntityData(EntityType.COMPLEX_PART, EnumEntityType.COMPLEX_PART));
		types.add(new Spigot13EntityData(EntityType.COW, EnumEntityType.COW));
		types.add(new Spigot13EntityData(EntityType.CREEPER, EnumEntityType.CREEPER));
		types.add(new Spigot13EntityData(EntityType.EGG, EnumEntityType.EGG));
		types.add(new Spigot13EntityData(EntityType.ENDER_CRYSTAL, EnumEntityType.ENDER_CRYSTAL));
		types.add(new Spigot13EntityData(EntityType.ENDER_DRAGON, EnumEntityType.ENDER_DRAGON));
		types.add(new Spigot13EntityData(EntityType.ENDERMAN, EnumEntityType.ENDERMAN));
		types.add(new Spigot13EntityData(EntityType.ENDERMITE, EnumEntityType.ENDERMITE));
		types.add(new Spigot13EntityData(EntityType.ENDER_PEARL, EnumEntityType.ENDER_PEARL));
		types.add(new Spigot13EntityData(EntityType.ENDER_SIGNAL, EnumEntityType.EYE_OF_ENDER));
		types.add(new Spigot13EntityData(EntityType.EXPERIENCE_ORB, EnumEntityType.EXPERIENCE_ORB));
		types.add(new Spigot13EntityData(EntityType.MINECART_TNT, EnumEntityType.TNT_MINECART));
		types.add(new Spigot13EntityData(EntityType.FALLING_BLOCK, EnumEntityType.FALLING_BLOCK));
		types.add(new Spigot13EntityData(EntityType.FIREBALL, EnumEntityType.FIREBALL));
		types.add(new Spigot13EntityData(EntityType.FIREWORK, EnumEntityType.FIREWORK));
		types.add(new Spigot13EntityData(EntityType.FISHING_HOOK, EnumEntityType.FISHING_HOOK));
		types.add(new Spigot13EntityData(EntityType.GHAST, EnumEntityType.GHAST));
		types.add(new Spigot13EntityData(EntityType.GIANT, EnumEntityType.GIANT));
		types.add(new Spigot13EntityData(EntityType.GUARDIAN, EnumEntityType.GUARDIAN));
		types.add(new Spigot13EntityData(EntityType.MINECART_HOPPER, EnumEntityType.HOPPER_MINECART));
		types.add(new Spigot13EntityData(EntityType.HORSE, EnumEntityType.HORSE));
		types.add(new Spigot13EntityData(EntityType.IRON_GOLEM, EnumEntityType.IRON_GOLEM));
		types.add(new Spigot13EntityData(EntityType.DROPPED_ITEM, EnumEntityType.ITEM));
		types.add(new Spigot13EntityData(EntityType.ITEM_FRAME, EnumEntityType.ITEM_FRAME));
		types.add(new Spigot13EntityData(EntityType.LEASH_HITCH, EnumEntityType.LEASH_HITCH));
		types.add(new Spigot13EntityData(EntityType.LIGHTNING, EnumEntityType.LIGHTNING));
		types.add(new Spigot13EntityData(EntityType.MAGMA_CUBE, EnumEntityType.MAGMA_CUBE));
		types.add(new Spigot13EntityData(EntityType.MINECART, EnumEntityType.RIDEABLE_MINECART));
		types.add(new Spigot13EntityData(EntityType.MUSHROOM_COW, EnumEntityType.MUSHROOM_COW));
		types.add(new Spigot13EntityData(EntityType.OCELOT, EnumEntityType.OCELOT));
		types.add(new Spigot13EntityData(EntityType.PAINTING, EnumEntityType.PAINTING));
		types.add(new Spigot13EntityData(EntityType.PIG, EnumEntityType.PIG));
		types.add(new Spigot13EntityData(EntityType.PIG_ZOMBIE, EnumEntityType.PIG_ZOMBIE));
		types.add(new Spigot13EntityData(EntityType.PLAYER, EnumEntityType.PLAYER));
		types.add(new Spigot13EntityData(EntityType.MINECART_FURNACE, EnumEntityType.FURNACE_MINECART));
		types.add(new Spigot13EntityData(EntityType.RABBIT, EnumEntityType.RABBIT));
		types.add(new Spigot13EntityData(EntityType.SHEEP, EnumEntityType.SHEEP));
		types.add(new Spigot13EntityData(EntityType.SILVERFISH, EnumEntityType.SILVERFISH));
		types.add(new Spigot13EntityData(EntityType.SKELETON, EnumEntityType.SKELETON));
		types.add(new Spigot13EntityData(EntityType.SLIME, EnumEntityType.SLIME));
		types.add(new Spigot13EntityData(EntityType.SMALL_FIREBALL, EnumEntityType.SMALL_FIREBALL));
		types.add(new Spigot13EntityData(EntityType.SNOWBALL, EnumEntityType.SNOWBALL));
		types.add(new Spigot13EntityData(EntityType.SNOWMAN, EnumEntityType.SNOWMAN));
		types.add(new Spigot13EntityData(EntityType.MINECART_MOB_SPAWNER, EnumEntityType.MOB_SPAWNER_MINECART));
		types.add(new Spigot13EntityData(EntityType.SPIDER, EnumEntityType.SPIDER));
		types.add(new Spigot13EntityData(EntityType.SPLASH_POTION, EnumEntityType.SPLASH_POTION));
		types.add(new Spigot13EntityData(EntityType.SQUID, EnumEntityType.SQUID));
		types.add(new Spigot13EntityData(EntityType.MINECART_CHEST, EnumEntityType.CHESTED_MINECART));
		types.add(new Spigot13EntityData(EntityType.THROWN_EXP_BOTTLE, EnumEntityType.THROWN_EXP_BOTTLE));
		types.add(new Spigot13EntityData(EntityType.PRIMED_TNT, EnumEntityType.PRIMED_TNT));
		types.add(new Spigot13EntityData(EntityType.VILLAGER, EnumEntityType.VILLAGER));
		types.add(new Spigot13EntityData(EntityType.WEATHER, EnumEntityType.WEATHER));
		types.add(new Spigot13EntityData(EntityType.WITCH, EnumEntityType.WITCH));
		types.add(new Spigot13EntityData(EntityType.WITHER, EnumEntityType.WITHER));
		types.add(new Spigot13EntityData(EntityType.WITHER_SKULL, EnumEntityType.WITHER_SKULL));
		types.add(new Spigot13EntityData(EntityType.WOLF, EnumEntityType.WOLF));
		types.add(new Spigot13EntityData(EntityType.ZOMBIE, EnumEntityType.ZOMBIE));
	}

	public static EnumEntityType getEntityType(Entity entity) {
		return types.stream().filter(data -> data.getEntityClass().isInstance(entity)).findAny().map(Spigot13EntityData::getEntityType).orElse(EnumEntityType.UNKNOWN);
	}

	public static Spigot13EntityData getEntityData(Entity entity) {
		return types.stream().filter(data -> data.getEntityClass().isInstance(entity)).findAny().orElse(null);
	}

	public static EntityType getSuperEntityType(EnumEntityType type) {
		return types.stream().filter(data -> data.getEntityType() == type).findAny().map(Spigot13EntityData::getSpongeEntityType).orElse(EntityType.UNKNOWN);
	}

	public static Spigot13EntityData getEntityData(EnumEntityType type) {
		return types.stream().filter(data -> data.getEntityType() == type).findAny().orElse(null);
	}


	public static EnumEntityType getEntityType(EntityType type) {
		return types.stream().filter(data -> data.getSpongeEntityType().equals(type)).findAny().map(Spigot13EntityData::getEntityType).orElse(EnumEntityType.UNKNOWN);
	}

	public static Spigot13EntityData getEntityData(EntityType type) {
		return types.stream().filter(data -> data.getSpongeEntityType().equals(type)).findAny().orElse(null);
	}


	public static WSEntity getWSEntity(Entity entity) {
		Validate.notNull(entity, "Entity cannot be null!");
		if (entities.containsKey(entity)) return entities.get(entity);
		else return addEntity(entity);
	}

	private static WSEntity createHandlerEntity(Entity entity) {
		Validate.notNull(entity, "Entity cannot be null!");
		if (entity instanceof Player) return PlayerParser.getOrCreatePlayer(entity, entity.getUniqueId());
		if (entity instanceof HumanEntity) return new Spigot13Human((HumanEntity) entity);

		Spigot13EntityData data = getEntityData(entity);
		if (data != null && data.getEntityType().getServerClass() != null) {
			try {
				return (WSEntity) ReflectionUtils.getConstructor(data.getEntityType().getServerClass(), data.getEntityClass()).newInstance(entity);
			} catch (Throwable e) {
				e.printStackTrace();
			}
		}

		if (entity instanceof Creature) return new Spigot13Creature((Creature) entity);
		if (entity instanceof LivingEntity) return new Spigot13LivingEntity((LivingEntity) entity);

		return new Spigot13Entity(entity);
	}


	public static WSEntity addEntity(Entity entity) {
		Validate.notNull(entity, "Entity cannot be null!");
		WSEntity wsEntity = createHandlerEntity(entity);
		entities.put(entity, wsEntity);
		return wsEntity;
	}

	public static void removeEntity(Entity entity) {
		Validate.notNull(entity, "Entity cannot be null!");
		entities.remove(entity);
	}

	public static Optional<WSEntity> getWSEntity(UUID uuid) {
		Optional<WSPlayer> player = WetSponge.getServer().getPlayer(uuid);
		if (player.isPresent()) return Optional.ofNullable(player.get());
		for (WSEntity entity : Collections.unmodifiableCollection(entities.values()))
			if (entity != null && entity.getUniqueId() != null && entity.getUniqueId().equals(uuid)) return Optional.of(entity);
		return Optional.empty();
	}

	public static Optional<WSEntity> getWSEntity(int id) {
		return entities.values().stream().filter(target -> target != null && target.getEntityId() == id).findAny();
	}

	public static boolean containsValue(EnumEntityType type) {
		return types.stream().anyMatch(data -> data.getEntityType() == type);
	}

}
