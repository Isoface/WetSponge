package com.degoos.wetsponge.plugin;

import org.bukkit.plugin.Plugin;

import java.util.List;
import java.util.Objects;

public class Spigot13BasePlugin implements WSBasePlugin {

	private Plugin plugin;

	public Spigot13BasePlugin(Plugin plugin) {
		this.plugin = plugin;
	}


	@Override
	public String getName() {
		return plugin.getName();
	}

	@Override
	public String getVersion() {
		return plugin.getDescription().getVersion();
	}

	@Override
	public String getDescription() {
		return plugin.getDescription().getDescription();
	}

	@Override
	public String getUrl() {
		return plugin.getDescription().getWebsite();
	}

	@Override
	public List<String> getAuthors() {
		return plugin.getDescription().getAuthors();
	}

	@Override
	public List<String> getDependencies() {
		return plugin.getDescription().getDepend();
	}

	@Override
	public List<String> getSoftDependencies() {
		return plugin.getDescription().getSoftDepend();
	}

	@Override
	public Object getHandled() {
		return plugin;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Spigot13BasePlugin that = (Spigot13BasePlugin) o;
		return Objects.equals(plugin, that.plugin);
	}

	@Override
	public int hashCode() {
		return Objects.hash(plugin);
	}

	@Override
	public String toString() {
		return "{BasePlugin " + getName() + " version " + getVersion() + "}";
	}
}
