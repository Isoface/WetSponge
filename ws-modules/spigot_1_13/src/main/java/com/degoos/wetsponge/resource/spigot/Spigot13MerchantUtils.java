package com.degoos.wetsponge.resource.spigot;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.entity.living.merchant.Spigot13Villager;
import com.degoos.wetsponge.entity.living.merchant.WSVillager;
import com.degoos.wetsponge.entity.living.player.Spigot13Player;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.item.Spigot13ItemStack;
import com.degoos.wetsponge.merchant.OldSpigotTrade;
import com.degoos.wetsponge.merchant.WSTrade;
import com.degoos.wetsponge.util.reflection.NMSUtils;
import com.degoos.wetsponge.util.reflection.ReflectionUtils;
import com.degoos.wetsponge.util.reflection.Spigot13HandledUtils;
import org.bukkit.Material;
import org.bukkit.entity.Villager;
import org.bukkit.inventory.ItemStack;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Spigot13MerchantUtils {

	public static Class<?> getOBCItemStackClass() {
		return NMSUtils.getOBCClass("inventory.CraftItemStack");
	}

	public static Class<?> getNMSMerchantRecipeClass() {
		return NMSUtils.getNMSClass("MerchantRecipe");
	}


	public static ItemStack asBukkitCopy(Object nmsItemStack) {
		try {
			Method m = getOBCItemStackClass().getDeclaredMethod("asBukkitCopy", NMSUtils.getNMSClass("ItemStack"));
			m.setAccessible(true);
			return (ItemStack) m.invoke(null, nmsItemStack);
		} catch (Throwable e) {
			e.printStackTrace();
			return null;
		}
	}


	public static Object asNMSCopy(ItemStack stack) {
		try {
			Method m = getOBCItemStackClass().getDeclaredMethod("asNMSCopy", ItemStack.class);
			m.setAccessible(true);
			return m.invoke(null, stack);
		} catch (Throwable e) {
			e.printStackTrace();
			return null;
		}
	}

	public static Object createNMSRecipe(WSTrade trade) {
		try {
			Object item1, item2, item3;
			item1 = asNMSCopy(((Spigot13ItemStack) trade.getFirstItem()).getHandled());
			if (!trade.getSecondItem().isPresent()) item2 = null;
			else item2 = asNMSCopy(((Spigot13ItemStack) trade.getSecondItem().get()).getHandled());
			item3 = asNMSCopy(((Spigot13ItemStack) trade.getResult()).getHandled());
			Class<?> isClass = NMSUtils.getNMSClass("ItemStack");
			return getNMSMerchantRecipeClass().getDeclaredConstructor(isClass, isClass, isClass, int.class, int.class)
				.newInstance(item1, item2, item3, trade.getUses(), trade.getMaxUses());
		} catch (Throwable e) {
			e.printStackTrace();
			return null;
		}
	}

	public static WSTrade createWSTrade(Object nmsOffer) {
		Field[] fields = getNMSMerchantRecipeClass().getDeclaredFields();
		Arrays.stream(fields).forEach(field -> field.setAccessible(true));
		try {
			return new OldSpigotTrade(fields[3].getInt(nmsOffer), fields[4].getInt(nmsOffer), new Spigot13ItemStack(asBukkitCopy(fields[0]
				.get(nmsOffer))), new Spigot13ItemStack(asBukkitCopy(fields[1].get(nmsOffer))), new Spigot13ItemStack(asBukkitCopy(fields[2].get(nmsOffer))), fields[5]
				.getBoolean(nmsOffer));
		} catch (Throwable ex) {
			ex.printStackTrace();
			return null;
		}
	}

	public static ArrayList<?> getMerchantRecipeList(WSVillager villager) {
		Villager spigotVillager = ((Spigot13Villager) villager).getHandled();
		Object minecraftVillager = Spigot13HandledUtils.getEntityHandle(spigotVillager);
		try {
			Field field = NMSUtils.getNMSClass("EntityVillager").getDeclaredField("br");
			ReflectionUtils.setAccessible(field);
			ArrayList<?> arrayList = (ArrayList<?>) field.get(minecraftVillager);
			if (arrayList == null) createNewList();
			return arrayList;
		} catch (Throwable e) {
			e.printStackTrace();
			return createNewList();
		}
	}

	public static ArrayList<?> createNewList() {
		try {
			return (ArrayList<?>) NMSUtils.getNMSClass("MerchantRecipeList").newInstance();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static void setMerchantRecipeList(WSVillager villager, List<?> arrayList) {
		Villager spigotVillager = ((Spigot13Villager) villager).getHandled();
		Object minecraftVillager = Spigot13HandledUtils.getEntityHandle(spigotVillager);
		try {
			Field field = NMSUtils.getNMSClass("EntityVillager").getDeclaredField("br");
			ReflectionUtils.setAccessible(field);
			field.set(minecraftVillager, arrayList);
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	public static List<WSTrade> getTrades(WSVillager villager) {
		return getMerchantRecipeList(villager).stream().map(Spigot13MerchantUtils::createWSTrade).collect(Collectors.toList());
	}

	public static void setTrades(WSVillager villager, List<WSTrade> trades) {
		List<?> list = createNewList();
		trades.stream().map(Spigot13MerchantUtils::createNMSRecipe).forEach(trade -> add(list, trade));
		setMerchantRecipeList(villager, list);
	}

	private static void add(List<?> list, Object recipe) {
		try {
			Method m = ReflectionUtils.getMethodByName(List.class, "add");
			m.setAccessible(true);
			m.invoke(list, recipe);
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	public static void openGUI(WSPlayer player, WSVillager villager) {
		try {
			Villager spigotVillager = ((Spigot13Villager) villager).getHandled();
			Object entityPlayer = Spigot13HandledUtils.getPlayerHandle(((Spigot13Player) player).getHandled());
			Object v = spigotVillager.getClass().getDeclaredMethod("getHandle").invoke(spigotVillager);

			if (WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD)) {
				if (WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_1_10_2))
					v.getClass().getDeclaredMethod("a", NMSUtils.getNMSClass("EntityHuman"), NMSUtils.getNMSClass("EnumHand"))
						.invoke(v, entityPlayer, NMSUtils.getNMSClass("EnumHand").getEnumConstants()[0]);
				else v.getClass().getDeclaredMethod("a", NMSUtils.getNMSClass("EntityHuman"), NMSUtils.getNMSClass("EnumHand"), NMSUtils.getNMSClass("ItemStack"))
					.invoke(v, entityPlayer, NMSUtils.getNMSClass("EnumHand").getEnumConstants()[0], NMSUtils.getOBCClass("inventory.CraftItemStack")
						.getMethod("asNMSCopy", ItemStack.class).invoke(null, new ItemStack(Material.STONE)));
			} else v.getClass().getDeclaredMethod("a", NMSUtils.getNMSClass("EntityHuman")).invoke(v, entityPlayer);
		} catch (Throwable ex) {
			ex.printStackTrace();
		}
	}


}
