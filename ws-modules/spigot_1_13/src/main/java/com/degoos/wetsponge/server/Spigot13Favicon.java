package com.degoos.wetsponge.server;

import com.degoos.wetsponge.util.InternalLogger;
import com.google.common.base.Charsets;
import com.google.common.base.Preconditions;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufInputStream;
import io.netty.buffer.ByteBufOutputStream;
import io.netty.buffer.Unpooled;
import io.netty.handler.codec.base64.Base64;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;

public class Spigot13Favicon implements WSFavicon {

	private BufferedImage image;
	private String encoded;

	public Spigot13Favicon(BufferedImage image) {
		this.image = image;
		try {
			Preconditions.checkArgument(image.getWidth() == 64, "favicon must be 64 pixels wide");
			Preconditions.checkArgument(image.getHeight() == 64, "favicon must be 64 pixels high");
			ByteBuf buf = Unpooled.buffer();
			try {
				ImageIO.write(image, "PNG", new ByteBufOutputStream(buf));
				ByteBuf base64 = Base64.encode(buf);
				try {
					encoded = "data:image/png;base64," + base64.toString(Charsets.UTF_8);
				} finally {
					base64.release();
				}
			} finally {
				buf.release();
			}
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An exception has occurred while WetSponge was trying to decode a server icon!");
			encoded = "data:image/png;base64,";
		}
	}

	public Spigot13Favicon(String encoded) {
		this.encoded = encoded;
		try {
			Preconditions.checkArgument(encoded.startsWith("data:image/png;base64,"), "Unknown favicon format");
			ByteBuf base64 = Unpooled.copiedBuffer(encoded.substring("data:image/png;base64,".length()), Charsets.UTF_8);
			try {
				ByteBuf buf = Base64.decode(base64);
				try {
					BufferedImage result = ImageIO.read(new ByteBufInputStream(buf));
					Preconditions.checkState(result.getWidth() == 64, "favicon must be 64 pixels wide");
					Preconditions.checkState(result.getHeight() == 64, "favicon must be 64 pixels high");
					image = result;
				} finally {
					buf.release();
				}
			} finally {
				base64.release();
			}
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An exception has occurred while WetSponge was trying to decode a server icon!");
			image = new BufferedImage(64, 64, BufferedImage.TYPE_INT_RGB);
			Graphics graphics = image.getGraphics();
			graphics.drawString("Error", 20, 32);
		}
	}

	@Override
	public String toBase64() {
		return encoded;
	}

	@Override
	public BufferedImage getImage() {
		return image;
	}
}
