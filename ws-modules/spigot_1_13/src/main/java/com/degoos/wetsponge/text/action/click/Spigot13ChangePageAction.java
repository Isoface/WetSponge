package com.degoos.wetsponge.text.action.click;

import com.degoos.wetsponge.util.NumericUtils;
import net.md_5.bungee.api.chat.ClickEvent;

public class Spigot13ChangePageAction extends Spigot13ClickAction implements WSChangePageAction {

    public Spigot13ChangePageAction(ClickEvent event) {
        super(event);
    }

    public Spigot13ChangePageAction(int page) {
        super(new ClickEvent(ClickEvent.Action.CHANGE_PAGE, String.valueOf(page)));
    }


    @Override
    public int getPage() {
        return NumericUtils.isInteger(getHandled().getValue()) ? Integer.parseInt(getHandled().getValue()) : 0;
    }
}
