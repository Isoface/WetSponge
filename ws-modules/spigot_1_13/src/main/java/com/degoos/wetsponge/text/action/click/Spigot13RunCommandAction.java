package com.degoos.wetsponge.text.action.click;

import net.md_5.bungee.api.chat.ClickEvent;

public class Spigot13RunCommandAction extends Spigot13ClickAction implements WSRunCommandAction {

    public Spigot13RunCommandAction(ClickEvent event) {
        super(event);
    }

    public Spigot13RunCommandAction(String command) {
        super(new ClickEvent(ClickEvent.Action.RUN_COMMAND, command));
    }

    @Override
    public String getCommand() {
        return getHandled().getValue();
    }
}
