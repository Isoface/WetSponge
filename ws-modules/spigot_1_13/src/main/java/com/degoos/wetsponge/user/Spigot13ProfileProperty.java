package com.degoos.wetsponge.user;

import com.mojang.authlib.properties.Property;

import javax.annotation.Nullable;
import java.util.Optional;

public class Spigot13ProfileProperty implements WSProfileProperty {


	public static WSProfileProperty of(String name, String value, @Nullable String signature) {
		return new Spigot13ProfileProperty(new Property(name, value, signature));
	}

	private Property profileProperty;


	public Spigot13ProfileProperty(Property profileProperty) {
		this.profileProperty = profileProperty;
	}

	@Override
	public String getName() {
		return profileProperty.getName();
	}

	@Override
	public String getValue() {
		return profileProperty.getValue();
	}

	@Override
	public Optional<String> getSignature() {
		return profileProperty.hasSignature() ? Optional.empty() : Optional.of(profileProperty.getSignature());
	}

	@Override
	public Property getHandled() {
		return profileProperty;
	}
}
