package com.degoos.wetsponge.user;


import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.parser.player.PlayerParser;
import com.degoos.wetsponge.util.Validate;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;

import java.util.Optional;
import java.util.UUID;

public class Spigot13User implements WSUser {

    public static Spigot13User of(UUID uuid) {
        return new Spigot13User(Bukkit.getOfflinePlayer(uuid));
    }

    private OfflinePlayer offlinePlayer;


    public Spigot13User(OfflinePlayer offlinePlayer) {
        Validate.notNull(offlinePlayer, "Offline player cannot be null!");
        this.offlinePlayer = offlinePlayer;
    }


    @Override
    public boolean isOnline() {
        return offlinePlayer.isOnline();
    }


    @Override
    public String getName() {
        return offlinePlayer.getName();
    }


    @Override
    public UUID getUniqueId() {
        return offlinePlayer.getUniqueId();
    }


    @Override
    public boolean isBanned() {
        return offlinePlayer.isBanned();
    }


    @Override
    public boolean isWhitelisted() {
        return offlinePlayer.isWhitelisted();
    }


    @Override
    public void setWhitelisted(boolean whitelisted) {
        offlinePlayer.setWhitelisted(whitelisted);
    }


    @Override
    public Optional<WSPlayer> getPlayer() {
        return Optional.ofNullable(offlinePlayer.getPlayer()).map(player -> PlayerParser.getOrCreatePlayer(player, player.getUniqueId()));
    }


    @Override
    public long getFirstPlayed() {
        return offlinePlayer.getFirstPlayed();
    }


    @Override
    public long getLastPlayed() {
        return offlinePlayer.getLastPlayed();
    }


    @Override
    public boolean hasPlayedBefore() {
        return offlinePlayer.hasPlayedBefore();
    }


    @Override
    public OfflinePlayer getHandled() {
        return offlinePlayer;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Spigot13User that = (Spigot13User) o;

        return offlinePlayer.equals(that.offlinePlayer);
    }


    @Override
    public int hashCode() {
        return offlinePlayer.hashCode();
    }
}
