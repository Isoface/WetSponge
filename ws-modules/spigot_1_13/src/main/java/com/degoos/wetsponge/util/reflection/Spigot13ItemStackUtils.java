package com.degoos.wetsponge.util.reflection;

import com.degoos.wetsponge.resource.spigot.Spigot13MerchantUtils;
import org.bukkit.inventory.ItemStack;

public class Spigot13ItemStackUtils {

	public static String getDisplayName(ItemStack itemStack) {
		try {
			Object is = Spigot13MerchantUtils.asNMSCopy(itemStack);
			Object nbt = ReflectionUtils.invokeMethod(is, "d", "display");
			if (nbt != null) {
				if ((boolean) ReflectionUtils.invokeMethod(nbt, "hasKeyOfType", "Name", 8)) return (String) ReflectionUtils.invokeMethod(nbt, "getString", "Name");
				if ((boolean) ReflectionUtils.invokeMethod(nbt, "hasKeyOfType", "LocName", 8)) return (String) ReflectionUtils.invokeMethod(nbt, "getString", "LocName");
			}

			return ReflectionUtils.invokeMethod(ReflectionUtils.invokeMethod(is, "getItem"), "getName") + ".name";
		} catch (Exception ex) {
			return "";
		}
	}

}
