package com.degoos.wetsponge.util.reflection;


import com.degoos.wetsponge.util.InternalLogger;

public class Spigot13TextUtils {

	private static Class<?> clazz = NMSUtils.getNMSClass("IChatBaseComponent");

	public static String toJSON(Object iChatBaseComponent) {
		try {
			return (String) NMSUtils.getNMSClass("IChatBaseComponent").getDeclaredClasses()[0].getMethod("a", new Class[]{clazz})
					.invoke(null, iChatBaseComponent);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}


	public static Object toIChatBaseComponentFromJSON(String json) {
		try {
			return NMSUtils.getNMSClass("IChatBaseComponent").getDeclaredClasses()[0].getMethod("a", new Class[]{String.class})
					.invoke(null, json);
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was creating a IChatBaseComponent!");
			return null;
		}
	}

	public static Object toIChatBaseComponentFromFormattedText(String text) {
		try {
			return NMSUtils.getNMSClass("IChatBaseComponent").getDeclaredClasses()[0].getMethod("a", new Class[]{String.class})
					.invoke(null, "{\"text\":\"" + text + "\"}");
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was creating a IChatBaseComponent!");
			return null;
		}
	}

}
