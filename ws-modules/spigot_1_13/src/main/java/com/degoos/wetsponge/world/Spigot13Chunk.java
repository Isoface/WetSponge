package com.degoos.wetsponge.world;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.block.Spigot13Block;
import com.degoos.wetsponge.block.WSBlock;
import com.degoos.wetsponge.block.tileentity.Spigot13TileEntity;
import com.degoos.wetsponge.block.tileentity.WSTileEntity;
import com.degoos.wetsponge.entity.WSEntity;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.listener.spigot.Spigot13WorldListener;
import com.degoos.wetsponge.parser.entity.Spigot13EntityParser;
import com.degoos.wetsponge.parser.world.WorldParser;
import com.degoos.wetsponge.util.reflection.Spigot13HandledUtils;
import org.bukkit.Chunk;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

public class Spigot13Chunk implements WSChunk {

	private Chunk chunk;

	public Spigot13Chunk(Chunk chunk) {
		this.chunk = chunk;
	}


	@Override
	public WSWorld getWorld() {
		return WorldParser.getOrCreateWorld(chunk.getWorld().getName(), chunk.getWorld());
	}

	@Override
	public int getX() {
		return chunk.getX();
	}

	@Override
	public int getZ() {
		return chunk.getZ();
	}

	@Override
	public WSBlock getBlock(int x, int y, int z) {
		return new Spigot13Block(chunk.getBlock(x, y, z));
	}

	@Override
	public Set<WSEntity> getEntities() {
		return Arrays.stream(chunk.getEntities()).map(Spigot13EntityParser::getWSEntity).collect(Collectors.toSet());
	}

	@Override
	public Set<WSTileEntity> getTileEntities() {
		return Arrays.stream(chunk.getTileEntities()).map(Spigot13TileEntity::new).collect(Collectors.toSet());
	}

	@Override
	public boolean load(boolean generate) {
		return chunk.load(generate);
	}

	@Override
	public void addToUnloadQueue() {
		Object chunkProvider = getWorld().getChunkProvider();
		try {
			if (WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD)) {
				Object chunkHandled = Spigot13HandledUtils.getHandle(chunk);
				chunkProvider.getClass().getMethod("unload", chunkHandled.getClass()).invoke(chunkProvider, chunkHandled);
			} else chunkProvider.getClass().getMethod("queueUnload", int.class, int.class).invoke(chunkProvider, getX(), getZ());
		} catch (Throwable ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public boolean isLoaded() {
		return chunk.isLoaded();
	}

	@Override
	public boolean canBeUnloaded() {
		return !Spigot13WorldListener.dontUnloadChunks.contains(chunk);
	}

	@Override
	public void setCanBeUnloaded(boolean canBeUnloaded) {
		if (canBeUnloaded) Spigot13WorldListener.dontUnloadChunks.remove(chunk);
		else Spigot13WorldListener.dontUnloadChunks.add(chunk);
	}

	@Override
	public boolean canBeSaved() {
		Object object = getWorld().getChunkProvider();
		try {
			return !getList("getCantSave", object).contains(Spigot13HandledUtils.getHandle(chunk));
		} catch (Throwable ex) {
			ex.printStackTrace();
			return true;
		}
	}

	@Override
	public void setCanBeSaved(boolean canSave) {
		Object object = getWorld().getChunkProvider();
		try {
			if (canSave) getList("getCantSave", object).remove(Spigot13HandledUtils.getHandle(chunk));
			else getList("getCantSave", object).add(Spigot13HandledUtils.getHandle(chunk));
		} catch (Throwable ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public Chunk getHandled() {
		return chunk;
	}

	public Object getBukkitHandled() {
		return Spigot13HandledUtils.getHandle(chunk);
	}

	private ArrayList getList(String getterName, Object object) throws Exception {
		return (ArrayList) object.getClass().getMethod(getterName).invoke(object);
	}
}
