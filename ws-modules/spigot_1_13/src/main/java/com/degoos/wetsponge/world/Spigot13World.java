package com.degoos.wetsponge.world;


import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.entity.Spigot13Entity;
import com.degoos.wetsponge.entity.WSEntity;
import com.degoos.wetsponge.entity.living.player.WSHuman;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.enums.EnumBiomeType;
import com.degoos.wetsponge.enums.EnumEntityType;
import com.degoos.wetsponge.enums.EnumMapBaseColor;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.map.WSMapView;
import com.degoos.wetsponge.material.WSBlockTypes;
import com.degoos.wetsponge.material.block.WSBlockType;
import com.degoos.wetsponge.mixin.spigot.Spigot13WSEntityHumanAssist;
import com.degoos.wetsponge.parser.entity.Spigot13EntityParser;
import com.degoos.wetsponge.parser.player.PlayerParser;
import com.degoos.wetsponge.user.WSGameProfile;
import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.Validate;
import com.degoos.wetsponge.util.reflection.NMSUtils;
import com.degoos.wetsponge.util.reflection.ReflectionUtils;
import com.degoos.wetsponge.util.reflection.Spigot13HandledUtils;
import com.degoos.wetsponge.util.reflection.Spigot13MapUtils;
import com.degoos.wetsponge.world.generation.WSWorldGenerator;
import com.flowpowered.math.vector.Vector2i;
import com.flowpowered.math.vector.Vector3d;
import com.flowpowered.math.vector.Vector3i;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Biome;
import org.bukkit.entity.Entity;
import org.bukkit.entity.HumanEntity;
import org.bukkit.event.entity.CreatureSpawnEvent;

import java.io.File;
import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.*;
import java.util.stream.Collectors;

public class Spigot13World implements WSWorld {

	private World world;
	private WSWorldProperties worldProperties;
	private Object chunkProvider, chunkLoader;
	private WSWorldBorder worldBorder;
	private Map<Integer, WSMapView> mapViews;

	public Spigot13World(Object world) {
		this((World) world);
	}

	public Spigot13World(World world) {
		Validate.notNull(world, "World cannot be null!");
		this.world = world;
		this.worldBorder = new Spigot13WorldBorder(world.getWorldBorder());
		mapViews = new HashMap<>();
		this.worldProperties = new Spigot13WorldProperties(world);
		chunkProvider = Spigot13WorldInjector.inject(world);
		try {
			chunkLoader = ReflectionUtils.getFirstObject(NMSUtils.getNMSClass("ChunkProviderServer"), NMSUtils.getNMSClass("IChunkLoader"), chunkProvider);
		} catch (Exception e) {
			InternalLogger.printException(e, "An exception has occurred while WetSponge was loading a world.");
		}
	}

	@Override
	public String getName() {
		return world.getName();
	}


	@Override
	public Set<WSEntity> getEntities() {
		return world.getEntities().stream().map(Spigot13EntityParser::getWSEntity).collect(Collectors.toSet());
	}

	@Override
	public Set<WSPlayer> getPlayers() {
		return getHandled().getPlayers().stream().map(player -> PlayerParser.getOrCreatePlayer(player, player.getUniqueId())).collect(Collectors.toSet());
	}


	@Override
	public UUID getUniqueId() {
		return world.getUID();
	}

	@Override
	public <T extends WSEntity> Optional<T> spawnEntity(Class<T> entityClass, Vector3d location) {
		WetSponge.getTimings().startTiming("Spawn entity");
		try {
			Optional<T> entity = createEntity(entityClass, location);
			entity.ifPresent(this::spawnEntity);
			WetSponge.getTimings().stopTiming();
			return entity;
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was spawning an entity!");
			WetSponge.getTimings().stopTiming();
			return Optional.empty();
		}
	}

	@Override
	public Optional<WSEntity> spawnEntity(EnumEntityType type, Vector3d location) {
		WetSponge.getTimings().startTiming("Spawn entity");
		try {
			Optional<WSEntity> entity = createEntity(type, location);
			entity.ifPresent(this::spawnEntity);
			WetSponge.getTimings().stopTiming();
			return entity;
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was spawning an entity!");
			WetSponge.getTimings().stopTiming();
			return Optional.empty();
		}
	}

	@Override
	public boolean spawnEntity(WSEntity entity) {
		WetSponge.getTimings().startTiming("Spawn entity");
		try {
			NMSUtils.getOBCClass("CraftWorld").getMethod("addEntity", NMSUtils.getNMSClass("Entity"), CreatureSpawnEvent.SpawnReason.class)
					.invoke(world, Spigot13HandledUtils.getEntityHandle(((Spigot13Entity) entity).getHandled()), CreatureSpawnEvent.SpawnReason.CUSTOM);
			WetSponge.getTimings().stopTiming();
			return true;
		} catch (Throwable ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was spawning an entity!");
			WetSponge.getTimings().stopTiming();
			return false;
		}
	}

	@Override
	public <T extends WSEntity> Optional<T> createEntity(Class<T> entityClass, Vector3d location) {
		return (Optional<T>) createEntity(EnumEntityType.getByClass(entityClass).orElse(EnumEntityType.UNKNOWN), location);
	}

	@Override
	public Optional<WSEntity> createEntity(EnumEntityType type, Vector3d location) {
		WetSponge.getTimings().startTiming("Create entity");
		if (type == EnumEntityType.UNKNOWN) {
			WetSponge.getTimings().stopTiming();
			return Optional.empty();
		}
		try {
			Class<? extends Entity> spigotClass = Spigot13EntityParser.getEntityData(type).getEntityClass();
			Object entity;
			if (type == EnumEntityType.ITEM) {
				Object worldHandled = Spigot13HandledUtils.getWorldHandle(world);
				entity = NMSUtils.getNMSClass("EntityItem").getConstructor(NMSUtils.getNMSClass("World"), double.class, double.class, double.class)
						.newInstance(worldHandled, location.getX(), location.getY(), location.getZ());
			} else entity = ReflectionUtils.getMethodByName(NMSUtils.getOBCClass("CraftWorld"), "createEntity")
					.invoke(world, new Location(world, location.getX(), location.getY(), location.getZ()), spigotClass);
			Entity bukkitEntity = entity == null ? null : (Entity) entity.getClass().getMethod("getBukkitEntity").invoke(entity);
			Optional<WSEntity> optionalEntity = Optional.ofNullable(bukkitEntity).map(Spigot13EntityParser::getWSEntity);
			WetSponge.getTimings().stopTiming();
			return optionalEntity;
		} catch (Throwable ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was creating an entity!");
			WetSponge.getTimings().stopTiming();
			return Optional.empty();
		}
	}

	@Override
	public Optional<WSHuman> createNPC(WSGameProfile gameProfile, Vector3d vector3d) {
		WetSponge.getTimings().startTiming("Create entity");
		try {
			Class<?> worldClass = NMSUtils.getNMSClass("World");

			Object world = Spigot13HandledUtils.getWorldHandle(this.world);


			Object player = Spigot13WSEntityHumanAssist.ENTITY_HUMAN_CLASS.getConstructor(worldClass,
					gameProfile.getHandled().getClass()).newInstance(world, gameProfile.getHandled());

			HumanEntity bukkitPlayer = (HumanEntity) player.getClass().getMethod("getBukkitEntity").invoke(player);
			WSEntity entity = Spigot13EntityParser.getWSEntity(bukkitPlayer);
			if (!(entity instanceof WSHuman)) return Optional.empty();
			WetSponge.getTimings().stopTiming();
			return Optional.ofNullable((WSHuman) entity);
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was creating a NPC!");
			WetSponge.getTimings().stopTiming();
			return Optional.empty();
		}
	}

	@Override
	public WSWorldProperties getProperties() {
		return worldProperties;
	}

	@Override
	public WSWorldGenerator getGenerator() {
		return (WSWorldGenerator) world.getGenerator();
	}

	@Override
	public Optional<WSChunk> getChunk(int x, int z) {
		return Optional.ofNullable(world.getChunkAt(x, z)).map(Spigot13Chunk::new);
	}

	@Override
	public Optional<WSChunk> getChunkAtLocation(int x, int z) {
		return Optional.ofNullable(world.getChunkAt(x >> 4, z >> 4)).map(Spigot13Chunk::new);
	}

	@Override
	public Optional<WSChunk> loadChunk(int x, int z, boolean shouldGenerate) {
		try {
			WetSponge.getTimings().startTiming("Load chunk");
			world.loadChunk(x, z, shouldGenerate);
			Optional<WSChunk> chunk = getChunk(x, z);
			WetSponge.getTimings().stopTiming();
			return chunk;
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was loading a chunk!");
			WetSponge.getTimings().stopTiming();
			return Optional.empty();
		}
	}

	@Override
	public Optional<WSChunk> loadChunkAtLocation(int x, int z, boolean shouldGenerate) {
		return loadChunk(x >> 4, z >> 4, shouldGenerate);
	}

	@Override
	public Set<WSChunk> getLoadedChunks() {
		return Arrays.stream(world.getLoadedChunks()).map(Spigot13Chunk::new).collect(Collectors.toSet());
	}

	@Override
	public EnumBiomeType getBiome(int x, int y, int z) {
		if (WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD))
			return EnumBiomeType.getBySpigotName(world.getBiome(x, z).name()).orElseThrow(NullPointerException::new);
		else return EnumBiomeType.getByOldSpigotName(world.getBiome(x, z).name()).orElseThrow(NullPointerException::new);
	}

	@Override
	public void setBiome(int x, int y, int z, EnumBiomeType biome) {
		world.setBiome(x, z, Biome.valueOf(WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD) ? biome.getSpigotName() : biome.getOldSpigotName()));
	}

	@Override
	public boolean isAutoSave() {
		return world.isAutoSave();
	}

	@Override
	public void setAutoSave(boolean autoSave) {
		world.setAutoSave(autoSave);
	}

	@Override
	public void save() {
		world.save();
	}

	@Override
	public void flushUnloadedChunksQueue() {
		try {
			chunkProvider.getClass().getMethod("unloadChunks").invoke(chunkProvider);
		} catch (Throwable ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public void shutdownAllChunks() {
		try {
			ReflectionUtils.invokeMethod(ReflectionUtils.getObject(chunkProvider, "chunks"), "clear");
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was unloading all chunks of the world " + world.getName());
		}
	}

	@Override
	public boolean canBeSaved() {
		try {
			return (boolean) chunkProvider.getClass().getField("canSave").get(chunkProvider);
		} catch (Throwable ex) {
			ex.printStackTrace();
			return true;
		}
	}

	@Override
	public void setCanBeSaved(boolean canSave) {
		try {
			chunkProvider.getClass().getField("canSave").set(chunkProvider, canSave);
		} catch (Throwable ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public void saveAllChunks() {
		try {
			Object nmsWorld = Spigot13HandledUtils.getWorldHandle(world);
			Collection<?> collection = (Collection<?>) ReflectionUtils.invokeMethod(chunkProvider, "a");

			if (collection == null) {
				InternalLogger.sendWarning("Cannot save chunks! Collection is null.");
				return;
			}

			boolean newVersion = WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD);

			Method method = newVersion ? ReflectionUtils.getMethod(chunkLoader.getClass(), "saveChunk", nmsWorld.getClass(), NMSUtils.getNMSClass("Chunk"), boolean
					.class)
					: ReflectionUtils.getMethod(chunkLoader.getClass(), "a", nmsWorld.getClass(), NMSUtils.getNMSClass("Chunk"));

			int i = 0;
			try {
				for (Object chunk : collection) {
					if (newVersion) method.invoke(chunkLoader, nmsWorld, chunk, false);
					else method.invoke(chunkLoader, nmsWorld, chunk);
					i++;
				}
			} catch (Exception ex) {
				InternalLogger.sendWarning("Error while saving chunks. Chunks saved: " + i);
			}

			Method flush = newVersion ? ReflectionUtils.getMethod(chunkLoader.getClass(), "a") : ReflectionUtils.getMethod(chunkLoader.getClass(), "c");

			if (newVersion) while ((boolean) flush.invoke(chunkLoader)) ;
			else while ((boolean) flush.invoke(chunkLoader)) ;

		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was trying to save the chunks of the world" + getName());
		}
	}

	@Override
	public WSBlockType getBlockType(Vector3i position) {
		return getBlockType(position.getX(), position.getY(), position.getZ());
	}

	@Override
	public WSBlockType getBlockType(int x, int y, int z) {
		try {
			return Spigot13HandledUtils.getMaterial(ReflectionUtils
					.invokeMethod(ReflectionUtils.invokeMethod(Spigot13HandledUtils.getWorldHandle(world), "getChunkAt", x >> 4, z >> 4), "getBlockState", x, y, z));
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was getting the block types of a block!");
			return null;
		}
	}

	@Override
	public Map<Vector3i, WSBlockType> getBlockTypesInArea(Vector3i pos1, Vector3i pos2) {
		return getBlockTypesInArea(pos1, pos2, false);
	}

	@Override
	public Map<Vector3i, WSBlockType> getBlockTypesInArea(Vector3i pos1, Vector3i pos2, boolean getAir) {
		WetSponge.getTimings().startTiming("Get block types in area");
		Vector3i min = new Vector3i(Math.min(pos1.getX(), pos2.getX()), Math.min(pos1.getY(), pos2.getY()), Math.min(pos1.getZ(), pos2.getZ()));
		Vector3i max = new Vector3i(Math.max(pos1.getX(), pos2.getX()), Math.max(pos1.getY(), pos2.getY()), Math.max(pos1.getZ(), pos2.getZ()));

		Vector2i minChunk = new Vector2i(min.getX() >> 4, min.getZ() >> 4);
		Vector2i maxChunk = new Vector2i(max.getX() >> 4, max.getZ() >> 4);
		Map<Vector3i, WSBlockType> blockTypes = new HashMap<>();
		try {
			Object nmsWorld = Spigot13HandledUtils.getWorldHandle(world);

			for (int cx = minChunk.getX(); cx <= maxChunk.getX(); cx++) {
				for (int cz = minChunk.getY(); cz <= maxChunk.getY(); cz++) {
					Object chunk = loadChunk(nmsWorld, cx, cz);
					if (chunk == null) {
						InternalLogger.sendWarning("Cannot load chunk (" + cx + ", " + cz + ")! WetSponge will skip it. (Method: getBlockTypesInArea)");
						continue;
					}
					int minX = 0;
					int minZ = 0;
					int maxX = 15;
					int maxZ = 15;
					if (cx == minChunk.getX()) minX = min.getX() - (cx << 4);
					if (cz == minChunk.getY()) minZ = min.getZ() - (cz << 4);
					if (cx == maxChunk.getX()) maxX = max.getX() - (cx << 4);
					if (cz == maxChunk.getY()) maxZ = max.getZ() - (cz << 4);
					for (int x = minX; x <= maxX; x++) {
						for (int z = minZ; z <= maxZ; z++) {
							for (int y = min.getY(); y <= max.getY(); y++) {
								int fx = (cx << 4) + x;
								int fz = (cz << 4) + z;
								WSBlockType blockType = Spigot13HandledUtils.getMaterial(ReflectionUtils.invokeMethod(chunk, "getBlockData", fx, y, fz));
								if (getAir || !blockType.getStringId().equalsIgnoreCase(WSBlockTypes.AIR.getStringId()))
									blockTypes.put(new Vector3i(fx, y, fz), blockType);
							}
						}
					}
				}
			}
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was getting the block types of an area!");
		}
		WetSponge.getTimings().stopTiming();
		return blockTypes;
	}

	/*@Override
	public WSSchematic createSchematic(Vector3i center, Vector3i pos1, Vector3i pos2, boolean copyAir) {
		Vector3i min = new Vector3i(Math.min(pos1.getX(), pos2.getX()), Math.min(pos1.getY(), pos2.getY()), Math.min(pos1.getZ(), pos2.getZ()));
		Vector3i max = new Vector3i(Math.max(pos1.getX(), pos2.getX()), Math.max(pos1.getY(), pos2.getY()), Math.max(pos1.getZ(), pos2.getZ()));

		Vector2i minChunk = new Vector2i(min.getX() >> 4, min.getZ() >> 4);
		Vector2i maxChunk = new Vector2i(max.getX() >> 4, max.getZ() >> 4);

		Map<Vector3i, WSBlockType> blockTypes = new HashMap<>();
		Map<Vector3i, String> tileEntities = new HashMap<>();
		try {
			Object nmsWorld = Spigot13HandledUtils.getWorldHandle(world);
			for (int cx = minChunk.getX(); cx <= maxChunk.getX(); cx++) {
				for (int cz = minChunk.getY(); cz <= maxChunk.getY(); cz++) {
					Object chunk = loadChunk(nmsWorld, cx, cz);
					if (chunk == null) {
						InternalLogger.sendWarning("Cannot load chunk (" + cx + ", " + cz + ")! WetSponge will skip it. (Method: createSchematic)");
						continue;
					}
					int minX = 0;
					int minZ = 0;
					int maxX = 15;
					int maxZ = 15;
					if (cx == minChunk.getX()) minX = min.getX() - (cx << 4);
					if (cz == minChunk.getY()) minZ = min.getZ() - (cz << 4);
					if (cx == maxChunk.getX()) maxX = max.getX() - (cx << 4);
					if (cz == maxChunk.getY()) maxZ = max.getZ() - (cz << 4);
					for (int x = minX; x <= maxX; x++) {
						for (int z = minZ; z <= maxZ; z++) {
							for (int y = min.getY(); y <= max.getY(); y++) {
								int fx = (cx << 4) + x;
								int fz = (cz << 4) + z;
								Object pos = Spigot13HandledUtils.getBlockPosition(fx, y, fz);
								WSBlockType blockType = Spigot13HandledUtils.getMaterial(ReflectionUtils.invokeMethod(chunk, "getBlockData", pos));
								boolean air;
								if ((air = blockType.getStringId().equalsIgnoreCase(WSBlockTypes.AIR.getStringId())) || copyAir) {
									Vector3i position = new Vector3i(fx, y, fz).sub(center);
									blockTypes.put(position, blockType);
									if (!air) {
										Object tileEntity = ReflectionUtils.invokeMethod(chunk, "a", pos, Enum
												.valueOf(((Class<? extends Enum>) NMSUtils.getNMSClass("Chunk$EnumTileEntityState")), "CHECK"));
										if (tileEntity != null) {
											Object compound = Spigot13NBTTagUtils.newNBTTagCompound();
											ReflectionUtils
													.invokeMethod(tileEntity, WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD) ? "save" : "b", compound);
											tileEntities.put(position, compound.toString());
										}
									}
								}
							}
						}
					}
				}
			}
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was getting the block types of an area!");
		}
		return new WSSchematic(blockTypes, tileEntities);
	}*/

	@Override
	public Map<Vector2i, EnumMapBaseColor> getMapColors(Vector3i pos1, Vector3i pos2) {
		WetSponge.getTimings().startTiming("Get map colors");
		Vector3i min = new Vector3i(Math.min(pos1.getX(), pos2.getX()), Math.min(pos1.getY(), pos2.getY()), Math.min(pos1.getZ(), pos2.getZ()));
		Vector3i max = new Vector3i(Math.max(pos1.getX(), pos2.getX()), Math.max(pos1.getY(), pos2.getY()), Math.max(pos1.getZ(), pos2.getZ()));

		Vector2i minChunk = new Vector2i(min.getX() >> 4, min.getZ() >> 4);
		Vector2i maxChunk = new Vector2i(max.getX() >> 4, max.getZ() >> 4);
		Map<Vector2i, EnumMapBaseColor> colors = new HashMap<>();
		try {
			Class<?> chunkClass = NMSUtils.getNMSClass("Chunk");
			Class<?> blockPosClass = NMSUtils.getNMSClass("BlockPosition");
			Method getBlockData = chunkClass.getMethod("getBlockData", blockPosClass);
			Constructor blockPosConstructor = blockPosClass.getConstructor(int.class, int.class, int.class);

			Object nmsWorld = Spigot13HandledUtils.getWorldHandle(world);

			int total = 0;
			for (int cx = minChunk.getX(); cx <= maxChunk.getX(); cx++)
				for (int cz = minChunk.getY(); cz <= maxChunk.getY(); cz++) total++;
			int count = 0;

			for (int cx = minChunk.getX(); cx <= maxChunk.getX(); cx++) {
				for (int cz = minChunk.getY(); cz <= maxChunk.getY(); cz++) {
					Object chunk = loadChunk(nmsWorld, cx, cz);
					count++;
					if (count % 5 == 0) InternalLogger.sendInfo("Saving map colors. " + count + "/" + total + " (" + ((count - 1) * 100 / total) + "%)");
					if (chunk == null) {
						InternalLogger.sendWarning("Cannot load chunk (" + cx + ", " + cz + ")! WetSponge will skip it. (Method: getMapColors)");
						continue;
					}
					int minX = 0;
					int minZ = 0;
					int maxX = 15;
					int maxZ = 15;
					if (cx == minChunk.getX()) minX = min.getX() - (cx << 4);
					if (cz == minChunk.getY()) minZ = min.getZ() - (cz << 4);
					if (cx == maxChunk.getX()) maxX = max.getX() - (cx << 4);
					if (cz == maxChunk.getY()) maxZ = max.getZ() - (cz << 4);
					for (int x = minX; x <= maxX; x++) {
						for (int z = minZ; z <= maxZ; z++) {
							for (int y = max.getY(); y >= min.getY(); y--) {
								int fx = (cx << 4) + x;
								int fz = (cz << 4) + z;
								Object nmsBT = getBlockData.invoke(chunk, blockPosConstructor.newInstance(fx, y, fz));
								WSBlockType blockType = Spigot13HandledUtils.getMaterial(nmsBT);
								EnumMapBaseColor color = Spigot13MapUtils.getMapBaseColor(nmsBT, nmsWorld, Spigot13HandledUtils.getBlockPosition(fx, y, fz), blockType.getStringId());
								if (color.equals(EnumMapBaseColor.AIR)) continue;
								colors.put(new Vector2i(fx, fz), color);
								break;
							}
						}
					}
				}
			}
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was getting the block types of an area!");
		}
		WetSponge.getTimings().stopTiming();
		return colors;
	}

	private Object loadChunk(Object nmsWorld, int cx, int cz) {
		WetSponge.getTimings().startTiming("Load nms chunk");
		try {
			Object o = ReflectionUtils.invokeMethod(chunkLoader, "loadChunk", nmsWorld, cx, cz);
			if (o == null) return null;
			WetSponge.getTimings().stopTiming();
			return Array.get(o, 0);
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was loading a chunk!");
			WetSponge.getTimings().stopTiming();
			return null;
		}
	}

	@Override
	public void triggerExplosion(WSExplosion explosion) {
		WSLocation location = explosion.getLocation();
		world.createExplosion(location.getX(), location.getY(), location.getZ(), explosion.getRadius(), explosion.canCauseFire(), explosion.shouldBreakBlocks());
	}

	@Override
	public WSWorldBorder getWorldBorder() {
		return worldBorder;
	}

	@Override
	public Map<Integer, WSMapView> getMapViews() {
		return new HashMap<>(mapViews);
	}

	@Override
	public Optional<WSMapView> getMapView(int mapId) {
		return Optional.ofNullable(mapViews.getOrDefault(mapId, null));
	}

	@Override
	public void putMapView(int mapId, WSMapView mapView) {
		Validate.notNull(mapView, "Map view cannot be null!");
		mapViews.put(mapId, mapView);
	}

	@Override
	public void removeMapView(int mapId) {
		mapViews.remove(mapId);
	}

	@Override
	public void clearMapViews() {
		mapViews.clear();
	}

	@Override
	public File getWorldFolder() {
		return world.getWorldFolder();
	}

	public Object getChunkProvider() {
		return chunkProvider;
	}


	@Override
	public World getHandled() {
		return world;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Spigot13World that = (Spigot13World) o;

		return world != null ? world.equals(that.world) : that.world == null;
	}


	@Override
	public int hashCode() {
		return world != null ? world.hashCode() : 0;
	}
}
