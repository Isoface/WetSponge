package com.degoos.wetsponge.world;

import com.degoos.wetsponge.enums.EnumDifficulty;
import com.degoos.wetsponge.enums.EnumGameMode;
import com.flowpowered.math.vector.Vector3i;
import org.bukkit.Difficulty;
import org.bukkit.World;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

public class Spigot13WorldProperties implements WSWorldProperties {

	private World world;

	public Spigot13WorldProperties(World world) {
		this.world = world;
	}

	@Override
	public boolean isInitialized() {
		return true;
	}

	@Override
	public String getWorldName() {
		return world.getName();
	}

	@Override
	public UUID getUniqueId() {
		return world.getUID();
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	@Override
	public void setEnabled(boolean enabled) {
	}

	@Override
	public boolean loadOnStartup() {
		return false;
	}

	@Override
	public void setLoadOnStartup(boolean loadOnStartup) {

	}

	@Override
	public boolean doesKeepSpawnLoaded() {
		return world.getKeepSpawnInMemory();
	}

	@Override
	public void setKeepSpawnLoaded(boolean keepSpawnLoaded) {
		world.setKeepSpawnInMemory(keepSpawnLoaded);
	}

	@Override
	public boolean doesGenerateSpawnOnLoad() {
		return false;
	}

	@Override
	public void setGenerateSpawnOnLoad(boolean generateSpawnOnLoad) {

	}

	@Override
	public Vector3i getSpawnPosition() {
		return new Spigot13Location(world.getSpawnLocation()).toVector3i();
	}

	@Override
	public void setSpawnPosition(Vector3i spawnPosition) {
		world.setSpawnLocation(spawnPosition.getX(), spawnPosition.getY(), spawnPosition.getZ());
	}

	@Override
	public long getSeed() {
		return world.getSeed();
	}

	@Override
	public void setSeed(long seed) {

	}

	@Override
	public long getTotalTime() {
		return world.getFullTime();
	}

	@Override
	public long getWorldTime() {
		return world.getTime();
	}

	@Override
	public void setWorldTime(long worldTime) {
		world.setTime(worldTime);
	}

	@Override
	public boolean isPVPEnabled() {
		return world.getPVP();
	}

	@Override
	public void setPVPEnabled(boolean pvpEnabled) {
		world.setPVP(pvpEnabled);
	}

	@Override
	public boolean isRaining() {
		return world.hasStorm();
	}

	@Override
	public void setRaining(boolean raining) {
		world.setStorm(raining);
	}

	@Override
	public int getRainTime() {
		return world.getWeatherDuration();
	}

	@Override
	public void setRainTime(int rainTime) {
		world.setWeatherDuration(rainTime);
	}

	@Override
	public boolean isThundering() {
		return world.isThundering();
	}

	@Override
	public void setThundering(boolean thundering) {
		world.setThundering(thundering);
	}

	@Override
	public int getThunderTime() {
		return world.getThunderDuration();
	}

	@Override
	public void setThunderTime(int thunderTime) {
		world.setThunderDuration(thunderTime);
	}

	@Override
	public EnumGameMode getGameMode() {
		return EnumGameMode.SURVIVAL;
	}

	@Override
	public void setGameMode(EnumGameMode gameMode) {

	}

	@Override
	public boolean usesMapFeatures() {
		return false;
	}

	@Override
	public void setMapFeaturesEnabled(boolean mapFeaturesEnabled) {

	}

	@Override
	public boolean isHardcore() {
		return false;
	}

	@Override
	public void setHardcore(boolean hardcore) {

	}

	@Override
	public boolean areCommandsAllowed() {
		return false;
	}

	@Override
	public void setCommandsAllowed(boolean commandsAllowed) {

	}

	@Override
	public EnumDifficulty getDifficulty() {
		return EnumDifficulty.getByValue(world.getDifficulty().getValue()).orElse(EnumDifficulty.NORMAL);
	}

	@Override
	public void setDifficulty(EnumDifficulty difficulty) {
		world.setDifficulty(Difficulty.getByValue(difficulty.getValue()));
	}

	@Override
	public boolean doesGenerateBonusChest() {
		return false;
	}

	@Override
	public Optional<String> getGameRule(String name) {
		return Optional.ofNullable(world.getGameRuleValue(name));
	}

	@Override
	public Map<String, String> getGameRules() {
		Map<String, String> gameRules = new HashMap<>();
		for (String gameRule : world.getGameRules()) {
			String value = world.getGameRuleValue(gameRule);
			if (value == null) continue;
			gameRules.put(gameRule, value);
		}
		return gameRules;
	}

	@Override
	public void setGameRule(String name, String value) {
		world.setGameRuleValue(name, value);
	}

	@Override
	public boolean removeGameRule(String name) {
		return world.setGameRuleValue(name, null);
	}

	@Override
	public int getMaxHeight() {
		return world.getMaxHeight();
	}

}
