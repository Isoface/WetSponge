package co.aikar.wetspongeutils;

import co.aikar.timings.TimingsManager;
import co.aikar.util.JSONUtil;
import com.google.common.base.Joiner;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import ninja.leaping.configurate.ConfigurationNode;
import org.spongepowered.common.SpongeImpl;

import java.util.Map.Entry;

/**
 * WetSponge on 23/09/2017 by IhToN.
 */
public class SpongeTimingsUtils {
	private static final Joiner CONFIG_PATH_JOINER = Joiner.on(".");

	public static JsonElement serializeConfigNode() {
		return serializeConfigNode(SpongeImpl.getGlobalConfig().getRootNode());
	}

	public static JsonElement serializeConfigNode(ConfigurationNode node) {
		if (node.hasMapChildren()) {
			JsonObject object = new JsonObject();
			for (Entry<Object, ? extends ConfigurationNode> entry : node.getChildrenMap().entrySet()) {
				String fullPath = CONFIG_PATH_JOINER.join(entry.getValue().getPath());
				if (fullPath.equals("sponge.sql") || TimingsManager.hiddenConfigs.contains(fullPath)) {
					continue;
				}
				object.add(entry.getKey().toString(), serializeConfigNode(entry.getValue()));
			}
			return object;
		}
		if (node.hasListChildren()) {
			JsonArray array = new JsonArray();
			for (ConfigurationNode child : node.getChildrenList()) {
				array.add(serializeConfigNode(child));
			}
			return array;
		}
		return JSONUtil.toJsonElement(node.getValue());
	}
}
