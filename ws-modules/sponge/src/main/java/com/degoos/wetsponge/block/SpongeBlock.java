package com.degoos.wetsponge.block;


import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.block.tileentity.*;
import com.degoos.wetsponge.enums.EnumMapBaseColor;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.material.SpongeMaterial;
import com.degoos.wetsponge.material.WSBlockTypes;
import com.degoos.wetsponge.material.block.WSBlockType;
import com.degoos.wetsponge.parser.world.WorldParser;
import com.degoos.wetsponge.world.SpongeLocation;
import com.degoos.wetsponge.world.WSLocation;
import com.degoos.wetsponge.world.WSWorld;
import net.minecraft.util.math.BlockPos;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

public class SpongeBlock implements WSBlock {

	private Location<World> location;


	public SpongeBlock(Location<World> location) {
		this.location = location;
	}


	@Override
	public WSLocation getLocation() {
		return new SpongeLocation(location);
	}


	@Override
	public WSWorld getWorld() {
		return WorldParser.getOrCreateWorld(location.getExtent().getName(), location.getExtent());
	}


	@Override
	public int getNumericalId() {
		return getBlockType().getNumericalId();
	}


	@Override
	public String getStringId() {
		return WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13) ? getOldStringId() : getNewStringId();
	}

	@Override
	public String getNewStringId() {
		return !WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13) ? location.getBlock().getType().getId() : getBlockType().getNewStringId();
	}

	@Override
	public String getOldStringId() {
		return WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13) ? location.getBlock().getType().getId() : getBlockType().getOldStringId();
	}


	@Override
	public WSBlockState createState() {
		return new SpongeBlockState(this);
	}


	@Override
	public SpongeTileEntity getTileEntity() {
		switch (getNumericalId()) {
			case 23:
				return new SpongeTileEntityDispenser(this);
			case 25:
				return new SpongeTileEntityNoteblock(this);
			case 52:
				return new SpongeTileEntityMonsterSpawner(this);
			case 54:
			case 146:
				return new SpongeTileEntityChest(this);
			case 61:
			case 62:
				return new SpongeTileEntityFurnace(this);
			case 63:
			case 68:
				return new SpongeTileEntitySign(this);
			case 84:
				return new SpongeTileEntityJukebox(this);
			case 116:
				return new SpongeTileEntityNameable(this);
			case 117:
				return new SpongeTileEntityBrewingStand(this);
			case 137:
				return new SpongeTileEntityCommandBlock(this);
			case 144:
				return new SpongeTileEntitySkull(this);
			case 154:
				return new SpongeTileEntityNameableInventory(this);
			default:
				return new SpongeTileEntity(this);
		}
	}

	@Override
	public WSBlockType getBlockType() {
		WSBlockType type = WSBlockTypes.getById(getStringId()).orElse(WSBlockTypes.AIR.getDefaultState());
		((SpongeMaterial) type).readContainer(getHandled().getBlock());
		return type;
	}

	@Override
	public WSBlock getRelative(EnumBlockFace direction) {
		return getLocation().add(direction.getRelative().toDouble()).getBlock();
	}

	@Override
	public EnumMapBaseColor getMapBaseColor() {
		BlockPos pos = new BlockPos(location.getX(), location.getY(), location.getZ());
		return EnumMapBaseColor.getById(((net.minecraft.world.World) location.getExtent()).getBlockState(pos)
				.getMapColor((net.minecraft.world.World) getWorld().getHandled(), pos).colorIndex).orElse(EnumMapBaseColor.AIR);
	}

	@Override
	public Location<World> getHandled() {
		return location;
	}

}
