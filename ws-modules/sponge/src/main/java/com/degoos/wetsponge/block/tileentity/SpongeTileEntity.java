package com.degoos.wetsponge.block.tileentity;


import com.degoos.wetsponge.block.SpongeBlock;
import com.degoos.wetsponge.nbt.WSNBTTagCompound;
import net.minecraft.nbt.NBTTagCompound;
import org.spongepowered.api.block.tileentity.TileEntity;

public class SpongeTileEntity implements WSTileEntity {

	private SpongeBlock block;
	private TileEntity tileEntity;


	public SpongeTileEntity(SpongeBlock block) {
		this.block = block;
		this.tileEntity = block.getHandled().getTileEntity().orElse(null);
	}

	public SpongeTileEntity(TileEntity tileEntity) {
		this.tileEntity = tileEntity;
		this.block = new SpongeBlock(tileEntity.getLocation());
	}


	@Override
	public SpongeBlock getBlock() {
		return block;
	}

	@Override
	public WSNBTTagCompound writeToNBTTagCompound(WSNBTTagCompound nbtTagCompound) {
		((net.minecraft.tileentity.TileEntity) this.getHandled()).writeToNBT((NBTTagCompound) nbtTagCompound.getHandled());
		return nbtTagCompound;
	}

	@Override
	public WSNBTTagCompound readFromNBTTagCompound(WSNBTTagCompound nbtTagCompound) {
		((net.minecraft.tileentity.TileEntity) this.getHandled()).readFromNBT((NBTTagCompound) nbtTagCompound.getHandled());
		return nbtTagCompound;
	}


	@Override
	public Object getHandled() {
		return tileEntity;
	}
}
