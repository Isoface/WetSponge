package com.degoos.wetsponge.block.tileentity;


import com.degoos.wetsponge.block.SpongeBlock;
import com.degoos.wetsponge.inventory.SpongeInventory;
import com.degoos.wetsponge.inventory.WSInventory;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import org.spongepowered.api.block.tileentity.carrier.Chest;

public class SpongeTileEntityChest extends SpongeTileEntityInventory implements WSTileEntityChest {

	public SpongeTileEntityChest(SpongeBlock block) {
		super(block);
	}

	private SpongeTileEntityChest(Chest tileEntity) {
		super(tileEntity);
	}

	@Override
	public WSInventory getSoloInventory() {
		return getInventory();
	}

	@Override
	public Optional<WSInventory> getDoubleChestInventory() {
		return getHandled().getDoubleChestInventory().map(SpongeInventory::new);
	}

	@Override
	public Set<WSTileEntityChest> getConnectedChests() {
		return getHandled().getConnectedChests().stream().map(SpongeTileEntityChest::new).collect(Collectors.toSet());
	}

	@Override
	public Chest getHandled() {
		return (Chest) super.getHandled();
	}
}
