package com.degoos.wetsponge.block.tileentity;


import com.degoos.wetsponge.block.SpongeBlock;
import org.spongepowered.api.block.tileentity.carrier.Furnace;
import org.spongepowered.api.data.key.Keys;

public class SpongeTileEntityFurnace extends SpongeTileEntityInventory implements WSTileEntityFurnace {

    public SpongeTileEntityFurnace(SpongeBlock block) {
        super(block);
    }


    @Override
    public int getBurnTime() {
        return getHandled().passedBurnTime().get();
    }

    @Override
    public void setBurnTime(int burnTime) {
        getHandled().offer(Keys.PASSED_BURN_TIME, burnTime);
    }

    @Override
    public int getCookTime() {
        return getHandled().maxCookTime().get();
    }

    @Override
    public void setCookTime(int cookTime) {
        getHandled().offer(Keys.PASSED_COOK_TIME, cookTime);
    }

    @Override
    public Furnace getHandled() {
        return (Furnace) super.getHandled();
    }

}
