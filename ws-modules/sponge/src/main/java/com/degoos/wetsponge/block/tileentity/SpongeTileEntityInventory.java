package com.degoos.wetsponge.block.tileentity;


import com.degoos.wetsponge.block.SpongeBlock;
import com.degoos.wetsponge.inventory.SpongeInventory;
import org.spongepowered.api.block.tileentity.carrier.TileEntityCarrier;

public class SpongeTileEntityInventory extends SpongeTileEntity implements WSTileEntityInventory {

	public SpongeTileEntityInventory(SpongeBlock block) {
		super(block);
	}

	public SpongeTileEntityInventory(TileEntityCarrier tileEntity) {
		super(tileEntity);
	}

	@Override
	public SpongeInventory getInventory() {
		return new SpongeInventory(getHandled().getInventory());
	}


	@Override
	public TileEntityCarrier getHandled() {
		return (TileEntityCarrier) super.getHandled();
	}
}
