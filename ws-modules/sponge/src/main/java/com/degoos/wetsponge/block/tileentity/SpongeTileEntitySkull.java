package com.degoos.wetsponge.block.tileentity;

import com.degoos.wetsponge.block.SpongeBlock;
import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.enums.block.EnumBlockTypeSkullType;
import com.degoos.wetsponge.resource.sponge.SpongeSkullBuilder;
import com.degoos.wetsponge.user.SpongeGameProfile;
import com.degoos.wetsponge.user.WSGameProfile;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.block.tileentity.Skull;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.SkullType;
import org.spongepowered.api.data.type.SkullTypes;
import org.spongepowered.api.util.Direction;

import java.net.URL;

public class SpongeTileEntitySkull extends SpongeTileEntity implements WSTileEntitySkull {


	public SpongeTileEntitySkull(SpongeBlock block) {
		super(block);
	}

	@Override
	public WSGameProfile getGameProfile() {
		return new SpongeGameProfile(getHandled().getBlock().get(Keys.REPRESENTED_PLAYER).orElse(null));
	}

	@Override
	public void setGameProfile(WSGameProfile gameProfile) {
		BlockState state = getHandled().getBlock();
		getHandled().getLocation().setBlock(state.with(Keys.REPRESENTED_PLAYER, ((SpongeGameProfile) gameProfile).getHandled()).orElse(state));
	}

	@Override
	public EnumBlockFace getOrientation() {
		return EnumBlockFace.getBySpongeName(getHandled().get(Keys.DIRECTION).orElse(Direction.DOWN).name()).orElse(EnumBlockFace.SOUTH);
	}

	@Override
	public void setOrientation(EnumBlockFace orientation) {
		getHandled().offer(Keys.DIRECTION, Direction.valueOf(orientation.getSpongeName()));
	}

	@Override
	public EnumBlockTypeSkullType getSkullType() {
		return EnumBlockTypeSkullType.getBySpongeName(getHandled().skullType().get().getName()).orElse(EnumBlockTypeSkullType.SKELETON);
	}

	@Override
	public void setSkullType(EnumBlockTypeSkullType skullType) {
		getHandled().offer(Keys.SKULL_TYPE, Sponge.getRegistry().getType(SkullType.class, skullType.getSpongeName()).orElse(SkullTypes.SKELETON));
	}

	@Override
	public void setTexture(String texture) {
		getHandled().getLocation().setBlock(SpongeSkullBuilder.updateSkullByTexture(getHandled().getBlock(), texture));
	}

	@Override
	public void setTexture(URL texture) {
		getHandled().getLocation().setBlock(SpongeSkullBuilder.updateSkullByURL(getHandled().getBlock(), texture));
	}

	@Override
	public void setTextureByPlayerName(String name) {
		getHandled().getLocation().setBlock(SpongeSkullBuilder.updateSkullByPlayerName(getHandled().getBlock(), name));
	}

	@Override
	public void findFormatAndSetTexture(String texture) {
		getHandled().getLocation().setBlock(SpongeSkullBuilder.updateSkullByUnknownFormat(getHandled().getBlock(), texture));
	}

	@Override
	public Skull getHandled() {
		return (Skull) super.getHandled();
	}
}
