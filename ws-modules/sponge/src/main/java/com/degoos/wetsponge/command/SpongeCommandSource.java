package com.degoos.wetsponge.command;


import com.degoos.wetsponge.text.SpongeText;
import com.degoos.wetsponge.text.WSText;
import java.util.HashSet;
import java.util.Map.Entry;
import java.util.Set;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.text.serializer.TextSerializers;

public class SpongeCommandSource implements WSCommandSource {

	private CommandSource source;


	public SpongeCommandSource(CommandSource sender) {
		this.source = sender;
	}


	@Override
	public String getName() {
		return source.getName();
	}


	@Override
	public boolean hasPermission(String name) {
		return source.hasPermission(name);
	}

	@Override
	public Set<String> getPermissions() {
		Set<String> set = new HashSet<>();
		source.getSubjectData().getAllPermissions().values()
			.forEach(target -> target.entrySet().stream().filter(Entry::getValue).forEach(entry -> set.add(entry.getKey())));
		return set;
	}

	@Override
	public void sendMessage(String message) {
		getHandled().sendMessage(TextSerializers.LEGACY_FORMATTING_CODE.deserialize(message));
	}


	@Override
	public void sendMessage(WSText text) {
		getHandled().sendMessage(((SpongeText) text).getHandled());
	}


	@Override
	public void sendMessages(String... messages) {
		for (String message : messages) sendMessage(message);
	}


	@Override
	public void sendMessages(WSText... texts) {
		for (WSText text : texts) sendMessage(text);
	}

	@Override
	public void performCommand(String command) {
		Sponge.getGame().getCommandManager().process(getHandled(), command);
	}


	@Override
	public CommandSource getHandled() {
		return source;
	}

}
