package com.degoos.wetsponge.console;


import com.degoos.wetsponge.command.SpongeCommandSource;
import com.degoos.wetsponge.text.SpongeText;
import com.degoos.wetsponge.text.WSText;
import org.spongepowered.api.command.source.ConsoleSource;
import org.spongepowered.api.text.serializer.TextSerializers;

public class SpongeConsoleSource extends SpongeCommandSource implements WSConsoleSource {


    public SpongeConsoleSource(ConsoleSource source) {
        super(source);
    }


    @Override
    public String getName() {
        return getHandled().getName();
    }


    @Override
    public void sendMessage(String message) {
        getHandled().sendMessage(TextSerializers.LEGACY_FORMATTING_CODE.deserialize(message));
    }


    @Override
    public void sendMessage(WSText text) {
        getHandled().sendMessage(((SpongeText) text).getHandled());
    }


    @Override
    public void sendMessages(String... messages) {
        for (String message : messages) sendMessage(message);
    }


    @Override
    public void sendMessages(WSText... texts) {
        for (WSText text : texts) sendMessage(text);
    }


    @Override
    public ConsoleSource getHandled() {
        return (ConsoleSource) super.getHandled();
    }

}
