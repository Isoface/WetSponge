package com.degoos.wetsponge.entity.hanging;

import com.degoos.wetsponge.entity.SpongeEntity;
import com.degoos.wetsponge.enums.block.EnumBlockFace;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.hanging.Hanging;
import org.spongepowered.api.util.Direction;

public class SpongeHanging extends SpongeEntity implements WSHanging {


	public SpongeHanging(Hanging entity) {
		super(entity);
	}

	@Override
	public EnumBlockFace getDirection() {
		return EnumBlockFace.getBySpongeName(getHandled().direction().get().name()).orElse(EnumBlockFace.EAST);
	}

	@Override
	public void setDirection(EnumBlockFace direction) {
		getHandled().offer(Keys.DIRECTION, Direction.valueOf(direction.getSpongeName()));
	}

	@Override
	public Hanging getHandled() {
		return (Hanging) super.getHandled();
	}
}
