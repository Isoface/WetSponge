package com.degoos.wetsponge.entity.living;

import com.degoos.wetsponge.enums.EnumEquipType;
import com.degoos.wetsponge.item.SpongeItemStack;
import com.degoos.wetsponge.item.WSItemStack;
import com.flowpowered.math.vector.Vector3d;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.HandTypes;
import org.spongepowered.api.entity.living.ArmorStand;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.equipment.EquipmentTypes;

import java.util.Optional;

public class SpongeArmorStand extends SpongeLivingEntity implements WSArmorStand {


    public SpongeArmorStand(ArmorStand entity) {
        super(entity);
    }

    @Override
    public Vector3d getHeadDirection() {
        return getHandled().getBodyPartRotationalData().headDirection().get();
    }

    @Override
    public void setHeadDirection(Vector3d headDirection) {
        getHandled().offer(Keys.HEAD_ROTATION, headDirection);
    }

    @Override
    public Vector3d getBodyRotation() {
        return getHandled().getBodyPartRotationalData().bodyRotation().get();
    }

    @Override
    public void setBodyRotation(Vector3d bodyRotation) {
        getHandled().offer(Keys.CHEST_ROTATION, bodyRotation);
    }

    @Override
    public Vector3d getLeftArmDirection() {
        return getHandled().getBodyPartRotationalData().leftArmDirection().get();
    }

    @Override
    public void setLeftArmDirection(Vector3d leftArmDirection) {
        getHandled().offer(Keys.LEFT_ARM_ROTATION, leftArmDirection);
    }

    @Override
    public Vector3d getRightArmDirection() {
        return getHandled().getBodyPartRotationalData().rightArmDirection().get();
    }

    @Override
    public void setRightArmDirection(Vector3d rightArmDirection) {
        getHandled().offer(Keys.RIGHT_ARM_ROTATION, rightArmDirection);
    }

    @Override
    public Vector3d getLeftLegDirection() {
        return getHandled().getBodyPartRotationalData().leftLegDirection().get();
    }

    @Override
    public void setLeftLegDirection(Vector3d leftLegDirection) {
        getHandled().offer(Keys.LEFT_LEG_ROTATION, leftLegDirection);
    }

    @Override
    public Vector3d getRightLegDirection() {
        return getHandled().getBodyPartRotationalData().rightLegDirection().get();
    }

    @Override
    public void setRightLegDirection(Vector3d rightLegDirection) {
        getHandled().offer(Keys.RIGHT_LEG_ROTATION, rightLegDirection);
    }

    @Override
    public boolean isMarker() {
        return getHandled().marker().get();
    }

    @Override
    public void setMarker(boolean marker) {
        getHandled().offer(Keys.ARMOR_STAND_MARKER, marker);
    }

    @Override
    public boolean isSmall() {
        return getHandled().small().get();
    }

    @Override
    public void setSmall(boolean small) {
        getHandled().offer(Keys.ARMOR_STAND_IS_SMALL, small);
    }

    @Override
    public boolean hasBasePlate() {
        return getHandled().basePlate().get();
    }

    @Override
    public void setBasePlate(boolean basePlate) {
        getHandled().offer(Keys.ARMOR_STAND_HAS_BASE_PLATE, basePlate);
    }

    @Override
    public boolean hasArms() {
        return getHandled().arms().get();
    }

    @Override
    public void setArms(boolean arms) {
        getHandled().offer(Keys.ARMOR_STAND_HAS_ARMS, arms);
    }

    @Override
    public boolean hasGravity() {
        return getHandled().gravity().get();
    }

    @Override
    public void setGravity(boolean gravity) {
        getHandled().offer(Keys.HAS_GRAVITY, gravity);
    }

    @Override
    public Optional<WSItemStack> getEquippedItem(EnumEquipType type) {
        Optional<ItemStack> itemStack;
        switch (type) {
            case HELMET:
                itemStack = getHandled().getHelmet();
                break;
            case CHESTPLATE:
                itemStack = getHandled().getChestplate();
                break;
            case LEGGINGS:
                itemStack = getHandled().getLeggings();
                break;
            case BOOTS:
                itemStack = getHandled().getBoots();
                break;
            case MAIN_HAND:
                itemStack = getHandled().getItemInHand(HandTypes.MAIN_HAND);
                break;
            case OFF_HAND:
                itemStack = getHandled().getItemInHand(HandTypes.OFF_HAND);
                break;
            default:
                itemStack = Optional.empty();
        }
        return itemStack.map(SpongeItemStack::new);
    }


    @Override
    public void setEquippedItem(EnumEquipType type, WSItemStack itemStack) {
        switch (type) {
            case HELMET:
                getHandled().setHelmet(itemStack == null ? null : ((SpongeItemStack) itemStack).getHandled());
                break;
            case CHESTPLATE:
                getHandled().setChestplate(itemStack == null ? null : ((SpongeItemStack) itemStack).getHandled());
                break;
            case LEGGINGS:
                getHandled().setLeggings(itemStack == null ? null : ((SpongeItemStack) itemStack).getHandled());
                break;
            case BOOTS:
                getHandled().setBoots(itemStack == null ? null : ((SpongeItemStack) itemStack).getHandled());
                break;
            case MAIN_HAND:
                getHandled().setItemInHand(HandTypes.MAIN_HAND, itemStack == null ? null : ((SpongeItemStack) itemStack).getHandled());
                break;
            case OFF_HAND:
                getHandled().setItemInHand(HandTypes.OFF_HAND, itemStack == null ? null : ((SpongeItemStack) itemStack).getHandled());
                break;
        }
    }


    @Override
    public ArmorStand getHandled() {
        return (ArmorStand) super.getHandled();
    }
}
