package com.degoos.wetsponge.entity.living;


import com.degoos.wetsponge.effect.potion.SpongePotionEffect;
import com.degoos.wetsponge.effect.potion.WSPotionEffect;
import com.degoos.wetsponge.entity.SpongeEntity;
import com.degoos.wetsponge.entity.WSEntity;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.entity.projectile.WSProjectile;
import com.degoos.wetsponge.enums.EnumEntityType;
import com.degoos.wetsponge.enums.EnumPotionEffectType;
import com.degoos.wetsponge.packet.WSPacket;
import com.degoos.wetsponge.packet.play.server.WSSPacketDestroyEntities;
import com.degoos.wetsponge.packet.play.server.WSSPacketSpawnMob;
import com.degoos.wetsponge.packet.play.server.WSSPacketSpawnPlayer;
import com.degoos.wetsponge.parser.entity.SpongeEntityParser;
import com.flowpowered.math.vector.Vector3d;
import net.minecraft.entity.EntityLiving;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.property.entity.EyeLocationProperty;
import org.spongepowered.api.effect.potion.PotionEffect;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.living.Living;
import org.spongepowered.api.entity.projectile.Projectile;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class SpongeLivingEntity extends SpongeEntity implements WSLivingEntity {

	private WSLivingEntity disguise;

	public SpongeLivingEntity(Living entity) {
		super(entity);
		this.disguise = null;
	}


	@Override
	public boolean isAlive() {
		return getHealth() > 0;
	}


	@Override
	public void kill() {
		setHealth(0);
	}


	@Override
	public double getHealth() {
		return getHandled().health().get();
	}


	@Override
	public void setHealth(double health) {
		getHandled().offer(Keys.HEALTH, health);
	}


	@Override
	public double getMaxHealth() {
		return getHandled().maxHealth().get();
	}


	@Override
	public void setMaxHealth(double maxHealth) {
		getHandled().offer(Keys.MAX_HEALTH, maxHealth);
	}

	@Override
	public Vector3d getRotation() {
		return getHandled().getRotation();
	}

	@Override
	public void setRotation(Vector3d rotation) {
		getHandled().setRotation(rotation);
	}

	@Override
	public Vector3d getHeadRotation() {
		return getHandled().getHeadRotation();
	}

	@Override
	public double getEyeHeight() {
		return getHandled().getProperty(EyeLocationProperty.class).map(target -> target.getValue().getY())
				.map(target -> target - getHandled().getLocation().getPosition().getY()).orElse(0.0);
	}

	@Override
	public void setHeadRotation(Vector3d rotation) {
		getHandled().setHeadRotation(rotation);
	}

	@Override
	public void lookAt(Vector3d targetPosition) {
		getHandled().lookAt(targetPosition);
	}

	@Override
	public Optional<WSLivingEntity> getDisguise() {
		return Optional.ofNullable(disguise);
	}


	@Override
	public void setDisguise(WSLivingEntity disguise) {
		if (this.disguise != null && disguise == null) {
			WSPacket removePacket = WSSPacketDestroyEntities.of(getEntityId());
			WSPacket spawnPacket = this instanceof WSPlayer ? WSSPacketSpawnPlayer.of((WSPlayer) this) : WSSPacketSpawnMob.of(this);
			getWorld().getPlayers().forEach(player -> {
				if (player.equals(this)) return;
				player.sendPacket(removePacket);
				player.sendPacket(spawnPacket);
			});
		}
		if (disguise != null) {
			WSPacket removePacket = WSSPacketDestroyEntities.of(getEntityId());
			WSPacket spawnPacket = WSSPacketSpawnMob.of(this);
			getWorld().getPlayers().forEach(player -> {
				if (player.equals(this)) return;
				player.sendPacket(removePacket);
				player.sendPacket(spawnPacket);
			});
		}
		this.disguise = disguise;
	}

	@Override
	public boolean hasDisguise() {
		return disguise != null;
	}

	@Override
	public void clearDisguise() {
		setDisguise(null);
	}

	@Override
	public Optional<WSEntity> getLeashHolder() {
		return Optional.ofNullable(((EntityLiving) getHandled()).getLeashHolder()).map(target -> (Entity) target).map(SpongeEntityParser::getWSEntity);
	}

	@Override
	public void setLeashHolder(WSEntity entity) {
		((EntityLiving) getHandled()).setLeashHolder((net.minecraft.entity.Entity) ((SpongeEntity) entity).getHandled(), true);
	}

	@Override
	public void addPotionEffect(WSPotionEffect effect) {
		List<PotionEffect> list = getHandled().get(Keys.POTION_EFFECTS).orElse(new ArrayList<>());
		list.add(((SpongePotionEffect) effect).getHandled());
		getHandled().offer(Keys.POTION_EFFECTS, list);
	}

	@Override
	public List<WSPotionEffect> getPotionEffects() {
		return getHandled().get(Keys.POTION_EFFECTS).orElse(new ArrayList<>()).stream().map(SpongePotionEffect::new).collect(Collectors.toList());
	}

	@Override
	public void clearAllPotionEffects() {
		getHandled().offer(Keys.POTION_EFFECTS, new ArrayList<>());
	}

	@Override
	public void removePotionEffect(EnumPotionEffectType potionEffectType) {
		getHandled().offer(Keys.POTION_EFFECTS, getHandled().get(Keys.POTION_EFFECTS).orElse(new ArrayList<>()).stream()
				.filter(potionEffect -> !potionEffect.getType().getId().equalsIgnoreCase(potionEffectType.getId())).collect(Collectors.toList()));
	}

	@Override
	public <T extends WSProjectile> Optional<T> launchProjectile(Class<T> projectile) {
		return launchProjectile(projectile, new Vector3d(0, 0, 0));
	}

	@Override
	public <T extends WSProjectile> Optional<T> launchProjectile(Class<T> projectile, Vector3d velocity) {
		EnumEntityType type = EnumEntityType.getByClass(projectile).orElse(EnumEntityType.UNKNOWN);
		if (type == EnumEntityType.UNKNOWN) return Optional.empty();
		try {
			Class<? extends Projectile> spongeClass = (Class<? extends Projectile>) SpongeEntityParser.getEntityData(type).getEntityClass();
			return getHandled().launchProjectile(spongeClass).map(entity -> (T) SpongeEntityParser.getWSEntity(entity));
		} catch (Throwable ex) {
			ex.printStackTrace();
			return Optional.empty();
		}
	}


	@Override
	public Living getHandled() {
		return (Living) super.getHandled();
	}
}
