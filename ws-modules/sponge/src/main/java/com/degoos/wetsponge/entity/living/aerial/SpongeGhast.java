package com.degoos.wetsponge.entity.living.aerial;

import com.degoos.wetsponge.entity.SpongeEntity;
import com.degoos.wetsponge.entity.WSEntity;
import com.degoos.wetsponge.entity.living.SpongeLivingEntity;
import com.degoos.wetsponge.parser.entity.SpongeEntityParser;
import java.util.Optional;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.living.monster.Ghast;

public class SpongeGhast extends SpongeLivingEntity implements WSGhast {


	public SpongeGhast(Ghast entity) {
		super(entity);
	}

	@Override
	public void setAI(boolean ai) {
		getHandled().offer(Keys.AI_ENABLED, ai);
	}

	@Override
	public boolean hasAI() {
		return getHandled().get(Keys.AI_ENABLED).orElse(false);
	}

	@Override
	public Optional<WSEntity> getTarget() {
		return getHandled().getTarget().map(SpongeEntityParser::getWSEntity);
	}

	@Override
	public void setTarget(WSEntity entity) {
		getHandled().setTarget(entity == null ? null : ((SpongeEntity) entity).getHandled());
	}

	@Override
	public Ghast getHandled() {
		return (Ghast) super.getHandled();
	}
}
