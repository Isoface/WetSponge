package com.degoos.wetsponge.entity.living.ambient;


import com.degoos.wetsponge.entity.living.SpongeAgent;
import org.spongepowered.api.entity.living.Ambient;

public class SpongeAmbient extends SpongeAgent implements WSAmbient {


	public SpongeAmbient (Ambient entity) {
		super(entity);
	}


	@Override
	public Ambient getHandled () {
		return (Ambient) super.getHandled();
	}
}
