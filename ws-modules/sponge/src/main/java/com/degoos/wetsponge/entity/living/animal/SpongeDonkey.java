package com.degoos.wetsponge.entity.living.animal;

import net.minecraft.entity.passive.AbstractChestHorse;
import org.spongepowered.api.entity.living.animal.Donkey;

public class SpongeDonkey extends SpongeAbstractHorse implements WSDonkey {

	public SpongeDonkey(Donkey entity) {
		super(entity);
	}

	@Override
	public boolean hasChest() {
		return ((AbstractChestHorse) getHandled()).hasChest();
	}

	@Override
	public void setChested(boolean chested) {
		((AbstractChestHorse) getHandled()).setChested(chested);
	}

	@Override
	public Donkey getHandled() {
		return (Donkey) super.getHandled();
	}
}
