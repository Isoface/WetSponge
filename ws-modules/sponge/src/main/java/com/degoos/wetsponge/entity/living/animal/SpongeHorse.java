package com.degoos.wetsponge.entity.living.animal;

import com.degoos.wetsponge.enums.EnumHorseArmorType;
import com.degoos.wetsponge.enums.EnumHorseColor;
import com.degoos.wetsponge.enums.EnumHorseStyle;
import com.degoos.wetsponge.inventory.SpongeInventory;
import com.degoos.wetsponge.inventory.WSInventory;
import com.degoos.wetsponge.item.SpongeItemStack;
import net.minecraft.entity.passive.EntityHorse;
import net.minecraft.item.ItemStack;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.HorseColor;
import org.spongepowered.api.data.type.HorseColors;
import org.spongepowered.api.data.type.HorseStyle;
import org.spongepowered.api.data.type.HorseStyles;
import org.spongepowered.api.entity.living.animal.Horse;

public class SpongeHorse extends SpongeAbstractHorse implements WSHorse {

	public SpongeHorse(Horse entity) {
		super(entity);
	}

	@Override
	public EnumHorseColor getHorseColor() {
		return EnumHorseColor.getByName(getHandled().get(Keys.HORSE_COLOR).orElse(HorseColors.WHITE).getId()).orElse(EnumHorseColor.WHITE);
	}

	@Override
	public void setHorseColor(EnumHorseColor horseColor) {
		getHandled().offer(Keys.HORSE_COLOR, Sponge.getRegistry().getType(HorseColor.class, horseColor.name()).orElse(HorseColors.WHITE));
	}

	@Override
	public EnumHorseStyle getHorseStyle() {
		return EnumHorseStyle.getByName(getHandled().get(Keys.HORSE_STYLE).orElse(HorseStyles.NONE).getId()).orElse(EnumHorseStyle.NONE);
	}

	@Override
	public void setHorseStyle(EnumHorseStyle horseStyle) {
		getHandled().offer(Keys.HORSE_STYLE, Sponge.getRegistry().getType(HorseStyle.class, horseStyle.name()).orElse(HorseStyles.NONE));
	}

	@Override
	public EnumHorseArmorType getArmor() {
		return EnumHorseArmorType.getByName(((EntityHorse) getHandled()).getHorseArmorType().name()).orElse(EnumHorseArmorType.NONE);
	}

	@Override
	public void setArmor(EnumHorseArmorType armor) {
		((EntityHorse) getHandled()).setHorseArmorStack((ItemStack) (Object) ((SpongeItemStack) armor.getArmorItemStack()).getHandled());
	}

	@Override
	public WSInventory getInventory() {
		return new SpongeInventory(getHandled().getInventory());
	}


	@Override
	public Horse getHandled() {
		return (Horse) super.getHandled();
	}
}
