package com.degoos.wetsponge.entity.living.animal;


import org.spongepowered.api.entity.living.animal.Mooshroom;

public class SpongeMooshroom extends SpongeCow implements WSMooshroom {


    public SpongeMooshroom(Mooshroom entity) {
        super(entity);
    }

    @Override
    public Mooshroom getHandled() {
        return (Mooshroom) super.getHandled();
    }
}
