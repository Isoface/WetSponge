package com.degoos.wetsponge.entity.living.animal;

import com.degoos.wetsponge.enums.EnumParrotVariant;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.ParrotVariant;
import org.spongepowered.api.data.type.ParrotVariants;
import org.spongepowered.api.entity.living.animal.Parrot;

import java.util.Optional;
import java.util.UUID;

public class SpongeParrot extends SpongeAnimal implements WSParrot {


	public SpongeParrot(Parrot entity) {
		super(entity);
	}

	@Override
	public EnumParrotVariant getVariant() {
		return EnumParrotVariant.valueOf(getHandled().get(Keys.PARROT_VARIANT).orElse(ParrotVariants.BLUE).getId());
	}

	@Override
	public void setVariant(EnumParrotVariant variant) {
		getHandled().offer(Keys.PARROT_VARIANT, Sponge.getRegistry().getType(ParrotVariant.class, variant.name()).orElseThrow(NullPointerException::new));
	}

	@Override
	public boolean isSitting() {
		return getHandled().get(Keys.IS_SITTING).orElse(false);
	}

	@Override
	public void setSitting(boolean sitting) {
		getHandled().offer(Keys.IS_SITTING, sitting);
	}

	@Override
	public boolean isTamed() {
		return getHandled().get(Keys.TAMED_OWNER).orElse(Optional.empty()).isPresent();
	}

	@Override
	public Optional<UUID> getTamer() {
		return getHandled().get(Keys.TAMED_OWNER).orElse(Optional.empty());
	}

	@Override
	public void setTamer(UUID tamer) {
		getHandled().offer(Keys.TAMED_OWNER, Optional.ofNullable(tamer));
	}

	@Override
	public Parrot getHandled() {
		return (Parrot) super.getHandled();
	}
}
