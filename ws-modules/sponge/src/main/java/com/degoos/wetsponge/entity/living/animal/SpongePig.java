package com.degoos.wetsponge.entity.living.animal;


import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.manipulator.mutable.entity.PigSaddleData;
import org.spongepowered.api.entity.living.animal.Pig;

public class SpongePig extends SpongeAnimal implements WSPig {


	public SpongePig (Pig entity) {
		super(entity);
	}


	@Override
	public boolean isSaddled () {
		return getHandled().getPigSaddleData().saddle().get();
	}


	@Override
	public void setSaddled (boolean saddled) {
		PigSaddleData saddleData = getHandled().getPigSaddleData();
		saddleData.set(Keys.PIG_SADDLE, saddled);
		getHandled().offer(saddleData);
	}


	@Override
	public Pig getHandled () {
		return (Pig) super.getHandled();
	}
}
