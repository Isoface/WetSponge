package com.degoos.wetsponge.entity.living.animal;

import com.degoos.wetsponge.enums.EnumDyeColor;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.DyeColor;
import org.spongepowered.api.data.type.DyeColors;
import org.spongepowered.api.entity.living.animal.Wolf;

import java.util.Optional;
import java.util.UUID;

public class SpongeWolf extends SpongeAnimal implements WSWolf {


    public SpongeWolf(Wolf entity) {
        super(entity);
    }

    @Override
    public EnumDyeColor getCollarColor() {
        return EnumDyeColor.getByName(getHandled().get(Keys.DYE_COLOR).orElse(DyeColors.RED).getName()).orElse(EnumDyeColor.RED);
    }

    @Override
    public void setCollarColor(EnumDyeColor collarColor) {
        getHandled().offer(Keys.DYE_COLOR, Sponge.getRegistry().getType(DyeColor.class, collarColor.getName()).orElse(DyeColors.RED));
    }

    @Override
    public boolean isAngry() {
        return getHandled().get(Keys.ANGRY).orElse(false);
    }

    @Override
    public void setAngry(boolean angry) {
        getHandled().offer(Keys.ANGRY, angry);
    }

    @Override
    public boolean isSitting() {
        return getHandled().get(Keys.IS_SITTING).orElse(false);
    }

    @Override
    public void setSitting(boolean sitting) {
        getHandled().offer(Keys.IS_SITTING, sitting);
    }

    @Override
    public boolean isTamed() {
        return getHandled().get(Keys.TAMED_OWNER).orElse(Optional.empty()).isPresent();
    }

    @Override
    public Optional<UUID> getTamer() {
        return getHandled().get(Keys.TAMED_OWNER).orElse(Optional.empty());
    }

    @Override
    public void setTamer(UUID tamer) {
        getHandled().offer(Keys.TAMED_OWNER, Optional.ofNullable(tamer));
    }

    @Override
    public Wolf getHandled() {
        return (Wolf) super.getHandled();
    }
}
