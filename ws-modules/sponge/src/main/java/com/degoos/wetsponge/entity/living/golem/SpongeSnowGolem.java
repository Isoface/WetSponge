package com.degoos.wetsponge.entity.living.golem;

import com.degoos.wetsponge.entity.living.SpongeCreature;
import net.minecraft.entity.monster.EntitySnowman;
import org.spongepowered.api.entity.living.golem.SnowGolem;

public class SpongeSnowGolem extends SpongeCreature implements WSSnowGolem {


	public SpongeSnowGolem(SnowGolem entity) {
		super(entity);
	}

	@Override
	public boolean hasPumpkinEquipped() {
		return ((EntitySnowman) getHandled()).isPumpkinEquipped();
	}

	@Override
	public void setPumpkinEquipped(boolean pumpkinEquipped) {
		((EntitySnowman) getHandled()).setPumpkinEquipped(pumpkinEquipped);
	}

	@Override
	public SnowGolem getHandled() {
		return (SnowGolem) super.getHandled();
	}
}
