package com.degoos.wetsponge.entity.living.monster;

import com.degoos.wetsponge.item.SpongeItemStack;
import com.degoos.wetsponge.material.WSMaterial;
import com.degoos.wetsponge.material.block.WSBlockType;
import org.spongepowered.api.entity.living.monster.Enderman;
import org.spongepowered.api.item.inventory.ItemStack;

import java.util.Optional;

public class SpongeEnderman extends SpongeMonster implements WSEnderman {


	public SpongeEnderman(Enderman entity) {
		super(entity);
	}

	@Override
	public Optional<WSBlockType> getCarriedBlock() {
		Optional<ItemStack> optional = getHandled().getInventory().peek();
		if (!optional.isPresent()) return Optional.empty();

		SpongeItemStack itemStack = new SpongeItemStack(optional.get());
		WSMaterial material = itemStack.getMaterial();
		if (material instanceof WSBlockType) return Optional.of((WSBlockType) material);
		else return Optional.empty();
	}

	@Override
	public void setCarriedBlock(WSBlockType carriedMaterial) {
		if (carriedMaterial == null) getHandled().getInventory().set(null);
		else getHandled().getInventory().set(new SpongeItemStack(carriedMaterial).getHandled());
	}

	@Override
	public Enderman getHandled() {
		return (Enderman) super.getHandled();
	}


}
