package com.degoos.wetsponge.entity.living.monster;

import org.spongepowered.api.entity.living.monster.Guardian;

public class SpongeGuardian extends SpongeMonster implements WSGuardian {


    public SpongeGuardian(Guardian entity) {
        super(entity);
    }


    @Override
    public Guardian getHandled() {
        return (Guardian) super.getHandled();
    }
}
