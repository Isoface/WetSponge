package com.degoos.wetsponge.entity.living.monster;


import com.degoos.wetsponge.enums.EnumEquipType;
import com.degoos.wetsponge.item.SpongeItemStack;
import com.degoos.wetsponge.item.WSItemStack;
import org.spongepowered.api.data.type.HandTypes;
import org.spongepowered.api.entity.living.monster.Skeleton;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.equipment.EquipmentTypes;

import java.util.Optional;

public class SpongeSkeleton extends SpongeMonster implements WSSkeleton {


    public SpongeSkeleton(Skeleton entity) {
        super(entity);
    }

    @Override
    public Optional<WSItemStack> getEquippedItem(EnumEquipType type) {
        Optional<ItemStack> itemStack;
        switch (type) {
            case HELMET:
                itemStack = getHandled().getHelmet();
                break;
            case CHESTPLATE:
                itemStack = getHandled().getChestplate();
                break;
            case LEGGINGS:
                itemStack = getHandled().getLeggings();
                break;
            case BOOTS:
                itemStack = getHandled().getBoots();
                break;
            case MAIN_HAND:
                itemStack = getHandled().getItemInHand(HandTypes.MAIN_HAND);
                break;
            case OFF_HAND:
                itemStack = getHandled().getItemInHand(HandTypes.OFF_HAND);
                break;
            default:
                itemStack = Optional.empty();
        }
        return itemStack.map(SpongeItemStack::new);
    }


    @Override
    public void setEquippedItem(EnumEquipType type, WSItemStack itemStack) {
        switch (type) {
            case HELMET:
                getHandled().setHelmet(itemStack == null ? null : ((SpongeItemStack) itemStack).getHandled());
                break;
            case CHESTPLATE:
                getHandled().setChestplate(itemStack == null ? null : ((SpongeItemStack) itemStack).getHandled());
                break;
            case LEGGINGS:
                getHandled().setLeggings(itemStack == null ? null : ((SpongeItemStack) itemStack).getHandled());
                break;
            case BOOTS:
                getHandled().setBoots(itemStack == null ? null : ((SpongeItemStack) itemStack).getHandled());
                break;
            case MAIN_HAND:
                getHandled().setItemInHand(HandTypes.MAIN_HAND, itemStack == null ? null : ((SpongeItemStack) itemStack).getHandled());
                break;
            case OFF_HAND:
                getHandled().setItemInHand(HandTypes.OFF_HAND, itemStack == null ? null : ((SpongeItemStack) itemStack).getHandled());
                break;
        }
    }


    @Override
    public Skeleton getHandled() {
        return (Skeleton) super.getHandled();
    }
}
