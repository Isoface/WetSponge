package com.degoos.wetsponge.entity.living.monster;

import org.spongepowered.api.entity.living.monster.Stray;

public class SpongeStray extends SpongeSkeleton implements WSStray {

	public SpongeStray(Stray entity) {
		super(entity);
	}

	@Override
	public Stray getHandled() {
		return (Stray) super.getHandled();
	}
}
