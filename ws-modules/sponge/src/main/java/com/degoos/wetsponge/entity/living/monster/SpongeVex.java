package com.degoos.wetsponge.entity.living.monster;

import org.spongepowered.api.entity.living.monster.Vex;

public class SpongeVex extends SpongeMonster implements WSVex {


    public SpongeVex(Vex entity) {
        super(entity);
    }

    @Override
    public Vex getHandled() {
        return (Vex) super.getHandled();
    }
}
