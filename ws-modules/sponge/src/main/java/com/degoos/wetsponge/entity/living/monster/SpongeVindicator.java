package com.degoos.wetsponge.entity.living.monster;


import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.living.monster.Vindicator;

public class SpongeVindicator extends SpongeMonster implements WSVindicator {


    public SpongeVindicator(Vindicator entity) {
        super(entity);
    }

    @Override
    public boolean isJonny() {
        return getHandled().johnny().get();
    }

    @Override
    public void setJonny(boolean jonny) {
        getHandled().offer(Keys.JOHNNY_VINDICATOR, jonny);
    }

    @Override
    public Vindicator getHandled() {
        return (Vindicator) super.getHandled();
    }


}
