package com.degoos.wetsponge.entity.living.player;

import com.degoos.wetsponge.entity.living.SpongeLivingEntity;
import com.degoos.wetsponge.enums.EnumEquipType;
import com.degoos.wetsponge.enums.EnumGameMode;
import com.degoos.wetsponge.inventory.SpongeInventory;
import com.degoos.wetsponge.inventory.WSInventory;
import com.degoos.wetsponge.item.SpongeItemStack;
import com.degoos.wetsponge.item.WSItemStack;
import com.degoos.wetsponge.user.SpongeGameProfile;
import com.degoos.wetsponge.user.WSGameProfile;
import net.minecraft.entity.player.EntityPlayer;
import org.spongepowered.api.data.type.HandTypes;
import org.spongepowered.api.entity.living.Human;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.Inventory;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.profile.GameProfile;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

public class SpongeHuman extends SpongeLivingEntity implements WSHuman {

	public SpongeHuman(Human entity) {
		super(entity);
	}

	@Override
	public int getFoodLevel() {
		return getHandled().foodLevel().get();
	}

	@Override
	public void setFoodLevel(int foodLevel) {
		getHandled().foodLevel().set(foodLevel);
	}

	@Override
	public WSInventory getInventory() {
		return new SpongeInventory(getHandled().getInventory());
	}

	@Override
	public WSInventory getEnderChestInventory() {
		return new SpongeInventory((Inventory) ((EntityPlayer) getHandled()).getInventoryEnderChest());
	}

	@Override
	public Optional<WSInventory> getOpenInventory() {
		return Optional.ofNullable(((EntityPlayer) getHandled()).openContainer).map(inventory -> new SpongeInventory((Inventory) inventory));
	}

	@Override
	public void openInventory(WSInventory inventory) {
		if (inventory == null) closeInventory();
	}

	@Override
	public void closeInventory() {
	}

	@Override
	public Optional<WSItemStack> getItemOnCursor() {
		ItemStack itemStack = (ItemStack) (Object) ((EntityPlayer) getHandled()).inventory.getItemStack();
		if (itemStack == null || itemStack.getItem().getId().equals(ItemTypes.AIR.getId())) return Optional.empty();
		return Optional.of(new SpongeItemStack(itemStack));
	}

	@Override
	public void setItemOnCursor(WSItemStack itemOnCursor) {
		if (itemOnCursor == null) ((EntityPlayer) getHandled()).inventory.setItemStack(net.minecraft.item.ItemStack.EMPTY);
		else ((EntityPlayer) getHandled()).inventory.setItemStack((net.minecraft.item.ItemStack) (Object) ((SpongeItemStack) itemOnCursor).getHandled());
	}

	@Override
	public EnumGameMode getGameMode() {
		return EnumGameMode.SURVIVAL;
	}

	@Override
	public void setGameMode(EnumGameMode gameMode) {
	}

	@Override
	public WSGameProfile getProfile() {
		return new SpongeGameProfile((GameProfile) ((EntityPlayer) getHandled()).getGameProfile());
	}

	@Override
	public Optional<WSItemStack> getEquippedItem(EnumEquipType type) {
		Optional<ItemStack> itemStack;
		switch (type) {
			case HELMET:
				itemStack = getHandled().getHelmet();
				break;
			case CHESTPLATE:
				itemStack = getHandled().getChestplate();
				break;
			case LEGGINGS:
				itemStack = getHandled().getLeggings();
				break;
			case BOOTS:
				itemStack = getHandled().getBoots();
				break;
			case MAIN_HAND:
				itemStack = getHandled().getItemInHand(HandTypes.MAIN_HAND);
				break;
			case OFF_HAND:
				itemStack = getHandled().getItemInHand(HandTypes.OFF_HAND);
				break;
			default:
				itemStack = Optional.empty();
		}
		return itemStack.map(SpongeItemStack::new);
	}


	@Override
	public void setEquippedItem(EnumEquipType type, WSItemStack itemStack) {
		switch (type) {
			case HELMET:
				getHandled().setHelmet(itemStack == null ? null : ((SpongeItemStack) itemStack).getHandled());
				break;
			case CHESTPLATE:
				getHandled().setChestplate(itemStack == null ? null : ((SpongeItemStack) itemStack).getHandled());
				break;
			case LEGGINGS:
				getHandled().setLeggings(itemStack == null ? null : ((SpongeItemStack) itemStack).getHandled());
				break;
			case BOOTS:
				getHandled().setBoots(itemStack == null ? null : ((SpongeItemStack) itemStack).getHandled());
				break;
			case MAIN_HAND:
				getHandled().setItemInHand(HandTypes.MAIN_HAND, itemStack == null ? null : ((SpongeItemStack) itemStack).getHandled());
				break;
			case OFF_HAND:
				getHandled().setItemInHand(HandTypes.OFF_HAND, itemStack == null ? null : ((SpongeItemStack) itemStack).getHandled());
				break;
		}
	}

	@Override
	public boolean hasPermission(String name) {
		return false;
	}

	@Override
	public Set<String> getPermissions() {
		return new HashSet<>();
	}

	@Override
	public Human getHandled() {
		return (Human) super.getHandled();
	}
}
