package com.degoos.wetsponge.entity.other;

import com.degoos.wetsponge.color.WSColor;
import com.degoos.wetsponge.effect.potion.SpongePotionEffect;
import com.degoos.wetsponge.effect.potion.WSPotionEffect;
import com.degoos.wetsponge.entity.SpongeEntity;
import com.degoos.wetsponge.enums.EnumPotionEffectType;
import com.degoos.wetsponge.particle.WSParticle;
import com.degoos.wetsponge.particle.WSParticles;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.effect.particle.ParticleType;
import org.spongepowered.api.effect.particle.ParticleTypes;
import org.spongepowered.api.effect.potion.PotionEffect;
import org.spongepowered.api.entity.AreaEffectCloud;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class SpongeAreaEffectCloud extends SpongeEntity implements WSAreaEffectCloud {


    public SpongeAreaEffectCloud(AreaEffectCloud entity) {
        super(entity);
    }

    @Override
    public WSColor getColor() {
        return WSColor.ofRGB(getHandled().color().get().getRgb());
    }

    @Override
    public void setColor(WSColor color) {
        getHandled().offer(Keys.AREA_EFFECT_CLOUD_COLOR, org.spongepowered.api.util.Color.ofRgb(color.toRGB()));
    }

    @Override
    public double getRadius() {
        return getHandled().radius().get();
    }

    @Override
    public void setRadius(double radius) {
        getHandled().offer(Keys.AREA_EFFECT_CLOUD_RADIUS, radius);
    }

    @Override
    public WSParticle getParticle() {
        return WSParticles.getByMinecraftId(getHandled().particleType().get().getId()).orElse(WSParticles.SPLASH_POTION);
    }

    @Override
    public void setParticle(WSParticle particle) {
        getHandled().offer(Keys.AREA_EFFECT_CLOUD_PARTICLE_TYPE, Sponge.getRegistry().getType(ParticleType.class, particle.getMinecraftId()).orElse(ParticleTypes
                .SPLASH_POTION));
    }

    @Override
    public int getDuration() {
        return getHandled().duration().get();
    }

    @Override
    public void setDuration(int duration) {
        getHandled().offer(Keys.AREA_EFFECT_CLOUD_DURATION, duration);
    }

    @Override
    public int getWaitTime() {
        return getHandled().waitTime().get();
    }

    @Override
    public void setWaitTime(int waitTime) {
        getHandled().offer(Keys.AREA_EFFECT_CLOUD_WAIT_TIME, waitTime);
    }

    @Override
    public double getRadiusOnUse() {
        return getHandled().radiusOnUse().get();
    }

    @Override
    public void setRadiusOnUse(double radiusOnUse) {
        getHandled().offer(Keys.AREA_EFFECT_CLOUD_RADIUS_ON_USE, radiusOnUse);
    }

    @Override
    public double getRadiusPerTick() {
        return getHandled().radiusPerTick().get();
    }

    @Override
    public void setRadiusPerTick(double radiusPerTick) {
        getHandled().offer(Keys.AREA_EFFECT_CLOUD_RADIUS_PER_TICK, radiusPerTick);
    }

    @Override
    public int getDurationOnUse() {
        return getHandled().durationOnUse().get();
    }

    @Override
    public void setDurationOnUse(int durationOnUse) {
        getHandled().offer(Keys.AREA_EFFECT_CLOUD_DURATION_ON_USE, durationOnUse);
    }

    @Override
    public int getApplicationDelay() {
        return getHandled().applicationDelay().get();
    }

    @Override
    public void setApplicationDelay(int applicationDelay) {
        getHandled().offer(Keys.AREA_EFFECT_CLOUD_REAPPLICATION_DELAY, applicationDelay);
    }

    @Override
    public int getAge() {
        return getHandled().age().get();
    }

    @Override
    public void setAge(int age) {
        getHandled().offer(Keys.AREA_EFFECT_CLOUD_AGE, age);
    }

    @Override
    public void addPotionEffect(WSPotionEffect effect) {
        List<PotionEffect> list = getHandled().effects().get();
        list.add(((SpongePotionEffect) effect).getHandled());
        getHandled().offer(Keys.POTION_EFFECTS, list);
    }

    @Override
    public List<WSPotionEffect> getPotionEffects() {
        return getHandled().effects().get().stream().map(SpongePotionEffect::new).collect(Collectors.toList());
    }

    @Override
    public void clearAllPotionEffects() {
        getHandled().offer(Keys.POTION_EFFECTS, new ArrayList<>());
    }

    @Override
    public void removePotionEffect(EnumPotionEffectType potionEffectType) {
        getHandled().offer(Keys.POTION_EFFECTS, getHandled().get(Keys.POTION_EFFECTS).orElse(new ArrayList<>()).stream().filter(potionEffect ->
                !potionEffect.getType().getName().equals(potionEffectType.name())).collect(Collectors.toList()));
    }


    @Override
    public AreaEffectCloud getHandled() {
        return (AreaEffectCloud) super.getHandled();
    }
}
