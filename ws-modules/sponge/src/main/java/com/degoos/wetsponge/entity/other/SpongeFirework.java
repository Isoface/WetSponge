package com.degoos.wetsponge.entity.other;

import com.degoos.wetsponge.SpongeWetSponge;
import com.degoos.wetsponge.entity.SpongeEntity;
import com.degoos.wetsponge.firework.SpongeFireworkEffect;
import com.degoos.wetsponge.firework.WSFireworkEffect;
import com.degoos.wetsponge.util.reflection.ReflectionUtils;
import net.minecraft.entity.item.EntityFireworkRocket;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.projectile.Firework;
import org.spongepowered.api.event.cause.Cause;
import org.spongepowered.api.item.FireworkEffect;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class SpongeFirework extends SpongeEntity implements WSFirework {


    public SpongeFirework(Firework entity) {
        super(entity);
    }

    @Override
    public List<WSFireworkEffect> getFireworkEffects() {
        return getHandled().effects().get().stream().map(SpongeFireworkEffect::new).collect(Collectors.toList());
    }

    @Override
    public void setFireworkEffects(Collection<WSFireworkEffect> effects) {
        getHandled().offer(Keys.FIREWORK_EFFECTS, effects.stream().map(effect -> ((SpongeFireworkEffect) effect).getHandled()).collect(Collectors.toList()));
    }

    @Override
    public boolean addFireworkEffect(WSFireworkEffect effect) {
        List<FireworkEffect> effects = getHandled().get(Keys.FIREWORK_EFFECTS).orElse(new ArrayList<>());
        boolean added = effects.add(((SpongeFireworkEffect) effect).getHandled());
        return added && getHandled().offer(Keys.FIREWORK_EFFECTS, effects).isSuccessful();
    }


    @Override
    public boolean addFireworkEffects(WSFireworkEffect... effects) {
        List<FireworkEffect> list = getHandled().get(Keys.FIREWORK_EFFECTS).orElse(new ArrayList<>());
        boolean added = list.addAll(Arrays.stream(effects).map(effect -> ((SpongeFireworkEffect) effect).getHandled()).collect(Collectors.toList()));
        return added && getHandled().offer(Keys.FIREWORK_EFFECTS, list).isSuccessful();
    }

    @Override
    public boolean addFireworkEffects(List<WSFireworkEffect> effects) {
        List<FireworkEffect> list = getHandled().get(Keys.FIREWORK_EFFECTS).orElse(new ArrayList<>());
        boolean added = list.addAll(effects.stream().map(effect -> ((SpongeFireworkEffect) effect).getHandled()).collect(Collectors.toList()));
        return added && getHandled().offer(Keys.FIREWORK_EFFECTS, list).isSuccessful();
    }

    @Override
    public boolean removeFireworkEffect(WSFireworkEffect effect) {
        List<FireworkEffect> effects = getHandled().get(Keys.FIREWORK_EFFECTS).orElse(new ArrayList<>());
        boolean removed = effects.remove(((SpongeFireworkEffect) effect).getHandled());
        return removed && getHandled().offer(Keys.FIREWORK_EFFECTS, effects).isSuccessful();
    }

    @Override
    public boolean removeFireworkEffects(WSFireworkEffect... effects) {
        List<FireworkEffect> list = getHandled().get(Keys.FIREWORK_EFFECTS).orElse(new ArrayList<>());
        boolean removed = list.removeAll(Arrays.stream(effects).map(effect -> ((SpongeFireworkEffect) effect).getHandled()).collect(Collectors.toList()));
        return removed && getHandled().offer(Keys.FIREWORK_EFFECTS, list).isSuccessful();
    }

    @Override
    public boolean removeFireworkEffects(List<WSFireworkEffect> effects) {
        List<FireworkEffect> list = getHandled().get(Keys.FIREWORK_EFFECTS).orElse(new ArrayList<>());
        boolean removed = list.removeAll(effects.stream().map(effect -> ((SpongeFireworkEffect) effect).getHandled()).collect(Collectors.toList()));
        return removed && getHandled().offer(Keys.FIREWORK_EFFECTS, list).isSuccessful();
    }

    @Override
    public void clearFireworkEffects() {
        getHandled().offer(Keys.FIREWORK_EFFECTS, new ArrayList<>()).isSuccessful();
    }

    @Override
    public boolean hasEffects() {
        return getHandled().get(Keys.FIREWORK_EFFECTS).map(List::isEmpty).orElse(true);
    }

    @Override
    public int getEffectSize() {
        return getHandled().get(Keys.FIREWORK_EFFECTS).map(List::size).orElse(0);
    }

    @Override
    public int getLifeTime() {
        try {
            return ReflectionUtils.setAccessible(EntityFireworkRocket.class.getDeclaredFields()[3]).getInt(getHandled());
        } catch (Throwable ex) {
            ex.printStackTrace();
            return 0;
        }
    }

    @Override
    public void setLifeTime(int lifeTime) {
        try {
            ReflectionUtils.setAccessible(EntityFireworkRocket.class.getDeclaredFields()[3]).setInt(getHandled(), lifeTime);
        } catch (Throwable ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public int getFireworkAge() {
        try {
            return ReflectionUtils.setAccessible(EntityFireworkRocket.class.getDeclaredFields()[2]).getInt(getHandled());
        } catch (Throwable ex) {
            ex.printStackTrace();
            return 0;
        }
    }

    @Override
    public void setFireworkAge(int fireworkAge) {
        try {
            ReflectionUtils.setAccessible(EntityFireworkRocket.class.getDeclaredFields()[2]).setInt(getHandled(), fireworkAge);
        } catch (Throwable ex) {
            ex.printStackTrace();
        }
    }


    @Override
    public void detonate() {
        getHandled().detonate();
    }

    @Override
    public Firework getHandled() {
        return (Firework) super.getHandled();
    }
}
