package com.degoos.wetsponge.entity.projectile;

import com.degoos.wetsponge.enums.EnumPickupStatus;
import net.minecraft.entity.projectile.EntityArrow;
import net.minecraft.entity.projectile.EntityArrow.PickupStatus;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.projectile.arrow.Arrow;

public class SpongeArrow extends SpongeProjectile implements WSArrow {


	public SpongeArrow(Arrow entity) {
		super(entity);
	}


	@Override
	public double getDamage() {
		return getHandled().damage().get();
	}

	@Override
	public void setDamage(double damage) {
		getHandled().offer(Keys.ATTACK_DAMAGE, damage);
	}

	@Override
	public int getKnockbackStrength() {
		return getHandled().knockbackStrength().get();
	}

	@Override
	public void setKnockbackStrength(int knockbackStrength) {
		getHandled().offer(Keys.KNOCKBACK_STRENGTH, knockbackStrength);
	}

	@Override
	public EnumPickupStatus getPickupStatus() {
		return EnumPickupStatus.getByName(((EntityArrow) getHandled()).pickupStatus.name()).orElse(EnumPickupStatus.ALLOWED);
	}

	@Override
	public void setPickupStatus(EnumPickupStatus status) {
		((EntityArrow) getHandled()).pickupStatus = PickupStatus.valueOf(status.name());
	}


	@Override
	public Arrow getHandled() {
		return (Arrow) super.getHandled();
	}
}
