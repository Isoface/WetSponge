package com.degoos.wetsponge.entity.projectile;

import com.degoos.wetsponge.util.InternalLogger;
import com.flowpowered.math.vector.Vector3d;
import java.util.Optional;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.projectile.explosive.fireball.Fireball;

public class SpongeFireball extends SpongeProjectile implements WSFireball {

	public SpongeFireball(Fireball entity) {
		super(entity);
	}

	@Override
	public Vector3d getDirection() {
		return getHandled().getVelocity();
	}

	@Override
	public void setDirection(Vector3d direction) {
		getHandled().setVelocity(direction);
	}

	@Override
	public int setExplosionRadius() {
		return getHandled().get(Keys.EXPLOSION_RADIUS).orElse(Optional.of(0)).orElse(0);
	}

	@Override
	public void setExplosionRadius(int explosionRadius) {
		getHandled().offer(Keys.EXPLOSION_RADIUS, Optional.of(explosionRadius));
	}

	@Override
	public void detonate() {
		InternalLogger.sendWarning("Method WSFireball#detonate() is not supported.");
	}

	@Override
	public Fireball getHandled() {
		return (Fireball) super.getHandled();
	}
}
