package com.degoos.wetsponge.entity.projectile;

import com.degoos.wetsponge.block.SpongeBlock;
import com.degoos.wetsponge.block.tileentity.SpongeTileEntityDispenser;
import com.degoos.wetsponge.block.tileentity.WSTileEntityDispenser;
import com.degoos.wetsponge.parser.entity.SpongeEntityParser;
import com.degoos.wetsponge.entity.SpongeEntity;
import com.degoos.wetsponge.entity.living.SpongeLivingEntity;
import com.degoos.wetsponge.entity.living.WSLivingEntity;
import org.spongepowered.api.block.tileentity.carrier.Dispenser;
import org.spongepowered.api.entity.living.Living;
import org.spongepowered.api.entity.projectile.Projectile;

public class SpongeProjectile extends SpongeEntity implements WSProjectile {


    public SpongeProjectile(Projectile entity) {
        super(entity);
    }


    @Override
    public WSProjectileSource getShooter() {
        if (getHandled().getShooter() instanceof Living)
            return (WSProjectileSource) SpongeEntityParser.getWSEntity((Living) getHandled().getShooter());
        if (getHandled().getShooter() instanceof Dispenser)
            return new SpongeTileEntityDispenser(new SpongeBlock(((Dispenser) getHandled().getShooter()).getLocatableBlock().getLocation()));
        else return new WSUnkownProjectileSource();
    }

    @Override
    public void setShooter(WSProjectileSource source) {
        if (source instanceof WSLivingEntity)
            getHandled().setShooter(((SpongeLivingEntity) source).getHandled());
        if (source instanceof WSTileEntityDispenser)
            getHandled().setShooter(((SpongeTileEntityDispenser) source).getHandled());
    }

    @Override
    public Projectile getHandled() {
        return (Projectile) super.getHandled();
    }

}
