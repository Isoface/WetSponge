package com.degoos.wetsponge.entity.vehicle;

import com.degoos.wetsponge.entity.SpongeEntity;
import org.spongepowered.api.entity.vehicle.Boat;

public class SpongeBoat extends SpongeEntity implements WSBoat {

    public SpongeBoat(Boat entity) {
        super(entity);
    }

    @Override
    public boolean isInWater() {
        return getHandled().isInWater();
    }

    @Override
    public double getMaxSpeed() {
        return getHandled().getMaxSpeed();
    }

    @Override
    public void setMaxSpeed(double maxSpeed) {
        getHandled().setMaxSpeed(maxSpeed);
    }

    @Override
    public boolean canMoveOnLand() {
        return getHandled().canMoveOnLand();
    }

    @Override
    public void setMoveOnLand(boolean moveOnLand) {
        getHandled().setMoveOnLand(moveOnLand);
    }

    @Override
    public double getOccupiedDeceleration() {
        return getHandled().getOccupiedDeceleration();
    }

    @Override
    public void setOccupiedDeceleration(double occupiedDeceleration) {
        getHandled().setOccupiedDeceleration(occupiedDeceleration);
    }

    @Override
    public double getUnoccupiedDeceleration() {
        return getHandled().getUnoccupiedDeceleration();
    }

    @Override
    public void setUnoccupiedDeceleration(double unoccupiedDeceleration) {
        getHandled().setUnoccupiedDeceleration(unoccupiedDeceleration);
    }


    @Override
    public Boat getHandled() {
        return (Boat) super.getHandled();
    }

}
