package com.degoos.wetsponge.entity.weather;

import org.spongepowered.api.entity.weather.Lightning;

public class SpongeLightning extends SpongeWeatherEffect implements WSLightning {

	public SpongeLightning(Lightning entity) {
		super(entity);
	}

	@Override
	public Lightning getHandled() {
		return (Lightning) super.getHandled();
	}
}
