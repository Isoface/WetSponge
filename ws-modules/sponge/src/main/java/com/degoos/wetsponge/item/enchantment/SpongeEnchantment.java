package com.degoos.wetsponge.item.enchantment;

import com.degoos.wetsponge.item.SpongeItemStack;
import com.degoos.wetsponge.item.WSItemStack;
import com.degoos.wetsponge.text.translation.WSTranslation;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.item.enchantment.EnchantmentType;
import org.spongepowered.api.item.enchantment.EnchantmentTypes;

public class SpongeEnchantment implements WSEnchantment {

	private EnchantmentType enchantment;

	public SpongeEnchantment(EnchantmentType enchantment) {
		this.enchantment = enchantment;
	}

	public SpongeEnchantment(String spongeId) {
		enchantment = Sponge.getRegistry().getType(EnchantmentType.class, spongeId).orElse(EnchantmentTypes.PROTECTION);
	}

	@Override
	public WSTranslation getTranslation() {
		throw new IllegalAccessError("Not supported by Spigot");
	}

	@Override
	public String getName() {
		return enchantment.getName();
	}

	@Override
	public int getMaximumLevel() {
		return enchantment.getMaximumLevel();
	}

	@Override
	public int getMinimumLevel() {
		return enchantment.getMinimumLevel();
	}

	@Override
	public boolean canBeAppliedToStack(WSItemStack itemStack) {
		return enchantment.canBeAppliedToStack(((SpongeItemStack) itemStack).getHandled());
	}

	@Override
	public boolean isCompatibleWith(WSEnchantment enchantment) {
		return this.enchantment.isCompatibleWith(((SpongeEnchantment) enchantment).getHandled());
	}

	@Override
	public EnchantmentType getHandled() {
		return enchantment;
	}
}
