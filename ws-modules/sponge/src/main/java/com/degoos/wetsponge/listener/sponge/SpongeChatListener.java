package com.degoos.wetsponge.listener.sponge;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.entity.living.player.SpongePlayer;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.event.message.WSChatEvent;
import com.degoos.wetsponge.parser.player.PlayerParser;
import com.degoos.wetsponge.text.SpongeText;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.source.ConsoleSource;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.Order;
import org.spongepowered.api.event.message.MessageChannelEvent;
import org.spongepowered.api.text.TextRepresentable;
import org.spongepowered.api.text.channel.MessageChannel;
import org.spongepowered.api.text.channel.MessageReceiver;

public class SpongeChatListener {

	@Listener(order = Order.FIRST)
	public void onChat(MessageChannelEvent.Chat event) {

		WSPlayer player = event.getCause().first(Player.class).map(target -> PlayerParser.getOrCreatePlayer(target, target.getUniqueId())).orElse(null);

		WSChatEvent wetSpongeEvent = new WSChatEvent(player, SpongeText.of(event.getOriginalMessage()), SpongeText.of(event.getRawMessage()), SpongeText
			.of(event.getFormatter().getHeader().toText()), SpongeText.of(event.getFormatter().getBody().toText()), SpongeText
			.of(event.getFormatter().getFooter().toText()),
			event.getChannel().isPresent() && event.getChannel().get().getMembers().stream().anyMatch(t -> t instanceof ConsoleSource),
			event.getChannel().isPresent() ? event.getChannel().get().getMembers().stream().filter(t -> t instanceof Player)
				.map(target -> PlayerParser.getOrCreatePlayer(target, ((Player) target).getUniqueId())).collect(Collectors.toSet()) : new HashSet<>());
		WetSponge.getEventManager().callEvent(wetSpongeEvent);

		event.getFormatter().setHeader((TextRepresentable) wetSpongeEvent.getHeader().getHandled());
		event.getFormatter().setBody((TextRepresentable) wetSpongeEvent.getBody().getHandled());
		event.getFormatter().setFooter((TextRepresentable) wetSpongeEvent.getFooter().getHandled());

		Set<MessageReceiver> set = wetSpongeEvent.getTargets().stream().map(t -> ((SpongePlayer) t).getHandled()).collect(Collectors.toSet());
		if (wetSpongeEvent.isSendToConsole()) set.add(Sponge.getServer().getConsole());
		event.setChannel(MessageChannel.fixed(set));
		event.setCancelled(wetSpongeEvent.isCancelled());
	}

}
