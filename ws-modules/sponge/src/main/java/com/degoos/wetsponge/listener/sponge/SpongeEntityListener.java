package com.degoos.wetsponge.listener.sponge;

import com.degoos.wetsponge.SpongeWetSponge;
import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.block.SpongeBlock;
import com.degoos.wetsponge.entity.WSEntity;
import com.degoos.wetsponge.entity.living.WSLivingEntity;
import com.degoos.wetsponge.entity.modifier.WSDamageModifier;
import com.degoos.wetsponge.entity.modifier.WSHealthModifier;
import com.degoos.wetsponge.enums.EnumDamageModifierType;
import com.degoos.wetsponge.enums.EnumDamageType;
import com.degoos.wetsponge.enums.EnumHealingType;
import com.degoos.wetsponge.enums.EnumHealthModifierType;
import com.degoos.wetsponge.event.entity.WSEntityDamageByBlockEvent;
import com.degoos.wetsponge.event.entity.WSEntityDamageByEntityEvent;
import com.degoos.wetsponge.event.entity.WSEntityDamageEvent;
import com.degoos.wetsponge.event.entity.WSEntityDeathEvent;
import com.degoos.wetsponge.event.entity.WSEntityDeathEvent.Post;
import com.degoos.wetsponge.event.entity.WSEntityDeathEvent.Pre;
import com.degoos.wetsponge.event.entity.WSEntityDestructEvent;
import com.degoos.wetsponge.event.entity.WSEntityHealEvent;
import com.degoos.wetsponge.event.entity.WSEntitySpawnEvent;
import com.degoos.wetsponge.parser.entity.SpongeEntityParser;
import com.degoos.wetsponge.text.SpongeText;
import com.degoos.wetsponge.util.InternalLogger;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.Order;
import org.spongepowered.api.event.cause.Cause;
import org.spongepowered.api.event.cause.EventContext;
import org.spongepowered.api.event.cause.entity.damage.DamageFunction;
import org.spongepowered.api.event.cause.entity.damage.DamageModifier;
import org.spongepowered.api.event.cause.entity.damage.DamageModifierType;
import org.spongepowered.api.event.cause.entity.damage.DamageModifierTypes;
import org.spongepowered.api.event.cause.entity.damage.source.BlockDamageSource;
import org.spongepowered.api.event.cause.entity.damage.source.DamageSource;
import org.spongepowered.api.event.cause.entity.damage.source.EntityDamageSource;
import org.spongepowered.api.event.cause.entity.health.HealthFunction;
import org.spongepowered.api.event.cause.entity.health.HealthModifier;
import org.spongepowered.api.event.cause.entity.health.HealthModifierType;
import org.spongepowered.api.event.cause.entity.health.HealthModifierTypes;
import org.spongepowered.api.event.cause.entity.health.source.HealingSource;
import org.spongepowered.api.event.entity.DamageEntityEvent;
import org.spongepowered.api.event.entity.DestructEntityEvent;
import org.spongepowered.api.event.entity.DestructEntityEvent.Death;
import org.spongepowered.api.event.entity.HealEntityEvent;
import org.spongepowered.api.event.entity.SpawnEntityEvent;
import org.spongepowered.api.event.filter.cause.First;

public class SpongeEntityListener {

	@Listener(order = Order.FIRST)
	public void onEntitySpawn(SpawnEntityEvent event) {
		try {
			event.getEntities().forEach(entity -> {
				WSEntity wsEntity = SpongeEntityParser.getWSEntity(entity);
				WSEntitySpawnEvent wetSpongeEvent = new WSEntitySpawnEvent(wsEntity);
				WetSponge.getEventManager().callEvent(wetSpongeEvent);
				event.setCancelled(wetSpongeEvent.isCancelled());
			});
		} catch (Throwable ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was parsing the event Sponge-SpawnEntityEvent!");
		}
	}

	@Listener(order = Order.FIRST)
	public void onEntityDespawn(DestructEntityEvent event) {
		try {

			if (event instanceof Death) {
				Post wetSpongeEvent = new Post(SpongeEntityParser.getWSEntity(event.getTargetEntity()), false, SpongeText.of(event.getMessage()));
				WetSponge.getEventManager().callEvent(wetSpongeEvent);
				event.setMessage(((SpongeText) wetSpongeEvent.getDeathMessage()).getHandled());
			} else {
				WSEntity entity = SpongeEntityParser.getWSEntity(event.getTargetEntity());
				WSEntityDestructEvent wetSpongeEvent = new WSEntityDestructEvent(entity);
				WetSponge.getEventManager().callEvent(wetSpongeEvent);
				SpongeEntityParser.removeEntity(event.getTargetEntity());
			}
		} catch (Throwable ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was parsing the event Sponge-DestructEntityEvent!");
		}
	}


	@Listener(order = Order.FIRST)
	public void onEntityDamage(DamageEntityEvent event, @First DamageSource damageSource) {
		try {
			WSEntity entity = SpongeEntityParser.getWSEntity(event.getTargetEntity());
			if (entity.isInvincible()) {
				event.setCancelled(true);
				return;
			}
			Set<WSDamageModifier> modifiers = event.getModifiers().stream()
				.map(modifier -> EnumDamageModifierType.getBySpongeName(modifier.getModifier().getType().getId())
					.map(type -> new WSDamageModifier(type, modifier.getFunction())).orElse(null)).collect(Collectors.toSet());
			EnumDamageType type = EnumDamageType.getBySpongeName(damageSource.getType().getName()).orElse(EnumDamageType.CUSTOM);

			WSEntityDamageEvent wetSpongeEvent;

			if (damageSource instanceof EntityDamageSource)
				wetSpongeEvent = new WSEntityDamageByEntityEvent(entity, event.getBaseDamage(), modifiers, type, SpongeEntityParser
					.getWSEntity(((EntityDamageSource) damageSource).getSource()), event.getFinalDamage());
			else if (damageSource instanceof BlockDamageSource)
				wetSpongeEvent = new WSEntityDamageByBlockEvent(entity, event.getBaseDamage(), modifiers, type, new SpongeBlock(((BlockDamageSource) damageSource)
					.getLocation()), event.getFinalDamage());
			else wetSpongeEvent = new WSEntityDamageEvent(entity, event.getBaseDamage(), modifiers, type, event.getFinalDamage());

			WetSponge.getEventManager().callEvent(wetSpongeEvent);

			event.setBaseDamage(wetSpongeEvent.getBaseDamage());
			wetSpongeEvent.getDamageModifiers().stream().filter(Objects::nonNull)
				.map(modifier -> new DamageFunction(DamageModifier.builder().cause(Cause.of(EventContext.empty(), SpongeWetSponge.getInstance()))
					.type(Sponge.getRegistry().getType(DamageModifierType.class, modifier.getDamageModifierType().name()).orElse(DamageModifierTypes.ARMOR))
					.build(), modifier.getFunction())).collect(Collectors.toSet()).forEach(modifier -> event.setDamage(modifier.getModifier(), modifier.getFunction()));
			event.setCancelled(wetSpongeEvent.isCancelled());

			if (entity instanceof WSLivingEntity && !wetSpongeEvent.isCancelled() && ((WSLivingEntity) entity).getHealth() - event.getFinalDamage() <= 0) {
				WSEntityDeathEvent.Pre deathEvent = new Pre(entity);
				WetSponge.getEventManager().callEvent(deathEvent);
				event.setCancelled(deathEvent.isCancelled());
			}
		} catch (Throwable ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was parsing the event Sponge-DamageEntityEvent!");
		}
	}

	@Listener(order = Order.FIRST)
	public void onEntityHeal(HealEntityEvent event, @First HealingSource healingSource) {
		try {
			WSEntity entity = SpongeEntityParser.getWSEntity(event.getTargetEntity());

			Set<WSHealthModifier> modifiers = event.getModifiers().stream()
				.map(modifier -> EnumHealthModifierType.getBySpongeName(modifier.getModifier().getType().getId())
					.map(type -> new WSHealthModifier(type, modifier.getFunction())).orElse(null)).collect(Collectors.toSet());

			EnumHealingType type = EnumHealingType.getBySpongeName(healingSource.getHealingType().getName()).orElse(EnumHealingType.GENERIC);

			WSEntityHealEvent wetSpongeEvent = new WSEntityHealEvent(entity, event.getBaseHealAmount(), modifiers, type, event.getFinalHealAmount());

			WetSponge.getEventManager().callEvent(wetSpongeEvent);

			event.setBaseHealAmount(wetSpongeEvent.getBaseHealth());
			wetSpongeEvent.getHealthModifiers().stream()
				.map(modifier -> new HealthFunction(HealthModifier.builder().cause(Cause.of(EventContext.empty(), SpongeWetSponge.getInstance()))
					.type(Sponge.getRegistry().getType(HealthModifierType.class, modifier.getHealthModifierType().name()).orElse(HealthModifierTypes.ARMOR))
					.build(), modifier.getFunction())).collect(Collectors.toSet())
				.forEach(modifier -> event.setHealAmount(modifier.getModifier(), modifier.getFunction()));
			event.setCancelled(wetSpongeEvent.isCancelled());
		} catch (Throwable ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was parsing the event Sponge-HealEntityEvent!");
		}
	}

}
