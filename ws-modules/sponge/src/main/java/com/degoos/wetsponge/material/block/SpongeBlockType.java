package com.degoos.wetsponge.material.block;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.material.SpongeMaterial;
import com.degoos.wetsponge.util.Validate;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.data.value.ValueContainer;
import org.spongepowered.api.item.inventory.ItemStack;

import java.util.Objects;

public class SpongeBlockType implements SpongeMaterial, WSBlockType {


    private int numericalId;
    private String oldStringId, newStringId;
    private int maxStackSize;

    public SpongeBlockType(int numericalId, String oldStringId, String newStringId, int maxStackSize) {
        this.numericalId = numericalId < 0 ? -1 : numericalId;
        this.oldStringId = oldStringId == null || oldStringId.equals("") ? null : oldStringId;
        this.newStringId = newStringId;
        this.maxStackSize = Math.max(1, maxStackSize);
    }

    @Override
    public SpongeBlockType clone() {
        return new SpongeBlockType(numericalId, oldStringId, newStringId, maxStackSize);
    }

    @Override
    public int getNumericalId() {
        return numericalId;
    }

    @Override
    public String getStringId() {
        return WetSponge.getVersion().isOlderThan(EnumServerVersion.MINECRAFT_1_13) ? getOldStringId() : getNewStringId();
    }

    @Override
    public String getNewStringId() {
        return newStringId;
    }

    @Override
    public String getOldStringId() {
        return oldStringId;
    }

    @Override
    public int getMaxStackSize() {
        return maxStackSize;
    }

    @Override
    public SpongeBlockType readContainer(ValueContainer<?> valueContainer) {
        return this;
    }

    @Override
    public ItemStack writeItemStack(ItemStack itemStack) {
        return itemStack;
    }

    public BlockState writeBlockState(BlockState blockState) {
        return blockState;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SpongeBlockType that = (SpongeBlockType) o;
        return numericalId == that.numericalId &&
                maxStackSize == that.maxStackSize &&
                Objects.equals(oldStringId, that.oldStringId) &&
                Objects.equals(newStringId, that.newStringId);
    }

    @Override
    public int hashCode() {

        return Objects.hash(numericalId, oldStringId, newStringId, maxStackSize);
    }
}
