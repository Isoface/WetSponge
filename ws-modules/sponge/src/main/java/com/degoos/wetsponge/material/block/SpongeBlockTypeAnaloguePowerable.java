package com.degoos.wetsponge.material.block;

import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.value.ValueContainer;
import org.spongepowered.api.item.inventory.ItemStack;

import java.util.Objects;

public class SpongeBlockTypeAnaloguePowerable extends SpongeBlockType implements WSBlockTypeAnaloguePowerable {

    private int power, maximumPower;

    public SpongeBlockTypeAnaloguePowerable(int numericalId, String oldStringId, String newStringId, int maxStackSize, int power, int maximumPower) {
        super(numericalId, oldStringId, newStringId, maxStackSize);
        this.power = power;
        this.maximumPower = maximumPower;
    }

    @Override
    public int getPower() {
        return power;
    }

    @Override
    public void setPower(int power) {
        this.power = Math.min(maximumPower, Math.max(0, power));
    }

    @Override
    public int gerMaximumPower() {
        return maximumPower;
    }

    @Override
    public SpongeBlockTypeAnaloguePowerable clone() {
        return new SpongeBlockTypeAnaloguePowerable(getNumericalId(), getOldStringId(), getNewStringId(), getMaxStackSize(), power, maximumPower);
    }

    @Override
    public ItemStack writeItemStack(ItemStack itemStack) {
        super.writeItemStack(itemStack);
        itemStack.offer(Keys.POWER, power);
        return itemStack;
    }

    @Override
    public BlockState writeBlockState(BlockState blockState) {
        blockState = super.writeBlockState(blockState);
        return blockState.with(Keys.POWER, power).orElse(blockState);
    }

    @Override
    public SpongeBlockTypeAnaloguePowerable readContainer(ValueContainer<?> valueContainer) {
        super.readContainer(valueContainer);
        power = valueContainer.get(Keys.POWER).orElse(0);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SpongeBlockTypeAnaloguePowerable that = (SpongeBlockTypeAnaloguePowerable) o;
        return power == that.power &&
                maximumPower == that.maximumPower;
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), power, maximumPower);
    }
}
