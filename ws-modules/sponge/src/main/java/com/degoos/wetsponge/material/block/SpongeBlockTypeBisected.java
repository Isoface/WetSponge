package com.degoos.wetsponge.material.block;

import com.degoos.wetsponge.enums.block.EnumBlockTypeBisectedHalf;
import com.degoos.wetsponge.util.Validate;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.PortionTypes;
import org.spongepowered.api.data.value.ValueContainer;
import org.spongepowered.api.item.inventory.ItemStack;

import java.util.Objects;

public class SpongeBlockTypeBisected extends SpongeBlockType implements WSBlockTypeBisected {

    private EnumBlockTypeBisectedHalf half;

    public SpongeBlockTypeBisected(int numericalId, String oldStringId, String newStringId, int maxStackSize, EnumBlockTypeBisectedHalf half) {
        super(numericalId, oldStringId, newStringId, maxStackSize);
        Validate.notNull(half, "Half cannot be null!");
        this.half = half;
    }

    @Override
    public EnumBlockTypeBisectedHalf getHalf() {
        return half;
    }

    @Override
    public void setHalf(EnumBlockTypeBisectedHalf half) {
        Validate.notNull(half, "Half cannot be null!");
        this.half = half;
    }

    @Override

    public SpongeBlockTypeBisected clone() {
        return new SpongeBlockTypeBisected(getNumericalId(), getOldStringId(), getNewStringId(), getMaxStackSize(), half);
    }

    @Override
    public ItemStack writeItemStack(ItemStack itemStack) {
        super.writeItemStack(itemStack);
        itemStack.offer(Keys.PORTION_TYPE, half == EnumBlockTypeBisectedHalf.TOP ? PortionTypes.TOP : PortionTypes.BOTTOM);
        return itemStack;
    }

    @Override
    public BlockState writeBlockState(BlockState blockState) {
        blockState = super.writeBlockState(blockState);
        return blockState.with(Keys.PORTION_TYPE, half == EnumBlockTypeBisectedHalf.TOP ? PortionTypes.TOP : PortionTypes.BOTTOM).orElse(blockState);
    }

    @Override
    public SpongeBlockTypeBisected readContainer(ValueContainer<?> valueContainer) {
        super.readContainer(valueContainer);
        half = valueContainer.get(Keys.PORTION_TYPE)
                .orElse(PortionTypes.BOTTOM).equals(PortionTypes.TOP) ? EnumBlockTypeBisectedHalf.TOP : EnumBlockTypeBisectedHalf.BOTTOM;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SpongeBlockTypeBisected that = (SpongeBlockTypeBisected) o;
        return half == that.half;
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), half);
    }
}
