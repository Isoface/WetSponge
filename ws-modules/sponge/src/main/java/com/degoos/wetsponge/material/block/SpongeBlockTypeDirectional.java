package com.degoos.wetsponge.material.block;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.util.Validate;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.value.ValueContainer;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.util.Direction;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class SpongeBlockTypeDirectional extends SpongeBlockType implements WSBlockTypeDirectional {

    private EnumBlockFace facing;
    private Set<EnumBlockFace> faces;

    public SpongeBlockTypeDirectional(int numericalId, String oldStringId, String newStringId, int maxStackSize, EnumBlockFace facing, Set<EnumBlockFace> faces) {
        super(numericalId, oldStringId, newStringId, maxStackSize);
        Validate.notNull(facing, "Facing cannot be null!");
        this.facing = facing;
        this.faces = faces;
    }

    @Override
    public EnumBlockFace getFacing() {
        return facing;
    }

    @Override
    public void setFacing(EnumBlockFace blockFace) {
        Validate.notNull(blockFace, "Facing cannot be null!");
        this.facing = blockFace;
    }

    @Override
    public Set<EnumBlockFace> getFaces() {
        return new HashSet<>(faces);
    }

    @Override
    public SpongeBlockTypeDirectional clone() {
        return new SpongeBlockTypeDirectional(getNumericalId(), getOldStringId(), getNewStringId(), getMaxStackSize(), facing, new HashSet<>(faces));
    }

    @Override
    public ItemStack writeItemStack(ItemStack itemStack) {
        super.writeItemStack(itemStack);
        itemStack.offer(Keys.DIRECTION, Direction.valueOf(facing.getSpongeName()));
        return itemStack;
    }

    @Override
    public BlockState writeBlockState(BlockState blockState) {
        blockState = super.writeBlockState(blockState);
        return blockState.with(Keys.DIRECTION, Direction.valueOf(facing.getSpongeName())).orElse(blockState);
    }

    @Override
    public SpongeBlockTypeDirectional readContainer(ValueContainer<?> valueContainer) {
        super.readContainer(valueContainer);
        facing = valueContainer.get(Keys.DIRECTION).map(target ->
                EnumBlockFace.getBySpongeName(target.name()).orElseThrow(NullPointerException::new)).orElse(EnumBlockFace.NORTH);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SpongeBlockTypeDirectional that = (SpongeBlockTypeDirectional) o;
        return facing == that.facing &&
                Objects.equals(faces, that.faces);
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), facing, faces);
    }
}
