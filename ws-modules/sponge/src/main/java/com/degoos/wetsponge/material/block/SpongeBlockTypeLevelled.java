package com.degoos.wetsponge.material.block;

import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.value.ValueContainer;
import org.spongepowered.api.item.inventory.ItemStack;

import java.util.Objects;

public class SpongeBlockTypeLevelled extends SpongeBlockType implements WSBlockTypeLevelled {

    private int level, maximumLevel;

    public SpongeBlockTypeLevelled(int numericalId, String oldStringId, String newStringId, int maxStackSize, int level, int maximumLevel) {
        super(numericalId, oldStringId, newStringId, maxStackSize);
        this.level = level;
        this.maximumLevel = maximumLevel;
    }

    @Override
    public int getLevel() {
        return level;
    }

    @Override
    public void setLevel(int level) {
        this.level = Math.min(maximumLevel, Math.max(0, level));
    }

    @Override
    public int getMaximumLevel() {
        return maximumLevel;
    }

    @Override
    public SpongeBlockTypeLevelled clone() {
        return new SpongeBlockTypeLevelled(getNumericalId(), getOldStringId(), getNewStringId(), getMaxStackSize(), level, maximumLevel);
    }

    @Override
    public ItemStack writeItemStack(ItemStack itemStack) {
        super.writeItemStack(itemStack);
        itemStack.offer(Keys.FLUID_LEVEL, level);
        return itemStack;
    }

    @Override
    public BlockState writeBlockState(BlockState blockState) {
        blockState = super.writeBlockState(blockState);
        return blockState.with(Keys.FLUID_LEVEL, level).orElse(blockState);
    }

    @Override
    public SpongeBlockTypeLevelled readContainer(ValueContainer<?> valueContainer) {
        super.readContainer(valueContainer);
        level = valueContainer.get(Keys.FLUID_LEVEL).orElse(0);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SpongeBlockTypeLevelled that = (SpongeBlockTypeLevelled) o;
        return level == that.level &&
                maximumLevel == that.maximumLevel;
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), level, maximumLevel);
    }
}
