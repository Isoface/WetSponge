package com.degoos.wetsponge.material.block;

import java.util.Objects;

public class SpongeBlockTypeLightable extends SpongeBlockType implements WSBlockTypeLightable {

    private boolean lit;

    public SpongeBlockTypeLightable(int numericalId, String oldStringId, String newStringId, int maxStackSize, boolean lit) {
        super(numericalId, oldStringId, newStringId, maxStackSize);
        this.lit = lit;
    }

    @Override
    public boolean isLit() {
        return lit;
    }

    @Override
    public void setLit(boolean lit) {
        this.lit = lit;
    }

    @Override
    public SpongeBlockTypeLightable clone() {
        return new SpongeBlockTypeLightable(getNumericalId(), getOldStringId(), getNewStringId(), getMaxStackSize(), lit);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SpongeBlockTypeLightable that = (SpongeBlockTypeLightable) o;
        return lit == that.lit;
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), lit);
    }
}
