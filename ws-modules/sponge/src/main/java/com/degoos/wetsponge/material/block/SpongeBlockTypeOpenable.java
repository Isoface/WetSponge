package com.degoos.wetsponge.material.block;

import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.value.ValueContainer;
import org.spongepowered.api.item.inventory.ItemStack;

import java.util.Objects;

public class SpongeBlockTypeOpenable extends SpongeBlockType implements WSBlockTypeOpenable {

    private boolean open;

    public SpongeBlockTypeOpenable(int numericalId, String oldStringId, String newStringId, int maxStackSize, boolean open) {
        super(numericalId, oldStringId, newStringId, maxStackSize);
        this.open = open;
    }

    @Override
    public boolean isOpen() {
        return open;
    }

    @Override
    public void setOpen(boolean open) {
        this.open = open;
    }

    @Override
    public SpongeBlockTypeOpenable clone() {
        return new SpongeBlockTypeOpenable(getNumericalId(), getOldStringId(), getNewStringId(), getMaxStackSize(), open);
    }

    @Override
    public ItemStack writeItemStack(ItemStack itemStack) {
        super.writeItemStack(itemStack);
        itemStack.offer(Keys.OPEN, open);
        return itemStack;
    }

    @Override
    public BlockState writeBlockState(BlockState blockState) {
        blockState = super.writeBlockState(blockState);
        return blockState.with(Keys.OPEN, open).orElse(blockState);
    }

    @Override
    public SpongeBlockTypeOpenable readContainer(ValueContainer<?> valueContainer) {
        super.readContainer(valueContainer);
        open = valueContainer.get(Keys.OPEN).orElse(false);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SpongeBlockTypeOpenable that = (SpongeBlockTypeOpenable) o;
        return open == that.open;
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), open);
    }
}
