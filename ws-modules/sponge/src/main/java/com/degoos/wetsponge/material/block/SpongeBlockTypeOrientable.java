package com.degoos.wetsponge.material.block;

import com.degoos.wetsponge.enums.block.EnumAxis;
import com.degoos.wetsponge.util.Validate;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.value.ValueContainer;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.util.Axis;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class SpongeBlockTypeOrientable extends SpongeBlockType implements WSBlockTypeOrientable {

    private EnumAxis axis;
    private Set<EnumAxis> axes;

    public SpongeBlockTypeOrientable(int numericalId, String oldStringId, String newStringId, int maxStackSize, EnumAxis axis, Set<EnumAxis> axes) {
        super(numericalId, oldStringId, newStringId, maxStackSize);
        Validate.notNull(axis, "Axis cannot be null!");
        this.axis = axis;
        this.axes = axes;
    }

    @Override
    public EnumAxis getAxis() {
        return axis;
    }

    @Override
    public void setAxis(EnumAxis axis) {
        Validate.notNull(axis, "Axis cannot be null!");
        this.axis = axis;
    }

    @Override
    public Set<EnumAxis> getAxes() {
        return new HashSet<>(axes);
    }

    @Override
    public SpongeBlockTypeOrientable clone() {
        return new SpongeBlockTypeOrientable(getNumericalId(), getOldStringId(), getNewStringId(), getMaxStackSize(), axis, axes);
    }

    @Override
    public ItemStack writeItemStack(ItemStack itemStack) {
        super.writeItemStack(itemStack);
        itemStack.offer(Keys.AXIS, Axis.valueOf(axis.name()));
        return itemStack;
    }

    @Override
    public BlockState writeBlockState(BlockState blockState) {
        blockState = super.writeBlockState(blockState);
        return blockState.with(Keys.AXIS, Axis.valueOf(axis.name())).orElse(blockState);
    }

    @Override
    public SpongeBlockTypeOrientable readContainer(ValueContainer<?> valueContainer) {
        super.readContainer(valueContainer);
        axis = valueContainer.get(Keys.AXIS).map(target -> EnumAxis.valueOf(target.name())).orElse(EnumAxis.Y);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SpongeBlockTypeOrientable that = (SpongeBlockTypeOrientable) o;
        return axis == that.axis &&
                Objects.equals(axes, that.axes);
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), axis, axes);
    }
}
