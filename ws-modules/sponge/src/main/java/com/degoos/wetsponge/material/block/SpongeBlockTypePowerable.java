package com.degoos.wetsponge.material.block;

import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.value.ValueContainer;
import org.spongepowered.api.item.inventory.ItemStack;

import java.util.Objects;

public class SpongeBlockTypePowerable extends SpongeBlockType implements WSBlockTypePowerable {

    private boolean powered;

    public SpongeBlockTypePowerable(int numericalId, String oldStringId, String newStringId, int maxStackSize, boolean powered) {
        super(numericalId, oldStringId, newStringId, maxStackSize);
        this.powered = powered;
    }

    @Override
    public boolean isPowered() {
        return powered;
    }

    @Override
    public void setPowered(boolean powered) {
        this.powered = powered;
    }

    @Override
    public SpongeBlockTypePowerable clone() {
        return new SpongeBlockTypePowerable(getNumericalId(), getOldStringId(), getNewStringId(), getMaxStackSize(), powered);
    }

    @Override
    public ItemStack writeItemStack(ItemStack itemStack) {
        super.writeItemStack(itemStack);
        itemStack.offer(Keys.POWERED, powered);
        return itemStack;
    }

    @Override
    public BlockState writeBlockState(BlockState blockState) {
        blockState = super.writeBlockState(blockState);
        return blockState.with(Keys.POWERED, powered).orElse(blockState);
    }

    @Override
    public SpongeBlockTypePowerable readContainer(ValueContainer<?> valueContainer) {
        super.readContainer(valueContainer);
        powered = valueContainer.get(Keys.POWERED).orElse(false);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SpongeBlockTypePowerable that = (SpongeBlockTypePowerable) o;
        return powered == that.powered;
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), powered);
    }
}
