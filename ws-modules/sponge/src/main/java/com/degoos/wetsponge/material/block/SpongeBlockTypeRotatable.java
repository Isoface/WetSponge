package com.degoos.wetsponge.material.block;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.util.Validate;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.value.ValueContainer;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.util.Direction;

import java.util.Objects;

public class SpongeBlockTypeRotatable extends SpongeBlockType implements WSBlockTypeRotatable {

    private EnumBlockFace rotation;

    public SpongeBlockTypeRotatable(int numericalId, String oldStringId, String newStringId, int maxStackSize, EnumBlockFace rotation) {
        super(numericalId, oldStringId, newStringId, maxStackSize);
        this.rotation = rotation;
    }

    @Override
    public EnumBlockFace getRotation() {
        return rotation;
    }

    @Override
    public void setRotation(EnumBlockFace rotation) {
        Validate.notNull(rotation, "Rotation cannot be null!");
        this.rotation = rotation;
    }

    @Override
    public SpongeBlockTypeRotatable clone() {
        return new SpongeBlockTypeRotatable(getNumericalId(), getOldStringId(), getNewStringId(), getMaxStackSize(), rotation);
    }

    @Override
    public ItemStack writeItemStack(ItemStack itemStack) {
        super.writeItemStack(itemStack);
        itemStack.offer(Keys.DIRECTION, Direction.valueOf(rotation.getSpongeName()));
        return itemStack;
    }

    @Override
    public BlockState writeBlockState(BlockState blockState) {
        blockState = super.writeBlockState(blockState);
        return blockState.with(Keys.DIRECTION, Direction.valueOf(rotation.getSpongeName())).orElse(blockState);
    }

    @Override
    public SpongeBlockTypeRotatable readContainer(ValueContainer<?> valueContainer) {
        super.readContainer(valueContainer);
        rotation = EnumBlockFace.getBySpongeName(valueContainer.get(Keys.DIRECTION).orElseThrow(NullPointerException::new).name()).orElseThrow(NullPointerException::new);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SpongeBlockTypeRotatable that = (SpongeBlockTypeRotatable) o;
        return rotation == that.rotation;
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), rotation);
    }
}
