package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.enums.block.EnumBlockTypeAnvilDamage;
import com.degoos.wetsponge.material.block.SpongeBlockTypeDirectional;
import com.degoos.wetsponge.util.Validate;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.data.value.ValueContainer;
import org.spongepowered.api.item.inventory.ItemStack;

import java.util.Objects;
import java.util.Set;

public class SpongeBlockTypeAnvil extends SpongeBlockTypeDirectional implements WSBlockTypeAnvil {

    private EnumBlockTypeAnvilDamage damage;

    public SpongeBlockTypeAnvil(EnumBlockFace facing, Set<EnumBlockFace> faces, EnumBlockTypeAnvilDamage damage) {
        super(145, "minecraft:anvil", "minecraft:anvil", 64, facing, faces);
        Validate.notNull(damage, "Damage cannot be null!");
        this.damage = damage;
    }

    @Override
    public String getNewStringId() {
        switch (damage) {
            case DAMAGED:
                return "minecraft:chipped_anvil";
            case VERY_DAMAGED:
                return "minecraft:damaged_anvil";
            case NORMAL:
            default:
                return "minecraft:anvil";
        }
    }

    @Override
    public EnumBlockTypeAnvilDamage getDamage() {
        return damage;
    }

    @Override
    public void setDamage(EnumBlockTypeAnvilDamage damage) {
        Validate.notNull(damage, "Damage cannot be null!");
        this.damage = damage;
    }

    @Override
    public SpongeBlockTypeAnvil clone() {
        return new SpongeBlockTypeAnvil(getFacing(), getFaces(), damage);
    }

    @Override
    public ItemStack writeItemStack(ItemStack itemStack) {
        super.writeItemStack(itemStack);
        return itemStack;
    }

    @Override
    public BlockState writeBlockState(BlockState blockState) {
        return super.writeBlockState(blockState);
    }

    @Override
    public SpongeBlockTypeAnvil readContainer(ValueContainer<?> valueContainer) {
        super.readContainer(valueContainer);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SpongeBlockTypeAnvil that = (SpongeBlockTypeAnvil) o;
        return damage == that.damage;
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), damage);
    }
}
