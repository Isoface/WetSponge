package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.material.block.SpongeBlockType;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.value.ValueContainer;
import org.spongepowered.api.item.inventory.ItemStack;

import java.util.Objects;

public class SpongeBlockTypeCake extends SpongeBlockType implements WSBlockTypeCake {

    private int bites, maximumBites;

    public SpongeBlockTypeCake(int bites, int maximumBites) {
        super(92, "minecraft:cake", "minecraft:cake", 64);
        this.bites = bites;
        this.maximumBites = maximumBites;
    }

    @Override
    public int getBites() {
        return bites;
    }

    @Override
    public void setBites(int bites) {
        this.bites = Math.min(maximumBites, Math.max(0, bites));
    }

    @Override
    public int getMaximumBites() {
        return maximumBites;
    }

    @Override
    public SpongeBlockTypeCake clone() {
        return new SpongeBlockTypeCake(bites, maximumBites);
    }

    @Override
    public ItemStack writeItemStack(ItemStack itemStack) {
        super.writeItemStack(itemStack);
        itemStack.offer(Keys.LAYER, bites);
        return itemStack;
    }

    @Override
    public BlockState writeBlockState(BlockState blockState) {
        blockState = super.writeBlockState(blockState);
        return blockState.with(Keys.LAYER, bites).orElse(blockState);
    }

    @Override
    public SpongeBlockTypeCake readContainer(ValueContainer<?> valueContainer) {
        super.readContainer(valueContainer);
        bites = valueContainer.get(Keys.LAYER).orElse(0);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SpongeBlockTypeCake that = (SpongeBlockTypeCake) o;
        return bites == that.bites &&
                maximumBites == that.maximumBites;
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), bites, maximumBites);
    }
}
