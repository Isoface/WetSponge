package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.enums.block.EnumBlockTypeChestType;
import com.degoos.wetsponge.material.block.SpongeBlockTypeDirectional;
import com.degoos.wetsponge.util.Validate;

import java.util.Objects;
import java.util.Set;

public class SpongeBlockTypeChest extends SpongeBlockTypeDirectional implements WSBlockTypeChest {

    private EnumBlockTypeChestType chestType;
    private boolean waterlogged;

    public SpongeBlockTypeChest(int numericalId, String oldStringId, String newStringId, int maxStackSize, EnumBlockFace facing, Set<EnumBlockFace> faces,
                                EnumBlockTypeChestType chestType, boolean waterlogged) {
        super(numericalId, oldStringId, newStringId, maxStackSize, facing, faces);
        Validate.notNull(chestType, "ChestType cannot be null!");
        this.chestType = chestType;
        this.waterlogged = waterlogged;
    }

    @Override
    public EnumBlockTypeChestType getType() {
        return chestType;
    }

    @Override
    public void setType(EnumBlockTypeChestType type) {
        Validate.notNull(type, "ChestType cannot be null!");
        this.chestType = type;
    }

    @Override
    public boolean isWaterlogged() {
        return waterlogged;
    }

    @Override
    public void setWaterlogged(boolean waterlogged) {
        this.waterlogged = waterlogged;
    }

    @Override
    public SpongeBlockTypeChest clone() {
        return new SpongeBlockTypeChest(getNumericalId(), getOldStringId(), getNewStringId(), getMaxStackSize(), getFacing(), getFaces(), chestType, waterlogged);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SpongeBlockTypeChest that = (SpongeBlockTypeChest) o;
        return waterlogged == that.waterlogged &&
                chestType == that.chestType;
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), chestType, waterlogged);
    }
}
