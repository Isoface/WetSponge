package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.WallTypes;
import org.spongepowered.api.data.value.ValueContainer;
import org.spongepowered.api.item.inventory.ItemStack;

import java.util.Objects;
import java.util.Set;

public class SpongeBlockTypeCobblestoneWall extends SpongeBlockTypeFence implements WSBlockTypeCobblestoneWall {

    private boolean mossy;

    public SpongeBlockTypeCobblestoneWall(Set<EnumBlockFace> faces, Set<EnumBlockFace> allowedFaces, boolean waterlogged, boolean mossy) {
        super(139, "minecraft:cobblestone_wall", "minecraft:cobblestone_wall", 64, faces, allowedFaces, waterlogged);
        this.mossy = mossy;
    }

    @Override
    public String getNewStringId() {
        return mossy ? "minecraft:mossy_cobblestone_wall" : "minecraft:cobblestone_wall";
    }

    @Override
    public boolean isMossy() {
        return mossy;
    }

    @Override
    public void setMossy(boolean mossy) {
        this.mossy = mossy;
    }

    @Override
    public SpongeBlockTypeCobblestoneWall clone() {
        return new SpongeBlockTypeCobblestoneWall(getFaces(), getAllowedFaces(), isWaterlogged(), mossy);
    }

    @Override
    public ItemStack writeItemStack(ItemStack itemStack) {
        super.writeItemStack(itemStack);
        itemStack.offer(Keys.WALL_TYPE, mossy ? WallTypes.MOSSY : WallTypes.NORMAL);
        return itemStack;
    }

    @Override
    public BlockState writeBlockState(BlockState blockState) {
        blockState = super.writeBlockState(blockState);
        return blockState.with(Keys.WALL_TYPE, mossy ? WallTypes.MOSSY : WallTypes.NORMAL).orElse(blockState);
    }

    @Override
    public SpongeBlockTypeCobblestoneWall readContainer(ValueContainer<?> valueContainer) {
        super.readContainer(valueContainer);
        mossy = valueContainer.get(Keys.WALL_TYPE).orElse(WallTypes.NORMAL).equals(WallTypes.MOSSY);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SpongeBlockTypeCobblestoneWall that = (SpongeBlockTypeCobblestoneWall) o;
        return mossy == that.mossy;
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), mossy);
    }
}
