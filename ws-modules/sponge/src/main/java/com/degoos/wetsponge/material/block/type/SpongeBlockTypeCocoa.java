package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.material.block.SpongeBlockTypeDirectional;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.value.ValueContainer;
import org.spongepowered.api.item.inventory.ItemStack;

import java.util.Objects;
import java.util.Set;

public class SpongeBlockTypeCocoa extends SpongeBlockTypeDirectional implements WSBlockTypeCocoa {

    private int age, maximumAge;

    public SpongeBlockTypeCocoa(EnumBlockFace facing, Set<EnumBlockFace> faces, int age, int maximumAge) {
        super(127, "minecraft:cocoa", "minecraft:cocoa", 64, facing, faces);
        this.age = age;
        this.maximumAge = maximumAge;
    }

    @Override
    public int getAge() {
        return age;
    }

    @Override
    public void setAge(int age) {
        this.age = Math.min(maximumAge, Math.max(0, age));
    }

    @Override
    public int getMaximumAge() {
        return maximumAge;
    }

    @Override
    public SpongeBlockTypeCocoa clone() {
        return new SpongeBlockTypeCocoa(getFacing(), getFaces(), age, maximumAge);
    }

    @Override

    public ItemStack writeItemStack(ItemStack itemStack) {
        super.writeItemStack(itemStack);
        itemStack.offer(Keys.GROWTH_STAGE, age);
        return itemStack;
    }

    @Override
    public BlockState writeBlockState(BlockState blockState) {
        blockState = super.writeBlockState(blockState);
        return blockState.with(Keys.GROWTH_STAGE, age).orElse(blockState);
    }

    @Override
    public SpongeBlockTypeCocoa readContainer(ValueContainer<?> valueContainer) {
        super.readContainer(valueContainer);
        age = valueContainer.get(Keys.GROWTH_STAGE).orElse(0);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SpongeBlockTypeCocoa that = (SpongeBlockTypeCocoa) o;
        return age == that.age &&
                maximumAge == that.maximumAge;
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), age, maximumAge);
    }
}
