package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.material.block.SpongeBlockTypeDirectional;

import java.util.Objects;
import java.util.Set;

public class SpongeBlockTypeCommandBlock extends SpongeBlockTypeDirectional implements WSBlockTypeCommandBlock {

    private boolean conditional;

    public SpongeBlockTypeCommandBlock(int numericalId, String oldStringId, String newStringId, int maxStackSize, EnumBlockFace facing, Set<EnumBlockFace> faces, boolean conditional) {
        super(numericalId, oldStringId, newStringId, maxStackSize, facing, faces);
        this.conditional = conditional;
    }

    @Override
    public boolean isConditional() {
        return conditional;
    }

    @Override
    public void setConditional(boolean conditional) {
        this.conditional = conditional;
    }

    @Override
    public SpongeBlockTypeCommandBlock clone() {
        return new SpongeBlockTypeCommandBlock(getNumericalId(), getOldStringId(), getNewStringId(), getMaxStackSize(), getFacing(), getFaces(), conditional);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SpongeBlockTypeCommandBlock that = (SpongeBlockTypeCommandBlock) o;
        return conditional == that.conditional;
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), conditional);
    }
}
