package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.material.block.SpongeBlockTypeDirectional;

import java.util.Objects;
import java.util.Set;

public class SpongeBlockTypeCoralWallFan extends SpongeBlockTypeDirectional implements WSBlockTypeCoralWallFan {

    private boolean waterlogged;

    public SpongeBlockTypeCoralWallFan(int numericalId, String oldStringId, String newStringId, int maxStackSize, EnumBlockFace facing, Set<EnumBlockFace> faces, boolean waterlogged) {
        super(numericalId, oldStringId, newStringId, maxStackSize, facing, faces);
        this.waterlogged = waterlogged;
    }

    @Override
    public boolean isWaterlogged() {
        return waterlogged;
    }

    @Override
    public void setWaterlogged(boolean waterlogged) {
        this.waterlogged = waterlogged;
    }

    @Override
    public SpongeBlockTypeCoralWallFan clone() {
        return new SpongeBlockTypeCoralWallFan(getNumericalId(), getOldStringId(), getNewStringId(), getMaxStackSize(), getFacing(), getFaces(), waterlogged);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SpongeBlockTypeCoralWallFan that = (SpongeBlockTypeCoralWallFan) o;
        return waterlogged == that.waterlogged;
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), waterlogged);
    }
}
