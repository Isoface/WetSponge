package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.material.block.SpongeBlockTypeDirectional;
import net.minecraft.block.BlockDispenser;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.state.IBlockState;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.data.value.ValueContainer;
import org.spongepowered.api.item.inventory.ItemStack;

import java.util.Objects;
import java.util.Set;

public class SpongeBlockTypeDispenser extends SpongeBlockTypeDirectional implements WSBlockTypeDispenser {

    private boolean triggered;

    public SpongeBlockTypeDispenser(int numericalId, String oldStringId, String newStringId, int maxStackSize, EnumBlockFace facing, Set<EnumBlockFace> faces, boolean triggered) {
        super(numericalId, oldStringId, newStringId, maxStackSize, facing, faces);
        this.triggered = triggered;
    }

    @Override
    public boolean isTriggered() {
        return true;
    }

    @Override
    public void setTriggered(boolean triggered) {
        this.triggered = triggered;
    }

    @Override
    public SpongeBlockTypeDispenser clone() {
        return new SpongeBlockTypeDispenser(getNumericalId(), getOldStringId(), getNewStringId(), getMaxStackSize(), getFacing(), getFaces(), triggered);
    }

    @Override
    public ItemStack writeItemStack(ItemStack itemStack) {
        super.writeItemStack(itemStack);
        return itemStack;
    }

    @Override
    public BlockState writeBlockState(BlockState blockState) {
        blockState = super.writeBlockState(blockState);
        IProperty<Boolean> property = BlockDispenser.TRIGGERED;
        blockState = (BlockState) ((IBlockState) blockState).withProperty(property, triggered);
        return blockState;
    }

    @Override
    public SpongeBlockTypeDispenser readContainer(ValueContainer<?> valueContainer) {
        super.readContainer(valueContainer);
        if (valueContainer instanceof IBlockState) {
            IProperty<Boolean> property = BlockDispenser.TRIGGERED;
            try {
                triggered = ((IBlockState) valueContainer).getValue(property);
            } catch (Exception ex) {
                triggered = false;
            }
        }
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SpongeBlockTypeDispenser that = (SpongeBlockTypeDispenser) o;
        return triggered == that.triggered;
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), triggered);
    }
}
