package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockTypeBisectedHalf;
import com.degoos.wetsponge.enums.block.EnumBlockTypeDoublePlantType;
import com.degoos.wetsponge.material.block.SpongeBlockTypeBisected;
import com.degoos.wetsponge.util.Validate;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.DoublePlantType;
import org.spongepowered.api.data.value.ValueContainer;
import org.spongepowered.api.item.inventory.ItemStack;

import java.util.Objects;

public class SpongeBlockTypeDoublePlant extends SpongeBlockTypeBisected implements WSBlockTypeDoublePlant {

    private EnumBlockTypeDoublePlantType doublePlantType;

    public SpongeBlockTypeDoublePlant(EnumBlockTypeBisectedHalf half, EnumBlockTypeDoublePlantType doublePlantType) {
        super(175, "minecraft:double_plant", "minecraft:sunflower", 64, half);
        Validate.notNull(doublePlantType, "Double plant type cannot be null!");
        this.doublePlantType = doublePlantType;
    }

    @Override
    public String getNewStringId() {
        switch (doublePlantType) {
            case LILAC:
                return "minecraft:lilac";
            case DOUBLE_TALLGRASS:
                return "minecraft:tall_grass";
            case LARGE_FERN:
                return "minecraft:large_fern";
            case ROSE_BUSH:
                return "minecraft:rose_bush";
            case PEONY:
                return "minecraft:peony";
            case SUNFLOWER:
            default:
                return "minecraft:sunflower";
        }
    }

    @Override
    public EnumBlockTypeDoublePlantType getDoublePlantType() {
        return doublePlantType;
    }

    @Override
    public void setDoublePlantType(EnumBlockTypeDoublePlantType doublePlantType) {
        Validate.notNull(doublePlantType, "Double plant type cannot be null!");
        this.doublePlantType = doublePlantType;
    }

    @Override
    public SpongeBlockTypeDoublePlant clone() {
        return new SpongeBlockTypeDoublePlant(getHalf(), doublePlantType);
    }

    @Override
    public ItemStack writeItemStack(ItemStack itemStack) {
        super.writeItemStack(itemStack);
        itemStack.offer(Keys.DOUBLE_PLANT_TYPE, Sponge.getRegistry().getType(DoublePlantType.class, doublePlantType.getSpongeName()).orElseThrow(NullPointerException::new));
        return itemStack;
    }

    @Override
    public BlockState writeBlockState(BlockState blockState) {
        blockState = super.writeBlockState(blockState);
        return blockState.with(Keys.DOUBLE_PLANT_TYPE, Sponge.getRegistry().getType(DoublePlantType.class,
                doublePlantType.getSpongeName()).orElseThrow(NullPointerException::new)).orElse(blockState);
    }

    @Override
    public SpongeBlockTypeDoublePlant readContainer(ValueContainer<?> valueContainer) {
        super.readContainer(valueContainer);
        doublePlantType = valueContainer.get(Keys.DOUBLE_PLANT_TYPE).map(target -> EnumBlockTypeDoublePlantType.getBySpongeName(target.getName())
                .orElseThrow(NullPointerException::new)).orElse(EnumBlockTypeDoublePlantType.SUNFLOWER);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SpongeBlockTypeDoublePlant that = (SpongeBlockTypeDoublePlant) o;
        return doublePlantType == that.doublePlantType;
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), doublePlantType);
    }
}
