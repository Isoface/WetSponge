package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.material.block.SpongeBlockTypeDirectional;
import net.minecraft.block.BlockEndPortalFrame;
import net.minecraft.block.state.IBlockState;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.value.ValueContainer;
import org.spongepowered.api.item.inventory.ItemStack;

import java.util.Objects;
import java.util.Set;

public class SpongeBlockTypeEndPortalFrame extends SpongeBlockTypeDirectional implements WSBlockTypeEndPortalFrame {

    private boolean eye;

    public SpongeBlockTypeEndPortalFrame(EnumBlockFace facing, Set<EnumBlockFace> faces, boolean eye) {
        super(120, "minecraft:end_portal_frame", "minecraft:end_portal_frame", 64, facing, faces);
        this.eye = eye;
    }

    @Override
    public boolean hasEye() {
        return eye;
    }

    @Override
    public void setEye(boolean eye) {
        this.eye = eye;
    }

    @Override
    public SpongeBlockTypeEndPortalFrame clone() {
        return new SpongeBlockTypeEndPortalFrame(getFacing(), getFaces(), eye);
    }

    @Override
    public ItemStack writeItemStack(ItemStack itemStack) {
        super.writeItemStack(itemStack);
        return itemStack;
    }

    @Override
    public BlockState writeBlockState(BlockState blockState) {
        blockState = super.writeBlockState(blockState);
        return (BlockState) ((IBlockState) blockState).withProperty(BlockEndPortalFrame.EYE, eye);
    }

    @Override
    public SpongeBlockTypeEndPortalFrame readContainer(ValueContainer<?> valueContainer) {
        super.readContainer(valueContainer);
        if (valueContainer instanceof IBlockState) {
            eye = ((IBlockState) valueContainer).getValue(BlockEndPortalFrame.EYE);
        }
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SpongeBlockTypeEndPortalFrame that = (SpongeBlockTypeEndPortalFrame) o;
        return eye == that.eye;
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), eye);
    }
}
