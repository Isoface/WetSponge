package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.material.block.SpongeBlockTypeDirectional;

import java.util.Objects;
import java.util.Set;

public class SpongeBlockTypeEnderchest extends SpongeBlockTypeDirectional implements WSBlockTypeEnderchest {

    private boolean waterlogged;

    public SpongeBlockTypeEnderchest(EnumBlockFace facing, Set<EnumBlockFace> faces, boolean waterlogged) {
        super(130, "minecraft:ender_chest", "minecraft:ender_chest", 64, facing, faces);
        this.waterlogged = waterlogged;
    }

    @Override
    public boolean isWaterlogged() {
        return waterlogged;
    }

    @Override
    public void setWaterlogged(boolean waterlogged) {
        this.waterlogged = waterlogged;
    }

    @Override
    public SpongeBlockTypeEnderchest clone() {
        return new SpongeBlockTypeEnderchest(getFacing(), getFaces(), waterlogged);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SpongeBlockTypeEnderchest that = (SpongeBlockTypeEnderchest) o;
        return waterlogged == that.waterlogged;
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), waterlogged);
    }
}
