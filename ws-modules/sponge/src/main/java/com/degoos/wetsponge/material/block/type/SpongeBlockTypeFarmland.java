package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.material.block.SpongeBlockType;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.value.ValueContainer;
import org.spongepowered.api.item.inventory.ItemStack;

import java.util.Objects;

public class SpongeBlockTypeFarmland extends SpongeBlockType implements WSBlockTypeFarmland {

    private int moisture, maximumMoisture;

    public SpongeBlockTypeFarmland(int moisture, int maximumMoisture) {
        super(60, "minecraft:farmland", "minecraft:farmland", 64);
        this.moisture = moisture;
        this.maximumMoisture = maximumMoisture;
    }

    @Override
    public int getMoisture() {
        return moisture;
    }

    @Override
    public void setMoisture(int moisture) {
        this.moisture = moisture;
    }

    @Override
    public int getMaximumMoisture() {
        return maximumMoisture;
    }

    @Override
    public SpongeBlockTypeFarmland clone() {
        return new SpongeBlockTypeFarmland(moisture, maximumMoisture);
    }

    @Override
    public ItemStack writeItemStack(ItemStack itemStack) {
        super.writeItemStack(itemStack);
        itemStack.offer(Keys.MOISTURE, moisture);
        return itemStack;
    }

    @Override
    public BlockState writeBlockState(BlockState blockState) {
        blockState = super.writeBlockState(blockState);
        return blockState.with(Keys.MOISTURE, moisture).orElse(blockState);
    }

    @Override
    public SpongeBlockTypeFarmland readContainer(ValueContainer<?> valueContainer) {
        super.readContainer(valueContainer);
        moisture = valueContainer.get(Keys.MOISTURE).orElse(0);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SpongeBlockTypeFarmland that = (SpongeBlockTypeFarmland) o;
        return moisture == that.moisture &&
                maximumMoisture == that.maximumMoisture;
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), moisture, maximumMoisture);
    }
}
