package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.material.block.SpongeBlockTypeMultipleFacing;
import net.minecraft.block.BlockFire;
import net.minecraft.block.state.BlockStateBase;
import net.minecraft.block.state.IBlockState;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.data.value.ValueContainer;
import org.spongepowered.api.item.inventory.ItemStack;

import java.util.Objects;
import java.util.Set;

public class SpongeBlockTypeFire extends SpongeBlockTypeMultipleFacing implements WSBlockTypeFire {

    private int age, maximumAge;

    public SpongeBlockTypeFire(Set<EnumBlockFace> faces, Set<EnumBlockFace> allowedFaces, int age, int maximumAge) {
        super(51, "minecraft:fire", "minecraft:fire", 64, faces, allowedFaces);
        this.age = age;
        this.maximumAge = maximumAge;
    }

    @Override
    public int getAge() {
        return age;
    }

    @Override
    public void setAge(int age) {
        this.age = Math.min(maximumAge, Math.max(0, age));
    }

    @Override
    public int getMaximumAge() {
        return maximumAge;
    }

    @Override
    public SpongeBlockTypeFire clone() {
        return new SpongeBlockTypeFire(getFaces(), getAllowedFaces(), age, maximumAge);
    }

    @Override
    public ItemStack writeItemStack(ItemStack itemStack) {
        return itemStack;
    }

    @Override
    public BlockState writeBlockState(BlockState blockState) {
        IBlockState iBlockState = (IBlockState) blockState;
        iBlockState = iBlockState.withProperty(BlockFire.NORTH, hasFace(EnumBlockFace.NORTH));
        iBlockState = iBlockState.withProperty(BlockFire.SOUTH, hasFace(EnumBlockFace.SOUTH));
        iBlockState = iBlockState.withProperty(BlockFire.EAST, hasFace(EnumBlockFace.EAST));
        iBlockState = iBlockState.withProperty(BlockFire.WEST, hasFace(EnumBlockFace.WEST));
        iBlockState = iBlockState.withProperty(BlockFire.UPPER, hasFace(EnumBlockFace.UP));
        iBlockState = iBlockState.withProperty(BlockFire.AGE, age);
        return (BlockState) iBlockState;
    }

    @Override
    public SpongeBlockTypeFire readContainer(ValueContainer<?> valueContainer) {
        if (!(valueContainer instanceof IBlockState)) return this;
        IBlockState iBlockState = (IBlockState) valueContainer;
        age = iBlockState.getValue(BlockFire.AGE);
        setFace(EnumBlockFace.NORTH, iBlockState.getValue(BlockFire.NORTH));
        setFace(EnumBlockFace.SOUTH, iBlockState.getValue(BlockFire.SOUTH));
        setFace(EnumBlockFace.EAST, iBlockState.getValue(BlockFire.EAST));
        setFace(EnumBlockFace.WEST, iBlockState.getValue(BlockFire.WEST));
        setFace(EnumBlockFace.UP, iBlockState.getValue(BlockFire.UPPER));
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SpongeBlockTypeFire that = (SpongeBlockTypeFire) o;
        return age == that.age &&
                maximumAge == that.maximumAge;
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), age, maximumAge);
    }

}
