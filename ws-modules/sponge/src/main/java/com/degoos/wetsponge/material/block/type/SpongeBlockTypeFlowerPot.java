package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockTypePottedPlant;
import com.degoos.wetsponge.material.block.SpongeBlockType;
import net.minecraft.block.BlockFlowerPot;
import net.minecraft.block.state.IBlockState;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.data.value.ValueContainer;
import org.spongepowered.api.item.inventory.ItemStack;

import java.util.Objects;

public class SpongeBlockTypeFlowerPot extends SpongeBlockType implements WSBlockTypeFlowerPot {

    private EnumBlockTypePottedPlant pottedPlant;

    public SpongeBlockTypeFlowerPot(EnumBlockTypePottedPlant pottedPlant) {
        super(140, "minecraft:flower_pot", "minecraft:flower_pot", 64);
        this.pottedPlant = pottedPlant == null ? EnumBlockTypePottedPlant.NONE : pottedPlant;
    }

    @Override
    public String getNewStringId() {
        return pottedPlant == EnumBlockTypePottedPlant.NONE ? "minecraft:flower_pot" : "minecraft:potted_" + pottedPlant.name().toLowerCase();
    }

    @Override
    public EnumBlockTypePottedPlant getPottedPlant() {
        return pottedPlant;
    }

    @Override
    public void setPottedPlant(EnumBlockTypePottedPlant pottedPlant) {
        this.pottedPlant = pottedPlant == null ? EnumBlockTypePottedPlant.NONE : pottedPlant;
    }

    @Override
    public SpongeBlockTypeFlowerPot clone() {
        return new SpongeBlockTypeFlowerPot(pottedPlant);
    }

    @Override
    public ItemStack writeItemStack(ItemStack itemStack) {
        return super.writeItemStack(itemStack);
    }

    @Override
    public BlockState writeBlockState(BlockState blockState) {
        return (BlockState) ((IBlockState) blockState).withProperty(BlockFlowerPot.CONTENTS, BlockFlowerPot.EnumFlowerType.valueOf(pottedPlant.getOldMinecraftName()));
    }

    @Override
    public SpongeBlockTypeFlowerPot readContainer(ValueContainer<?> valueContainer) {
        if (!(valueContainer instanceof IBlockState)) return this;

        IBlockState iBlockState = (IBlockState) valueContainer;

        pottedPlant = EnumBlockTypePottedPlant.getByOldMinecraftName(iBlockState.getValue(BlockFlowerPot.CONTENTS).getName()).orElseThrow(NullPointerException::new);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SpongeBlockTypeFlowerPot that = (SpongeBlockTypeFlowerPot) o;
        return pottedPlant == that.pottedPlant;
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), pottedPlant);
    }
}
