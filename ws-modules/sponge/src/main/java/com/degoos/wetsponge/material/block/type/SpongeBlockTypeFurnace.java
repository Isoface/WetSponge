package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.material.block.SpongeBlockTypeDirectional;

import java.util.Objects;
import java.util.Set;

public class SpongeBlockTypeFurnace extends SpongeBlockTypeDirectional implements WSBlockTypeFurnace {

    private boolean lit;

    public SpongeBlockTypeFurnace(EnumBlockFace facing, Set<EnumBlockFace> faces, boolean lit) {
        super(61, "minecraft:furnace", "minecraft:furnace", 64, facing, faces);
        this.lit = lit;
    }

    @Override
    public int getNumericalId() {
        return isLit() ? 62 : 61;
    }

    @Override
    public String getOldStringId() {
        return isLit() ? "minecraft:lit_furnace" : "minecraft:furnace";
    }

    @Override
    public boolean isLit() {
        return lit;
    }

    @Override
    public void setLit(boolean lit) {
        this.lit = lit;
    }

    @Override
    public SpongeBlockTypeFurnace clone() {
        return new SpongeBlockTypeFurnace(getFacing(), getFaces(), lit);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SpongeBlockTypeFurnace that = (SpongeBlockTypeFurnace) o;
        return lit == that.lit;
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), lit);
    }
}
