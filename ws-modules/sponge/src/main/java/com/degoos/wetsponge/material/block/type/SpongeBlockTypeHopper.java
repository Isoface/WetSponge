package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.material.block.SpongeBlockTypeDirectional;
import net.minecraft.block.BlockHopper;
import net.minecraft.block.state.IBlockState;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.data.value.ValueContainer;
import org.spongepowered.api.item.inventory.ItemStack;

import java.util.Objects;
import java.util.Set;

public class SpongeBlockTypeHopper extends SpongeBlockTypeDirectional implements WSBlockTypeHopper {

    private boolean enabled;

    public SpongeBlockTypeHopper(EnumBlockFace facing, Set<EnumBlockFace> faces, boolean enabled) {
        super(154, "minecraft:hopper", "minecraft:hopper", 64, facing, faces);
        this.enabled = enabled;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    @Override
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override

    public SpongeBlockTypeHopper clone() {
        return new SpongeBlockTypeHopper(getFacing(), getFaces(), enabled);
    }

    @Override
    public ItemStack writeItemStack(ItemStack itemStack) {
        super.writeItemStack(itemStack);
        return itemStack;
    }

    @Override
    public BlockState writeBlockState(BlockState blockState) {
        blockState = super.writeBlockState(blockState);
        return (BlockState) ((IBlockState) blockState).withProperty(BlockHopper.ENABLED, enabled);
    }

    @Override
    public SpongeBlockTypeHopper readContainer(ValueContainer<?> valueContainer) {
        super.readContainer(valueContainer);
        if (!(valueContainer instanceof IBlockState)) return this;
        enabled = ((IBlockState) valueContainer).getValue(BlockHopper.ENABLED);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SpongeBlockTypeHopper that = (SpongeBlockTypeHopper) o;
        return enabled == that.enabled;
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), enabled);
    }
}
