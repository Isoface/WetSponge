package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockTypeDisguiseType;
import com.degoos.wetsponge.material.block.SpongeBlockType;
import com.degoos.wetsponge.util.Validate;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.DisguisedBlockType;
import org.spongepowered.api.data.value.ValueContainer;
import org.spongepowered.api.item.inventory.ItemStack;

import java.util.Objects;

public class SpongeBlockTypeInfestedStone extends SpongeBlockType implements WSBlockTypeInfestedStone {

    private EnumBlockTypeDisguiseType disguiseType;

    public SpongeBlockTypeInfestedStone(EnumBlockTypeDisguiseType disguiseType) {
        super(97, "minecraft:monster_egg", "minecraft:infested_stone", 64);
        Validate.notNull(disguiseType, "Disguise type cannot be null!");
        this.disguiseType = disguiseType;
    }

    @Override
    public String getNewStringId() {
        switch (disguiseType) {
            case STONE_BRICK:
                return "minecraft:infested_stone_bricks";
            case COBBLESTONE:
                return "minecraft:infested_cobblestone";
            case MOSSY_STONE_BRICK:
                return "minecraft:infested_mossy_stone_bricks";
            case CRACKED_STONE_BRICK:
                return "minecraft:infested_cracked_stone_bricks";
            case CHISELED_STONE_BRICK:
                return "minecraft:infested_chiseled_stone_bricks";
            case STONE:
            default:
                return "minecraft:infested_stone";
        }
    }

    @Override
    public EnumBlockTypeDisguiseType getDisguiseType() {
        return disguiseType;
    }

    @Override
    public void setDisguiseType(EnumBlockTypeDisguiseType disguiseType) {
        Validate.notNull(disguiseType, "Disguise type cannot be null!");
        this.disguiseType = disguiseType;
    }

    @Override
    public SpongeBlockTypeInfestedStone clone() {
        return new SpongeBlockTypeInfestedStone(disguiseType);
    }

    @Override
    public ItemStack writeItemStack(ItemStack itemStack) {
        super.writeItemStack(itemStack);
        itemStack.offer(Keys.DISGUISED_BLOCK_TYPE, Sponge.getRegistry().getType(DisguisedBlockType.class, disguiseType.name()).orElseThrow(NullPointerException::new));
        return itemStack;
    }

    @Override
    public BlockState writeBlockState(BlockState blockState) {
        blockState = super.writeBlockState(blockState);
        return blockState.with(Keys.DISGUISED_BLOCK_TYPE, Sponge.getRegistry().getType(DisguisedBlockType.class,
                disguiseType.name()).orElseThrow(NullPointerException::new)).orElse(blockState);
    }

    @Override
    public SpongeBlockTypeInfestedStone readContainer(ValueContainer<?> valueContainer) {
        super.readContainer(valueContainer);
        disguiseType = EnumBlockTypeDisguiseType.getByName(valueContainer.get(Keys.DISGUISED_BLOCK_TYPE).get().getName()).orElse(EnumBlockTypeDisguiseType.STONE);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SpongeBlockTypeInfestedStone that = (SpongeBlockTypeInfestedStone) o;
        return disguiseType == that.disguiseType;
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), disguiseType);
    }
}
