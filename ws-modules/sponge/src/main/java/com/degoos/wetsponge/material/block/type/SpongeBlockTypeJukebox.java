package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.material.block.SpongeBlockType;
import net.minecraft.block.BlockJukebox;
import net.minecraft.block.state.IBlockState;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.data.value.ValueContainer;
import org.spongepowered.api.item.inventory.ItemStack;

import java.util.Objects;

public class SpongeBlockTypeJukebox extends SpongeBlockType implements WSBlockTypeJukebox {

    private boolean record;

    public SpongeBlockTypeJukebox(boolean record) {
        super(84, "minecraft:jukebox", "minecraft:jukebox", 64);
        this.record = record;
    }

    @Override
    public boolean hasRecord() {
        return record;
    }

    @Override
    public SpongeBlockTypeJukebox clone() {
        return new SpongeBlockTypeJukebox(record);
    }

    @Override
    public ItemStack writeItemStack(ItemStack itemStack) {
        super.writeItemStack(itemStack);
        return itemStack;
    }

    @Override
    public BlockState writeBlockState(BlockState blockState) {
        blockState = super.writeBlockState(blockState);
        return (BlockState) ((IBlockState) blockState).withProperty(BlockJukebox.HAS_RECORD, record);
    }

    @Override
    public SpongeBlockTypeJukebox readContainer(ValueContainer<?> valueContainer) {
        super.readContainer(valueContainer);
        if (valueContainer instanceof IBlockState)
            record = ((IBlockState) valueContainer).getValue(BlockJukebox.HAS_RECORD);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SpongeBlockTypeJukebox that = (SpongeBlockTypeJukebox) o;
        return record == that.record;
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), record);
    }
}
