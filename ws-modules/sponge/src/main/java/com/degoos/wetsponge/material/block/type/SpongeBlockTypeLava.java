package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.material.block.SpongeBlockTypeLevelled;

public class SpongeBlockTypeLava extends SpongeBlockTypeLevelled implements WSBlockTypeLava {

    public SpongeBlockTypeLava(int level, int maximumLevel) {
        super(9, "minecraft:lava", "minecraft:lava", 64, level, maximumLevel);
    }

    @Override
    public int getNumericalId() {
        return getLevel() == 0 ? 11 : 10;
    }

    @Override
    public String getOldStringId() {
        return getLevel() == 0 ? "minecraft:lava" : "minecraft:flowing_lava";
    }

    @Override
    public SpongeBlockTypeLava clone() {
        return new SpongeBlockTypeLava(getLevel(), getMaximumLevel());
    }
}
