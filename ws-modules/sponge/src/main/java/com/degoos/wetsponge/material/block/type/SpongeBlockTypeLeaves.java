package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumWoodType;
import com.degoos.wetsponge.material.block.SpongeBlockType;
import com.degoos.wetsponge.util.Validate;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.TreeType;
import org.spongepowered.api.data.type.TreeTypes;
import org.spongepowered.api.data.value.ValueContainer;
import org.spongepowered.api.item.inventory.ItemStack;

import java.util.Objects;

public class SpongeBlockTypeLeaves extends SpongeBlockType implements WSBlockTypeLeaves {

    private boolean persistent;
    private int distance;
    private EnumWoodType woodType;

    public SpongeBlockTypeLeaves(boolean persistent, int distance, EnumWoodType woodType) {
        super(18, "minecraft_leaves", "minecraft:leaves", 64);
        Validate.notNull(woodType, "Wood type cannot be null!");
        this.persistent = persistent;
        this.distance = Math.min(7, Math.max(1, distance));
        this.woodType = woodType;
    }

    @Override
    public int getNumericalId() {
        return getWoodType() == EnumWoodType.ACACIA || getWoodType() == EnumWoodType.DARK_OAK ? 18 : 161;
    }

    @Override
    public String getNewStringId() {
        return getWoodType() == EnumWoodType.ACACIA || getWoodType() == EnumWoodType.DARK_OAK ? "minecraft:leaves" : "minecraft:leaves2";
    }

    @Override
    public String getOldStringId() {
        return "minecraft:" + getWoodType().name().toLowerCase() + "_leaves";
    }

    @Override
    public boolean isPersistent() {
        return persistent;
    }

    @Override
    public void setPersistent(boolean persistent) {
        this.persistent = persistent;
    }

    @Override
    public int getDistance() {
        return distance;
    }

    @Override
    public void setDistance(int distance) {
        this.distance = Math.min(7, Math.max(1, distance));
    }

    @Override
    public EnumWoodType getWoodType() {
        return woodType;
    }

    @Override
    public void setWoodType(EnumWoodType woodType) {
        Validate.notNull(woodType, "Wood type cannot be null!");
        this.woodType = woodType;
    }

    @Override
    public SpongeBlockTypeLeaves clone() {
        return new SpongeBlockTypeLeaves(persistent, distance, woodType);
    }

    @Override
    public ItemStack writeItemStack(ItemStack itemStack) {
        super.writeItemStack(itemStack);
        itemStack.offer(Keys.DECAYABLE, !persistent);
        itemStack.offer(Keys.TREE_TYPE, Sponge.getRegistry().getType(TreeType.class, woodType.name()).orElseThrow(NullPointerException::new));
        return itemStack;
    }

    @Override
    public BlockState writeBlockState(BlockState blockState) {
        blockState = super.writeBlockState(blockState);
        blockState = blockState.with(Keys.DECAYABLE, !persistent).orElse(blockState);
        return blockState.with(Keys.TREE_TYPE, Sponge.getRegistry().getType(TreeType.class, woodType.name()).orElseThrow(NullPointerException::new)).orElse(blockState);
    }

    @Override
    public SpongeBlockTypeLeaves readContainer(ValueContainer<?> valueContainer) {
        super.readContainer(valueContainer);
        persistent = !valueContainer.get(Keys.DECAYABLE).orElse(true);
        woodType = EnumWoodType.getByName(valueContainer.get(Keys.TREE_TYPE).orElse(TreeTypes.OAK).getName()).orElseThrow(NullPointerException::new);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SpongeBlockTypeLeaves that = (SpongeBlockTypeLeaves) o;
        return persistent == that.persistent &&
                distance == that.distance &&
                woodType == that.woodType;
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), persistent, distance, woodType);
    }
}
