package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumAxis;
import com.degoos.wetsponge.enums.block.EnumWoodType;
import com.degoos.wetsponge.material.block.SpongeBlockTypeOrientable;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.LogAxes;
import org.spongepowered.api.data.type.LogAxis;
import org.spongepowered.api.data.type.TreeType;
import org.spongepowered.api.data.type.TreeTypes;
import org.spongepowered.api.data.value.ValueContainer;
import org.spongepowered.api.item.inventory.ItemStack;

import java.util.Objects;
import java.util.Set;

public class SpongeBlockTypeLog extends SpongeBlockTypeOrientable implements WSBlockTypeLog {

    private EnumWoodType woodType;
    private boolean stripped, allFacesCovered;

    public SpongeBlockTypeLog(EnumAxis axis, Set<EnumAxis> axes, EnumWoodType woodType, boolean stripped, boolean allFacesCovered) {
        super(17, "minecraft:log", "minecraft:log", 64, axis, axes);
        this.woodType = woodType;
        this.stripped = stripped;
        this.allFacesCovered = allFacesCovered;
    }

    @Override
    public int getNumericalId() {
        return getWoodType() == EnumWoodType.ACACIA || getWoodType() == EnumWoodType.DARK_OAK ? 162 : 17;
    }

    @Override
    public String getOldStringId() {
        return getWoodType() == EnumWoodType.ACACIA || getWoodType() == EnumWoodType.DARK_OAK ? "minecraft:log2" : "minecraft:log";
    }

    @Override
    public String getNewStringId() {
        return "minecraft:" + (stripped ? "stripped_" : "") + getWoodType().name().toLowerCase() + (allFacesCovered ? "_wood" : "_log");
    }

    @Override
    public EnumWoodType getWoodType() {
        return woodType;
    }

    @Override
    public void setWoodType(EnumWoodType woodType) {
        this.woodType = woodType;
    }

    @Override
    public boolean isStripped() {
        return stripped;
    }

    @Override
    public void setStripped(boolean stripped) {
        this.stripped = stripped;
    }

    @Override
    public boolean hasAllFacesCovered() {
        return allFacesCovered;
    }

    @Override
    public void setAllFacesCovered(boolean allFacesCovered) {
        this.allFacesCovered = allFacesCovered;
    }

    @Override
    public SpongeBlockTypeLog clone() {
        return new SpongeBlockTypeLog(getAxis(), getAxes(), woodType, stripped, allFacesCovered);
    }

    @Override
    public ItemStack writeItemStack(ItemStack itemStack) {
        itemStack.offer(Keys.LOG_AXIS, allFacesCovered ? LogAxes.NONE : Sponge.getRegistry().getType(LogAxis.class, getAxis().name()).orElseThrow(NullPointerException::new));
        itemStack.offer(Keys.TREE_TYPE, Sponge.getRegistry().getType(TreeType.class, woodType.name()).orElseThrow(NullPointerException::new));
        return itemStack;
    }

    @Override
    public BlockState writeBlockState(BlockState blockState) {
        blockState = blockState.with(Keys.LOG_AXIS,
                allFacesCovered ? LogAxes.NONE : Sponge.getRegistry().getType(LogAxis.class, getAxis().name()).orElseThrow(NullPointerException::new)).orElse(blockState);
        return blockState.with(Keys.TREE_TYPE, Sponge.getRegistry().getType(TreeType.class, woodType.name()).orElseThrow(NullPointerException::new)).orElse(blockState);
    }

    @Override
    public SpongeBlockTypeLog readContainer(ValueContainer<?> valueContainer) {
        woodType = EnumWoodType.getByName(valueContainer.get(Keys.TREE_TYPE).orElse(TreeTypes.OAK).getName()).orElseThrow(NullPointerException::new);
        LogAxis axis = valueContainer.get(Keys.LOG_AXIS).orElse(LogAxes.NONE);
        if (axis.getName().equals(LogAxes.NONE.getName())) {
            allFacesCovered = true;
            setAxis(EnumAxis.Y);
        } else {
            allFacesCovered = false;
            setAxis(EnumAxis.valueOf(axis.getName()));
        }
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SpongeBlockTypeLog that = (SpongeBlockTypeLog) o;
        return stripped == that.stripped &&
                allFacesCovered == that.allFacesCovered &&
                woodType == that.woodType;
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), woodType, stripped, allFacesCovered);
    }
}
