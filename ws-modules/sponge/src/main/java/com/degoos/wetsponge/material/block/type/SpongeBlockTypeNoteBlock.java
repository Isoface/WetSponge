package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.EnumInstrument;
import com.degoos.wetsponge.material.block.SpongeBlockTypePowerable;
import com.degoos.wetsponge.util.Validate;

import java.util.Objects;

public class SpongeBlockTypeNoteBlock extends SpongeBlockTypePowerable implements WSBlockTypeNoteBlock {

    private EnumInstrument instrument;
    private int note;

    public SpongeBlockTypeNoteBlock(boolean powered, EnumInstrument instrument, int note) {
        super(25, "minecraft:noteblock", "minecraft:note_block", 64, powered);
        Validate.notNull(instrument, "Instrument cannot be null!");
        this.instrument = instrument;
        this.note = Math.max(0, Math.min(25, note));
    }

    @Override
    public EnumInstrument getInstrument() {
        return instrument;
    }

    @Override
    public void setInstrument(EnumInstrument instrument) {
        Validate.notNull(instrument, "Instrument cannot be null!");
        this.instrument = instrument;
    }

    @Override
    public int getNote() {
        return note;
    }

    @Override
    public void setNote(int note) {
        this.note = Math.max(0, Math.min(25, note));
    }

    @Override
    public SpongeBlockTypeNoteBlock clone() {
        return new SpongeBlockTypeNoteBlock(isPowered(), instrument, note);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SpongeBlockTypeNoteBlock that = (SpongeBlockTypeNoteBlock) o;
        return note == that.note &&
                instrument == that.instrument;
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), instrument, note);
    }
}
