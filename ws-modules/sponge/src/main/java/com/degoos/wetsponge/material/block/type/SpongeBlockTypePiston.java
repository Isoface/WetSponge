package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.material.block.SpongeBlockTypeDirectional;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.value.ValueContainer;
import org.spongepowered.api.item.inventory.ItemStack;

import java.util.Objects;
import java.util.Set;

public class SpongeBlockTypePiston extends SpongeBlockTypeDirectional implements WSBlockTypePiston {

    private boolean extended;

    public SpongeBlockTypePiston(int numericalId, String oldStringId, String newStringId, int maxStackSize, EnumBlockFace facing, Set<EnumBlockFace> faces, boolean extended) {
        super(numericalId, oldStringId, newStringId, maxStackSize, facing, faces);
        this.extended = extended;
    }

    @Override
    public boolean isExtended() {
        return extended;
    }

    @Override
    public void setExtended(boolean extended) {
        this.extended = extended;
    }

    @Override
    public SpongeBlockTypePiston clone() {
        return new SpongeBlockTypePiston(getNumericalId(), getOldStringId(), getNewStringId(), getMaxStackSize(), getFacing(), getFaces(), extended);
    }

    @Override
    public ItemStack writeItemStack(ItemStack itemStack) {
        super.writeItemStack(itemStack);
        itemStack.offer(Keys.EXTENDED, extended);
        return itemStack;
    }

    @Override
    public BlockState writeBlockState(BlockState blockState) {
        blockState = super.writeBlockState(blockState);
        return blockState.with(Keys.EXTENDED, extended).orElse(blockState);
    }

    @Override
    public SpongeBlockTypePiston readContainer(ValueContainer<?> valueContainer) {
        super.readContainer(valueContainer);
        extended = valueContainer.get(Keys.EXTENDED).orElse(false);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SpongeBlockTypePiston that = (SpongeBlockTypePiston) o;
        return extended == that.extended;
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), extended);
    }
}
