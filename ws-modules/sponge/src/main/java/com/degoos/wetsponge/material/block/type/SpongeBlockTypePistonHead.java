package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.enums.block.EnumBlockTypePistonType;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.data.value.ValueContainer;
import org.spongepowered.api.item.inventory.ItemStack;

import java.util.Objects;
import java.util.Set;

public class SpongeBlockTypePistonHead extends SpongeBlockTypeTechnicalPiston implements WSBlockTypePistonHead {

    private boolean shortPiston;

    public SpongeBlockTypePistonHead(EnumBlockFace facing, Set<EnumBlockFace> faces, EnumBlockTypePistonType pistonType, boolean shortPiston) {
        super(34, "minecraft:piston_head", "minecraft:piston_head", 64, facing, faces, pistonType);
        this.shortPiston = shortPiston;
    }

    @Override
    public boolean isShort() {
        return shortPiston;
    }

    @Override
    public void setShort(boolean s) {
        this.shortPiston = s;
    }

    @Override
    public SpongeBlockTypePistonHead clone() {
        return new SpongeBlockTypePistonHead(getFacing(), getFaces(), getType(), shortPiston);
    }

    @Override
    public ItemStack writeItemStack(ItemStack itemStack) {
        super.writeItemStack(itemStack);
        return itemStack;
    }

    @Override
    public BlockState writeBlockState(BlockState blockState) {
        return super.writeBlockState(blockState);
    }

    @Override
    public SpongeBlockTypePistonHead readContainer(ValueContainer<?> valueContainer) {
        super.readContainer(valueContainer);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SpongeBlockTypePistonHead that = (SpongeBlockTypePistonHead) o;
        return shortPiston == that.shortPiston;
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), shortPiston);
    }
}
