package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.material.block.SpongeBlockTypeLightable;

public class SpongeBlockTypeRedstoneOre extends SpongeBlockTypeLightable implements WSBlockTypeRedstoneOre {


    public SpongeBlockTypeRedstoneOre(boolean lit) {
        super(73, "minecraft:redstone_ore", "minecraft:redstone_ore", 64, lit);
    }

    @Override
    public String getOldStringId() {
        return isLit() ? "minecraft:lit_redstone_ore" : "minecraft:redstone_ore";
    }

    @Override
    public SpongeBlockTypeRedstoneOre clone() {
        return new SpongeBlockTypeRedstoneOre(isLit());
    }
}
