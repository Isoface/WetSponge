package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;

import java.util.Set;

public class SpongeBlockTypeRedstoneTorch extends SpongeBlockTypeTorch implements WSBlockTypeRedstoneTorch {

    private boolean lit;

    public SpongeBlockTypeRedstoneTorch(EnumBlockFace facing, Set<EnumBlockFace> faces, boolean lit) {
        super(75, "minecraft:redstone_torch", "minecraft:redstone_torch", 64, "minecraft:redstone_wall_torch", facing, faces);
        this.lit = lit;
    }

    @Override
    public int getNumericalId() {
        return lit ? 76 : 75;
    }

    @Override
    public String getOldStringId() {
        return lit ? "minecraft:redstone_torch" : "minecraft:unlit_redstone_torch";
    }


    @Override
    public boolean isLit() {
        return lit;
    }

    @Override
    public void setLit(boolean lit) {
        this.lit = lit;
    }

    @Override
    public SpongeBlockTypeRedstoneTorch clone() {
        return new SpongeBlockTypeRedstoneTorch(getFacing(), getFaces(), lit);
    }
}
