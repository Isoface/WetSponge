package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockTypeSandType;
import com.degoos.wetsponge.material.block.SpongeBlockType;
import com.degoos.wetsponge.util.Validate;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.SandType;
import org.spongepowered.api.data.value.ValueContainer;
import org.spongepowered.api.item.inventory.ItemStack;

import java.util.Objects;

public class SpongeBlockTypeSand extends SpongeBlockType implements WSBlockTypeSand {

    private EnumBlockTypeSandType sandType;

    public SpongeBlockTypeSand(EnumBlockTypeSandType sandType) {
        super(12, "minecraft:sand", "minecraft:sand", 64);
        Validate.notNull(sandType, "Sand type cannot be null!");
        this.sandType = sandType;
    }

    @Override
    public String getNewStringId() {
        return sandType == EnumBlockTypeSandType.NORMAL ? "minecraft:sand" : "minecraft:red_sand";
    }

    @Override
    public EnumBlockTypeSandType getSandType() {
        return sandType;
    }

    @Override
    public void setSandType(EnumBlockTypeSandType sandType) {
        Validate.notNull(sandType, "Sand type cannot be null!");
        this.sandType = sandType;
    }

    @Override
    public SpongeBlockTypeSand clone() {
        return new SpongeBlockTypeSand(sandType);
    }

    @Override
    public ItemStack writeItemStack(ItemStack itemStack) {
        super.writeItemStack(itemStack);
        itemStack.offer(Keys.SAND_TYPE, Sponge.getRegistry().getType(SandType.class, sandType.name()).orElseThrow(NullPointerException::new));
        return itemStack;
    }

    @Override
    public BlockState writeBlockState(BlockState blockState) {
        blockState = super.writeBlockState(blockState);
        return blockState.with(Keys.SAND_TYPE, Sponge.getRegistry().getType(SandType.class, sandType.name()).orElseThrow(NullPointerException::new)).orElse(blockState);
    }

    @Override
    public SpongeBlockTypeSand readContainer(ValueContainer<?> valueContainer) {
        super.readContainer(valueContainer);
        sandType = EnumBlockTypeSandType.getByName(valueContainer.get(Keys.SAND_TYPE).get().getName()).orElse(EnumBlockTypeSandType.NORMAL);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SpongeBlockTypeSand that = (SpongeBlockTypeSand) o;
        return sandType == that.sandType;
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), sandType);
    }
}
