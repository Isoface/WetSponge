package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockTypeSandType;
import com.degoos.wetsponge.enums.block.EnumBlockTypeSandstoneType;
import com.degoos.wetsponge.material.block.SpongeBlockType;
import com.degoos.wetsponge.util.Validate;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.SandstoneType;
import org.spongepowered.api.data.value.ValueContainer;
import org.spongepowered.api.item.inventory.ItemStack;

import java.util.Objects;

public class SpongeBlockTypeSandstone extends SpongeBlockType implements WSBlockTypeSandstone {

    private EnumBlockTypeSandType sandType;
    private EnumBlockTypeSandstoneType sandstoneType;

    public SpongeBlockTypeSandstone(EnumBlockTypeSandType sandType, EnumBlockTypeSandstoneType sandstoneType) {
        super(24, "minecraft:sandstone", "minecraft:sandstone", 64);
        Validate.notNull(sandType, "Sand type cannot be null!");
        Validate.notNull(sandstoneType, "Sandstone type cannot be null!");
        this.sandType = sandType;
        this.sandstoneType = sandstoneType;
    }

    @Override
    public int getNumericalId() {
        return sandType == EnumBlockTypeSandType.NORMAL ? 24 : 179;
    }

    @Override
    public String getOldStringId() {
        return sandType == EnumBlockTypeSandType.RED ? "minecraft:red_sandstone" : "minecraft:sandstone";
    }

    @Override
    public String getNewStringId() {
        String prefix = sandstoneType == EnumBlockTypeSandstoneType.DEFAULT ? "" : sandstoneType == EnumBlockTypeSandstoneType.SMOOTH ? "cut_" : "chiseled_";
        return prefix + (sandType == EnumBlockTypeSandType.RED ? "red_sandstone" : "sandstone");
    }

    @Override
    public EnumBlockTypeSandType getSandType() {
        return sandType;
    }

    @Override
    public void setSandType(EnumBlockTypeSandType sandType) {
        Validate.notNull(sandType, "Sand type cannot be null!");
        this.sandType = sandType;
    }

    @Override
    public EnumBlockTypeSandstoneType getSandstoneType() {
        return sandstoneType;
    }

    @Override
    public void setSandstoneType(EnumBlockTypeSandstoneType sandstoneType) {
        Validate.notNull(sandstoneType, "Sandstone type cannot be null!");
        this.sandstoneType = sandstoneType;
    }

    @Override
    public SpongeBlockTypeSandstone clone() {
        return new SpongeBlockTypeSandstone(sandType, sandstoneType);
    }

    @Override
    public ItemStack writeItemStack(ItemStack itemStack) {
        super.writeItemStack(itemStack);
        itemStack.offer(Keys.SANDSTONE_TYPE, Sponge.getRegistry().getType(SandstoneType.class, sandstoneType.name()).orElseThrow(NullPointerException::new));
        return itemStack;
    }

    @Override
    public BlockState writeBlockState(BlockState blockState) {
        blockState = super.writeBlockState(blockState);
        return blockState.with(Keys.SANDSTONE_TYPE,
                Sponge.getRegistry().getType(SandstoneType.class, sandstoneType.name()).orElseThrow(NullPointerException::new)).orElse(blockState);
    }

    @Override
    public SpongeBlockTypeSandstone readContainer(ValueContainer<?> valueContainer) {
        super.readContainer(valueContainer);
        sandstoneType = EnumBlockTypeSandstoneType.getByName(valueContainer.get(Keys.SANDSTONE_TYPE).get().getName()).orElse(EnumBlockTypeSandstoneType.DEFAULT);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SpongeBlockTypeSandstone that = (SpongeBlockTypeSandstone) o;
        return sandType == that.sandType &&
                sandstoneType == that.sandstoneType;
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), sandType, sandstoneType);
    }
}
