package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumWoodType;
import com.degoos.wetsponge.material.block.SpongeBlockType;
import com.degoos.wetsponge.util.Validate;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.TreeType;
import org.spongepowered.api.data.value.ValueContainer;
import org.spongepowered.api.item.inventory.ItemStack;

import java.util.Objects;

public class SpongeBlockTypeSapling extends SpongeBlockType implements WSBlockTypeSapling {

    private EnumWoodType woodType;
    private int stage, maximumStage;

    public SpongeBlockTypeSapling(EnumWoodType woodType, int stage, int maximumStage) {
        super(6, "minecraft:sapling", "minecraft:sapling", 64);
        Validate.notNull(woodType, "Wood type cannot be null!");
        this.woodType = woodType;
        this.stage = stage;
        this.maximumStage = maximumStage;
    }

    @Override
    public String getNewStringId() {
        switch (getWoodType()) {
            case SPRUCE:
                return "minecraft:spruce_sapling";
            case BIRCH:
                return "minecraft:birch_sapling";
            case JUNGLE:
                return "minecraft:jungle_sapling";
            case ACACIA:
                return "minecraft:acacia_sapling";
            case DARK_OAK:
                return "minecraft:dark_oak_sapling";
            case OAK:
            default:
                return "minecraft:oak_sapling";
        }
    }

    @Override
    public EnumWoodType getWoodType() {
        return woodType;
    }

    @Override
    public void setWoodType(EnumWoodType woodType) {
        Validate.notNull(woodType, "Wood type cannot be null!");
        this.woodType = woodType;
    }

    @Override
    public int getStage() {
        return stage;
    }

    @Override
    public void setStage(int stage) {
        this.stage = Math.min(maximumStage, Math.max(0, stage));
    }

    @Override
    public int getMaximumStage() {
        return maximumStage;
    }

    @Override
    public SpongeBlockTypeSapling clone() {
        return new SpongeBlockTypeSapling(woodType, stage, maximumStage);
    }

    @Override
    public ItemStack writeItemStack(ItemStack itemStack) {
        super.writeItemStack(itemStack);
        itemStack.offer(Keys.GROWTH_STAGE, stage);
        itemStack.offer(Keys.TREE_TYPE, Sponge.getRegistry().getType(TreeType.class, woodType.name()).orElseThrow(NullPointerException::new));
        return itemStack;
    }

    @Override
    public BlockState writeBlockState(BlockState blockState) {
        blockState = super.writeBlockState(blockState);
        blockState = blockState.with(Keys.GROWTH_STAGE, stage).orElse(blockState);
        return blockState.with(Keys.TREE_TYPE, Sponge.getRegistry().getType(TreeType.class, woodType.name()).orElseThrow(NullPointerException::new)).orElse(blockState);
    }

    @Override
    public SpongeBlockTypeSapling readContainer(ValueContainer<?> valueContainer) {
        super.readContainer(valueContainer);
        stage = valueContainer.get(Keys.GROWTH_STAGE).orElse(0);
        woodType = EnumWoodType.getByName(valueContainer.get(Keys.TREE_TYPE).get().getName()).orElseThrow(NullPointerException::new);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SpongeBlockTypeSapling that = (SpongeBlockTypeSapling) o;
        return stage == that.stage &&
                maximumStage == that.maximumStage &&
                woodType == that.woodType;
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), woodType, stage, maximumStage);
    }
}
