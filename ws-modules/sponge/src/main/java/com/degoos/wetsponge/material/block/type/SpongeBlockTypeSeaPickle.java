package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.material.block.SpongeBlockTypeWaterlogged;

import java.util.Objects;

public class SpongeBlockTypeSeaPickle extends SpongeBlockTypeWaterlogged implements WSBlockTypeSeaPickle {

    private int pickles, minimumPickles, maximumPickles;

    public SpongeBlockTypeSeaPickle(boolean waterLogged, int pickles, int minimumPickles, int maximumPickles) {
        super(-1, null, "minecraft:sea_pickle", 64, waterLogged);
        this.pickles = pickles;
        this.minimumPickles = minimumPickles;
        this.maximumPickles = maximumPickles;
    }

    @Override
    public int getPickles() {
        return pickles;
    }

    @Override
    public void setPickles(int pickles) {
        this.pickles = Math.min(maximumPickles, Math.max(minimumPickles, pickles));
    }

    @Override
    public int getMinimumPickles() {
        return minimumPickles;
    }

    @Override
    public int getMaximumPickles() {
        return maximumPickles;
    }

    @Override
    public SpongeBlockTypeSeaPickle clone() {
        return new SpongeBlockTypeSeaPickle(isWaterlogged(), pickles, minimumPickles, maximumPickles);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SpongeBlockTypeSeaPickle that = (SpongeBlockTypeSeaPickle) o;
        return pickles == that.pickles &&
                minimumPickles == that.minimumPickles &&
                maximumPickles == that.maximumPickles;
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), pickles, minimumPickles, maximumPickles);
    }
}
