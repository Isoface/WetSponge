package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.material.block.SpongeBlockTypeRotatable;

import java.util.Objects;

public class SpongeBlockTypeSign extends SpongeBlockTypeRotatable implements WSBlockTypeSign {

    private boolean waterlogged;

    public SpongeBlockTypeSign(EnumBlockFace rotation, boolean waterlogged) {
        super(63, "minecraft:standing_sign", "minecraft:sign", 64, rotation);
        this.waterlogged = waterlogged;
    }

    @Override
    public boolean isWaterlogged() {
        return waterlogged;
    }

    @Override
    public void setWaterlogged(boolean waterlogged) {
        this.waterlogged = waterlogged;
    }

    @Override
    public SpongeBlockTypeSign clone() {
        return new SpongeBlockTypeSign(getRotation(), waterlogged);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SpongeBlockTypeSign that = (SpongeBlockTypeSign) o;
        return waterlogged == that.waterlogged;
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), waterlogged);
    }
}
