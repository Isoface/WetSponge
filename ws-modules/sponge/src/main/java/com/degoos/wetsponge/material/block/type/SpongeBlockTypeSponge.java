package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.material.block.SpongeBlockType;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.value.ValueContainer;
import org.spongepowered.api.item.inventory.ItemStack;

import java.util.Objects;

public class SpongeBlockTypeSponge extends SpongeBlockType implements WSBlockTypeSponge {

    private boolean wet;

    public SpongeBlockTypeSponge(boolean wet) {
        super(19, "minecraft:sponge", "minecraft:sponge", 64);
        this.wet = wet;
    }

    @Override
    public String getNewStringId() {
        return wet ? "minecraft:wet_sponge" : "minecraft:sponge";
    }

    @Override
    public boolean isWet() {
        return wet;
    }

    @Override
    public void setWet(boolean wet) {
        this.wet = wet;
    }

    @Override
    public SpongeBlockTypeSponge clone() {
        return new SpongeBlockTypeSponge(wet);
    }

    @Override
    public ItemStack writeItemStack(ItemStack itemStack) {
        super.writeItemStack(itemStack);
        itemStack.offer(Keys.IS_WET, wet);
        return itemStack;
    }

    @Override
    public BlockState writeBlockState(BlockState blockState) {
        blockState = super.writeBlockState(blockState);
        return blockState.with(Keys.IS_WET, wet).orElse(blockState);
    }

    @Override
    public SpongeBlockTypeSponge readContainer(ValueContainer<?> valueContainer) {
        super.readContainer(valueContainer);
        wet = valueContainer.get(Keys.IS_WET).orElse(false);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SpongeBlockTypeSponge that = (SpongeBlockTypeSponge) o;
        return wet == that.wet;
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), wet);
    }
}
