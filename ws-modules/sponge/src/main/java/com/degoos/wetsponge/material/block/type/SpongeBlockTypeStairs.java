package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.enums.block.EnumBlockTypeBisectedHalf;
import com.degoos.wetsponge.enums.block.EnumBlockTypeStairShape;
import com.degoos.wetsponge.material.block.SpongeBlockTypeDirectional;
import com.degoos.wetsponge.util.Validate;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.PortionTypes;
import org.spongepowered.api.data.type.StairShape;
import org.spongepowered.api.data.type.StairShapes;
import org.spongepowered.api.data.value.ValueContainer;
import org.spongepowered.api.item.inventory.ItemStack;

import java.util.Objects;
import java.util.Set;

public class SpongeBlockTypeStairs extends SpongeBlockTypeDirectional implements WSBlockTypeStairs {


    private EnumBlockTypeStairShape shape;
    private EnumBlockTypeBisectedHalf half;
    private boolean waterlogged;

    public SpongeBlockTypeStairs(int numericalId, String oldStringId, String newStringId, int maxStackSize, EnumBlockFace facing, Set<EnumBlockFace> faces,
                                 EnumBlockTypeStairShape shape, EnumBlockTypeBisectedHalf half, boolean waterlogged) {
        super(numericalId, oldStringId, newStringId, maxStackSize, facing, faces);
        Validate.notNull(shape, "Shape cannot be null!");
        Validate.notNull(half, "Half cannot be null!");
        this.shape = shape;
        this.half = half;
        this.waterlogged = waterlogged;
    }

    @Override
    public EnumBlockTypeStairShape getShape() {
        return shape;
    }

    @Override
    public void setShape(EnumBlockTypeStairShape shape) {
        Validate.notNull(shape, "Shape cannot be null!");
        this.shape = shape;
    }

    @Override
    public EnumBlockTypeBisectedHalf getHalf() {
        return half;
    }

    @Override
    public void setHalf(EnumBlockTypeBisectedHalf half) {
        Validate.notNull(half, "Half cannot be null!");
        this.half = half;
    }

    @Override
    public boolean isWaterlogged() {
        return waterlogged;
    }

    @Override
    public void setWaterlogged(boolean waterlogged) {
        this.waterlogged = waterlogged;
    }

    @Override
    public SpongeBlockTypeStairs clone() {
        return new SpongeBlockTypeStairs(getNumericalId(), getOldStringId(), getNewStringId(), getMaxStackSize(), getFacing(), getFaces(), shape, half, waterlogged);
    }

    @Override
    public ItemStack writeItemStack(ItemStack itemStack) {
        super.writeItemStack(itemStack);
        itemStack.offer(Keys.PORTION_TYPE, half == EnumBlockTypeBisectedHalf.BOTTOM ? PortionTypes.BOTTOM : PortionTypes.TOP);
        itemStack.offer(Keys.STAIR_SHAPE, Sponge.getRegistry().getType(StairShape.class, shape.name()).orElseThrow(NullPointerException::new));
        return itemStack;
    }

    @Override
    public BlockState writeBlockState(BlockState blockState) {
        blockState = super.writeBlockState(blockState);
        blockState = blockState.with(Keys.STAIR_SHAPE, Sponge.getRegistry().getType(StairShape.class, shape.name()).orElseThrow(NullPointerException::new)).orElse(blockState);
        return blockState.with(Keys.PORTION_TYPE, half == EnumBlockTypeBisectedHalf.BOTTOM ? PortionTypes.BOTTOM : PortionTypes.TOP).orElse(blockState);
    }

    @Override
    public SpongeBlockTypeStairs readContainer(ValueContainer<?> valueContainer) {
        super.readContainer(valueContainer);
        shape = EnumBlockTypeStairShape.valueOf(valueContainer.get(Keys.STAIR_SHAPE).orElse(StairShapes.STRAIGHT).getName());
        half = valueContainer.get(Keys.PORTION_TYPE).orElse(PortionTypes.BOTTOM).getId().equals(PortionTypes.TOP.getId())
                ? EnumBlockTypeBisectedHalf.TOP : EnumBlockTypeBisectedHalf.BOTTOM;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SpongeBlockTypeStairs that = (SpongeBlockTypeStairs) o;
        return waterlogged == that.waterlogged &&
                shape == that.shape &&
                half == that.half;
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), shape, half, waterlogged);
    }
}
