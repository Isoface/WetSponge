package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockTypeStoneType;
import com.degoos.wetsponge.material.block.SpongeBlockType;
import com.degoos.wetsponge.util.Validate;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.StoneType;
import org.spongepowered.api.data.value.ValueContainer;
import org.spongepowered.api.item.inventory.ItemStack;

import java.util.Objects;

public class SpongeBlockTypeStone extends SpongeBlockType implements WSBlockTypeStone {

    private EnumBlockTypeStoneType stoneType;

    public SpongeBlockTypeStone(EnumBlockTypeStoneType stoneType) {
        super(1, "minecraft:stone", "minecraft:stone", 64);
        Validate.notNull(stoneType, "Stone type cannot be null!");
        this.stoneType = stoneType;
    }

    @Override
    public String getNewStringId() {
        switch (stoneType) {
            case DIORITE:
                return "minecraft:diorite";
            case GRANITE:
                return "minecraft:granite";
            case ANDESITE:
                return "minecraft:andesite";
            case SMOOTH_DIORITE:
                return "minecraft:polished_diorite";
            case SMOOTH_GRANITE:
                return "minecraft:polished_granite";
            case SMOOTH_ANDESITE:
                return "minecraft:polished_andesite";
            case STONE:
            default:
                return "minecraft:stone";
        }
    }

    @Override
    public EnumBlockTypeStoneType getStoneType() {
        return stoneType;
    }

    @Override
    public void setStoneType(EnumBlockTypeStoneType stoneType) {
        Validate.notNull(stoneType, "Stone type cannot be null!");
        this.stoneType = stoneType;
    }

    @Override
    public SpongeBlockTypeStone clone() {
        return new SpongeBlockTypeStone(stoneType);
    }

    @Override
    public ItemStack writeItemStack(ItemStack itemStack) {
        super.writeItemStack(itemStack);
        itemStack.offer(Keys.STONE_TYPE, Sponge.getRegistry().getType(StoneType.class, stoneType.name()).orElseThrow(NullPointerException::new));
        return itemStack;
    }

    @Override
    public BlockState writeBlockState(BlockState blockState) {
        blockState = super.writeBlockState(blockState);
        return blockState.with(Keys.STONE_TYPE, Sponge.getRegistry().getType(StoneType.class, stoneType.name()).orElseThrow(NullPointerException::new)).orElse(blockState);
    }

    @Override
    public SpongeBlockTypeStone readContainer(ValueContainer<?> valueContainer) {
        super.readContainer(valueContainer);
        stoneType = EnumBlockTypeStoneType.getByName(valueContainer.get(Keys.STONE_TYPE).get().getName()).orElseThrow(NullPointerException::new);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SpongeBlockTypeStone that = (SpongeBlockTypeStone) o;
        return stoneType == that.stoneType;
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), stoneType);
    }
}
