package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.enums.block.EnumBlockTypeBisectedHalf;
import com.degoos.wetsponge.material.block.SpongeBlockTypeDirectional;
import com.degoos.wetsponge.util.Validate;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.PortionTypes;
import org.spongepowered.api.data.value.ValueContainer;
import org.spongepowered.api.item.inventory.ItemStack;

import java.util.Objects;
import java.util.Set;

public class SpongeBlockTypeTrapDoor extends SpongeBlockTypeDirectional implements WSBlockTypeTrapDoor {

    private EnumBlockTypeBisectedHalf half;
    private boolean open, powered, waterlogged;

    public SpongeBlockTypeTrapDoor(int numericalId, String oldStringId, String newStringId, int maxStackSize, EnumBlockFace facing,
                                   Set<EnumBlockFace> faces, EnumBlockTypeBisectedHalf half, boolean open, boolean powered, boolean waterlogged) {
        super(numericalId, oldStringId, newStringId, maxStackSize, facing, faces);
        Validate.notNull(half, "Half cannot be null!");
        this.half = half;
        this.open = open;
        this.powered = powered;
        this.waterlogged = waterlogged;
    }

    @Override
    public EnumBlockTypeBisectedHalf getHalf() {
        return half;
    }

    @Override
    public void setHalf(EnumBlockTypeBisectedHalf half) {
        Validate.notNull(half, "Half cannot be null!");
        this.half = half;
    }

    @Override
    public boolean isOpen() {
        return open;
    }

    @Override
    public void setOpen(boolean open) {
        this.open = open;
    }

    @Override
    public boolean isPowered() {
        return powered;
    }

    @Override
    public void setPowered(boolean powered) {
        this.powered = powered;
    }

    @Override
    public boolean isWaterlogged() {
        return waterlogged;
    }

    @Override
    public void setWaterlogged(boolean waterlogged) {
        this.waterlogged = waterlogged;
    }

    @Override
    public SpongeBlockTypeTrapDoor clone() {
        return new SpongeBlockTypeTrapDoor(getNumericalId(), getOldStringId(), getNewStringId(),
                getMaxStackSize(), getFacing(), getFaces(), half, open, powered, waterlogged);
    }

    @Override
    public ItemStack writeItemStack(ItemStack itemStack) {
        super.writeItemStack(itemStack);
        itemStack.offer(Keys.OPEN, open);
        itemStack.offer(Keys.PORTION_TYPE, half == EnumBlockTypeBisectedHalf.TOP ? PortionTypes.TOP : PortionTypes.BOTTOM);
        return itemStack;
    }

    @Override
    public BlockState writeBlockState(BlockState blockState) {
        blockState = super.writeBlockState(blockState);
        blockState = blockState.with(Keys.OPEN, open).orElse(blockState);
        return blockState.with(Keys.PORTION_TYPE, half == EnumBlockTypeBisectedHalf.TOP ? PortionTypes.TOP : PortionTypes.BOTTOM).orElse(blockState);
    }

    @Override
    public SpongeBlockTypeTrapDoor readContainer(ValueContainer<?> valueContainer) {
        super.readContainer(valueContainer);
        open = valueContainer.get(Keys.OPEN).orElse(false);
        half = valueContainer.get(Keys.PORTION_TYPE).orElse(PortionTypes.BOTTOM).equals(PortionTypes.BOTTOM) ? EnumBlockTypeBisectedHalf.BOTTOM : EnumBlockTypeBisectedHalf.TOP;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SpongeBlockTypeTrapDoor that = (SpongeBlockTypeTrapDoor) o;
        return open == that.open &&
                powered == that.powered &&
                waterlogged == that.waterlogged &&
                half == that.half;
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), half, open, powered, waterlogged);
    }
}
