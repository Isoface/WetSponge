package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.material.block.SpongeBlockTypeMultipleFacing;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.value.ValueContainer;
import org.spongepowered.api.item.inventory.ItemStack;

import java.util.Objects;
import java.util.Set;

public class SpongeBlockTypeTripwire extends SpongeBlockTypeMultipleFacing implements WSBlockTypeTripwire {

    private boolean disarmed, attached, powered;

    public SpongeBlockTypeTripwire(Set<EnumBlockFace> faces, Set<EnumBlockFace> allowedFaces, boolean disarmed, boolean attached, boolean powered) {
        super(132, "minecraft:tripwire", "minecraft:tripwire", 64, faces, allowedFaces);
        this.disarmed = disarmed;
        this.attached = attached;
        this.powered = powered;
    }

    @Override
    public boolean isDisarmed() {
        return disarmed;
    }

    @Override
    public void setDisarmed(boolean disarmed) {
        this.disarmed = disarmed;
    }

    @Override
    public boolean isAttached() {
        return attached;
    }

    @Override
    public void setAttached(boolean attached) {
        this.attached = attached;
    }

    @Override
    public boolean isPowered() {
        return powered;
    }

    @Override
    public void setPowered(boolean powered) {
        this.powered = powered;
    }

    @Override
    public SpongeBlockTypeTripwire clone() {
        return new SpongeBlockTypeTripwire(getFaces(), getAllowedFaces(), disarmed, attached, powered);
    }

    @Override
    public ItemStack writeItemStack(ItemStack itemStack) {
        super.writeItemStack(itemStack);
        itemStack.offer(Keys.ATTACHED, attached);
        itemStack.offer(Keys.POWERED, powered);
        itemStack.offer(Keys.DISARMED, disarmed);
        return itemStack;
    }

    @Override
    public BlockState writeBlockState(BlockState blockState) {
        blockState = super.writeBlockState(blockState);
        blockState = blockState.with(Keys.ATTACHED, attached).orElse(blockState);
        blockState = blockState.with(Keys.POWERED, powered).orElse(blockState);
        return blockState.with(Keys.DISARMED, disarmed).orElse(blockState);
    }

    @Override
    public SpongeBlockTypeTripwire readContainer(ValueContainer<?> valueContainer) {
        super.readContainer(valueContainer);
        attached = valueContainer.get(Keys.ATTACHED).orElse(false);
        powered = valueContainer.get(Keys.POWERED).orElse(false);
        disarmed = valueContainer.get(Keys.DISARMED).orElse(false);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SpongeBlockTypeTripwire that = (SpongeBlockTypeTripwire) o;
        return disarmed == that.disarmed &&
                attached == that.attached &&
                powered == that.powered;
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), disarmed, attached, powered);
    }
}
