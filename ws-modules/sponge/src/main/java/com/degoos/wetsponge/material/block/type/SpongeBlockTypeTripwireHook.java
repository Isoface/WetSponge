package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.material.block.SpongeBlockTypeDirectional;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.value.ValueContainer;
import org.spongepowered.api.item.inventory.ItemStack;

import java.util.Objects;
import java.util.Set;

public class SpongeBlockTypeTripwireHook extends SpongeBlockTypeDirectional implements WSBlockTypeTripwireHook {

    private boolean attached, powered;

    public SpongeBlockTypeTripwireHook(EnumBlockFace facing, Set<EnumBlockFace> faces, boolean attached, boolean powered) {
        super(131, "minecraft:tripwire_hook", "minecraft:tripwire_hook", 64, facing, faces);
        this.attached = attached;
        this.powered = powered;
    }

    @Override
    public boolean isAttached() {
        return attached;
    }

    @Override
    public void setAttached(boolean attached) {
        this.attached = attached;
    }

    @Override
    public boolean isPowered() {
        return powered;
    }

    @Override

    public void setPowered(boolean powered) {
        this.powered = powered;
    }

    @Override
    public SpongeBlockTypeTripwireHook clone() {
        return new SpongeBlockTypeTripwireHook(getFacing(), getFaces(), attached, powered);
    }

    @Override
    public ItemStack writeItemStack(ItemStack itemStack) {
        super.writeItemStack(itemStack);
        itemStack.offer(Keys.ATTACHED, attached);
        itemStack.offer(Keys.POWERED, powered);
        return itemStack;
    }

    @Override
    public BlockState writeBlockState(BlockState blockState) {
        blockState = super.writeBlockState(blockState);
        blockState = blockState.with(Keys.ATTACHED, attached).orElse(blockState);
        return blockState.with(Keys.POWERED, powered).orElse(blockState);
    }

    @Override
    public SpongeBlockTypeTripwireHook readContainer(ValueContainer<?> valueContainer) {
        super.readContainer(valueContainer);
        attached = valueContainer.get(Keys.ATTACHED).orElse(false);
        powered = valueContainer.get(Keys.POWERED).orElse(false);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SpongeBlockTypeTripwireHook that = (SpongeBlockTypeTripwireHook) o;
        return attached == that.attached &&
                powered == that.powered;
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), attached, powered);
    }
}
