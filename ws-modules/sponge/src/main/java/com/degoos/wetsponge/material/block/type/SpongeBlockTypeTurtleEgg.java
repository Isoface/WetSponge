package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.material.block.SpongeBlockType;

import java.util.Objects;

public class SpongeBlockTypeTurtleEgg extends SpongeBlockType implements WSBlockTypeTurtleEgg {

    private int eggs, minimumEggs, maximumEggs, hatch, maximumHatch;

    public SpongeBlockTypeTurtleEgg(int eggs, int minimumEggs, int maximumEggs, int hatch, int maximumHatch) {
        super(-1, null, "minecraft:turtle_egg", 64);
        this.eggs = eggs;
        this.minimumEggs = minimumEggs;
        this.maximumEggs = maximumEggs;
        this.hatch = hatch;
        this.maximumHatch = maximumHatch;
    }

    @Override
    public int getEggs() {
        return eggs;
    }

    @Override
    public void setEggs(int eggs) {
        this.eggs = Math.max(minimumEggs, Math.min(maximumEggs, eggs));
    }

    @Override
    public int getMinimumEggs() {
        return minimumEggs;
    }

    @Override
    public int getMaximumEggs() {
        return maximumEggs;
    }

    @Override
    public int getHatch() {
        return hatch;
    }

    @Override
    public void setHatch(int hatch) {
        this.hatch = Math.max(0, Math.min(maximumHatch, hatch));
    }

    @Override
    public int getMaximumHatch() {
        return maximumHatch;
    }

    @Override
    public SpongeBlockTypeTurtleEgg clone() {
        return new SpongeBlockTypeTurtleEgg(eggs, minimumEggs, maximumEggs, hatch, maximumHatch);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SpongeBlockTypeTurtleEgg that = (SpongeBlockTypeTurtleEgg) o;
        return eggs == that.eggs &&
                minimumEggs == that.minimumEggs &&
                maximumEggs == that.maximumEggs &&
                hatch == that.hatch &&
                maximumHatch == that.maximumHatch;
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), eggs, minimumEggs, maximumEggs, hatch, maximumHatch);
    }
}
