package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumBlockFace;
import com.degoos.wetsponge.material.block.SpongeBlockTypeDirectional;

import java.util.Objects;
import java.util.Set;

public class SpongeBlockTypeWallSign extends SpongeBlockTypeDirectional implements WSBlockTypeWallSign {

    private boolean waterlogged;

    public SpongeBlockTypeWallSign(EnumBlockFace facing, Set<EnumBlockFace> faces, boolean waterlogged) {
        super(68, "minecraft:wall_sign", "minecraft:wall_sign", 64, facing, faces);
        this.waterlogged = waterlogged;
    }

    @Override
    public boolean isWaterlogged() {
        return waterlogged;
    }

    @Override
    public void setWaterlogged(boolean waterlogged) {
        this.waterlogged = waterlogged;
    }

    @Override
    public SpongeBlockTypeWallSign clone() {
        return new SpongeBlockTypeWallSign(getFacing(), getFaces(), waterlogged);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SpongeBlockTypeWallSign that = (SpongeBlockTypeWallSign) o;
        return waterlogged == that.waterlogged;
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), waterlogged);
    }
}
