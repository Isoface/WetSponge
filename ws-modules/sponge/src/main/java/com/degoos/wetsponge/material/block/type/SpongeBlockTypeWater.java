package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.material.block.SpongeBlockTypeLevelled;

public class SpongeBlockTypeWater extends SpongeBlockTypeLevelled implements WSBlockTypeWater {

    public SpongeBlockTypeWater(int level, int maximumLevel) {
        super(9, "minecraft:water", "minecraft:water", 64, level, maximumLevel);
    }

    @Override
    public int getNumericalId() {
        return getLevel() == 0 ? 9 : 8;
    }

    @Override
    public String getOldStringId() {
        return getLevel() == 0 ? "minecraft:water" : "minecraft:flowing_water";
    }

    @Override
    public SpongeBlockTypeWater clone() {
        return new SpongeBlockTypeWater(getLevel(), getMaximumLevel());
    }
}
