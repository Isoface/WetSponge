package com.degoos.wetsponge.material.block.type;

import com.degoos.wetsponge.enums.block.EnumWoodType;
import com.degoos.wetsponge.material.block.SpongeBlockType;
import com.degoos.wetsponge.util.Validate;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.TreeType;
import org.spongepowered.api.data.value.ValueContainer;
import org.spongepowered.api.item.inventory.ItemStack;

import java.util.Objects;

public class SpongeBlockTypeWoodPlanks extends SpongeBlockType implements WSBlockTypeWoodPlanks {

    private EnumWoodType woodType;

    public SpongeBlockTypeWoodPlanks(EnumWoodType woodType) {
        super(5, "minecraft:planks", "minecraft:planks", 64);
        Validate.notNull(woodType, "Wood type cannot be null!");
        this.woodType = woodType;
    }

    @Override
    public String getNewStringId() {
        switch (getWoodType()) {
            case SPRUCE:
                return "minecraft:spruce_planks";
            case BIRCH:
                return "minecraft:birch_planks";
            case JUNGLE:
                return "minecraft:jungle_planks";
            case ACACIA:
                return "minecraft:acacia_planks";
            case DARK_OAK:
                return "minecraft:dark_oak_planks";
            case OAK:
            default:
                return "minecraft:oak_planks";
        }
    }

    @Override
    public EnumWoodType getWoodType() {
        return woodType;
    }

    @Override
    public void setWoodType(EnumWoodType woodType) {
        Validate.notNull(woodType, "Wood type cannot be null");
        this.woodType = woodType;
    }

    @Override
    public SpongeBlockTypeWoodPlanks clone() {
        return new SpongeBlockTypeWoodPlanks(woodType);
    }

    @Override
    public ItemStack writeItemStack(ItemStack itemStack) {
        super.writeItemStack(itemStack);
        itemStack.offer(Keys.TREE_TYPE, Sponge.getRegistry().getType(TreeType.class, woodType.name()).orElseThrow(NullPointerException::new));
        return itemStack;
    }

    @Override
    public BlockState writeBlockState(BlockState blockState) {
        blockState = super.writeBlockState(blockState);
        return blockState.with(Keys.TREE_TYPE, Sponge.getRegistry().getType(TreeType.class, woodType.name()).orElseThrow(NullPointerException::new)).orElse(blockState);
    }

    @Override
    public SpongeBlockTypeWoodPlanks readContainer(ValueContainer<?> valueContainer) {
        super.readContainer(valueContainer);
        woodType = EnumWoodType.getByName(valueContainer.get(Keys.TREE_TYPE).get().getName()).orElseThrow(NullPointerException::new);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SpongeBlockTypeWoodPlanks that = (SpongeBlockTypeWoodPlanks) o;
        return woodType == that.woodType;
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), woodType);
    }
}
