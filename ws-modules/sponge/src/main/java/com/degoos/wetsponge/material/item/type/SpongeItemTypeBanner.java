package com.degoos.wetsponge.material.item.type;

import com.degoos.wetsponge.block.tileentity.extra.WSBannerPattern;
import com.degoos.wetsponge.enums.EnumDyeColor;
import com.degoos.wetsponge.enums.block.EnumBannerPatternShape;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import org.spongepowered.api.data.value.ValueContainer;
import org.spongepowered.api.item.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

public class SpongeItemTypeBanner extends SpongeItemTypeDyeColored implements WSItemTypeBanner {

    private List<WSBannerPattern> patterns;

    public SpongeItemTypeBanner(EnumDyeColor dyeColor, List<WSBannerPattern> patterns) {
        super(425, "minecraft:banner", "banner", 64, dyeColor);
        this.patterns = patterns == null ? new ArrayList<>() : patterns.stream().map(WSBannerPattern::clone).collect(Collectors.toList());
    }

    @Override
    public List<WSBannerPattern> getPatterns() {
        return patterns;
    }

    @Override
    public void setPatterns(List<WSBannerPattern> patterns) {
        this.patterns = patterns == null ? new ArrayList<>() : patterns;
    }

    @Override
    public SpongeItemTypeBanner clone() {
        return new SpongeItemTypeBanner(getDyeColor(), patterns);
    }

    @Override
    public ItemStack writeItemStack(ItemStack itemStack) {
        super.writeItemStack(itemStack);

        NBTTagCompound compound = ((net.minecraft.item.ItemStack) (Object) itemStack).getTagCompound();
        if (compound == null) compound = new NBTTagCompound();
        NBTTagCompound banner = new NBTTagCompound();
        banner.setInteger("Base", getDyeColor().getDyeData());

        NBTTagList patterns = new NBTTagList();

        this.patterns.forEach(pattern -> {
            NBTTagCompound tag = new NBTTagCompound();
            tag.setInteger("Color", pattern.getColor().getDyeData());
            tag.setString("Pattern", pattern.getShape().getCode());
            patterns.appendTag(tag);
        });

        banner.setTag("Patterns", patterns);

        compound.setTag("BlockEntityTag", banner);

        ((net.minecraft.item.ItemStack) (Object) itemStack).setTagCompound(compound);

        return itemStack;
    }

    @Override
    public SpongeItemTypeBanner readContainer(ValueContainer<?> valueContainer) {
        super.readContainer(valueContainer);
        this.patterns.clear();
        NBTTagCompound compound = ((net.minecraft.item.ItemStack) (Object) valueContainer).getTagCompound();
        if (compound == null) return this;
        NBTBase base = compound.getTag("BlockEntityTag");
        if (!(base instanceof NBTTagCompound)) return this;
        NBTBase patterns = ((NBTTagCompound) base).getTag("Patterns");
        if (!(patterns instanceof NBTTagList)) return this;
        for (int i = 0; i < ((NBTTagList) patterns).tagCount(); i++) {
            NBTBase pattern = ((NBTTagList) patterns).get(i);
            if (!(pattern instanceof NBTTagCompound)) return this;
            Optional<EnumBannerPatternShape> optional = EnumBannerPatternShape.getByCode(((NBTTagCompound) pattern).getString("Pattern"));
            if (!optional.isPresent()) continue;
            this.patterns.add(new WSBannerPattern(optional.get(), EnumDyeColor.getByDyeData((byte) ((NBTTagCompound) pattern).getInteger("Color"))
                    .orElse(EnumDyeColor.BLACK)));
        }
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SpongeItemTypeBanner that = (SpongeItemTypeBanner) o;
        return Objects.equals(patterns, that.patterns);
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), patterns);
    }
}
