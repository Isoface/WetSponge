package com.degoos.wetsponge.material.item.type;

import com.degoos.wetsponge.enums.item.EnumItemTypeCoalType;
import com.degoos.wetsponge.material.item.SpongeItemType;
import com.degoos.wetsponge.util.Validate;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.CoalType;
import org.spongepowered.api.data.type.CoalTypes;
import org.spongepowered.api.data.value.ValueContainer;
import org.spongepowered.api.item.inventory.ItemStack;

import java.util.Objects;

public class SpongeItemTypeCoal extends SpongeItemType implements WSItemTypeCoal {

    private EnumItemTypeCoalType coalType;

    public SpongeItemTypeCoal(EnumItemTypeCoalType coalType) {
        super(263, "minecraft:coal", "minecraft:coal", 64);
        Validate.notNull(coalType, "Coal type cannot be null!");
        this.coalType = coalType;
    }

    @Override
    public String getNewStringId() {
        return coalType == EnumItemTypeCoalType.COAL ? "minecraft:coal" : "minecraft:charcoal";
    }

    @Override
    public EnumItemTypeCoalType getCoalType() {
        return coalType;
    }

    @Override
    public void setCoalType(EnumItemTypeCoalType coalType) {
        Validate.notNull(coalType, "Coal type cannot be null!");
        this.coalType = coalType;
    }

    @Override
    public SpongeItemTypeCoal clone() {
        return new SpongeItemTypeCoal(coalType);
    }

    @Override
    public ItemStack writeItemStack(ItemStack itemStack) {
        super.writeItemStack(itemStack);
        itemStack.offer(Keys.COAL_TYPE, Sponge.getRegistry().getType(CoalType.class, coalType.name()).orElse(CoalTypes.COAL));
        return itemStack;
    }

    @Override
    public SpongeItemTypeCoal readContainer(ValueContainer<?> valueContainer) {
        super.readContainer(valueContainer);
        coalType = EnumItemTypeCoalType.getByName(valueContainer.get(Keys.COAL_TYPE).orElse(CoalTypes.COAL).getName()).orElse(EnumItemTypeCoalType.COAL);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SpongeItemTypeCoal that = (SpongeItemTypeCoal) o;
        return coalType == that.coalType;
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), coalType);
    }
}
