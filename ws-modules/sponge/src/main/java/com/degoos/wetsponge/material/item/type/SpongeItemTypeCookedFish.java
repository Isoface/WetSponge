package com.degoos.wetsponge.material.item.type;

import com.degoos.wetsponge.enums.item.EnumItemTypeCookedFishType;
import com.degoos.wetsponge.material.item.SpongeItemType;
import com.degoos.wetsponge.util.Validate;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.CookedFish;
import org.spongepowered.api.data.type.CookedFishes;
import org.spongepowered.api.data.value.ValueContainer;
import org.spongepowered.api.item.inventory.ItemStack;

import java.util.Objects;

public class SpongeItemTypeCookedFish extends SpongeItemType implements WSItemTypeCookedFish {

    private EnumItemTypeCookedFishType cookedFishType;

    public SpongeItemTypeCookedFish(EnumItemTypeCookedFishType cookedFishType) {
        super(263, "minecraft:cooked_fish", "minecraft:cooked_cod", 64);
        Validate.notNull(cookedFishType, "Cooked fish type cannot be null!");
        this.cookedFishType = cookedFishType;
    }

    @Override
    public String getNewStringId() {
        switch (cookedFishType) {
            case SALMON:
                return "minecraft:cooked_salmon";
            case COD:
            default:
                return "minecraft:cooked_cod";
        }
    }

    @Override
    public EnumItemTypeCookedFishType getCookedFishType() {
        return cookedFishType;
    }

    public void setCookedFishType(EnumItemTypeCookedFishType cookedFishType) {
        Validate.notNull(cookedFishType, "Cooked fish type cannot be null!");
        this.cookedFishType = cookedFishType;
    }

    @Override
    public SpongeItemTypeCookedFish clone() {
        return new SpongeItemTypeCookedFish(cookedFishType);
    }

    @Override
    public ItemStack writeItemStack(ItemStack itemStack) {
        super.writeItemStack(itemStack);
        itemStack.offer(Keys.COOKED_FISH, Sponge.getRegistry().getType(CookedFish.class, cookedFishType.name()).orElse(CookedFishes.COD));
        return itemStack;
    }

    @Override
    public SpongeItemTypeCookedFish readContainer(ValueContainer<?> valueContainer) {
        super.readContainer(valueContainer);
        cookedFishType = EnumItemTypeCookedFishType.getByName(valueContainer.get(Keys.COOKED_FISH)
                .orElse(CookedFishes.COD).getName()).orElse(EnumItemTypeCookedFishType.COD);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SpongeItemTypeCookedFish that = (SpongeItemTypeCookedFish) o;
        return cookedFishType == that.cookedFishType;
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), cookedFishType);
    }
}
