package com.degoos.wetsponge.material.item.type;

import com.degoos.wetsponge.firework.SpongeFireworkEffect;
import com.degoos.wetsponge.firework.WSFireworkEffect;
import com.degoos.wetsponge.material.item.SpongeItemType;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.value.ValueContainer;
import org.spongepowered.api.item.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class SpongeItemTypeFirework extends SpongeItemType implements WSItemTypeFirework {

    private int power;
    private List<WSFireworkEffect> effects;

    public SpongeItemTypeFirework(int power, List<WSFireworkEffect> effects) {
        super(401, "minecraft:fireworks", "minecraft:firework_rocket", 64);
        this.power = power;
        this.effects = effects == null ? new ArrayList<>() : new ArrayList<>(effects);
    }

    @Override
    public int getPower() {
        return power;
    }

    @Override
    public void setPower(int power) {
        this.power = power;
    }

    @Override
    public List<WSFireworkEffect> getEffects() {
        return effects;
    }

    @Override
    public void setEffects(List<WSFireworkEffect> effects) {
        this.effects = effects == null ? new ArrayList<>() : effects;
    }

    @Override
    public SpongeItemTypeFirework clone() {
        return new SpongeItemTypeFirework(power, effects);
    }

    @Override
    public ItemStack writeItemStack(ItemStack itemStack) {
        super.writeItemStack(itemStack);
        itemStack.offer(Keys.FIREWORK_EFFECTS, effects.stream().map(effect -> ((SpongeFireworkEffect) effect).getHandled())
                .collect(Collectors.toList()));
        return itemStack;
    }

    @Override
    public SpongeItemTypeFirework readContainer(ValueContainer<?> valueContainer) {
        super.readContainer(valueContainer);
        effects = valueContainer.get(Keys.FIREWORK_EFFECTS).orElseThrow(NullPointerException::new).stream().map(SpongeFireworkEffect::new)
                .collect(Collectors.toList());
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SpongeItemTypeFirework that = (SpongeItemTypeFirework) o;
        return power == that.power &&
                Objects.equals(effects, that.effects);
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), power, effects);
    }
}
