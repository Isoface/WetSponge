package com.degoos.wetsponge.material.item.type;

import com.degoos.wetsponge.enums.item.EnumItemTypeFishType;
import com.degoos.wetsponge.material.item.SpongeItemType;
import com.degoos.wetsponge.util.Validate;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.Fish;
import org.spongepowered.api.data.type.Fishes;
import org.spongepowered.api.data.value.ValueContainer;
import org.spongepowered.api.item.inventory.ItemStack;

import java.util.Objects;

public class SpongeItemTypeFish extends SpongeItemType implements WSItemTypeFish {

    private EnumItemTypeFishType fishType;

    public SpongeItemTypeFish(EnumItemTypeFishType fishType) {
        super(263, "minecraft:fish", "minecraft:cod", 64);
        Validate.notNull(fishType, "Fish type cannot be null!");
        this.fishType = fishType;
    }

    @Override
    public String getNewStringId() {
        switch (fishType) {
            case SALMON:
                return "minecraft:salmon";
            case TROPICAL_FISH:
                return "minecraft:tropical_fish";
            case PUFFERFISH:
                return "minecraft:pufferfish";
            case COD:
            default:
                return "minecraft:cod";
        }
    }

    @Override
    public EnumItemTypeFishType getFishType() {
        return fishType;
    }

    @Override
    public void setFishType(EnumItemTypeFishType fishType) {
        Validate.notNull(fishType, "Fish type cannot be null!");
        this.fishType = fishType;
    }

    @Override
    public SpongeItemTypeFish clone() {
        return new SpongeItemTypeFish(fishType);
    }

    @Override
    public ItemStack writeItemStack(ItemStack itemStack) {
        super.writeItemStack(itemStack);
        itemStack.offer(Keys.FISH_TYPE, Sponge.getRegistry().getType(Fish.class, fishType.getSpongeName()).orElse(Fishes.COD));
        return itemStack;
    }

    @Override
    public SpongeItemTypeFish readContainer(ValueContainer<?> valueContainer) {
        super.readContainer(valueContainer);
        fishType = EnumItemTypeFishType.getBySpongeName(valueContainer.get(Keys.FISH_TYPE).orElse(Fishes.COD).getName()).orElse(EnumItemTypeFishType.COD);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SpongeItemTypeFish that = (SpongeItemTypeFish) o;
        return fishType == that.fishType;
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), fishType);
    }
}
