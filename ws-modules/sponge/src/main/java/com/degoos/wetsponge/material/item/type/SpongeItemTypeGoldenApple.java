package com.degoos.wetsponge.material.item.type;

import com.degoos.wetsponge.enums.item.EnumItemTypeGoldenAppleType;
import com.degoos.wetsponge.material.item.SpongeItemType;
import com.degoos.wetsponge.util.Validate;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.GoldenApple;
import org.spongepowered.api.data.type.GoldenApples;
import org.spongepowered.api.data.value.ValueContainer;
import org.spongepowered.api.item.inventory.ItemStack;

import java.util.Objects;

public class SpongeItemTypeGoldenApple extends SpongeItemType implements WSItemTypeGoldenApple {

    private EnumItemTypeGoldenAppleType goldenAppleType;

    public SpongeItemTypeGoldenApple(EnumItemTypeGoldenAppleType goldenAppleType) {
        super(322, "minecraft:golden_apple", "minecraft:golden_apple", 64);
        Validate.notNull(goldenAppleType, "Golden apple type cannot be null!");
        this.goldenAppleType = goldenAppleType;
    }

    @Override
    public EnumItemTypeGoldenAppleType getGoldenAppleType() {
        return goldenAppleType;
    }

    @Override
    public void setGoldenAppleType(EnumItemTypeGoldenAppleType goldenAppleType) {
        Validate.notNull(goldenAppleType, "Golden apple type cannot be null!");
        this.goldenAppleType = goldenAppleType;
    }

    @Override
    public SpongeItemTypeGoldenApple clone() {
        return new SpongeItemTypeGoldenApple(goldenAppleType);
    }

    @Override
    public ItemStack writeItemStack(ItemStack itemStack) {
        super.writeItemStack(itemStack);
        itemStack.offer(Keys.GOLDEN_APPLE_TYPE, Sponge.getRegistry().getType(GoldenApple.class, goldenAppleType.name()).orElse(GoldenApples.GOLDEN_APPLE));
        return itemStack;
    }

    @Override
    public SpongeItemTypeGoldenApple readContainer(ValueContainer<?> valueContainer) {
        super.readContainer(valueContainer);
        goldenAppleType = EnumItemTypeGoldenAppleType.getByName(valueContainer.get(Keys.GOLDEN_APPLE_TYPE).orElse(GoldenApples.GOLDEN_APPLE).getName())
                .orElse(EnumItemTypeGoldenAppleType.GOLDEN_APPLE);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SpongeItemTypeGoldenApple that = (SpongeItemTypeGoldenApple) o;
        return goldenAppleType == that.goldenAppleType;
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), goldenAppleType);
    }
}
