package com.degoos.wetsponge.material.item.type;

import com.degoos.wetsponge.color.WSColor;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.value.ValueContainer;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.util.Color;

import java.util.Objects;

public class SpongeItemTypeLeatherArmor extends SpongeItemTypeDamageable implements WSItemTypeLeatherArmor {

    private WSColor color;

    public SpongeItemTypeLeatherArmor(int numericalId, String oldStringId, String newStringId, int damage, int maxUses, WSColor color) {
        super(numericalId, oldStringId, newStringId, damage, maxUses);
        this.color = color;
    }


    @Override
    public WSColor getColor() {
        return color;
    }

    @Override
    public void setColor(WSColor color) {
        this.color = color;
    }

    @Override
    public SpongeItemTypeLeatherArmor clone() {
        return new SpongeItemTypeLeatherArmor(getNumericalId(), getOldStringId(), getNewStringId(), getDamage(), getMaxUses(), color);
    }

    @Override
    public ItemStack writeItemStack(ItemStack itemStack) {
        super.writeItemStack(itemStack);
        itemStack.offer(Keys.COLOR, Color.ofRgb(color.toRGB()));
        return itemStack;
    }

    @Override
    public SpongeItemTypeLeatherArmor readContainer(ValueContainer<?> valueContainer) {
        super.readContainer(valueContainer);
        color = valueContainer.get(Keys.COLOR).map(Color::getRgb).map(WSColor::ofRGB).orElse(null);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SpongeItemTypeLeatherArmor that = (SpongeItemTypeLeatherArmor) o;
        return Objects.equals(color, that.color);
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), color);
    }
}
