package com.degoos.wetsponge.material.item.type;

import com.degoos.wetsponge.bridge.material.item.BridgeItemTypeSkull;
import com.degoos.wetsponge.enums.block.EnumBlockTypeSkullType;
import com.degoos.wetsponge.material.item.SpongeItemType;
import com.degoos.wetsponge.user.SpongeGameProfile;
import com.degoos.wetsponge.user.WSGameProfile;
import com.degoos.wetsponge.util.Validate;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.SkullType;
import org.spongepowered.api.data.value.ValueContainer;
import org.spongepowered.api.item.inventory.ItemStack;

import java.net.URL;
import java.util.Objects;
import java.util.Optional;

public class SpongeItemTypeSkull extends SpongeItemType implements WSItemTypeSkull {


    private WSGameProfile profile;
    private EnumBlockTypeSkullType skullType;

    public SpongeItemTypeSkull(WSGameProfile profile, EnumBlockTypeSkullType skullType) {
        super(397, "minecraft:skull", "minecraft:skull", 64);
        Validate.notNull(skullType, "Skull type cannot be null!");
        this.profile = profile;
        this.skullType = skullType;
    }

    @Override
    public String getNewStringId() {
        return skullType.getMinecraftName() + (skullType == EnumBlockTypeSkullType.SKELETON || skullType == EnumBlockTypeSkullType.WITHER_SKELETON ? "_skull" : "_head");
    }

    public Optional<WSGameProfile> getProfile() {
        return Optional.ofNullable(profile);
    }

    public void setProfile(WSGameProfile profile) {
        this.profile = profile;
    }

    public void setTexture(String texture) {
        BridgeItemTypeSkull.setTexture(texture, profile);
    }

    public void setTexture(URL texture) {
        BridgeItemTypeSkull.setTexture(texture, profile);
    }

    public void setTextureByPlayerName(String name) {
        profile = BridgeItemTypeSkull.setTextureByPlayerName(name);
    }

    public void findFormatAndSetTexture(String texture) {
        if (texture.toLowerCase().startsWith("http:") || texture.toLowerCase().startsWith("https:"))
            setTexture(getTexture(texture));
        else if (texture.length() <= 16) setTextureByPlayerName(texture);
        else setTexture(texture);
    }

    public EnumBlockTypeSkullType getSkullType() {
        return skullType;
    }

    public void setSkullType(EnumBlockTypeSkullType skullType) {
        Validate.notNull(skullType, "Skull type cannot be null!");
        this.skullType = skullType;
    }

    private String getTexture(String url) {
        if (url == null) return null;
        return String.format("{textures:{SKIN:{url:\"%s\"}}}", url);
    }

    @Override
    public SpongeItemTypeSkull clone() {
        return new SpongeItemTypeSkull(profile, skullType);
    }

    @Override
    public ItemStack writeItemStack(ItemStack itemStack) {
        super.writeItemStack(itemStack);
        itemStack.offer(Keys.SKULL_TYPE, Sponge.getRegistry().getType(SkullType.class, skullType.getSpongeName()).orElseThrow(NullPointerException::new));
        itemStack.offer(Keys.REPRESENTED_PLAYER, profile == null ? null : ((SpongeGameProfile) profile).getHandled());
        return itemStack;
    }

    @Override
    public SpongeItemTypeSkull readContainer(ValueContainer<?> valueContainer) {
        super.readContainer(valueContainer);
        skullType = EnumBlockTypeSkullType.getBySpongeName(valueContainer.get(Keys.SKULL_TYPE).orElseThrow(NullPointerException::new)
                .getName()).orElse(EnumBlockTypeSkullType.SKELETON);
        org.spongepowered.api.profile.GameProfile profile = valueContainer.get(Keys.REPRESENTED_PLAYER).orElse(null);
        if (profile == null) this.profile = null;
        else this.profile = new SpongeGameProfile(profile);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SpongeItemTypeSkull that = (SpongeItemTypeSkull) o;
        return Objects.equals(profile, that.profile) &&
                skullType == that.skullType;
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), profile, skullType);
    }
}
