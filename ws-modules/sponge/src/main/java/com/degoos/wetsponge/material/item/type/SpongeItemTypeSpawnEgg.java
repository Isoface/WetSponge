package com.degoos.wetsponge.material.item.type;

import com.degoos.wetsponge.enums.EnumEntityType;
import com.degoos.wetsponge.material.item.SpongeItemType;
import com.degoos.wetsponge.util.Validate;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.value.ValueContainer;
import org.spongepowered.api.entity.EntityType;
import org.spongepowered.api.item.inventory.ItemStack;

import java.util.Objects;

public class SpongeItemTypeSpawnEgg extends SpongeItemType implements WSItemTypeSpawnEgg {

    private EnumEntityType entityType;

    public SpongeItemTypeSpawnEgg(EnumEntityType entityType) {
        super(383, "minecraft:spawn_egg", "minecraft:$entity_spawn_egg", 64);
        Validate.notNull(entityType, "Entity type cannot be null!");
        this.entityType = entityType;
    }

    @Override
    public String getNewStringId() {
        return "minecraft:" + entityType.getName().toLowerCase() + "_spawn_egg";
    }


    @Override
    public EnumEntityType getEntityType() {
        return entityType;
    }

    @Override
    public void setEntityType(EnumEntityType entityType) {
        Validate.notNull(entityType, "Entity type cannot be null!");
        this.entityType = entityType;
    }


    @Override
    public SpongeItemTypeSpawnEgg clone() {
        return new SpongeItemTypeSpawnEgg(entityType);
    }

    @Override
    public ItemStack writeItemStack(ItemStack itemStack) {
        super.writeItemStack(itemStack);
        itemStack.offer(Keys.SPAWNABLE_ENTITY_TYPE, Sponge.getRegistry().getType(EntityType.class, entityType.getName()).orElseThrow(NullPointerException::new));
        return itemStack;
    }

    @Override
    public SpongeItemTypeSpawnEgg readContainer(ValueContainer<?> valueContainer) {
        super.readContainer(valueContainer);
        entityType = EnumEntityType.getByName(valueContainer.get(Keys.SPAWNABLE_ENTITY_TYPE).orElseThrow(NullPointerException::new).getId())
                .orElseThrow(NullPointerException::new);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SpongeItemTypeSpawnEgg that = (SpongeItemTypeSpawnEgg) o;
        return entityType == that.entityType;
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), entityType);
    }
}
