package com.degoos.wetsponge.material.item.type;

import com.degoos.wetsponge.enums.item.EnumItemTypeBookGeneration;
import com.degoos.wetsponge.material.item.SpongeItemType;
import com.degoos.wetsponge.text.SpongeText;
import com.degoos.wetsponge.text.WSText;
import com.degoos.wetsponge.util.Validate;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.value.ValueContainer;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.text.Text;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class SpongeItemTypeWrittenBook extends SpongeItemType implements WSItemTypeWrittenBook {


    private WSText title;
    private WSText author;
    private List<WSText> pages;
    private EnumItemTypeBookGeneration generation;


    public SpongeItemTypeWrittenBook(int id, String stringId, String stringId13, WSText title, WSText author, List<WSText> pages, EnumItemTypeBookGeneration generation) {
        super(id, stringId, stringId13, 1);
        Validate.notNull(generation, "Generation cannot be null!");
        this.title = title;
        this.author = author;
        this.pages = pages == null ? new ArrayList<>() : pages;
        this.generation = generation;
    }

    @Override
    public WSText getAuthor() {
        return author;
    }

    @Override
    public WSItemTypeWrittenBook setAuthor(WSText author) {
        this.author = author;
        return this;
    }

    @Override
    public WSText getTitle() {
        return title;
    }

    @Override
    public WSItemTypeWrittenBook setTitle(WSText title) {
        this.title = title;
        return this;
    }

    @Override
    public List<WSText> getPages() {
        return pages;
    }

    @Override
    public WSItemTypeWrittenBook addPage(WSText pageText) {
        pages.add(pageText);
        return this;
    }

    @Override
    public WSItemTypeWrittenBook setPages(List<WSText> pages) {
        this.pages = pages;
        return this;
    }

    @Override
    public EnumItemTypeBookGeneration getGeneration() {
        return generation;
    }

    @Override
    public void setGeneration(EnumItemTypeBookGeneration generation) {
        Validate.notNull(generation, "Generation cannot be null!");
        this.generation = generation;
    }

    @Override
    public SpongeItemTypeWrittenBook clone() {
        return new SpongeItemTypeWrittenBook(getNumericalId(), getStringId(), getStringId(), title, author, new ArrayList<>(pages), generation);
    }

    @Override
    public ItemStack writeItemStack(ItemStack itemStack) {
        super.writeItemStack(itemStack);
        itemStack.offer(Keys.BOOK_AUTHOR, (Text) author.getHandled());
        itemStack.offer(Keys.BOOK_PAGES, pages.stream().map(text -> (Text) text.getHandled()).collect(Collectors.toList()));
        itemStack.offer(Keys.DISPLAY_NAME, (Text) title.getHandled());
        itemStack.offer(Keys.GENERATION, generation.getValue());
        return itemStack;
    }

    @Override
    public SpongeItemTypeWrittenBook readContainer(ValueContainer<?> valueContainer) {
        super.readContainer(valueContainer);
        author = valueContainer.get(Keys.BOOK_AUTHOR).map(SpongeText::of).orElse((SpongeText) WSText.empty());
        pages = valueContainer.get(Keys.BOOK_PAGES).orElse(new ArrayList<>()).stream().map(SpongeText::of).collect(Collectors.toList());
        title = valueContainer.get(Keys.DISPLAY_NAME).map(SpongeText::of).orElse((SpongeText) WSText.empty());
        generation = EnumItemTypeBookGeneration.getByValue(valueContainer.get(Keys.GENERATION).orElse(0)).orElse(EnumItemTypeBookGeneration.ORIGINAL);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SpongeItemTypeWrittenBook that = (SpongeItemTypeWrittenBook) o;
        return Objects.equals(title, that.title) &&
                Objects.equals(author, that.author) &&
                Objects.equals(pages, that.pages) &&
                generation == that.generation;
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), title, author, pages, generation);
    }
}
