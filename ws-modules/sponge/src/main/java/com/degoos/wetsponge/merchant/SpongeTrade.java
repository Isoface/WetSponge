package com.degoos.wetsponge.merchant;

import com.degoos.wetsponge.item.SpongeItemStack;
import com.degoos.wetsponge.item.WSItemStack;
import com.degoos.wetsponge.util.Validate;
import java.util.Optional;
import org.spongepowered.api.item.inventory.ItemStackSnapshot;
import org.spongepowered.api.item.merchant.TradeOffer;

public class SpongeTrade implements WSTrade {

	private TradeOffer trade;


	public SpongeTrade(TradeOffer trade) {
		this.trade = trade;
	}

	@Override
	public int getUses() {
		return trade.getUses();
	}

	@Override
	public int getMaxUses() {
		return trade.getMaxUses();
	}

	@Override
	public WSItemStack getResult() {
		return new SpongeItemStack(trade.getSellingItem().createStack());
	}


	@Override
	public WSItemStack getFirstItem() {
		return new SpongeItemStack(trade.getFirstBuyingItem().createStack());
	}


	@Override
	public Optional<WSItemStack> getSecondItem() {
		return trade.getSecondBuyingItem().filter(item -> !item.getType().getId().equals("minecraft:air")).map(ItemStackSnapshot::createStack).map(SpongeItemStack::new);
	}

	@Override
	public boolean doesGrantExperience() {
		return trade.doesGrantExperience();
	}

	@Override
	public TradeOffer getHandled() {
		return trade;
	}

	public static class Builder implements WSTrade.Builder {

		private TradeOffer.Builder builder;

		public Builder() {
			builder = TradeOffer.builder();
		}

		@Override
		public WSTrade.Builder uses(int uses) {
			builder.uses(uses);
			return this;
		}

		@Override
		public WSTrade.Builder maxUses(int maxUses) {
			builder.maxUses(maxUses);
			return this;
		}

		@Override
		public WSTrade.Builder result(WSItemStack result) {
			Validate.notNull(result, "Result cannot be null!");
			builder.sellingItem(((SpongeItemStack) result).getHandled());
			return this;
		}

		@Override
		public WSTrade.Builder firstItem(WSItemStack firstItem) {
			Validate.notNull(firstItem, "First item cannot be null!");
			builder.firstBuyingItem(((SpongeItemStack) firstItem).getHandled());
			return this;
		}

		@Override
		public WSTrade.Builder secondItem(WSItemStack secondItem) {
			if (secondItem == null || secondItem.getMaterial().getNumericalId() == 0) builder.secondBuyingItem(null);
			else builder.secondBuyingItem(((SpongeItemStack) secondItem).getHandled());
			return this;
		}

		@Override
		public WSTrade.Builder canGrantExperience(boolean grantExperience) {
			builder.canGrantExperience(grantExperience);
			return this;
		}

		@Override
		public WSTrade build() {
			return new SpongeTrade(builder.build());
		}
	}

}
