package com.degoos.wetsponge.mixin.sponge.interfaces;

import net.minecraft.world.chunk.Chunk;

public interface WSMixinChunkProviderServer {

	public Chunk vanillaProvideChunk(int x, int z);

}
