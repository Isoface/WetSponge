package com.degoos.wetsponge.mixin.sponge.mixin;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.data.WSTransaction;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.enums.EnumEquipType;
import com.degoos.wetsponge.event.entity.player.WSPlayerChangeHotbarSlotEvent;
import com.degoos.wetsponge.event.entity.player.WSPlayerSwapHandItemsEvent;
import com.degoos.wetsponge.item.SpongeItemStack;
import com.degoos.wetsponge.item.WSItemStack;
import com.degoos.wetsponge.packet.play.server.WSSPacketHeldItemChange;
import com.degoos.wetsponge.parser.player.PlayerParser;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.network.NetHandlerPlayServer;
import net.minecraft.network.play.client.CPacketHeldItemChange;
import net.minecraft.network.play.client.CPacketPlayerDigging;
import net.minecraft.network.play.client.CPacketPlayerDigging.Action;
import net.minecraft.util.EnumHand;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(value = NetHandlerPlayServer.class, priority = Integer.MAX_VALUE)
public class MixinNetHandlerPlayServer {

	@Shadow
	public EntityPlayerMP player;

	public WSPlayer wsPlayer = null;

	@Inject(method = "processPlayerDigging", at = @At(value = "HEAD"), cancellable = true)
	public void processPlayerDigging(CPacketPlayerDigging packetPlayerDigging, CallbackInfo callbackInfo) {
		if (packetPlayerDigging.getAction().equals(Action.SWAP_HELD_ITEMS)) {
			WSPlayer player = wsPlayer == null ? wsPlayer = PlayerParser.getPlayer(this.player.getUniqueID()).orElse(null) : wsPlayer;
			WSItemStack offHand = player.getEquippedItem(EnumEquipType.OFF_HAND).orElse(null);
			WSItemStack mainHand = player.getEquippedItem(EnumEquipType.MAIN_HAND).orElse(null);
			WSPlayerSwapHandItemsEvent event = new WSPlayerSwapHandItemsEvent(player, new WSTransaction<>(mainHand, offHand), new WSTransaction<>(offHand, mainHand));
			WetSponge.getEventManager().callEvent(event);
			if (event.isCancelled()) {
				callbackInfo.cancel();
				return;
			}
			if (!this.player.isSpectator()) {
				WSItemStack main = event.getMainHand().getNewData();
				WSItemStack off = event.getOffHand().getNewData();
				if (main == null) this.player.setHeldItem(EnumHand.MAIN_HAND, ItemStack.EMPTY);
				else this.player.setHeldItem(EnumHand.MAIN_HAND, (ItemStack) (Object) ((SpongeItemStack) main).getHandled());
				if (off == null) this.player.setHeldItem(EnumHand.OFF_HAND, ItemStack.EMPTY);
				else this.player.setHeldItem(EnumHand.OFF_HAND, (ItemStack) (Object) ((SpongeItemStack) off).getHandled());
			}
			callbackInfo.cancel();
		}
	}

	@Inject(method = "processHeldItemChange", at = @At(value = "HEAD"), cancellable = true)
	public void onProcessHeldItemChange(CPacketHeldItemChange packet, CallbackInfo callbackInfo) {
		WSPlayer player = wsPlayer == null ? wsPlayer = PlayerParser.getPlayer(this.player.getUniqueID()).orElse(null) : wsPlayer;
		WSPlayerChangeHotbarSlotEvent event = new WSPlayerChangeHotbarSlotEvent(player, player.getSelectedHotbarSlot(), packet.getSlotId());
		WetSponge.getEventManager().callEvent(event);
		if (event.isCancelled()) {
			player.sendPacket(WSSPacketHeldItemChange.of(player.getSelectedHotbarSlot()));
			this.player.markPlayerActive();
			callbackInfo.cancel();
		}
	}

}
