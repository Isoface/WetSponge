package com.degoos.wetsponge.mixin.sponge.mixin.entity;

import com.degoos.wetsponge.mixin.sponge.interfaces.WSMixinAbstractHorse;
import net.minecraft.entity.passive.AbstractHorse;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;

@Mixin(value = AbstractHorse.class, priority = Integer.MAX_VALUE)
public class MixinAbstractHorse implements WSMixinAbstractHorse {

	private int maxTemper = 100;

	@Overwrite
	public int getMaxTemper() {
		return maxTemper;
	}

	@Override
	public void setMaxTemper(int maxTemper) {
		this.maxTemper = maxTemper;
	}

}
