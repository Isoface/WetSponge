package com.degoos.wetsponge.nbt;

import net.minecraft.nbt.NBTTagByte;

public class SpongeNBTTagByte extends SpongeNBTBase implements WSNBTTagByte {

	public SpongeNBTTagByte(NBTTagByte nbtTagByte) {
		super(nbtTagByte);
	}

	public SpongeNBTTagByte(byte b) {
		this(new NBTTagByte(b));
	}

	@Override
	public long getLong() {
		return getHandled().getLong();
	}

	@Override
	public int getInt() {
		return getHandled().getInt();
	}

	@Override
	public short getShort() {
		return getHandled().getShort();
	}

	@Override
	public byte getByte() {
		return getHandled().getByte();
	}

	@Override
	public double getDouble() {
		return getHandled().getDouble();
	}

	@Override
	public float getFloat() {
		return getHandled().getFloat();
	}

	@Override
	public WSNBTTagByte copy() {
		return new SpongeNBTTagByte(getHandled().copy());
	}

	@Override
	public NBTTagByte getHandled() {
		return (NBTTagByte) super.getHandled();
	}
}
