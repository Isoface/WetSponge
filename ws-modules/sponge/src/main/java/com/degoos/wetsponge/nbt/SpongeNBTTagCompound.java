package com.degoos.wetsponge.nbt;

import net.minecraft.nbt.JsonToNBT;
import net.minecraft.nbt.NBTTagCompound;

import javax.annotation.Nullable;
import java.util.Set;
import java.util.UUID;

public class SpongeNBTTagCompound extends SpongeNBTBase implements WSNBTTagCompound {

	public SpongeNBTTagCompound(NBTTagCompound nbtTagCompound) {
		super(nbtTagCompound);
	}

	public SpongeNBTTagCompound() {
		this(new NBTTagCompound());
	}

	public SpongeNBTTagCompound(String nbt) throws Exception {
		this(JsonToNBT.getTagFromJson(NBTTagUpdater.update(nbt)));
	}

	@Override
	public Set<String> getKeySet() {
		return getHandled().getKeySet();
	}

	@Override
	public int getSize() {
		return getHandled().getSize();
	}

	@Override
	public void setTag(String key, WSNBTBase wsnbt) {
		getHandled().setTag(key, ((SpongeNBTBase) wsnbt.getHandled()).getHandled());
	}

	@Override
	public void setByte(String key, byte b) {
		getHandled().setByte(key, b);
	}

	@Override
	public void setShort(String key, short s) {
		getHandled().setShort(key, s);
	}

	@Override
	public void setInteger(String key, int i) {
		getHandled().setInteger(key, i);
	}

	@Override
	public void setLong(String key, long l) {
		getHandled().setLong(key, l);
	}

	@Override
	public void setUniqueId(String key, UUID uuid) {
		getHandled().setUniqueId(key, uuid);
	}

	@Override
	public void setFloat(String key, float f) {
		getHandled().setFloat(key, f);
	}

	@Override
	public void setDouble(String key, double d) {
		getHandled().setDouble(key, d);
	}

	@Override
	public void setString(String key, String string) {
		getHandled().setString(key, string);
	}

	@Override
	public void setByteArray(String key, byte[] bytes) {
		getHandled().setByteArray(key, bytes);
	}

	@Override
	public void setIntArray(String key, int[] ints) {
		getHandled().setIntArray(key, ints);
	}

	@Override
	public void setBoolean(String key, boolean b) {
		getHandled().setBoolean(key, b);
	}

	@Override
	public WSNBTBase getTag(String key) {
		return SpongeNBTTagParser.parse(getHandled().getTag(key));
	}

	@Override
	public byte getTagId(String key) {
		return getHandled().getTagId(key);
	}

	@Override
	public boolean hasKey(String key) {
		return getHandled().hasKey(key);
	}

	@Override
	public boolean hasKeyOfType(String key, int id) {
		return getHandled().hasKey(key, id);
	}

	@Override
	public byte getByte(String key) {
		return getHandled().getByte(key);
	}

	@Override
	public short getShort(String key) {
		return getHandled().getShort(key);
	}

	@Override
	public int getInteger(String key) {
		return getHandled().getInteger(key);
	}

	@Override
	public long getLong(String key) {
		return getHandled().getLong(key);
	}

	@Nullable
	@Override
	public UUID getUniqueId(String key) {
		return getHandled().getUniqueId(key);
	}

	@Override
	public boolean hasUniqueId(String key) {
		return getHandled().hasUniqueId(key);
	}

	@Override
	public float getFloat(String key) {
		return getHandled().getFloat(key);
	}

	@Override
	public double getDouble(String key) {
		return getHandled().getDouble(key);
	}

	@Override
	public String getString(String key) {
		return getHandled().getString(key);
	}

	@Override
	public byte[] getByteArray(String key) {
		return getHandled().getByteArray(key);
	}

	@Override
	public int[] getIntArray(String key) {
		return getHandled().getIntArray(key);
	}

	@Override
	public WSNBTTagCompound getCompoundTag(String key) {
		return new SpongeNBTTagCompound(getHandled().getCompoundTag(key));
	}

	@Override
	public WSNBTTagList getTagList(String key, int expectedTagId) {
		return new SpongeNBTTagList(getHandled().getTagList(key, expectedTagId));
	}

	@Override
	public boolean getBoolean(String key) {
		return getHandled().getBoolean(key);
	}

	@Override
	public void removeTag(String key) {
		getHandled().removeTag(key);
	}

	@Override
	public void merge(WSNBTTagCompound wsnbtTagCompound) {
		getHandled().merge((NBTTagCompound) wsnbtTagCompound.getHandled());
	}

	@Override
	public WSNBTTagCompound copy() {
		return new SpongeNBTTagCompound(getHandled().copy());
	}

	@Override
	public NBTTagCompound getHandled() {
		return (NBTTagCompound) super.getHandled();
	}
}
