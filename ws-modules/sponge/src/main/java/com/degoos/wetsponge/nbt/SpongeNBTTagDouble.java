package com.degoos.wetsponge.nbt;

import net.minecraft.nbt.NBTTagDouble;

public class SpongeNBTTagDouble extends SpongeNBTBase implements WSNBTTagDouble {

	public SpongeNBTTagDouble(NBTTagDouble nbtTagDouble) {
		super(nbtTagDouble);
	}

	public SpongeNBTTagDouble(double d) {
		this(new NBTTagDouble(d));
	}

	@Override
	public long getLong() {
		return getHandled().getLong();
	}

	@Override
	public int getInt() {
		return getHandled().getInt();
	}

	@Override
	public short getShort() {
		return getHandled().getShort();
	}

	@Override
	public byte getByte() {
		return getHandled().getByte();
	}

	@Override
	public double getDouble() {
		return getHandled().getDouble();
	}

	@Override
	public float getFloat() {
		return getHandled().getFloat();
	}

	@Override
	public WSNBTTagDouble copy() {
		return new SpongeNBTTagDouble(getHandled().copy());
	}

	@Override
	public NBTTagDouble getHandled() {
		return (NBTTagDouble) super.getHandled();
	}
}
