package com.degoos.wetsponge.nbt;

import com.degoos.wetsponge.util.InternalLogger;
import java.lang.reflect.Constructor;
import net.minecraft.nbt.NBTTagEnd;

public class SpongeNBTTagEnd extends SpongeNBTBase implements WSNBTTagEnd {

	public SpongeNBTTagEnd(NBTTagEnd nbtTagEnd) {
		super(nbtTagEnd);
	}

	public SpongeNBTTagEnd() {
		this(newInstance());
	}

	@Override
	public WSNBTTagEnd copy() {
		return new SpongeNBTTagEnd(getHandled().copy());
	}

	@Override
	public NBTTagEnd getHandled() {
		return (NBTTagEnd) super.getHandled();
	}

	private static NBTTagEnd newInstance() {
		try {
			Constructor<NBTTagEnd> constructor = NBTTagEnd.class.getDeclaredConstructor();
			constructor.setAccessible(true);
			return constructor.newInstance();
		} catch (Exception e) {
			InternalLogger.printException(e, "An error has occurred while WetSponge was creating a new instance of a NBTTagEnd!");
			return null;
		}
	}
}
