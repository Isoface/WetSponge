package com.degoos.wetsponge.nbt;

import net.minecraft.nbt.NBTTagFloat;

public class SpongeNBTTagFloat extends SpongeNBTBase implements WSNBTTagFloat {

	public SpongeNBTTagFloat(NBTTagFloat nbtTagFloat) {
		super(nbtTagFloat);
	}

	public SpongeNBTTagFloat(float f) {
		this(new NBTTagFloat(f));
	}

	@Override
	public long getLong() {
		return getHandled().getLong();
	}

	@Override
	public int getInt() {
		return getHandled().getInt();
	}

	@Override
	public short getShort() {
		return getHandled().getShort();
	}

	@Override
	public byte getByte() {
		return getHandled().getByte();
	}

	@Override
	public double getDouble() {
		return getHandled().getDouble();
	}

	@Override
	public float getFloat() {
		return getHandled().getFloat();
	}

	@Override
	public WSNBTTagFloat copy() {
		return new SpongeNBTTagFloat(getHandled().copy());
	}

	@Override
	public NBTTagFloat getHandled() {
		return (NBTTagFloat) super.getHandled();
	}
}
