package com.degoos.wetsponge.nbt;

import net.minecraft.nbt.NBTTagInt;

public class SpongeNBTTagInt extends SpongeNBTBase implements WSNBTTagInt {


	public SpongeNBTTagInt(NBTTagInt nbtTagInt) {
		super(nbtTagInt);
	}

	public SpongeNBTTagInt(int i) {
		this(new NBTTagInt(i));
	}

	@Override
	public long getLong() {
		return getHandled().getLong();
	}

	@Override
	public int getInt() {
		return getHandled().getInt();
	}

	@Override
	public short getShort() {
		return getHandled().getShort();
	}

	@Override
	public byte getByte() {
		return getHandled().getByte();
	}

	@Override
	public double getDouble() {
		return getHandled().getDouble();
	}

	@Override
	public float getFloat() {
		return getHandled().getFloat();
	}

	@Override
	public WSNBTTagInt copy() {
		return new SpongeNBTTagInt(getHandled().copy());
	}

	@Override
	public NBTTagInt getHandled() {
		return (NBTTagInt) super.getHandled();
	}
}
