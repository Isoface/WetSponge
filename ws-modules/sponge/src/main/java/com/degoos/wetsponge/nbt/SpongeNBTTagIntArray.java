package com.degoos.wetsponge.nbt;

import net.minecraft.nbt.NBTTagIntArray;

public class SpongeNBTTagIntArray extends SpongeNBTBase implements WSNBTTagIntArray {

	public SpongeNBTTagIntArray(NBTTagIntArray nbtTagIntArray) {
		super(nbtTagIntArray);
	}

	public SpongeNBTTagIntArray(int[] array) {
		this(new NBTTagIntArray(array));
	}

	@Override
	public int[] getIntArray() {
		return getHandled().getIntArray();
	}


	@Override
	public WSNBTTagIntArray copy() {
		return new SpongeNBTTagIntArray(getHandled().copy());
	}

	@Override
	public NBTTagIntArray getHandled() {
		return (NBTTagIntArray) super.getHandled();
	}
}
