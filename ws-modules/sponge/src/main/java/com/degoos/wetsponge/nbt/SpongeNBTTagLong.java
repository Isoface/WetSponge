package com.degoos.wetsponge.nbt;

import net.minecraft.nbt.NBTTagLong;

public class SpongeNBTTagLong extends SpongeNBTBase implements WSNBTTagLong {

	public SpongeNBTTagLong(NBTTagLong nbtTagLong) {
		super(nbtTagLong);
	}

	public SpongeNBTTagLong(long l) {
		this(new NBTTagLong(l));
	}

	@Override
	public long getLong() {
		return getHandled().getLong();
	}

	@Override
	public int getInt() {
		return getHandled().getInt();
	}

	@Override
	public short getShort() {
		return getHandled().getShort();
	}

	@Override
	public byte getByte() {
		return getHandled().getByte();
	}

	@Override
	public double getDouble() {
		return getHandled().getDouble();
	}

	@Override
	public float getFloat() {
		return getHandled().getFloat();
	}

	@Override
	public WSNBTTagLong copy() {
		return new SpongeNBTTagLong(getHandled().copy());
	}

	@Override
	public NBTTagLong getHandled() {
		return (NBTTagLong) super.getHandled();
	}
}
