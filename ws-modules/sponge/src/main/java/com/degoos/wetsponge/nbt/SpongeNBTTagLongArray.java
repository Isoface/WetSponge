package com.degoos.wetsponge.nbt;

import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.reflection.ReflectionUtils;
import net.minecraft.nbt.NBTTagLongArray;

public class SpongeNBTTagLongArray extends SpongeNBTBase implements WSNBTTagLongArray {

	public SpongeNBTTagLongArray(NBTTagLongArray nbtTagLongArray) {
		super(nbtTagLongArray);
	}

	public SpongeNBTTagLongArray(long[] array) {
		this(new NBTTagLongArray(array));
	}

	@Override
	public long[] getLongArray() {
		try {
			return ReflectionUtils.getFirstObject(NBTTagLongArray.class, long[].class, getHandled());
		} catch (Exception e) {
			InternalLogger.printException(e, "An error has occurred while WetSponge was getting the value of a NBTTag!");
			return new long[0];
		}
	}


	@Override
	public WSNBTTagLongArray copy() {
		return new SpongeNBTTagLongArray(getHandled().copy());
	}

	@Override
	public NBTTagLongArray getHandled() {
		return (NBTTagLongArray) super.getHandled();
	}
}
