package com.degoos.wetsponge.nbt;

import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagByte;
import net.minecraft.nbt.NBTTagByteArray;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagDouble;
import net.minecraft.nbt.NBTTagEnd;
import net.minecraft.nbt.NBTTagFloat;
import net.minecraft.nbt.NBTTagInt;
import net.minecraft.nbt.NBTTagIntArray;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.nbt.NBTTagLong;
import net.minecraft.nbt.NBTTagLongArray;
import net.minecraft.nbt.NBTTagShort;
import net.minecraft.nbt.NBTTagString;

public class SpongeNBTTagParser {

	public static WSNBTBase parse(NBTBase nbtBase) {
		switch (nbtBase.getId()) {
			case 0:
				return new SpongeNBTTagEnd((NBTTagEnd) nbtBase);
			case 1:
				return new SpongeNBTTagByte((NBTTagByte) nbtBase);
			case 2:
				return new SpongeNBTTagShort((NBTTagShort) nbtBase);
			case 3:
				return new SpongeNBTTagInt((NBTTagInt) nbtBase);
			case 4:
				return new SpongeNBTTagLong((NBTTagLong) nbtBase);
			case 5:
				return new SpongeNBTTagFloat((NBTTagFloat) nbtBase);
			case 6:
				return new SpongeNBTTagDouble((NBTTagDouble) nbtBase);
			case 7:
				return new SpongeNBTTagByteArray((NBTTagByteArray) nbtBase);
			case 8:
				return new SpongeNBTTagString((NBTTagString) nbtBase);
			case 9:
				return new SpongeNBTTagList((NBTTagList) nbtBase);
			case 10:
				return new SpongeNBTTagCompound((NBTTagCompound) nbtBase);
			case 11:
				return new SpongeNBTTagIntArray((NBTTagIntArray) nbtBase);
			case 12:
				return new SpongeNBTTagLongArray((NBTTagLongArray) nbtBase);
			default:
				return null;
		}
	}

}
