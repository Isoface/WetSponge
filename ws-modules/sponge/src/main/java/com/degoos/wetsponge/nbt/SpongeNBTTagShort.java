package com.degoos.wetsponge.nbt;

import net.minecraft.nbt.NBTTagShort;

public class SpongeNBTTagShort extends SpongeNBTBase implements WSNBTTagShort {

	public SpongeNBTTagShort(NBTTagShort nbtTagShort) {
		super(nbtTagShort);
	}

	public SpongeNBTTagShort(short s) {
		this(new NBTTagShort(s));
	}

	@Override
	public long getLong() {
		return getHandled().getLong();
	}

	@Override
	public int getInt() {
		return getHandled().getInt();
	}

	@Override
	public short getShort() {
		return getHandled().getShort();
	}

	@Override
	public byte getByte() {
		return getHandled().getByte();
	}

	@Override
	public double getDouble() {
		return getHandled().getDouble();
	}

	@Override
	public float getFloat() {
		return getHandled().getFloat();
	}

	@Override
	public WSNBTTagShort copy() {
		return new SpongeNBTTagShort(getHandled().copy());
	}

	@Override
	public NBTTagShort getHandled() {
		return (NBTTagShort) super.getHandled();
	}
}
