package com.degoos.wetsponge.nbt;

import net.minecraft.nbt.NBTTagString;

public class SpongeNBTTagString extends SpongeNBTBase implements WSNBTTagString {

	public SpongeNBTTagString(NBTTagString nbtTagString) {
		super(nbtTagString);
	}

	public SpongeNBTTagString(String s) {
		this(new NBTTagString(s));
	}

	@Override
	public String getString() {
		return getHandled().getString();
	}

	@Override
	public WSNBTTagString copy() {
		return new SpongeNBTTagString(getHandled().copy());
	}

	@Override
	public NBTTagString getHandled() {
		return (NBTTagString) super.getHandled();
	}
}
