package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.enums.EnumEquipType;
import com.degoos.wetsponge.item.SpongeItemStack;
import com.degoos.wetsponge.item.WSItemStack;
import com.degoos.wetsponge.material.WSBlockTypes;
import com.degoos.wetsponge.packet.SpongePacket;
import com.degoos.wetsponge.util.Validate;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.network.Packet;
import net.minecraft.network.play.server.SPacketEntityEquipment;

import java.lang.reflect.Field;
import java.util.Arrays;

public class SpongeSPacketEntityEquipment extends SpongePacket implements WSSPacketEntityEquipment {

	private int entityId;
	private EnumEquipType equipType;
	private WSItemStack itemStack;
	private boolean changed;

	public SpongeSPacketEntityEquipment(int entityId, EnumEquipType equipType, WSItemStack itemStack) {
		super(new SPacketEntityEquipment());
		Validate.notNull(equipType, "Equip type cannot be null!");
		this.entityId = entityId;
		this.equipType = equipType;
		this.itemStack = itemStack == null ? null : itemStack.clone();
		this.changed = false;
		update();
	}

	public SpongeSPacketEntityEquipment(Packet<?> packet) {
		super(packet);
		this.changed = false;
		refresh();
	}

	@Override
	public int getEntityId() {
		return entityId;
	}

	@Override
	public void setEntityId(int entityId) {
		this.entityId = entityId;
		changed = true;
	}

	@Override
	public EnumEquipType getEquipType() {
		return equipType;
	}

	@Override
	public void setEquipType(EnumEquipType equipType) {
		Validate.notNull(equipType, "Equip type cannot be null!");
		this.equipType = equipType;
		changed = true;
	}

	@Override
	public WSItemStack getItemStack() {
		return itemStack.clone();
	}

	@Override
	public void setItemStack(WSItemStack itemStack) {
		this.itemStack = itemStack == null ? null : itemStack.clone();
		changed = true;
	}

	@Override
	public void update() {
		try {
			Field[] fields = getHandler().getClass().getDeclaredFields();
			Arrays.stream(fields).forEach(field -> field.setAccessible(true));
			fields[0].setInt(getHandler(), entityId);
			fields[1].set(getHandler(), EntityEquipmentSlot.valueOf(equipType.name()));
			fields[2].set(getHandler(), itemStack == null ? WSItemStack.of(WSBlockTypes.AIR).getHandled() : itemStack.getHandled());
		} catch (Throwable ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public void refresh() {
		try {
			Field[] fields = getHandler().getClass().getDeclaredFields();
			Arrays.stream(fields).forEach(field -> field.setAccessible(true));
			entityId = fields[0].getInt(getHandler());
			itemStack = new SpongeItemStack((org.spongepowered.api.item.inventory.ItemStack) fields[2].get(getHandler()));
			equipType = EnumEquipType.getByMinecraftName(((Enum) fields[1].get(getHandler())).name()).orElse(EnumEquipType.MAIN_HAND);
		} catch (Throwable ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public boolean hasChanged() {
		return changed;
	}
}
