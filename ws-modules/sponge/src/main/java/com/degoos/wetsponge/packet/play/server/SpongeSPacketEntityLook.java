package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.entity.WSEntity;
import com.flowpowered.math.vector.Vector2i;
import net.minecraft.network.Packet;
import net.minecraft.network.play.server.SPacketEntity;

public class SpongeSPacketEntityLook extends SpongeSPacketEntity implements WSSPacketEntityLook {

	public SpongeSPacketEntityLook(WSEntity entity, Vector2i rotation, boolean onGround) {
		this(entity.getEntityId(), rotation, onGround);
	}

	public SpongeSPacketEntityLook(int entity, Vector2i rotation, boolean onGround) {
		super(new SPacketEntity.S16PacketEntityLook(entity, (byte) rotation.getY(), (byte) rotation.getX(), onGround));
	}

	public SpongeSPacketEntityLook(Packet<?> packet) {
		super(packet);
	}
}
