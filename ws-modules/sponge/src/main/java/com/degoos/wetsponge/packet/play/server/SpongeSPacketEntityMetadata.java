package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.entity.SpongeEntity;
import com.degoos.wetsponge.entity.WSEntity;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.packet.SpongePacket;
import com.degoos.wetsponge.text.WSText;
import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.Validate;
import com.degoos.wetsponge.util.reflection.ReflectionUtils;
import net.minecraft.entity.Entity;
import net.minecraft.network.Packet;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.network.play.server.SPacketEntityMetadata;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SpongeSPacketEntityMetadata extends SpongePacket implements WSSPacketEntityMetadata {

	private int entityId;
	private List entries;
	private boolean changed;

	private static Field dataWatcherObject;

	static {
		try {
			if (WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD))
				dataWatcherObject = ReflectionUtils.setAccessible(Entity.class.getDeclaredField("field_184242_az"));
			else dataWatcherObject = null;
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was initializing a packet class!");
			dataWatcherObject = null;
		}
	}

	public SpongeSPacketEntityMetadata(WSEntity entity) {
		super(new SPacketEntityMetadata());
		Validate.notNull(entity, "Entity cannot be null!");
		this.entityId = entity.getEntityId();
		this.entries = ((Entity) ((SpongeEntity) entity).getHandled()).getDataManager().getDirty();
		if (entries == null) entries = new ArrayList<EntityDataManager.DataEntry>();
		this.changed = false;
		update();
	}

	public SpongeSPacketEntityMetadata(Packet<?> packet) {
		super(packet);
		this.changed = false;
		refresh();
		if (entries == null) entries = new ArrayList<EntityDataManager.DataEntry>();
	}

	@Override
	public int getEntityId() {
		return entityId;
	}

	@Override
	public void setEntityId(int entityId) {
		if (entityId != this.entityId) {
			this.entityId = entityId;
			changed = true;
		}
	}

	@Override
	public void setMetadataOf(WSEntity entity) {
		Validate.notNull(entity, "Entity cannot be null!");
		this.entries = ((Entity) ((SpongeEntity) entity).getHandled()).getDataManager().getDirty();
		changed = true;
	}

	@Override
	public void setCustomName(WSText text) {
		try {
			for (Object o : entries) {

				EntityDataManager.DataEntry<?> entry = (EntityDataManager.DataEntry<?>) o;

				if (entry.getKey().getId() == 2 && (entry.getValue() == null || entry.getValue() instanceof String)) {
					EntityDataManager.DataEntry<String> stringEntry = (EntityDataManager.DataEntry<String>) entry;
					stringEntry.setValue(text.toFormattingText());
					break;
				}

				DataParameter<String> parameter = (DataParameter<String>) dataWatcherObject.get(null);
				entries.add(new EntityDataManager.DataEntry<>(parameter, text == null ? null : text.toFormattingText()));
			}
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was setting a custom name to a metadata packet!");
		}
	}


	@Override
	public void update() {
		try {
			Field[] fields = getHandler().getClass().getDeclaredFields();
			Arrays.stream(fields).forEach(field -> field.setAccessible(true));
			fields[0].setInt(getHandler(), entityId);
			fields[1].set(getHandler(), entries);
		} catch (Throwable ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public void refresh() {
		try {
			Field[] fields = getHandler().getClass().getDeclaredFields();
			Arrays.stream(fields).forEach(field -> field.setAccessible(true));
			this.entityId = fields[0].getInt(getHandler());
			this.entries = (List) fields[1].get(getHandler());
		} catch (Throwable ex) {
			ex.printStackTrace();
		}

	}

	@Override
	public boolean hasChanged() {
		return changed;
	}
}
