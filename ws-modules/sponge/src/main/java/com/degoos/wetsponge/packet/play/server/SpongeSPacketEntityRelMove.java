package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.entity.WSEntity;
import com.flowpowered.math.vector.Vector3i;
import net.minecraft.network.Packet;
import net.minecraft.network.play.server.SPacketEntity;

public class SpongeSPacketEntityRelMove extends SpongeSPacketEntity implements WSSPacketEntityRelMove {

    public SpongeSPacketEntityRelMove(WSEntity entity, Vector3i position, boolean onGround) {
        this(entity.getEntityId(), position, onGround);
    }

    public SpongeSPacketEntityRelMove(int entity, Vector3i position, boolean onGround) {
        super(new SPacketEntity.S15PacketEntityRelMove(entity, position.getX(), position.getY(), position.getZ(), onGround));
    }

    public SpongeSPacketEntityRelMove(Packet<?> packet) {
        super(packet);
    }
}
