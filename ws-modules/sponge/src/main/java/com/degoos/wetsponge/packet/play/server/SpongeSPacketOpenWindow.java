package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.packet.SpongePacket;
import com.degoos.wetsponge.text.SpongeText;
import com.degoos.wetsponge.text.WSText;
import com.degoos.wetsponge.util.InternalLogger;
import net.minecraft.network.Packet;
import net.minecraft.network.play.server.SPacketOpenWindow;
import net.minecraft.util.text.ITextComponent;
import org.spongepowered.api.text.Text;
import org.spongepowered.common.text.SpongeTexts;

import java.lang.reflect.Field;
import java.util.Arrays;

public class SpongeSPacketOpenWindow extends SpongePacket implements WSSPacketOpenWindow {

	private boolean changed;
	private int windowId;
	private String inventoryType;
	private WSText windowTitle;
	private int slotCount;
	private int entityId;

	public SpongeSPacketOpenWindow(int windowId, String inventoryType, WSText windowTitle, int slotCount, int entityId) {
		super(new SPacketOpenWindow());
		this.windowId = windowId;
		this.inventoryType = inventoryType;
		this.windowTitle = windowTitle;
		this.slotCount = slotCount;
		this.entityId = entityId;
		this.changed = false;
		update();
	}

	public SpongeSPacketOpenWindow(Packet<?> packet) {
		super(packet);
		refresh();
		changed = false;
	}

	@Override
	public int getWindowId() {
		return windowId;
	}

	@Override
	public void setWindowId(int windowId) {
		this.windowId = windowId;
		changed = true;
	}

	@Override
	public String getInventoryType() {
		return inventoryType;
	}

	@Override
	public void setInventoryType(String inventoryType) {
		this.inventoryType = inventoryType;
		changed = true;
	}

	@Override
	public WSText getWindowTitle() {
		return windowTitle;
	}

	@Override
	public void setWindowTitle(WSText windowTitle) {
		this.windowTitle = windowTitle;
		changed = true;
	}

	@Override
	public int getSlotCount() {
		return slotCount;
	}

	@Override
	public void setSlotCount(int slotCount) {
		this.slotCount = slotCount;
		changed = true;
	}

	@Override
	public int getEntityId() {
		return entityId;
	}

	@Override
	public void setEntityId(int entityId) {
		this.entityId = entityId;
		changed = true;
	}

	@Override
	public void update() {
		Field[] fields = getHandler().getClass().getDeclaredFields();
		Arrays.stream(fields).forEach(field -> field.setAccessible(true));

		try {
			fields[0].setInt(getHandler(), windowId);
			fields[1].set(getHandler(), inventoryType);
			fields[2].set(getHandler(), SpongeTexts.toComponent((Text) windowTitle.getHandled()));
			fields[3].setInt(getHandler(), slotCount);
			fields[4].setInt(getHandler(), entityId);
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was updating a packet!");
		}
	}

	@Override
	public void refresh() {
		Field[] fields = getHandler().getClass().getDeclaredFields();
		Arrays.stream(fields).forEach(field -> field.setAccessible(true));

		try {
			windowId = fields[0].getInt(getHandler());
			inventoryType = (String) fields[1].get(getHandler());
			windowTitle = SpongeText.of(SpongeTexts.toText((ITextComponent) fields[2].get(getHandler())));
			slotCount = fields[3].getInt(getHandler());
			entityId = fields[4].getInt(getHandler());
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was refreshing a packet!");
		}
	}

	@Override
	public boolean hasChanged() {
		return changed;
	}
}
