package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.packet.SpongePacket;
import com.degoos.wetsponge.server.response.WSServerStatusResponse;
import com.degoos.wetsponge.server.response.WSStatusPlayers;
import com.degoos.wetsponge.server.response.WSStatusVersion;
import com.degoos.wetsponge.text.SpongeText;
import com.degoos.wetsponge.user.SpongeGameProfile;
import com.degoos.wetsponge.util.ListUtils;
import com.degoos.wetsponge.util.Validate;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.stream.Collectors;
import net.minecraft.network.Packet;
import net.minecraft.network.ServerStatusResponse;
import net.minecraft.network.ServerStatusResponse.Players;
import net.minecraft.network.ServerStatusResponse.Version;
import net.minecraft.network.play.server.SPacketSignEditorOpen;
import net.minecraft.util.text.ITextComponent;
import org.spongepowered.api.profile.GameProfile;
import org.spongepowered.api.text.Text;

public class SpongeSPacketServerInfo extends SpongePacket implements WSSPacketServerInfo {

	private WSServerStatusResponse response;
	private boolean changed;

	public SpongeSPacketServerInfo(WSServerStatusResponse response) {
		super(new SPacketSignEditorOpen());
		Validate.notNull(response, "Response cannot be null!");
		this.response = response;
		this.changed = false;
		update();
	}

	public SpongeSPacketServerInfo(Packet<?> packet) {
		super(packet);
		this.changed = false;
		refresh();
	}

	@Override
	public void update() {
		try {
			Field[] fields = getHandler().getClass().getDeclaredFields();
			Arrays.stream(fields).forEach(field -> field.setAccessible(true));
			ServerStatusResponse nmsResponse = new ServerStatusResponse();
			nmsResponse.setFavicon(response.getFaviconString());
			nmsResponse.setServerDescription((ITextComponent) ((SpongeText) response.getMOTD()).getHandled());
			nmsResponse.setVersion(new Version(response.getVersion().getName(), response.getVersion().getProtocol()));
			Players players = new Players(response.getPlayers().getMaxPlayers(), response.getPlayers().getPlayerCount());
			players.setPlayers(ListUtils.toArray(com.mojang.authlib.GameProfile.class, response.getPlayers().getProfiles().stream()
				.map(profile -> (com.mojang.authlib.GameProfile) profile.getHandled()).collect(Collectors.toList())));
			nmsResponse.setPlayers(players);
			fields[1].set(getHandler(), nmsResponse);
		} catch (Throwable ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public void refresh() {
		try {
			Field[] fields = getHandler().getClass().getDeclaredFields();
			Arrays.stream(fields).forEach(field -> field.setAccessible(true));
			ServerStatusResponse nmsResponse = (ServerStatusResponse) fields[1].get(getHandler());
			response = new WSServerStatusResponse(SpongeText.of((Text) nmsResponse.getServerDescription()), nmsResponse.getFavicon(), new WSStatusPlayers(nmsResponse
				.getPlayers().getMaxPlayers(), nmsResponse.getPlayers().getOnlinePlayerCount(), ListUtils.toSet(nmsResponse.getPlayers().getPlayers()).stream()
				.map(profile -> new SpongeGameProfile((GameProfile) profile)).collect(Collectors.toSet())), new WSStatusVersion(nmsResponse.getVersion()
				.getName(), nmsResponse.getVersion().getProtocol()));
		} catch (Throwable ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public boolean hasChanged() {
		return changed || response.hasChanged();
	}

	@Override
	public WSServerStatusResponse getResponse() {
		return response;
	}

	@Override
	public void setResponse(WSServerStatusResponse response) {
		this.response = response;
		this.changed = true;
	}
}
