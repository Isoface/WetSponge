package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.packet.SpongePacket;
import com.degoos.wetsponge.util.Validate;
import com.flowpowered.math.vector.Vector3d;
import net.minecraft.network.Packet;
import net.minecraft.network.play.server.SPacketSignEditorOpen;
import net.minecraft.util.math.BlockPos;

import java.lang.reflect.Field;
import java.util.Arrays;

public class SpongeSPacketSignEditorOpen extends SpongePacket implements WSSPacketSignEditorOpen {

    private Vector3d position;
    private boolean changed;

    public SpongeSPacketSignEditorOpen(Vector3d position) {
        super(new SPacketSignEditorOpen());
        Validate.notNull(position, "Position cannot be null!");
        this.position = position;
        update();
    }

    public SpongeSPacketSignEditorOpen(Packet<?> packet) {
        super(packet);
        refresh();
    }

    @Override
    public void update() {
        try {
            Field[] fields = getHandler().getClass().getDeclaredFields();
            Arrays.stream(fields).forEach(field -> field.setAccessible(true));
            fields[0].set(getHandler(), new BlockPos(position.getX(), position.getY(), position.getZ()));
        } catch (Throwable ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void refresh() {
        try {
            Field[] fields = getHandler().getClass().getDeclaredFields();
            Arrays.stream(fields).forEach(field -> field.setAccessible(true));
            BlockPos blockPos = (BlockPos) fields[0].get(getHandler());
            position = new Vector3d(blockPos.getX(), blockPos.getY(), blockPos.getZ());
        } catch (Throwable ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public boolean hasChanged() {
        return changed;
    }


    @Override
    public Vector3d getPosition() {
        return position;
    }

    @Override
    public void setPosition(Vector3d position) {
        Validate.notNull(position, "Position cannot be null!");
        this.position = position;
        changed = true;
    }
}
