package com.degoos.wetsponge.packet.play.server;

import com.degoos.wetsponge.entity.SpongeEntity;
import com.degoos.wetsponge.entity.WSEntity;
import com.degoos.wetsponge.entity.living.WSLivingEntity;
import com.degoos.wetsponge.packet.SpongePacket;
import com.degoos.wetsponge.util.Validate;
import com.flowpowered.math.vector.Vector2d;
import com.flowpowered.math.vector.Vector3d;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.network.Packet;
import net.minecraft.network.play.server.SPacketSpawnObject;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Optional;
import java.util.UUID;

public class SpongeSPacketSpawnObject extends SpongePacket implements WSSPacketSpawnObject {

    private Optional<WSEntity> entity;
    private int entityId;
    private UUID uniqueId;
    private Vector3d position, velocity;
    private Vector2d rotation;
    private int data;
    private int type;
    private boolean changed;

    public SpongeSPacketSpawnObject(WSEntity entity, int type, int data) {
        super(new SPacketSpawnObject((Entity) ((SpongeEntity) entity).getHandled(), type, data));
        this.entity = Optional.ofNullable(entity);
        refresh();
    }

    public SpongeSPacketSpawnObject(WSEntity entity, Vector3d position, Vector3d velocity, Vector2d rotation, int type, int data) {
        super(new SPacketSpawnObject());
        updateEntity(entity);
        this.position = position;
        this.velocity = velocity;
        this.rotation = rotation;
        this.type = type;
        this.data = data;
        update();
    }

    public SpongeSPacketSpawnObject(Packet<?> packet) {
        super(packet);
        this.entity = Optional.empty();
        refresh();
    }

    public Optional<WSEntity> getEntity() {
        return entity;
    }

    public void setEntity(WSLivingEntity entity) {
        Validate.notNull(entity, "Entity cannot be null!");
        updateEntity(entity);
    }

    public int getEntityId() {
        return entityId;
    }

    public void setEntityId(int entityId) {
        changed = true;
        this.entityId = entityId;
    }

    public UUID getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(UUID uniqueId) {
        changed = true;
        this.uniqueId = uniqueId;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        changed = true;
        this.type = type;
    }

    public Vector3d getPosition() {
        return position;
    }

    public void setPosition(Vector3d position) {
        changed = true;
        this.position = position;
    }

    public Vector3d getVelocity() {
        return velocity;
    }

    public void setVelocity(Vector3d velocity) {
        changed = true;
        this.velocity = velocity;
    }

    public Vector2d getRotation() {
        return rotation;
    }

    public void setRotation(Vector2d rotation) {
        changed = true;
        this.rotation = rotation;
    }


    public int getData() {
        return data;
    }

    public void setData(int data) {
        changed = true;
        this.data = data;
    }

    private void updateEntity(WSEntity entity) {
        EntityLivingBase livingBase = (EntityLivingBase) ((SpongeEntity) entity).getHandled();
        this.entity = Optional.ofNullable(entity);
        this.entityId = livingBase.getEntityId();
        this.uniqueId = livingBase.getUniqueID();
    }

    @Override
    public void update() {
        try {
            Field[] fields = getHandler().getClass().getDeclaredFields();
            Arrays.stream(fields).forEach(field -> field.setAccessible(true));
            fields[0].setInt(getHandler(), entityId);
            fields[1].set(getHandler(), uniqueId);

            fields[2].setDouble(getHandler(), position.getX());
            fields[3].setDouble(getHandler(), position.getY());
            fields[4].setDouble(getHandler(), position.getZ());

            fields[5].setInt(getHandler(), (int) (velocity.getX() * 8000.0D));
            fields[6].setInt(getHandler(), (int) (velocity.getY() * 8000.0D));
            fields[7].setInt(getHandler(), (int) (velocity.getZ() * 8000.0D));

            fields[8].setInt(getHandler(), ((int) (rotation.getX() * 256.0F / 360.0F)));
            fields[9].setInt(getHandler(), ((int) (rotation.getY() * 256.0F / 360.0F)));

            fields[10].setInt(getHandler(), type);
            fields[11].setInt(getHandler(), data);

        } catch (Throwable ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void refresh() {
        try {
            Field[] fields = getHandler().getClass().getDeclaredFields();
            Arrays.stream(fields).forEach(field -> field.setAccessible(true));
            entityId = fields[0].getInt(getHandler());
            uniqueId = (UUID) fields[1].get(getHandler());

            position = new Vector3d(fields[2].getDouble(getHandler()), fields[3].getDouble(getHandler()), fields[4].getDouble(getHandler()));

            velocity = new Vector3d(fields[5].getInt(getHandler()) / 8000.0D,
                    fields[6].getInt(getHandler()) / 8000.0D, fields[7].getInt(getHandler()) / 8000.0D);

            rotation = new Vector2d(fields[8].getInt(getHandler()) * 360.0D / 256.0D, fields[9].getInt(getHandler()) * 360.0D / 256.0D);

            type = fields[10].getInt(getHandler());
            data = fields[11].getInt(getHandler());

            entity = Optional.empty();
        } catch (Throwable ex) {
            ex.printStackTrace();
        }

    }

    @Override
    public boolean hasChanged() {
        return changed;
    }
}
