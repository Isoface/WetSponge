package com.degoos.wetsponge.parser.entity;


import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.entity.SpongeEntity;
import com.degoos.wetsponge.entity.WSEntity;
import com.degoos.wetsponge.entity.living.SpongeCreature;
import com.degoos.wetsponge.entity.living.SpongeLivingEntity;
import com.degoos.wetsponge.entity.living.player.SpongeHuman;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.enums.EnumEntityType;
import com.degoos.wetsponge.enums.EnumServerVersion;
import com.degoos.wetsponge.parser.player.PlayerParser;
import com.degoos.wetsponge.util.Validate;
import com.degoos.wetsponge.util.reflection.ReflectionUtils;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.EntityType;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.entity.living.Creature;
import org.spongepowered.api.entity.living.Human;
import org.spongepowered.api.entity.living.Living;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.entity.projectile.arrow.Arrow;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class SpongeEntityParser {

	private static Set<SpongeEntityData> types;
	private static Map<Entity, WSEntity> entities;

	public static void load() {
		types = new HashSet<>();
		entities = new ConcurrentHashMap<>();
		if (WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_OLD)) {
			types.add(new SpongeEntityData(EntityTypes.AREA_EFFECT_CLOUD, EnumEntityType.AREA_EFFECT_CLOUD));
			types.add(new SpongeEntityData(EntityTypes.DRAGON_FIREBALL, EnumEntityType.DRAGON_FIREBALL));
			types.add(new SpongeEntityData(EntityTypes.SPLASH_POTION, EnumEntityType.LINGERING_POTION));
			types.add(new SpongeEntityData(EntityTypes.SHULKER, EnumEntityType.SHULKER));
			types.add(new SpongeEntityData(EntityTypes.SHULKER_BULLET, EnumEntityType.SHULKER_BULLET));
			types.add(new SpongeEntityData(EntityTypes.TIPPED_ARROW, EnumEntityType.TIPPED_ARROW));
			types.add(new SpongeEntityData(EntityTypes.SPECTRAL_ARROW, EnumEntityType.SPECTRAL_ARROW));
		}
		if (WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_1_9_2)) types.add(new SpongeEntityData(EntityTypes.POLAR_BEAR, EnumEntityType.POLAR_BEAR));
		if (WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_1_10_2)) {
			types.add(new SpongeEntityData(EntityTypes.ELDER_GUARDIAN, EnumEntityType.ELDER_GUARDIAN));
			types.add(new SpongeEntityData(EntityTypes.EVOCATION_ILLAGER, EnumEntityType.EVOCATION_ILLAGER));
			types.add(new SpongeEntityData(EntityTypes.EVOCATION_FANGS, EnumEntityType.EVOCATION_FANGS));
			types.add(new SpongeEntityData(EntityTypes.HUSK, EnumEntityType.HUSK));
			types.add(new SpongeEntityData(EntityTypes.LLAMA, EnumEntityType.LLAMA));
			types.add(new SpongeEntityData(EntityTypes.LLAMA_SPIT, EnumEntityType.LLAMA_SPIT));
			types.add(new SpongeEntityData(EntityTypes.MULE, EnumEntityType.MULE));
			types.add(new SpongeEntityData(EntityTypes.SKELETON_HORSE, EnumEntityType.SKELETON_HORSE));
			types.add(new SpongeEntityData(EntityTypes.STRAY, EnumEntityType.STRAY));
			types.add(new SpongeEntityData(EntityTypes.VEX, EnumEntityType.VEX));
			types.add(new SpongeEntityData(EntityTypes.VINDICATION_ILLAGER, EnumEntityType.VINDICATION_ILLAGER));
			types.add(new SpongeEntityData(EntityTypes.WITHER_SKELETON, EnumEntityType.WITHER_SKELETON));
			types.add(new SpongeEntityData(EntityTypes.ZOMBIE_HORSE, EnumEntityType.ZOMBIE_HORSE));
			types.add(new SpongeEntityData(EntityTypes.ZOMBIE_VILLAGER, EnumEntityType.ZOMBIE_VILLAGER));
			types.add(new SpongeEntityData(EntityTypes.DONKEY, EnumEntityType.DONKEY));
		}
		if (WetSponge.getVersion().isNewerThan(EnumServerVersion.MINECRAFT_1_11_2)) {
			types.add(new SpongeEntityData(EntityTypes.PARROT, EnumEntityType.PARROT));
		}
		types.add(new SpongeEntityData(EntityTypes.ARMOR_STAND, EnumEntityType.ARMOR_STAND));
		types.add(new SpongeEntityData(EntityTypes.TIPPED_ARROW, EnumEntityType.ARROW, Arrow.class)); //ARROW???
		types.add(new SpongeEntityData(EntityTypes.BAT, EnumEntityType.BAT));
		types.add(new SpongeEntityData(EntityTypes.BLAZE, EnumEntityType.BLAZE));
		types.add(new SpongeEntityData(EntityTypes.BOAT, EnumEntityType.BOAT));
		types.add(new SpongeEntityData(EntityTypes.CAVE_SPIDER, EnumEntityType.CAVE_SPIDER));
		types.add(new SpongeEntityData(EntityTypes.CHICKEN, EnumEntityType.CHICKEN));
		types.add(new SpongeEntityData(EntityTypes.COMMANDBLOCK_MINECART, EnumEntityType.COMMAND_BLOCK_MINECART));
		types.add(new SpongeEntityData(EntityTypes.COMPLEX_PART, EnumEntityType.COMPLEX_PART));
		types.add(new SpongeEntityData(EntityTypes.COW, EnumEntityType.COW));
		types.add(new SpongeEntityData(EntityTypes.CREEPER, EnumEntityType.CREEPER));
		types.add(new SpongeEntityData(EntityTypes.EGG, EnumEntityType.EGG));
		types.add(new SpongeEntityData(EntityTypes.ENDER_CRYSTAL, EnumEntityType.ENDER_CRYSTAL));
		types.add(new SpongeEntityData(EntityTypes.ENDER_DRAGON, EnumEntityType.ENDER_DRAGON));
		types.add(new SpongeEntityData(EntityTypes.ENDERMAN, EnumEntityType.ENDERMAN));
		types.add(new SpongeEntityData(EntityTypes.ENDERMITE, EnumEntityType.ENDERMITE));
		types.add(new SpongeEntityData(EntityTypes.ENDER_PEARL, EnumEntityType.ENDER_PEARL));
		types.add(new SpongeEntityData(EntityTypes.EYE_OF_ENDER, EnumEntityType.EYE_OF_ENDER));
		types.add(new SpongeEntityData(EntityTypes.EXPERIENCE_ORB, EnumEntityType.EXPERIENCE_ORB));
		types.add(new SpongeEntityData(EntityTypes.TNT_MINECART, EnumEntityType.TNT_MINECART));
		types.add(new SpongeEntityData(EntityTypes.FALLING_BLOCK, EnumEntityType.FALLING_BLOCK));
		types.add(new SpongeEntityData(EntityTypes.FIREBALL, EnumEntityType.FIREBALL));
		types.add(new SpongeEntityData(EntityTypes.FIREWORK, EnumEntityType.FIREWORK));
		types.add(new SpongeEntityData(EntityTypes.FISHING_HOOK, EnumEntityType.FISHING_HOOK));
		types.add(new SpongeEntityData(EntityTypes.GHAST, EnumEntityType.GHAST));
		types.add(new SpongeEntityData(EntityTypes.GIANT, EnumEntityType.GIANT));
		types.add(new SpongeEntityData(EntityTypes.GUARDIAN, EnumEntityType.GUARDIAN));
		types.add(new SpongeEntityData(EntityTypes.HOPPER_MINECART, EnumEntityType.HOPPER_MINECART));
		types.add(new SpongeEntityData(EntityTypes.HORSE, EnumEntityType.HORSE));
		types.add(new SpongeEntityData(EntityTypes.IRON_GOLEM, EnumEntityType.IRON_GOLEM));
		types.add(new SpongeEntityData(EntityTypes.ITEM, EnumEntityType.ITEM));
		types.add(new SpongeEntityData(EntityTypes.ITEM_FRAME, EnumEntityType.ITEM_FRAME));
		types.add(new SpongeEntityData(EntityTypes.LEASH_HITCH, EnumEntityType.LEASH_HITCH));
		types.add(new SpongeEntityData(EntityTypes.LIGHTNING, EnumEntityType.LIGHTNING));
		types.add(new SpongeEntityData(EntityTypes.MAGMA_CUBE, EnumEntityType.MAGMA_CUBE));
		types.add(new SpongeEntityData(EntityTypes.RIDEABLE_MINECART, EnumEntityType.RIDEABLE_MINECART));
		types.add(new SpongeEntityData(EntityTypes.MUSHROOM_COW, EnumEntityType.MUSHROOM_COW));
		types.add(new SpongeEntityData(EntityTypes.OCELOT, EnumEntityType.OCELOT));
		types.add(new SpongeEntityData(EntityTypes.PAINTING, EnumEntityType.PAINTING));
		types.add(new SpongeEntityData(EntityTypes.PIG, EnumEntityType.PIG));
		types.add(new SpongeEntityData(EntityTypes.PIG_ZOMBIE, EnumEntityType.PIG_ZOMBIE));
		types.add(new SpongeEntityData(EntityTypes.PLAYER, EnumEntityType.PLAYER));
		types.add(new SpongeEntityData(EntityTypes.FURNACE_MINECART, EnumEntityType.FURNACE_MINECART));
		types.add(new SpongeEntityData(EntityTypes.RABBIT, EnumEntityType.RABBIT));
		types.add(new SpongeEntityData(EntityTypes.SHEEP, EnumEntityType.SHEEP));
		types.add(new SpongeEntityData(EntityTypes.SILVERFISH, EnumEntityType.SILVERFISH));
		types.add(new SpongeEntityData(EntityTypes.SKELETON, EnumEntityType.SKELETON));
		types.add(new SpongeEntityData(EntityTypes.SLIME, EnumEntityType.SLIME));
		types.add(new SpongeEntityData(EntityTypes.SMALL_FIREBALL, EnumEntityType.SMALL_FIREBALL));
		types.add(new SpongeEntityData(EntityTypes.SNOWBALL, EnumEntityType.SNOWBALL));
		types.add(new SpongeEntityData(EntityTypes.SNOWMAN, EnumEntityType.SNOWMAN));
		types.add(new SpongeEntityData(EntityTypes.MOB_SPAWNER_MINECART, EnumEntityType.MOB_SPAWNER_MINECART));
		types.add(new SpongeEntityData(EntityTypes.SPIDER, EnumEntityType.SPIDER));
		types.add(new SpongeEntityData(EntityTypes.SPLASH_POTION, EnumEntityType.SPLASH_POTION));
		types.add(new SpongeEntityData(EntityTypes.SQUID, EnumEntityType.SQUID));
		types.add(new SpongeEntityData(EntityTypes.CHESTED_MINECART, EnumEntityType.CHESTED_MINECART));
		types.add(new SpongeEntityData(EntityTypes.THROWN_EXP_BOTTLE, EnumEntityType.THROWN_EXP_BOTTLE));
		types.add(new SpongeEntityData(EntityTypes.PRIMED_TNT, EnumEntityType.PRIMED_TNT));
		types.add(new SpongeEntityData(EntityTypes.VILLAGER, EnumEntityType.VILLAGER));
		types.add(new SpongeEntityData(EntityTypes.WEATHER, EnumEntityType.WEATHER));
		types.add(new SpongeEntityData(EntityTypes.WITCH, EnumEntityType.WITCH));
		types.add(new SpongeEntityData(EntityTypes.WITHER, EnumEntityType.WITHER));
		types.add(new SpongeEntityData(EntityTypes.WITHER_SKULL, EnumEntityType.WITHER_SKULL));
		types.add(new SpongeEntityData(EntityTypes.WOLF, EnumEntityType.WOLF));
		types.add(new SpongeEntityData(EntityTypes.ZOMBIE, EnumEntityType.ZOMBIE));
	}

	public static EnumEntityType getEntityType(Entity entity) {
		return types.stream().filter(data -> data.getEntityClass().isInstance(entity)).findAny().map(SpongeEntityData::getEntityType).orElse(EnumEntityType.UNKNOWN);
	}

	public static SpongeEntityData getEntityData(Entity entity) {
		return types.stream().filter(data -> data.getEntityClass().isInstance(entity)).findAny().orElse(null);
	}

	public static EntityType getSuperEntityType(EnumEntityType type) {
		return types.stream().filter(data -> data.getEntityType() == type).findAny().map(SpongeEntityData::getSpongeEntityType).orElse(EntityTypes.UNKNOWN);
	}

	public static SpongeEntityData getEntityData(EnumEntityType type) {
		return types.stream().filter(data -> data.getEntityType() == type).findAny().orElse(null);
	}


	public static EnumEntityType getEntityType(EntityType type) {
		return types.stream().filter(data -> data.getSpongeEntityType().equals(type)).findAny().map(SpongeEntityData::getEntityType).orElse(EnumEntityType.UNKNOWN);
	}

	public static SpongeEntityData getEntityData(EntityType type) {
		return types.stream().filter(data -> data.getSpongeEntityType().equals(type)).findAny().orElse(null);
	}


	public static WSEntity getWSEntity(Entity entity) {
		Validate.notNull(entity, "Entity cannot be null!");
		if (entities.containsKey(entity)) return entities.get(entity);
		else return addEntity(entity);
	}

	public static WSEntity createHandlerEntity(Entity entity) {
		Validate.notNull(entity, "Entity cannot be null!");
		if (entity instanceof Player) return PlayerParser.getOrCreatePlayer(entity, entity.getUniqueId());
		if (entity instanceof Human) return new SpongeHuman((Human) entity);

		SpongeEntityData data = getEntityData(entity);
		if (data != null && data.getEntityType().getServerClass() != null) {
			try {
				return (WSEntity) ReflectionUtils.getConstructor(data.getEntityType().getServerClass(), data.getEntityClass()).newInstance(entity);
			} catch (Throwable e) {
				e.printStackTrace();
			}
		}

		if (entity instanceof Creature) return new SpongeCreature((Creature) entity);
		if (entity instanceof Living) return new SpongeLivingEntity((Living) entity);

		return new SpongeEntity(entity);
	}

	public static WSEntity addEntity(Entity entity) {
		Validate.notNull(entity, "Entity cannot be null!");
		WSEntity wsEntity = createHandlerEntity(entity);
		entities.put(entity, wsEntity);
		return wsEntity;
	}

	public static void removeEntity(Entity entity) {
		Validate.notNull(entity, "Entity cannot be null!");
		entities.remove(entity);
	}

	public static Optional<WSEntity> getWSEntity(UUID uuid) {
		Optional<WSPlayer> player = WetSponge.getServer().getPlayer(uuid);
		if (player.isPresent()) return Optional.ofNullable(player.get());
		for (WSEntity entity : Collections.unmodifiableCollection(entities.values()))
			if (entity != null && entity.getUniqueId() != null && entity.getUniqueId().equals(uuid)) return Optional.of(entity);
		return Optional.empty();
	}

	public static Optional<WSEntity> getWSEntity(int id) {
		return entities.values().stream().filter(target -> target != null && target.getEntityId() == id).findAny();
	}


	public static boolean containsValue(EnumEntityType type) {
		return types.stream().anyMatch(data -> data.getEntityType() == type);
	}

}
