package com.degoos.wetsponge.parser.packet;

import com.degoos.wetsponge.material.WSBlockTypes;
import com.degoos.wetsponge.material.block.SpongeBlockType;
import com.degoos.wetsponge.material.block.WSBlockType;
import com.degoos.wetsponge.packet.SpongePacket;
import com.degoos.wetsponge.packet.WSPacket;
import com.degoos.wetsponge.packet.play.client.*;
import com.degoos.wetsponge.packet.play.server.*;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.network.Packet;
import net.minecraft.network.play.client.*;
import net.minecraft.network.play.server.*;
import net.minecraft.network.status.server.SPacketServerInfo;
import org.spongepowered.api.block.BlockState;

public class SpongePacketParser {

	public static WSPacket parse(Packet<?> packet) {
		//SERVER
		if (packet instanceof SPacketAnimation) return new SpongeSPacketAnimation(packet);
		if (packet instanceof SPacketBlockChange) return new SpongeSPacketBlockChange(packet);
		if (packet instanceof SPacketMultiBlockChange) return new SpongeSPacketMultiBlockChange(packet);
		if (packet instanceof SPacketSpawnGlobalEntity) return new SpongeSPacketSpawnGlobalEntity(packet);
		if (packet instanceof SPacketSpawnExperienceOrb) return new SpongeSPacketSpawnExperienceOrb(packet);
		if (packet instanceof SPacketSpawnMob) return new SpongeSPacketSpawnMob(packet);
		if (packet instanceof SPacketSpawnObject) return new SpongeSPacketSpawnObject(packet);
		if (packet instanceof SPacketSpawnPlayer) return new SpongeSPacketSpawnPlayer(packet);
		if (packet instanceof SPacketDestroyEntities) return new SpongeSPacketDestroyEntities(packet);
		if (packet instanceof SPacketCloseWindow) return new SpongeSPacketCloseWindows(packet);
		if (packet instanceof SPacketSignEditorOpen) return new SpongeSPacketSignEditorOpen(packet);
		if (packet instanceof SPacketEntity.S17PacketEntityLookMove) return new SpongeSPacketEntityLookMove(packet);
		if (packet instanceof SPacketEntity.S16PacketEntityLook) return new SpongeSPacketEntityLook(packet);
		if (packet instanceof SPacketEntity.S15PacketEntityRelMove) return new SpongeSPacketEntityRelMove(packet);
		if (packet instanceof SPacketEntity) return new SpongeSPacketEntity(packet);
		if (packet instanceof SPacketHeldItemChange) return new SpongeSPacketHeldItemChange(packet);
		if (packet instanceof SPacketEntityMetadata) return new SpongeSPacketEntityMetadata(packet);
		if (packet instanceof SPacketEntityTeleport) return new SpongeSPacketEntityTeleport(packet);
		if (packet instanceof SPacketEntityHeadLook) return new SpongeSPacketEntityHeadLook(packet);
		if (packet instanceof SPacketEntityProperties) return new SpongeSPacketEntityProperties(packet);
		if (packet instanceof SPacketMaps) return new SpongeSPacketMaps(packet);
		if (packet instanceof SPacketServerInfo) return new SpongeSPacketServerInfo(packet);
		if (packet instanceof SPacketWindowItems) return new SpongeSPacketWindowItems(packet);
		if (packet instanceof SPacketSetSlot) return new SpongeSPacketSetSlot(packet);
		if (packet instanceof SPacketOpenWindow) return new SpongeSPacketOpenWindow(packet);
		if (packet instanceof SPacketPlayerListItem) return new SpongeSPacketPlayerListItem(packet);
		if (packet instanceof SPacketEntityEquipment) return new SpongeSPacketEntityEquipment(packet);
		if (packet instanceof SPacketUseBed) return new SpongeSPacketUseBed(packet);

		//CLIENT
		if (packet instanceof CPacketCloseWindow) return new SpongeCPacketCloseWindows(packet);
		if (packet instanceof CPacketUpdateSign) return new SpongeCPacketUpdateSign(packet);
		if (packet instanceof CPacketPlayerTryUseItemOnBlock) return new SpongeCPacketPlayerTryUseItemOnBlock(packet);
		if (packet instanceof CPacketUseEntity) return new SpongeCPacketUseEntity(packet);
		if (packet instanceof CPacketEntityAction) return new SpongeCPacketEntityAction(packet);

		return new SpongePacket(packet) {
			@Override
			public void update() {
			}

			@Override
			public void refresh() {
			}

			@Override
			public boolean hasChanged() {
				return false;
			}
		};
	}

	public static IBlockState getBlockState(WSBlockType material) {
		try {

			Block block = Block.getBlockById(material.getNumericalId());
			return (IBlockState) ((SpongeBlockType) material).writeBlockState((BlockState) block.getDefaultState());
		} catch (Throwable ex) {
			ex.printStackTrace();
			return null;
		}
	}

	public static WSBlockType getMaterial(IBlockState blockState) {
		try {
			Block block = blockState.getBlock();
			int id = Block.REGISTRY.getIDForObject(block);
			WSBlockType type = WSBlockTypes.getById(id).orElse(new SpongeBlockType(id, "", "", 64));
			((SpongeBlockType) type).readContainer((BlockState) blockState);
			return ((SpongeBlockType) type).readContainer((BlockState) blockState);
		} catch (Throwable ex) {
			ex.printStackTrace();
			return null;
		}
	}

}
