package com.degoos.wetsponge.particle;


import com.degoos.wetsponge.entity.living.player.SpongePlayer;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.exception.particle.WSInvalidParticleException;
import com.degoos.wetsponge.world.WSLocation;
import com.flowpowered.math.vector.Vector3d;
import com.flowpowered.math.vector.Vector3f;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.effect.particle.ParticleEffect;
import org.spongepowered.api.effect.particle.ParticleType;

import java.util.Arrays;
import java.util.Collection;
import java.util.Optional;

public class SpongeParticle implements WSParticle {

    private String id, minecraftId, spigotName;
    private ParticleType effect;


    public SpongeParticle(String id, String minecraftId, String spigotName) {
        this.id = id;
        this.minecraftId = minecraftId;
        this.spigotName = spigotName;
        Optional<ParticleType> optional = Sponge.getRegistry().getType(ParticleType.class, minecraftId);
        if (!optional.isPresent()) throw new WSInvalidParticleException("Cannot found particle " + minecraftId + "!");
        effect = optional.get();
    }


    @Override
    public void spawnParticle(WSLocation location, float speed, int amount, WSPlayer... players) {
        spawnParticle(location, speed, amount, new Vector3f(1, 1, 1), players);
    }


    @Override
    public void spawnParticle(WSLocation location, float speed, int amount, Collection<WSPlayer> players) {
        spawnParticle(location, speed, amount, new Vector3f(1, 1, 1), players);
    }


    @Override
    public void spawnParticle(WSLocation location, float speed, int amount, Vector3d playerRadius) {
        spawnParticle(location, speed, amount, new Vector3f(1, 1, 1), playerRadius);
    }


    @Override
    public void spawnParticle(WSLocation location, float speed, int amount, Vector3f radius, WSPlayer... players) {
        spawnParticle(location, speed, amount, radius, Arrays.asList(players));
    }


    @Override
    public void spawnParticle(WSLocation location, float speed, int amount, Vector3f radius, Collection<WSPlayer> players) {
        if (effect == null) return;
        players.forEach(player -> ((SpongePlayer) player).getHandled()
                .spawnParticles(ParticleEffect.builder()
                        .type(effect)
                        .offset(new Vector3d(radius.getX(), radius.getY(), radius.getZ()))
                        .velocity(new Vector3d(speed, speed, speed))
                        .quantity(amount)
                        .build(), location.toVector3d()));
    }


    @Override
    public void spawnParticle(WSLocation location, float speed, int amount, Vector3f radius, Vector3d playerRadius) {
        spawnParticle(location, speed, amount, radius, location.getNearbyPlayers(playerRadius));
    }


    @Override
    public String getOldMinecraftId() {
        return id;
    }


    @Override
    public String getMinecraftId() {
        return minecraftId;
    }

    @Override
    public String getSpigotName() {
        return spigotName;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SpongeParticle that = (SpongeParticle) o;

        return id.equals(that.id);
    }


    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
