package com.degoos.wetsponge.resource.sponge;

import static com.google.common.base.Preconditions.checkNotNull;

import com.degoos.wetsponge.SpongeWetSponge;
import com.degoos.wetsponge.entity.living.player.SpongePlayer;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.parser.player.PlayerParser;
import com.degoos.wetsponge.resource.WSBungeeCord;
import com.degoos.wetsponge.text.WSText;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Maps;
import java.net.InetSocketAddress;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.function.BiPredicate;
import java.util.function.Consumer;
import org.spongepowered.api.GameState;
import org.spongepowered.api.Platform;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.network.ChannelBinding;
import org.spongepowered.api.network.ChannelBuf;
import org.spongepowered.api.network.RawDataListener;
import org.spongepowered.api.network.RemoteConnection;

public class SpongeBungeeCord implements WSBungeeCord {

	private Object plugin;
	private ChannelBinding.RawDataChannel chan;
	private ChannelListener listener;

	public SpongeBungeeCord() {
		plugin = SpongeWetSponge.getInstance();
		chan = Sponge.getChannelRegistrar().createRawChannel(plugin, "BungeeCord");
		listener = new ChannelListener();
		chan.addListener(Platform.Type.SERVER, listener);
	}

	private void checkState() throws IllegalStateException {
		if (!Sponge.getGame().getState().equals(GameState.SERVER_STARTED)) {
			throw new IllegalStateException("Server has not started!");
		}
	}

	private WSPlayer getWSPlayer() throws IllegalStateException {
		try {
			return PlayerParser.getPlayer(Sponge.getServer().getOnlinePlayers().iterator().next().getUniqueId()).orElse(null);
		} catch (NoSuchElementException ex) {
			throw new NoWSPlayerOnlineException();
		}
	}

	private Player getPlayer() throws IllegalStateException {
		try {
			return Sponge.getServer().getOnlinePlayers().iterator().next();
		} catch (NoSuchElementException ex) {
			throw new NoWSPlayerOnlineException();
		}
	}

	private ChannelBuf getChannelBuf() {
		try {
			return (ChannelBuf) Class.forName("org.spongepowered.common.network.SpongeNetworkManager").getMethod("toChannelBuf", Class.forName("io.netty.buffer" +
				".ByteBuf"))
				.invoke(null, Class.forName("io.netty.buffer.Unpooled").getMethod("buffer").invoke(null));
		} catch (Throwable e) {
			throw new RuntimeException(e);
		}
	}

	public void connectWSPlayer(WSPlayer player, String server) {
		checkState();
		checkNotNull(player, "p");
		checkNotNull(server, "server");
		chan.sendTo(((SpongePlayer) player).getHandled(), buf -> buf.writeUTF("Connect").writeUTF(server));
	}

	public void connectWSPlayer(String player, String server) {
		checkState();
		checkNotNull(player, "WSPlayer");
		checkNotNull(server, "server");
		chan.sendTo(getPlayer(), buf -> buf.writeUTF("ConnectOther").writeUTF(player).writeUTF(server));
	}

	public CompletableFuture<InetSocketAddress> getWSPlayerIP(WSPlayer player) {
		checkState();
		checkNotNull(player, "WSPlayer");
		chan.sendTo(((SpongePlayer) player).getHandled(), buf -> buf.writeUTF("IP"));
		CompletableFuture<InetSocketAddress> addressCompletableFuture = new CompletableFuture<>();
		listener.map.put((buf, conn) -> buf.resetRead().readUTF().equals("IP") && conn.equals(player), buf -> addressCompletableFuture
			.complete(new InetSocketAddress(buf.readUTF(), buf.readInteger())));
		return addressCompletableFuture;
	}

	public CompletableFuture<Integer> getWSPlayerCount(String server) {
		checkState();
		checkNotNull(server, "server");
		chan.sendTo(getPlayer(), buf -> buf.writeUTF("PlayerCount").writeUTF(server));
		CompletableFuture<Integer> count = new CompletableFuture<>();
		listener.map.put((buf, conn) -> buf.resetRead().readUTF().equals("PlayerCount") && buf.readUTF().equals(server), buf -> count.complete(buf.readInteger()));
		return count;
	}

	public CompletableFuture<Integer> getGlobalWSPlayerCount() {
		return getWSPlayerCount("ALL");
	}

	public CompletableFuture<List<String>> getOnlineWSPlayers(String server) {
		checkState();
		checkNotNull(server, "server");
		chan.sendTo(getPlayer(), buf -> buf.writeUTF("PlayerList").writeUTF(server));
		CompletableFuture<List<String>> list = new CompletableFuture<>();
		listener.map.put((buf, conn) -> buf.resetRead().readUTF().equals("PlayerList") && buf.readUTF().equals(server), buf -> list
			.complete(ImmutableList.copyOf(buf.readUTF().split(", "))));
		return list;
	}

	public CompletableFuture<List<String>> getAllOnlineWSPlayers() {
		return getOnlineWSPlayers("ALL");
	}

	public CompletableFuture<List<String>> getServerList() {
		checkState();
		CompletableFuture<List<String>> list = new CompletableFuture<>();
		chan.sendTo(getPlayer(), buf -> buf.writeUTF("GetServers"));
		listener.map.put((buf, conn) -> buf.resetRead().readUTF().equals("GetServers"), buf -> list.complete(ImmutableList.copyOf(buf.readUTF().split(", "))));
		return list;
	}

	@SuppressWarnings("deprecated")
	public void sendMessage(String WSPlayer, WSText message) {
		checkState();
		checkNotNull(WSPlayer, "WSPlayer");
		checkNotNull(message, "message");
		chan.sendTo(getPlayer(), buf -> buf.writeUTF("Message").writeUTF(WSPlayer).writeUTF(message.toFormattingText()));
	}

	public CompletableFuture<String> getServerName() {
		checkState();
		chan.sendTo(getPlayer(), buf -> buf.writeUTF("GetServer"));
		CompletableFuture<String> message = new CompletableFuture<>();
		listener.map.put((buf, conn) -> buf.resetRead().readUTF().equals("GetServer"), buf -> message.complete(buf.readUTF()));
		return message;
	}

	public void sendServerPluginMessage(byte[] payload, String channel, String server) {
		Buffer newPayload = ByteBuffer.wrap(payload);
		checkState();
		checkNotNull(payload, "payload");
		checkNotNull(channel, "channel");
		checkNotNull(server, "server");
		ChannelBuf buffer = getChannelBuf();
		buffer.resetRead();
		chan.sendTo(getPlayer(), buf -> buf.writeUTF("Forward").writeUTF(server).writeUTF(channel).writeByteArray(payload));
	}

	public void sendGlobalPluginMessage(byte[] payload, String channel) {
		sendServerPluginMessage(payload, channel, "ALL");
	}

	public void sendWSPlayerPluginMessage(byte[] payload, String channel, String WSPlayer) {
		checkState();
		checkNotNull(payload, "payload");
		checkNotNull(channel, "channel");
		checkNotNull(WSPlayer, "WSPlayer");
		ChannelBuf buffer = getChannelBuf();
		buffer.resetRead();
		chan.sendTo(getPlayer(), buf -> buf.writeUTF("ForwardToPlayer").writeUTF(WSPlayer).writeUTF(channel).writeByteArray(payload));
	}

	public CompletableFuture<UUID> getRealUUID(WSPlayer player) {
		checkState();
		checkNotNull(player, "WSPlayer");
		chan.sendTo(((SpongePlayer) player).getHandled(), buf -> buf.writeUTF("UUID"));
		CompletableFuture<UUID> id = new CompletableFuture<>();
		listener.map.put((buf, conn) -> buf.resetRead().readUTF().equals("UUID") && conn.equals(player), buf -> id.complete(UUID.fromString(buf.readUTF())));
		return id;
	}

	public CompletableFuture<UUID> getRealUUID(String WSPlayer) {
		checkState();
		checkNotNull(WSPlayer, "WSPlayer");
		chan.sendTo(getPlayer(), buf -> buf.writeUTF("UUIDOther").writeUTF(WSPlayer));
		CompletableFuture<UUID> id = new CompletableFuture<>();
		listener.map
			.put((buf, conn) -> buf.resetRead().readUTF().equals("UUIDOther") && buf.readUTF().equals(WSPlayer), buf -> id.complete(UUID.fromString(buf.readUTF())));
		return id;
	}

	public CompletableFuture<InetSocketAddress> getServerIP(String server) {
		checkState();
		checkNotNull(server, "server");
		chan.sendTo(getPlayer(), buf -> buf.writeUTF("ServerIP").writeUTF(server));
		CompletableFuture<InetSocketAddress> address = new CompletableFuture<>();
		listener.map.put((buf, conn) -> buf.resetRead().readUTF().equals("ServerIP") && buf.readUTF().equals(server), buf -> address
			.complete(new InetSocketAddress(buf.readUTF(), buf.readShort())));
		return address;
	}

	public void kickWSPlayer(String WSPlayer, WSText reason) {
		checkState();
		checkNotNull(WSPlayer, "WSPlayer");
		checkNotNull(reason, "reason");
		chan.sendTo(getPlayer(), buf -> buf.writeUTF("KickWSPlayer").writeUTF(WSPlayer).writeUTF(reason.toFormattingText()));
	}

	private static class ChannelListener implements RawDataListener {

		Map<BiPredicate<ChannelBuf, RemoteConnection>, Consumer<ChannelBuf>> map = Maps.newConcurrentMap();

		@Override
		public void handlePayload(ChannelBuf data, RemoteConnection connection, Platform.Type side) {
			map.keySet().stream().filter(pred -> pred.test(data, connection)).findFirst().ifPresent(pred -> {
				map.get(pred).accept(data);
				map.remove(pred);
			});
		}
	}

}
