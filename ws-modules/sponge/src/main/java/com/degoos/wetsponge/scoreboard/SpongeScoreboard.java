package com.degoos.wetsponge.scoreboard;

import com.degoos.wetsponge.enums.EnumCriteria;
import com.degoos.wetsponge.enums.EnumDisplaySlot;
import com.degoos.wetsponge.text.SpongeText;
import com.degoos.wetsponge.text.WSText;
import com.degoos.wetsponge.util.Validate;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import javax.annotation.Nullable;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.scoreboard.Scoreboard;
import org.spongepowered.api.scoreboard.Team;
import org.spongepowered.api.scoreboard.critieria.Criteria;
import org.spongepowered.api.scoreboard.critieria.Criterion;
import org.spongepowered.api.scoreboard.displayslot.DisplaySlot;
import org.spongepowered.api.scoreboard.displayslot.DisplaySlots;
import org.spongepowered.api.scoreboard.objective.Objective;
import org.spongepowered.api.text.Text;

public class SpongeScoreboard implements WSScoreboard {

	private Scoreboard scoreboard;

	public SpongeScoreboard(Scoreboard scoreboard) {
		Validate.notNull(scoreboard, "Scoreboard cannot be null!");
		this.scoreboard = scoreboard;
	}


	@Override
	public Optional<WSObjective> getObjective(String name) {
		Validate.notNull(name, "Name cannot be null!");
		return scoreboard.getObjective(name).map(SpongeObjective::new);
	}

	@Override
	public Optional<WSObjective> getObjective(EnumDisplaySlot displaySlot) {
		Validate.notNull(displaySlot, "DisplaySlot cannot be null!");
		return scoreboard.getObjective(Sponge.getRegistry().getType(DisplaySlot.class, displaySlot.getSpongeName()).orElse(DisplaySlots.SIDEBAR))
			.map(SpongeObjective::new);
	}

	@Override
	public WSObjective getOrCreateObjective(String name, @Nullable WSText displayName, EnumCriteria criteria) {
		Validate.notNull(criteria, "Criteria cannot be null!");
		Validate.notNull(name, "Name cannot be null!");
		Optional<WSObjective> optional = getObjective(name);
		if (optional.isPresent()) return optional.get();
		Objective objective = Objective.builder().name(name).displayName(displayName == null ? Text.of(name) : ((SpongeText) displayName).getHandled())
			.criterion(Sponge.getRegistry().getType(Criterion.class, criteria.getSpongeName()).orElse(Criteria.DUMMY)).build();
		scoreboard.addObjective(objective);
		return new SpongeObjective(objective);
	}

	@Override
	public void updateDisplaySlot(@Nullable WSObjective objective, EnumDisplaySlot displaySlot) {
		Validate.notNull(displaySlot, "DisplaySlot cannot be null!");
		if (objective == null) scoreboard.clearSlot(Sponge.getRegistry().getType(DisplaySlot.class, displaySlot.getSpongeName()).orElse(DisplaySlots.SIDEBAR));
		else {
			if (objective instanceof WSUpdatableObjective) objective = ((WSUpdatableObjective) objective).getObjective();
			scoreboard.updateDisplaySlot(((SpongeObjective) objective).getHandled(), Sponge.getRegistry().getType(DisplaySlot.class, displaySlot.getSpongeName())
				.orElse(DisplaySlots.SIDEBAR));
		}
	}

	@Override
	public Set<WSObjective> getObjectives() {
		return scoreboard.getObjectives().stream().map(SpongeObjective::new).collect(Collectors.toSet());
	}

	@Override
	public void removeObjective(WSObjective objective) {
		Validate.notNull(objective, "Objective cannot be null!");
		if (objective instanceof WSUpdatableObjective) objective = ((WSUpdatableObjective) objective).getObjective();
		scoreboard.removeObjective(((SpongeObjective) objective).getHandled());
	}

	@Override
	public void clearObjectives() {
		new HashSet<>(scoreboard.getObjectives()).forEach(scoreboard::removeObjective);
	}

	@Override
	public Optional<WSTeam> getTeam(String name) {
		Validate.notNull(name, "Name cannot be null!");
		return scoreboard.getTeam(name).map(SpongeTeam::new);
	}

	@Override
	public boolean hasTeam(String name) {
		Validate.notNull(name, "Name cannot be null!");
		return getTeam(name).isPresent();
	}

	@Override
	public WSTeam getOrCreateTeam(String name) {
		Validate.notNull(name, "Name cannot be null!");
		Optional<WSTeam> optional = getTeam(name);
		if (optional.isPresent()) return optional.get();
		Team team = Team.builder().name(name).build();
		scoreboard.registerTeam(team);
		return new SpongeTeam(team);
	}

	@Override
	public Set<WSTeam> getTeams() {
		return scoreboard.getTeams().stream().map(SpongeTeam::new).collect(Collectors.toSet());
	}

	@Override
	public Optional<WSTeam> getMemberTeam(WSText member) {
		Validate.notNull(member, "Member cannot be null!");
		return scoreboard.getMemberTeam(((SpongeText) member).getHandled()).map(SpongeTeam::new);
	}

	@Override
	public boolean unregisterTeam(WSTeam team) {
		Validate.notNull(team, "Team cannot be null!");
		return team.unregister();
	}

	@Override
	public void unregisterAllTeams() {
		getTeams().forEach(WSTeam::unregister);
	}

	@Override
	public Scoreboard getHandled() {
		return scoreboard;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		SpongeScoreboard that = (SpongeScoreboard) o;

		return scoreboard.equals(that.scoreboard);
	}

	@Override
	public int hashCode() {
		return scoreboard.hashCode();
	}
}
