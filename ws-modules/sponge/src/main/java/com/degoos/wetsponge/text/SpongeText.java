package com.degoos.wetsponge.text;


import com.degoos.wetsponge.enums.EnumTextColor;
import com.degoos.wetsponge.enums.EnumTextStyle;
import com.degoos.wetsponge.util.StringUtils;
import com.degoos.wetsponge.resource.centeredmessages.CenteredMessageSender;
import com.degoos.wetsponge.text.action.click.SpongeClickAction;
import com.degoos.wetsponge.text.action.click.WSClickAction;
import com.degoos.wetsponge.text.action.hover.SpongeHoverAction;
import com.degoos.wetsponge.text.action.hover.WSHoverAction;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.TranslatableText;
import org.spongepowered.api.text.format.TextColor;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.text.format.TextStyle;
import org.spongepowered.api.text.serializer.TextSerializers;

import java.util.*;
import java.util.stream.Collectors;

public class SpongeText implements WSText {

	private Text text;

	public static SpongeText of(Text text) {
		if (text instanceof TranslatableText) return new SpongeTranslatableText((TranslatableText) text);
		return new SpongeText(text);
	}

	protected SpongeText(Text text) {
		this.text = text;
	}


	public SpongeText(String string) {
		this.text = Text.of(string);
	}


	public static SpongeText getByFormattingText(String text) {
		return new SpongeText(TextSerializers.LEGACY_FORMATTING_CODE.deserialize(text));
	}

	@Override
	public String getText() {
		return text.toPlainSingle();
	}


	@Override
	public Optional<EnumTextColor> getColor() {
		return EnumTextColor.getByName(text.getColor().getId());
	}


	@Override
	public Collection<EnumTextStyle> getStyles() {
		List<EnumTextStyle> styles = new ArrayList<>();
		TextStyle style = text.getStyle();
		if (style.isBold().orElse(false)) styles.add(EnumTextStyle.BOLD);
		if (style.isItalic().orElse(false)) styles.add(EnumTextStyle.ITALIC);
		if (style.isObfuscated().orElse(false)) styles.add(EnumTextStyle.OBFUSCATED);
		if (style.hasStrikethrough().orElse(false)) styles.add(EnumTextStyle.STRIKETHROUGH);
		if (style.hasUnderline().orElse(false)) styles.add(EnumTextStyle.UNDERLINE);
		return styles;
	}


	@Override
	public Collection<? extends WSText> getChildren() {
		return text.getChildren().stream().map(SpongeText::of).collect(Collectors.toList());
	}


	@Override
	public Optional<WSClickAction> getClickAction() {
		return getHandled().getClickAction().map(SpongeClickAction::of);
	}


	@Override
	public Optional<WSHoverAction> getHoverAction() {
		return getHandled().getHoverAction().map(SpongeHoverAction::of);
	}


	@Override
	public boolean contains(String sequence) {
		if (text.toPlainSingle().contains(sequence)) return true;
		for (WSText text : getChildren()) if (text.contains(sequence)) return true;
		return false;
	}

	@Override
	public List<WSText> split(String regex) {
		List<WSText> list = new ArrayList<>();

		boolean endsWithRegex = false;
		WSText clone = moveChildrenToOneList();
		WSText parent = clone.cloneSingle();
		if (parent.getText().contains(regex)) {
			endsWithRegex = parent.getText().endsWith(regex);
			String[] strings = parent.getText().split(regex);
			parent = clone.cloneSingle(strings.length > 0 ? strings[0] : "");
			if (strings.length > 1) {
				for (int i = 1; i < strings.length; i++) {
					list.add(parent);
					parent = clone.cloneSingle(strings[i]);
				}
			}
		}

		if (endsWithRegex) list.add(parent);
		WSText.Builder lastBuilder = endsWithRegex ? null : parent.toBuilder();

		Collection<? extends WSText> children = clone.getChildren();

		for (WSText child : children) {
			if (child.getText().contains(regex)) {
				String[] strings = child.getText().split(regex);
				if (strings.length <= 1) {
					WSText.Builder childBuilder = applyParent(child, parent, strings.length == 0 ? "" : strings[0]);
					if (endsWithRegex) lastBuilder = childBuilder;
					else lastBuilder.append(childBuilder.build());
					endsWithRegex = child.getText().endsWith(regex);
					if (endsWithRegex) list.add(lastBuilder.build());
					continue;
				}
				if (endsWithRegex) lastBuilder = child.cloneSingle(strings[0]).toBuilder();
				else lastBuilder.append(child.cloneSingle(strings[0]));

				endsWithRegex = child.getText().endsWith(regex);

				for (int i = 1; i < strings.length; i++) {
					list.add(lastBuilder.build());
					WSText.Builder childBuilder = applyParent(child, parent, strings[1]);
					if (!child.getColor().isPresent() && parent.getColor().isPresent()) childBuilder.color(parent.getColor().get());
					Collection<EnumTextStyle> styles = child.getStyles();
					styles.addAll(parent.getStyles());
					childBuilder.style(styles);
					if (!child.getHoverAction().isPresent() && parent.getHoverAction().isPresent()) childBuilder.hoverAction(parent.getHoverAction().get());
					if (!child.getClickAction().isPresent() && parent.getClickAction().isPresent()) childBuilder.clickAction(parent.getClickAction().get());
					lastBuilder = childBuilder;
				}
				if (endsWithRegex) list.add(lastBuilder.build());

			} else {
				WSText.Builder childBuilder = applyParent(child, parent, child.getText());
				if (endsWithRegex) lastBuilder = childBuilder;
				else lastBuilder.append(childBuilder.build());
				endsWithRegex = false;
			}
		}
		if (!endsWithRegex) list.add(lastBuilder.build());
		return list;
	}


	private WSText.Builder applyParent(WSText child, WSText parent, String name) {
		WSText.Builder childBuilder = child.cloneSingle(name).toBuilder();
		if (!child.getColor().isPresent() && parent.getColor().isPresent()) childBuilder.color(parent.getColor().get());
		Collection<EnumTextStyle> styles = child.getStyles();
		styles.addAll(parent.getStyles());
		childBuilder.style(styles);
		if (!child.getHoverAction().isPresent() && parent.getHoverAction().isPresent()) childBuilder.hoverAction(parent.getHoverAction().get());
		if (!child.getClickAction().isPresent() && parent.getClickAction().isPresent()) childBuilder.clickAction(parent.getClickAction().get());
		return childBuilder;
	}


	@Override
	public WSText moveChildrenToOneList() {
		WSText.Builder builder = cloneSingle().toBuilder();
		getChildren().forEach(child -> {
			builder.append(child.cloneSingle());
			child.getChildren().forEach(childChild -> append(builder, childChild, child));
		});
		return builder.build();
	}


	private void append(WSText.Builder builder, WSText child, WSText parent) {
		WSText.Builder childBuilder = child.cloneSingle().toBuilder();
		if (!child.getColor().isPresent() && parent.getColor().isPresent()) childBuilder.color(parent.getColor().get());
		Collection<EnumTextStyle> styles = child.getStyles();
		styles.addAll(parent.getStyles());
		childBuilder.style(styles);
		if (!child.getHoverAction().isPresent() && parent.getHoverAction().isPresent()) childBuilder.hoverAction(parent.getHoverAction().get());
		if (!child.getClickAction().isPresent() && parent.getClickAction().isPresent()) childBuilder.clickAction(parent.getClickAction().get());
		WSText newChild = childBuilder.build();
		builder.append(newChild);
		child.getChildren().forEach(childChild -> append(builder, childChild, newChild));
	}


	@Override
	public WSText replace(String toReplace, String replacement) {
		WSText.Builder clone = cloneSingle(getText().replace(toReplace, replacement)).toBuilder();
		getChildren().forEach(child -> clone.append(child.replace(toReplace, replacement)));
		return clone.build();
	}

	@Override
	public WSText replace(String toReplace, WSText replacement) {
		if (!contains(toReplace)) return this;
		boolean endsWithToReplace = toPlain().endsWith(toReplace);
		List<WSText> split = split(toReplace);
		WSText.Builder firstBuilder = split.get(0).toBuilder();
		firstBuilder.append(replacement);
		for (int i = 1; i < split.size() - (endsWithToReplace ? 0 : 1); i++) {
			WSText text = split.get(i);
			WSText.Builder builder = text.toBuilder();
			builder.append(applyParent(replacement, text, replacement.getText()).build());
			firstBuilder.append(builder.build());
		}
		return firstBuilder.build();
	}


	@Override
	public String toFormattingText() {
		return TextSerializers.LEGACY_FORMATTING_CODE.serialize(text);
	}


	@Override
	public String toPlain() {
		return text.toPlain();
	}

	@Override
	public WSText clone() {
		return clone(getText());
	}

	@Override
	public WSText clone(String string) {
		WSText.Builder text = WSText.builder(string);
		getColor().ifPresent(text::color);
		text.style(getStyles());
		getHoverAction().ifPresent(text::hoverAction);
		getClickAction().ifPresent(text::clickAction);
		getChildren().forEach(child -> text.append(child.clone()));
		return text.build();
	}

	@Override
	public WSText cloneSingle() {
		return cloneSingle(getText());
	}

	@Override
	public WSText cloneSingle(String string) {
		WSText.Builder text = WSText.builder(string);
		getColor().ifPresent(text::color);
		text.style(getStyles());
		getHoverAction().ifPresent(text::hoverAction);
		getClickAction().ifPresent(text::clickAction);
		return text.build();
	}


	@Override
	public Text getHandled() {
		return text;
	}


	public String toString() {
		return text.toString();
	}


	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		SpongeText that = (SpongeText) o;

		return that.toFormattingText().equals(toFormattingText());
	}


	@Override
	public int hashCode() {
		return toFormattingText().hashCode();
	}


	public static class Builder implements WSText.Builder {

		private Text.Builder builder;


		public Builder() {
			builder = Text.builder();
		}


		public Builder(String string) {
			builder = Text.builder(string);
		}


		public Builder(WSText text) {
			builder = ((SpongeText) text).getHandled().toBuilder();
		}


		@Override
		public WSText build() {
			return of(builder.build());
		}


		@Override
		public WSText.Builder color(EnumTextColor color) {
			builder.color(Sponge.getRegistry().getType(TextColor.class, color.name()).orElse(TextColors.WHITE));
			return this;
		}

		@Override
		public WSText.Builder style(EnumTextStyle style) {
			return style(Collections.singleton(style));
		}


		@Override
		public WSText.Builder style(Collection<EnumTextStyle> styles) {
			TextStyle style = new TextStyle(false, false, false, false, false);
			for (EnumTextStyle textStyle : styles) {
				switch (textStyle) {
					case OBFUSCATED:
						style = style.obfuscated(true);
						break;
					case BOLD:
						style = style.bold(true);
						break;
					case STRIKETHROUGH:
						style = style.strikethrough(true);
						break;
					case UNDERLINE:
						style = style.underline(true);
						break;
					case ITALIC:
						style = style.italic(true);
						break;
					case RESET:
						style = new TextStyle(false, false, false, false, false);
						break;
				}
			}
			builder.style(style);
			return this;
		}


		@Override
		public WSText.Builder clickAction(WSClickAction action) {
			if (action == null) builder.onClick(null);
			else builder.onClick(((SpongeClickAction) action).getHandled());
			return this;
		}


		@Override
		public WSText.Builder hoverAction(WSHoverAction action) {
			if (action == null) builder.onHover(null);
			else builder.onHover(((SpongeHoverAction) action).getHandled());
			return this;
		}


		@Override
		public WSText.Builder append(WSText... children) {
			Text[] texts = new Text[children.length];
			for (int i = 0; i < children.length; i++)
				texts[i] = ((SpongeText) children[i]).getHandled();
			builder.append(texts);
			return this;
		}


		@Override
		public WSText.Builder append(Collection<? extends WSText> children) {
			builder.append(children.stream().map(text -> ((SpongeText) text).getHandled()).collect(Collectors.toList()));
			return this;
		}


		@Override
		public WSText.Builder newLine() {
			builder.append(Text.NEW_LINE);
			return this;
		}


		@Override
		public WSText.Builder center() {
			builder = TextSerializers.LEGACY_FORMATTING_CODE
					.deserialize(CenteredMessageSender.getCenteredMessage(TextSerializers.LEGACY_FORMATTING_CODE.serialize(builder.build()))).toBuilder();
			return this;
		}

		@Override
		public WSText.Builder stripColors() {
			builder = TextSerializers.LEGACY_FORMATTING_CODE.deserialize(StringUtils.stripColors(TextSerializers.LEGACY_FORMATTING_CODE.serialize(builder.build())))
					.toBuilder();
			return this;
		}


		@Override
		public WSText.Builder translateColors() {
			builder = TextSerializers.LEGACY_FORMATTING_CODE.deserialize(TextSerializers.LEGACY_FORMATTING_CODE.serialize(builder.build()).replace('&', '\u00A7'))
					.toBuilder();
			return this;
		}
	}
}
