package com.degoos.wetsponge.text.action;

import org.spongepowered.api.text.action.TextAction;

public class SpongeTextAction implements WSTextAction {

    private TextAction<?> action;

    public SpongeTextAction(TextAction<?> action) {
        this.action = action;
    }

    public TextAction<?> getHandled () {
        return action;
    }
}
