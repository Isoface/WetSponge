package com.degoos.wetsponge.text.action.click;

import org.spongepowered.api.text.action.ClickAction;
import org.spongepowered.api.text.action.TextActions;

public class SpongeChangePageAction extends SpongeClickAction implements WSChangePageAction {

    public SpongeChangePageAction(int page) {
        super(TextActions.changePage(page));
    }

    public SpongeChangePageAction(ClickAction.ChangePage action) {
        super(action);
    }

    @Override
    public int getPage() {
        return getHandled().getResult();
    }

    @Override
    public ClickAction.ChangePage getHandled() {
        return (ClickAction.ChangePage) super.getHandled();
    }


}
