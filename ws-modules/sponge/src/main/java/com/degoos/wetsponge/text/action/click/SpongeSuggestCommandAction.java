package com.degoos.wetsponge.text.action.click;

import org.spongepowered.api.text.action.ClickAction;
import org.spongepowered.api.text.action.TextActions;

public class SpongeSuggestCommandAction extends SpongeClickAction implements WSSuggestCommandAction {

    public SpongeSuggestCommandAction(String command) {
        super(TextActions.suggestCommand(command));
    }

    public SpongeSuggestCommandAction(ClickAction.SuggestCommand action) {
        super(action);
    }

    @Override
    public String getCommand() {
        return getHandled().getResult();
    }

    @Override
    public ClickAction.SuggestCommand getHandled() {
        return (ClickAction.SuggestCommand) super.getHandled();
    }
}
