package com.degoos.wetsponge.text.translation;


import java.util.Locale;
import org.spongepowered.api.text.translation.Translation;

public class SpongeTranslation implements WSTranslation {

	private Translation translation;


	public SpongeTranslation (Translation translation) {
		this.translation = translation;
	}


	@Override
	public String get () {
		return translation.get();
	}


	@Override
	public String get (Locale locale) {
		return translation.get(locale);
	}


	@Override
	public String get (Locale locale, Object... args) {
		return translation.get(locale, args);
	}


	@Override
	public String get (Object... args) {
		return translation.get(args);
	}


	@Override
	public String getId () {
		return translation.getId();
	}

	public Translation getHandled() {
		return translation;
	}
}
