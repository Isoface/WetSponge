package com.degoos.wetsponge.user;

import org.spongepowered.api.Sponge;
import org.spongepowered.api.profile.property.ProfileProperty;

import javax.annotation.Nullable;
import java.util.Optional;

public class SpongeProfileProperty implements WSProfileProperty {


	public static WSProfileProperty of(String name, String value, @Nullable String signature) {
		return new SpongeProfileProperty(Sponge.getServer().getGameProfileManager().createProfileProperty(name, value, signature));
	}

	private ProfileProperty profileProperty;


	public SpongeProfileProperty(ProfileProperty profileProperty) {
		this.profileProperty = profileProperty;
	}

	@Override
	public String getName() {
		return profileProperty.getName();
	}

	@Override
	public String getValue() {
		return profileProperty.getValue();
	}

	@Override
	public Optional<String> getSignature() {
		return profileProperty.getSignature();
	}

	@Override
	public ProfileProperty getHandled() {
		return profileProperty;
	}
}
