package com.degoos.wetsponge.util;

import net.minecraft.block.properties.IProperty;
import net.minecraft.block.state.IBlockState;

public class SpongeIBlockStateUtils {


	public <T extends Comparable<T>> T getValueOrElse(IProperty<T> property, IBlockState state, T orElse) {
		try {
			return state.getValue(property);
		} catch (Exception ex) {
			return orElse;
		}
	}
}
