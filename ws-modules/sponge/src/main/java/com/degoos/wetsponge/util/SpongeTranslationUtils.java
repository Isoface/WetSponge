package com.degoos.wetsponge.util;

import com.degoos.wetsponge.text.translation.SpongeTranslation;
import com.degoos.wetsponge.text.translation.WSTranslation;

public class SpongeTranslationUtils {

	public static WSTranslation getMaterialTranslation(String stringId) {
		try {
			return new SpongeTranslation(org.spongepowered.api.Sponge.getRegistry().getType(org.spongepowered.api.item.ItemType.class, stringId).get().getTranslation());
		} catch (Exception ex) {
			return new SpongeTranslation(new org.spongepowered.common.text.translation.SpongeTranslation(""));
		}
	}

}
