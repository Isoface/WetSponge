package com.degoos.wetsponge.util.blockray;

import com.degoos.wetsponge.block.SpongeBlock;
import com.degoos.wetsponge.block.WSBlock;
import com.degoos.wetsponge.material.block.WSBlockType;
import com.degoos.wetsponge.util.Validate;
import com.degoos.wetsponge.world.WSWorld;
import com.flowpowered.math.vector.Vector3d;
import org.spongepowered.api.util.blockray.BlockRay;
import org.spongepowered.api.util.blockray.BlockRayHit;
import org.spongepowered.api.world.World;

import java.util.Optional;
import java.util.function.Predicate;

public class SpongeBlockRay implements WSBlockRay {

	private BlockRay<World> blockRay;

	public SpongeBlockRay(BlockRay<World> blockRay) {
		this.blockRay = blockRay;
	}

	@Override
	public boolean hasNext() {
		return blockRay.hasNext();
	}

	@Override
	public WSBlock next() {
		BlockRayHit<World> hit = blockRay.next();
		return new SpongeBlock(hit.getLocation());
	}

	@Override
	public Optional<WSBlock> end() {
		return blockRay.end().map(target -> new SpongeBlock(target.getLocation()));
	}

	public static class Builder implements WSBlockRay.Builder {

		private WSWorld world;
		private Vector3d origin;
		private Vector3d direction;
		private double yOffset;
		private double maxDistance;
		private Predicate<WSBlockType> skipFilter;

		public Builder(WSWorld world) {
			Validate.notNull(world, "World cannot be null!");
			this.world = world;
			this.skipFilter = target -> true;
		}

		@Override
		public WSBlockRay.Builder origin(Vector3d origin) {
			this.origin = origin;
			return this;
		}

		@Override
		public WSBlockRay.Builder direction(Vector3d direction) {
			this.direction = direction;
			return this;
		}

		@Override
		public WSBlockRay.Builder yOffset(double yOffset) {
			this.yOffset = yOffset;
			return this;
		}

		@Override
		public WSBlockRay.Builder maxDistance(double maxDistance) {
			this.maxDistance = maxDistance;
			return this;
		}

		@Override
		public WSBlockRay.Builder skipFilter(Predicate<WSBlockType> skipFilter) {
			this.skipFilter = skipFilter;
			return this;
		}

		@Override
		public WSBlockRay build() {
			Validate.notNull(origin, "Origin cannot be null!");
			Validate.notNull(direction, "Direction cannot be null!");
			Validate.isTrue(maxDistance > 0, "MaxDistance must be bigger than 0!");
			return new SpongeBlockRay(BlockRay.from((World) world.getHandled(), origin.add(0, yOffset, 0))
					.direction(direction).distanceLimit(maxDistance).stopFilter(target -> true).skipFilter(target ->
							!skipFilter.test(new SpongeBlock(target.getLocation()).getBlockType())).build());
		}
	}
}
