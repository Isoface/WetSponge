package com.degoos.wetsponge.world;

import com.degoos.wetsponge.entity.WSEntity;
import com.degoos.wetsponge.entity.explosive.WSExplosive;
import com.degoos.wetsponge.parser.entity.SpongeEntityParser;
import com.degoos.wetsponge.parser.world.WorldParser;
import com.degoos.wetsponge.util.Validate;
import java.util.Optional;
import javax.annotation.Nullable;
import org.spongepowered.api.entity.explosive.Explosive;
import org.spongepowered.api.world.explosion.Explosion;

public class SpongeExplosion implements WSExplosion {

	private Explosion explosion;

	public SpongeExplosion(Explosion explosion) {
		this.explosion = explosion;
	}

	@Override
	public Optional<WSExplosive> getSourceExplosive() {
		Explosive explosive = explosion.getSourceExplosive().orElse(null);
		if (explosive == null) return Optional.empty();
		WSEntity entity = SpongeEntityParser.getWSEntity(explosive);
		if (entity instanceof WSExplosive) return Optional.of((WSExplosive) entity);
		return Optional.empty();
	}

	@Override
	public float getRadius() {
		return explosion.getRadius();
	}

	@Override
	public boolean canCauseFire() {
		return explosion.canCauseFire();
	}

	@Override
	public boolean shouldPlaySmoke() {
		return explosion.shouldPlaySmoke();
	}

	@Override
	public boolean shouldBreakBlocks() {
		return explosion.shouldBreakBlocks();
	}

	@Override
	public boolean shouldDamageEntities() {
		return explosion.shouldDamageEntities();
	}


	@Override
	public WSLocation getLocation() {
		return new SpongeLocation(explosion.getLocation());
	}

	@Override
	public WSWorld getWorld() {
		return WorldParser.getOrCreateWorld(explosion.getWorld().getName(), explosion.getWorld());
	}

	@Override
	public WSExplosion.Builder toBuilder() {
		return new Builder().sourceExplosive(getSourceExplosive().orElse(null)).radius(getRadius()).canCauseFire(canCauseFire()).shouldPlaySmoke(shouldPlaySmoke())
			.shouldBreakBlocks(shouldBreakBlocks()).shouldDamageEntities(shouldDamageEntities()).location(getLocation());
	}

	@Override
	public Explosion getHandler() {
		return explosion;
	}

	public static class Builder implements WSExplosion.Builder {

		private Explosion.Builder builder;

		public Builder() {
			builder = Explosion.builder();
		}

		@Override
		public WSExplosion.Builder location(WSLocation location) {
			Validate.notNull(location, "Location cannot be null!");
			builder.location(((SpongeLocation) location).getLocation().getLocation());
			return this;
		}

		@Override
		public WSExplosion.Builder sourceExplosive(@Nullable WSExplosive explosive) {
			if (explosive == null) builder.sourceExplosive(null);
			return this;
		}

		@Override
		public WSExplosion.Builder radius(float radius) {
			builder.radius(radius);
			return this;
		}

		@Override
		public WSExplosion.Builder canCauseFire(boolean canCauseFire) {
			builder.canCauseFire(canCauseFire);
			return this;
		}

		@Override
		public WSExplosion.Builder shouldDamageEntities(boolean shouldDamageEntities) {
			builder.shouldDamageEntities(shouldDamageEntities);
			return this;
		}

		@Override
		public WSExplosion.Builder shouldPlaySmoke(boolean shouldPlaySmoke) {
			builder.shouldPlaySmoke(shouldPlaySmoke);
			return this;
		}

		@Override
		public WSExplosion.Builder shouldBreakBlocks(boolean shouldBreakBlocks) {
			builder.shouldBreakBlocks(shouldBreakBlocks);
			return this;
		}


		@Override
		public WSExplosion build() throws IllegalArgumentException {
			return new SpongeExplosion(builder.build());
		}
	}
}
