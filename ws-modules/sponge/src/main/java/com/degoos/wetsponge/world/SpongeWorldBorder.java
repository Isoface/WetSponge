package com.degoos.wetsponge.world;

import com.flowpowered.math.vector.Vector2d;
import com.flowpowered.math.vector.Vector3d;
import org.spongepowered.api.world.WorldBorder;

public class SpongeWorldBorder implements WSWorldBorder {

	private WorldBorder worldBorder;

	public SpongeWorldBorder(WorldBorder worldBorder) {
		this.worldBorder = worldBorder;
	}

	@Override
	public void copyPropertiesFrom(WSWorldBorder worldBorder) {
		this.worldBorder.copyPropertiesFrom(((SpongeWorldBorder) worldBorder).getHandled());
	}

	@Override
	public Vector2d getCenter() {
		Vector3d center = worldBorder.getCenter();
		return new Vector2d(center.getX(), center.getZ());
	}

	@Override
	public void setCenter(Vector2d center) {
		worldBorder.setCenter(center.getX(), center.getY());
	}

	@Override
	public void setCenter(double x, double z) {
		worldBorder.setCenter(x, z);
	}

	@Override
	public double getDamageAmount() {
		return worldBorder.getDamageAmount();
	}

	@Override
	public void setDamageAmount(double damageAmount) {
		worldBorder.setDamageAmount(damageAmount);
	}

	@Override
	public double getDamageThreshold() {
		return worldBorder.getDamageThreshold();
	}

	@Override
	public void setDamageThreshold(double damageThreshold) {
		worldBorder.setDamageThreshold(damageThreshold);
	}

	@Override
	public double getDiameter() {
		return worldBorder.getDiameter();
	}

	@Override
	public void setDiameter(double diameter) {
		worldBorder.setDiameter(diameter);
	}

	@Override
	public void setDiameter(double diameter, long time) {
		worldBorder.setDiameter(diameter, time);
	}

	@Override
	public void setDiameter(double startDiameter, double endDiameter, long time) {
		worldBorder.setDiameter(startDiameter, endDiameter, time);
	}

	@Override
	public double getNewDiameter() {
		return worldBorder.getNewDiameter();
	}

	@Override
	public int getWarningDistance() {
		return worldBorder.getWarningDistance();
	}

	@Override
	public void setWarningDistance(int warningDistance) {
		worldBorder.setWarningDistance(warningDistance);
	}

	@Override
	public int getWarningTime() {
		return worldBorder.getWarningTime();
	}

	@Override
	public void setWaningTime(int waningTime) {
		worldBorder.setWarningTime(waningTime);
	}

	@Override
	public WorldBorder getHandled() {
		return worldBorder;
	}
}
