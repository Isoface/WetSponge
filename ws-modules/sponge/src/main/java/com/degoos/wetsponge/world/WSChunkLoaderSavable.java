package com.degoos.wetsponge.world;

import java.io.IOException;
import net.minecraft.world.World;
import net.minecraft.world.chunk.Chunk;

public interface WSChunkLoaderSavable {

	boolean canSaveChunks(WSWorld world);

	void setSaveChunks(boolean saveChunks, WSWorld world);

	void saveAllChunks(WSWorld wsWorld);

	Chunk loadVanillaChunk(World world, int chunkX, int chunkZ) throws IOException;
}
