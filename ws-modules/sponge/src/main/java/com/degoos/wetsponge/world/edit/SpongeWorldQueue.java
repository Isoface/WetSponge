package com.degoos.wetsponge.world.edit;

import com.degoos.wetsponge.SpongeWetSponge;
import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.material.block.WSBlockType;
import com.degoos.wetsponge.mixin.sponge.interfaces.WSMixinChunk;
import com.degoos.wetsponge.mixin.sponge.interfaces.WSMixinChunkProviderServer;
import com.degoos.wetsponge.packet.play.server.WSSPacketBlockChange;
import com.degoos.wetsponge.parser.packet.SpongePacketParser;
import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.world.WSWorld;
import com.flowpowered.math.vector.Vector2i;
import com.flowpowered.math.vector.Vector3i;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.chunk.Chunk;
import org.spongepowered.api.scheduler.Task;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class SpongeWorldQueue implements WSWorldQueue {

	private Map<Vector2i, Map<Vector3i, WSBlockType>> types;
	private WSWorld world;

	public SpongeWorldQueue(WSWorld world) {
		this.world = world;
		types = new HashMap<>();
	}

	@Override
	public void setBlock(Vector3i position, WSBlockType blockType) {
		if (position.getY() < 0 || position.getY() > world.getProperties().getMaxHeight()) return;
		Vector2i chunk = new Vector2i(position.getX() >> 4, position.getZ() >> 4);
		Map<Vector3i, WSBlockType> chunkMap = types.get(chunk);
		if (chunkMap == null) chunkMap = new HashMap<>();
		chunkMap.put(position, blockType);
		types.put(chunk, chunkMap);
	}

	@Override
	public void flush(boolean sendPacket, boolean async) {
		if (Thread.currentThread() != WetSponge.getMainThread()) {
			Task.builder().execute(() -> flush(sendPacket, async)).submit(SpongeWetSponge.getInstance());
			return;
		}
		try {
			Map<Vector2i, Chunk> chunks = new HashMap<>();
			types.keySet()
					.forEach(vector -> chunks.put(vector, ((WSMixinChunkProviderServer) world.getChunkProvider()).vanillaProvideChunk(vector.getX(), vector.getY())));
			chunks.values().forEach(chunk -> ((WSMixinChunk) chunk).setCanBeUnloaded(false));
			Thread thread = new Thread(() -> {
				if (sendPacket) {
					Set<WSPlayer> players = world.getPlayers();
					types.forEach((vector2i, blocks) -> {
						try {
							Chunk chunk = chunks.get(vector2i);
							blocks.forEach((vector3i, blockType) -> {
								chunk.setBlockState(new BlockPos(vector3i.getX(), vector3i.getY(), vector3i.getZ()), SpongePacketParser.getBlockState(blockType));
								WSSPacketBlockChange packet = WSSPacketBlockChange.of(blockType, vector3i);
								players.forEach(player -> player.sendPacket(packet));
							});
							((WSMixinChunk) chunk).setCanBeUnloaded(true);
						} catch (Exception ex) {
							InternalLogger.printException(ex, "An error has occurred while WetSponge was flushing a queue!");
						}
					});
					return;
				}
				types.forEach((vector2i, blocks) -> {
					try {
						Chunk chunk = chunks.get(vector2i);
						if (!chunk.isLoaded()) chunk.onLoad();
						blocks.forEach((vector3i, blockType) -> chunk
								.setBlockState(new BlockPos(vector3i.getX(), vector3i.getY(), vector3i.getZ()), SpongePacketParser.getBlockState(blockType)));
						((WSMixinChunk) chunk).setCanBeUnloaded(true);
					} catch (Exception ex) {
						InternalLogger.printException(ex, "An error has occurred while WetSponge was flushing a queue!");
					}
				});
			});
			if (async) thread.start();
			else thread.run();
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while WetSponge was flushing a queue!");
		}
	}
}
