package com.degoos.wetsponge.world.generation;

import com.degoos.wetsponge.material.WSBlockTypes;
import com.degoos.wetsponge.material.block.SpongeBlockType;
import com.degoos.wetsponge.material.block.WSBlockType;
import com.flowpowered.math.vector.Vector3i;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.block.BlockType;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.world.extent.MutableBlockVolume;

public class SpongeBlockVolume implements WSBlockVolume {

	private MutableBlockVolume blockVolume;

	public SpongeBlockVolume(MutableBlockVolume blockVolume) {
		this.blockVolume = blockVolume;
	}

	@Override
	public Vector3i getBlockMin() {
		return blockVolume.getBlockMin();
	}

	@Override
	public Vector3i getBlockMax() {
		return blockVolume.getBlockMax();
	}

	@Override
	public Vector3i getBlockSize() {
		return getBlockSize();
	}

	@Override
	public boolean containsBlock(int x, int y, int z) {
		return blockVolume.containsBlock(x, y, z);
	}

	@Override
	public WSBlockType getBlock(int x, int y, int z) {
		BlockState state = blockVolume.getBlock(x, y, z);
		if (state == null) return null;
		WSBlockType blockType = WSBlockTypes.getById(state.getId()).orElse(WSBlockTypes.AIR.getDefaultState());
		return ((SpongeBlockType) blockType).readContainer(state);
	}

	@Override
	public boolean setBlock(int x, int y, int z, WSBlockType blockType) {
		BlockType type = Sponge.getRegistry().getType(BlockType.class, blockType.getStringId()).orElse(BlockTypes.AIR);
		BlockState state = type.getDefaultState();
		state = ((SpongeBlockType) blockType).writeBlockState(state);
		return blockVolume.setBlock(x, y, z, state);
	}

	@Override
	public MutableBlockVolume getHandled() {
		return blockVolume;
	}


}
